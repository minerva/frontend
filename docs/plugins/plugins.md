# Plugin Integration with Minerva

To seamlessly integrate your plugin with Minerva, follow these guidelines to ensure smooth registration, HTML structure creation, and interaction with Minerva.

## Registering plugin with Minerva

Your plugin should utilize the `window.minerva.plugins.registerPlugin` method for plugin registration. When the plugin is initialized, this method should be called inside plugin initialization method. The function `window.minerva.plugins.registerPlugin` takes an object as an argument:

```ts
{
  pluginName: string;
  pluginVersion: string;
  pluginUrl: string;
  withoutPanel: boolean | undefined;
}
```

##### Usage example:

```javascript
window.minerva.plugins.registerPlugin({
  pluginName: 'Your Plugin Name',
  pluginVersion: '1.8.3',
  pluginUrl: 'https://example.com/plugins/plugin.js',
  withoutPanel: false,
});
```

## Creating Plugin's HTML Structure

The `window.minerva.plugins.registerPlugin` method returns object with `element` property which is a DOM element, allowing your plugin to append its HTML content to the DOM. Use this element to create and modify the HTML structure of your plugin.

```javascript
// Plugin registration
const { element } = window.minerva.plugins.registerPlugin({
  pluginName: 'Your Plugin Name',
  pluginVersion: '1.0.0',
  pluginUrl: 'your-plugin-url',
});

// Modify plugin's HTML structure
const yourContent = document.createElement('div');
yourContent.textContent = 'Your Plugin Content';
element.appendChild(yourContent);
```

## Interacting with Minerva

All interactions with Minerva should happen through the `window.minerva` object. This object includes:

- configuration: includes information about available types of elements, reactions, miriam types, configuration options, map types and so on
- methods will be added in the future

## Example of plugin code before bundling:

```javascript
require('../css/styles.css');
const $ = require('jquery');

let pluginContainer;

const createStructure = () => {
  $(
    `<div class="flex flex-col items-center p-2.5">
      <h1 class="text-lg">My plugin ${minerva.configuration.overlayTypes[0].name}</h1>
      <input class="mt-2.5 p-2.5 rounded-s font-semibold outline-none border border-[#cacaca] bg-[#f7f7f8]" value="https://minerva-dev.lcsb.uni.lu/minerva">
      <button type="button" id="remove-listeners">Remove all event listeners</button>
      <button type="button" id="remove-listener">Remove onShowOverlay listener</button>
    </div>`,
  ).appendTo(pluginContainer);
};

function initPlugin() {
  const {
    element,
    events: { addListener, removeListener, removeAllListeners },
  } = minerva.plugins.registerPlugin({
    pluginName,
    pluginVersion,
    pluginUrl,
  });

  pluginContainer = element;
  createStructure();

  const callbackShowOverlay = data => {
    console.log('onShowOverlay', data);
  };

  const callbackRemoveDataOverlay = data => {
    console.log('onRemoveDataOverlay', data);
  };

  addListener('onShowOverlay', callbackShowOverlay);

  addListener('onRemoveDataOverlay', callbackRemoveDataOverlay);

  $('#remove-listener').on('click', function () {
    removeListener('onShowOverlay', callbackShowOverlay);
  });

  $('#remove-listeners').on('click', function () {
    removeAllListeners();
  });
}

initPlugin();
```

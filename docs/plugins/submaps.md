### Submaps

#### Get current open map id

To get current open map id, plugins can use the `getOpenMapId` method defined in `window.minerva.map.data` object available globally. It returns id of current open map.

##### Example of getOpenMapId method usage:

```javascript
window.minerva.map.data.getOpenMapId();
```

#### Get Models

To get data about all available submaps, plugins can use the `getModels` method defined in `window.minerva.map.data`. This method returns array with data about all submaps.

##### Example of getModels usage:

```javascript
window.minerva.map.data.getModels();
```

#### Open Map

To open map, plugins can use the `openMap` method defined in `window.minerva.map`. This method takes one argument: an object with an `id` property that indicates the map ID.

##### Example of openMap usage:

```javascript
window.minerva.map.openMap({ id: 51 });
```

### Data / BioEntities

The methods contained within 'Data / BioEntities' are used to access/modify data on content/drugs/chemicals entities, as well as pin, surface and line markers.

Below is a description of the methods, as well as the types they return. A description of the object types can be found in folder `/docs/types/`.

**Available data access methods include:**

- `getAllChemicals`
  - gets list of searched chemicals
  - returns array of `Chemical`
- `getAllContent`
  - gets list of searched content
  - returns array of `BioEntity`
- `getAllDrugs`
  - gets list of searched drugs
  - returns array of `Drug`
- `getAllMarkers`
  - gets list of added markers
  - returns array of `Marker`
- `getShownElements`
  - gets list of all currently shown content/chemicals/drugs BioEntities + markers
  - returns object of
    ```
    {
        content: BioEntity[]
        drugs: BioEntity[]
        chemicals: BioEntity[]
        markers: Marker[]
    }
    ```

**Available data modify methods include:**

##### `clearAllElements`

- accepts single argument
  - array of `string` which includes one or more of: `'drugs'`, `'chemicals'`, `'content'`, `'marker'`
- clears **all** elements of provided type(s)
- returns nothing
- example:
  ```ts
  window.minerva.data.bioEntities.clearAllElements(['drugs', 'chemicals']);
  ```

##### `addSingleMarker`

- accepts single argument of object below
  - **object:**
    ```
    {
        type: 'pin' OR 'surface' OR 'line'
        id: string [optional]
        color: string
        opacity: number
        x: number [optional]
        y: number [optional]
        width: number [optional]
        height: number [optional]
        number: number [optional]
        modelId: number [optional]
        start: { x: number; y: number } [optional]
        end: { x: number; y: number } [optional]
    }
    ```
  - **id** - optional, if not provided uuidv4 is generated
  - **color** - should be provided in hex format with hash (example: `#FF0000`)
  - **opacity** - should be a float between `0` and `1` (example: `0.54`)
  - **x** - x coord on the map [surface/pin marker only]
  - **y** - y coord on the map [surface/pin marker only]
  - **width** - width of surface
  - **height** - height of surface
  - **number** - number presented on the pin
  - **modelId** - if marker should be visible only on single map, modelId should be provided
  - **start** - start point of the line
  - **end** - end point of the line
- adds one marker to markers list
- returns created `Marker`
- examples:
  ```ts
  window.minerva.data.bioEntities.addSingleMarker({
    type: 'surface',
    color: '#106AD7',
    opacity: 0.67,
    x: 4438,
    y: 1124,
    width: 200,
    height: 300,
    modelId: 52,
  });
  ```
  ```ts
  window.minerva.data.bioEntities.addSingleMarker({
    type: 'pin',
    color: '#106AD7',
    opacity: 1,
    x: 8723,
    y: 4322,
    number: 43,
  });
  window.minerva.data.bioEntities.addSingleMarker({
    type: 'line',
    color: '#106AD7',
    opacity: 1,
    modelId: 52,
    start: {
      x: 8723,
      y: 4322,
    },
    end: {
      x: 4438,
      y: 1124,
    },
  });
  ```

##### `removeSingleMarker`

- accepts single argument of `string` which represents marker `id`
- removes one marker from markers list
- returns nothing
- example:
  ```ts
  window.minerva.data.bioEntities.removeSingleMarker('f4c068d1-9695-4f1b-928c-d0cff92164b2');
  ```

##### `removeAllMarkers`

- removes all markers from markers list
- returns nothing
- example:
  ```ts
  window.minerva.data.bioEntities.removeAllMarkers();
  ```

### Overlays

#### Get list of available data overlays

To get list of available data overlays, plugins can use the `getDataOverlays` method defined in `window.minerva.overlays.data`. This method returns array with all overlays.

##### Example of getDataOverlays usage:

```javascript
window.minerva.overlays.data.getDataOverlays();
```

#### Get list of visible data overlays

To get list of visible data overlays, plugins can use the `getVisibleDataOverlays` method defined in `window.minerva.overlays.data`. This method returns array with all visible data overlays.

##### Example of getVisibleDataOverlays usage:

```javascript
window.minerva.overlays.data.getVisibleDataOverlays();
```

#### Show an overlay

To show an overlay, plugins can use the `showDataOverlay` method defined in `window.minerva.overlays`. This method takes following arguments:

- overlayId - the ID of the overlay that the plugin wants to show.
- setBackgroundEmpty (optional) - whether `showDataOverlay` should set the background to empty if available when it shows overlay. Its value should be a boolean type.

##### Example of showDataOverlay usage:

```javascript
window.minerva.overlays.showDataOverlay(109);

window.minerva.overlays.showDataOverlay(112, true);
```

#### Hide an overlay

To hide an overlay, plugins can use the `hideDataOverlay` method defined in `window.minerva.overlays`. This method takes one argument: the ID of the overlay that the plugin wants to hide.

##### Example of showDataOverlay usage:

```javascript
window.minerva.overlays.hideDataOverlay(109);
```

#### Add an overlay

To add an overlay, plugins can use the `addDataOverlay` method defined in `window.minerva.overlays`. This method takes one argument: the object with the following properties:

- name (string): The name of the overlay.

- description (optional string): A description of the overlay.

- filename (optional string): The filename of the overlay data.

- fileContent (string or text File): The content of the overlay data.

- type (optional string): The type of overlay data.

##### Example of addDataOverlay usage:

```javascript
window.minerva.overlays.addDataOverlay({
  name: 'Plugin Test',
  fileContent: 'plugin test content',
});
```

#### Remove an overlay

To remove an overlay, plugins can use the `removeDataOverlay` method defined in `window.minerva.overlays`. This method takes one argument: the ID of the overlay that the plugin wants to remove.

##### Example of removeDataOverlay usage:

```javascript
window.minerva.overlays.removeDataOverlay(129);
```

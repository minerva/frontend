### Events

Plugins can interact with Minerva by subscribing to events. These events allow plugins to respond to user actions, system events, or changes in the application state.

#### Add Event Listener

To listen for specific events, plugins can use the `addListener` method in `events` object returned by `window.minerva.plugins.registerPlugin`. This method takes two arguments: the name of the event and a callback function to handle the event.
**Available Events**:

- onAddDataOverlay - triggered after successfully adding an overlay; the created overlay is passed as an argument. Example argument:

```json
{
  "name": "Example Overlay",
  "creator": "appu-admin",
  "description": "Different",
  "genomeType": null,
  "genomeVersion": null,
  "idObject": 149,
  "publicOverlay": false,
  "type": "GENERIC",
  "order": 9
}
```

- onRemoveDataOverlay - triggered after successfully removing an overlay; the ID of the removed overlay is passed as an argument. Example argument:

```
43
```

- onShowOverlay - triggered after displaying an overlay on the map; the displayed overlay is passed as an argument. Example argument:

```json
{
  "name": "Generic advanced format overlay",
  "creator": "appu-admin",
  "description": "Data set provided by a user",
  "genomeType": null,
  "genomeVersion": null,
  "idObject": 20,
  "publicOverlay": true,
  "type": "GENERIC",
  "order": 9
}
```

- onHideOverlay - triggered after disabling an overlay on the map; the disabled overlay is passed as an argument. Example argument:

```json
{
  "name": "colored overlay",
  "creator": "appu-admin",
  "description": "",
  "genomeType": null,
  "genomeVersion": null,
  "idObject": 24,
  "publicOverlay": true,
  "type": "GENERIC",
  "order": 10
}
```

- onBackgroundOverlayChange - triggered after changing the background; the identifier of the new background is passed as an argument. Example argument:

```
15
```

- onSearch - triggered after completing a search; the elements returned by the search are passed as arguments. Three separate events 'onSearch' are triggered, each with a different searched category type. Category types include: bioEntity, drugs, chemicals, reaction. Example argument:

```
{
  type: 'drugs',
  searchValues: ['PRKN'],
  results: [
    [{
      bioEntity: {
        id: 38253,
        model: 52,
        glyph: null,
        submodel: null,
        compartment: 46644,
        elementId: 'path_0_sa11305',
        x: 18412,
        y: 3088.653195488725,
        z: 2298,
        width: 80,
        height: 40,
        fontSize: 12,
        fontColor: {
          alpha: 255,
          rgb: -16777216,
        },
        fillColor: {
          alpha: 255,
          rgb: -3342388,
        },
        borderColor: {
          alpha: 255,
          rgb: -16777216,
        },
        visibilityLevel: '4',
        transparencyLevel: '0',
        notes: '',
        symbol: 'PRKN',
        fullName: 'parkin RBR E3 ubiquitin protein ligase',
        abbreviation: null,
        formula: null,
        name: 'PRKN',
        nameX: 18412,
        nameY: 3088.653195488725,
        nameWidth: 80,
        nameHeight: 40,
        nameVerticalAlign: 'MIDDLE',
        nameHorizontalAlign: 'CENTER',
        synonyms: ['AR-JP', 'PDJ', 'parkin'],
        formerSymbols: ['PARK2'],
        activity: false,
        lineWidth: 1,
        complex: 38252,
        initialAmount: null,
        charge: null,
        initialConcentration: 0,
        onlySubstanceUnits: false,
        homodimer: 1,
        hypothetical: null,
        boundaryCondition: false,
        constant: false,
        modificationResidues: [{
          id: 58046,
          idModificationResidue: 'rs2',
          name: '',
          x: 18481.67916137211,
          y: 3118.9740341163433,
          z: 2299,
          width: 15,
          height: 15,
          borderColor: {
            alpha: 255,
            rgb: -16777216,
          },
          fontSize: 10,
          state: null,
          size: 225,
          center: {
            x: 18489.17916137211,
            y: 3126.4740341163433,
          },
          elementId: 'rs2',
        }, ],
        stringType: 'Protein',
        substanceUnits: null,
        references: [{
          link: 'https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRKN',
          type: 'HGNC_SYMBOL',
          resource: 'PRKN',
          id: 173229,
          annotatorClassName: 'lcsb.mapviewer.annotation.services.annotators.HgncAnnotator',
        }, ],
        compartmentName: 'Dopamine metabolism',
        complexName: 'insoluble aggregates',
      },
      perfect: true,
    }, ],
  ],
};
```

- onClear - after clearing the search; no arguments are passed

- onZoomChanged - triggered after changing the zoom level on the map; the zoom level and the map ID are passed as argument. Example argument:

```json
{
  "modelId": 52,
  "zoom": 9.033753064925367
}
```

- onCenterChanged - triggered after the coordinates of the map center change; the coordinates of the center and map ID are passed as argument. Example argument:

```json
{
  "modelId": 52,
  "x": 8557,
  "y": 1675
}
```

- onBioEntityClick - triggered when someone clicks on a pin; the element to which the pin is attached is passed as an argument. Example argument:

```json
{
  "id": 40072,
  "modelId": 52,
  "type": "ALIAS"
}
```

- onPinIconClick - triggered when someone clicks on a pin icon; the element to which the pin is attached is passed as an argument. Example argument:

```json
{
  "id": 40072
}
```

Marker pin:

```json
{
  "id": "b0a478ad-7e7a-47f5-8130-e96cbeaa0cfe"
}
```

- onSurfaceClick - triggered when someone clicks on an overlay surface; the element to which the pin is attached is passed as an argument. Example argument:

```json
{
  "id": 18
}
```

Surface marker overlay:

```json
{
  "id": "a3a5305f-acfa-47ff-bf77-a26d017c6eb3"
}
```

- onSubmapOpen - triggered when submap opens; the submap identifier is passed as an argument. Example argument:

```
52
```

- onSubmapClose - triggered when a submap closes; the submap identifier is passed as an argument. Example argument:

```
51
```

##### Example of adding event listener:

```javascript
const {
  element,
  events: { addListener, removeListener, removeAllListeners },
} = minerva.plugins.registerPlugin({
  pluginName,
  pluginVersion,
  pluginUrl,
});

const callbackShowOverlay = data => {
  console.log('onShowOverlay', data);
};

addListener('onShowOverlay', callbackShowOverlay);
```

#### Remove Event Listener

To remove event listener, plugins can use the `removeListener` method in `events` object returned by `window.minerva.plugins.registerPlugin`. This method takes two arguments: the name of the event and the reference to the callback function previously used to add the listener.

```javascript
const {
  element,
  events: { addListener, removeListener, removeAllListeners },
} = minerva.plugins.registerPlugin({
  pluginName,
  pluginVersion,
  pluginUrl,
});

const callbackShowOverlay = data => {
  console.log('onShowOverlay', data);
};

addListener('onShowOverlay', callbackShowOverlay);

removeListener('onShowOverlay', callbackShowOverlay);
```

#### Remove All Event Listeners

To remove all event listeners attached by a plugin, plugins can use the `removeAllListeners` method in `events` object returned by `window.minerva.plugins.registerPlugin`.

```javascript
const {
  element,
  events: { addListener, removeListener, removeAllListeners },
} = minerva.plugins.registerPlugin({
  pluginName,
  pluginVersion,
  pluginUrl,
});

const callbackShowOverlay = data => {
  console.log('onShowOverlay', data);
};

const callbackHideOverlay = data => {
  console.log('onHideOverlay', data);
};

const callbackOpenSubamp = data => {
  console.log('onSubmapOpen', data);
};

addListener('onHideOverlay', callbackHideOverlay);

addListener('onSubmapOpen', callbackOpenSubamp);

addListener('onShowOverlay', callbackShowOverlay);

removeAllListeners();
```

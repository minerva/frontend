# Error Documentation

## Map Errors

- **Map with provided id does not exist**: This error occurs when the provided map id does not correspond to any existing map.

- **Unable to retrieve the id of the active map: the modelId is not a number**: This error occurs when the modelId parameter provided from store to retrieve the id of the active map is not a number.

## Search Errors

- **Invalid query type. The query should be of string type**: This error occurs when the query parameter is not of string type.

- **Invalid coordinates type or values**: This error occurs when the coordinates parameter is missing keys, or its values are not of number type.

- **Invalid model id type. The model id should be a number**: This error occurs when the modelId parameter is not of number type.

## Project Errors

- **Project does not exist**: This error occurs when the project data is not available.

- **Project ID does not exist**: This error occurs when the project ID is not available.

## Overlay Errors

- **Overlay name is not provided**: This error occurs when the name of the overlay is missing or not provided.

- **Failed to read file**: This error occurs when there is an issue reading the content of a file. Check if it's text file.

- **Invalid type of fileContent**: This error occurs when the fileContent parameter is of an invalid type.

- **Overlay with provided id does not exist**: This error occurs when the provided overlay id does not correspond to any existing overlay.

- **Overlay with provided id is not active**: This error occurs when the provided overlay id corresponds to an overlay that is not currently active.

- **Overlay with provided id is already active**: This error occurs when the provided overlay id corresponds to an overlay that is already active.

## Zoom errors

- **Provided zoom value exceeds max zoom of ...**: This error occurs when `zoom` param of `setZoom` exceeds max zoom value of the selected map

- **Provided zoom value exceeds min zoom of ...**: This error occurs when `zoom` param of `setZoom` exceeds min zoom value of the selected map

## Event Errors

- **Invalid event type: ...**: This error occurs when an event type is not allowed or recognized.

## Plugin Errors

- **Plugin "..." has crashed. Please contact the plugin developer for assistance**: This error occurs when a plugin encounters an unexpected error and crashes. Users are advised to contact the plugin developer for assistance.

- **The URL specified in the plugin script does not match the actual script URL provided during the registration or loading process**: This error occurs when the URL specified in the plugin script does not match the script URL provided during the registration or loading process.

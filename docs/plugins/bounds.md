### Bounds

#### Get Bounds

To get bounds of the current active map, plugins can use the `getBounds` method defined in `window.minerva.map.data` object available globally. It returns object with properties x1, y1, x2, y2

- x1, y1 - top left corner coordinates
- x2, y2 - right bottom corner coordinates

Example of returned object:

```
{
  x1: 12853,
  y1: 4201,
  x2: 23327,
  y2: 9575
}
```

##### Example of getBounds method usage:

```javascript
window.minerva.map.data.getBounds();
```

#### Fit bounds

To zoom in the map in a way that rectangle defined by coordinates is visible, plugins can use the `fitBounds` method defined in `window.minerva.map` object available globally. This method takes one argument: object with properties x1, y1, x2, y2.

- x1, y1 - top left corner coordinates
- x2, y2 - right bottom corner coordinates

##### Example of fitBounds method usage:

```javascript
window.minerva.map.fitBounds({
  x1: 14057.166666666668,
  y1: 6805.337365980873,
  x2: 14057.166666666668,
  y2: 6805.337365980873,
});
```

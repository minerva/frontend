### Project Info

Minerva provides an API that allows users to retrieve specific information about a project. This API exposes various methods to access details related to a project.

**Get Project ID:**
To get project ID, plugins can use the `getProjectId` method defined in `window.minerva.project.data` object.

##### Example usage of getProjectId method:

```javascript
window.minerva.project.data.getProjectId();
```

**Get Project Name:**
To get project name, plugins can use the `getName` method defined in `window.minerva.project.data` object.

##### Example usage of getName method:

```javascript
window.minerva.project.data.getName();
```

**Get Project Version:**
To get project version, plugins can use the `getVersion` method defined in `window.minerva.project.data` object.

##### Example usage of getVersion method:

```javascript
window.minerva.project.data.getVersion();
```

**Get Disease:**
To get disease identifier associated with the project, plugins can use the `getDisease` method defined in `window.minerva.project.data` object.

##### Example usage of getDisease method:

```javascript
window.minerva.project.data.getDisease();
```

**Get Organism:**
To get organism identifier associated with the project, plugins can use the `getOrganism` method defined in `window.minerva.project.data` object.

##### Example usage of getOrganism method:

```javascript
window.minerva.project.data.getOrganism();
```

**Get Api Urls:**
To get Api urls associated with the project, plugins can use the `getApiUrls` method defined in `window.minerva.project.data` object.

##### Example usage of getApiUrls method:

```javascript
window.minerva.project.data.getApiUrls();
```

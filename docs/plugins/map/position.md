### Map position

With use of the methods below plugins can access and modify user position data.

#### Get zoom

To get current zoom value, plugins can use the `window.minerva.map.getZoom()` method, which returns current zoom value as a number.

**Example:**

```ts
const currentZoom = window.minerva.map.getZoom();
console.log(currentZoom); // 5
```

#### Set zoom

To modify current zoom value, plugins can use the `window.minerva.map.setZoom(zoom)` method. This function accepts non-negative number as an argument and returns nothing. If argument is invalid, `setZoom` method throws an error.

**Valid example:**

```ts
window.minerva.map.setZoom(7.54);
console.log(window.minerva.map.getZoom()); // 7.54
```

**Invalid example:**

```ts
window.minerva.map.setZoom(-14);
// Uncaught ZodError: [...]
```

#### Get center

User position is defined as center coordinate. Its value is defined as x/y/z points of current viewport center translated to map position. Plugins can access center value and modify it.

To get current position value, plugins can use the `window.minerva.map.getCenter()` method which returns current position value as an object containing `x`, `y` and `z` fields. All of them are non-negative numbers but `z` is an optional field and it defines current zoom value. If argument is invalid, `getCenter` method throws an error.

**Valid example:**

```ts
const currentCenter = window.minerva.map.getCenter();
console.log(currentCenter); // {x: 13256, y: 8118, z: 5}
```

#### Set center

To modify position center value plugins can use `window.minerva.map.setCenter(positionObject)` which accepts single object as an argument and returns nothing. This object should contain `x`, `y` fields and `z` optionally. All of them are non-negative numbers. If argument is invalid, `setCenter` method throws an error.

**Valid example:**

```ts
window.minerva.map.setCenter({ x: 13256, y: 8118, z: 5 });
console.log(window.minerva.map.getCenter()); // {x: 13256, y: 8118, z: 5}
```

**Invalid example:**

```ts
window.minerva.map.setCenter({ x: 13256, y: 8118, z: -5 });
// Uncaught ZodError: [...]
```

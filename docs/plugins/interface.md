### Interface

#### getOpenedPanel

Return string identifying panel that is currently opened. Available options:

```
undefined
'none'
'search'
'project-info'
'plugins'
'export'
'legend'
'submaps'
'reaction'
'overlays'
'bio-entity'
'comment'
'available-plugins'
'layers';
```

##### Example of getOpenedPanel method usage:

```javascript
window.minerva.map.data.getOpenedPanel();
```

#### hidePanel

Close opened panel.

##### Example of hidePanel method usage:

```javascript
//close specific panel if the panel is opened
window.minerva.map.data.hidePanel('available-plugins');

//close the panel (regardless which one is opened)
window.minerva.map.data.hidePanel('available-plugins');
```

#### OpenPanel

Open specified panel.

##### Example of openPanel method usage:

```javascript
window.minerva.map.data.openPanel('available-plugins');
```

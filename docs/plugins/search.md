### Search

Search can be done by query or coordinates. To search, plugins can use the `triggerSearch` method in `window.minerva.map` object available globally.

**Search by query:**
If we want to search using a query, we need to provide an object with the following properties as an argument:

- query: this should be the search string.
- perfectSearch: this property indicates whether results should be a perfect match or not. Its value should be a boolean type. This property is optional, and by default, its value is `false`.
- fitBounds: should the map be resized to the rectangle fitting all results. Its value should be a boolean type. This property is optional, and by default, its value is `false`.

##### Example of search by query:

```javascript
window.minerva.map.triggerSearch({ query: 'NR4A2;SNCA;aspirin', perfectSearch: true });

window.minerva.map.triggerSearch({ query: 'NR4A2;SNCA;aspirin;morphine;PINK1' });

window.minerva.map.triggerSearch({ query: 'PINK1', fitBounds: true });
```

**Search by coordinates**:
If we want to search using coordinates, we need to provide an object with the following properties as an argument:

- coordinates: this property should indicate the x and y coordinates on the map. Its value should be an object type with x and y properties
- modelId: this property should indicate submap identifier. Its value should be a number type
- fitBounds: should the map be resized to the rectangle fitting all results. Its value should be a boolean type. This property is optional, and by default, its value is `false`.

##### Example of search by query:

```javascript
window.minerva.map.triggerSearch({ coordinates: { x: 947, y: 503 }, modelId: 60 });

window.minerva.map.triggerSearch({
  coordinates: { x: 1947, y: 5203 },
  modelId: 52,
  fitBounds: true,
});

window.minerva.map.triggerSearch({ coordinates: { x: 1947, y: 5203 }, modelId: 60 });

window.minerva.map.triggerSearch({
  coordinates: { x: 1947, y: 5203 },
  modelId: 51,
  fitBounds: true,
});
```

### Legend

#### Set Legend

To set a legend for a specific plugin, plugins can use the `setLegend` method in `legend` object returned by `window.minerva.plugins.registerPlugin`. This method takes one argument:

- legend: an array containing image urls

##### Example of setLegend method usage:

```javascript
const {
  element,
  legend: { setLegend },
} = minerva.plugins.registerPlugin({
  pluginName,
  pluginVersion,
  pluginUrl,
});

setLegend([
  'https://pdmap.uni.lu/minerva/resources/images/legend_d.png',
  'https://pdmap.uni.lu/minerva/resources/images/legend_a.png',
  'https://pdmap.uni.lu/minerva/resources/images/legend_b.png',
]);
```

#### Remove Legend

To remove a legend associated with a specific plugin, plugins can use the `removeLegend` method in the `legend` object returned by `window.minerva.plugins.registerPlugin`.

##### Example of removeLegend method usage:

```javascript
const {
  element,
  legend: { removeLegend },
} = minerva.plugins.registerPlugin({
  pluginName,
  pluginVersion,
  pluginUrl,
});

removeLegend();
```

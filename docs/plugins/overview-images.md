### Overview images

The methods contained within 'Overview images' are used to access data on Overview images and modify behavior of Overview images modal.

Below is a description of the methods, as well as the types they return. description of the object types can be found in folder `/docs/types/`.

**Available data access methods include:**

- `getCurrentOverviewImage`
  - gets currently selected overview image
  - returns `OverviewImageView` or `undefined`
- `getOverviewImage`
  - gets all loaded overview images
  - returns array of `OverviewImageView`

**Available data modify methods include:**

##### `hideOverviewImageModal`

- accepts no arguments
- hides overview image modal if opened
- returns nothing
- example:
  ```ts
  window.minerva.overviewImage.hideOverviewImageModal();
  ```

##### `selectOverviewImage`

- accepts single argument of number representing id of one of loaded overview images
- selects overview image of provided id as current, if image does not exists throws an error
- returns nothing
- example:
  ```ts
  window.minerva.overviewImage.selectOverviewImage(42);
  ```

##### `showOverviewImageModal`

- accepts single argument of number representing id of one of loaded overview images
- selects overview image of provided id as current and opens overview image modal, if image does not exists throws an error
- returns nothing
- example:
  ```ts
  window.minerva.overviewImage.showOverviewImageModal(24);
  ```

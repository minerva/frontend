# State Management

---

## **Overview**

In the project, we've adopted Redux for managing centralized state across the application. Redux serves as the go-to solution for storing data that needs to be accessed by multiple components throughout the application. This includes global data such as user authentication status, fetched data from APIs, and state that affects multiple UI components or pages.

**Use Cases for Redux:**

- **Global Data Sharing**: Redux is used when data needs to be shared and accessed by multiple components across different parts of the application.
- **Complex State Logic**: It's beneficial for managing complex state logic or interactions between different parts of the application, providing a centralized location for state management.

On the other hand, local component state is utilized for managing state that is confined to a specific component and doesn't need to be shared with the rest of the application. Local state is preferred in scenarios where the data is only relevant to a particular component and doesn't impact other parts of the application.

**Use Cases for Local State:**

- Component-specific State: Local state is used when data is specific to a single component and doesn't need to be shared with other parts of the application.
- UI-related State: It's suitable for managing UI-related state within a component, such as form inputs, toggles, or modal visibility.
- Performance Considerations: Local state may be preferred for small-scale components or simple UI elements to avoid unnecessary overhead from setting up Redux. By utilizing Redux for centralized state management and local component state for isolated data requirements, we maintain a clear and efficient state management strategy throughout our application.

## **Introduction to Redux**

Redux is a predictable state container for JavaScript apps, particularly those built with React. It helps manage the state of your application in a centralized store, making it easier to maintain and reason about your application's data flow. [Learn more](https://redux.js.org/introduction/getting-started)

In modern web development, managing application state can become complex as your application grows in size and complexity. As components become more interconnected and data flows between them, it can become challenging to keep track of state changes and ensure that your application behaves as expected.  
Redux addresses these challenges by introducing a single source of truth for your application's state. Instead of scattering state across multiple components, Redux encourages you to store all of your application's state in a centralized store. This makes it easier to understand and debug your application's state, as you have a clear picture of how data flows through your application.

Additionally, integrating [Redux DevTools](https://github.com/reduxjs/redux-devtools) into your development workflow can greatly enhance the debugging experience. [Redux DevTools](https://github.com/reduxjs/redux-devtools) provide a powerful set of tools for inspecting and debugging your Redux state and actions in real-time. By visualizing the state changes and action dispatches, developers can identify and troubleshoot issues more efficiently, making Redux DevTools an invaluable asset for development and debugging tasks.

![][image1]

## **Why Extend Redux with Redux Toolkit?**

Traditional Redux can be verbose and repetitive, leading to boilerplate code and decreased developer productivity. Redux Toolkit is the official, opinionated toolset for efficient Redux development. It streamlines the Redux workflow by providing utilities to simplify common tasks. [Learn more](https://redux-toolkit.js.org/introduction/why-rtk-is-redux-today)

## **Why Redux Thunk?**

The redux-thunk middleware is automatically added to Redux Toolkit. The Redux store only allows for synchronous dispatch actions, and a Redux store doesn’t have any async logic knowledge of its own. It just understands how to dispatch activities in sync and update the state. Do note that Reducer is a pure function; hence cannot be used to handle API calls. It should not cause side effects and should not directly mutate the state. In React, you should not modify it directly. Redux-thunk is a middleware that enables users to use asynchronous functions in place of API calls. [Learn more](https://github.com/reduxjs/redux-thunk)

## **State design**

In application, state management is structured around a set of reducers, each responsible for managing a specific slice of the overall application state. This modular approach to state management ensures a clear separation of concerns and facilitates easier maintenance and scalability of our codebase.

The state is divided into the following slices, each managed by its corresponding reducer:

- search: state related to search functionality.
- project: state related to information about project.
- drugs: state related to drug entities from search functionality.
- chemicals: state related to chemical entities from search functionality.
- bioEntity: state related to bio entities from search functionality.
- drawer: state related to side drawer visibility and content.
- modal: state related to modal visibility and content.
- map: state related to maps visualization, position and interaction.
- backgrounds: state related to map background layers.
- overlays: state related to map overlay layers.
- models: state related to map models related to current project.
- reactions: state related to reactions.
- contextMenu: state related to context menu.
- cookieBanner: state related to cookie consent banner.
- user: state related to user authentication.
- configuration: state related to application configuration.
- overlayBioEntity: state related to bioEntities connected with overlays.
- legend: state related to map legends.
- statistics: state related to statistics about element annotations and reaction annotations.
- compartmentPathways: state related to compartment pathways used in export tab.
- publications: state related to publications and data displayed in publications modal.
- export: state related to export tab.
- plugins: state related to plugin functionality.
- markers: state related to map markers.
- entityNumber: state related to entity numbers displayed on pins.

![][image2]

##

## **Folder Structure for Redux Management**

In our application, Redux state management is organized within a dedicated directory named [**redux**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux). This directory serves as the central location for all Redux-related code. Within the redux directory, we maintain a **store.ts** file, which contains the configuration for our Redux store setup. This includes the creation of the Redux store instance using **configureStore()** from Redux Toolkit, as well as any middleware setup such as Redux Thunk. In addition to the **store.ts** file, each slice of our Redux store has its own dedicated folder within the redux directory.

For example, we have a slice of state for managing user-related data, we have a folder named [user](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/user). Within this folder, we maintain the following files:

- `user.slice.ts`: This file contains the slice definition, including initial state and sometimes reducers.
- `user.thunks.ts`: Thunks are asynchronous action creators that allow us to perform side effects such as API calls. In this file, we define thunks that dispatch actions and interact with the Redux store.
- `user.selectors.ts`: Selectors are functions that extract specific pieces of state from the Redux store. Here, we define selectors that retrieve user-related data from the store, encapsulating any necessary logic for data retrieval.
- `user.reducers.ts`: This file contains any additional reducers specific to the user slice.
- `user.mock.ts`: For testing purposes, we may include a user.mock.ts file that contains mock data for testing the behavior of the user slice.
- `user.types.ts`: Typescript types related to the user slice are maintained in this file. It defines interfaces or types for the data structures used within the user slice, ensuring type safety and consistency throughout the application.

![][image3]

## **How to manage state?**

To manage state efficiently with Redux Toolkit, we utilize several key concepts: slices, thunks, selectors, and reducers. Slices define a portion of the Redux store state along with its related reducers and actions. We use the createSlice function to define slices, encapsulating the initial state and reducers.

### **Slice:**

Slices in Redux Toolkit define a logical grouping of state, reducers, and actions. They provide a structured approach to managing specific parts of the application state. With slices, you encapsulate related state and functionality, making it easier to maintain and reason about your Redux codebase. Redux slices are typically created using the **createSlice** function provided by Redux Toolkit. This function allows you to define initial state, reducers, and additional action creators all within a single slice definition. Slices encapsulate the core principles of Redux by providing a standardized way to handle state mutations through reducers. By organizing your Redux logic into slices, you can modularize your codebase and isolate concerns, leading to improved readability and maintainability.  
Example:

```typescript
// user.slice.ts
import { createSlice } from '@reduxjs/toolkit';
import { getSessionValidReducer, loginReducer } from './user.reducers';
import { UserState } from './user.types';

export const initialState: UserState = {
  loading: 'idle',
  authenticated: false,
  error: { name: '', message: '' },
  login: null,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {},
  extraReducers: builder => {
    loginReducer(builder);
    getSessionValidReducer(builder);
  },
});

export default userSlice.reducer;
```

### **Thunks:**

Thunks enable handling of asynchronous logic such as API requests within Redux. We define thunks using the **createAsyncThunk** function, dispatching actions as necessary. [Learn more about thunks](https://redux.js.org/tutorials/essentials/part-5-async-logic#thunks-and-async-logic)  
Example:

```typescript
// user.thunks.ts
import { createAsyncThunk } from '@reduxjs/toolkit';
import { axiosInstance } from '@/services/api/utils/axiosInstance';
import { validateDataUsingZodSchema } from '@/utils/validateDataUsingZodSchema';
import { loginSchema } from '@/models/loginSchema';
import { sessionSchemaValid } from '@/models/sessionValidSchema';
import { Login, SessionValid } from '@/types/models';
import { getErrorMessage } from '@/utils/getErrorMessage';
import { apiPath } from '../apiPath';
import { closeModal } from '../modal/modal.slice';

export const login = createAsyncThunk(
  'user/login',
  async (credentials: { login: string; password: string }, { dispatch, rejectWithValue }) => {
    try {
      const searchParams = new URLSearchParams(credentials);
      const response = await axiosInstance.post<Login>(apiPath.postLogin(), searchParams, {
        withCredentials: true,
      });

      const isDataValid = validateDataUsingZodSchema(response.data, loginSchema);
      dispatch(closeModal());

      return isDataValid ? response.data : undefined;
    } catch (error) {
      const errorMessage = getErrorMessage({
        error,
        prefix: 'Login',
      });

      return rejectWithValue(errorMessage);
    }
  },
);

export const getSessionValid = createAsyncThunk('user/getSessionValid', async () => {
  const response = await axiosInstance.get<SessionValid>(apiPath.getSessionValid(), {
    withCredentials: true,
  });

  const isDataValid = validateDataUsingZodSchema(response.data, sessionSchemaValid);

  return isDataValid ? response.data.login : null;
});
```

### **Reducers:**

Reducers define how the state is updated based on dispatched actions. [Learn more about reducers](https://redux.js.org/tutorials/essentials/part-1-overview-concepts#reducers)  
Example:

```typescript
// user.reducers.ts
import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { getSessionValid, login } from './user.thunks';
import { UserState } from './user.types';

export const loginReducer = (builder: ActionReducerMapBuilder<UserState>): void => {
  builder
    .addCase(login.pending, state => {
      state.loading = 'pending';
    })
    .addCase(login.fulfilled, (state, action) => {
      state.authenticated = true;
      state.loading = 'succeeded';
      state.login = action.payload?.login || null;
    })
    .addCase(login.rejected, state => {
      state.authenticated = false;
      state.loading = 'failed';
    });
};

export const getSessionValidReducer = (builder: ActionReducerMapBuilder<UserState>): void => {
  builder
    .addCase(getSessionValid.pending, state => {
      state.loading = 'pending';
    })
    .addCase(getSessionValid.fulfilled, (state, action) => {
      state.authenticated = true;
      state.loading = 'succeeded';
      state.login = action.payload;
    })
    .addCase(getSessionValid.rejected, state => {
      state.authenticated = false;
      state.loading = 'failed';
      // TODO: error management to be discussed in the team
    });
};
```

### **Selectors:**

Selectors allow us to efficiently extract specific parts of the state.  
**When using selectors in components, it's often beneficial to use useSelector from react-redux to access the state. This ensures that your component is re-rendered only when the relevant state changes.**  
While **store.getState()** can be used to directly access the Redux store state, it's generally not recommended for use in components. **It can result in unnecessary re-renders of components.** [Learn more about selectors](https://redux.js.org/tutorials/fundamentals/part-5-ui-react#reading-state-from-the-store-with-useselector)  
Below are the implementations of selectors for accessing user-related state:

```typescript
// user.selectors.ts
import { rootSelector } from '@/redux/root/root.selectors';
import { createSelector } from '@reduxjs/toolkit';

export const userSelector = createSelector(rootSelector, state => state.user);

export const authenticatedUserSelector = createSelector(userSelector, state => state.authenticated);
export const loadingUserSelector = createSelector(userSelector, state => state.loading);
export const loginUserSelector = createSelector(userSelector, state => state.login);
```

##

[image1]: StateManagement1.png
[image2]: StateManagement2.png
[image3]: StateManagement3.png

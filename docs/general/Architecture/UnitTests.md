# Unit Tests

---

Unit tests are implemented within the system, both for components and individual utilities. Our aim in implementing the tests was to test all possible scenarios from the user's perspective, we used BDD Testing. In the test development process, we used tools such as Jest and React Testing Library.

Test coverage in the application is between 90-95%. This is due to the fact that we dispensed with redundant tests, i.e. tests that contained themselves. In cases where the tests of a component also tested util behaviour, we dispensed with additional util testing.

![][image1]

##### Example

- Testing the hook function with renderHook (React Testing Library)
- BDD approach: When something, then something
- An internal util was used to enable mocking of the Redux store. This is important because the hook tested here uses Redux data with the help of selectors

##### References

- [https://katalon.com/resources-center/blog/bdd-testing](https://katalon.com/resources-center/blog/bdd-testing)
- [https://jestjs.io/](https://jestjs.io/)
- [https://testing-library.com/docs/react-testing-library/intro/](https://testing-library.com/docs/react-testing-library/intro/)
- [https://react.dev/reference/react/hooks](https://react.dev/reference/react/hooks)

[image1]: UnitTests1.png

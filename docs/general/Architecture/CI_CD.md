# CI/CD

---

### **CI Pipeline**

- Commitlint
  - Standardisation of commit messages according to conventional approvals
  - [https://commitlint.js.org/\#/](https://commitlint.js.org/#/)
  - [https://github.com/conventional-changelog/commitlint/blob/master/docs/guides-ci-setup.md\#gitlab-ci](https://github.com/conventional-changelog/commitlint/blob/master/docs/guides-ci-setup.md#gitlab-ci)
- Danger
  - Coding style
    - linting
    - code coverage
  - Merge request metrics
    - PR should have description and tags
  - Standardization of PR according to Conventional Commits
    - Title standard
  - [https://docs.gitlab.com/ee/development/dangerbot.html](https://docs.gitlab.com/ee/development/dangerbot.html)
- Checks
  - Build
  - Unit tests

### **CD Pipeline (staging)**

- Vercel
- Vercel enables free deployment and its automation (branch change detection and deployment)

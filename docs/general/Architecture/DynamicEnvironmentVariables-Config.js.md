# Dynamic Environment Variables \- Config.js

---

To enable dynamic changes to environment variables after building a Next.js application into static files, without the need for rebuilding the application after variable changes, created a solution where we create a [config.js](https://gitlab.lcsb.uni.lu/minerva/frontend/-/blob/development/public/config.js) file in the [/public](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/public) folder. Inside this file, we assign the variables we want to be dynamic to an object called **window.config**. By assigning these variables to the window object, they are not included in the bundle, allowing us to dynamically change them without rebuilding the application.

```javascript /public/config.js
window.config = {
  BASE_API_URL: 'https://lux1.atcomp.pl/minerva/api',
  BASE_NEW_API_URL: 'https://lux1.atcomp.pl/minerva/new_api/',
  BASE_MAP_IMAGES_URL: 'https://lux1.atcomp.pl/',
  DEFAULT_PROJECT_ID: 'pdmap_appu_test',
  ADMIN_PANEL_URL: 'https://lux1.atcomp.pl/minerva/admin.xhtml',
};
```

Then, using the [**getConfigValue**](<https://gitlab.lcsb.uni.lu/minerva/frontend/-/blob/development/src/constants/index.utils.ts?ref_type=heads#:~:text=export%20const%20getConfigValue%20%3D%20(key%3A%20ConfigKeys)%3A%20string%20%3D%3E%20%7B>) function, we retrieve the value from the **window.config** object and assign them to constants, for example, in the [constants/index.ts](https://gitlab.lcsb.uni.lu/minerva/frontend/-/blob/development/src/constants/index.ts?ref_type=heads#L6) file.

```typescript
export const BASE_API_URL = getConfigValue('BASE_API_URL');
export const BASE_MAP_IMAGES_URL = getConfigValue('BASE_MAP_IMAGES_URL');
export const BASE_NEW_API_URL = getConfigValue('BASE_NEW_API_URL');
export const DEFAULT_PROJECT_ID = getConfigValue('DEFAULT_PROJECT_ID');
```

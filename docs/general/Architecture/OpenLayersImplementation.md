# OpenLayers Implementation

---

The core part of the application is the map, its rendering, browsing and display of elements. We have based it on the OpenLayers tool. An OL (OpenLayers) instance is created and modified with the help of a series of functions and hooks that manage the instance depending on the changes in the Redux store.

![][image1]

The structure of the OL implementation is divided into two main elements: config and listeners. The config contains functions that handle the layers displayed on the map (pins, surfaces, markers, overlays), while the listeners contain functions that listen for various changes on the map (user clicks, position moves and other actions).

##### References

- [https://react.dev/reference/react/hooks](https://react.dev/reference/react/hooks)
- [https://openlayers.org/en/latest/apidoc/module-ol_Map-Map.html](https://openlayers.org/en/latest/apidoc/module-ol_Map-Map.html)

[image1]: OpenLayers1.png

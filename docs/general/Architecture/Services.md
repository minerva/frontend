# Services

---

The application includes three services: two Axios-based and one internal. Axios is a tool for handling HTTP requests efficiently.

1. AxiosInstance Service:
   - This service is used for the old API and supports all paths that require the use of the old API
2. AxiosInstanceNewAPI Service:
   - This service is used for the new API and works in the same way as the previous one
3. PluginsManager Service:
   - It is an internal service that handles the communication between the application-plugin

##### References

- [https://github.com/axios/axios](https://github.com/axios/axios)

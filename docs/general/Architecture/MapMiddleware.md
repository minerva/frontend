# Map Middleware

---

**Map middleware is a key element of the entire Minerva map data management process.** Its action is triggered by any of the following actions:

- setMapData,
- setActiveMap,
- openMapAndSetActive,
- closeMapAndSetMainMapActive,
- setMapBackground,
- openMapAndOrSetActiveIfSelected.

The job of the middleware is to parse the new state of the map in Redux into data, which then goes back into Redux. This is due to the fact that every time the map changes, its current parameters change. Hence, for the sake of data integrity, we have created middleware that handles this situation.

##### Example

When we open an already opened map (setActiveMap), the system determines a new background id, a new position of the user on the map (so that it returns to the position at which it was last on this map), a new model id, and then performs the action of changing the position and data.

##### Middleware file path

[src/redux/map/middleware/map.middleware.ts](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map/middleware/map.middleware.ts)

##### References

- [https://redux-toolkit.js.org/api/createListenerMiddleware](https://redux-toolkit.js.org/api/createListenerMiddleware)

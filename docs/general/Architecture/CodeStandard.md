# Code Standard

---

### **Folders Structure**

├── \[public\]  
├── \[pages\]  
├── \[docs\]  
├── \[cypress\] \*\*\<- structure in cypress docs\*\*  
├── tailwind.config.js  
├── \[src\]  
 ├── \[assets\] \*\*\<- global assets\*\*  
 ├── \[vectors\]  
 ├── \[branding\]  
 └── logo.svg  
 ├── \[icons\]  
 ├── icon-a.svg \*\*\<- kebab-case\*\*  
 └── icon-b.svg  
 └── \[images\]  
 ├── \[banners\]  
 └── main-banner.png  
 ├── \[components\] \*\*\<- domain components\*\*  
 ├── \[DomainName\]  
 ├── \[utils\]  
 └── domainUtil.ts  
 ├── \[Component\]  
 └── \[SthComponent\]  
 └── \[OtherDomainName\]  
 ├── \[OtherComponent\]  
 └── \[SomeComponent\]  
 ├── \[shared\] \*\*\<- not-domain components (shared between domains)\*\*  
 ├── \[Button\]  
 └── \[Select\]  
 ├── \[constants\] \*\*\<- global constants\*\*  
 ├── common.ts  
 ├── api.ts  
 └── map.ts  
 ├── \[services\] \*\*\<- services clients\*\*  
 ├── \[samplecms\]  
 └── ...  
 ├── \[api\]  
 ├── ... \*\*\<- depends on service client\*\*  
 └── index.ts  
 ├── \[types\] \*\*\<- global types\*\*  
 ├── analytics.ts  
 ├── error.ts  
 ├── fonts.ts  
 └── common.ts  
 └── \[utils\] \*\*\<- global helpers\*\*  
 ├── \[analytics\]  
 ├── someHelper.ts  
 └── index.ts  
 ├── \[bugsnag\]  
 └── index.ts  
 ├── \[number\]  
 ├── multiplyNumberBy.ts  
 └── divideNumberBy.ts  
 └── \[array\]  
 └── doSth.ts

### **Component structure**

├── \[DisplayArea\]  
 ├── \[Map\]  
 ├── \[Pin\] \*\*\<- subcomponent\*\*  
 ├── Pin.component.test.ts  
 ├── Pin.test.tsx  
 └── index.ts  
 ├── \[utils\] \*\*\<- single-component utils\*\*  
 ├── getSomeData.ts  
 └── getSomeData.test.ts  
 ├── \[assets\] \*\*\<- single-component assets\*\*  
 ├── some-asset.png  
 └── other-asset.svg  
 ├── Map.component.test.tsx  
 ├── Map.test.tsx  
 ├── Map.types.ts  
 ├── Map.constants.ts  
 ├── Map.style.ts \*\*\<- depends on design framework\*\*  
 └── index.ts  
 ├── \[OtherComponent\]  
 └── ...

### **Linter and rules**

- eslint
- rules: airbnb standard [https://www.npmjs.com/package/eslint-config-airbnb](https://www.npmjs.com/package/eslint-config-airbnb)

### **Naming**

- folder: kebab-case
- asset: kebab-case
- util/hook: camelCase
- Component and its elements: PascalCase
- domain: PascalCase

### **Domain**

- a group of components centred around a core domain: `DisplayArea` , `FunctionalArea` , `Plugin` , `Layout`
- the domain itself does not export any component outside its domain, it is just a bag for storing them
- the division used is the result of a structural approach (the structure of the directories mirrors the structure of the components)

### **Component/Subcomponent structure**

- a single component with possible subcomponents within it
- only create relevant files/directories if necessary
- **naming:** in naming, the base name of the component is repeated for the sake of readability of imports and Developer Experience \- seeing "style.ts", "types.ts" in the editor from many files will not distinguish them so easily
- **single-component single-responsibility:** utilities and assets satisfying the single-component single-responsibility principle are possible in a component \- if we need a complex utility in a component (but only in it), it should go into the appropriate folder
- example:
  - index.ts \- component export \+ types/utils exports (`@components/DisplayArea/Map/Map.tsx` → `@components/DisplayArea/Map`)
    - `export { default } from “./Map.view.tsx”`
  - Map.**component**.tsx \- component view
  - Map.**component.test**.tsx \- component’s unit tests
  - Map.**styles**.scss
  - Map.**types**.ts \- local types // only if type is used in some other place than Map.component.tsx only
  - Map.**constants**.ts \- local constants
  - **utils/…** \- subfolder for utils/hooks
  - **assets/…** \- subfolder for assets

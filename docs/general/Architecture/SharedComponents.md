# Shared Components

---

# **What are Shared Components?**

Shared components are reusable UI elements that can be utilized across different parts of an application. They are designed to encapsulate specific functionality or visual elements, promoting consistency, maintainability, and scalability within the application architecture.

**Purpose of Shared Components:**

- **Reusability**: Shared components allow developers to build UI elements once and reuse them across multiple parts of the application. This reduces redundancy in code and ensures consistency in user interface design.
- **Consistency**: By centralizing common UI elements into shared components, developers can maintain a consistent look and feel throughout the application. This improves the overall user experience and brand identity.
- **Maintenance**: Updates or modifications to shared components are applied universally across all instances where they are used. This simplifies maintenance tasks and reduces the risk of inconsistencies or errors.
- **Scalability**: As the application grows, shared components facilitate scalability by providing a modular approach to UI development. New features or sections can be easily integrated by leveraging existing shared components.

**Shared components are located in [/shared](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared) directory**

Available shared components:

- Accordion
- Button
- DrawerHeading
- DrawerHeadingBackwardButton
- Icon
- IconButton
- Input
- LinkButton
- LoadingIndicator
- Textarea
- Toast

## **Accordion**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Accordion)  
The Accordion component provides an accessible accordion UI pattern for displaying collapsible content sections.

Options:

- children (React.ReactNode): The content sections of the accordion.
- preExpanded (ID\[\]): An array of IDs representing the initially expanded content sections.
- allowMultipleExpanded (boolean): If true, allows multiple content sections to be expanded simultaneously.
- allowZeroExpanded (boolean): If true, allows all content sections to be collapsed.
- onChange ((args: ID\[\]): void): A callback function triggered when the state of the expanded content sections changes.
- className (string): Additional CSS classes to apply to the accordion.

![][image1]

## **Button**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Button)  
The Button component creates customizable buttons with different visual styles and optional icons. It's designed to be flexible and reusable across various parts of application

Options:

- className (string): Additional CSS classes to apply to the button.
- variantStyles (string): Specifies the visual style of the button. Options include 'primary', 'secondary', 'ghost', or 'quiet'. Default is 'primary'.
- icon (string): Specifies the icon to be displayed inside the button. Default is 'chevron-right'.
- isIcon (boolean): If true, an icon will be displayed inside the button. Default is false.
- isFrontIcon (boolean): If true, the icon will be displayed before the button text. Default is false.  
  ![][image2]

## **DrawerHeading**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/DrawerHeading)  
The DrawerHeading component represents the header section of a drawer, typically containing a title and a close button.

Options:

- title (string | React.ReactNode): The title to be displayed in the heading. This can be a string or a React node.

![][image3]

##

## **DrawerHeadingBackwardButton**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/DrawerHeadingBackwardButton)  
The DrawerHeadingBackwardButton component represents a header section with a backward navigation button and a close button for a drawer.

Options:

- backwardFunction (): A function to execute when the backward button is clicked.
- children (React.ReactNode): The content to be displayed next to the backward button.

![][image4]

## **Icon**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Icon)  
The Icon component renders SVG icons from a predefined set of icon types. It allows for easy integration of icons into various parts of your application.

Options:

- name (IconTypes): Specifies the type of icon to be rendered.
- className (string): Additional CSS classes to apply to the icon.

![][image5]

## **IconButton**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/IconButton)  
The IconButton component represents a button with an icon, typically used for triggering actions or navigation within your application. It provides flexibility in styling and functionality.

Options:

- className (string): Additional CSS classes to apply to the button.
- classNameIcon (string): Additional CSS classes to apply to the icon.
- icon (IconTypes): Specifies the type of icon to be displayed on the button.
- isActive (boolean): If true, the button is in an active state.

![][image6]

## **Input**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Input)  
The Input component represents an input field with customizable styling and size options.

Options:

- className (string): Additional CSS classes to apply to the input field.
- styleVariant (StyleVariant): Specifies the style variant of the input field. Options include 'primary'. Default is 'primary'.
- sizeVariant (SizeVariant): Specifies the size variant of the input field. Options include 'small' or 'medium'. Default is 'small'.

![][image7]

## **LinkButton**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/LinkButton)  
The LinkButton component represents a styled anchor tag (link) with customizable variants which look similar to button.

Props:

- variant (VariantStyle): Specifies the style variant of the link button. Options include 'primary'. Default is 'primary'.
- className (string): Additional CSS classes to apply to the link button.
- children (React.ReactNode): The content to be displayed inside the link button.

![][image8]

## **LoadingIndicator**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/LoadingIndicator)  
The LoadingIndicator component displays a loading spinner icon to indicate that content is being loaded or processed.

Options:

- height (number): The height of the loading spinner icon. Default is 16\.
- width (number): The width of the loading spinner icon. Default is 16\.

![][image9]

## **Textarea**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Textarea)  
The Textarea component provides a customizable textarea input field with predefined styling variants.

Options:

- className (string): Additional CSS classes to apply to the textarea.
- styleVariant (StyleVariant): Specifies the style variant of the textarea. Options include 'primary'. Default is 'primary'.

![][image10]

##

## **Toast**

[url](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/shared/Toast)  
The Toast component displays a temporary notification message, typically used to inform users about the outcome of an action.

Options:

- type ('success' | 'error'): Specifies the type of toast. Options include 'success' or 'error'.
- message (string): The message to be displayed in the toast.
- onDismiss ((): void): A callback function to be called when the toast is dismissed.

![][image11]

[image1]: ShareComponents1.png
[image2]: ShareComponents2.png
[image3]: ShareComponents3.png
[image4]: ShareComponents4.png
[image5]: ShareComponents5.png
[image6]: ShareComponents6.png
[image7]: ShareComponents7.png
[image8]: ShareComponents8.png
[image9]: ShareComponents9.png
[image10]: ShareComponents10.png
[image11]: ShareComponents11.png

# Zod

---

To maintain data consistency between the frontend and the backend, we use a tool called Zod. Zod is a tool with the help of which all objects occurring in the application and coming from the backend are defined. These objects are defined in the form of model schemas in the [/src/models](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/models?ref_type=heads) directory.

![][image1]  
We use the models in two ways, firstly to convert them into the form of interfaces so that they can be used for proper application typing.

![][image2]  
Secondly, we use it in thunks, where we parse the incoming data from the backend and compare it with the structure in the model. When the incoming object from the backend differs from the model, it is discarded and an error is thrown. This protects the application from misbehaving when the backend behaves unexpectedly.

##### References

- [https://github.com/colinhacks/zod](https://github.com/colinhacks/zod)
- [https://www.typescriptlang.org/](https://www.typescriptlang.org/)
- [https://redux.js.org/usage/writing-logic-thunks](https://redux.js.org/usage/writing-logic-thunks)

[image1]: Zod1.png
[image2]: Zod2.png

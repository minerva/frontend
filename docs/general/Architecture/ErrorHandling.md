# Error Handling

---

Any error that occurs in the Redux thunk is caught by the appropriate function (rejectWithValue). This function then passes the error message to the middleware (error.middleware.ts), which triggers a show toast with the error content.

Each thunk has a separate try/catch that catches and processes errors in it. It is very important to keep this in mind when modifying thunks or adding new ones. Errors are not automatically caught.

If an error occurs somewhere outside of thunks, use the showToast function directly with the parameter type: error to show a message with the appropriate content. Currently, errors are not clustered in any way with a single function or any other type of algorithm.

![][image1]  
**Example:** Error parsed to error message and then handled by rejectWithValue

![][image2]  
**Example:** Thunk error middleware

##### References

- [https://redux.js.org/usage/writing-logic-thunks](https://redux.js.org/usage/writing-logic-thunks)
- [https://redux-toolkit.js.org/api/createAsyncThunk](https://redux-toolkit.js.org/api/createAsyncThunk)

[image1]: ErrorHandling1.png
[image2]: ErrorHandling2.png

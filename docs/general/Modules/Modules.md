# Modules

---

Application is divided into several modules, each serving a specific purpose in the overall functionality of the application. These modules are designed to encapsulate related functionality, making our codebase more organized, maintainable, and scalable.

## **AppWrapper**

[module URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/AppWrapper)  
The AppWrapper module serves as a central hub for our React application. It encompasses various wrappers essential for the functioning of our application, including:

- **AppWrapper.component.tsx**: This component serves as a container for aggregating all wrappers required by our React application. It includes essential components such as:
  - MapInstanceProvider: Provides the application-wide instance of the OpenLayers map.
  - Toaster: Responsible for displaying toast notifications using the [Sonner](https://www.npmjs.com/package/sonner) library.

```typescript jsx
 export const AppWrapper = ({ children }: AppWrapperProps): JSX.Element => (
 <MapInstanceProvider>
   <Provider store={store}>
     <>
       <Toaster
         position="top-center"
         visibleToasts={1}
         style={{
           width: '700px',
         }}
       />
       {children}
     </>
   </Provider>
 </MapInstanceProvider>
);
```

## **FunctionalArea**

[module URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea)

The FunctionalArea module comprises functional elements that extend beyond the scope of map-related functionalities. Functional components that may not always pertain exclusively to maps. For instance, a context menu might be utilized beyond map interactions, and components like the cookie banner are not inherently tied to the map functionality.  
Module FunctionalArea includes:

- Context Menu
- Cookie Banner
- Map Navigation
- Modal
- NavBar
- TopBar

### **Context menu**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/ContextMenu)  
![][image1]  
The module is located in the [**/ContextMenu**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/ContextMenu) folder, with the main file being **ContextMenu.component.tsx**. Right-clicking on the map displays a context menu that now includes an option to display the MolArt modal if a UnitProtId exists. The state regarding whether the context menu should be displayed and data regarding the context menu is saved in the redux store in a state called **contextMenu** which code is located in directory [**redux/contextMenu**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/contextMenu).

```typescript jsx
 export const ContextMenu = (): React.ReactNode => {
 const dispatch = useAppDispatch();
 const { isOpen, coordinates } = useAppSelector(contextMenuSelector); //visibility
 const unitProtId = useAppSelector(searchedBioEntityElementUniProtIdSelector);

 const isUnitProtIdAvailable = (): boolean => unitProtId !== undefined; // if context menu option should be blocked

 const getUnitProtId = (): string | undefined => {
   return isUnitProtIdAvailable() ? unitProtId : 'no UnitProt ID available';
 };

 const handleOpenMolArtClick = (): void => {
   if (isUnitProtIdAvailable()) {
     dispatch(closeContextMenu());
     dispatch(openMolArtModalById(unitProtId));
   }
 };

 return (
   <div
     className={twMerge(
       'absolute z-10 rounded-lg border border-[\#DBD9D9] bg-white p-4',
       isOpen ? '' : 'hidden',
     )}
     style={{
       left: `${coordinates[FIRST_ARRAY_ELEMENT]}px`,
       top: `${coordinates[SECOND_ARRAY_ELEMENT]}px`,
     }}
     data-testid="context-modal"
   >
     <button
       className={twMerge(
         'cursor-pointer text-xs font-normal',
         !isUnitProtIdAvailable() ? 'cursor-not-allowed text-greyscale-700' : '',
       )}
       onClick={handleOpenMolArtClick}
       type="button"
       data-testid="open-molart"
     >
       Open MolArt ({getUnitProtId()})
     </button>
   </div>
 );
};
```

![][image2]

### **Cookie banner**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/CookieBanner)  
![][image3]

The module is located in the [**/CookieBanner**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/CookieBanner) folder, with the main file being **CookieBanner.component.tsx**. Cookie consent is saved in local storage and when entering the application it is synchronized with the redux store in the **cookieBanner** state and displaying/hiding the cookie banner in accordance with the saved consent. State **cookieBanner** available in [redux/cookieBanner](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/cookieBanner)

```typescript jsx
 export const CookieBanner = (): React.ReactNode => {
 const dispatch = useAppDispatch();
 const { visible, accepted } = useAppSelector(selectCookieBanner); // BANNER VISIBILITY
//SYNC STORE WITH LOCAL STORAGE BELOW:
 useEffect(() => {
   const isAccepted =
     localStorage.getItem(USER_ACCEPTED_COOKIES_COOKIE_NAME) ===
     USER_ACCEPTED_COOKIES_COOKIE_VALUE.ACCEPTED;
   if (isAccepted) {
     dispatch(acceptCookies());
   } else {
     dispatch(showBanner());
   }
 }, [dispatch]);

 const handleAcceptCookies = (): void => {
   dispatch(acceptCookies());
   localStorage.setItem(
     USER_ACCEPTED_COOKIES_COOKIE_NAME,
     USER_ACCEPTED_COOKIES_COOKIE_VALUE.ACCEPTED,
   );
 };

 if (!visible || accepted) {
   return null;
 }

 return (<div/>
//....
 );
};
```

![][image4]

### **Map navigation**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/MapNavigation)  
![][image5]

The module is located in the [**/MapNavigation**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/MapNavigation) folder, with the main file being **MapNavigation.component.tsx**. It is responsible for displaying tabs with all open maps/submaps and highlighting the tab that is equivalent to the active map. Clicking on a tab changes the active map to the map assigned to the tab. Data about open submaps is taken from the redux store from the **map** state, which is located in the [**redux/map**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map) folder.

### **Modal**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/Modal)  
![][image6]

A modal is a user interface element that appears on top of the main content to prompt the user for information or confirm an action before proceeding further. It typically requires user interaction and draws focus by dimming the background content.  
The module containing all available modals within the application:

- Edit Overlay Modal
- Login Modal
- MolArt Modal
- Overview Images Modal
- Publications Modal
- LoggedIn Menu Modal

All modals are defined in separate folders within the main [**/Modal**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/Modal) directory and utilized in the **Modal.component.tsx** file. The currently displayed modal is determined by the value in the Redux store's state named **modal** available in [redux/modal](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/modal) folder, state includes properties such as **isOpen**, **modalName**, **modalTitle** and other properties for the state of specific modals

```typescript jsx
 export const Modal = (): React.ReactNode => {
 const { isOpen, modalName } = useAppSelector(modalSelector);

 return (
   <>
     {isOpen && modalName === 'overview-images' && (
       <ModalLayout>
         <OverviewImagesModal />
       </ModalLayout>
     )}
    ...
   </>
 );
};
```

![][image7]

### **NavBar**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/NavBar)  
![][image8]

The module located in the [**/NavBar**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/NavBar) folder, with the main file being **NavBar.component.tsx**, contains navigation allowing for the opening of specific drawers and docs. It also displays information about the project version and the logos.

### **TopBar**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/TopBar)  
![][image9]  
The module located in the [**/TopBar**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/FunctionalArea/TopBar) folder, with the main file being **TopBar.component.tsx**, contains:

- Clear Anchors Button \- button that clears all anchors from the map and is located in **/ClearAnchorsButton** directory
- SearchBar \- search input responsible for searching data and displaying results in drawer. Located in **/SearchBar**
- User Login Button \- responsible for displaying login modal. Button located in **/User/UnauthenticatedUser** directory
- Logged-In User Button \- responsible for displaying logged in button and user options. Located in **/User/AuthenticatedUser** directory
- Submaps Button \- responsible for opening submaps drawer. Defined in **TopBar.component.tsx** file.
- Overlays Button \- responsible for opening overlays' drawer. Defined in **TopBar.component.tsx** file.
- Browse overview images Button \- responsible for opening overview images modal. Button defined in **TopBar.component.tsx** file.
- Project name \- Defined in **TopBar.component.tsx** file.

## **Map**

[module URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map)  
The Map module contains components and functionalities specifically related to the map and interactions with it. This module focuses on features and utilities tailored to map rendering and manipulation. Module Map includes:

- Drawer
- Legend
- Map Additional Actions
- Map Additional Options
- Map Viewer
- Plugins Drawer

### **Drawer**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/Drawer)  
![][image10]

Drawer is a panel displayed on the left side of the application next to the navbar.  
The module containing all available drawers within the application:

- Available Plugins Drawer
- Bio Entity Drawer
- Export Drawer
- Overlays Drawer
- Project Info Drawer
- Reaction Drawer
- Search Drawer
- Submaps Drawer

All drawers are defined in separate folders within the main [**/Drawer**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/Drawer) directory and utilized in the **Drawer.component.tsx** file. The currently displayed drawer is determined by the value in the Redux store's state named **drawer** available in [redux/drawer](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/drawer) folder, state includes properties such as **isOpen** and **drawerName** and other properties for the state of specific drawers.

```typescript jsx
export const Drawer = (): JSX.Element => {
 const { isOpen, drawerName } = useAppSelector(drawerSelector);

 return (
   <div
     className={twMerge(
       'absolute bottom-0 left-[88px] top-[104px] z-10 h-calc-drawer w-[432px] \-translate-x-full transform border border-divide bg-white-pearl text-font-500 transition-all duration-500',
       isOpen && 'translate-x-0',
     )}
     role={DRAWER_ROLE}
   >
    …
   </div>
 );
};
```

![][image11]

### **Legend**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/Legend)  
![][image12]

The module located in the [**/Legend**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/Legend) folder, with the main file being **Legend.component.tsx**, contains header, legend tabs, legend images and all this is located in dedicated folders. The data is taken from store redux from the **legend** property in state, available in [redux/legend](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/legend) folder.

```typescript jsx
 export const Legend: React.FC = () => {
 const { isOpen } = useAppSelector(legendSelector);
 const allPluginLegends = useAppSelector(pluginLegendsSelector);
 const isAnyPluginLegendExists = React.useMemo(
   () => Object.values(allPluginLegends).length > ZERO,
   [allPluginLegends],
 );
 return (
   <div
     className={twMerge(
       'absolute bottom-0 left-[88px] z-10 w-[calc(100%-88px)] \-translate-y-[-100%] transform border border-divide bg-white-pearl text-font-500 transition-all duration-500',
       isOpen && 'translate-y-0',
     )}
     role={LEGEND_ROLE}
   >
     <LegendHeader />
     {isAnyPluginLegendExists ? <LegendTabs /> : null}
     <LegendImages />
   </div>
 );
};
```

### **Map Additional Actions**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapAdditionalActions)  
![][image13]

The module is located in the [**/MapAdditionalActions**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapAdditionalActions) folder, with the main file being **MapAdditionalActions.component.tsx,** contains:

- ZoomInToBioEntities Button \- responsible for zooming the map to the found results if they exist. If not, the map should center itself. This updates the map position in the redux store in a state called map \- located in [redux/map](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map)
- ZoomIn Button \- responsible for zooming in the map. This updates the map zoom in the redux store in a state called map \- located in [redux/map](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map)
- ZoomOut Button \- responsible for zooming out the map. This updates the map zoom in the redux store in a state called map \- located in [redux/map](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map)

### **Map Additional Options**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapAdditionalOptions)  
![][image14]

The module is located in the [**/MapAdditionalOptions**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapAdditionalOptions) folder, with the main file being **MapAdditionalOptions.component.tsx,** contains:

- BackgroundsSelector \- allows you to change the map background. Data about available backgrounds is taken from the redux store from the backgrounds state available in [redux/backgrounds](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/backgrounds). When we select a new background, the data is updated in the redux store in the state of maps available in the [redux/map](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/map) folder

![][image15]

### **Map Viewer**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapViewer)  
![][image16]  
The module is located in the [**/MapViewer**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapViewer) folder, with the main file being **MapViewer.component.tsx.**  
The module is responsible for displaying the map using the Open Layers library. The main initialization is in the **useOlMap.ts** file. This file is a React hook that uses the utils stored in the [**/MapViewer/utils**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapViewer/utils) folder. This folder contains map configuration files for the open layers library, e.g. overlays layer, pins layers in the [**/config**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapViewer/utils/config) folder and listeners, e.g. map right click/map left click in the [**/listeners**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/MapViewer/utils/listeners) folder.  
All layers for the map are used in the file **useOlMapLayers.ts** and a hook from this file is called in **useOlMap.ts** to initialize all layers.  
Same thing happens with listeners, all listeners are used in **useOlMapListeners.ts** file and this hook also called in **useOlMap.ts** to initialize all listeners.

```typescript jsx
export const useOlMap: UseOlMap = ({ target } = {}) => {
  const mapRef = React.useRef<null | HTMLDivElement>(null);
  const { mapInstance, handleSetMapInstance } = useMapInstance();
  const view = useOlMapView({ mapInstance });
  useOlMapLayers({ mapInstance }); // layers
  useOlMapListeners({ view, mapInstance }); // listeners

  useEffect(() => {
    // checking if innerHTML is empty due to possibility of target element cloning by openlayers map instance
    if (!mapRef.current || mapRef.current.innerHTML !== '') {
      return;
    }

    const map = new Map({
      target: target || mapRef.current,
    });

    // remove zoom controls as we are using our own
    map.getControls().forEach(mapControl => {
      if (mapControl instanceof Zoom) {
        map.removeControl(mapControl);
      }
    });

    handleSetMapInstance(map);
  }, [target, handleSetMapInstance]);

  return {
    mapRef,
    mapInstance,
  };
};
```

###

### **Plugins drawer**

[URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/PluginsDrawer)  
**![][image17]**

The module is located in the [**/PluginsDrawer**](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/Map/PluginsDrawer) folder, with the main file being **PluginsDrawer.component.tsx.**  
Whether the plugins drawer is open and which plugin is the currently displayed active plugin is determined by the value in the Redux store's state named **plugins** available in [redux/plugins](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/redux/plugins).

```typescript jsx
 export const PluginsDrawer = (): JSX.Element => {
 const { isOpen } = useSelector(pluginsDrawerSelector);

 return (
   <div
     className={twMerge(
       'absolute bottom-0 right-0 top-[104px] z-20 flex h-calc-drawer w-[432px] translate-x-full transform flex-col border border-divide bg-white-pearl text-font-500 transition-all duration-500',
       isOpen && 'translate-x-0',
     )}
     role={PLUGINS_DRAWER_ROLE}
   >
     <PluginsTabs />
     <PluginsHeader />
     <PluginContent />
   </div>
 );
};
```

**![][image18]**

## **SPA**

[module URL](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/components/SPA)  
The SPA (Single Page Application) module represents the core definition of our Single Page Application. It imports and orchestrates all other modules within our application. Key initializations, such as useReduxBusQueryManager hooks, useInitializeStore, and PluginsManager configurations, are set up within this module.

- [useInitialStore](https://gitlab.lcsb.uni.lu/minerva/frontend/-/blob/development/src/utils/initialize/useInitializeStore.ts) \- The useInitialStore hook is responsible for fetching initial data and storing the retrieved data in the Redux store, taking into account the data available in the query parameters (query params) of the URL. The primary purpose of this hook is to initialize the Redux store with data fetched from external sources, such as APIs or query parameters present in the URL. By leveraging this hook, we ensure that the application starts with the necessary data preloaded into the store, enabling seamless rendering and interaction with the user interface.
- [useReduxBusQueryManager](https://gitlab.lcsb.uni.lu/minerva/frontend/-/blob/development/src/utils/query-manager/useReduxBusQueryManager.ts) \- The useReduxBusQueryManager hook is responsible for synchronizing query parameters in the URL with the data available in the Redux store. For example, when enabling an overlay, the data related to the overlay's visibility is updated in the Redux store and subsequently reflected in the URL via this hook. This hook serves the critical purpose of maintaining synchronization between the application state stored in the Redux store and the query parameters present in the URL. By utilizing this hook, we ensure that changes to application state, such as toggling overlays, are reflected in the URL, enabling users to share or bookmark specific states of the application.
- [PluginsManager](https://gitlab.lcsb.uni.lu/minerva/frontend/-/tree/development/src/services/pluginsManager) \- initializes a service responsible for managing plugins developed by other developers. This service facilitates various tasks crucial for plugin integration and functionality within our application. It provides an API for plugin registration, enabling seamless integration of third-party plugins into our application ecosystem. Additionally, the PluginsManager synchronizes data provided to plugins with the application store, ensuring consistent access to shared data across the application and its plugins. Moreover, it manages events exposed to plugins, enabling communication between plugins and the core application logic. Overall, the PluginsManager plays a pivotal role in enabling extensibility and customization within our application architecture, empowering developers to enhance and extend the functionality of our application through modular plugin integration.

```typescript jsx
 export const MinervaSPA = (): JSX.Element => {
 useInitializeStore();
 useReduxBusQueryManager();

 useEffect(() => {
   const unsubscribe = PluginsManager.init();

   return () => unsubscribe();
 }, []);

 return (
   <div className={twMerge('relative', manrope.variable)}>
     <FunctionalArea />
     <Map />
     <Modal />
     <ContextMenu />
     <CookieBanner />
   </div>
 );
};
```

[image1]: Modules1.png
[image2]: Modules2.png
[image3]: Modules3.png
[image4]: Modules4.png
[image5]: Modules5.png
[image6]: Modules6.png
[image7]: Modules7.png
[image8]: Modules8.png
[image9]: Modules9.png
[image10]: Modules10.png
[image11]: Modules11.png
[image12]: Modules12.png
[image13]: Modules13.png
[image14]: Modules14.png
[image15]: Modules15.png
[image16]: Modules16.png
[image17]: Modules17.png
[image18]: Modules18.png

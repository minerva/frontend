# Base Guide

---

### **Codebase**

- Full standardisation of linting, components and folder structure \[Code Standard\]
- Guarantee of standardisation \[CI/CD\]

### **Commit/PR**

- Full standardisation of commits/PRs according to Conventional Commits
  - [https://www.conventionalcommits.org/en/v1.0.0/](https://www.conventionalcommits.org/en/v1.0.0/)
  - [https://highlab.pl/conventional-commits/](https://highlab.pl/conventional-commits/)
- We should minimise to zero the cases of unclear commit messages and PR titles. Good nomenclature is:
  - better understanding of the changes being made, Code Review with increased contextual awareness
  - Versioning automation
- Guarantee of standardisation \[CI/CD\]

### **Branches**

- Production branch \- `main`
- Staging branch \- `develop`
- Feature branch \- `feat/MIN-123-sth-to-do`, `fix/MIN-243-sth-to-do`
- Implementation path:
  - Feature implementation \= `feat/MIN-123-sth-to-do` → `develop`
  - Release \= `develop` → `main`

```json
{
  "type": "object",
  "properties": {
    "id": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "description": {
      "type": ["string", "null"]
    },
    "synonyms": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "brandNames": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "bloodBrainBarrier": {
      "type": "string"
    },
    "references": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "link": {
            "anyOf": [
              {
                "type": "string",
                "format": "uri"
              },
              {
                "type": "null"
              }
            ]
          },
          "article": {
            "type": "object",
            "properties": {
              "title": {
                "type": "string"
              },
              "authors": {
                "type": "array",
                "items": {
                  "type": "string"
                }
              },
              "journal": {
                "type": "string"
              },
              "year": {
                "type": "number"
              },
              "link": {
                "type": "string"
              },
              "pubmedId": {
                "type": "string"
              },
              "citationCount": {
                "type": "number"
              }
            },
            "required": [
              "title",
              "authors",
              "journal",
              "year",
              "link",
              "pubmedId",
              "citationCount"
            ],
            "additionalProperties": false
          },
          "type": {
            "type": "string"
          },
          "resource": {
            "type": "string"
          },
          "id": {
            "type": "number"
          },
          "annotatorClassName": {
            "type": "string"
          }
        },
        "required": ["link", "type", "resource", "id", "annotatorClassName"],
        "additionalProperties": false
      }
    },
    "targets": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "type": "string"
          },
          "references": {
            "type": "array",
            "items": {
              "$ref": "#/properties/references/items"
            }
          },
          "targetElements": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "id": {
                  "type": "number"
                },
                "stringType": {
                  "type": "string"
                },
                "name": {
                  "type": "string"
                },
                "elementId": {
                  "type": "string"
                },
                "model": {
                  "type": "number"
                },
                "references": {
                  "type": "array",
                  "items": {
                    "$ref": "#/properties/references/items"
                  }
                },
                "z": {
                  "type": "number"
                },
                "notes": {
                  "type": "string"
                },
                "symbol": {
                  "type": ["string", "null"]
                },
                "homodimer": {
                  "type": "number"
                },
                "nameX": {
                  "type": "number"
                },
                "nameY": {
                  "type": "number"
                },
                "nameWidth": {
                  "type": "number"
                },
                "nameHeight": {
                  "type": "number"
                },
                "nameVerticalAlign": {
                  "type": "string"
                },
                "nameHorizontalAlign": {
                  "type": "string"
                },
                "width": {
                  "type": "number"
                },
                "height": {
                  "type": "number"
                },
                "visibilityLevel": {
                  "type": "string"
                },
                "transparencyLevel": {
                  "type": "string"
                },
                "synonyms": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "formerSymbols": {
                  "type": "array",
                  "items": {
                    "type": "string"
                  }
                },
                "fullName": {
                  "type": ["string", "null"]
                },
                "compartmentName": {
                  "type": ["string", "null"]
                },
                "abbreviation": {
                  "type": ["string", "null"]
                },
                "formula": {
                  "type": ["string", "null"]
                },
                "glyph": {
                  "anyOf": [
                    {
                      "type": "object",
                      "properties": {
                        "file": {
                          "type": "number"
                        },
                        "id": {
                          "type": "number"
                        }
                      },
                      "required": ["file", "id"],
                      "additionalProperties": false
                    },
                    {
                      "type": "null"
                    }
                  ]
                },
                "activity": {
                  "type": "boolean"
                },
                "structuralState": {
                  "anyOf": [
                    {
                      "type": "object",
                      "properties": {
                        "value": {
                          "type": "string"
                        },
                        "position": {
                          "type": "object",
                          "properties": {
                            "x": {
                              "type": "number"
                            },
                            "y": {
                              "type": "number"
                            }
                          },
                          "required": ["x", "y"],
                          "additionalProperties": false
                        },
                        "z": {
                          "type": "number"
                        },
                        "width": {
                          "type": "number"
                        },
                        "height": {
                          "type": "number"
                        },
                        "fontSize": {
                          "type": "number"
                        },
                        "size": {
                          "type": "number"
                        },
                        "center": {
                          "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/position"
                        },
                        "borderColor": {
                          "type": "object",
                          "properties": {
                            "alpha": {
                              "type": "number"
                            },
                            "rgb": {
                              "type": "number"
                            }
                          },
                          "required": ["alpha", "rgb"],
                          "additionalProperties": false
                        },
                        "elementId": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "value",
                        "position",
                        "z",
                        "width",
                        "height",
                        "fontSize",
                        "size",
                        "center",
                        "borderColor",
                        "elementId"
                      ],
                      "additionalProperties": false
                    },
                    {
                      "type": "null"
                    }
                  ]
                },
                "hypothetical": {
                  "type": ["boolean", "null"]
                },
                "boundaryCondition": {
                  "type": "boolean"
                },
                "constant": {
                  "type": "boolean"
                },
                "initialAmount": {
                  "type": ["number", "null"]
                },
                "initialConcentration": {
                  "type": ["number", "null"]
                },
                "charge": {
                  "type": ["number", "null"]
                },
                "substanceUnits": {
                  "type": ["string", "null"]
                },
                "onlySubstanceUnits": {
                  "type": "boolean"
                },
                "modificationResidues": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "number"
                      },
                      "idModificationResidue": {
                        "type": "string"
                      },
                      "name": {
                        "type": "string"
                      },
                      "position": {
                        "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/position"
                      },
                      "z": {
                        "type": "number"
                      },
                      "borderColor": {
                        "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/borderColor"
                      },
                      "state": {
                        "anyOf": [
                          {
                            "type": ["string", "number"]
                          },
                          {
                            "type": "null"
                          }
                        ]
                      },
                      "size": {
                        "type": "number"
                      },
                      "elementId": {
                        "type": "string"
                      }
                    },
                    "required": [
                      "id",
                      "idModificationResidue",
                      "name",
                      "z",
                      "borderColor",
                      "size",
                      "elementId"
                    ],
                    "additionalProperties": false
                  }
                },
                "complex": {
                  "type": ["number", "null"]
                },
                "compartment": {
                  "type": ["number", "null"]
                },
                "submodel": {
                  "anyOf": [
                    {
                      "type": "object",
                      "properties": {
                        "mapId": {
                          "type": "number"
                        },
                        "type": {
                          "type": "string"
                        }
                      },
                      "required": ["mapId", "type"],
                      "additionalProperties": false
                    },
                    {
                      "type": "null"
                    }
                  ]
                },
                "x": {
                  "type": "number"
                },
                "y": {
                  "type": "number"
                },
                "lineWidth": {
                  "type": "number"
                },
                "fontColor": {
                  "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/borderColor"
                },
                "fontSize": {
                  "type": "number"
                },
                "fillColor": {
                  "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/borderColor"
                },
                "borderColor": {
                  "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/borderColor"
                },
                "smiles": {
                  "anyOf": [
                    {
                      "anyOf": [
                        {
                          "not": {}
                        },
                        {
                          "type": "string"
                        }
                      ]
                    },
                    {
                      "type": "null"
                    }
                  ]
                },
                "inChI": {
                  "type": ["string", "null"]
                },
                "inChIKey": {
                  "type": ["string", "null"]
                },
                "thickness": {
                  "type": "number"
                },
                "outerWidth": {
                  "type": "number"
                },
                "innerWidth": {
                  "type": "number"
                },
                "idReaction": {
                  "type": "string"
                },
                "reversible": {
                  "type": "boolean"
                },
                "mechanicalConfidenceScore": {
                  "type": "boolean"
                },
                "lowerBound": {
                  "type": "boolean"
                },
                "upperBound": {
                  "type": "boolean"
                },
                "subsystem": {
                  "type": "string"
                },
                "geneProteinReaction": {
                  "type": "string"
                },
                "kinetics": {
                  "type": "null"
                },
                "products": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "aliasId": {
                        "type": "number"
                      },
                      "stoichiometry": {
                        "type": ["number", "null"]
                      },
                      "type": {
                        "type": "string"
                      }
                    },
                    "required": ["aliasId", "stoichiometry"],
                    "additionalProperties": false
                  }
                },
                "reactants": {
                  "type": "array",
                  "items": {
                    "$ref": "#/properties/targets/items/properties/targetElements/items/properties/products/items"
                  }
                },
                "modifiers": {
                  "type": "array",
                  "items": {
                    "$ref": "#/properties/targets/items/properties/targetElements/items/properties/products/items"
                  }
                },
                "processCoordinates": {
                  "type": "null"
                },
                "line": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "number"
                    },
                    "width": {
                      "type": "number"
                    },
                    "color": {
                      "$ref": "#/properties/targets/items/properties/targetElements/items/properties/structuralState/anyOf/0/properties/borderColor"
                    },
                    "z": {
                      "type": "number"
                    },
                    "segments": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "x1": {
                            "type": "number"
                          },
                          "y1": {
                            "type": "number"
                          },
                          "x2": {
                            "type": "number"
                          },
                          "y2": {
                            "type": "number"
                          }
                        },
                        "required": ["x1", "y1", "x2", "y2"],
                        "additionalProperties": false
                      }
                    },
                    "startArrow": {
                      "type": "object",
                      "properties": {
                        "arrowType": {
                          "type": "string"
                        },
                        "angle": {
                          "type": "number"
                        },
                        "lineType": {
                          "type": "string"
                        },
                        "length": {
                          "type": "number"
                        }
                      },
                      "required": ["arrowType", "angle", "lineType", "length"],
                      "additionalProperties": false
                    },
                    "endArrow": {
                      "$ref": "#/properties/targets/items/properties/targetElements/items/properties/line/properties/startArrow"
                    },
                    "lineType": {
                      "type": "string"
                    }
                  },
                  "required": [
                    "id",
                    "width",
                    "color",
                    "z",
                    "segments",
                    "startArrow",
                    "endArrow",
                    "lineType"
                  ],
                  "additionalProperties": false
                },
                "operators": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "properties": {
                      "id": {
                        "type": "number"
                      },
                      "line": {
                        "$ref": "#/properties/targets/items/properties/targetElements/items/properties/line"
                      },
                      "inputs": {
                        "type": "array",
                        "items": {
                          "type": "object",
                          "properties": {
                            "id": {
                              "type": "number"
                            }
                          },
                          "required": ["id"],
                          "additionalProperties": false
                        }
                      },
                      "outputs": {
                        "not": {}
                      },
                      "operatorText": {
                        "type": "string"
                      },
                      "reactantOperator": {
                        "type": "boolean"
                      },
                      "productOperator": {
                        "type": "boolean"
                      },
                      "modifierOperator": {
                        "type": "boolean"
                      }
                    },
                    "required": [
                      "id",
                      "line",
                      "inputs",
                      "operatorText",
                      "reactantOperator",
                      "productOperator",
                      "modifierOperator"
                    ],
                    "additionalProperties": false
                  }
                }
              },
              "required": [
                "id",
                "stringType",
                "name",
                "elementId",
                "model",
                "references",
                "z",
                "notes",
                "symbol",
                "nameX",
                "nameY",
                "nameWidth",
                "nameHeight",
                "nameVerticalAlign",
                "nameHorizontalAlign",
                "width",
                "height",
                "visibilityLevel",
                "transparencyLevel",
                "synonyms",
                "formerSymbols",
                "fullName",
                "abbreviation",
                "formula",
                "glyph",
                "compartment",
                "submodel",
                "x",
                "y",
                "fontColor",
                "fontSize",
                "fillColor",
                "borderColor"
              ],
              "additionalProperties": false
            }
          },
          "targetParticipants": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "link": {
                  "type": "string"
                },
                "type": {
                  "type": "string"
                },
                "resource": {
                  "type": "string"
                },
                "id": {
                  "type": "number"
                },
                "annotatorClassName": {
                  "type": "string"
                }
              },
              "required": ["link", "type", "resource", "id", "annotatorClassName"],
              "additionalProperties": false
            }
          }
        },
        "required": ["name", "references", "targetElements", "targetParticipants"],
        "additionalProperties": false
      }
    }
  },
  "required": [
    "id",
    "name",
    "description",
    "synonyms",
    "brandNames",
    "bloodBrainBarrier",
    "references",
    "targets"
  ],
  "additionalProperties": false,
  "$schema": "http://json-schema.org/draft-07/schema#"
}
```

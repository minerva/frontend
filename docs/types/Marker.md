```ts
interface MarkerBase {
  type: 'pin' | 'surface';
  id: string;
  color: string;
  opacity: number;
  x: number;
  y: number;
  number?: number;
  modelId?: number;
}
```

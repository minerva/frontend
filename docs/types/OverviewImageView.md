```json
{
  "type": "object",
  "properties": {
    "idObject": {
      "type": "number"
    },
    "filename": {
      "type": "string"
    },
    "width": {
      "type": "number"
    },
    "height": {
      "type": "number"
    },
    "links": {
      "type": "array",
      "items": {
        "anyOf": [
          {
            "type": "object",
            "properties": {
              "idObject": {
                "type": "number"
              },
              "polygon": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "x": {
                      "type": "number"
                    },
                    "y": {
                      "type": "number"
                    }
                  },
                  "required": ["x", "y"],
                  "additionalProperties": false
                }
              },
              "imageLinkId": {
                "type": "number"
              },
              "type": {
                "type": "string"
              }
            },
            "required": ["idObject", "polygon", "imageLinkId", "type"],
            "additionalProperties": false
          },
          {
            "type": "object",
            "properties": {
              "idObject": {
                "type": "number"
              },
              "polygon": {
                "type": "array",
                "items": {
                  "$ref": "#/definitions/overviewImageView/properties/links/items/anyOf/0/properties/polygon/items"
                }
              },
              "zoomLevel": {
                "type": "number"
              },
              "modelPoint": {
                "$ref": "#/definitions/overviewImageView/properties/links/items/anyOf/0/properties/polygon/items"
              },
              "modelLinkId": {
                "type": "number"
              },
              "type": {
                "type": "string"
              }
            },
            "required": ["idObject", "polygon", "zoomLevel", "modelPoint", "modelLinkId", "type"],
            "additionalProperties": false
          }
        ]
      }
    }
  },
  "required": ["idObject", "filename", "width", "height", "links"],
  "additionalProperties": false
}
```

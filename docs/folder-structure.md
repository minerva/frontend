# Folder structure

├── [public]
├── [pages]
├── [docs]
├── [cypress] <- structure in cypress docs
├── tailwind.config.js
├── [src]
├── [assets] <- global assets
├── [vectors]
├── [branding]
└── logo.svg
├── [icons]
├── icon-a.svg <- kebab-case
└── icon-b.svg
└── [images]
├── [banners]
└── main-banner.png
├── [components] <- domain components
├── [DomainName]
├── [utils]
└── domainUtil.ts
├── [Component]
└── [SthComponent]
└── [OtherDomainName]
├── [OtherComponent]
└── [SomeComponent]
├── [shared] <- not-domain components (shared between domains)
├── [Button]
└── [Select]
├── [constants] <- global constants
├── common.ts
├── api.ts
└── map.ts
├── [services] <- services clients
├── [samplecms]
└── ...
├── [api]
├── ... <- depends on service client
└── index.ts
├── [types] <- global types
├── analytics.ts
├── error.ts
├── fonts.ts
└── common.ts
└── [utils] <- global helpers
├── [analytics]
├── someHelper.ts
└── index.ts
├── [bugsnag]
└── index.ts
├── [number]
├── multiplyNumberBy.ts
└── divideNumberBy.ts
└── [array]
└── doSth.ts

# Component structure

├── [DisplayArea]
├── [Map]
├── [Pin] <- subcomponent
├── Pin.component.test.ts
├── Pin.test.tsx
└── index.ts
├── [utils] <- single-component utils
├── getSomeData.ts
└── getSomeData.test.ts
├── [assets] <- single-component assets
├── some-asset.png
└── other-asset.svg
├── Map.component.test.tsx
├── Map.test.tsx
├── Map.types.ts
├── Map.constants.ts
├── Map.style.ts <- depends on design framework
└── index.ts
├── [OtherComponent]
└── ...

### Folder Naming Convention

- catalog: kebab-case
- asset: kebab-case
- util/hook: camelCase
- components: PascalCase
- domena: PascalCase

### Domena

- grupa komponentów skupionych wokół jakiejś głównej domeny: `DisplayArea` , `FunctionalArea` , `Plugin` , `Layout`
- domena sama w sobie nie eksportuje żadnego komponentu poza swoją domenę, jest tylko workiem na ich przechowywanie
- zastosowany podział wynika z podejścia strukturalnego (struktura katalogów odwzorowuje strukturę komponentów)

### Komponent/Subcomponent

- pojedynczy komponent z ew. subcomponentami wewnątrz siebie
- odpowiednie pliki/katalogi tworzymy tylko jeśli jest taka potrzeba
- **nazewnictwo:** w nazewnictwie powtórzona jest bazowa nazwa komponentu z uwagi na czytelność importów i Developer Experience - widząc w edytorze “style.ts”, “types.ts” z wielu plików ich nie odróżnimy tak łatwo
- **single-component single-responsibility:** w komponencie możliwe są utile i assety spełniające zasadę single-component single-responsibility - jeśli potrzebujemy złożonego utila w komponencie (ale **tylko** w nim), to powinien trafić do odpowiedniego folderu
- przykład:
  - index.ts - eksport komponentu + czyste eksportowanie ew. typów/utilów
    (`@components/DisplayArea/Map/Map.tsx` → `@components/DisplayArea/Map`) - `export { default } from “./Map.view.tsx”`
  - Map.**component**.tsx - widok komponentu
  - Map.**component.test**.tsx - unit test komponentu
  - Map.**styles**.scss
  - Map.**types**.ts - lokalne typy // w przypadku kiedy typ jest używany poza Map.component.tsx
  - Map.**constants**.ts - lokalne constants
  - **utils/…** - subkatalog komponentu do lokalnych utilów, hooków
  - **assets/…** - subkatalog komponentu do lokalnych assetów

### Shared komponent

- komponenty reużywane w wielu domenach
- zasady komponentów wewnątrz jw.

// const root = 'https://minerva-dev.lcsb.uni.lu';
// const root = 'https://scimap.lcsb.uni.lu';
// const root = 'https://imsavar.elixir-luxembourg.org';
// const root = 'https://pdmap.uni.lu';

const root = 'https://lux1.atcomp.pl';
// const root = 'http://localhost:8080';
window.config = {
  BASE_API_URL: `${root}/minerva/api`,
  BASE_NEW_API_URL: `${root}/minerva/new_api/`,
  BASE_MAP_IMAGES_URL: `${root}`,
  DEFAULT_PROJECT_ID: 'sample',
  ADMIN_PANEL_URL: `${root}/minerva/admin.xhtml`,
};

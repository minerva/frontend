/** @type {import("next").NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  basePath: process.env.APP_PREFIX ? process.env.APP_PREFIX + '/index.html' : '',
  assetPrefix: process.env.APP_PREFIX ? process.env.APP_PREFIX : '',
  productionBrowserSourceMaps: true,
  output: 'export',
  images: {
    unoptimized: true,
  },
  env: {
    ZOD_SEED: process.env.ZOD_SEED || 123,
  },
};

module.exports = nextConfig;

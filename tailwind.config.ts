import type { Config } from 'tailwindcss';

const config: Config = {
  content: ['./src/**/*.ts', './src/**/*.tsx'],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#ebf4ff',
          200: '#d4e5fa',
          500: '#106ad7',
          600: '#0055bb',
          700: '#004393',
        },
        greyscale: {
          500: '#eeeeee',
          600: '#cacaca',
          700: '#a5a5a5',
          800: '#858585',
        },
        font: {
          400: '#6a6977',
          500: '#070130',
        },
        cinnabar: '#ec4d2c',
        'med-sea-green': '#3ab65d',
        cultured: '#f7f7f8',
        lotion: '#fafafa',
        'white-pearl': '#ffffff',
        divide: '#e1e0e6',
        orange: '#f48c40',
        purple: '#6400e3',
        pink: '#f1009f',
        'cetacean-blue': '#070130',
        'hyperlink-blue': '#0048ff',
      },
      height: {
        'calc-drawer': 'calc(100% - 104px)',
      },
      boxShadow: {
        primary: '4px 8px 32px 0px rgba(0, 0, 0, 0.12)',
        tableBorderDivide: '0 0 0 1px #e1e0e6',
      },
      dropShadow: {
        primary: '0px 4px 24px rgba(0, 0, 0, 0.08)',
      },
    },
    fontFamily: {
      manrope: ['var(--font-manrope)'],
    },
  },
  plugins: [],
};
export default config;

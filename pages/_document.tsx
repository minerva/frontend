import { Head, Html, Main, NextScript } from 'next/document';
import Script from 'next/script';

const Document = (): React.ReactNode => (
  <Html>
    <Head>
      <link rel="shortcut icon" href="./favicon.ico" />
    </Head>
    <body>
      <Main />
      <div id="modal-root" />
      <NextScript />
      <Script src="./config.js" strategy="beforeInteractive" />
    </body>
  </Html>
);

export default Document;

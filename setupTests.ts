import '@testing-library/jest-dom';
import 'jest-canvas-mock';

// used by openlayers module
global.ResizeObserver = jest.fn().mockImplementation(() => ({
  observe: jest.fn(),
  unobserve: jest.fn(),
  disconnect: jest.fn(),
}));

jest.mock('next/router', () => require('next-router-mock'));

global.TextEncoder = jest.fn().mockImplementation(() => ({
  encode: jest.fn(),
}));

const localStorageMock = (() => {
  let store: {
    [key: PropertyKey]: string;
  } = {};

  return {
    getItem: jest.fn((key: PropertyKey) => store[key] || null),
    setItem: jest.fn((key: PropertyKey, value: any) => {
      store[key] = value.toString();
    }),
    removeItem: jest.fn((key: PropertyKey) => {
      delete store[key];
    }),
    clear: jest.fn(() => {
      store = {};
    }),
  };
})();

Object.defineProperty(global, 'localStorage', {
  value: localStorageMock,
});

Object.defineProperty(window, 'config', {
  value: {
    BASE_API_URL: 'https://lux1.atcomp.pl/minerva/api',
    BASE_NEW_API_URL: 'https://lux1.atcomp.pl/minerva/new_api/',
    BASE_MAP_IMAGES_URL: 'https://lux1.atcomp.pl/',
    DEFAULT_PROJECT_ID: 'pdmap_appu_test',
    ADMIN_PANEL_URL: 'https://lux1.atcomp.pl/minerva/admin.xhtml',
  },
});

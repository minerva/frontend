import { store } from '@/redux/store';
import { openedDrawerSelector } from '@/redux/drawer/drawer.selectors';

export const getOpenedPanel = (): string | undefined => {
  const { getState } = store;
  const drawerName = openedDrawerSelector(getState());

  return drawerName;
};

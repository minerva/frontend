import { store } from '@/redux/store';
import { DrawerName } from '@/types/drawerName';
import { openDrawer } from '@/redux/drawer/drawer.slice';

export const openPanel = (panelName: DrawerName): void => {
  const { dispatch } = store;
  dispatch(openDrawer(panelName));
};

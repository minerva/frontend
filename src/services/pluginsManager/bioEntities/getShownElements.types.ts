import { ModelElement, Marker } from '@/types/models';

export interface GetShownElementsPluginMethodResult {
  content: ModelElement[];
  drugs: ModelElement[];
  chemicals: ModelElement[];
  markers: Marker[];
}

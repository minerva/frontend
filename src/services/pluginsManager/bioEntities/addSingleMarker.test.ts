import { addMarkerToMarkersData } from '@/redux/markers/markers.slice';
import { MarkerWithOptionalId } from '@/redux/markers/markers.types';
import { Marker, MarkerLine, MarkerPin, MarkerSurface } from '@/types/models';
import { ZodError } from 'zod';
import { store } from '../../../redux/store';
import { addSingleMarker } from './addSingleMarker';

jest.mock('../../../redux/store');

const VALID_MARKERS: MarkerWithOptionalId[] = [
  {
    id: 'id-123',
    type: 'pin',
    color: '#F48C41',
    opacity: 0.68,
    x: 1000,
    y: 200,
    number: 75,
    modelId: 52,
  } as MarkerPin,
  {
    type: 'surface',
    color: '#106AD7',
    opacity: 0.24,
    x: 442,
    y: 442,
    width: 600,
    height: 500,
    number: 37,
  } as MarkerSurface,
  {
    type: 'line',
    color: '#106AD7',
    opacity: 0.7312,
    modelId: 52,
    start: {
      x: 1200,
      y: 432,
    },
    end: {
      x: 332,
      y: 112,
    },
  } as MarkerLine,
];

export const INVALID_MARKERS: unknown[] = [
  {
    id: 'id-123',
    type: 'pin',
    x: 1000,
    y: 200,
    number: 75,
    modelId: 52,
  },
  {
    type: 'surface',
    color: '#106AD7',
    opacity: 0.24,
    x: 442,
    number: 37,
  },
  {
    type: 'line',
    color: '#106AD7',
    opacity: 0.7312,
    modelId: 52,
    start: {
      x: 1200,
      y: 432,
    },
  },
  {
    id: 123345,
    color: '#106AD7',
    opacity: 0.7312,
    modelId: 52,
    start: {
      x: 1200,
      y: 432,
    },
  },
];

describe('addSingleMarker - plugin method', () => {
  const dispatchSpy = jest.spyOn(store, 'dispatch');

  it.each(VALID_MARKERS)(
    'should dispatch addMarkerToMarkersData and return valid marker with id',
    marker => {
      const markerResults = addSingleMarker(marker);

      expect(dispatchSpy).toHaveBeenCalledWith(
        addMarkerToMarkersData({ ...marker, id: markerResults.id } as Marker),
      );
    },
  );

  it.each(INVALID_MARKERS)('should throw error', marker => {
    expect(() => addSingleMarker(marker as MarkerWithOptionalId)).toThrow(ZodError);
  });
});

import { removeMarkerFromMarkersData } from '@/redux/markers/markers.slice';
import { store } from '@/redux/store';

export const removeSingleMarker = (id: string): void => {
  const { dispatch } = store;
  dispatch(removeMarkerFromMarkersData(id));
};

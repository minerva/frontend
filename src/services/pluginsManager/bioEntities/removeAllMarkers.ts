import { setMarkersData } from '@/redux/markers/markers.slice';
import { store } from '@/redux/store';

export const removeAllMarkers = (): void => {
  const { dispatch } = store;
  dispatch(setMarkersData([]));
};

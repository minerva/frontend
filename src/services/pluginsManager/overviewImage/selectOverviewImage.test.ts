import { OVERVIEW_IMAGE_ERRORS } from '@/constants/errors';
import { setOverviewImageId } from '@/redux/modal/modal.slice';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState, store } from '@/redux/store';
import { selectOverviewImage } from './selectOverviewImage';

jest.mock('../../../redux/store');

describe('selectOverviewImage - plugin method', () => {
  const dispatchSpy = jest.spyOn(store, 'dispatch');
  const getStateSpy = jest.spyOn(store, 'getState');

  describe('when image id is valid', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            project: {
              ...INITIAL_STORE_STATE_MOCK.project,
              data: {
                ...INITIAL_STORE_STATE_MOCK.project.data,
                overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
              },
            },
          }) as RootState,
      );
    });

    it('should dispatch action set overview image', () => {
      selectOverviewImage(PROJECT_OVERVIEW_IMAGE_MOCK.id);

      expect(dispatchSpy).toHaveBeenCalledWith(setOverviewImageId(PROJECT_OVERVIEW_IMAGE_MOCK.id));
    });
  });

  describe('when image id is NOT valid', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            project: {
              ...INITIAL_STORE_STATE_MOCK.project,
              data: {
                ...INITIAL_STORE_STATE_MOCK.project.data,
                overviewImageViews: [],
              },
            },
          }) as RootState,
      );
    });

    it('should throw error', () => {
      expect(() => selectOverviewImage(PROJECT_OVERVIEW_IMAGE_MOCK.id)).toThrow(
        OVERVIEW_IMAGE_ERRORS.IMAGE_ID_IS_INVALID,
      );
    });
  });
});

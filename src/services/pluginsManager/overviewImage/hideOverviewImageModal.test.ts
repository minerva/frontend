import { closeModal } from '@/redux/modal/modal.slice';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState, store } from '@/redux/store';
import { hideOverviewImageModal } from './hideOverviewImageModal';

jest.mock('../../../redux/store');

describe('hideOverviewImageModal - util', () => {
  const getStateSpy = jest.spyOn(store, 'getState');

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('when opened modal is overview image', () => {
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            modal: {
              ...INITIAL_STORE_STATE_MOCK.modal,
              modalName: 'overview-images',
            },
          }) as RootState,
      );
    });

    it('should close modal', () => {
      hideOverviewImageModal();

      expect(dispatchSpy).toHaveBeenCalledWith(closeModal());
    });
  });

  describe('when opened modal is NOT overview image', () => {
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            modal: {
              ...INITIAL_STORE_STATE_MOCK.modal,
              modalName: 'login',
            },
          }) as RootState,
      );
    });

    it('should not close modal', () => {
      hideOverviewImageModal();

      expect(dispatchSpy).not.toHaveBeenCalledWith(closeModal());
    });
  });
});

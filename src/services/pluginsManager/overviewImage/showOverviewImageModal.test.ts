import { OVERVIEW_IMAGE_ERRORS } from '@/constants/errors';
import { openOverviewImagesModalById } from '@/redux/modal/modal.slice';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState, store } from '@/redux/store';
import { showOverviewImageModal } from './showOverviewImageModal';

jest.mock('../../../redux/store');

describe('showOverviewImageModal - plugin method', () => {
  const dispatchSpy = jest.spyOn(store, 'dispatch');
  const getStateSpy = jest.spyOn(store, 'getState');

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('when image id is not provided', () => {
    const defaultImageId = 23332;

    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            project: {
              ...INITIAL_STORE_STATE_MOCK.project,
              data: {
                ...INITIAL_STORE_STATE_MOCK.project.data,
                overviewImageViews: [
                  PROJECT_OVERVIEW_IMAGE_MOCK,
                  {
                    ...PROJECT_OVERVIEW_IMAGE_MOCK,
                    id: defaultImageId,
                  },
                ],
                topOverviewImage: {
                  ...PROJECT_OVERVIEW_IMAGE_MOCK,
                  id: defaultImageId,
                },
              },
            },
          }) as RootState,
      );
    });

    it('should dispatch action set overview image with defaultImageId', () => {
      showOverviewImageModal();

      expect(dispatchSpy).toHaveBeenCalledWith(openOverviewImagesModalById(defaultImageId));
    });
  });

  describe('when image id is valid', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            project: {
              ...INITIAL_STORE_STATE_MOCK.project,
              data: {
                ...INITIAL_STORE_STATE_MOCK.project.data,
                overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
                topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
              },
            },
          }) as RootState,
      );
    });

    it('should dispatch action set overview image', () => {
      showOverviewImageModal(PROJECT_OVERVIEW_IMAGE_MOCK.id);

      expect(dispatchSpy).toHaveBeenCalledWith(
        openOverviewImagesModalById(PROJECT_OVERVIEW_IMAGE_MOCK.id),
      );
    });
  });

  describe('when image id is NOT valid', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            ...INITIAL_STORE_STATE_MOCK,
            project: {
              ...INITIAL_STORE_STATE_MOCK.project,
              data: {
                ...INITIAL_STORE_STATE_MOCK.project.data,
                overviewImageViews: [],
                topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
              },
            },
          }) as RootState,
      );
    });

    it('should throw error', () => {
      expect(() => showOverviewImageModal(PROJECT_OVERVIEW_IMAGE_MOCK.id)).toThrow(
        OVERVIEW_IMAGE_ERRORS.IMAGE_ID_IS_INVALID,
      );
    });
  });
});

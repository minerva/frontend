import { Chemical, Drug, ElementSearchResult, MapOverlay, ModelElement } from '@/types/models';
import { SearchModelElementDataState } from '@/redux/modelElements/modelElements.types';
import { dispatchEvent } from './pluginsEventBus';

export type BackgroundEvents = 'onBackgroundOverlayChange';
export type OverlayEvents =
  | 'onAddDataOverlay'
  | 'onRemoveDataOverlay'
  | 'onShowOverlay'
  | 'onHideOverlay'
  | 'onSurfaceClick';
export type SubmapEvents =
  | 'onSubmapOpen'
  | 'onSubmapClose'
  | 'onZoomChanged'
  | 'onCenterChanged'
  | 'onBioEntityClick'
  | 'onPinIconClick';
export type SearchEvents = 'onSearch';

export type PluginEvents = 'onPluginUnload';

export type Events = OverlayEvents | BackgroundEvents | SubmapEvents | SearchEvents | PluginEvents;

export type ZoomChanged = {
  zoom: number;
  modelId: number;
};

export type CenteredCoordinates = {
  modelId: number;
  x: number;
  y: number;
};

export type ClickedBioEntity = {
  id: number;
  type: string;
  modelId: number;
};

export type ClickedPinIcon = {
  id: number | string;
};

export type ClickedSurfaceOverlay = {
  id: number | string;
};

export type SearchDataReaction = {
  type: 'reaction';
  searchValues: string[] | ElementSearchResult[];
  results: SearchModelElementDataState[][];
};

type SearchDataModelElementResults = {
  perfect: boolean;
  bioEntity: ModelElement;
};

export type SearchDataModelElement = {
  type: 'bioEntity';
  searchValues: string[] | ElementSearchResult[];
  results: SearchDataModelElementResults[][];
};

export type SearchDataDrugs = {
  type: 'drugs';
  searchValues: string[];
  results: Drug[][];
};

export type SearchDataChemicals = {
  type: 'chemicals';
  searchValues: string[];
  results: Chemical[][];
};

export type PluginUnloaded = {
  hash: string;
};

export type SearchData =
  | SearchDataModelElement
  | SearchDataDrugs
  | SearchDataChemicals
  | SearchDataReaction;

export type EventsData =
  | number
  | MapOverlay
  | ZoomChanged
  | CenteredCoordinates
  | ClickedBioEntity
  | ClickedPinIcon
  | ClickedSurfaceOverlay
  | SearchData
  | PluginUnloaded;

export type UnknownCallback = (data: unknown) => void;

export type PluginsEventBusType = {
  events: {
    hash: string;
    pluginName: string;
    type: Events;
    callback: UnknownCallback;
  }[];
  addListener: (hash: string, pluginName: string, type: Events, callback: UnknownCallback) => void;
  addLocalListener: <T>(type: Events, callback: (data: T) => void) => void;
  removeListener: (hash: string, type: Events, callback: unknown) => void;
  removeLocalListener: <T>(type: Events, callback: T) => void;
  removeAllListeners: (hash: string) => void;
  dispatchEvent: typeof dispatchEvent;
};

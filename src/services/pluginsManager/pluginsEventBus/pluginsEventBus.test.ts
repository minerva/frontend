/* eslint-disable no-magic-numbers */
import { RootState, store } from '@/redux/store';

import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import { FIRST_ARRAY_ELEMENT, SECOND_ARRAY_ELEMENT, THIRD_ARRAY_ELEMENT } from '@/constants/common';
import {
  PLUGINS_INITIAL_STATE_LIST_MOCK,
  PLUGINS_INITIAL_STATE_MOCK,
} from '@/redux/plugins/plugins.mock';
import { showToast } from '@/utils/showToast';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { PluginsEventBus } from './pluginsEventBus';
import { ERROR_INVALID_EVENT_TYPE, ERROR_PLUGIN_CRASH } from '../errorMessages';

const plugin = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];
const secondPlugin = PLUGINS_MOCK[SECOND_ARRAY_ELEMENT];
const thirdPlugin = PLUGINS_MOCK[THIRD_ARRAY_ELEMENT];

jest.mock('../../../utils/showToast');

describe('PluginsEventBus', () => {
  beforeEach(() => {
    PluginsEventBus.events = [];
  });
  it('should store event listener', () => {
    const callback = jest.fn();
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);

    expect(PluginsEventBus.events).toEqual([
      {
        hash: plugin.hash,
        type: 'onAddDataOverlay',
        pluginName: plugin.name,
        callback,
      },
    ]);
  });

  it('should dispatch event correctly', () => {
    const callback = jest.fn();
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);
    PluginsEventBus.dispatchEvent('onAddDataOverlay', overlayFixture);

    expect(callback).toHaveBeenCalled();
  });

  it('should throw error if event type is incorrect', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect(() => PluginsEventBus.dispatchEvent('onData' as any, overlayFixture)).toThrow(
      ERROR_INVALID_EVENT_TYPE('onData'),
    );
  });
  it('should remove listener only for specific plugin, event type, and callback', () => {
    const callback = (): void => {};

    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onBioEntityClick', callback);
    PluginsEventBus.addListener(secondPlugin.hash, secondPlugin.name, 'onBioEntityClick', callback);

    expect(PluginsEventBus.events).toHaveLength(3);

    PluginsEventBus.removeListener(plugin.hash, 'onAddDataOverlay', callback);
    expect(PluginsEventBus.events).toHaveLength(2);
    expect(PluginsEventBus.events).toEqual([
      {
        callback,
        hash: plugin.hash,
        pluginName: plugin.name,
        type: 'onBioEntityClick',
      },
      {
        callback,
        hash: secondPlugin.hash,
        pluginName: secondPlugin.name,
        type: 'onBioEntityClick',
      },
    ]);
  });
  it('should throw if listener is not defined by plugin', () => {
    const callback = (): void => {};

    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onBioEntityClick', callback);
    PluginsEventBus.addListener(secondPlugin.hash, secondPlugin.name, 'onBioEntityClick', callback);

    expect(PluginsEventBus.events).toHaveLength(3);

    expect(() => PluginsEventBus.removeListener(plugin.hash, 'onHideOverlay', callback)).toThrow(
      "Listener doesn't exist",
    );
    expect(PluginsEventBus.events).toHaveLength(3);
  });
  it('should not remove listener when event with the same event type is defined by the same plugin but with different callback', () => {
    const callback = (): void => {};
    const secondCallback = (): void => {};

    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', secondCallback);

    PluginsEventBus.removeListener(plugin.hash, 'onAddDataOverlay', callback);

    expect(PluginsEventBus.events).toHaveLength(1);
    expect(PluginsEventBus.events).toEqual([
      {
        callback: secondCallback,
        hash: plugin.hash,
        pluginName: plugin.name,
        type: 'onAddDataOverlay',
      },
    ]);
  });
  it('should remove all listeners defined by specific plugin', () => {
    const callback = (): void => {};
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onAddDataOverlay', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onBackgroundOverlayChange', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onSubmapOpen', callback);
    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onHideOverlay', callback);
    PluginsEventBus.addListener(secondPlugin.hash, secondPlugin.name, 'onSubmapOpen', callback);
    PluginsEventBus.addListener(thirdPlugin.hash, thirdPlugin.name, 'onSearch', callback);

    PluginsEventBus.removeAllListeners(plugin.hash);

    expect(PluginsEventBus.events).toHaveLength(2);
    expect(PluginsEventBus.events).toEqual([
      {
        callback,
        hash: secondPlugin.hash,
        type: 'onSubmapOpen',
        pluginName: secondPlugin.name,
      },
      {
        callback,
        hash: thirdPlugin.hash,
        type: 'onSearch',
        pluginName: thirdPlugin.name,
      },
    ]);
  });
  it('should show toast when event callback provided by plugin throws error', () => {
    const getStateSpy = jest.spyOn(store, 'getState');
    getStateSpy.mockImplementation(
      () =>
        ({
          plugins: {
            ...PLUGINS_INITIAL_STATE_MOCK,
            activePlugins: {
              data: {
                [plugin.hash]: plugin,
              },
              pluginsId: [plugin.hash],
            },
            list: PLUGINS_INITIAL_STATE_LIST_MOCK,
          },
        }) as RootState,
    );

    const callbackMock = jest.fn(() => {
      throw new Error('Invalid callback');
    });

    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onPluginUnload', callbackMock);

    PluginsEventBus.dispatchEvent('onPluginUnload', {
      hash: plugin.hash,
    });

    expect(callbackMock).toHaveBeenCalled();
    expect(callbackMock).toThrow('Invalid callback');
    expect(showToast).toHaveBeenCalledWith({
      message: ERROR_PLUGIN_CRASH(plugin.name),
      type: 'error',
    });
  });
  it('should call all event callbacks for specific event type even if one event callback provided by plugin throws error', () => {
    const getStateSpy = jest.spyOn(store, 'getState');
    getStateSpy.mockImplementation(
      () =>
        ({
          plugins: {
            ...PLUGINS_INITIAL_STATE_MOCK,
            activePlugins: {
              data: {
                [plugin.hash]: plugin,
              },
              pluginsId: [plugin.hash],
            },
            list: PLUGINS_INITIAL_STATE_LIST_MOCK,
          },
        }) as RootState,
    );

    const errorCallbackMock = jest.fn(() => {
      throw new Error('Invalid callback');
    });

    const callbackMock = jest.fn(() => {
      return 'plguin';
    });

    PluginsEventBus.addListener(plugin.hash, plugin.name, 'onSubmapOpen', errorCallbackMock);
    PluginsEventBus.addListener(secondPlugin.hash, secondPlugin.name, 'onSubmapOpen', callbackMock);

    PluginsEventBus.dispatchEvent('onSubmapOpen', 109);

    expect(errorCallbackMock).toHaveBeenCalled();
    expect(errorCallbackMock).toThrow('Invalid callback');

    expect(callbackMock).toHaveBeenCalled();
  });
});

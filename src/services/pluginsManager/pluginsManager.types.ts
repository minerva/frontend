import { MinervaPlugin } from '@/types/models';
import { Unsubscribe } from '@reduxjs/toolkit';
import { configurationMapper } from './pluginsManager.utils';

export type RegisterPlugin = {
  pluginName: string;
  pluginVersion: string;
  pluginUrl: string;
  withoutPanel?: boolean | undefined;
};

export type MinervaConfiguration = ReturnType<typeof configurationMapper>;

export type PluginsManagerType = {
  hashedPlugins: {
    [url: string]: string;
  };
  setHashedPlugin({ pluginUrl, pluginScript }: { pluginUrl: string; pluginScript: string }): string;
  init(): Unsubscribe;
  registerPlugin({ pluginName, pluginVersion, pluginUrl, withoutPanel }: RegisterPlugin): {
    element: HTMLDivElement;
  };
  createAndGetPluginContent(plugin: Pick<MinervaPlugin, 'hash'>, detached: boolean): HTMLDivElement;
  removePluginContent(plugin: Pick<MinervaPlugin, 'hash'>): void;
  activePlugins: {
    [pluginId: string]: string[];
  };
  pluginsOccurrences: {
    [pluginId: string]: number;
  };
  unloadActivePlugin(hash: string): void;
  getExtendedPluginHash(hash: string): string;
  updatePluginOccurrence(hash: string, extendedHash: string): void;
  getExtendedPluginName(hash: string, extendedHash: string, pluginName: string): string;
};

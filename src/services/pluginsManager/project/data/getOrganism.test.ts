import { RootState, store } from '@/redux/store';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { getOrganism } from './getOrganism';

jest.mock('../../../../redux/store');

describe('getOrganism', () => {
  const getStateSpy = jest.spyOn(store, 'getState');
  it('should return the project organism from project if project exists and data is valid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: projectFixture,
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getOrganism()).toEqual(projectFixture.organism);
  });
  it('should throw error if project does not exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(() => getOrganism()).toThrow('Project does not exist');
  });
  it('should return undefined if project data is invalid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: {},
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getOrganism()).toEqual(undefined);
  });
});

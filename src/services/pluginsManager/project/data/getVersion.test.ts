import { RootState, store } from '@/redux/store';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { getVersion } from './getVersion';

jest.mock('../../../../redux/store');

describe('getVersion', () => {
  const getStateSpy = jest.spyOn(store, 'getState');
  it('should return the project id from project if project exists and data is valid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: projectFixture,
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getVersion()).toEqual(projectFixture.version);
  });
  it('should throw error if project does not exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(() => getVersion()).toThrow('Project does not exist');
  });
  it('should return undefined if project data is invalid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: {},
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getVersion()).toEqual(undefined);
  });
});

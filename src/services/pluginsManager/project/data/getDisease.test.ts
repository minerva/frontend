import { RootState, store } from '@/redux/store';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { getDisease } from './getDisease';

jest.mock('../../../../redux/store');

describe('getDisease', () => {
  const getStateSpy = jest.spyOn(store, 'getState');
  it('should return the disease from project if project exists and data is valid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: projectFixture,
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getDisease()).toEqual(projectFixture.disease);
  });
  it('should throw error if project does not exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(() => getDisease()).toThrow('Project does not exist');
  });
  it('returns undefined if project data is invalid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            data: {},
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        }) as RootState,
    );

    expect(getDisease()).toEqual(undefined);
  });
});

/* eslint-disable no-magic-numbers */

import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PluginsContextMenu } from '@/services/pluginsManager/pluginContextMenu/pluginsContextMenu';

const plugin = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

jest.mock('../../../utils/showToast');

describe('PluginsContextMenu', () => {
  beforeEach(() => {
    PluginsContextMenu.menuItems = [];
  });
  afterEach(() => {
    PluginsContextMenu.menuItems = [];
  });
  describe('addMenu', () => {
    it('add store context menu', () => {
      const callback = jest.fn();
      const id = PluginsContextMenu.addMenu(plugin.hash, 'Click me', '', true, callback);

      expect(PluginsContextMenu.menuItems).toEqual([
        {
          hash: plugin.hash,
          style: '',
          name: 'Click me',
          enabled: true,
          id,
          callback,
        },
      ]);
    });
    it('update store context menu', () => {
      const callback = jest.fn();
      const id = PluginsContextMenu.addMenu(plugin.hash, 'Click me', '', true, callback);
      PluginsContextMenu.updateMenu(plugin.hash, id, 'New name', '.stop-me', false);

      expect(PluginsContextMenu.menuItems).toEqual([
        {
          hash: plugin.hash,
          style: '.stop-me',
          name: 'New name',
          enabled: false,
          id,
          callback,
        },
      ]);
    });
    it('remove from store context menu', () => {
      const callback = jest.fn();
      const id = PluginsContextMenu.addMenu(plugin.hash, 'Click me', '', true, callback);
      PluginsContextMenu.removeMenu(plugin.hash, id);

      expect(PluginsContextMenu.menuItems).toEqual([]);
    });
  });
});

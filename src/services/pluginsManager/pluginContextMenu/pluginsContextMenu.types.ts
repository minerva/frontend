import { ModelElement, NewReaction } from '@/types/models';

export type ClickCoordinates = {
  modelId: number;
  x: number;
  y: number;
  zoom: number;
};

export type PluginContextMenuItemType = {
  id: string;
  hash: string;
  name: string;
  style: string;
  enabled: boolean;
  callback: (
    coordinates: ClickCoordinates,
    element: ModelElement | NewReaction | undefined,
  ) => void;
};

export type PluginsContextMenuType = {
  menuItems: PluginContextMenuItemType[];
  addMenu: (
    hash: string,
    name: string,
    style: string,
    enabled: boolean,
    callback: (
      coordinates: ClickCoordinates,
      element: ModelElement | NewReaction | undefined,
    ) => void,
  ) => string;
  removeMenu: (hash: string, id: string) => void;
  updateMenu: (hash: string, id: string, name: string, style: string, enabled: boolean) => void;
};

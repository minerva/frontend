import { setMapPosition } from '@/redux/map/map.slice';
import { store } from '@/redux/store';
import { Point } from '@/types/map';
import { ZodError } from 'zod';
import { setCenter } from './setCenter';

jest.mock('../../../../redux/store');

describe('setCenter - plugin method', () => {
  const dispatchSpy = jest.spyOn(store, 'dispatch');

  describe('when position is invalid', () => {
    const invalidPositions = [
      {
        x: -1,
        y: 1,
        z: 1,
      },
      {
        x: 1,
        y: -1,
        z: 1,
      },
      {
        x: 1,
        y: 1,
        z: -1,
      },
      {
        y: 1,
      },
      {
        x: 1,
      },
    ] as Point[];

    it.each(invalidPositions)('should throw error', position => {
      expect(() => setCenter(position)).toThrow(ZodError);
    });
  });

  describe('when position is valid', () => {
    const position: Point = {
      x: 500,
      y: 200,
      z: 2,
    };

    it('should set map position', () => {
      setCenter(position);

      expect(dispatchSpy).toHaveBeenCalledWith(setMapPosition(position));
    });
  });
});

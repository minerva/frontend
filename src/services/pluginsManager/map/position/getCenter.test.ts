import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { RootState, store } from '@/redux/store';
import { getCenter } from './getCenter';

jest.mock('../../../../redux/store');

describe('getCenter - plugin method', () => {
  const getStateSpy = jest.spyOn(store, 'getState');

  getStateSpy.mockImplementation(
    () =>
      ({
        map: {
          data: {
            ...initialMapDataFixture,
            position: {
              ...initialMapDataFixture.position,
              last: {
                x: 2137,
                y: 420,
                z: 1.488,
              },
            },
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
      }) as RootState,
  );

  it('should return last position from Redux', () => {
    expect(getCenter()).toStrictEqual({
      x: 2137,
      y: 420,
      z: 1.488,
    });
  });
});

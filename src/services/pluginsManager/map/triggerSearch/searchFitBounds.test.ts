/* eslint-disable no-magic-numbers */
import { handleSetBounds } from '@/utils/map/useSetBounds';
import { Map } from 'ol';
import * as getVisibleBioEntitiesPolygonCoordinates from './getVisibleBioEntitiesPolygonCoordinates';
import { searchFitBounds } from './searchFitBounds';

jest.mock('../../../../utils/map/useSetBounds');

jest.mock('./getVisibleBioEntitiesPolygonCoordinates', () => ({
  __esModule: true,
  ...jest.requireActual('./getVisibleBioEntitiesPolygonCoordinates'),
}));

const getVisibleBioEntitiesPolygonCoordinatesSpy = jest.spyOn(
  getVisibleBioEntitiesPolygonCoordinates,
  'getVisibleBioEntitiesPolygonCoordinates',
);

describe('searchFitBounds', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should not handle set bounds if data is not valid', () => {
    getVisibleBioEntitiesPolygonCoordinatesSpy.mockImplementation(() => undefined);

    searchFitBounds();

    expect(handleSetBounds).not.toHaveBeenCalled();
  });
  it('should not handle set bounds if map instance is not valid', () => {
    getVisibleBioEntitiesPolygonCoordinatesSpy.mockImplementation(() => ({
      mapInstance: null,
      maxZoom: 5,
      polygonCoordinates: [
        [231, 231],
        [842, 271],
      ],
    }));

    searchFitBounds();

    expect(handleSetBounds).not.toHaveBeenCalled();
  });
  it('should handle set bounds if provided data is valid', () => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const maxZoom = 5;
    const polygonCoordinates = [
      [231, 231],
      [842, 271],
    ];

    getVisibleBioEntitiesPolygonCoordinatesSpy.mockImplementation(() => ({
      mapInstance,
      maxZoom,
      polygonCoordinates,
    }));

    searchFitBounds();

    expect(handleSetBounds).toHaveBeenCalled();
    expect(handleSetBounds).toHaveBeenCalledWith(mapInstance, maxZoom, polygonCoordinates);
  });
  it('should handle set bounds with max zoom if zoom is not provided in argument', () => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const maxZoom = 23;
    const polygonCoordinates = [
      [231, 231],
      [842, 271],
    ];

    getVisibleBioEntitiesPolygonCoordinatesSpy.mockImplementation(() => ({
      mapInstance,
      maxZoom,
      polygonCoordinates,
    }));

    searchFitBounds();

    expect(handleSetBounds).toHaveBeenCalled();
    expect(handleSetBounds).toHaveBeenCalledWith(mapInstance, maxZoom, polygonCoordinates);
  });
  it('should handle set bounds with zoom provided in argument instead of max zoom', () => {
    const zoom = 12;
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const maxZoom = 23;
    const polygonCoordinates = [
      [231, 231],
      [842, 271],
    ];

    getVisibleBioEntitiesPolygonCoordinatesSpy.mockImplementation(() => ({
      mapInstance,
      maxZoom,
      polygonCoordinates,
    }));

    searchFitBounds(zoom);

    expect(handleSetBounds).toHaveBeenCalled();
    expect(handleSetBounds).toHaveBeenCalledWith(mapInstance, zoom, polygonCoordinates);
  });
});

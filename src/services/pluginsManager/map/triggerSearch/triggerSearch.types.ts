export type SearchByQueryParams = {
  query: string;
  perfectSearch?: boolean;
  fitBounds?: boolean;
};

export type Coordinates = {
  x: number;
  y: number;
};

export type SearchByCoordinatesParams = {
  coordinates: Coordinates;
  modelId: number;
  fitBounds?: boolean;
};

export type SearchParams = SearchByCoordinatesParams | SearchByQueryParams;

import { bioEntityResponseFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { apiPath } from '@/redux/apiPath';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import { MAP_INITIAL_STATE } from '@/redux/map/map.constants';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { resetReactionsData } from '@/redux/reactions/reactions.slice';
import { RootState, store } from '@/redux/store';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { waitFor } from '@testing-library/react';
import { HttpStatusCode } from 'axios';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { searchByQuery } from './searchByQuery';
import { searchFitBounds } from './searchFitBounds';

const MOCK_SEARCH_BY_QUERY_STORE = {
  models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
  map: {
    ...MAP_INITIAL_STATE,
    data: {
      ...MAP_INITIAL_STATE.data,
      modelId: HISTAMINE_MAP_ID,
      size: {
        width: 256,
        height: 256,
        tileSize: 256,
        minZoom: 1,
        maxZoom: 1,
      },
    },
  },
  drugs: {
    loading: 'succeeded',
    error: { message: '', name: '' },
  },
  drawer: {
    ...DRAWER_INITIAL_STATE,
    bioEntityDrawerState: {
      bioentityId: undefined,
      drugs: {},
      chemicals: {},
    },
  },
};

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'park7';

jest.mock('./searchFitBounds');
jest.mock('../../../../redux/store');
const dispatchSpy = jest.spyOn(store, 'dispatch');
const getStateSpy = jest.spyOn(store, 'getState');

describe('searchByQuery', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should fit bounds after search if hasFitBounds param is true', async () => {
    dispatchSpy.mockImplementation(() => ({
      unwrap: (): Promise<void> => Promise.resolve(),
    }));

    getStateSpy.mockImplementation(() => MOCK_SEARCH_BY_QUERY_STORE as RootState);
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, drugsFixture);

    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, chemicalsFixture);

    searchByQuery(SEARCH_QUERY, false, true);

    await waitFor(() => {
      expect(searchFitBounds).toHaveBeenCalled();
    });
  });
  it('should not fit bounds after search if hasFitBounds param is false', async () => {
    dispatchSpy.mockImplementation(() => ({
      unwrap: (): Promise<void> => Promise.resolve(),
    }));

    getStateSpy.mockImplementation(() => MOCK_SEARCH_BY_QUERY_STORE as RootState);
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, drugsFixture);

    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, chemicalsFixture);

    searchByQuery(SEARCH_QUERY, false, false);

    await waitFor(() => {
      expect(searchFitBounds).not.toHaveBeenCalled();
    });
  });

  it('should reset reactions data on every trigger', async () => {
    dispatchSpy.mockImplementation(() => ({
      unwrap: (): Promise<void> => Promise.resolve(),
    }));

    getStateSpy.mockImplementation(() => MOCK_SEARCH_BY_QUERY_STORE as RootState);
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, drugsFixture);

    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, chemicalsFixture);

    searchByQuery(SEARCH_QUERY, false, true);

    expect(dispatchSpy).toHaveBeenCalledWith(resetReactionsData());
  });
});

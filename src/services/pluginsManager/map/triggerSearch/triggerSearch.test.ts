/* eslint-disable no-magic-numbers */
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { CONFIGURATION_INITIAL_STORE_MOCKS } from '@/redux/configuration/configuration.mock';
import { bioEntityResponseFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { apiPath } from '@/redux/apiPath';
import { RootState, store } from '@/redux/store';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { waitFor } from '@testing-library/react';
import { HttpStatusCode } from 'axios';
import { Feature, Map } from 'ol';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import VectorLayer from 'ol/layer/Vector';
import { FEATURE_TYPE } from '@/constants/features';
import * as leftClickHandleAlias from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/leftClickHandleAlias';
import { MapManager } from '@/services/pluginsManager/map/mapManager';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import { NewReactionsState } from '@/redux/newReactions/newReactions.types';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import { MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';
import { ERROR_INVALID_MODEL_ID_TYPE } from '../../errorMessages';
import { triggerSearch } from './triggerSearch';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'park7';
const point = { x: 545.8013, y: 500.9926 };
const modelId = 1000;

const MOCK_STATE = {
  drawer: {
    isOpen: false,
  },
  map: {
    data: { ...initialMapDataFixture },
    loading: 'succeeded',
    error: { message: '', name: '' },
    openedMaps: openedMapsThreeSubmapsFixture,
  },
  modelElements: {
    data: {
      0: {
        data: [],
        loading: 'succeeded',
        error: { message: '', name: '' },
      },
    },
    search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
  } as ModelElementsState,
  newReactions: {
    0: {
      data: [],
      loading: 'succeeded',
      error: { message: '', name: '' },
    },
  } as NewReactionsState,
  configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
};

jest.mock('../../../../redux/store');
jest.mock(
  '../../../../components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/leftClickHandleAlias',
  () => ({
    __esModule: true,
    ...jest.requireActual(
      '../../../../components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/leftClickHandleAlias',
    ),
  }),
);
const leftClickHandleAliasSpy = jest.spyOn(leftClickHandleAlias, 'leftClickHandleAlias');

jest.mock(
  '../../../../components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/handleFeaturesClick',
  () => ({
    handleFeaturesClick: jest.fn(),
  }),
);

describe('triggerSearch', () => {
  let mapInstance: Map;
  const vectorLayer = new VectorLayer({});
  vectorLayer.set('type', LAYER_TYPE.PROCESS_LAYER);

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    mapInstance = new Map({ target: dummyElement });
    MapManager.setMapInstance(mapInstance);
    jest.clearAllMocks();
  });

  describe('search by query', () => {
    it('should throw error if query param is wrong type', async () => {
      jest.spyOn(store, 'getState').mockImplementation(() => MOCK_STATE as RootState);
      const invalidParams = {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        query: 123 as any,
      };

      await expect(triggerSearch(invalidParams)).rejects.toThrowError(
        'Invalid query type. The query should be of string type',
      );
    });
    it('should search for provided query and open drawer when it is not open', async () => {
      const getState = jest
        .spyOn(store, 'getState')
        .mockImplementation(() => MOCK_STATE as RootState);
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

      mockedAxiosClient
        .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, drugsFixture);

      mockedAxiosClient
        .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, chemicalsFixture);

      await expect(
        triggerSearch({
          query: SEARCH_QUERY,
        }),
      ).resolves.toBe(undefined);

      expect(store.dispatch).toHaveBeenCalledTimes(3);

      expect(store.dispatch).not.toHaveBeenCalledWith({
        payload: SEARCH_QUERY,
        type: 'drawer/selectTab',
      });

      expect(store.dispatch).toHaveBeenLastCalledWith({
        payload: SEARCH_QUERY,
        type: 'drawer/openSearchDrawerWithSelectedTab',
      });
      getState.mockRestore();
    });
    it('should search for provided query and select default tab when drawer is already open', async () => {
      const getState = jest.spyOn(store, 'getState').mockImplementation(
        () =>
          ({
            ...MOCK_STATE,
            drawer: {
              isOpen: true,
            },
          }) as RootState,
      );

      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

      mockedAxiosClient
        .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, drugsFixture);

      mockedAxiosClient
        .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, chemicalsFixture);

      await expect(
        triggerSearch({
          query: SEARCH_QUERY,
        }),
      ).resolves.toBe(undefined);

      expect(getState).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledTimes(3);
      expect(store.dispatch).not.toHaveBeenLastCalledWith({
        payload: SEARCH_QUERY,
        type: 'drawer/openSearchDrawerWithSelectedTab',
      });
      expect(store.dispatch).toHaveBeenLastCalledWith({
        payload: SEARCH_QUERY,
        type: 'drawer/selectTab',
      });

      getState.mockRestore();
    });
  });

  describe('search by coordinates', () => {
    it('should throw error if coordinates param is wrong type', async () => {
      jest.spyOn(store, 'getState').mockImplementation(() => MOCK_STATE as RootState);
      const invalidParams = {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        coordinates: {} as any,
        modelId: 53,
      };

      await expect(triggerSearch(invalidParams)).rejects.toThrowError(
        'Invalid coordinates type or values',
      );
    });

    it('should throw error if model id param is wrong type', async () => {
      jest.spyOn(store, 'getState').mockImplementation(() => MOCK_STATE as RootState);
      const invalidParams = {
        coordinates: { x: 992, y: 993 },
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        modelId: '53' as any,
      };

      await expect(triggerSearch(invalidParams)).rejects.toThrowError(ERROR_INVALID_MODEL_ID_TYPE);
    });

    it('should search result with proper data', async () => {
      const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }];
      jest.spyOn(store, 'getState').mockImplementation(() => MOCK_STATE as RootState);
      jest.spyOn(store, 'dispatch').mockImplementation(
        jest.fn(() => ({
          unwrap: jest.fn().mockResolvedValue(mockBioEntities),
        })),
      );
      const feature = new Feature({ id: 1, type: FEATURE_TYPE.ALIAS, zIndex: 1 });
      jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
        callback(feature, vectorLayer, null as unknown as SimpleGeometry);
      });

      const params = {
        coordinates: point,
        modelId,
      };

      await expect(triggerSearch(params)).resolves.toBe(undefined);

      await waitFor(() => {
        expect(leftClickHandleAliasSpy).toHaveBeenCalledWith(store.dispatch, undefined);
      });
    });

    it('should not search result if there is no bio entity with specific coordinates', async () => {
      jest.spyOn(store, 'getState').mockImplementation(() => MOCK_STATE as RootState);
      jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
        callback(new Feature({ zIndex: 1 }), vectorLayer, null as unknown as SimpleGeometry);
      });
      const params = {
        coordinates: point,
        modelId,
      };

      await expect(triggerSearch(params)).resolves.toBe(undefined);

      expect(leftClickHandleAliasSpy).not.toHaveBeenCalled();
    });
  });
});

/* eslint-disable no-magic-numbers */
import { MAP_INITIAL_STATE } from '@/redux/map/map.constants';
import { RootState, store } from '@/redux/store';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';

import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { MODEL_ELEMENTS_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import { getVisibleBioEntitiesPolygonCoordinates } from './getVisibleBioEntitiesPolygonCoordinates';

jest.mock('../../../../redux/store');

const getStateSpy = jest.spyOn(store, 'getState');

describe('getVisibleBioEntitiesPolygonCoordinates', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should return undefined if received array does not contain bioEntities with current map id', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
          map: {
            ...MAP_INITIAL_STATE,
            data: {
              ...MAP_INITIAL_STATE.data,
              modelId: HISTAMINE_MAP_ID,
              size: {
                width: 256,
                height: 256,
                tileSize: 256,
                minZoom: 1,
                maxZoom: 1,
              },
            },
          },
          modelElements: MODEL_ELEMENTS_INITIAL_STATE_MOCK,
          drugs: {
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
          drawer: {
            ...DRAWER_INITIAL_STATE,
            bioEntityDrawerState: {
              bioentityId: undefined,
              drugs: {},
              chemicals: {},
            },
          },
        }) as RootState,
    );

    expect(getVisibleBioEntitiesPolygonCoordinates()).toBe(undefined);
  });
  it('should return coordinates, max zoom, and map instance if received array contain bioEntities with current map id and max zoom', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
          map: {
            ...MAP_INITIAL_STATE,
            data: {
              ...MAP_INITIAL_STATE.data,
              modelId: 0,
              size: {
                width: 256,
                height: 256,
                tileSize: 256,
                minZoom: 1,
                maxZoom: 5,
              },
            },
          },
          modelElements: {
            data: {
              0: {
                data: [modelElementFixture],
                loading: 'succeeded',
                error: { message: '', name: '' },
              },
            },
            search: {
              data: [
                {
                  searchQueryElement: modelElementFixture.id.toString(),
                  loading: 'succeeded',
                  error: DEFAULT_ERROR,
                  data: [
                    {
                      modelElement: { ...modelElementFixture, model: 0, x: 97, y: 53, z: 1 },
                      perfect: true,
                    },
                    {
                      modelElement: { ...modelElementFixture, model: 0, x: 12, y: 25, z: 1 },
                      perfect: true,
                    },
                  ],
                },
              ],
              loading: 'succeeded',
              error: DEFAULT_ERROR,
            },
          } as ModelElementsState,
          drugs: {
            data: [
              {
                searchQueryElement: '',
                loading: 'succeeded',
                error: { name: '', message: '' },
                data: undefined,
              },
            ],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
          chemicals: {
            data: [
              {
                searchQueryElement: '',
                loading: 'succeeded',
                error: { name: '', message: '' },
                data: undefined,
              },
            ],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
          drawer: {
            ...DRAWER_INITIAL_STATE,
            bioEntityDrawerState: {
              bioentityId: undefined,
              drugs: {},
              chemicals: {},
            },
            searchDrawerState: {
              ...DRAWER_INITIAL_STATE.searchDrawerState,
              selectedSearchElement: modelElementFixture.id.toString(),
            },
          },
        }) as RootState,
    );

    expect(getVisibleBioEntitiesPolygonCoordinates()).toEqual({
      mapInstance: null,
      maxZoom: 5,
      polygonCoordinates: [
        [-18158992, 11740728],
        [-4852834, 16123932],
      ],
    });
  });
});

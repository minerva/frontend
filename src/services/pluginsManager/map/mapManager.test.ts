import { Map } from 'ol';

import { MapInstance } from '@/types/map';
import { MapManager } from './mapManager';

describe('MapManager', () => {
  describe('getMapInstance', () => {
    it('should return null if no map instance is set', () => {
      expect(MapManager.getMapInstance()).toBeNull();
    });

    it('should return the set map instance', () => {
      const dummyElement = document.createElement('div');
      const mapInstance = new Map({ target: dummyElement });
      MapManager.setMapInstance(mapInstance);
      expect(MapManager.getMapInstance()).toEqual(mapInstance);
    });
  });

  describe('setMapInstance', () => {
    beforeEach(() => {
      MapManager.mapInstance = null;
    });
    it('should set the map instance', () => {
      const dummyElement = document.createElement('div');
      const mapInstance = new Map({ target: dummyElement });
      MapManager.setMapInstance(mapInstance);
      expect(MapManager.mapInstance).toEqual(mapInstance);
    });
    it('should throw error if map instance is not valid', () => {
      expect(() => MapManager.setMapInstance({} as MapInstance)).toThrow('Not valid map instance');
    });
  });
});

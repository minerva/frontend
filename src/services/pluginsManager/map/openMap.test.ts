/* eslint-disable no-magic-numbers */
import { openMapAndSetActive, setActiveMap } from '@/redux/map/map.slice';
import { RootState, store } from '@/redux/store';
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { MODELS_MOCK, MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID } from '@/constants/mocks';
import { PluginsEventBus } from '../pluginsEventBus';
import { openMap } from './openMap';

jest.mock('../../../redux/store');

describe('openMap', () => {
  const getStateSpy = jest.spyOn(store, 'getState');
  const dispatchSpy = jest.spyOn(store, 'dispatch');
  const pluginDispatchEvent = jest.spyOn(PluginsEventBus, 'dispatchEvent');

  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should set active map when map with provided id is already opened', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          map: {
            data: { ...initialMapDataFixture, modelId: HISTAMINE_MAP_ID },
            loading: 'succeeded',
            error: { message: '', name: '' },
            openedMaps: openedMapsThreeSubmapsFixture,
          },
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        }) as RootState,
    );

    openMap({ id: MAIN_MAP_ID });

    expect(dispatchSpy).toHaveBeenCalledWith(setActiveMap({ modelId: MAIN_MAP_ID }));
    expect(pluginDispatchEvent).toHaveBeenCalledWith('onSubmapClose', HISTAMINE_MAP_ID);
    expect(pluginDispatchEvent).toHaveBeenCalledWith('onSubmapOpen', MAIN_MAP_ID);
  });

  it('should open map and set active map when map with provided id is not already opened', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          map: {
            data: { ...initialMapDataFixture, modelId: HISTAMINE_MAP_ID },
            loading: 'succeeded',
            error: { message: '', name: '' },
            openedMaps: openedMapsThreeSubmapsFixture,
          },
          models: {
            data: MODELS_MOCK,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        }) as RootState,
    );

    openMap({ id: 5061 });

    expect(dispatchSpy).toHaveBeenCalledWith(
      openMapAndSetActive({ modelId: 5061, modelName: 'Wnt signaling' }),
    );
    expect(pluginDispatchEvent).toHaveBeenCalledWith('onSubmapClose', HISTAMINE_MAP_ID);
    expect(pluginDispatchEvent).toHaveBeenCalledWith('onSubmapOpen', 5061);
  });

  it('should throw an error when map with provided id does not exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          map: {
            data: { ...initialMapDataFixture, modelId: HISTAMINE_MAP_ID },
            loading: 'succeeded',
            error: { message: '', name: '' },
            openedMaps: openedMapsThreeSubmapsFixture,
          },
          models: {
            data: MODELS_MOCK,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        }) as RootState,
    );

    expect(() => openMap({ id: 3 })).toThrow('Map with provided id does not exist');
    expect(store.dispatch).not.toHaveBeenCalled();
    expect(PluginsEventBus.dispatchEvent).not.toHaveBeenCalled();
  });
});

/* eslint-disable no-magic-numbers */
import { MAP_DATA_INITIAL_STATE } from '@/redux/map/map.constants';
import { store } from '@/redux/store';
import { Map } from 'ol';
import { MapManager } from '../mapManager';
import { getBounds } from './getBounds';

describe('getBounds', () => {
  it('should return undefined if map instance does not exist', () => {
    expect(getBounds()).toEqual(undefined);
  });
  it('should return current bounds if map instance exist', () => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    MapManager.setMapInstance(mapInstance);

    jest.spyOn(mapInstance, 'getView').mockImplementation(
      () =>
        ({
          calculateExtent: () => [
            -14409068.309137221, 17994265.029590994, -13664805.690862779, 18376178.970409006,
          ],
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any,
    );

    const getStateSpy = jest.spyOn(store, 'getState');
    getStateSpy.mockImplementation(
      () =>
        ({
          map: {
            data: {
              ...MAP_DATA_INITIAL_STATE,
              size: {
                width: 26779.25,
                height: 13503,
                tileSize: 256,
                minZoom: 2,
                maxZoom: 9,
              },
            },
            loading: 'idle',
            error: {
              name: '',
              message: '',
            },
            openedMaps: [],
          },
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any,
    );

    expect(getBounds()).toEqual({
      x1: 15044,
      y1: 4441,
      x2: 17034,
      y2: 5461,
    });
  });
});

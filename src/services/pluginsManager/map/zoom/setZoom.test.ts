/* eslint-disable no-magic-numbers */
import { ZOOM_ERRORS } from '@/constants/errors';
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { setLastPositionZoom } from '@/redux/map/map.slice';
import { RootState, store } from '@/redux/store';
import { ZodError } from 'zod';
import { setZoom } from './setZoom';

jest.mock('../../../../redux/store');

describe('setZoom - plugin method', () => {
  const dispatchSpy = jest.spyOn(store, 'dispatch');
  const getStateSpy = jest.spyOn(store, 'getState');

  beforeEach(() => {
    getStateSpy.mockImplementation(
      () =>
        ({
          map: {
            data: {
              ...initialMapDataFixture,
              position: {
                ...initialMapDataFixture.position,
                last: {
                  x: 2137,
                  y: 420,
                  z: 1.488,
                },
              },
              size: {
                ...initialMapDataFixture.size,
                minZoom: 2,
                maxZoom: 8,
              },
            },
            loading: 'succeeded',
            error: { message: '', name: '' },
            openedMaps: openedMapsThreeSubmapsFixture,
          },
        }) as RootState,
    );
  });

  describe('when zoom value type is invalid', () => {
    const invalidZoom = [-1, -123, '-123'] as number[];

    it.each(invalidZoom)('should throw error', zoom => {
      expect(() => setZoom(zoom)).toThrow(ZodError);
    });
  });

  describe('when zoom value value exeeds max zoom', () => {
    const invalidZoom = [444, 21, 9] as number[];

    it.each(invalidZoom)('should throw error', zoom => {
      expect(() => setZoom(zoom)).toThrow(ZOOM_ERRORS.ZOOM_VALUE_TOO_HIGH(8));
    });
  });

  describe('when zoom value value exeeds min zoom', () => {
    const invalidZoom = [1, 0] as number[];

    it.each(invalidZoom)('should throw error', zoom => {
      expect(() => setZoom(zoom)).toThrow(ZOOM_ERRORS.ZOOM_VALUE_TOO_LOW(2));
    });
  });

  describe('when zoom is valid', () => {
    const zoom = 2;

    it('should set map zoom', () => {
      setZoom(zoom);

      expect(dispatchSpy).toHaveBeenCalledWith(setLastPositionZoom({ zoom }));
    });
  });
});

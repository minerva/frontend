/* eslint-disable no-magic-numbers */
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { RootState, store } from '@/redux/store';
import { getZoom } from './getZoom';

jest.mock('../../../../redux/store');

describe('getZoom - plugin method', () => {
  const getStateSpy = jest.spyOn(store, 'getState');

  describe('when last position zoom is present', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            map: {
              data: {
                ...initialMapDataFixture,
                position: {
                  ...initialMapDataFixture.position,
                  last: {
                    x: 2137,
                    y: 420,
                    z: 1.488,
                  },
                },
              },
              loading: 'succeeded',
              error: { message: '', name: '' },
              openedMaps: openedMapsThreeSubmapsFixture,
            },
          }) as RootState,
      );
    });

    it('should return last position from Redux', () => {
      expect(getZoom()).toEqual(1.488);
    });
  });

  describe('when last position zoom is NOT present', () => {
    beforeEach(() => {
      getStateSpy.mockImplementation(
        () =>
          ({
            map: {
              data: {
                ...initialMapDataFixture,
                position: {
                  ...initialMapDataFixture.position,
                  last: {
                    x: 2137,
                    y: 420,
                  },
                },
              },
              loading: 'succeeded',
              error: { message: '', name: '' },
              openedMaps: openedMapsThreeSubmapsFixture,
            },
          }) as RootState,
      );
    });

    it('should return undefined', () => {
      expect(getZoom()).toBeUndefined();
    });
  });
});

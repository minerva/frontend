import { mapDataLastPositionSelector } from '@/redux/map/map.selectors';
import { store } from '@/redux/store';

export const getZoom = (): number | undefined => {
  const { getState } = store;
  const lastPosition = mapDataLastPositionSelector(getState());

  return lastPosition?.z;
};

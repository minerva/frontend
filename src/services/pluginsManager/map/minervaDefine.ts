import { PLUGIN_MINERVA_VERSION_NOT_SUPPORTED } from '@/redux/plugins/plugins.constants';

export const minervaDefine = (): void => {
  throw new Error(PLUGIN_MINERVA_VERSION_NOT_SUPPORTED);
};

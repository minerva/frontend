/* eslint-disable no-magic-numbers */
import { MAP_DATA_INITIAL_STATE } from '@/redux/map/map.constants';
import { Map } from 'ol';
import { store } from '@/redux/store';
import { fitBounds } from './fitBounds';
import { MapManager } from '../mapManager';

jest.mock('../../../../redux/store');

describe('fitBounds', () => {
  beforeEach(() => {
    jest.clearAllMocks();
    MapManager.mapInstance = null;
  });
  it('fitBounds should return undefined', () => {
    expect(
      fitBounds({
        x1: 5,
        y1: 10,
        x2: 15,
        y2: 20,
      }),
    ).toBe(undefined);
  });

  describe('when mapInstance is set', () => {
    it('should call and set map instance view properly', () => {
      const dummyElement = document.createElement('div');
      const mapInstance = new Map({ target: dummyElement });
      MapManager.setMapInstance(mapInstance);
      const view = mapInstance.getView();
      const fitSpy = jest.spyOn(view, 'fit');
      const getStateSpy = jest.spyOn(store, 'getState');
      getStateSpy.mockImplementation(
        () =>
          ({
            map: {
              data: {
                ...MAP_DATA_INITIAL_STATE,
                size: {
                  width: 256,
                  height: 256,
                  tileSize: 256,
                  minZoom: 1,
                  maxZoom: 1,
                },
              },
              loading: 'idle',
              error: {
                name: '',
                message: '',
              },
              openedMaps: [],
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
          }) as any,
      );

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb => cb());

      fitBounds({
        x1: 10,
        y1: 10,
        x2: 15,
        y2: 20,
      });

      // expect(getViewSpy).toHaveBeenCalledTimes(1);
      expect(fitSpy).toHaveBeenCalledWith([-18472078, 16906648, -17689363, 18472078], {
        maxZoom: 1,
        padding: [128, 128, 128, 128],
        size: undefined,
      });
    });
    it('should use max zoom value', () => {
      const dummyElement = document.createElement('div');
      const mapInstance = new Map({ target: dummyElement });
      MapManager.setMapInstance(mapInstance);
      const view = mapInstance.getView();
      const fitSpy = jest.spyOn(view, 'fit');
      const getStateSpy = jest.spyOn(store, 'getState');
      getStateSpy.mockImplementation(
        () =>
          ({
            map: {
              data: {
                ...MAP_DATA_INITIAL_STATE,
                size: {
                  width: 256,
                  height: 256,
                  tileSize: 256,
                  minZoom: 1,
                  maxZoom: 99,
                },
              },
              loading: 'idle',
              error: {
                name: '',
                message: '',
              },
              openedMaps: [],
            },
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
          }) as any,
      );

      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb => cb());

      fitBounds({
        x1: 10,
        y1: 10,
        x2: 15,
        y2: 20,
      });

      // expect(getViewSpy).toHaveBeenCalledTimes(1);
      expect(fitSpy).toHaveBeenCalledWith([-18472078, 16906648, -17689363, 18472078], {
        maxZoom: 99,
        padding: [128, 128, 128, 128],
        size: undefined,
      });
    });
  });
});

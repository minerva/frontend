/* eslint-disable no-magic-numbers */
import { pointToProjection } from './fitBounds.utils';

describe('pointToProjection - util', () => {
  describe('when mapSize arg is invalid', () => {
    const validPoint = {
      x: 0,
      y: 0,
    };

    const invalidMapSize = {
      width: -256 * 10,
      height: -256 * 10,
      tileSize: -256,
      minZoom: -1,
      maxZoom: -10,
    };

    it('should return fallback value on function call', () => {
      expect(pointToProjection(validPoint, invalidMapSize)).toStrictEqual([0, -0]);
    });
  });

  describe('when point and map size is valid', () => {
    const validPoint = {
      x: 256 * 100,
      y: 256 * 100,
    };

    const validMapSize = {
      width: 256 * 10,
      height: 256 * 10,
      tileSize: 256,
      minZoom: 1,
      maxZoom: 10,
    };

    const results = [380712659, -238107693];

    it('should return valid lat lng value on function call', () => {
      const [x, y] = pointToProjection(validPoint, validMapSize);

      expect(x).toBe(results[0]);
      expect(y).toBe(results[1]);
    });
  });
  describe('when point arg is invalid', () => {
    const invalidPoint = {
      x: 'x',
      y: 'y',
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as any;

    const validMapSize = {
      width: 256 * 10,
      height: 256 * 10,
      tileSize: 256,
      minZoom: 1,
      maxZoom: 10,
    };

    it('should return fallback value on function call', () => {
      expect(pointToProjection(invalidPoint, validMapSize)).toStrictEqual([0, 0]);
    });
  });
});

import { HALF } from '@/constants/dividers';
import { DEFAULT_TILE_SIZE } from '@/constants/map';

const BOUNDS_PADDING = DEFAULT_TILE_SIZE / HALF;
export const DEFAULT_PADDING = [BOUNDS_PADDING, BOUNDS_PADDING, BOUNDS_PADDING, BOUNDS_PADDING];

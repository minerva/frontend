/* eslint-disable no-magic-numbers */
import { RootState, store } from '@/redux/store';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import {
  ERROR_OVERLAY_ID_ALREADY_ACTIVE,
  ERROR_OVERLAY_ID_NOT_FOUND,
} from '@/services/pluginsManager/errorMessages';
import { getOverlayBioEntityForAllModels } from '@/redux/overlayBioEntity/overlayBioEntity.thunk';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import { showDataOverlay } from './showDataOverlay';

jest.mock('../../../../../redux/overlayBioEntity/overlayBioEntity.thunk');
jest.mock('../../../../../redux/store');

const OVERLAY_ID = overlaysPageFixture.content[0].id;

describe('showDataOverlay function', () => {
  afterEach(() => {
    jest.clearAllMocks(); // Clear mocks after each test
  });

  const getStateSpy = jest.spyOn(store, 'getState');

  it('should throw error if overlay is already active', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        }) as RootState,
    );

    expect(() => showDataOverlay(OVERLAY_ID)).toThrow(ERROR_OVERLAY_ID_ALREADY_ACTIVE);
  });

  it('should throw error if matching overlay is not found', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        }) as RootState,
    );

    expect(() => showDataOverlay(991)).toThrow(ERROR_OVERLAY_ID_NOT_FOUND);
  });

  it('should dispatch overlay id to show', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK },
        }) as RootState,
    );

    showDataOverlay(OVERLAY_ID);

    expect(getOverlayBioEntityForAllModels).toHaveBeenCalledWith({ overlayId: OVERLAY_ID });
  });

  it('should dispatch plugin event with overlay to show', () => {
    const pluginDispatchEvent = jest.spyOn(PluginsEventBus, 'dispatchEvent');

    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK },
        }) as RootState,
    );

    showDataOverlay(OVERLAY_ID);

    expect(pluginDispatchEvent).toHaveBeenCalledWith(
      'onShowOverlay',
      overlaysPageFixture.content[0],
    );
  });
});

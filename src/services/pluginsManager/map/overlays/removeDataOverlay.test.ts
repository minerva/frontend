/* eslint-disable no-magic-numbers */
import { RootState, store } from '@/redux/store';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { removeOverlay } from '@/redux/overlays/overlays.thunks';
import { removeDataOverlay } from './removeDataOverlay';
import { ERROR_OVERLAY_ID_NOT_FOUND } from '../../errorMessages';

jest.mock('../../../../redux/store');
jest.mock('../../../../redux/overlays/overlays.thunks');

const MOCK_STATE = {
  overlays: {
    ...OVERLAYS_INITIAL_STATE_MOCK,
    userOverlays: {
      data: overlaysPageFixture.content,
      loading: 'idle',
      error: DEFAULT_ERROR,
    },
  },
};

describe('removeDataOverlay', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const getStateSpy = jest.spyOn(store, 'getState');
  it('should throw error if matching overlay not found', () => {
    const overlayId = 991;
    getStateSpy.mockImplementation(() => MOCK_STATE as RootState);

    expect(() => removeDataOverlay(overlayId)).toThrow(ERROR_OVERLAY_ID_NOT_FOUND);
  });

  it('should dispatch removeOverlay with correct overlayId if matching overlay is found', () => {
    getStateSpy.mockImplementation(() => MOCK_STATE as RootState);
    const overlayId = overlaysPageFixture.content[0].id;

    removeDataOverlay(overlayId);

    expect(removeOverlay).toHaveBeenCalledWith({ overlayId });
  });
});

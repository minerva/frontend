import {
  createdOverlayFileFixture,
  overlayFixture,
  uploadedOverlayFileContentFixture,
} from '@/models/fixtures/overlaysFixture';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { apiPath } from '@/redux/apiPath';
import { RootState, store } from '@/redux/store';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { addOverlay } from '@/redux/overlays/overlays.thunks';
import {
  ERROR_OVERLAY_NAME_NOT_PROVIDED,
  ERROR_PROJECT_ID_NOT_FOUND,
} from '@/services/pluginsManager/errorMessages';
import { DEFAULT_GROUP } from '@/components/Map/Drawer/OverlaysDrawer/UserOverlayForm/UserOverlayForm.constants';
import { addDataOverlay } from './addDataOverlay';
import { DEFAULT_FILE_NAME, DEFAULT_TYPE } from './addDataOverlay.constants';

jest.mock('../../../../../redux/store');
jest.mock('../../../../../redux/overlays/overlays.thunks');

const MOCK_STATE = {
  project: {
    data: {
      ...projectFixture,
    },
    loading: 'succeeded',
    error: { message: '', name: '' },
  },
};

const mockedAxiosClient = mockNetworkResponse();

describe('addDataOverlay', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const getStateSpy = jest.spyOn(store, 'getState');

  const overlay = {
    name: 'Mock Overlay',
    description: 'Mock Description',
    filename: 'mockFile.txt',
    fileContent: 'Mock File Content',
    type: 'mockType',
  };
  it('should add overlay with provided data', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(createdOverlayFileFixture.id))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosClient
      .onPost(apiPath.createOverlay(projectFixture.projectId))
      .reply(HttpStatusCode.Ok, overlayFixture);

    getStateSpy.mockImplementation(() => MOCK_STATE as RootState);

    await addDataOverlay(overlay);

    expect(addOverlay).toHaveBeenCalledWith({
      content: overlay.fileContent,
      description: overlay.description,
      filename: overlay.filename,
      name: overlay.name,
      projectId: projectFixture.projectId,
      type: overlay.type,
      group: DEFAULT_GROUP,
    });
  });

  it('should throw error when project id is not found', async () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          project: {
            ...MOCK_STATE.project,
            data: {
              ...MOCK_STATE.project.data,
              projectId: '',
            },
          },
        }) as RootState,
    );

    await expect(() => addDataOverlay(overlay)).rejects.toThrow(ERROR_PROJECT_ID_NOT_FOUND);
  });

  it('should throw error when overlay name is not provided', async () => {
    getStateSpy.mockImplementation(() => MOCK_STATE as RootState);

    const overlayWithoutName = {
      ...overlay,
      name: '',
    };

    await expect(addDataOverlay(overlayWithoutName)).rejects.toThrow(
      ERROR_OVERLAY_NAME_NOT_PROVIDED,
    );
  });

  it('should add overlay with default values when optional parameters are not provided', async () => {
    getStateSpy.mockImplementation(() => MOCK_STATE as RootState);

    const overlayWithoutDefaultValues = {
      name: 'Mock Overlay',
      fileContent: 'Mock File Content',
    };

    await addDataOverlay(overlayWithoutDefaultValues);

    expect(addOverlay).toHaveBeenCalledWith({
      content: overlay.fileContent,
      description: '',
      filename: DEFAULT_FILE_NAME,
      name: overlay.name,
      projectId: projectFixture.projectId,
      type: DEFAULT_TYPE,
      group: DEFAULT_GROUP,
    });
  });
});

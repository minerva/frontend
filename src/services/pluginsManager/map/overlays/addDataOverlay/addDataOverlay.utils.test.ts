/* eslint-disable no-magic-numbers */
import {
  ERROR_FAILED_TO_READ_FILE,
  ERROR_INVALID_TYPE_FILE_CONTENT,
} from '@/services/pluginsManager/errorMessages';
import { getOverlayContent } from './addDataOverlay.utils';

describe('getOverlayContent', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should return string if fileContent is a string', async () => {
    const content = 'This is a string content';
    const result = await getOverlayContent(content);
    expect(result).toBe(content);
  });

  it('should return file content if fileContent is a File object', async () => {
    const fileContent = 'File content';
    const file = new File([fileContent], 'test.txt', { type: 'text/plain' });
    const result = await getOverlayContent(file);
    expect(result).toBe(fileContent);
  });

  it('should throw error if fileContent is neither a string nor a File object', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    await expect(getOverlayContent(123 as any)).rejects.toThrow(ERROR_INVALID_TYPE_FILE_CONTENT);
  });

  it('should throw error if there is an error reading the file', async () => {
    const file = new File(['File content'], 'test.txt', { type: 'text/plain' });
    const error = new Error('Failed to read file');
    jest.spyOn(FileReader.prototype, 'readAsText').mockImplementation(() => {
      throw error;
    });

    await expect(getOverlayContent(file)).rejects.toThrow(ERROR_FAILED_TO_READ_FILE);
  });
});

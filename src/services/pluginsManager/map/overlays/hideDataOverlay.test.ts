/* eslint-disable no-magic-numbers */
import { RootState, store } from '@/redux/store';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import { hideDataOverlay } from './hideDataOverlay';
import { PluginsEventBus } from '../../pluginsEventBus';
import { ERROR_OVERLAY_ID_NOT_ACTIVE, ERROR_OVERLAY_ID_NOT_FOUND } from '../../errorMessages';

const OVERLAY_ID = overlaysPageFixture.content[0].id;

describe('hideDataOverlay', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const getStateSpy = jest.spyOn(store, 'getState');
  it('should throw error if overlay is not active', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [123] },
        }) as RootState,
    );
    expect(() => hideDataOverlay(OVERLAY_ID)).toThrow(ERROR_OVERLAY_ID_NOT_ACTIVE);
  });

  it('should throw error if matching overlay is not found', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        }) as RootState,
    );

    expect(() => hideDataOverlay(431)).toThrow(ERROR_OVERLAY_ID_NOT_FOUND);
  });

  it('should hide overlay with provided id', () => {
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        }) as RootState,
    );

    hideDataOverlay(OVERLAY_ID);

    expect(dispatchSpy).toHaveBeenCalledWith({
      payload: { overlayId: OVERLAY_ID },
      type: 'overlayBioEntity/removeOverlayBioEntityForGivenOverlay',
    });
  });
  it('should dispatch plugin event with hidden overlay', () => {
    const pluginDispatchEvent = jest.spyOn(PluginsEventBus, 'dispatchEvent');

    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },
          overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        }) as RootState,
    );

    hideDataOverlay(OVERLAY_ID);

    expect(pluginDispatchEvent).toHaveBeenCalledWith(
      'onHideOverlay',
      overlaysPageFixture.content[0],
    );
  });
});

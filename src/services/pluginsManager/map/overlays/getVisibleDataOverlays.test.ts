import { DEFAULT_ERROR } from '@/constants/errors';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { RootState, store } from '@/redux/store';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import { DataOverlay } from '@/services/pluginsManager/map/overlays/types/DataOverlay';
import { getVisibleDataOverlays } from './getVisibleDataOverlays';

const ACTIVE_OVERLAYS_IDS = overlaysPageFixture.content.map(overlay => overlay.id);

describe('getVisibleDataOverlays', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const getStateSpy = jest.spyOn(store, 'getState');

  it('should return active overlays', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: overlaysPageFixture.content,
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },

          overlayBioEntity: {
            ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
            overlaysId: ACTIVE_OVERLAYS_IDS,
          },
        }) as RootState,
    );

    expect(getVisibleDataOverlays()).toEqual(
      overlaysPageFixture.content.map(overlay => new DataOverlay(overlay)),
    );
  });

  it('should return empty array if no active overlays', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_INITIAL_STATE_MOCK,
            userOverlays: {
              data: [],
              loading: 'idle',
              error: DEFAULT_ERROR,
            },
          },

          overlayBioEntity: OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
        }) as RootState,
    );

    expect(getVisibleDataOverlays()).toEqual([]);
  });
});

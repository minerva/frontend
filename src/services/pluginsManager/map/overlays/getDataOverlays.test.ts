import {
  OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
  PUBLIC_OVERLAYS_MOCK,
  USER_OVERLAYS_MOCK,
} from '@/redux/overlays/overlays.mock';
import { RootState, store } from '@/redux/store';
import { getDataOverlays } from './getDataOverlays';

describe('getDataOverlays', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const getStateSpy = jest.spyOn(store, 'getState');

  it('should return combined overlays and user overlays if exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
            userOverlays: {
              data: USER_OVERLAYS_MOCK,
              error: { message: '', name: '' },
              loading: 'succeeded',
            },
          },
        }) as RootState,
    );

    expect(getDataOverlays()).toEqual([...PUBLIC_OVERLAYS_MOCK, ...USER_OVERLAYS_MOCK]);
  });

  it('should return overlays if user overlays are not present', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
            userOverlays: {
              data: [],
              error: { message: '', name: '' },
              loading: 'succeeded',
            },
          },
        }) as RootState,
    );

    expect(getDataOverlays()).toEqual(PUBLIC_OVERLAYS_MOCK);
  });

  it('should return empty array if no overlays or user overlays exist', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          overlays: {
            ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
            data: [],
            userOverlays: {
              data: [],
              error: { message: '', name: '' },
              loading: 'succeeded',
            },
          },
        }) as RootState,
    );

    expect(getDataOverlays()).toEqual([]);
  });
});

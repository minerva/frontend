import { RootState, store } from '@/redux/store';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { getOpenMapId } from './getOpenMapId';
import { ERROR_INVALID_MODEL_ID_TYPE_FOR_RETRIEVAL } from '../errorMessages';

describe('getOpenMapId', () => {
  const getStateMock = jest.spyOn(store, 'getState');

  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should return the modelId of the current map', () => {
    getStateMock.mockImplementation(
      () =>
        ({
          map: initialMapStateFixture,
        }) as RootState,
    );

    expect(getOpenMapId()).toEqual(initialMapStateFixture.data.modelId);
  });

  it('should throw an error if modelId is not a number', () => {
    getStateMock.mockImplementation(
      () =>
        ({
          map: {
            ...initialMapStateFixture,
            data: {
              ...initialMapStateFixture.data,
              modelId: null,
            },
          },
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any,
    );

    expect(() => getOpenMapId()).toThrowError(ERROR_INVALID_MODEL_ID_TYPE_FOR_RETRIEVAL);
  });
});

import { RootState, store } from '@/redux/store';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { getModels } from './getModels';

jest.mock('../../../../redux/store');

describe('getModels', () => {
  const getStateSpy = jest.spyOn(store, 'getState');
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should return models when data is valid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          models: {
            data: modelsFixture,
          },
        }) as RootState,
    );

    expect(getModels()).toEqual(modelsFixture);
  });

  it('should return empty array when data is invalid', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          models: {
            data: 'invalid',
          },
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
        }) as any,
    );

    expect(getModels()).toEqual([]);
  });

  it('should return empty array when data is empty', () => {
    getStateSpy.mockImplementation(
      () =>
        ({
          models: {
            data: [],
          },
        }) as RootState,
    );

    expect(getModels()).toEqual([]);
  });
});

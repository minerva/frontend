/* eslint-disable no-magic-numbers */
import { ONE, TWO, ZERO } from '@/constants/common';
import { PLUGINS_CONTENT_ELEMENT_ATTR_NAME, PLUGINS_CONTENT_ELEMENT_ID } from '@/constants/plugins';
import { configurationFixture } from '@/models/fixtures/configurationFixture';
import { store } from '@/redux/store';
import { registerPlugin } from '@/redux/plugins/plugins.thunks';
import { PluginsManager } from './pluginsManager';
import { configurationMapper } from './pluginsManager.utils';
import { ERROR_PLUGIN_URL_MISMATCH } from './errorMessages';

jest.mock('../../redux/store');
jest.mock('../../redux/plugins/plugins.thunks');

jest.mock('uuid', () => ({
  v4: jest.fn().mockReturnValue('uuidValue'),
}));

const createWrapperInDocument = (): HTMLDivElement => {
  const wrapper = document.createElement('div');
  wrapper.id = PLUGINS_CONTENT_ELEMENT_ID;

  document.body.appendChild(wrapper);

  return wrapper;
};

const createPluginElement = (hash: string): void => {
  const element = document.createElement('div');
  element.setAttribute(PLUGINS_CONTENT_ELEMENT_ATTR_NAME, hash);

  const wrapper = document.querySelector(`#${PLUGINS_CONTENT_ELEMENT_ID}`);
  wrapper?.append(element);
};

describe('PluginsManager', () => {
  const originalWindow = { ...global.window };

  beforeEach(() => {
    global.window = { ...originalWindow };
  });

  afterEach(() => {
    global.window = originalWindow;
    PluginsManager.hashedPlugins = {};
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('setHashedPlugin correctly computes hash and updates hashedPlugins', () => {
    const pluginUrl = 'https://example.com/plugin.js';
    const pluginScript = 'console.log("Hello, Plugin!");';

    PluginsManager.setHashedPlugin({ pluginUrl, pluginScript });

    expect(PluginsManager.hashedPlugins[pluginUrl]).toBe('edc7eeafccc9e1ab66f713298425947b');
  });

  it('init subscribes to store changes and updates minerva configuration', () => {
    (store.getState as jest.Mock).mockReturnValueOnce({
      configuration: { main: { data: configurationFixture } },
    });

    PluginsManager.init();

    expect(store.subscribe).toHaveBeenCalled();

    // Simulate store change
    (store.subscribe as jest.Mock).mock.calls[0][0]();

    expect(store.getState).toHaveBeenCalled();
    expect(window.minerva.configuration).toEqual(configurationMapper(configurationFixture));
  });
  it('init does not update minerva configuration when configuration is undefined', () => {
    (store.getState as jest.Mock).mockReturnValueOnce({
      configuration: { main: { data: undefined } },
    });

    PluginsManager.init();

    expect(store.subscribe).toHaveBeenCalled();

    // Simulate store change
    (store.subscribe as jest.Mock).mock.calls[0][0]();

    expect(store.getState).toHaveBeenCalled();
    expect(window.minerva.configuration).toBeUndefined();
  });

  it('registerPlugin dispatches action and returns element', () => {
    const pluginName = 'TestPlugin';
    const pluginVersion = '1.0.0';
    const pluginUrl = 'https://example.com/test-plugin.js';
    const pluginScript = 'console.log("Hello, Plugin!");';

    PluginsManager.setHashedPlugin({ pluginUrl, pluginScript });

    const result = PluginsManager.registerPlugin({ pluginName, pluginVersion, pluginUrl });

    expect(store.dispatch).toHaveBeenCalled();

    expect(result.element).toBeDefined();
  });

  it('createAndGetPluginContent creates a content in wrapper and returns element', () => {
    const pluginName = 'TestPlugin';
    const pluginVersion = '1.0.0';
    const pluginUrl = 'https://example.com/test-plugin.js';
    const hash = '128ce10ae1b46ec4bc6d7c07278b5c9e';
    PluginsManager.hashedPlugins = {
      [pluginUrl]: hash,
    };

    createWrapperInDocument();

    const result = PluginsManager.registerPlugin({ pluginName, pluginVersion, pluginUrl });

    expect(result.element).toBeDefined();
    expect(result.element.getAttribute(PLUGINS_CONTENT_ELEMENT_ATTR_NAME)).toBe(hash);

    expect(result.element.parentNode).toBeDefined();
    expect(result.element.parentElement?.id).toBe(PLUGINS_CONTENT_ELEMENT_ID);
  });

  it('removePluginContent removes content in wrapper and returns element', () => {
    const hash = '128ce10ae1b46ec4bc6d7c07278b5c9e';

    const wrapper = createWrapperInDocument();
    createPluginElement(hash);

    PluginsManager.removePluginContent({ hash });

    expect(
      document.querySelector(`div[${PLUGINS_CONTENT_ELEMENT_ATTR_NAME}="${hash}"]`),
    ).not.toBeInTheDocument();

    expect(wrapper?.childElementCount).toEqual(ZERO);
  });
  it('should throw error if plugin url from script does not match actual script url', () => {
    const pluginName = 'TestPlugin';
    const pluginVersion = '1.0.0';
    const pluginUrl = 'https://example.com/test-plugin.js';
    const scriptPluginUrl = 'https://example.com/test-plugin.js?param_x';
    const hash = '128ce10ae1b46ec4bc6d7c07278b5c9e';
    PluginsManager.hashedPlugins = {
      [pluginUrl]: hash,
    };

    expect(() =>
      PluginsManager.registerPlugin({
        pluginName,
        pluginVersion,
        pluginUrl: scriptPluginUrl,
      }),
    ).toThrow(ERROR_PLUGIN_URL_MISMATCH);
  });

  it('should allow load multiple plugins', () => {
    const pluginName = 'TestPlugin';
    const pluginVersion = '1.0.0';
    const pluginUrl = 'https://example.com/test-plugin.js';
    const hash = '128ce10ae1b46ec4bc6d7c07278b5c9e';

    PluginsManager.activePlugins = {};
    PluginsManager.pluginsOccurrences = {};
    PluginsManager.hashedPlugins = {
      [pluginUrl]: hash,
    };

    PluginsManager.registerPlugin({ pluginName, pluginVersion, pluginUrl });
    PluginsManager.registerPlugin({ pluginName, pluginVersion, pluginUrl });
    PluginsManager.registerPlugin({ pluginName, pluginVersion, pluginUrl });

    expect(registerPlugin).toHaveBeenNthCalledWith(
      ONE,
      expect.objectContaining({
        isPublic: false,
        extendedPluginName: 'TestPlugin',
        pluginName: 'TestPlugin',
      }),
    );

    expect(registerPlugin).toHaveBeenNthCalledWith(
      TWO,
      expect.objectContaining({
        isPublic: false,
        extendedPluginName: 'TestPlugin (1)',
        pluginName: 'TestPlugin',
      }),
    );

    expect(registerPlugin).toHaveBeenNthCalledWith(
      3,
      expect.objectContaining({
        isPublic: false,
        extendedPluginName: 'TestPlugin (2)',
        pluginName: 'TestPlugin',
      }),
    );
  });
  describe('updatePluginOccurrence', () => {
    it('should increment occurrence if hash exists in pluginsOccurrences and extendedHash has prefix', () => {
      const hash = 'someHash';
      const extendedHash = 'prefix-someHash';

      PluginsManager.pluginsOccurrences = { [hash]: 1 };
      const expectedPluginsOccurrences = { [hash]: 2 };

      PluginsManager.updatePluginOccurrence(hash, extendedHash);

      expect(PluginsManager.pluginsOccurrences).toEqual(expectedPluginsOccurrences);
    });

    it('should initialize occurrence if hash does not exist in pluginsOccurrences', () => {
      const hash = 'someHash';
      const extendedHash = 'someHash-suffix';

      PluginsManager.pluginsOccurrences = {};
      const expectedPluginsOccurrences = { [hash]: ZERO };

      PluginsManager.updatePluginOccurrence(hash, extendedHash);

      expect(PluginsManager.pluginsOccurrences).toEqual(expectedPluginsOccurrences);
    });

    it('should not modify occurrence if extendedHash does not have prefix', () => {
      const hash = 'someHash';
      const extendedHash = 'someHash';

      PluginsManager.pluginsOccurrences = { [hash]: ONE };

      PluginsManager.updatePluginOccurrence(hash, extendedHash);

      expect(PluginsManager.pluginsOccurrences).toEqual({ [hash]: ONE });
    });
  });
  describe('getExtendedPluginName', () => {
    it('should return pluginName if plugin is not active', () => {
      const hash = 'someHash';
      const extendedHash = 'someHash-suffix';
      const pluginName = 'Some Plugin';
      PluginsManager.activePlugins = {
        [hash]: [hash],
      };

      const result = PluginsManager.getExtendedPluginName(hash, extendedHash, pluginName);

      expect(result).toBe(pluginName);
    });

    it('should return pluginName without number if hash does not have prefix', () => {
      const hash = 'someHash';
      const extendedHash = 'someOtherHash';
      const pluginName = 'Some Plugin';

      PluginsManager.activePlugins = {
        [hash]: [hash, 'hash1'],
      };

      const result = PluginsManager.getExtendedPluginName(hash, extendedHash, pluginName);

      expect(result).toBe(pluginName);
    });

    it('should return pluginName with number if hash has prefix and there is more active plugins than 1', () => {
      const hash = 'someHash';
      const extendedHash = 'suffix-someHash';
      const pluginName = 'Some Plugin';

      PluginsManager.activePlugins = {
        [hash]: ['somePluginId1', 'somePluginId2'],
      };

      const result = PluginsManager.getExtendedPluginName(hash, extendedHash, pluginName);

      expect(result).toBe(`${pluginName} (${ONE})`);
    });
  });
  describe('getExtendedPluginHash', () => {
    beforeEach(() => {
      PluginsManager.activePlugins = {};
    });

    it('should return the original hash if it is not in active plugins', () => {
      const hash = 'someHash';
      const expectedExtendedHash = hash;

      const result = PluginsManager.getExtendedPluginHash(hash);

      expect(result).toBe(expectedExtendedHash);
    });

    it('should return hash with prefix if the original hash is in active plugins', () => {
      const hash = 'someHash';

      PluginsManager.activePlugins = {
        [hash]: [hash],
      };

      const result = PluginsManager.getExtendedPluginHash(hash);

      expect(result).toBe('a7d06e6d48c9cd8b7726c5d91238408f-someHash');
    });
  });
  describe('unloadActivePlugin', () => {
    beforeEach(() => {
      PluginsManager.activePlugins = {};
      PluginsManager.pluginsOccurrences = {};
    });

    it('should remove the hash from activePlugins and set occurrences to zero if the hash exists in activePlugins and its occurrences are zero after removal', () => {
      const hash = 'someHash';
      PluginsManager.activePlugins[hash] = [hash];
      PluginsManager.pluginsOccurrences[hash] = 0;

      PluginsManager.unloadActivePlugin(hash);

      expect(PluginsManager.activePlugins[hash]).toEqual([]);
      expect(PluginsManager.pluginsOccurrences[hash]).toBe(0);
    });

    it('should remove the hash from activePlugins and not modify occurrences if some hash exists in activePlugins', () => {
      const hash = 'someHash';
      const hashWithPrefix = `xxx-${hash}`;
      const occurrences = 2;
      PluginsManager.activePlugins[hash] = [hash, hashWithPrefix];
      PluginsManager.pluginsOccurrences[hash] = occurrences;

      PluginsManager.unloadActivePlugin(hashWithPrefix);

      expect(PluginsManager.activePlugins[hash]).toEqual([hash]);
      expect(PluginsManager.pluginsOccurrences[hash]).toBe(occurrences);
    });

    it('should not modify activePlugins or occurrences if the hash does not exist in activePlugins', () => {
      const hash = 'hash';

      const hashWithPrefix = `xxx-aaa`;
      const occurrences = 1;
      PluginsManager.activePlugins[hash] = [hash];
      PluginsManager.pluginsOccurrences[hash] = occurrences;

      PluginsManager.unloadActivePlugin(hashWithPrefix);

      expect(PluginsManager.activePlugins[hash]).toEqual([hash]);
      expect(PluginsManager.pluginsOccurrences[hash]).toEqual(occurrences);
    });
  });
});

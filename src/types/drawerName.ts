export type DrawerName =
  | 'none'
  | 'search'
  | 'project-info'
  | 'plugins'
  | 'export'
  | 'legend'
  | 'submaps'
  | 'reaction'
  | 'overlays'
  | 'overlay-group'
  | 'bio-entity'
  | 'comment'
  | 'available-plugins'
  | 'layers';

export type Loading = 'idle' | 'pending' | 'succeeded' | 'failed';
export interface LoadingInterface {
  loading: 'idle' | 'pending' | 'succeeded' | 'failed';
}

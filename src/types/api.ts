export interface QueryOptions<Response> {
  method: 'GET' | 'POST';
  path: string;
  response: Response;
}

export interface Query<Params, Response> {
  (params: Params): QueryOptions<Response>;
}

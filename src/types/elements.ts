export type ElementId = string | number;
export type Tab = string;
export type ElementIdTab = [ElementId, Tab];
export type ElementIdTabObj = Record<ElementId, Tab>;

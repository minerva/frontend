import { ModelElement } from './models';
import { PinType } from './pin';

export interface ModelElementWithPinType extends ModelElement {
  type: PinType;
}

export type MultiPinModelElement = ModelElementWithPinType[];

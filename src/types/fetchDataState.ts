import { Loading } from './loadingState';

export type FetchDataState<T, T2 = undefined> = {
  data: T | T2;
  loading: Loading;
  error: Error;
};

export type MultiSearchData<T, T2 = undefined> = {
  searchQueryElement: string;
} & FetchDataState<T, T2>;

export type MultiFetchDataState<T> = {
  data: MultiSearchData<T>[];
  loading: Loading;
  error: Error;
};

export type KeyedFetchDataState<T, T2 = undefined> = Record<string, FetchDataState<T, T2>>;

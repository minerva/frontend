export interface StandarizedPublication {
  pubmedId: string;
  year: string;
  journal: string;
  authors: string;
  title: string;
  modelNames: string;
  elementsIds: string;
}

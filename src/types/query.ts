import { Point } from './map';

export interface QueryData {
  id?: string;
  searchValue?: string[];
  perfectMatch: boolean;
  modelId?: number;
  backgroundId?: number;
  initialPosition?: Partial<Point>;
  overlaysId?: number[];
  pluginsId?: string[];
  oauthLogin?: string;
}

export interface QueryDataParams {
  id?: string;
  searchValue?: string;
  perfectMatch: boolean;
  modelId?: number;
  backgroundId?: number;
  x?: number;
  y?: number;
  z?: number;
  overlaysId?: string;
  pluginsId?: string;
}

export interface QueryDataRouterParams {
  id?: string;
  searchValue?: string;
  perfectMatch?: string;
  modelId?: string;
  backgroundId?: string;
  x?: string;
  y?: string;
  z?: string;
  overlaysId?: string;
  pluginsId?: string;
  oauthLogin?: string;
}

import Map from 'ol/Map';

export interface Point {
  x: number;
  y: number;
  z?: number;
}

export type LatLng = [number, number];

export type MapInstance = Map | undefined;

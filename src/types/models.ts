import { bioEntityContentSchema } from '@/models/bioEntityContentSchema';
import { bioEntityResponseSchema } from '@/models/bioEntityResponseSchema';
import { chemicalSchema } from '@/models/chemicalSchema';
import { colorSchema } from '@/models/colorSchema';
import { configurationOptionSchema } from '@/models/configurationOptionSchema';
import { configurationSchema, formatSchema, miriamTypesSchema } from '@/models/configurationSchema';
import { drugSchema } from '@/models/drugSchema';
import { elementSearchResult, elementSearchResultType } from '@/models/elementSearchResult';
import { exportElementsSchema, exportNetworkchema } from '@/models/exportSchema';
import { geneVariant } from '@/models/geneVariant';
import { lineSchema } from '@/models/lineSchema';
import { loginSchema } from '@/models/loginSchema';
import { createdOverlayFileSchema, mapOverlaySchema } from '@/models/mapOverlaySchema';
import {
  markerLineSchema,
  markerPinSchema,
  markerSchema,
  markerSurfaceSchema,
  markerWithPositionSchema,
} from '@/models/markerSchema';
import { mapModelSchema, mapModelsSchema } from '@/models/modelSchema';
import {
  overlayBioEntitySchema,
  overlayElementWithBioEntitySchema,
  overlayElementWithReactionSchema,
} from '@/models/overlayBioEntitySchema';
import { overlayLeftBioEntitySchema } from '@/models/overlayLeftBioEntitySchema';
import { overlayLeftReactionSchema } from '@/models/overlayLeftReactionSchema';
import {
  overviewImageLink,
  overviewImageLinkImage,
  overviewImageLinkModel,
  overviewImageLinkSearch,
} from '@/models/overviewImageLink';
import { overviewImageView } from '@/models/overviewImageView';
import { pluginSchema } from '@/models/pluginSchema';
import { projectSchema } from '@/models/projectSchema';
import { publicationSchema } from '@/models/publicationsSchema';
import { referenceSchema } from '@/models/referenceSchema';
import { sessionSchemaValid } from '@/models/sessionValidSchema';
import { statisticsSchema } from '@/models/statisticsSchema';
import { targetSchema } from '@/models/targetSchema';
import { targetSearchNameResult } from '@/models/targetSearchNameResult';
import { userPrivilegeSchema } from '@/models/userPrivilegesSchema';
import { z } from 'zod';
import { commentSchema } from '@/models/commentSchema';
import { userSchema } from '@/models/userSchema';
import { javaStacktraceSchema } from '@/models/javaStacktraceSchema';
import { oauthSchema } from '@/models/oauthSchema';
import { bioShapeSchema } from '@/models/bioShapeSchema';
import { shapeSchema } from '@/models/shapeSchema';
import { modelElementModificationSchema } from '@/models/modelElementModificationSchema';
import { lineTypeSchema } from '@/models/lineTypeSchema';
import { layerSchema } from '@/models/layerSchema';
import { layerTextSchema } from '@/models/layerTextSchema';
import { layerRectSchema } from '@/models/layerRectSchema';
import { pageableSchema } from '@/models/pageableSchema';
import { modelElementSchema } from '@/models/modelElementSchema';
import { layerOvalSchema } from '@/models/layerOvalSchema';
import { layerLineSchema } from '@/models/layerLineSchema';
import { arrowTypeSchema } from '@/models/arrowTypeSchema';
import { arrowSchema } from '@/models/arrowSchema';
import { shapeRelAbsSchema } from '@/models/shapeRelAbsSchema';
import { shapeRelAbsBezierPointSchema } from '@/models/shapeRelAbsBezierPointSchema';
import { newReactionSchema } from '@/models/newReactionSchema';
import { reactionProduct } from '@/models/reactionProduct';
import { operatorSchema } from '@/models/operatorSchema';
import { modificationResiduesSchema } from '@/models/modificationResiduesSchema';
import { segmentSchema } from '@/models/segmentSchema';
import { layerImageSchema } from '@/models/layerImageSchema';
import { glyphSchema } from '@/models/glyphSchema';
import { overlayGroupSchema } from '@/models/overlayGroupSchema';
import { publicationElementSchema } from '@/models/publicationElementSchema';

export type Project = z.infer<typeof projectSchema>;
export type OverviewImageView = z.infer<typeof overviewImageView>;
export type OverviewImageLink = z.infer<typeof overviewImageLink>;
export type OverviewImageLinkImage = z.infer<typeof overviewImageLinkImage>;
export type OverviewImageLinkModel = z.infer<typeof overviewImageLinkModel>;
export type OverviewImageLinkSearch = z.infer<typeof overviewImageLinkSearch>;

export type MapModels = z.infer<typeof mapModelsSchema>;
export type MapModel = z.infer<typeof mapModelSchema>;
export type BioShape = z.infer<typeof bioShapeSchema>;
export type LineType = z.infer<typeof lineTypeSchema>;
export type ArrowType = z.infer<typeof arrowTypeSchema>;
const layersSchema = pageableSchema(layerSchema);
export type Layers = z.infer<typeof layersSchema>;
export type Layer = z.infer<typeof layerSchema>;
export type LayerText = z.infer<typeof layerTextSchema>;
export type LayerRect = z.infer<typeof layerRectSchema>;
export type LayerOval = z.infer<typeof layerOvalSchema>;
export type LayerLine = z.infer<typeof layerLineSchema>;
export type LayerImage = z.infer<typeof layerImageSchema>;
export type Arrow = z.infer<typeof arrowSchema>;
export type Glyph = z.infer<typeof glyphSchema>;
const modelElementsSchema = pageableSchema(modelElementSchema);
export type ModelElements = z.infer<typeof modelElementsSchema>;
export type ModelElement = z.infer<typeof modelElementSchema>;
export type Shape = z.infer<typeof shapeSchema>;
export type ShapeRelAbs = z.infer<typeof shapeRelAbsSchema>;
export type ShapeRelAbsBezierPoint = z.infer<typeof shapeRelAbsBezierPointSchema>;
export type Modification = z.infer<typeof modelElementModificationSchema>;
export type MapOverlay = z.infer<typeof mapOverlaySchema>;
export type Drug = z.infer<typeof drugSchema>;
export type PinDetailsItem = z.infer<typeof targetSchema>;
export type BioEntityContent = z.infer<typeof bioEntityContentSchema>;
export type BioEntityResponse = z.infer<typeof bioEntityResponseSchema>;
export type Chemical = z.infer<typeof chemicalSchema>;
export type NewReaction = z.infer<typeof newReactionSchema>;
const newReactionsSchema = pageableSchema(newReactionSchema);
export type NewReactions = z.infer<typeof newReactionsSchema>;
export type Operator = z.infer<typeof operatorSchema>;
export type ReactionProduct = z.infer<typeof reactionProduct>;
export type Reference = z.infer<typeof referenceSchema>;
export type ElementSearchResult = z.infer<typeof elementSearchResult>;
export type ElementSearchResultType = z.infer<typeof elementSearchResultType>;
export type SessionValid = z.infer<typeof sessionSchemaValid>;
export type Segment = z.infer<typeof segmentSchema>;
export type Login = z.infer<typeof loginSchema>;
export type ConfigurationOption = z.infer<typeof configurationOptionSchema>;
export type Configuration = z.infer<typeof configurationSchema>;
export type ConfigurationFormatSchema = z.infer<typeof formatSchema>;
export type ConfigurationMiramiTypes = z.infer<typeof miriamTypesSchema>;
export type OverlayBioEntity = z.infer<typeof overlayBioEntitySchema>;
export type OverlayElementWithReaction = z.infer<typeof overlayElementWithReactionSchema>;
export type OverlayElementWithBioEntity = z.infer<typeof overlayElementWithBioEntitySchema>;
export type OverlayLeftBioEntity = z.infer<typeof overlayLeftBioEntitySchema>;
export type OverlayLeftReaction = z.infer<typeof overlayLeftReactionSchema>;
export type OverlayGroup = z.infer<typeof overlayGroupSchema>;
export type Line = z.infer<typeof lineSchema>;
export type CreatedOverlayFile = z.infer<typeof createdOverlayFileSchema>;
export type Color = z.infer<typeof colorSchema>;
export type Statistics = z.infer<typeof statisticsSchema>;
export type Publication = z.infer<typeof publicationSchema>;
export type PublicationElement = z.infer<typeof publicationElementSchema>;
export type ExportNetwork = z.infer<typeof exportNetworkchema>;
export type ExportElements = z.infer<typeof exportElementsSchema>;
export type MinervaPlugin = z.infer<typeof pluginSchema>; // Plugin type interferes with global Plugin type
export type GeneVariant = z.infer<typeof geneVariant>;
export type TargetSearchNameResult = z.infer<typeof targetSearchNameResult>;
export type UserPrivilege = z.infer<typeof userPrivilegeSchema>;
export type User = z.infer<typeof userSchema>;
export type MarkerPin = z.infer<typeof markerPinSchema>;
export type MarkerSurface = z.infer<typeof markerSurfaceSchema>;
export type MarkerLine = z.infer<typeof markerLineSchema>;
export type MarkerWithPosition = z.infer<typeof markerWithPositionSchema>;
export type Marker = z.infer<typeof markerSchema>;
export type ModificationResidue = z.infer<typeof modificationResiduesSchema>;
export type JavaStacktrace = z.infer<typeof javaStacktraceSchema>;
export type Comment = z.infer<typeof commentSchema>;

export type PageOf<T> = {
  totalPages: number;
  totalElements: number;
  numberOfElements: number;
  size: number;
  number: number;
  content: T[];
};

export type FilteredPageOf<T> = PageOf<T> & {
  filteredSize: number;
};

export type OAuth = z.infer<typeof oauthSchema>;

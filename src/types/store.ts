export type ThunkConfig = {
  rejectValue: string;
};

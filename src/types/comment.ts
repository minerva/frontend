import { Comment } from './models';
import { PinType } from './pin';

export interface CommentWithPinType extends Comment {
  pinType: PinType;
}

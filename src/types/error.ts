export interface PluginError {
  message: string;
}

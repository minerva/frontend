import { Color, GeneVariant } from './models';

export type OverlayBioEntityRenderType = 'line' | 'rectangle' | 'submap-link';

export type OverlayBioEntityRender = {
  id: number | string;
  modelId: number;
  /** bottom left corner of whole element, xMin */
  x1: number;
  /** bottom left corner of whole element, yMin */
  y1: number;
  /** top right corner of whole element, xMax */
  x2: number;
  /** top right corner of whole element, yMax */
  y2: number;
  width: number;
  height: number;
  value: number | null;
  overlayId: number;
  color: Color | null;
  hexColor?: string;
  type: OverlayBioEntityRenderType;
  geneVariants?: GeneVariant[] | null;
  name?: string;
};

export interface OverlayReactionCoords {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
  id: number;
  height: number;
  width: number;
}

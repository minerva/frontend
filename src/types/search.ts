export type BioEntityType = 'ALIAS' | 'REACTION';

export type IdSearchQuery = {
  elementId: number;
  modelId: number;
  type: BioEntityType;
  addNumbersToEntityNumber?: boolean;
};

export type MultiSearchByIdParams = {
  elementsToFetch: IdSearchQuery[];
};

export type PerfectMultiSearchParams = {
  searchQueries: string[];
  isPerfectMatch: boolean;
};

export type PerfectSearchParams = {
  searchQuery: string;
  isPerfectMatch: boolean;
  addNumbersToEntityNumber?: boolean;
};

import { Reference } from './models';

export type ReferenceFiltered = Omit<Reference, 'link'> & { link: string };

export type ReferenceGroup = {
  references: ReferenceFiltered[];
  source: string;
};

export type ReferenceGrouped = ReferenceGroup[];

import { Point } from './map';

export type LinePoint = [Point, Point];

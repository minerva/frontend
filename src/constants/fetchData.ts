import { FetchDataState } from '@/types/fetchDataState';
import { DEFAULT_ERROR } from './errors';

export const DEFAULT_FETCH_DATA: FetchDataState<[]> = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

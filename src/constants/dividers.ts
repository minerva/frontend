export const ONE_AND_HALF = 1.5;
export const HALF = 2;
export const TWO_AND_HALF = 2.5;
export const THIRD = 3;
export const QUARTER = 4;

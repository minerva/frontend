export const USER_ROLE = {
  ADMIN: 'admin',
  CURATOR: 'curator',
  USER: 'user',
};

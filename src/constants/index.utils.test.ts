import { getConfigValue, getProjectIdFromUrl } from './index.utils';

describe('getConfigValue - util', () => {
  it('should return value for existing key', () => {
    expect(getConfigValue('BASE_API_URL')).toBe('https://lux1.atcomp.pl/minerva/api');
  });

  it('should return default value for non-existing key', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect(getConfigValue('nonExistingKey' as any)).toBe('');
  });
});

describe('getProjectIdFromUrl - util', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should return project ID from URL if present', () => {
    jest.spyOn(URLSearchParams.prototype, 'get').mockReturnValue('my_project_id');
    expect(getProjectIdFromUrl()).toBe('my_project_id');
  });

  it('should return default project ID if not present in URL', () => {
    jest.spyOn(URLSearchParams.prototype, 'get').mockReturnValue(null);
    expect(getProjectIdFromUrl()).toBeNull();
  });
  it('should return undefined if window is undefined', () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    jest.spyOn(global as any, 'window', 'get').mockImplementation(() => undefined);

    expect(getProjectIdFromUrl()).toBeUndefined();
  });
});

export const PLUGINS_CONTENT_ELEMENT_ATTR_NAME = 'data-hash';
export const PLUGINS_CONTENT_ELEMENT_ID = 'plugins';
export const PLUGIN_HASH_PREFIX_SEPARATOR = '-';

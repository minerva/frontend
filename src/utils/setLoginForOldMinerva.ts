export const setLoginForOldMinerva = (loginName: string | undefined): void => {
  // old minerva require this information
  if (loginName) {
    window.localStorage.setItem('LOGIN', loginName);
  } else {
    window.localStorage.removeItem('LOGIN');
  }
};

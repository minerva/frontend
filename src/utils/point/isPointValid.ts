import { mapPointSchema } from '@/models/mapPoint';
import { Point } from '@/types/map';

export const isPointValid = (point: Point): boolean => {
  const { success } = mapPointSchema.safeParse(point);
  return success;
};

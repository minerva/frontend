/* eslint-disable no-magic-numbers */
import { Point } from '@/types/map';
import { isPointValid } from './isPointValid';

describe('isPointValid - util', () => {
  const cases = [
    [true, 1, 1, undefined], // x, y valid, z undefined
    [true, 1, 1, 1], // x, y, z valid
    [false, 1, undefined, 1], // y undefined
    [false, undefined, 1, 1], // x undefined
    [false, undefined, undefined, 1], // x, y undefined
    [false, 1, -1, 1], // y negative
    [false, -1, 1, 1], // x negative
    [false, -1, -1, 1], // x, y negative
    [false, -1, -1, -1], // x, y, z negative
  ];

  it.each(cases)('should return %s for point x=%s, y=%s, z=%s', (result, x, y, z) => {
    expect(isPointValid({ x, y, z } as Point)).toBe(result);
  });
});

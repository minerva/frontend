/* eslint-disable no-magic-numbers */
import { createCanvas } from './getCanvas';
import { getFontSizeToFit } from './getFontSizeToFit';

const getContext = (): CanvasRenderingContext2D => {
  const canvas = createCanvas({ width: 100, height: 100 });
  return canvas.getContext('2d') as CanvasRenderingContext2D;
};

describe('getFontSizeToFit', () => {
  const cases: [string, string, number, number][] = [
    ['Hello', 'Helvetica', 50, 10],
    ['123', 'Arial', 48, 16],
    ['1', '', 48, 48],
    ['Text', '', 0, 0],
    ['', '', 0, 0],
  ];
  it.each(cases)(
    'on text=%s, fontFace=%s, maxWidth=%s it should return value %s',
    (text, fontFace, maxWidth, result) => {
      const ctx = getContext();
      expect(getFontSizeToFit(ctx, text, fontFace, maxWidth)).toBeCloseTo(result);
    },
  );
});

/* eslint-disable no-magic-numbers */
import { createCanvas } from './getCanvas';

describe('getCanvas', () => {
  it('should return HTMLCanvasElement with valid size on positive params', () => {
    const result = createCanvas({
      width: 800,
      height: 600,
    });

    expect(result).toBeInstanceOf(HTMLCanvasElement);
    expect(result.width).toEqual(800);
    expect(result.height).toEqual(600);
  });
});

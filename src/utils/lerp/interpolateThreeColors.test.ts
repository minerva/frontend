/* eslint-disable no-magic-numbers */
import { RGBColor } from '@/types/colors';
import { interpolateThreeColors } from './interpolateThreeColors';

const LEFT_COLOR = { r: 255, g: 0, b: 0 }; // Red
const MIDDLE_COLOR = { r: 0, g: 255, b: 0 }; // Green
const RIGHT_COLOR = { r: 0, g: 0, b: 255 }; // Blue

describe('interpolateThreeColors - util', () => {
  const cases: [number, RGBColor][] = [
    [-1, LEFT_COLOR],
    [-0.75, { r: 191, g: 64, b: 0 }],
    [-0.5, { r: 128, g: 128, b: 0 }],
    [-0.25, { r: 64, g: 191, b: 0 }],
    [0, MIDDLE_COLOR],
    [0.25, { r: 0, g: 191, b: 64 }],
    [0.5, { r: 0, g: 128, b: 128 }],
    [0.75, { r: 0, g: 64, b: 191 }],
    [1, RIGHT_COLOR],
  ];

  it.each(cases)(
    `for linear gradient with range [-1,0,1]: left color (-1) ${JSON.stringify(
      LEFT_COLOR,
    )}, middle color (0) ${JSON.stringify(MIDDLE_COLOR)} and right Color (1) ${JSON.stringify(
      RIGHT_COLOR,
    )} and position %s should return %s`,
    (input, output) => {
      expect(
        interpolateThreeColors({
          leftColor: LEFT_COLOR,
          middleColor: MIDDLE_COLOR,
          rightColor: RIGHT_COLOR,
          position: input,
        }),
      ).toEqual(output);
    },
  );
});

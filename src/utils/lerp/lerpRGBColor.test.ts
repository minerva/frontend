/* eslint-disable no-magic-numbers */
import { lerpRGBColor } from './lerpRGBColor';

describe('interpolateColor', () => {
  const leftColor = { r: 255, g: 0, b: 0 }; // Red
  const rightColor = { r: 0, g: 255, b: 0 }; // Green

  it('should return color1 for position 0', () => {
    const result = lerpRGBColor({ leftColor, rightColor, position: 0 });
    expect(result).toEqual(leftColor);
  });

  it('should return color2 for position 1', () => {
    const result = lerpRGBColor({ leftColor, rightColor, position: 1 });
    expect(result).toEqual(rightColor);
  });

  it('should interpolate colors for position 0.25', () => {
    const result = lerpRGBColor({ leftColor, rightColor, position: 0.25 });
    expect(result).toEqual({ r: 191, g: 64, b: 0 });
  });

  it('should interpolate colors for position 0.5', () => {
    const result = lerpRGBColor({ leftColor, rightColor, position: 0.5 });
    expect(result).toEqual({ r: 128, g: 128, b: 0 });
  });

  it('should interpolate colors for position 0.75', () => {
    const result = lerpRGBColor({ leftColor, rightColor, position: 0.75 });
    expect(result).toEqual({ r: 64, g: 191, b: 0 });
  });
});

import { parseQueryToTypes } from './parseQueryToTypes';

describe('parseQueryToTypes', () => {
  it('should return valid data', () => {
    expect({}).toEqual({});

    expect(parseQueryToTypes({ searchValue: 'nadh;aspirin' })).toEqual({
      searchValue: ['nadh', 'aspirin'],
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: undefined },
      overlaysId: undefined,
    });

    expect(parseQueryToTypes({ perfectMatch: 'true' })).toEqual({
      searchValue: undefined,
      perfectMatch: true,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: undefined },
      overlaysId: undefined,
    });

    expect(parseQueryToTypes({ perfectMatch: 'false' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: undefined },
      overlaysId: undefined,
    });

    expect(parseQueryToTypes({ modelId: '666' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: 666,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: undefined },
      overlaysId: undefined,
    });
    expect(parseQueryToTypes({ x: '2137' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: 2137, y: undefined, z: undefined },
      overlaysId: undefined,
    });
    expect(parseQueryToTypes({ y: '1372' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: 1372, z: undefined },
      overlaysId: undefined,
    });
    expect(parseQueryToTypes({ z: '3721' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: 3721 },
      overlaysId: undefined,
    });
    expect(parseQueryToTypes({ overlaysId: '1,2,3' })).toEqual({
      searchValue: undefined,
      perfectMatch: false,
      modelId: undefined,
      backgroundId: undefined,
      initialPosition: { x: undefined, y: undefined, z: undefined },
      // eslint-disable-next-line no-magic-numbers
      overlaysId: [1, 2, 3],
    });
  });
});

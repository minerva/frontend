// eslint-disable-next-line import/no-extraneous-dependencies
import MockAdapter from 'axios-mock-adapter';
import { axiosInstance, axiosInstanceNewAPI } from '@/services/api/utils/axiosInstance';

export const mockNetworkResponse = (): MockAdapter => {
  const mock = new MockAdapter(axiosInstance);
  return mock;
};

export const mockNetworkNewAPIResponse = (): MockAdapter => {
  const mock = new MockAdapter(axiosInstanceNewAPI);
  return mock;
};

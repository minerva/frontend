import { createErrorData } from '@/utils/error-report/errorReporting';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { loginFixture } from '@/models/fixtures/loginFixture';
import { login, logout } from '@/redux/user/user.thunks';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { store } from '@/redux/store';
import { getConfiguration } from '@/redux/configuration/configuration.thunks';
import { configurationFixture } from '@/models/fixtures/configurationFixture';
import { userFixture } from '@/models/fixtures/userFixture';
import { SerializedError } from '@reduxjs/toolkit';
import { javaStacktraceFixture } from '@/models/fixtures/javaStacktraceFixture';

const mockedAxiosClient = mockNetworkResponse();

const CREDENTIALS = {
  login: 'test',
  password: 'password',
};

describe('createErrorData', () => {
  it('should add stacktrace', async () => {
    const error = await createErrorData(new Error('hello'), store.getState());
    expect(error.stacktrace).not.toEqual('');
  });

  it('should add url', async () => {
    const error = await createErrorData(new Error('hello'), store.getState());
    expect(error.url).not.toBeNull();
  });

  it('should add browser', async () => {
    const error = await createErrorData(new Error('hello'), store.getState());
    expect(error.browser).not.toBeNull();
  });

  it('should add guest login when not logged', async () => {
    const error = await createErrorData(new Error('hello'), store.getState());
    expect(error.login).toBe('anonymous');
  });

  it('should add login when logged', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    await store.dispatch(login(CREDENTIALS));

    const error = await createErrorData(new Error('hello'), store.getState());
    expect(error.login).not.toBe('anonymous');
    expect(error.login).toBe(loginFixture.login);
  });

  it('should add timestamp', async () => {
    const error = await createErrorData(new Error(), store.getState());
    expect(error.timestamp).not.toBeNull();
  });

  it('should add version', async () => {
    mockedAxiosClient
      .onGet(apiPath.getConfiguration())
      .reply(HttpStatusCode.Ok, configurationFixture);
    await store.dispatch(getConfiguration());

    const error = await createErrorData(new Error(), store.getState());
    expect(error.version).not.toBeNull();
  });

  it('should add email when logged', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    await store.dispatch(login(CREDENTIALS));

    const error = await createErrorData(new Error(), store.getState());
    expect(error.email).toBe(userFixture.email);
  });

  it('email should be empty when not logged', async () => {
    mockedAxiosClient.onPost(apiPath.logout()).reply(HttpStatusCode.Ok, {});
    await store.dispatch(logout());
    const error = await createErrorData(new Error(), store.getState());
    expect(error.email).toBeNull();
  });

  it('java stacktrace should be attached if error report provides info', async () => {
    mockedAxiosClient
      .onGet(apiPath.getStacktrace('dab932be-1e2e-45d7-b57a-aff30e2629e6'))
      .reply(HttpStatusCode.Ok, javaStacktraceFixture);

    const error: SerializedError = {
      code: 'dab932be-1e2e-45d7-b57a-aff30e2629e6',
    };
    const errorData = await createErrorData(error, store.getState());
    expect(errorData.javaStacktrace).not.toBeNull();
  });
});

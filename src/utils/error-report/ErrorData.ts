export type ErrorData = {
  url: string | null;
  login: string | null;
  email: string | null;
  browser: string | null;
  timestamp: number | null;
  version: string | null;
  comment: string | null;
  stacktrace: string;
  javaStacktrace: string | null;
};

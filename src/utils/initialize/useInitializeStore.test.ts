import { PROJECT_ID } from '@/constants';
import { modelsFixture, modelsPageFixture } from '@/models/fixtures/modelsFixture';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { apiPath } from '@/redux/apiPath';
import { modelsDataSelector } from '@/redux/models/models.selectors';
import { overlaysDataSelector } from '@/redux/overlays/overlays.selectors';
import { projectDataSelector } from '@/redux/project/project.selectors';
import { initDataLoadingInitialized } from '@/redux/root/init.selectors';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook, waitFor } from '@testing-library/react';
import { HttpStatusCode } from 'axios';
import * as hook from './useInitializeStore';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

describe('useInitializeStore - hook', () => {
  describe('when fired', () => {
    beforeAll(() => {
      mockedAxiosNewClient
        .onGet(apiPath.getModelsString())
        .reply(HttpStatusCode.Ok, modelsPageFixture);
      mockedAxiosNewClient
        .onGet(apiPath.getAllOverlaysByProjectIdQuery(PROJECT_ID, { publicOverlay: true }))
        .reply(HttpStatusCode.Ok, overlaysPageFixture);
      mockedAxiosNewClient
        .onGet(apiPath.getProjectById(PROJECT_ID))
        .reply(HttpStatusCode.Ok, projectFixture);
    });

    it('should fetch project data in store', async () => {
      const { Wrapper, store } = getReduxWrapperWithStore();
      renderHook(() => hook.useInitializeStore(), { wrapper: Wrapper });

      await waitFor(() => {
        const data = projectDataSelector(store.getState());
        expect(data).toEqual(projectFixture);
      });
    });

    it('should fetch overlays data in store', async () => {
      const { Wrapper, store } = getReduxWrapperWithStore();
      renderHook(() => hook.useInitializeStore(), { wrapper: Wrapper });

      await waitFor(() => {
        const data = overlaysDataSelector(store.getState());
        expect(data).toEqual(overlaysPageFixture.content);
      });
    });

    it('should fetch models data in store', async () => {
      const { Wrapper, store } = getReduxWrapperWithStore();
      renderHook(() => hook.useInitializeStore(), { wrapper: Wrapper });

      await waitFor(() => {
        const data = modelsDataSelector(store.getState());
        expect(data).toEqual(modelsFixture);
      });
    });

    it('should use valid initialize value', () => {
      const { Wrapper, store } = getReduxWrapperWithStore();
      const initializedeBefore = initDataLoadingInitialized(store.getState());
      renderHook(() => hook.useInitializeStore(), { wrapper: Wrapper });
      const initializedAfter = initDataLoadingInitialized(store.getState());

      expect(initializedeBefore).toBe(false);
      expect(initializedAfter).toBe(true);
    });
  });
});

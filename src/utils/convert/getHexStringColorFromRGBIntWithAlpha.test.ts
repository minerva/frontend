import { getHexStringColorFromRGBIntWithAlpha } from './getHexStringColorFromRGBIntWithAlpha';

const OPACITY_80 = 0.8;

describe('getHexTricolorGradientColorWithAlpha ', () => {
  it('should return the correct result with input with negative rgb integer', () => {
    expect(getHexStringColorFromRGBIntWithAlpha({ rgbInt: -3342388, alpha: OPACITY_80 })).toEqual(
      '#ccffcccc',
    );
  });

  it('should return the correct result with input with positive rgb integer', () => {
    expect(getHexStringColorFromRGBIntWithAlpha({ rgbInt: 57, alpha: OPACITY_80 })).toEqual(
      '#000039cc',
    );
  });
});

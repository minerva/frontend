import { getHexTricolorGradientColorWithAlpha } from './getHexTricolorGradientColorWithAlpha';

const RED_HEX = '#ff0000';
const GREEN_HEX = '#00ff00';
const BLUE_HEX = '#0000ff';

describe('getHexTricolorGradientColorWithAlpha', () => {
  const cases: [{ alpha: number | undefined; position: number }, string][] = [
    [{ alpha: 1, position: -1 }, '#FF0000ff'],
    [{ alpha: 0.8, position: -0.75 }, '#BF4000cc'],
    [{ alpha: 0.5, position: -0.5 }, '#80800080'],
    [{ alpha: 0, position: -0.25 }, '#40BF0000'],
    [{ alpha: 1, position: 0 }, '#00FF00ff'],
    [{ alpha: 1, position: 0.25 }, '#00BF40ff'],
    [{ alpha: 1, position: 0.5 }, '#008080ff'],
    [{ alpha: 1, position: 0.75 }, '#0040BFff'],
    [{ alpha: 1, position: 1 }, '#0000FFff'],
  ];

  it.each(cases)(`and position %s should return %s`, (input, output) => {
    expect(
      getHexTricolorGradientColorWithAlpha({
        leftColor: RED_HEX,
        middleColor: GREEN_HEX,
        rightColor: BLUE_HEX,
        alpha: input.alpha,
        position: input.position,
      }),
    ).toEqual(output);
  });
});

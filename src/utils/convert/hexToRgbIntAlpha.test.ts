import hexToRgbIntAlpha from './hexToRgbIntAlpha';

describe('hexToRgbIntAlpha', () => {
  it('should convert a valid 6-character HEX color to RGB integer and alpha', () => {
    const result = hexToRgbIntAlpha('#ff5733');
    expect(result).toEqual({ rgb: 16734003, alpha: 255 });
  });

  it('should convert a valid 8-character HEX color to RGB integer and alpha', () => {
    const result = hexToRgbIntAlpha('#ff5733cc');
    expect(result).toEqual({ rgb: 16734003, alpha: 204 });
  });

  it('should throw an error for an invalid HEX color length', () => {
    expect(() => hexToRgbIntAlpha('#f5')).toThrow('Invalid HEX color. Must be 6 or 8 characters.');
  });

  it('should throw an error for a malformed HEX color', () => {
    expect(() => hexToRgbIntAlpha('not-a-hex')).toThrow(
      'Invalid HEX color. Must be 6 or 8 characters.',
    );
  });
});

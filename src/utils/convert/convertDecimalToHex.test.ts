/* eslint-disable no-magic-numbers */

import { convertDecimalToHexColor } from './convertDecimalToHex';

describe('convertDecimalToHexColor - util', () => {
  it('should convert small decimal', () => {
    expect(convertDecimalToHexColor(57)).toEqual('#000039');
    expect(convertDecimalToHexColor(0)).toEqual('#000000');
  });

  it('should convert negative decimal', () => {
    expect(convertDecimalToHexColor(-3342388)).toEqual('#ccffcc');
    expect(convertDecimalToHexColor(-750)).toBe('#fffd12');
  });
});

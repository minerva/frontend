import { rgbToHex } from './rgbToHex'; // Replace 'yourFileName' with the actual file name where your function is defined

describe('rgbToHex - util', () => {
  it('should convert RGB values to hex format', () => {
    // Test case 1: Black color
    expect(rgbToHex({ r: 0, g: 0, b: 0 })).toBe('#000000');

    // Test case 2: White color
    expect(rgbToHex({ r: 255, g: 255, b: 255 })).toBe('#FFFFFF');

    // Test case 3: Red color
    expect(rgbToHex({ r: 255, g: 0, b: 0 })).toBe('#FF0000');

    // Test case 4: Green color
    expect(rgbToHex({ r: 0, g: 255, b: 0 })).toBe('#00FF00');

    // Test case 5: Blue color
    expect(rgbToHex({ r: 0, g: 0, b: 255 })).toBe('#0000FF');

    // Test case 6: Custom color
    expect(rgbToHex({ r: 128, g: 64, b: 32 })).toBe('#804020');
  });

  it('should handle invalid input values', () => {
    // Test case 1: Negative RGB values
    expect(() => rgbToHex({ r: -1, g: 0, b: 255 })).toThrow();

    // Test case 2: RGB values exceeding 255
    expect(() => rgbToHex({ r: 256, g: 128, b: 64 })).toThrow();

    // Test case 3: Non-integer RGB values
    expect(() => rgbToHex({ r: 50.5, g: 100.75, b: 150.25 })).toThrow();
  });
});

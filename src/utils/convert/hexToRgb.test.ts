import { expandHexToFullFormatIfItsShorthanded, hexToRgb } from './hexToRgb';

describe('expandHexToFullFormatIfItsShorthanded', () => {
  it('should expand short-handed hex string to full format', () => {
    const result = expandHexToFullFormatIfItsShorthanded('#abc');
    expect(result).toBe('#aabbcc');
  });

  it('should not modify full-format hex string', () => {
    const result = expandHexToFullFormatIfItsShorthanded('#aabbcc');
    expect(result).toBe('#aabbcc');
  });

  it('should handle hex string without leading #', () => {
    const result = expandHexToFullFormatIfItsShorthanded('abc');
    expect(result).toBe('#aabbcc');
  });

  it('should return original string if it does not match short-hand regex', () => {
    const result = expandHexToFullFormatIfItsShorthanded('invalid');
    expect(result).toBe('#invalid');
  });
});

describe('hexToRgb', () => {
  it('should convert valid hex string to RGB object', () => {
    const result = hexToRgb('#aabbcc');
    expect(result).toEqual({ r: 170, g: 187, b: 204 });
  });

  it('should return null for invalid hex string', () => {
    const result = hexToRgb('invalid');
    expect(result).toBeNull();
  });

  it('should handle hex string without leading #', () => {
    const result = hexToRgb('aabbcc');
    expect(result).toEqual({ r: 170, g: 187, b: 204 });
  });

  it('should return null for hex string with invalid characters', () => {
    const result = hexToRgb('#xyz123');
    expect(result).toBeNull();
  });

  it('should convert short-handed RGB hex string without leading # to RGB object', () => {
    const result = hexToRgb('abc'); // Short-handed RGB hex string without leading #
    expect(result).toEqual({ r: 170, g: 187, b: 204 });
  });

  it('should handle short-handed RGB hex string with invalid characters', () => {
    const result = hexToRgb('#xyz'); // Short-handed RGB hex string with invalid characters
    expect(result).toBeNull();
  });

  it('should handle short-handed RGB hex string with invalid characters and without leading #', () => {
    const result = hexToRgb('xyz'); // Short-handed RGB hex string with invalid characters and without leading #
    expect(result).toBeNull();
  });
});

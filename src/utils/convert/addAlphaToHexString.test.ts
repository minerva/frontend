import { addAlphaToHexString } from './addAlphaToHexString';

const Opactiy100 = 1;
const Opacity80 = 0.8;
const Opacity50 = 0.5;
const Opacity0 = 0;

describe('addAlphaToHexString', () => {
  const cases: [[string, number | undefined], string][] = [
    [['#ff0000', undefined], '#ff0000ff'],
    [['#ff0000', Opactiy100], '#ff0000ff'],
    [['#ff0000', Opacity80], '#ff0000cc'],
    [['#ff0000', Opacity50], '#ff000080'],
    [['#ff0000', Opacity0], '#ff000000'],
  ];

  it.each(cases)('for %s should return %s', (input, output) => {
    expect(addAlphaToHexString(...input)).toEqual(output);
  });
});

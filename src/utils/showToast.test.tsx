/* eslint-disable no-magic-numbers */
import { toast } from 'sonner';
import { showToast } from './showToast';

jest.mock('sonner', () => ({
  toast: {
    custom: jest.fn(),
    dismiss: jest.fn(),
  },
}));

describe('showToast', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should call toast.custom on showToast call', () => {
    showToast({ type: 'success', message: 'Success message' });
    expect(toast.custom).toHaveBeenCalledTimes(1);
  });
});

import { WithoutNullableKeys } from '@/types/utils';

export const getTruthyObjectOrUndefined = <I extends object>(
  obj: I,
): WithoutNullableKeys<I> | undefined => {
  const isValueTruthy = (value: unknown): boolean =>
    value !== null && value !== undefined && typeof value !== 'undefined';

  const isObjectEntriesAreTruthy = ([, value]: [string, unknown]): boolean =>
    typeof value === 'object' && value !== null
      ? Object.entries(value).every(isObjectEntriesAreTruthy)
      : isValueTruthy(value);

  const isObjectTruthy = (value: object): boolean =>
    Object.entries(value).every(isObjectEntriesAreTruthy);

  return isObjectTruthy(obj) ? (obj as WithoutNullableKeys<I>) : undefined;
};

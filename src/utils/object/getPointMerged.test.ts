import { Point } from '@/types/map';
import { getPointMerged } from './getPointMerged';

describe('getPointMerged', () => {
  const cases: [Partial<Point>, Point, Point][] = [
    [
      { x: 1, y: 1, z: 0 },
      { x: 0, y: 0, z: 0 },
      { x: 1, y: 1, z: 0 },
    ],
    [
      { x: 0, y: 0 },
      { x: 1, y: 0, z: 1 },
      { x: 0, y: 0, z: 1 },
    ],
    [{ x: 0 }, { x: 1, y: 1, z: 0 }, { x: 0, y: 1, z: 0 }],
  ];

  it.each(cases)('should return valid merged point', (primaryPoint, secondaryPoint, finalPoint) => {
    expect(getPointMerged(primaryPoint, secondaryPoint)).toStrictEqual(finalPoint);
  });
});

import { getTruthyObjectOrUndefined } from './getTruthyObjectOrUndefined';

describe('getTruthyObjectOrUndefined - util', () => {
  it('shoud return a truthy object if the object is truthy', () => {
    const objectAllValuesTruthy = {
      someKey: 1,
      otherKey: '',
      someOtherKey: {
        value: 'isTruthy',
      },
    };

    expect(getTruthyObjectOrUndefined(objectAllValuesTruthy)).toStrictEqual(objectAllValuesTruthy);
  });

  it('shoud return a truthy object if the object is empty', () => {
    const objectNoneValues = {};

    expect(getTruthyObjectOrUndefined(objectNoneValues)).toStrictEqual(objectNoneValues);
  });

  it('shoud return undefined if the object is partially truthy', () => {
    const objectSomeValuesTruthy = {
      someKey: undefined,
      otherKey: null,
      someOtherKey: {
        value: 'isTruthy',
      },
    };

    expect(getTruthyObjectOrUndefined(objectSomeValuesTruthy)).toBe(undefined);
  });

  it("shoud return undefined if the objects's nested objects is partially truthy", () => {
    const objectNestedValuesTruthy = {
      someKey: 1,
      otherKey: '',
      someOtherKey: {
        value: null,
      },
    };

    expect(getTruthyObjectOrUndefined(objectNestedValuesTruthy)).toBe(undefined);
  });
});

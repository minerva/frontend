import { Point } from '@/types/map';

export const getPointMerged = (primaryPoint: Partial<Point>, secondaryPoint: Point): Point => ({
  x: primaryPoint.x ?? secondaryPoint.x,
  y: primaryPoint.y ?? secondaryPoint.y,
  z: primaryPoint.z ?? secondaryPoint.z,
});

/* eslint-disable no-magic-numbers */
import { getErrorMessage } from './getErrorMessage';
import { mockAxiosError } from './getErrorMessage.test.utils';

describe('getErrorMessage function', () => {
  it('should return custom message if provided', () => {
    const error = new Error('Custom Error');
    const errorMessage = getErrorMessage({ error, message: 'This is a custom message' });
    expect(errorMessage).toBe('This is a custom message');
  });

  it('should return extracted Axios error message', () => {
    const error = mockAxiosError(401, 'Unauthorized');
    const errorMessage = getErrorMessage({ error });
    expect(errorMessage).toBe('Unauthorized');
  });

  it('should return error message from Error instance', () => {
    const error = new Error('Network Error');
    const errorMessage = getErrorMessage({ error });
    expect(errorMessage).toBe('Network Error');
  });

  it('should return default error message if error is of unknown type', () => {
    const errorMessage = getErrorMessage({ error: {} });
    expect(errorMessage).toBe('An unknown error occurred. Please try again later.');
  });

  it('should prepend prefix to error message', () => {
    const error = new Error('Server Error');
    const errorMessage = getErrorMessage({ error, prefix: 'Error occurred' });
    expect(errorMessage).toBe('Error occurred: Server Error');
  });
});

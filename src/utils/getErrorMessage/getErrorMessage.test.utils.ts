import { AxiosError } from 'axios';

type MockAxiosError = AxiosError<{ error: string; reason: string }>;

export const mockAxiosError = (status: number, reason: string | null): MockAxiosError =>
  ({
    isAxiosError: true,
    response: {
      status,
      data: {
        reason,
        error: reason,
      },
    },
  }) as MockAxiosError;

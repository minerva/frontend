/* eslint-disable no-magic-numbers */
import { mockAxiosError } from './getErrorMessage.test.utils';
import { extractAxiosErrorMessage } from './getErrorMessage.utils';

describe('extractAxiosErrorMessage', () => {
  it('should return the error message from Axios error response if exist', () => {
    const error = mockAxiosError(404, 'Not Found');
    expect(extractAxiosErrorMessage(error)).toBe('Not Found');
  });
  it('should return error message defined by response status if error response does not exist', () => {
    const error = mockAxiosError(500, null);
    expect(extractAxiosErrorMessage(error)).toBe(
      'Unexpected server error. Please try again later or contact support.',
    );
  });
  it('should return the default error message if status code is not defined in predefined error messages list and error response does not exist', () => {
    const error = mockAxiosError(418, null);
    expect(extractAxiosErrorMessage(error)).toBe(
      'An unknown error occurred. Please try again later.',
    );
  });
});

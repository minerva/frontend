import { HTTP_ERROR_MESSAGES } from './getErrorMessage.constants';

export type HttpStatuses = keyof typeof HTTP_ERROR_MESSAGES;

export const getUniqueArray = <T>(elements: T[]): T[] => [...new Set([...elements])];

/* eslint-disable @typescript-eslint/no-explicit-any */
import { groupBy } from './groupBy';

interface InputObject {
  title: string;
  value: number;
}

describe('groupBy - util', () => {
  const cases: [InputObject[], (value: InputObject) => string, { [key: string]: InputObject[] }][] =
    [
      [
        [
          {
            title: 'GROUP_1',
            value: 1,
          },
          {
            title: 'GROUP_1',
            value: 2,
          },
          {
            title: 'GROUP_1',
            value: 3,
          },
          {
            title: 'GROUP_2',
            value: 1,
          },
          {
            title: 'GROUP_3',
            value: 1,
          },
        ],
        (obj): string => obj.title,
        {
          GROUP_1: [
            { title: 'GROUP_1', value: 1 },
            { title: 'GROUP_1', value: 2 },
            { title: 'GROUP_1', value: 3 },
          ],
          GROUP_2: [{ title: 'GROUP_2', value: 1 }],
          GROUP_3: [{ title: 'GROUP_3', value: 1 }],
        },
      ],
      [
        [
          {
            title: '1',
            value: 1,
          },
          {
            title: '1',
            value: 2,
          },
          {
            title: '1',
            value: 3,
          },
          {
            title: '2',
            value: 1,
          },
          {
            title: '3',
            value: 1,
          },
        ],
        (obj): string => obj.value.toString(),
        {
          '1': [
            { title: '1', value: 1 },
            { title: '2', value: 1 },
            { title: '3', value: 1 },
          ],
          '2': [{ title: '1', value: 2 }],
          '3': [{ title: '1', value: 3 }],
        },
      ],
    ];

  it.each(cases)('should return valid data basing on predicate', (input, predicate, output) => {
    expect(groupBy(input as any[], predicate)).toStrictEqual(output);
  });
});

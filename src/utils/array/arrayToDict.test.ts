import arrayToDict from './arrayToDict';

describe('arrayToDict', () => {
  interface Person {
    id: number;
    name: string;
    age: number;
    isActive: boolean;
  }

  const people: Person[] = [
    { id: 1, name: 'John', age: 30, isActive: true },
    { id: 2, name: 'Anna', age: 25, isActive: false },
    { id: 3, name: 'Peter', age: 28, isActive: true },
  ];

  it('create dict with key "id" and value "name"', () => {
    const result = arrayToDict(people, 'id', 'name');
    expect(result).toEqual({
      1: 'John',
      2: 'Anna',
      3: 'Peter',
    });
  });

  it('create dict with key "name" and value "age"', () => {
    const result = arrayToDict(people, 'name', 'age');
    expect(result).toEqual({
      John: 30,
      Anna: 25,
      Peter: 28,
    });
  });

  it('handles duplicate keys, overwriting previous values', () => {
    const duplicateData = [
      { id: 1, name: 'John', age: 30, isActive: true },
      { id: 1, name: 'Anna', age: 25, isActive: false },
    ];
    const result = arrayToDict(duplicateData, 'id', 'name');
    expect(result).toEqual({
      1: 'Anna',
    });
  });
});

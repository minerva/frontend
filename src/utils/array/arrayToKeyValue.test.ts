/* eslint-disable no-magic-numbers */
import arrayToKeyValue from './arrayToKeyValue';

describe('arrayToKeyValue', () => {
  interface Person {
    id: number;
    name: string;
    age: number;
    isActive: boolean;
  }

  const people: Person[] = [
    { id: 1, name: 'John', age: 30, isActive: true },
    { id: 2, name: 'Anna', age: 25, isActive: false },
    { id: 3, name: 'Peter', age: 28, isActive: true },
  ];

  it('create dict with key "id" and value "name"', () => {
    const result = arrayToKeyValue(people, 'id');
    expect(result).toEqual({
      1: people[0],
      2: people[1],
      3: people[2],
    });
  });

  it('create dict with key "name" and value "age"', () => {
    const result = arrayToKeyValue(people, 'name');
    expect(result).toEqual({
      John: people[0],
      Anna: people[1],
      Peter: people[2],
    });
  });

  it('handles duplicate keys, overwriting previous values', () => {
    const duplicateData = [
      { id: 1, name: 'John', age: 30, isActive: true },
      { id: 1, name: 'Anna', age: 25, isActive: false },
    ];
    const result = arrayToKeyValue(duplicateData, 'id');
    expect(result).toEqual({
      1: duplicateData[1],
    });
  });
});

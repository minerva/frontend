import { z } from 'zod';
import { validateDataUsingZodSchema } from './validateDataUsingZodSchema';

const testObjectSchema = z.object({
  name: z.string(),
});

const validObject = { name: 'John' };
const invalidObject = { name: 1234 };

describe('validateDataUsingZodSchema - utils', () => {
  it('should return true for given cases', () => {
    expect(validateDataUsingZodSchema(validObject, testObjectSchema)).toBe(true);
  });

  it('should return false for given cases', () => {
    expect(validateDataUsingZodSchema(invalidObject, testObjectSchema)).toBe(false);
  });
});

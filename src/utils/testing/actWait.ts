// eslint-disable-next-line import/no-extraneous-dependencies
import { act } from '@testing-library/react';

const ZERO_SECONDS = 0;

export const actWait = async (ms: number = ZERO_SECONDS): Promise<void> => {
  await act(
    async (): Promise<void> =>
      new Promise(resolve => {
        setTimeout(resolve, ms);
      }),
  );
};

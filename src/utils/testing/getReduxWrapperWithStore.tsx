import { RootState, StoreType, middlewares, reducers } from '@/redux/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import { WebSocketEntityUpdatesProvider } from '@/utils/websocket-entity-updates/webSocketEntityUpdatesProvider';
import { MapInstanceContext, MapInstanceProvider } from '../context/mapInstanceContext';

interface WrapperProps {
  children: React.ReactNode;
}

export type InitialStoreState = Partial<RootState>;
export type ReduxComponentWrapper = ({ children }: WrapperProps) => JSX.Element;
export interface Options {
  mapInstanceContextValue?: MapInstanceContext;
}

export type GetReduxWrapperUsingSliceReducer = (
  initialState?: InitialStoreState,
  options?: Options,
) => {
  Wrapper: ReduxComponentWrapper;
  store: StoreType;
};

jest.mock('react-use-websocket', () => ({
  __esModule: true,
  default: jest.fn(() => ({
    sendMessage: jest.fn(),
    lastJsonMessage: null,
    readyState: 1,
  })),
  ReadyState: {
    CONNECTING: 0,
    OPEN: 1,
    CLOSING: 2,
    CLOSED: 3,
  },
}));

export const getReduxWrapperWithStore: GetReduxWrapperUsingSliceReducer = (
  preloadedState: InitialStoreState = {},
  options: Options = {},
) => {
  const testStore = configureStore({
    reducer: reducers,
    preloadedState,
    middleware: getDefaultMiddleware => getDefaultMiddleware().prepend(...middlewares),
  });

  const Wrapper = ({ children }: WrapperProps): JSX.Element => (
    <MapInstanceProvider initialValue={options.mapInstanceContextValue}>
      <Provider store={testStore}>
        <WebSocketEntityUpdatesProvider>{children}</WebSocketEntityUpdatesProvider>
      </Provider>
    </MapInstanceProvider>
  );

  return { Wrapper, store: testStore };
};

/* eslint-disable import/no-extraneous-dependencies */
import { AppDispatch, RootState, middlewares } from '@/redux/store';
import { Provider } from 'react-redux';
import configureStore, { MockStoreEnhanced } from 'redux-mock-store';
import thunk from 'redux-thunk';

interface WrapperProps {
  children: React.ReactNode;
}

export type InitialStoreState = Partial<RootState>;

type GetReduxStoreWithActionsListener = (initialState?: InitialStoreState) => {
  Wrapper: ({ children }: WrapperProps) => JSX.Element;
  store: MockStoreEnhanced<Partial<RootState>, AppDispatch>;
};

export const getReduxStoreWithActionsListener: GetReduxStoreWithActionsListener = (
  preloadedState: InitialStoreState = {},
) => {
  const testStore = configureStore<Partial<RootState>, AppDispatch>([thunk, ...middlewares])(
    preloadedState,
  );

  const Wrapper = ({ children }: WrapperProps): JSX.Element => (
    <Provider store={testStore}>{children}</Provider>
  );

  return { Wrapper, store: testStore };
};

import { Reducer } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '../createStoreInstanceUsingSliceReducer';

interface WrapperProps {
  children: React.ReactNode;
}

type GetReduxWrapperUsingSliceReducer = <StateType>(
  name: string,
  passedReducer: Reducer<StateType>,
) => {
  Wrapper: ({ children }: WrapperProps) => JSX.Element;
  store: ToolkitStoreWithSingleSlice<StateType>;
};

export const getReduxWrapperUsingSliceReducer: GetReduxWrapperUsingSliceReducer = (
  reducerName,
  reducerInstance,
) => {
  const store = createStoreInstanceUsingSliceReducer(reducerName, reducerInstance);

  const Wrapper = ({ children }: WrapperProps): JSX.Element => (
    <Provider store={store}>{children}</Provider>
  );

  return { Wrapper, store };
};

import { OverlayReactionCoords } from '@/types/OLrendering';
import { Line } from '@/types/models';
import { getOverlayReactionCoordsFromLine } from './getOverlayReactionCoords';

const LINE_DATA_BASE: Line = {
  id: 66141,
  width: 1,
  color: {
    alpha: 255,
    rgb: -16777216,
  },
  z: 0,
  segments: [
    {
      x1: 4457.375604345491,
      y1: 7111.933125147456,
      x2: 4462.61826820353,
      y2: 7105.89040426431,
    },
  ],
  startArrow: {
    arrowType: 'NONE',
    angle: 2.748893571891069,
    lineType: 'SOLID',
    length: 15,
  },
  endArrow: {
    arrowType: 'NONE',
    angle: 2.748893571891069,
    lineType: 'SOLID',
    length: 15,
  },
  lineType: 'SOLID',
};

describe('getOverlayReactionCoords - util', () => {
  const cases: [Line, OverlayReactionCoords[]][] = [
    [
      {
        ...LINE_DATA_BASE,
        segments: [
          {
            x1: 10,
            y1: 10,
            x2: 100,
            y2: 100,
          },
        ],
      },
      [{ height: -90, id: 66141, width: 90, x1: 10, x2: 100, y1: 10, y2: 100 }],
    ],
    [
      {
        ...LINE_DATA_BASE,
        segments: [
          {
            x1: 10,
            y1: 10,
            x2: 2000,
            y2: 0,
          },
        ],
      },
      [{ height: 10, id: 66141, width: 1990, x1: 10, x2: 2000, y1: 10, y2: 0 }],
    ],
    [
      {
        ...LINE_DATA_BASE,
        segments: [
          {
            x1: 0,
            y1: 0,
            x2: 0,
            y2: 0,
          },
        ],
      },
      [{ height: 0, id: 66141, width: 0, x1: 0, x2: 0, y1: 0, y2: 0 }],
    ],
  ];

  it.each(cases)('should return valid result', (line, result) => {
    expect(getOverlayReactionCoordsFromLine(line)).toStrictEqual(result);
  });
});

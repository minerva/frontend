import {
  overlayElementWithBioEntityFixture,
  overlayElementWithReactionFixture,
} from '@/models/fixtures/overlayBioEntityFixture';
import { isBioEntity, isReaction } from './overlaysElementsTypeGuards';

describe('overlaysElementsTypeGruards - utils', () => {
  describe('isReaction', () => {
    describe('when is reaction', () => {
      it('should return true', () => {
        expect(isReaction(overlayElementWithReactionFixture)).toBe(true);
      });
    });

    describe('when is bioentity', () => {
      it('should return false', () => {
        expect(isReaction(overlayElementWithBioEntityFixture)).toBe(false);
      });
    });
  });

  describe('isBioEntity', () => {
    describe('when is reaction', () => {
      it('should return false', () => {
        expect(isBioEntity(overlayElementWithReactionFixture)).toBe(false);
      });
    });

    describe('when is bioentity', () => {
      it('should return true', () => {
        expect(isBioEntity(overlayElementWithBioEntityFixture)).toBe(true);
      });
    });
  });
});

import { MAP_DATA_INITIAL_STATE, OPENED_MAPS_INITIAL_STATE } from '@/redux/map/map.constants';
import { Loading } from '@/types/loadingState';
import { renderHook, waitFor } from '@testing-library/react';
import mockRouter from 'next-router-mock';
import { getReduxWrapperWithStore } from '../testing/getReduxWrapperWithStore';
import { useReduxBusQueryManager } from './useReduxBusQueryManager';

describe('useReduxBusQueryManager - util', () => {
  describe('on init when data is NOT loaded', () => {
    const { Wrapper } = getReduxWrapperWithStore();

    jest.mock('./../../redux/root/init.selectors', () => ({
      initDataAndMapLoadingFinished: jest.fn().mockImplementation(() => false),
    }));

    it('should not update query', () => {
      const routerReplaceSpy = jest.spyOn(mockRouter, 'replace');
      renderHook(() => useReduxBusQueryManager(), { wrapper: Wrapper });
      expect(routerReplaceSpy).not.toHaveBeenCalled();
    });
  });

  describe('on init when data is loaded', () => {
    const loadedDataMock = {
      data: [],
      loading: 'succeeded' as Loading,
      error: { name: '', message: '' },
      addOverlay: {
        loading: 'idle' as Loading,
        error: { name: '', message: '' },
      },
      userOverlays: {
        data: [],
        loading: 'idle' as Loading,
        error: { name: '', message: '' },
      },
      updateOverlays: {
        loading: 'idle' as Loading,
        error: { name: '', message: '' },
      },
      removeOverlay: {
        loading: 'idle' as Loading,
        error: { name: '', message: '' },
      },
    };

    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        ...loadedDataMock,
        data: undefined,
        projectId: '',
      },
      map: {
        ...loadedDataMock,
        data: {
          ...MAP_DATA_INITIAL_STATE,
          modelId: 54,
          backgroundId: 13,
          position: {
            initial: {
              x: 0,
              y: 0,
              z: 0,
            },
            last: {
              x: 1245,
              y: 6422,
              z: 3,
            },
          },
        },
        openedMaps: OPENED_MAPS_INITIAL_STATE,
      },
      models: loadedDataMock,
      overlays: loadedDataMock,
    });

    it('should update query', async () => {
      const routerReplaceSpy = jest.spyOn(mockRouter, 'replace');
      renderHook(() => useReduxBusQueryManager(), { wrapper: Wrapper });
      await waitFor(() => expect(routerReplaceSpy).toHaveBeenCalled());
    });

    it('should update query params to valid ones', async () => {
      renderHook(() => useReduxBusQueryManager(), { wrapper: Wrapper });
      await waitFor(() =>
        expect(mockRouter.query).toMatchObject({
          backgroundId: 13,
          modelId: 54,
          x: 1245,
          y: 6422,
          z: 3,
        }),
      );
    });
  });
});

export const boundNumber = (value: number, minVal?: number, maxVal?: number): number => {
  const valueBoundedMax = Math.max(value, minVal || value);
  return Math.min(valueBoundedMax, maxVal || value);
};

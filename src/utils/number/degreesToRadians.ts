const HALF_CIRCLE_DEGREES = 180;

export const degreesToRadians = (deg: number): number => {
  return deg * (Math.PI / HALF_CIRCLE_DEGREES);
};

/* eslint-disable no-magic-numbers */
import { degreesToRadians } from './degreesToRadians';

describe('degreesToRadians - util', () => {
  it('coverts positive degree to close positive radians', () => {
    expect(degreesToRadians(180)).toBeCloseTo(3.14159);
  });

  it('coverts negative degree to close negative radians', () => {
    expect(degreesToRadians(-203)).toBeCloseTo(-3.54302);
  });

  it('coverts zero degree to zero radians', () => {
    expect(degreesToRadians(0)).toBe(0);
  });
});

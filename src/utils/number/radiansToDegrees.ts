const HALF_CIRCLE_DEGREES = 180;

export const radiansToDegrees = (rad: number): number => rad / (Math.PI / HALF_CIRCLE_DEGREES);

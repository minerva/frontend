/* eslint-disable no-magic-numbers */
import { radiansToDegrees } from './radiansToDegrees';

describe('radiansToDegrees - util', () => {
  it('coverts positive radian to close positive degrees', () => {
    expect(radiansToDegrees(10)).toBeCloseTo(572.958);
  });

  it('coverts negative radian to close negative degrees', () => {
    expect(radiansToDegrees(-6.45772)).toBeCloseTo(-370);
  });

  it('coverts zero radian to zero degrees', () => {
    expect(radiansToDegrees(0)).toBe(0);
  });
});

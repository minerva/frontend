/* eslint-disable no-magic-numbers */
import { boundNumber } from './boundNumber';

describe('boundNumber - util', () => {
  const cases = [
    [1, 0, 2, 1],
    [1, 2, 2, 2],
    [2, 0, 1, 1],
  ];

  it.each(cases)(
    'should return valid bounded number | v = %s, minMax = (%s, %s), final = %s',
    (value, minVal, maxVal, finalVal) => {
      expect(boundNumber(value, minVal, maxVal)).toEqual(finalVal);
    },
  );
});

/* eslint-disable no-magic-numbers */
import { roundToTwoDigits } from './roundToTwoDigits';

describe('roundToTwoDiggits', () => {
  it('should round a positive number with more than two decimal places to two decimal places', () => {
    expect(roundToTwoDigits(3.14159265359)).toBe(3.14);
    expect(roundToTwoDigits(2.71828182845)).toBe(2.72);
    expect(roundToTwoDigits(1.23456789)).toBe(1.23);
  });

  it('should round a negative number with more than two decimal places to two decimal places', () => {
    expect(roundToTwoDigits(-3.14159265359)).toBe(-3.14);
    expect(roundToTwoDigits(-2.71828182845)).toBe(-2.72);
    expect(roundToTwoDigits(-1.23456789)).toBe(-1.23);
  });

  it('should round a number with exactly two decimal places to two decimal places', () => {
    expect(roundToTwoDigits(3.14)).toBe(3.14);
    expect(roundToTwoDigits(2.72)).toBe(2.72);
    expect(roundToTwoDigits(1.23)).toBe(1.23);
  });

  it('should round a number with less than two decimal places to two decimal places', () => {
    expect(roundToTwoDigits(3)).toBe(3.0);
    expect(roundToTwoDigits(2.7)).toBe(2.7);
    expect(roundToTwoDigits(1.2)).toBe(1.2);
  });

  it('should round zero to two decimal places', () => {
    expect(roundToTwoDigits(0)).toBe(0);
  });
});

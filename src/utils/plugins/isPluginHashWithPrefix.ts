import { PLUGIN_HASH_PREFIX_SEPARATOR } from '@/constants/plugins';

export const isPluginHashWithPrefix = (hash: string): boolean => {
  return hash.includes(PLUGIN_HASH_PREFIX_SEPARATOR);
};

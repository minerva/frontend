import { getPluginHashWithoutPrefix } from './getPluginHashWithoutPrefix';

describe('getPluginHashWithoutPrefix', () => {
  it('should return hash without prefix if hash contains the prefix separator', () => {
    const hash = 'prefix-hash';
    const expectedResult = 'hash';
    const result = getPluginHashWithoutPrefix(hash);
    expect(result).toBe(expectedResult);
  });

  it('should return the same hash if hash does not contain the prefix separator', () => {
    const hash = 'hash';
    const result = getPluginHashWithoutPrefix(hash);
    expect(result).toBe(hash);
  });
});

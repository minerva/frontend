import { isPluginHashWithPrefix } from './isPluginHashWithPrefix';

describe('isPluginHashWithPrefix', () => {
  it('should return true if the hash contains the prefix separator', () => {
    const hash = 'someHashprefix-hash';
    const result = isPluginHashWithPrefix(hash);
    expect(result).toBe(true);
  });

  it('should return false if the hash does not contain the prefix separator', () => {
    const hash = 'someHash';
    const result = isPluginHashWithPrefix(hash);
    expect(result).toBe(false);
  });
});

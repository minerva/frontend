import {
  ENTITY_TYPES,
  ENTITY_OPERATION_TYPES,
  ACKNOWLEDGE_OPERATION_TYPES,
} from '@/utils/websocket-entity-updates/webSocketEntityUpdates.constants';

export type EntityType = (typeof ENTITY_TYPES)[keyof typeof ENTITY_TYPES];
export type EntityOperationType =
  (typeof ENTITY_OPERATION_TYPES)[keyof typeof ENTITY_OPERATION_TYPES];
export type AcknowledgeOperationType =
  (typeof ACKNOWLEDGE_OPERATION_TYPES)[keyof typeof ACKNOWLEDGE_OPERATION_TYPES];

export interface WebSocketEntityUpdateInterface {
  entityId: number;
  entityType: EntityType;
  entityVersion: number;
  mapId: number;
  projectId: string;
  layerId: number;
  type: EntityOperationType;
}

export interface WebSocketAcknowledgeInterface {
  commandId: string | null;
  message: string;
  type: AcknowledgeOperationType;
}

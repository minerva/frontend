import mapReducer, { setMapData } from '@/redux/map/map.slice';
/* eslint-disable no-magic-numbers */
import { act, renderHook } from '@testing-library/react';
import { getReduxWrapperUsingSliceReducer } from '../testing/getReduxWrapperUsingSliceReducer';
import { usePointToProjection } from './usePointToProjection';

describe('usePointToProjection - util', () => {
  const { Wrapper, store } = getReduxWrapperUsingSliceReducer('map', mapReducer);

  describe('when mapSize arg is undefined in redux', () => {
    const validPoint = {
      x: 0,
      y: 0,
    };

    const undefinedMapSize = undefined;

    it('should return fallback value on function call', () => {
      act(() => {
        store.dispatch(
          setMapData({
            size: undefinedMapSize,
          }),
        );
      });

      const {
        result: { current: pointToProjection },
      } = renderHook(usePointToProjection, { wrapper: Wrapper });
      expect(pointToProjection(validPoint)).toStrictEqual([0, -0]);
    });
  });

  describe('when mapSize arg is invalid in redux', () => {
    const validPoint = {
      x: 0,
      y: 0,
    };

    // all numbers should be non-negative
    const invalidMapSize = {
      width: -256 * 10,
      height: -256 * 10,
      tileSize: -256,
      minZoom: -1,
      maxZoom: -10,
    };

    it('should return fallback value on function call', () => {
      act(() => {
        store.dispatch(
          setMapData({
            size: invalidMapSize,
          }),
        );
      });

      const {
        result: { current: pointToProjection },
      } = renderHook(usePointToProjection, { wrapper: Wrapper });
      expect(pointToProjection(validPoint)).toStrictEqual([0, -0]);
    });
  });

  describe('when all args are valid in redux', () => {
    const validPoint = {
      x: 256 * 100,
      y: 256 * 100,
    };

    const validMapSize = {
      width: 256 * 10,
      height: 256 * 10,
      tileSize: 256,
      minZoom: 1,
      maxZoom: 10,
    };

    const results = [380712659, -238107693];

    it('should return valid lat lng value on function call', () => {
      act(() => {
        store.dispatch(
          setMapData({
            size: validMapSize,
          }),
        );
      });

      const {
        result: { current: pointToProjection },
      } = renderHook(usePointToProjection, { wrapper: Wrapper });
      const [x, y] = pointToProjection(validPoint);

      expect(x).toBeCloseTo(results[0]);
      expect(y).toBeCloseTo(results[1]);
    });
  });
});

/* eslint-disable no-magic-numbers */
import { ONE } from '@/constants/common';
import { MAP_DATA_INITIAL_STATE } from '@/redux/map/map.constants';
import { renderHook } from '@testing-library/react';
import { Map } from 'ol';
import { Coordinate } from 'ol/coordinate';
import { getReduxWrapperWithStore } from '../testing/getReduxWrapperWithStore';
import { useSetBounds } from './useSetBounds';

describe('useSetBounds - hook', () => {
  const coordinates: Coordinate[] = [
    [128, 128],
    [192, 192],
  ];

  describe('when mapInstance is not set', () => {
    it('setBounds should return void', () => {
      const { Wrapper } = getReduxWrapperWithStore(
        {
          map: {
            data: {
              ...MAP_DATA_INITIAL_STATE,
              size: {
                width: 256,
                height: 256,
                tileSize: 256,
                minZoom: 1,
                maxZoom: 1,
              },
            },
            loading: 'idle',
            error: {
              name: '',
              message: '',
            },
            openedMaps: [],
          },
        },
        {
          mapInstanceContextValue: {
            mapInstance: undefined,
            handleSetMapInstance: () => {},
          },
        },
      );

      const {
        result: { current: setBounds },
      } = renderHook(() => useSetBounds(), { wrapper: Wrapper });

      expect(setBounds(coordinates)).toBe(undefined);
    });
  });

  describe('when mapInstance is set', () => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const view = mapInstance.getView();
    const getViewSpy = jest.spyOn(mapInstance, 'getView');
    const fitSpy = jest.spyOn(view, 'fit');

    it('setBounds should set  return void', () => {
      const { Wrapper } = getReduxWrapperWithStore(
        {
          map: {
            data: {
              ...MAP_DATA_INITIAL_STATE,
              size: {
                width: 256,
                height: 256,
                tileSize: 256,
                minZoom: 1,
                maxZoom: 1,
              },
            },
            loading: 'idle',
            error: {
              name: '',
              message: '',
            },
            openedMaps: [],
          },
        },
        {
          mapInstanceContextValue: {
            mapInstance,
            handleSetMapInstance: () => {},
          },
        },
      );

      const {
        result: { current: setBounds },
      } = renderHook(() => useSetBounds(), { wrapper: Wrapper });

      expect(setBounds(coordinates)).toStrictEqual({
        extent: [128, 128, 192, 192],
        options: { maxZoom: 1, padding: [128, 128, 128, 128], size: undefined },
        // size is real size on the screen, so it'll be undefined in the jest
      });
      expect(getViewSpy).toHaveBeenCalledTimes(ONE);
      expect(fitSpy).toHaveBeenCalledWith([128, 128, 192, 192], {
        maxZoom: 1,
        padding: [128, 128, 128, 128],
        size: undefined,
      });
    });
  });
});

/* eslint-disable no-magic-numbers */
import { LatLng, Point } from '@/types/map';
import { Options, latLngToPoint } from './latLngToPoint';

describe('latLngToPoint - util', () => {
  describe('on map with default tileSize = 256', () => {
    const mapSize = {
      width: 5170,
      height: 1535.1097689075634,
      tileSize: 256,
      minZoom: 2,
      maxZoom: 7,
    };

    const cases: [LatLng, Point, Options][] = [
      [
        [84.480312233386, -159.90463877126223],
        {
          x: 1154.3668616952698,
          y: 359.7865610953942,
        },
        {
          rounded: false,
        },
      ],
      [
        [84.20644283660516, -153.43406886300772],
        {
          x: 1526,
          y: 519,
        },
        {
          rounded: true,
        },
      ],
    ];

    it.each(cases)('should return valid point', (latLng, point, options) =>
      expect(latLngToPoint(latLng, mapSize, options)).toStrictEqual(point),
    );
  });

  describe('on map with non-default tileSize = 128', () => {
    const mapSize = {
      width: 26779.25,
      height: 13503,
      tileSize: 256,
      minZoom: 2,
      maxZoom: 9,
    };

    const cases: [LatLng, Point, Options][] = [
      [
        [843.480312233386, -84.90463877126223],
        {
          x: 28295.360579829732,
          y: 33077.1123303386,
        },
        {
          rounded: false,
        },
      ],
      [
        [32443.4536345435, -5546654.543645645],
        {
          x: -1650338094,
          y: 39175,
        },
        {
          rounded: true,
        },
      ],
    ];

    it.each(cases)('should return valid point', (latLng, point, options) =>
      expect(latLngToPoint(latLng, mapSize, options)).toStrictEqual(point),
    );
  });
});

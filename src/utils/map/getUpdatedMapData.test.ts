import { DEFAULT_ZOOM } from '@/constants/map';
import { singleModelFixture } from '@/models/fixtures/modelsFixture';
import { getUpdatedMapData } from './getUpdatedMapData';

const HALF = 2;

describe('getUpdatedMapData - util', () => {
  describe('when model does not have default values', () => {
    const model = {
      ...singleModelFixture,
      defaultCenterX: null,
      defaultCenterY: null,
      defaultZoomLevel: null,
    };

    it('should return correct value', () => {
      const result = {
        modelId: model.id,
        size: {
          width: model.width,
          height: model.height,
          tileSize: model.tileSize,
          minZoom: model.minZoom,
          maxZoom: model.maxZoom,
        },
        position: {
          initial: {
            x: model.width / HALF,
            y: model.height / HALF,
            z: DEFAULT_ZOOM,
          },
          last: {
            x: model.width / HALF,
            y: model.height / HALF,
            z: DEFAULT_ZOOM,
          },
        },
      };

      expect(getUpdatedMapData({ model })).toMatchObject(result);
    });
  });

  describe('when model has default falsy values', () => {
    const model = {
      ...singleModelFixture,
      defaultCenterX: 0,
      defaultCenterY: 0,
      defaultZoomLevel: null,
    };

    it('should return correct value', () => {
      const result = {
        modelId: model.id,
        size: {
          width: model.width,
          height: model.height,
          tileSize: model.tileSize,
          minZoom: model.minZoom,
          maxZoom: model.maxZoom,
        },
        position: {
          initial: {
            x: 0,
            y: 0,
            z: DEFAULT_ZOOM,
          },
          last: {
            x: 0,
            y: 0,
            z: DEFAULT_ZOOM,
          },
        },
      };

      expect(getUpdatedMapData({ model })).toMatchObject(result);
    });
  });

  describe('when model has default truthy values', () => {
    const model = {
      ...singleModelFixture,
      defaultCenterX: 10,
      defaultCenterY: 10,
      defaultZoomLevel: 1,
    };

    it('should return correct value', () => {
      const result = {
        modelId: model.id,
        size: {
          width: model.width,
          height: model.height,
          tileSize: model.tileSize,
          minZoom: model.minZoom,
          maxZoom: model.maxZoom,
        },
        position: {
          initial: {
            x: 10,
            y: 10,
            z: 1,
          },
          last: {
            x: 10,
            y: 10,
            z: 1,
          },
        },
      };

      expect(getUpdatedMapData({ model })).toMatchObject(result);
    });
  });
});

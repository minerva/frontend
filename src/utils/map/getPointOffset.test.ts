/* eslint-disable no-magic-numbers */
import { ZodError } from 'zod';
import { getPointOffset } from './getPointOffset';

describe('getPointOffset - util', () => {
  describe('when all args are valid', () => {
    const validPoint = {
      x: 256,
      y: 256 * 2,
    };

    const validMapSize = {
      width: 256 * 10,
      height: 256 * 10,
      tileSize: 256,
      minZoom: 1,
      maxZoom: 10,
    };

    const results = {
      x: -102.4,
      y: -76.8,
    };

    it('should return valid point origin and shifted values', () => {
      expect(getPointOffset(validPoint, validMapSize)).toMatchObject(results);
    });

    it('should not throw error', () => {
      expect(() => getPointOffset(validPoint, validMapSize)).not.toThrow(ZodError);
    });
  });

  describe('when mapSize arg is invalid', () => {
    const validPoint = {
      x: 256,
      y: 256,
    };

    // all numbers should be non-negative
    const invalidMapSize = {
      width: -256 * 10,
      height: -256 * 10,
      tileSize: -256,
      minZoom: -1,
      maxZoom: -10,
    };

    it('should throw error', () => {
      expect(() => getPointOffset(validPoint, invalidMapSize)).toThrow(ZodError);
    });
  });
});

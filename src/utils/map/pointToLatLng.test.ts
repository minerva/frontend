/* eslint-disable no-magic-numbers */
import { LATLNG_FALLBACK } from '@/constants/map';
import { pointToLngLat } from './pointToLatLng';

describe('pointToLatLng - util', () => {
  describe('when mapSize arg is undefined', () => {
    const validPoint = {
      x: 0,
      y: 0,
    };

    const mapSizeUndefined = undefined;

    it('should return fallback value', () => {
      expect(pointToLngLat(validPoint, mapSizeUndefined)).toBe(LATLNG_FALLBACK);
    });
  });

  describe('when mapSize arg is invalid', () => {
    const validPoint = {
      x: 0,
      y: 0,
    };

    // all numbers should be non-negative
    const invalidMapSize = {
      width: -256 * 10,
      height: -256 * 10,
      tileSize: -256,
      minZoom: -1,
      maxZoom: -10,
    };

    it('should return fallback value', () => {
      const logger = jest.spyOn(console, 'error').mockImplementation(() => {});

      expect(pointToLngLat(validPoint, invalidMapSize)).toBe(LATLNG_FALLBACK);
      // TODO: need to rething way of handling parsing errors, for now let's leave it to console.log
      // eslint-disable-next-line no-console
      expect(logger).toBeCalledTimes(1);
    });
  });

  describe('when all args are valid', () => {
    const validPoint = {
      x: -256 * 5,
      y: 256 * 5,
    };

    const validMapSize = {
      width: 256 * 10,
      height: 256 * 10,
      tileSize: 256,
      minZoom: 1,
      maxZoom: 10,
    };

    const results = [-360, 0];

    it('should return valid lat lng value', () => {
      expect(pointToLngLat(validPoint, validMapSize)).toStrictEqual(results);
    });
  });
});

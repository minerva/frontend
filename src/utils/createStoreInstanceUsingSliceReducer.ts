import { AnyAction, Reducer, ThunkMiddleware, configureStore } from '@reduxjs/toolkit';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';

export type ToolkitStoreWithSingleSlice<StateType> = ToolkitStore<
  { [name: string]: StateType },
  AnyAction,
  [
    ThunkMiddleware<
      {
        [name: string]: StateType;
      },
      AnyAction
    >,
  ]
>;

export type CreateStoreInstanceUsingSliceReducer = <StateType>(
  name: string,
  passedReducer: Reducer<StateType>,
) => ToolkitStoreWithSingleSlice<StateType>;

export const createStoreInstanceUsingSliceReducer: CreateStoreInstanceUsingSliceReducer = (
  name,
  passedReducer,
) =>
  configureStore({
    reducer: {
      [name]: passedReducer,
    },
  });

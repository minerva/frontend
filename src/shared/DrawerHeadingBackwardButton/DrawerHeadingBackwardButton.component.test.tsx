import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { drawerSearchStepOneFixture } from '@/redux/drawer/drawerFixture';
import { DrawerHeadingBackwardButton } from './DrawerHeadingBackwardButton.component';

const backwardFunction = jest.fn();

const renderComponent = (
  children: React.ReactNode,
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <DrawerHeadingBackwardButton backwardFunction={backwardFunction}>
          {children}
        </DrawerHeadingBackwardButton>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DrawerHeadingBackwardButton - component', () => {
  beforeEach(() => {
    backwardFunction.mockReset();
  });

  it('should render passed values', () => {
    renderComponent('Title');

    expect(screen.getByRole('back-button')).toBeInTheDocument();
    expect(screen.getByText('Title')).toBeInTheDocument();
    expect(screen.getByRole('close-drawer-button')).toBeInTheDocument();
  });

  it('should call backward function on back button click', () => {
    renderComponent('Title');

    const backButton = screen.getByRole('back-button');
    backButton.click();

    expect(backwardFunction).toBeCalled();
  });

  it('should call class drawer on close button click', () => {
    const { store } = renderComponent('Title', {
      drawer: {
        ...drawerSearchStepOneFixture,
      },
    });
    expect(store.getState().drawer.isOpen).toBe(true);

    const closeButton = screen.getByRole('close-drawer-button');
    closeButton.click();

    expect(store.getState().drawer.isOpen).toBe(false);
  });
});

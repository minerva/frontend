/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Textarea } from './Textarea.component';

describe('Textarea - Component', () => {
  it('should render with proper testid', () => {
    render(<Textarea data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toBeInTheDocument();
  });

  it('should apply the default style variant when none is provided', () => {
    render(<Textarea data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toHaveClass('bg-cultured');
  });

  it('should apply the specified style variant', () => {
    render(<Textarea styleVariant="primary" data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toHaveClass('bg-cultured');
  });

  it('should merge custom class with style variant classes', () => {
    render(<Textarea className="text-red-500" styleVariant="primary" data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toHaveClass('text-red-500');
    expect(textareaElement).toHaveClass('bg-cultured');
  });

  it('should handle onChange event', () => {
    const handleChange = jest.fn();
    render(<Textarea onChange={handleChange} data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    fireEvent.change(textareaElement, { target: { value: 'Hello, World!' } });
    expect(handleChange).toHaveBeenCalledTimes(1);
  });

  it('should render with a placeholder', () => {
    const placeholderText = 'Type here...';
    render(<Textarea placeholder={placeholderText} data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toHaveAttribute('placeholder', placeholderText);
  });

  it('should render with a default value', () => {
    const defaultValue = 'Initial value';
    render(<Textarea defaultValue={defaultValue} data-testid="textarea" />);
    const textareaElement = screen.getByTestId('textarea');
    expect(textareaElement).toHaveValue(defaultValue);
  });
});

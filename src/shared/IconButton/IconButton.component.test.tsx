import { screen, render, RenderResult } from '@testing-library/react';
import { IconButton, IconButtonProps } from './IconButton.component';

const renderComponent = (props: IconButtonProps): RenderResult => render(<IconButton {...props} />);

describe('IconButton - component', () => {
  it('should render IconButton', () => {
    renderComponent({ icon: 'info' });

    expect(screen.getByTestId('icon-button')).toBeInTheDocument();
  });

  it('should render IconButton with plugin icon', () => {
    renderComponent({ icon: 'plugin' });

    expect(screen.getByTestId('icon-button').firstChild).toHaveClass('stroke-font-400');
  });

  it('should render IconButton as active', () => {
    renderComponent({ icon: 'info', isActive: true });

    expect(screen.getByTestId('icon-button')).toHaveClass('bg-white-pearl');
    expect(screen.getByTestId('icon-button').firstChild).toHaveClass('fill-primary-500');
  });
});

import { render, screen } from '@testing-library/react';
import { LinkButton } from './LinkButton.component';

describe('LinkButton', () => {
  it('renders without crashing', () => {
    render(<LinkButton>Test</LinkButton>);
    expect(screen.getByText('Test')).toBeInTheDocument();
  });

  it('applies the primary variant by default', () => {
    render(<LinkButton>Test</LinkButton>);
    const button = screen.getByText('Test');
    expect(button).toHaveClass(
      'bg-primary-500 text-white-pearl hover:bg-primary-600 active:bg-primary-700 disabled:bg-greyscale-700',
    );
  });

  it('applies additional classes passed in', () => {
    // eslint-disable-next-line tailwindcss/no-custom-classname
    render(<LinkButton className="extra-class">Test</LinkButton>);
    const button = screen.getByText('Test');
    expect(button).toHaveClass('extra-class');
  });

  it('passes through additional props to the anchor element', () => {
    render(<LinkButton data-testid="my-button">Test</LinkButton>);
    const button = screen.getByTestId('my-button');
    expect(button).toBeInTheDocument();
  });
});

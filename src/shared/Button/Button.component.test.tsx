import { screen, render, RenderResult } from '@testing-library/react';
import { Button } from './Button.component';

const renderComponent = (): RenderResult => render(<Button />);

describe('Button - component ', () => {
  it('should render name Button', () => {
    renderComponent();

    expect(screen.getByText('Button')).toBeInTheDocument();
  });
});

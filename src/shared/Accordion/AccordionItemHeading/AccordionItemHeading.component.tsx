import { AccordionItemHeading as AIH } from 'react-accessible-accordion';

interface AccordionItemHeadingProps {
  children: React.ReactNode;
}

export const AccordionItemHeading = ({ children }: AccordionItemHeadingProps): JSX.Element => (
  <AIH className="py-4">{children}</AIH>
);

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Autocomplete } from './Autocomplete.component';

interface Option {
  id: number;
  name: string;
}

describe('Autocomplete', () => {
  const options: Option[] = [
    { id: 1, name: 'Option 1' },
    { id: 2, name: 'Option 2' },
    { id: 3, name: 'Option 3' },
  ];

  it('renders the component with placeholder', () => {
    render(
      <Autocomplete
        options={options}
        valueKey="id"
        labelKey="name"
        placeholder="Select an option"
        onChange={() => {}}
      />,
    );

    const placeholder = screen.getByText('Select an option');
    expect(placeholder).toBeInTheDocument();
  });

  it('displays options and handles selection', () => {
    const handleChange = jest.fn();

    render(
      <Autocomplete
        options={options}
        valueKey="id"
        labelKey="name"
        placeholder="Select an option"
        onChange={handleChange}
      />,
    );

    const dropdown = screen.getByTestId('autocomplete');
    if (!dropdown.firstChild) {
      throw new Error('Dropdown does not have a firstChild');
    }
    fireEvent.keyDown(dropdown.firstChild, { key: 'ArrowDown' });

    const option1 = screen.getByText('Option 1');
    const option2 = screen.getByText('Option 2');
    expect(option1).toBeInTheDocument();
    expect(option2).toBeInTheDocument();

    fireEvent.click(option1);

    expect(handleChange).toHaveBeenCalledWith({ id: 1, name: 'Option 1' });
  });
});

interface MoveFrontIconProps {
  className?: string;
}

export const MoveFrontIcon = ({ className }: MoveFrontIconProps): JSX.Element => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 100 100"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    className={className}
  >
    <rect x="4" y="4" width="64" height="64" rx="10" ry="10" strokeWidth="8" />
    <rect
      x="30"
      y="30"
      width="68"
      height="68"
      rx="12"
      ry="12"
      strokeWidth="16"
      stroke="white"
      fill="none"
    />
    <rect x="32" y="32" width="64" height="64" rx="10" ry="10" strokeWidth="8" fill="white" />
  </svg>
);

interface LayersIconProps {
  className?: string;
}

export const LayersIcon = ({ className }: LayersIconProps): JSX.Element => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 22 22"
    fill="none"
    className={className}
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12 4L4 8.5L12 13L20 8.5L12 4Z"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
      fill="none"
    />
    <path
      d="M4 12.5L12 17L20 12.5"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
      fill="none"
    />
    <path
      d="M4 16.5L12 21L20 16.5"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
      fill="none"
    />
  </svg>
);

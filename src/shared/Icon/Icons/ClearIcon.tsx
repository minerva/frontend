interface ClearIconProps {
  className?: string;
}

export const ClearIcon = ({ className, ...rest }: ClearIconProps): JSX.Element => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    className={className}
    xmlns="http://www.w3.org/2000/svg"
    {...rest}
  >
    <g clipPath="url(#clip0_4431_10860)">
      <path
        d="M12 23.7279L5.63604 17.364C2.12132 13.8492 2.12132 8.15076 5.63604 4.63604C9.15076 1.12132 14.8492 1.12132 18.364 4.63604C21.8787 8.15076 21.8787 13.8492 18.364 17.364L12 23.7279ZM16.9497 15.9497C19.6834 13.2161 19.6834 8.78392 16.9497 6.05025C14.2161 3.31658 9.78392 3.31658 7.05025 6.05025C4.31658 8.78392 4.31658 13.2161 7.05025 15.9497L12 20.8995L16.9497 15.9497ZM12 13C10.8954 13 10 12.1046 10 11C10 9.89543 10.8954 9 12 9C13.1046 9 14 9.89543 14 11C14 12.1046 13.1046 13 12 13Z"
        fill="currentColor"
      />
      <rect x="13" y="-2" width="12" height="12" rx="6" fill="white" />
      <path
        d="M18.9999 3.2952L21.4748 0.820312L22.1819 1.52742L19.707 4.0023L22.1819 6.47715L21.4748 7.18425L18.9999 4.7094L16.525 7.18425L15.8179 6.47715L18.2928 4.0023L15.8179 1.52742L16.525 0.820312L18.9999 3.2952Z"
        fill="currentColor"
      />
    </g>
    <defs>
      <clipPath id="clip0_4431_10860">
        <rect width="24" height="24" fill="white" />
      </clipPath>
    </defs>
  </svg>
);

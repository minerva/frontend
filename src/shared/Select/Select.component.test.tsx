/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Select } from '.';

describe('Select Component', () => {
  const mockOptions = [
    { id: 1, name: 'Option 1' },
    { id: 2, name: 'Option 2' },
    { id: 3, name: 'Option 3' },
  ];

  const mockOnChange = jest.fn();

  it('renders the Select component', () => {
    render(<Select options={mockOptions} selectedId={1} onChange={mockOnChange} />);
    expect(screen.getByTestId('select-component')).toBeInTheDocument();
  });

  it('displays the selected option name', () => {
    render(<Select options={mockOptions} selectedId={1} onChange={mockOnChange} />);
    expect(screen.getByText('Option 1')).toBeInTheDocument();
  });

  it('opens the dropdown when clicked', () => {
    render(<Select options={mockOptions} selectedId={1} onChange={mockOnChange} />);
    const toggleButton = screen.getByTestId('dropdown-button-name');

    fireEvent.click(toggleButton);
    expect(screen.getByRole('listbox')).toBeVisible();
  });

  it('calls onChange with the correct value when an option is clicked', () => {
    render(<Select options={mockOptions} selectedId={1} onChange={mockOnChange} />);
    fireEvent.click(screen.getByTestId('dropdown-button-name'));
    const optionToSelect = screen.getByText('Option 3');

    fireEvent.click(optionToSelect);
    expect(mockOnChange).toHaveBeenCalledWith(3);
  });
});

/* eslint-disable no-magic-numbers */
import { render, screen, fireEvent } from '@testing-library/react';
import { Toast } from './Toast.component';

describe('Toast component', () => {
  it('renders success message correctly', () => {
    const message = 'Success message';
    render(<Toast type="success" message={message} onDismiss={() => {}} />);
    const toastElement = screen.getByText(message);
    expect(toastElement).toBeInTheDocument();
    expect(toastElement).toHaveClass('text-green-500');
  });

  it('renders error message correctly', () => {
    const message = 'Error message';
    render(<Toast type="error" message={message} onDismiss={() => {}} />);
    const toastElement = screen.getByText(message);
    expect(toastElement).toBeInTheDocument();
    expect(toastElement).toHaveClass('text-red-500');
  });

  it('calls onDismiss when close button is clicked', () => {
    const mockOnDismiss = jest.fn();
    render(<Toast type="success" message="Success message" onDismiss={mockOnDismiss} />);
    const closeButton = screen.getByRole('button');
    fireEvent.click(closeButton);
    expect(mockOnDismiss).toHaveBeenCalledTimes(1);
  });
});

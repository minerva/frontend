/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Input } from './Input.component';

describe('Input - component', () => {
  it('should render with proper testid', () => {
    render(<Input data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toBeInTheDocument();
  });

  it('should apply the default style and size variants when none are provided', () => {
    render(<Input data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveClass('bg-cultured');
    expect(inputElement).toHaveClass('rounded-s');
  });

  it('should apply the specified style variant', () => {
    render(<Input styleVariant="primary" data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveClass('bg-cultured');
  });

  it('should apply the specified size variant', () => {
    render(<Input sizeVariant="medium" data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveClass('rounded-lg');
    expect(inputElement).toHaveClass('h-12');
    expect(inputElement).toHaveClass('text-sm');
  });

  it('should merge custom class with style and size variant classes', () => {
    render(
      <Input
        className="text-red-500"
        styleVariant="primary"
        sizeVariant="medium"
        data-testid="input"
      />,
    );
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveClass('text-red-500');
    expect(inputElement).toHaveClass('h-12');
    expect(inputElement).toHaveClass('bg-cultured');
  });

  it(' should handle onChange event', () => {
    const handleChange = jest.fn();
    render(<Input onChange={handleChange} data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    fireEvent.change(inputElement, { target: { value: 'Hello, World!' } });
    expect(handleChange).toHaveBeenCalledTimes(1);
  });

  it('should render with a placeholder', () => {
    const placeholderText = 'Type here...';
    render(<Input placeholder={placeholderText} data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveAttribute('placeholder', placeholderText);
  });

  it('should render with a default value', () => {
    const defaultValue = 'Initial value';
    render(<Input defaultValue={defaultValue} data-testid="input" />);
    const inputElement = screen.getByTestId('input');
    expect(inputElement).toHaveValue(defaultValue);
  });
});

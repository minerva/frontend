import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { openedDrawerSubmapsFixture } from '@/redux/drawer/drawerFixture';
import { DrawerHeading } from './DrawerHeading.component';

const renderComponent = (
  title: string,
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <DrawerHeading title={title} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DrawerHeading - component', () => {
  it('should display passed title', () => {
    renderComponent('Example');

    expect(screen.getByText('Example')).toBeInTheDocument();
  });

  it('should call class drawer on close button click', () => {
    const { store } = renderComponent('Example', {
      drawer: {
        ...openedDrawerSubmapsFixture,
      },
    });

    expect(store.getState().drawer.isOpen).toBe(true);

    const closeButton = screen.getByRole('close-drawer-button');
    closeButton.click();

    expect(store.getState().drawer.isOpen).toBe(false);
  });
});

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { ColorTilePicker } from './ColorTilePicker.component';

describe('ColorTilePicker', () => {
  it('renders with the initial color', () => {
    render(<ColorTilePicker initialColor={{ rgb: 65280, alpha: 255 }} colorChange={jest.fn()} />);
    const colorTile = screen.getByRole('button');
    expect(colorTile).toHaveStyle({ backgroundColor: '#00ff00' });
  });

  it('toggles color picker visibility on click', () => {
    render(<ColorTilePicker initialColor={{ rgb: 0, alpha: 255 }} colorChange={jest.fn()} />);
    const colorTile = screen.getByRole('button');

    expect(screen.queryByTestId('color-picker')).not.toBeInTheDocument();

    fireEvent.click(colorTile);
    expect(screen.getByTestId('color-picker')).toBeInTheDocument();

    fireEvent.click(colorTile);
    expect(screen.queryByTestId('color-picker')).not.toBeInTheDocument();
  });

  it('closes color picker when clicking outside', () => {
    render(<ColorTilePicker initialColor={{ rgb: 0, alpha: 255 }} colorChange={jest.fn()} />);
    const colorTile = screen.getByRole('button');

    fireEvent.click(colorTile);
    expect(screen.getByTestId('color-picker')).toBeInTheDocument();

    fireEvent.mouseDown(document);
    expect(screen.queryByTestId('color-picker')).not.toBeInTheDocument();
  });

  it('handles keyboard interaction (Enter key)', () => {
    render(<ColorTilePicker initialColor={{ rgb: 0, alpha: 255 }} colorChange={jest.fn()} />);
    const colorTile = screen.getByRole('button');

    expect(screen.queryByTestId('color-picker')).not.toBeInTheDocument();

    fireEvent.keyDown(colorTile, { key: 'Enter', code: 'Enter', charCode: 13 });
    expect(screen.getByTestId('color-picker')).toBeInTheDocument();

    fireEvent.keyDown(colorTile, { key: 'Enter', code: 'Enter', charCode: 13 });
    expect(screen.queryByTestId('color-picker')).not.toBeInTheDocument();
  });
});

/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { OutsideClickWrapper } from '.';

describe('OutsideClickWrapper', () => {
  it('should call onOutsideClick when click outside the component', () => {
    const handleOutsideClick = jest.fn();
    const { getByText } = render(
      <OutsideClickWrapper onOutsideClick={handleOutsideClick}>
        <div>Inner element</div>
      </OutsideClickWrapper>,
    );

    const innerElement = getByText('Inner element');

    fireEvent.mouseDown(document.body);

    expect(handleOutsideClick).toHaveBeenCalledTimes(1);

    fireEvent.mouseDown(innerElement);

    expect(handleOutsideClick).toHaveBeenCalledTimes(1);
  });

  it('should not call onOutsideClick when click inside the component', () => {
    const handleOutsideClick = jest.fn();
    const { getByText } = render(
      <OutsideClickWrapper onOutsideClick={handleOutsideClick}>
        <div>Inner element</div>
      </OutsideClickWrapper>,
    );

    const innerElement = getByText('Inner element');

    fireEvent.mouseDown(innerElement);

    expect(handleOutsideClick).not.toHaveBeenCalled();
  });
});

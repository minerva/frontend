import { render, screen } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { MapViewer } from './MapViewer.component';
import { MAP_VIEWER_ROLE } from './MapViewer.constants';

const renderComponent = (): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });

  return (
    render(
      <Wrapper>
        <MapViewer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('MapViewer - component', () => {
  it('should render component container', () => {
    renderComponent();

    expect(screen.getByRole(MAP_VIEWER_ROLE)).toBeInTheDocument();
  });

  it('should render openlayers map inside the component', () => {
    renderComponent();

    const FIRST_NODE = 0;

    expect(screen.getByRole(MAP_VIEWER_ROLE).childNodes[FIRST_NODE]).toHaveClass('ol-viewport');
  });
});

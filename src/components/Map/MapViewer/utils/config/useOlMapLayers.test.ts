/* eslint-disable no-magic-numbers */
import { MAP_DATA_INITIAL_STATE, OPENED_MAPS_INITIAL_STATE } from '@/redux/map/map.constants';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import React from 'react';
import { useOlMap } from '@/components/Map/MapViewer/utils/useOlMap';
import { useOlMapLayers } from '@/components/Map/MapViewer/utils/config/useOlMapLayers';

const useRefValue = {
  current: null,
};

Object.defineProperty(useRefValue, 'current', {
  get: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
  set: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
});

jest.spyOn(React, 'useRef').mockReturnValue(useRefValue);

jest.mock('./additionalLayers/useOlMapAdditionalLayers', () => ({
  useOlMapAdditionalLayers: jest.fn(() => []),
}));

describe('useOlMapLayers - util', () => {
  const getRenderedHookResults = (): BaseLayer[] => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        data: {
          ...MAP_DATA_INITIAL_STATE,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
          position: {
            initial: {
              x: 256,
              y: 256,
            },
            last: {
              x: 256,
              y: 256,
            },
          },
        },
        loading: 'idle',
        error: {
          name: '',
          message: '',
        },
        openedMaps: OPENED_MAPS_INITIAL_STATE,
      },
    });
    const dummyElement = document.createElement('div');
    const { result: hohResult } = renderHook(() => useOlMap({ target: dummyElement }), {
      wrapper: Wrapper,
    });

    const { result } = renderHook(
      () => useOlMapLayers({ mapInstance: hohResult.current.mapInstance }),
      {
        wrapper: Wrapper,
      },
    );
    return result.current;
  };

  it('should return valid VectorLayer instance [1]', () => {
    const result = getRenderedHookResults();
    expect(result[0]).toBeInstanceOf(VectorLayer);
    expect(result[0].getSourceState()).toBe('ready');
  });
});

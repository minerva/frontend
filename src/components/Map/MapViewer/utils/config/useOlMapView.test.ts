/* eslint-disable no-magic-numbers */
import { MAP_DATA_INITIAL_STATE, OPENED_MAPS_INITIAL_STATE } from '@/redux/map/map.constants';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { setMapPosition } from '@/redux/map/map.slice';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { act, renderHook, waitFor } from '@testing-library/react';
import { View } from 'ol';
import Map from 'ol/Map';
import React from 'react';
import { useOlMap } from '../useOlMap';
import { useOlMapView } from './useOlMapView';

const useRefValue = {
  current: null,
};

Object.defineProperty(useRefValue, 'current', {
  get: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
  set: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
});

jest.spyOn(React, 'useRef').mockReturnValue(useRefValue);

jest.mock('./additionalLayers/useOlMapAdditionalLayers', () => ({
  useOlMapAdditionalLayers: jest.fn(() => []),
}));

describe('useOlMapView - util', () => {
  it('should modify view of the map instance on INITIAL position config change', async () => {
    const { Wrapper, store } = getReduxWrapperWithStore({
      map: initialMapStateFixture,
    });

    const dummyElement = document.createElement('div');
    const { result: hohResult } = renderHook(() => useOlMap({ target: dummyElement }), {
      wrapper: Wrapper,
    });

    const setViewSpy = jest.spyOn(hohResult.current.mapInstance as Map, 'setView');
    const CALLED_TWICE = 2;

    await act(() => {
      store.dispatch(
        setMapPosition({
          x: 0,
          y: 0,
        }),
      );
    });

    renderHook(() => useOlMapView({ mapInstance: hohResult.current.mapInstance }), {
      wrapper: Wrapper,
    });

    await waitFor(() => expect(setViewSpy).toBeCalledTimes(CALLED_TWICE));
  });

  it('should return valid View instance', async () => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        data: {
          ...MAP_DATA_INITIAL_STATE,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
          position: {
            initial: {
              x: 128,
              y: 128,
            },
            last: {
              x: 128,
              y: 128,
            },
          },
        },
        loading: 'idle',
        error: {
          name: '',
          message: '',
        },
        openedMaps: OPENED_MAPS_INITIAL_STATE,
      },
    });
    const dummyElement = document.createElement('div');
    const { result: hohResult } = renderHook(() => useOlMap({ target: dummyElement }), {
      wrapper: Wrapper,
    });

    const { result } = renderHook(
      () => useOlMapView({ mapInstance: hohResult.current.mapInstance }),
      {
        wrapper: Wrapper,
      },
    );

    expect(result.current).toBeInstanceOf(View);
    expect(result.current.getCenter()).toStrictEqual([0, -0]);
  });
});

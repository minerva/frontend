import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { PinType } from '@/types/pin';
import { UsePointToProjectionResult, usePointToProjection } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import Style from 'ol/style/Style';
import { commentsFixture } from '@/models/fixtures/commentsFixture';
import { getCommentsFeatures } from '@/components/Map/MapViewer/utils/config/commentsLayer/getCommentsFeatures';

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getCommentsFeatures', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const comments = commentsFixture.map(comment => ({
    ...comment,
    pinType: 'comment' as PinType,
  }));

  const pointToProjection = getPointToProjection(Wrapper);

  it('should return array of instances of Feature with Style', () => {
    const result = getCommentsFeatures(comments, {
      pointToProjection,
    });

    result.forEach(resultElement => {
      expect(resultElement).toBeInstanceOf(Feature);
      expect(resultElement.getStyle()).toBeInstanceOf(Style);
    });
  });
});

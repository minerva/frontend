/* eslint-disable no-magic-numbers */
import { Feature } from 'ol';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { useEffect, useMemo, useState } from 'react';
import { usePointToProjection } from '@/utils/map/usePointToProjection';
import { useSelector } from 'react-redux';
import {
  arrowTypesSelector,
  bioShapesSelector,
  lineTypesSelector,
} from '@/redux/shapes/shapes.selectors';
import { MapInstance } from '@/types/map';
import {
  modelElementsForCurrentModelSelector,
  modelElementsCurrentModelLoadingSelector,
} from '@/redux/modelElements/modelElements.selector';
import { currentModelIdSelector, modelsIdsSelector } from '@/redux/models/models.selectors';
import { getModelElementsForModel } from '@/redux/modelElements/modelElements.thunks';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import {
  newReactionsForCurrentModelSelector,
  newReactionsLoadingSelector,
} from '@/redux/newReactions/newReactions.selectors';
import { getNewReactionsForModel } from '@/redux/newReactions/newReactions.thunks';
import {
  getOverlayOrderSelector,
  overlayBioEntitiesForCurrentModelSelector,
} from '@/redux/overlayBioEntity/overlayBioEntity.selector';
import { groupBy } from '@/utils/array/groupBy';
import { useGetOverlayColor } from '@/components/Map/MapViewer/utils/config/overlaysLayer/useGetOverlayColor';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { markersSufraceOfCurrentMapDataSelector } from '@/redux/markers/markers.selectors';
import { parseSurfaceMarkersToBioEntityRender } from '@/components/Map/MapViewer/utils/config/overlaysLayer/parseSurfaceMarkersToBioEntityRender';
import processModelElements from '@/components/Map/MapViewer/utils/config/processLayer/processModelElements';
import useDebouncedValue from '@/utils/useDebouncedValue';
import { mapBackgroundSelector, mapDataSizeSelector } from '@/redux/map/map.selectors';
import { ZOOM_RESCALING_FACTOR } from '@/constants/map';
import { OverlayOrder } from '@/redux/overlayBioEntity/overlayBioEntity.utils';
import MarkerOverlay from '@/components/Map/MapViewer/utils/shapes/overlay/MarkerOverlay';
import LineOverlay from '@/components/Map/MapViewer/utils/shapes/overlay/LineOverlay';
import getOverlays from '@/components/Map/MapViewer/utils/shapes/overlay/getOverlays';
import Reaction from '@/components/Map/MapViewer/utils/shapes/reaction/Reaction';
import CompartmentPathway from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentPathway';
import Glyph from '@/components/Map/MapViewer/utils/shapes/elements/Glyph/Glyph';
import CompartmentCircle from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentCircle';
import CompartmentSquare from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentSquare';
import MapElement from '@/components/Map/MapViewer/utils/shapes/elements/MapElement';
import areOverlayOrdersNotEqual from '@/components/Map/MapViewer/utils/shapes/overlay/areOverlayOrdersNotEqual';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import { modelLoadedSuccessfullySelector, modelLoadingSelector } from '@/redux/selectors';
import { projectSbgnFormatSelector } from '@/redux/project/project.selectors';

export const useOlMapProcessLayer = ({
  mapInstance,
}: {
  mapInstance: MapInstance;
}): VectorLayer<VectorSource<Feature>> => {
  const dispatch = useAppDispatch();

  const [overlaysOrderState, setOverlaysOrderState] = useState<Array<OverlayOrder>>([]);
  const [allModelElementsLoaded, setAllModelElementsLoaded] = useState<boolean>(false);
  const currentModelId = useSelector(currentModelIdSelector);
  const shapes = useSelector(bioShapesSelector);
  const mapSize = useSelector(mapDataSizeSelector);
  const lineTypes = useSelector(lineTypesSelector);
  const arrowTypes = useSelector(arrowTypesSelector);
  const overlaysOrder = useSelector(getOverlayOrderSelector);
  const backgroundId = useSelector(mapBackgroundSelector);
  const currentMarkers = useAppSelector(markersSufraceOfCurrentMapDataSelector);
  const markersRender = useMemo(() => {
    return parseSurfaceMarkersToBioEntityRender(currentMarkers);
  }, [currentMarkers]);

  const bioEntities = useAppSelector(overlayBioEntitiesForCurrentModelSelector);
  const reactionsForCurrentModel = useAppSelector(newReactionsForCurrentModelSelector);
  const modelElementsLoading = useAppSelector(modelElementsCurrentModelLoadingSelector);
  const reactionsLoading = useAppSelector(newReactionsLoadingSelector);
  const sbgnFormat = useAppSelector(projectSbgnFormatSelector);

  const modelElementsForCurrentModel = useAppSelector(modelElementsForCurrentModelSelector);
  const debouncedBioEntities = useDebouncedValue(bioEntities, 1000);
  const { getOverlayBioEntityColorByAvailableProperties } = useGetOverlayColor();

  const modelsIds = useAppSelector(modelsIdsSelector);
  const isModelLoading = useAppSelector(modelLoadingSelector);
  const modelLoadedSuccessfully = useAppSelector(modelLoadedSuccessfullySelector);
  useEffect(() => {
    const fetchModelElements = async (): Promise<void> => {
      if (!isModelLoading && modelLoadedSuccessfully && !allModelElementsLoaded) {
        const modelsIdsToFetch = modelsIds.filter(id => id !== currentModelId);
        const modelElementsPromises = modelsIdsToFetch.map(modelId => {
          return dispatch(getModelElementsForModel(modelId));
        });
        await Promise.all(modelElementsPromises);
        setAllModelElementsLoaded(true);
      }
    };

    fetchModelElements();
  }, [
    modelLoadedSuccessfully,
    isModelLoading,
    modelsIds,
    currentModelId,
    dispatch,
    allModelElementsLoaded,
  ]);

  const pointToProjection = usePointToProjection();

  const vectorSource = useMemo(() => new VectorSource(), []);

  const mapModelOriginalMaxZoom = mapInstance?.getView().get('originalMaxZoom');

  const isCorrectMapInstanceViewScale = useMemo(() => {
    return mapSize.maxZoom * ZOOM_RESCALING_FACTOR === mapModelOriginalMaxZoom;
  }, [mapModelOriginalMaxZoom, mapSize.maxZoom]);

  useEffect(() => {
    if (areOverlayOrdersNotEqual(overlaysOrderState, overlaysOrder)) {
      setOverlaysOrderState(overlaysOrder);
    }
  }, [overlaysOrder, overlaysOrderState]);

  useEffect(() => {
    if (!currentModelId) {
      return;
    }
    if (!['succeeded', 'pending'].includes(modelElementsLoading)) {
      dispatch(getModelElementsForModel(currentModelId));
    }
    if (!['succeeded', 'pending'].includes(reactionsLoading)) {
      dispatch(getNewReactionsForModel(currentModelId));
    }
  }, [currentModelId, dispatch, reactionsLoading, modelElementsLoading]);

  const groupedElementsOverlays = useMemo(() => {
    const elementsBioEntitesOverlay = debouncedBioEntities.filter(
      bioEntity => bioEntity.type !== 'line',
    );
    const grouped = groupBy(elementsBioEntitesOverlay, bioEntity => bioEntity.id.toString());
    return getOverlays(grouped, getOverlayBioEntityColorByAvailableProperties);
  }, [debouncedBioEntities, getOverlayBioEntityColorByAvailableProperties]);

  const linesOverlays = useMemo(() => {
    return bioEntities.filter(bioEntity => bioEntity.type === 'line');
  }, [bioEntities]);

  const linesOverlaysFeatures = useMemo(() => {
    if (!isCorrectMapInstanceViewScale) {
      return [];
    }
    return linesOverlays.map(lineOverlay => {
      return new LineOverlay({
        lineOverlay,
        getOverlayColor: getOverlayBioEntityColorByAvailableProperties,
        pointToProjection,
        mapInstance,
      }).lineFeature;
    });
  }, [
    getOverlayBioEntityColorByAvailableProperties,
    isCorrectMapInstanceViewScale,
    linesOverlays,
    mapInstance,
    pointToProjection,
  ]);

  const markerOverlaysFeatures = useMemo(() => {
    if (!isCorrectMapInstanceViewScale) {
      return [];
    }
    return markersRender.map(marker => {
      return new MarkerOverlay({
        markerOverlay: marker,
        getOverlayColor: getOverlayBioEntityColorByAvailableProperties,
        pointToProjection,
        mapInstance,
      }).markerFeature;
    });
  }, [
    getOverlayBioEntityColorByAvailableProperties,
    isCorrectMapInstanceViewScale,
    mapInstance,
    markersRender,
    pointToProjection,
  ]);

  const reactions = useMemo(() => {
    if (!reactionsForCurrentModel || !isCorrectMapInstanceViewScale) {
      return [];
    }
    return reactionsForCurrentModel.map(reaction => {
      const reactionShapes = shapes && shapes[reaction.sboTerm];
      if (!reactionShapes) {
        return [];
      }
      const reactionObject = new Reaction({
        id: reaction.id,
        line: reaction.line,
        products: reaction.products,
        reactants: reaction.reactants,
        modifiers: reaction.modifiers,
        operators: reaction.operators,
        zIndex: reaction.z,
        lineTypes,
        arrowTypes,
        shapes: reactionShapes,
        pointToProjection,
        vectorSource,
        sbgnFormat,
        mapInstance,
      });
      return [reactionObject.lineFeature, ...reactionObject.reactionFeatures];
    });
  }, [
    reactionsForCurrentModel,
    isCorrectMapInstanceViewScale,
    shapes,
    lineTypes,
    arrowTypes,
    pointToProjection,
    vectorSource,
    sbgnFormat,
    mapInstance,
  ]);

  const elements: Array<
    MapElement | CompartmentCircle | CompartmentSquare | CompartmentPathway | Glyph
  > = useMemo(() => {
    if (!modelElementsForCurrentModel || !shapes || !isCorrectMapInstanceViewScale) {
      return [];
    }
    return processModelElements(
      modelElementsForCurrentModel,
      shapes,
      lineTypes,
      groupedElementsOverlays,
      overlaysOrderState,
      getOverlayBioEntityColorByAvailableProperties,
      vectorSource,
      mapInstance,
      pointToProjection,
      backgroundId,
      sbgnFormat,
      mapSize,
    );
  }, [
    modelElementsForCurrentModel,
    shapes,
    isCorrectMapInstanceViewScale,
    lineTypes,
    groupedElementsOverlays,
    overlaysOrderState,
    getOverlayBioEntityColorByAvailableProperties,
    vectorSource,
    mapInstance,
    pointToProjection,
    backgroundId,
    sbgnFormat,
    mapSize,
  ]);

  const features = useMemo(() => {
    const reactionsFeatures = reactions.flat();
    const elementsFeatures = elements.map(element => element.feature);
    return [
      ...reactionsFeatures,
      ...elementsFeatures,
      ...linesOverlaysFeatures,
      ...markerOverlaysFeatures,
    ];
  }, [elements, linesOverlaysFeatures, markerOverlaysFeatures, reactions]);

  useEffect(() => {
    vectorSource.clear();
    vectorSource.addFeatures(features);
  }, [features, vectorSource]);

  return useMemo(() => {
    const vectorLayer = new VectorLayer({
      zIndex: 0,
      source: vectorSource,
      updateWhileAnimating: true,
      updateWhileInteracting: true,
    });
    vectorLayer.set('type', LAYER_TYPE.PROCESS_LAYER);
    return vectorLayer;
  }, [vectorSource]);
};

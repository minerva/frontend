import { ModelElement } from '@/types/models';
import { HorizontalAlign, VerticalAlign } from '@/components/Map/MapViewer/MapViewer.types';
import { MapInstance } from '@/types/map';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import { BioShapesDict, LineTypeDict } from '@/redux/shapes/shapes.types';
import { OverlayOrder } from '@/redux/overlayBioEntity/overlayBioEntity.utils';
import { OverlayBioEntityRender } from '@/types/OLrendering';
import { GetOverlayBioEntityColorByAvailableProperties } from '@/components/Map/MapViewer/utils/config/overlaysLayer/useGetOverlayColor';
import VectorSource from 'ol/source/Vector';
import { MapSize } from '@/redux/map/map.types';
import Glyph from '@/components/Map/MapViewer/utils/shapes/elements/Glyph/Glyph';
import CompartmentPathway from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentPathway';
import CompartmentSquare from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentSquare';
import CompartmentCircle from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentCircle';
import MapElement from '@/components/Map/MapViewer/utils/shapes/elements/MapElement';
import { COMPARTMENT_SBO_TERM } from '@/components/Map/MapViewer/MapViewer.constants';

export default function processModelElements(
  modelElements: Array<ModelElement>,
  shapes: BioShapesDict,
  lineTypes: LineTypeDict,
  groupedElementsOverlays: Record<string, Array<OverlayBioEntityRender>>,
  overlaysOrder: Array<OverlayOrder>,
  getOverlayColor: GetOverlayBioEntityColorByAvailableProperties,
  vectorSource: VectorSource,
  mapInstance: MapInstance,
  pointToProjection: UsePointToProjectionResult,
  backgroundId: number,
  sbgnFormat: boolean,
  mapSize: MapSize,
): Array<MapElement | CompartmentCircle | CompartmentSquare | CompartmentPathway | Glyph> {
  const overlaysVisible = Boolean(overlaysOrder.length);
  const validElements: Array<
    MapElement | CompartmentCircle | CompartmentSquare | CompartmentPathway | Glyph
  > = [];
  modelElements.forEach((element: ModelElement) => {
    if (element.glyph) {
      const glyph = new Glyph({
        elementId: element.id,
        glyphId: element.glyph.id,
        x: element.x,
        y: element.y,
        width: element.width,
        height: element.height,
        zIndex: element.z,
        pointToProjection,
        mapInstance,
        mapSize,
      });
      validElements.push(glyph);
      return;
    }

    if (element.sboTerm === COMPARTMENT_SBO_TERM) {
      const compartmentProps = {
        id: element.id,
        sboTerm: element.sboTerm,
        complexId: element.complex,
        compartmentId: element.compartment,
        pathwayId: element.pathway,
        x: element.x,
        y: element.y,
        nameX: element.nameX,
        nameY: element.nameY,
        nameHeight: element.nameHeight,
        nameWidth: element.nameWidth,
        width: element.width,
        height: element.height,
        zIndex: element.z,
        innerWidth: element.innerWidth,
        outerWidth: element.outerWidth,
        thickness: element.thickness,
        fontColor: element.fontColor,
        fillColor: element.fillColor,
        borderColor: element.borderColor,
        nameVerticalAlign: element.nameVerticalAlign as VerticalAlign,
        nameHorizontalAlign: element.nameHorizontalAlign as HorizontalAlign,
        text: element.name,
        fontSize: element.fontSize,
        overlaysVisible,
        pointToProjection,
        mapInstance,
        vectorSource,
        backgroundId,
        mapSize,
      };
      if (element.shape === 'OVAL_COMPARTMENT') {
        validElements.push(new CompartmentCircle(compartmentProps));
      } else if (element.shape === 'SQUARE_COMPARTMENT') {
        validElements.push(new CompartmentSquare(compartmentProps));
      } else if (element.shape === 'PATHWAY') {
        validElements.push(new CompartmentPathway(compartmentProps));
      }
      return;
    }
    const elementShapes = shapes[element.sboTerm];
    if (elementShapes) {
      validElements.push(
        new MapElement({
          id: element.id,
          complexId: element.complex,
          compartmentId: element.compartment,
          pathwayId: element.pathway,
          sboTerm: element.sboTerm,
          shapes: elementShapes,
          x: element.x,
          y: element.y,
          nameX: element.nameX,
          nameY: element.nameY,
          nameHeight: element.nameHeight,
          nameWidth: element.nameWidth,
          width: element.width,
          height: element.height,
          zIndex: element.z,
          lineWidth: element.lineWidth,
          lineType: element.borderLineType,
          fontColor: element.fontColor,
          fillColor: element.fillColor,
          borderColor: element.borderColor,
          nameVerticalAlign: element.nameVerticalAlign as VerticalAlign,
          nameHorizontalAlign: element.nameHorizontalAlign as HorizontalAlign,
          homodimer: element.homodimer,
          activity: element.activity,
          text: element.name,
          fontSize: element.fontSize,
          pointToProjection,
          mapInstance,
          vectorSource,
          modifications: element.modificationResidues,
          lineTypes,
          bioShapes: shapes,
          overlays: groupedElementsOverlays[element.id],
          overlaysOrder,
          overlaysVisible,
          getOverlayColor,
          backgroundId,
          sbgnFormat,
          mapSize,
        }),
      );
    }
  });
  return validElements;
}

import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import VectorLayer from 'ol/layer/Vector';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { Map } from 'ol';
import { useOlMapProcessLayer } from '@/components/Map/MapViewer/utils/config/processLayer/useOlMapProcessLayer';

describe('useOlMapReactionsLayer - util', () => {
  it('should return VectorLayer', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: initialMapStateFixture,
    });

    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const { result } = renderHook(() => useOlMapProcessLayer({ mapInstance }), {
      wrapper: Wrapper,
    });

    expect(result.current).toBeInstanceOf(VectorLayer);
    expect(result.current.getSourceState()).toBe('ready');
  });
});

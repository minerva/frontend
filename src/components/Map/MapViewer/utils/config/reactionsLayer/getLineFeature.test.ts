import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { LinePoint } from '@/types/reactions';
import { UsePointToProjectionResult, usePointToProjection } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import { LineString } from 'ol/geom';
import { getLineFeature } from './getLineFeature';

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getLineFeature', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const pointToProjection = getPointToProjection(Wrapper);

  const linePoints: LinePoint = [
    {
      x: 16,
      y: 32,
    },
    {
      x: 54,
      y: 16,
    },
  ];

  it('should return valid Feature object', () => {
    const result = getLineFeature(linePoints, pointToProjection);

    expect(result).toBeInstanceOf(Feature);
  });

  it('should return valid Feature object with LineString geometry', () => {
    const result = getLineFeature(linePoints, pointToProjection);
    const geometry = result.getGeometry();

    expect(geometry).toBeInstanceOf(LineString);
    // eslint-disable-next-line no-magic-numbers
    expect(geometry?.getExtent()).toStrictEqual([Infinity, -238107693, Infinity, -238107693]);
  });
});

/* eslint-disable no-magic-numbers */
import { NewReaction } from '@/types/models';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { getReactionLineSegments } from '@/components/Map/MapViewer/utils/config/reactionsLayer/getReactionLineSegments';

describe('getReactionLineSegments', () => {
  it('should return all segments from reaction line and associated entities', () => {
    const mockSegments1 = [
      { x1: 0, y1: 0, x2: 1, y2: 1 },
      { x1: 1, y1: 1, x2: 2, y2: 2 },
    ];
    const mockSegments2 = [
      { x1: 2, y1: 2, x2: 3, y2: 3 },
      { x1: 4, y1: 4, x2: 5, y2: 5 },
    ];
    const mockSegments3 = [
      { x1: 6, y1: 6, x2: 7, y2: 7 },
      { x1: 8, y1: 8, x2: 9, y2: 9 },
    ];
    const mockSegments4 = [
      { x1: 10, y1: 10, x2: 11, y2: 11 },
      { x1: 12, y1: 12, x2: 13, y2: 13 },
    ];
    const mockSegments5 = [
      { x1: 14, y1: 14, x2: 15, y2: 15 },
      { x1: 16, y1: 16, x2: 17, y2: 17 },
    ];

    const mockReaction: NewReaction = newReactionFixture;
    mockReaction.operators = [mockReaction.operators[0]];
    mockReaction.reactants = [mockReaction.reactants[0]];
    mockReaction.modifiers = [mockReaction.modifiers[0]];
    mockReaction.products = [mockReaction.products[0]];

    mockReaction.line.segments = mockSegments1;
    mockReaction.reactants[0].line.segments = mockSegments2;
    mockReaction.products[0].line.segments = mockSegments3;
    mockReaction.modifiers[0].line.segments = mockSegments4;
    mockReaction.operators[0].line.segments = mockSegments5;

    const result = getReactionLineSegments(mockReaction);

    expect(result).toEqual([
      ...mockSegments1,
      ...mockSegments2,
      ...mockSegments3,
      ...mockSegments4,
      ...mockSegments5,
    ]);
  });
});

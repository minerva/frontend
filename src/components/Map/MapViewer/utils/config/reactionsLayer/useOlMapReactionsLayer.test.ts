import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import VectorLayer from 'ol/layer/Vector';
import Style from 'ol/style/Style';
import { useOlMapReactionsLayer } from './useOlMapReactionsLayer';

describe('useOlMapReactionsLayer - util', () => {
  const { Wrapper } = getReduxWrapperWithStore();

  it('should return VectorLayer', () => {
    const { result } = renderHook(() => useOlMapReactionsLayer(), {
      wrapper: Wrapper,
    });

    expect(result.current).toBeInstanceOf(VectorLayer);
    expect(result.current.getSourceState()).toBe('ready');
  });

  it('should return VectorLayer with valid Style', () => {
    const { result } = renderHook(() => useOlMapReactionsLayer(), {
      wrapper: Wrapper,
    });

    const vectorLayer = result.current;
    const style = vectorLayer.getStyle();

    expect(style).not.toBeInstanceOf(Array);
    expect((style as Style).getFill()).toEqual({ color_: '#00AAFF', patternImage_: null });
    expect((style as Style).getStroke()).toMatchObject({ color_: '#00AAFF', width_: 6 });
  });
});

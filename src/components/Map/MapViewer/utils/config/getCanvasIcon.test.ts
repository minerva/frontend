/* eslint-disable no-magic-numbers */
import { DEFAULT_FONT_FAMILY } from '@/constants/font';
import { createCanvas } from '@/utils/canvas/getCanvas';
import {
  drawNumberOnCanvas,
  drawPinOnCanvas,
  getTextPosition,
  getTextWidth,
} from './getCanvasIcon';

const getContext = (): CanvasRenderingContext2D => {
  const canvas = createCanvas({ width: 100, height: 100 });
  return canvas.getContext('2d') as CanvasRenderingContext2D;
};

const ONCE = 1;

describe('getCanvasIcon - util', () => {
  beforeEach(() => {
    jest.restoreAllMocks();
  });

  describe('getTextWidth - subUtil', () => {
    const cases: [number, number][] = [
      [1, 6.25],
      [7, 8.333],
      [43, 12.5],
      [105, 16.666],
    ];

    it.each(cases)('on value=%s should return %s', (input, output) => {
      expect(getTextWidth(input)).toBeCloseTo(output);
    });
  });

  describe('getTextPosition - subUtil', () => {
    const cases: [number, number, number, number][] = [
      [100, 100, -37.5, -27.2],
      [532, 443, -253.5, -164.4],
      [10, 0, 7.5, 12.8],
      [0, 10, 12.5, 8.8],
      [0, 0, 12.5, 12.8],
    ];

    it.each(cases)(
      'on textWidth=%s textHeight=%s should return x=%s y=%s',
      (textWidth, textHeight, x, y) => {
        expect(getTextPosition(textWidth, textHeight)).toMatchObject({
          x,
          y,
        });
      },
    );
  });

  describe('drawPinOnCanvas - subUtil', () => {
    const color = '#000000';

    it('should run set fillStyle with color', () => {
      const ctx = getContext();
      drawPinOnCanvas({ color }, ctx);
      expect(ctx.fillStyle).toBe(color);
    });

    it('should run fill method with valid arguments', () => {
      const ctx = getContext();
      const fillSpy = jest.spyOn(ctx, 'fill');
      drawPinOnCanvas({ color }, ctx);

      const call = fillSpy.mock.calls[0][0];
      expect(call).toBeInstanceOf(Path2D);
      expect(fillSpy).toBeCalledTimes(ONCE);
    });
  });

  describe('drawNumberOnCanvas - subUtil', () => {
    const ctx = getContext();
    const fillTextSpy = jest.spyOn(ctx, 'fillText');
    const value = 69;

    beforeAll(() => {
      drawNumberOnCanvas(
        {
          value,
        },
        ctx,
      );
    });
    it('should set valid ctx fields', () => {
      expect(ctx.fillStyle).toBe('#ffffff');
      expect(ctx.textBaseline).toBe('top');
      expect(ctx.font).toBe(`6.25px ${DEFAULT_FONT_FAMILY}`);
    });

    it('should run fillText once with valid args', () => {
      expect(fillTextSpy).toBeCalledWith(`${value}`, 6.25, 12.8);
      expect(fillTextSpy).toBeCalledTimes(ONCE);
    });
  });
});

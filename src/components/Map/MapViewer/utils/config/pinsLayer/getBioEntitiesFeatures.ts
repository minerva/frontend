import { EntityNumber } from '@/redux/entityNumber/entityNumber.types';
import { ModelElementWithPinType } from '@/types/modelElement';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import { Feature } from 'ol';
import { getModelElementSingleFeature } from './getModelElementSingleFeature';

export const getBioEntitiesFeatures = (
  bioEntites: ModelElementWithPinType[],
  {
    pointToProjection,
    entityNumber,
    activeIds,
  }: {
    pointToProjection: UsePointToProjectionResult;
    entityNumber: EntityNumber;
    activeIds: (string | number)[];
  },
): Feature[] => {
  return bioEntites.map(bioEntity =>
    getModelElementSingleFeature(bioEntity, {
      pointToProjection,
      type: bioEntity.type,
      // pin's index number
      value: entityNumber?.[bioEntity.elementId],
      isActive: activeIds.includes(bioEntity.id),
    }),
  );
};

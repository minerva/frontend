import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { PIN_MARKER } from '@/redux/models/marker.mock';
import { UsePointToProjectionResult, usePointToProjection } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import Style from 'ol/style/Style';
import { getMarkerSingleFeature } from './getMarkerSingleFeature';
import * as getPinStyle from './getPinStyle';

jest.mock('./getPinStyle', () => ({
  __esModule: true,
  ...jest.requireActual('./getPinStyle'),
}));

const getPinStyleSpy = jest.spyOn(getPinStyle, 'getPinStyle');

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getMarkerSingleFeature - subUtil', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const pointToProjection = getPointToProjection(Wrapper);

  it('should return instance of Feature with Style type=%s', () => {
    const result = getMarkerSingleFeature(PIN_MARKER, {
      pointToProjection,
    });

    const style = result.getStyle() as Style;

    expect(result).toBeInstanceOf(Feature);
    expect(style).toBeInstanceOf(Style);
  });

  it('should run getPinStyle with valid args for type=%s', () => {
    getMarkerSingleFeature(PIN_MARKER, {
      pointToProjection,
    });

    expect(getPinStyleSpy).toHaveBeenCalledWith({
      color: '#0000001a',
      value: PIN_MARKER.number,
    });
  });
});

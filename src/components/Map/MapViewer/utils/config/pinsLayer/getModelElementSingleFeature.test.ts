/* eslint-disable no-magic-numbers */
import { PINS_COLORS, TEXT_COLOR } from '@/constants/canvas';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { PinType } from '@/types/pin';
import { addAlphaToHexString } from '@/utils/convert/addAlphaToHexString';
import { UsePointToProjectionResult, usePointToProjection } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import Style from 'ol/style/Style';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { getModelElementSingleFeature } from './getModelElementSingleFeature';
import * as getPinStyle from './getPinStyle';

jest.mock('./getPinStyle', () => ({
  __esModule: true,
  ...jest.requireActual('./getPinStyle'),
}));

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getModelElementSingleFeature - subUtil', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const pointToProjection = getPointToProjection(Wrapper);

  const value = 1448;
  const pinTypes: PinType[] = ['modelElement', 'drugs', 'chemicals'];

  it.each(pinTypes)('should return instance of Feature with Style type=%s', type => {
    const result = getModelElementSingleFeature(modelElementFixture, {
      pointToProjection,
      type,
      value,
      isActive: true,
    });

    const style = result.getStyle() as Style;

    expect(result).toBeInstanceOf(Feature);
    expect(style).toBeInstanceOf(Style);
  });

  it.each(pinTypes)('should run getPinStyle with valid args for type=%s', type => {
    const getPinStyleSpy = jest.spyOn(getPinStyle, 'getPinStyle');

    getModelElementSingleFeature(modelElementFixture, {
      pointToProjection,
      type,
      value,
      isActive: true,
    });

    expect(getPinStyleSpy).toHaveBeenCalledWith({
      color: PINS_COLORS[type],
      value,
      textColor: TEXT_COLOR,
    });
  });

  it.each(pinTypes)(
    'should run getPinStyle with valid args for isActive=fasle and type=%s',
    type => {
      const getPinStyleSpy = jest.spyOn(getPinStyle, 'getPinStyle');

      getModelElementSingleFeature(modelElementFixture, {
        pointToProjection,
        type,
        value,
        isActive: false,
      });

      expect(getPinStyleSpy).toHaveBeenCalledWith({
        color: addAlphaToHexString(PINS_COLORS[type], 0.5),
        value,
        textColor: addAlphaToHexString(TEXT_COLOR, 0.5),
      });
    },
  );
});

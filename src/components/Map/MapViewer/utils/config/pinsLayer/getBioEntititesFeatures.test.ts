import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { bioEntitiesContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { PinType } from '@/types/pin';
import { usePointToProjection, UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import Style from 'ol/style/Style';
import { getBioEntitiesFeatures } from './getBioEntitiesFeatures';

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getBioEntitiesFeatures - subUtil', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const bioEntities = bioEntitiesContentFixture.map(({ bioEntity }) => ({
    ...bioEntity,
    type: 'modelElement' as PinType,
  }));
  const pointToProjection = getPointToProjection(Wrapper);

  it('should return array of instances of Feature with Style type=%s', () => {
    const result = getBioEntitiesFeatures(bioEntities, {
      pointToProjection,
      entityNumber: {},
      activeIds: [bioEntities[FIRST_ARRAY_ELEMENT].id],
    });

    result.forEach(resultElement => {
      expect(resultElement).toBeInstanceOf(Feature);
      expect(resultElement.getStyle()).toBeInstanceOf(Style);
    });
  });
});

/* eslint-disable no-magic-numbers */
import {
  allBioEntitiesWithTypeOfCurrentMapSelector,
  allVisibleBioEntitiesIdsSelector,
} from '@/redux/bioEntity/bioEntity.selectors';
import { entityNumberDataSelector } from '@/redux/entityNumber/entityNumber.selectors';
import { markersPinsOfCurrentMapDataSelector } from '@/redux/markers/markers.selectors';
import { ModelElement } from '@/types/models';
import { usePointToProjection } from '@/utils/map/usePointToProjection';
import Feature from 'ol/Feature';
import { Geometry } from 'ol/geom';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { useCallback, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import { getBioEntitiesFeatures } from './getBioEntitiesFeatures';
import { getMarkersFeatures } from './getMarkersFeatures';
import { getMultipinsModelElements } from './getMultipinsModelElements';
import { getMultipinBioEntititesIds } from './getMultipinsBioEntitiesIds';
import { getMultipinFeatures } from './getMultipinsFeatures';

export const useOlMapPinsLayer = (): VectorLayer<VectorSource<Feature<Geometry>>> => {
  const pointToProjection = usePointToProjection();
  const activeIds = useSelector(allVisibleBioEntitiesIdsSelector);
  const modelElements = useSelector(allBioEntitiesWithTypeOfCurrentMapSelector);
  const markersEntities = useSelector(markersPinsOfCurrentMapDataSelector);
  const entityNumber = useSelector(entityNumberDataSelector);
  const multiPinsBioEntities = useMemo(
    () =>
      getMultipinsModelElements({
        modelElements,
      }),
    [modelElements],
  );
  const multipinsIds = getMultipinBioEntititesIds(multiPinsBioEntities);
  const isMultiPin = useCallback(
    (b: ModelElement): boolean => multipinsIds.includes(b.id),
    [multipinsIds],
  );

  const elementsFeatures = useMemo(
    () =>
      [
        getBioEntitiesFeatures(
          modelElements.filter(b => !isMultiPin(b)),
          {
            pointToProjection,
            entityNumber,
            activeIds,
          },
        ),
        getMultipinFeatures(multiPinsBioEntities, {
          pointToProjection,
          entityNumber,
          activeIds,
        }),
        getMarkersFeatures(markersEntities, { pointToProjection }),
      ].flat(),
    [
      modelElements,
      pointToProjection,
      entityNumber,
      activeIds,
      multiPinsBioEntities,
      markersEntities,
      isMultiPin,
    ],
  );

  const vectorSource = useMemo(() => new VectorSource(), []);

  useEffect(() => {
    vectorSource.clear();
    vectorSource.addFeatures(elementsFeatures);
  }, [elementsFeatures, vectorSource]);

  return useMemo(() => {
    const vectorLayer = new VectorLayer({
      zIndex: Infinity,
      source: vectorSource,
    });
    vectorLayer.set('type', LAYER_TYPE.PINS_LAYER);
    return vectorLayer;
  }, [vectorSource]);
};

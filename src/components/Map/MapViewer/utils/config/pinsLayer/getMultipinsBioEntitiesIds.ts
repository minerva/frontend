import { MultiPinModelElement } from '@/types/modelElement';

export const getMultipinBioEntititesIds = (
  multipins: MultiPinModelElement[],
): (string | number)[] => multipins.flat().map(({ id }) => id);

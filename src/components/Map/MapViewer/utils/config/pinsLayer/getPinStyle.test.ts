import { PIN_SIZE } from '@/constants/canvas';
import { ZERO } from '@/constants/common';
import Style from 'ol/style/Style';
import { getPinStyle } from './getPinStyle';

describe('getPinStyle - subUtil', () => {
  const input = {
    color: '#000000',
    value: 420,
  };

  const result = getPinStyle(input);

  it('should return instance of Style', () => {
    expect(result).toBeInstanceOf(Style);
  });

  it('should return image object with displacament of pin size height', () => {
    const image = result.getImage();
    expect(image?.getDisplacement()).toStrictEqual([ZERO, PIN_SIZE.height]);
  });

  it('should return image of pin size', () => {
    const image = result.getImage();
    expect(image?.getImageSize()).toStrictEqual([PIN_SIZE.width, PIN_SIZE.height]);
  });
});

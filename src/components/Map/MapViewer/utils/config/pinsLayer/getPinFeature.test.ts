import { HALF } from '@/constants/dividers';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { PIN_MARKER } from '@/redux/models/marker.mock';
import { usePointToProjection } from '@/utils/map/usePointToProjection';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import { getPinFeature } from './getPinFeature';

describe('getPinFeature - subUtil', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const { bioEntity } = bioEntityContentFixture;
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper: Wrapper,
  });
  const pointToProjection = usePointToProjectionHook.current;
  const result = getPinFeature(bioEntity, pointToProjection, 'modelElement');

  it('should return instance of Feature', () => {
    expect(result).toBeInstanceOf(Feature);
  });

  it('should return id as name', () => {
    expect(result.get('id')).toBe(bioEntity.id);
  });

  it('should return point parsed with point to projection', () => {
    const [x, y] = result.getGeometry()?.getExtent() || [];
    const geometryPoint = pointToProjection({
      x: bioEntity.x + bioEntity.width / HALF,
      y: bioEntity.y + bioEntity.height / HALF,
    });

    expect([x, y]).toStrictEqual(geometryPoint);
  });

  describe('when is Marker Pin', () => {
    const pinMarkerResult = getPinFeature(PIN_MARKER, pointToProjection, 'modelElement');

    it('should return point parsed with point to projection', () => {
      const [x, y] = pinMarkerResult.getGeometry()?.getExtent() || [];
      const geometryPoint = pointToProjection({
        x: PIN_MARKER.x,
        y: PIN_MARKER.y,
      });

      expect([x, y]).toStrictEqual(geometryPoint);
    });
  });
});

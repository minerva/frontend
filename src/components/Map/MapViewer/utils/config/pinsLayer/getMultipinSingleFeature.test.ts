import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { EntityNumber } from '@/redux/entityNumber/entityNumber.types';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { MultiPinModelElement } from '@/types/modelElement';
import { PinType } from '@/types/pin';
import { UsePointToProjectionResult, usePointToProjection } from '@/utils/map/usePointToProjection';
import {
  GetReduxWrapperUsingSliceReducer,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import Style from 'ol/style/Style';
import { getMultipinSingleFeature } from './getMultipinSingleFeature';
import * as getMultipinStyle from './getMultipinStyle';

jest.mock('./getMultipinStyle', () => ({
  __esModule: true,
  ...jest.requireActual('./getMultipinStyle'),
}));

const ONE_MULTI_BIO_ENTITIES: MultiPinModelElement = [
  {
    ...bioEntityContentFixture.bioEntity,
    type: 'modelElement' as PinType,
    x: 100,
    y: 100,
  },
  {
    ...bioEntityContentFixture.bioEntity,
    type: 'drugs' as PinType,
    x: 100,
    y: 100,
  },
];

const ENTITY_NUMBER: EntityNumber = {
  [ONE_MULTI_BIO_ENTITIES[FIRST_ARRAY_ELEMENT].elementId]: 100,
};

const getMultipinStyleSpy = jest.spyOn(getMultipinStyle, 'getMultipinStyle');

const getPointToProjection = (
  wrapper: ReturnType<GetReduxWrapperUsingSliceReducer>['Wrapper'],
): UsePointToProjectionResult => {
  const { result: usePointToProjectionHook } = renderHook(() => usePointToProjection(), {
    wrapper,
  });

  return usePointToProjectionHook.current;
};

describe('getMultipinSingleFeature - subUtil', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const pointToProjection = getPointToProjection(Wrapper);

  it('should return instance of Feature with Style type=%s', () => {
    const result = getMultipinSingleFeature(ONE_MULTI_BIO_ENTITIES, {
      pointToProjection,
      entityNumber: ENTITY_NUMBER,
      activeIds: [],
    });

    const style = result.getStyle() as Style;

    expect(result).toBeInstanceOf(Feature);
    expect(style).toBeInstanceOf(Style);
  });

  it('should run getPinStyle with valid args for type=%s', () => {
    getMultipinSingleFeature(ONE_MULTI_BIO_ENTITIES, {
      pointToProjection,
      entityNumber: ENTITY_NUMBER,
      activeIds: [],
    });

    expect(getMultipinStyleSpy).toHaveBeenCalledWith({
      pins: [
        { color: '#0c4fa180', textColor: '#FFFFFF80', value: 100 },
        { color: '#F48C4180', textColor: '#FFFFFF80', value: undefined },
      ],
    });
  });
});

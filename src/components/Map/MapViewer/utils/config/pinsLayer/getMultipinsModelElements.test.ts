import { MultiPinModelElement } from '@/types/modelElement';
import { PinType } from '@/types/pin';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { getMultipinsModelElements } from './getMultipinsModelElements';

const ZERO_MULTI_BIO_ENTITIES: MultiPinModelElement = [
  {
    ...modelElementFixture,
    type: 'modelElement' as PinType,
  },
  {
    ...modelElementFixture,
    x: 1000,
    type: 'modelElement' as PinType,
  },
];

const ONE_MULTI_BIO_ENTITIES: MultiPinModelElement = [
  {
    ...modelElementFixture,
    type: 'modelElement' as PinType,
    x: 100,
    y: 100,
  },
  {
    ...modelElementFixture,
    type: 'drugs' as PinType,
    x: 100,
    y: 100,
  },
];

const FEW_MULTI_BIO_ENTITIES_WITH_MULTIPLIED_TYPE: MultiPinModelElement = [
  {
    ...modelElementFixture,
    type: 'modelElement' as PinType,
    x: 100,
    y: 100,
  },
  {
    ...modelElementFixture,
    type: 'drugs' as PinType,
    x: 100,
    y: 100,
  },
  {
    ...modelElementFixture,
    type: 'drugs' as PinType,
    x: 100,
    y: 100,
  },
  {
    ...modelElementFixture,
    type: 'drugs' as PinType,
    x: 100,
    y: 100,
  },
];

describe('getMultipinsBioEntities - util', () => {
  it('should return empty array if theres no multi pins', () => {
    expect(getMultipinsModelElements({ modelElements: ZERO_MULTI_BIO_ENTITIES })).toStrictEqual([]);
  });

  it('should return valid multi pins', () => {
    expect(getMultipinsModelElements({ modelElements: ONE_MULTI_BIO_ENTITIES })).toStrictEqual([
      ONE_MULTI_BIO_ENTITIES,
    ]);
  });

  it('should return valid multi pins if theres few types of pins', () => {
    expect(
      getMultipinsModelElements({ modelElements: FEW_MULTI_BIO_ENTITIES_WITH_MULTIPLIED_TYPE }),
    ).toStrictEqual([ONE_MULTI_BIO_ENTITIES]);
  });
});

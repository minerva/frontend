import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import VectorLayer from 'ol/layer/Vector';
import { useOlMapPinsLayer } from './useOlMapPinsLayer';

describe('useOlMapPinsLayer - util', () => {
  const { Wrapper } = getReduxWrapperWithStore();

  it('should return VectorLayer', () => {
    const { result } = renderHook(() => useOlMapPinsLayer(), {
      wrapper: Wrapper,
    });

    expect(result.current).toBeInstanceOf(VectorLayer);
    expect(result.current.getSourceState()).toBe('ready');
  });
});

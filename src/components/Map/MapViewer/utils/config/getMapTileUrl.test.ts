import { BASE_MAP_IMAGES_URL } from '@/constants';
import { getMapTileUrl } from './getMapTileUrl';

describe('getMapTileUrl - util', () => {
  describe('when projectDirectory is empty', () => {
    it('should return empty value', () => {
      const projectDirectory = undefined;
      const currentBackgroundImagePath = 'currentBackgroundImagePath';
      const result = '';

      expect(
        getMapTileUrl({
          projectDirectory,
          currentBackgroundImagePath,
        }),
      ).toBe(result);
    });
  });

  describe('when all args are valid', () => {
    it('should return correct value', () => {
      const projectDirectory = 'directory';
      const currentBackgroundImagePath = 'currentBackgroundImagePath';
      const result = `${BASE_MAP_IMAGES_URL}/map_images/${projectDirectory}/${currentBackgroundImagePath}/{z}/{x}/{y}.PNG`;

      expect(
        getMapTileUrl({
          projectDirectory,
          currentBackgroundImagePath,
        }),
      ).toBe(result);
    });
  });
});

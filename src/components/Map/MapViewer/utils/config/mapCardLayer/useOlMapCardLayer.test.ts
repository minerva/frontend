/* eslint-disable no-magic-numbers */
import { MAP_DATA_INITIAL_STATE, OPENED_MAPS_INITIAL_STATE } from '@/redux/map/map.constants';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import BaseLayer from 'ol/layer/Base';
import VectorLayer from 'ol/layer/Vector';
import React from 'react';
import { useOlMapCardLayer } from './useOlMapCardLayer';

const useRefValue = {
  current: null,
};

Object.defineProperty(useRefValue, 'current', {
  get: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
  set: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
});

jest.spyOn(React, 'useRef').mockReturnValue(useRefValue);

describe('useOlMapCardLayer - util', () => {
  const getRenderedHookResults = (): BaseLayer => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        data: {
          ...MAP_DATA_INITIAL_STATE,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
          position: {
            initial: {
              x: 256,
              y: 256,
            },
            last: {
              x: 256,
              y: 256,
            },
          },
        },
        loading: 'idle',
        error: {
          name: '',
          message: '',
        },
        openedMaps: OPENED_MAPS_INITIAL_STATE,
      },
    });

    const { result } = renderHook(() => useOlMapCardLayer(), {
      wrapper: Wrapper,
    });

    return result.current;
  };

  it('should return valid VectorLayer instance', () => {
    const result = getRenderedHookResults();

    expect(result).toBeInstanceOf(VectorLayer);
    expect(result.getSourceState()).toBe('ready');
  });
});

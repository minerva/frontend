import { OverlayBioEntityRender } from '@/types/OLrendering';
import { MarkerSurface } from '@/types/models';
import { parseSurfaceMarkersToBioEntityRender } from './parseSurfaceMarkersToBioEntityRender';

const MARKERS: MarkerSurface[] = [
  {
    type: 'surface',
    id: '1',
    color: '#000000',
    opacity: 0.1,
    x: 1200,
    y: 500,
    width: 100,
    height: 50,
    number: 0,
    modelId: 0,
  },
  {
    type: 'surface',
    id: '2',
    color: '#FF0000',
    opacity: 0.67,
    x: 432,
    y: 2343,
    width: 100,
    height: 50,
    number: 23,
    modelId: 33,
  },
  {
    type: 'surface',
    id: '3',
    color: '#FFFFFF',
    opacity: 0,
    x: 1,
    y: 1,
    width: 2,
    height: 2,
    number: 1,
    modelId: 1,
  },
];

const EXPECTED_RETURN: OverlayBioEntityRender[] = [
  {
    color: null,
    height: 50,
    hexColor: '#0000001a',
    id: '1',
    modelId: 0,
    overlayId: 0,
    type: 'rectangle',
    value: 0.1,
    width: 100,
    x1: 1200,
    x2: 1300,
    y1: 550,
    y2: 500,
  },
  {
    color: null,
    height: 50,
    hexColor: '#FF0000ab',
    id: '2',
    modelId: 33,
    overlayId: 0,
    type: 'rectangle',
    value: 0.67,
    width: 100,
    x1: 432,
    x2: 532,
    y1: 2393,
    y2: 2343,
  },
  {
    color: null,
    height: 2,
    hexColor: '#FFFFFF00',
    id: '3',
    modelId: 1,
    overlayId: 0,
    type: 'rectangle',
    value: 0,
    width: 2,
    x1: 1,
    x2: 3,
    y1: 3,
    y2: 1,
  },
];

describe('parseSurfaceMarkersToBioEntityRender - util', () => {
  it('returns correctly parsed markers for every element', () => {
    expect(parseSurfaceMarkersToBioEntityRender(MARKERS)).toStrictEqual(EXPECTED_RETURN);
  });
});

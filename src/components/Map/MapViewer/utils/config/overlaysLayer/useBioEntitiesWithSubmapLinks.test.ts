/* eslint-disable no-magic-numbers */
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { CONFIGURATION_INITIAL_STORE_MOCKS } from '@/redux/configuration/configuration.mock';
import { PUBLIC_OVERLAYS_MOCK } from '@/redux/overlays/overlays.mock';
import { mapStateWithCurrentlySelectedMainMapFixture } from '@/redux/map/map.fixtures';
import {
  MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_DIFFERENT_COLORS,
  MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_SAME_COLORS,
  OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
} from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import {
  MODEL_ELEMENT_LINKING_TO_SUBMAP,
  MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
} from '@/redux/modelElements/modelElements.mock';
import { useBioEntitiesWithSubmapsLinks } from './useBioEntitiesWithSubmapLinks';

const RESULT_SUBMAP_LINKS_DIFFERENT_VALUES = [
  {
    type: 'submap-link',
    id: 97,
    modelId: 52,
    amount: 0,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2100.5,
    y2: 2000.5,
    overlayId: 12,
    height: 100,
    value: Infinity,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 1,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2025.5,
    y2: 2000.5,
    overlayId: 12,
    height: 25,
    value: 0.8,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 1,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2050.5,
    y2: 2025.5,
    overlayId: 12,
    height: 25,
    value: 0.5,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 1,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2075.5,
    y2: 2050.5,
    overlayId: 12,
    height: 25,
    value: 0.4,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 1,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2100.5,
    y2: 2075.5,
    overlayId: 12,
    height: 25,
    value: null,
    color: {
      alpha: 255,
      rgb: -2348283,
    },
  },
  {
    type: 'rectangle',
    id: 1,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 3128.653195488725,
    y2: 3088.653195488725,
    overlayId: 12,
    height: 10,
    value: 0,
    color: null,
  },
];

export const RESULT_SUBMAP_LINKS_SAME_COLORS = [
  {
    type: 'submap-link',
    amount: 0,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2100.5,
    y2: 2000.5,
    overlayId: 12,
    height: 100,
    value: Infinity,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 2,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2050.5,
    y2: 2000.5,
    overlayId: 12,
    height: 50,
    value: 23,
    color: null,
  },
  {
    type: 'submap-link',
    amount: 2,
    id: 97,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 2100.5,
    y2: 2050.5,
    overlayId: 12,
    height: 50,
    value: null,
    color: {
      alpha: 255,
      rgb: -2348283,
    },
  },
  {
    type: 'rectangle',
    id: 1,
    modelId: 52,
    width: 30,
    x1: 18412,
    x2: 18492,
    y1: 3128.653195488725,
    y2: 3088.653195488725,
    overlayId: 12,
    height: 10,
    value: 0,
    color: null,
  },
];

describe('useBioEntitiesWithSubmapsLinks', () => {
  it('should return bioEntities without submaps links if no submaps links are present', () => {
    const { Wrapper } = getReduxStoreWithActionsListener({
      overlayBioEntity: {
        ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
        overlaysId: PUBLIC_OVERLAYS_MOCK.map(o => o.id),
      },
      configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
      modelElements: {
        data: {
          52: {
            data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
      },
      map: mapStateWithCurrentlySelectedMainMapFixture,
    });

    const {
      result: { current },
    } = renderHook(() => useBioEntitiesWithSubmapsLinks(), {
      wrapper: Wrapper,
    });

    expect(current).toEqual([]);
  });

  describe('submap links with the same ID and overlayID but different values or colors', () => {
    it('should create submap link with Infinity value, for displaying black border of submap link', () => {
      const { Wrapper } = getReduxStoreWithActionsListener({
        overlayBioEntity: {
          overlaysId: [12],
          data: {
            12: {
              52: MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_DIFFERENT_COLORS,
            },
          },
        },
        modelElements: {
          data: {
            52: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
        map: mapStateWithCurrentlySelectedMainMapFixture,

        models: {
          ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
        },
      });

      const {
        result: { current },
      } = renderHook(() => useBioEntitiesWithSubmapsLinks(), {
        wrapper: Wrapper,
      });

      expect(current[0]).toEqual({
        type: 'submap-link',
        amount: 0,
        id: 97,
        modelId: 52,
        width: 30,
        x1: 18412,
        x2: 18492,
        y1: 2100.5,
        y2: 2000.5,
        overlayId: 12,
        height: 100,
        value: Infinity,
        color: null,
      });
    });

    it('should modify height, coordinates and return in sorted order to create submap link from several submap link rectangles', () => {
      const { Wrapper } = getReduxStoreWithActionsListener({
        overlayBioEntity: {
          overlaysId: [12],
          data: {
            12: {
              52: MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_DIFFERENT_COLORS,
            },
          },
        },
        modelElements: {
          data: {
            52: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
        map: mapStateWithCurrentlySelectedMainMapFixture,

        models: {
          ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
        },
      });

      const {
        result: { current },
      } = renderHook(() => useBioEntitiesWithSubmapsLinks(), {
        wrapper: Wrapper,
      });

      expect(current).toStrictEqual(RESULT_SUBMAP_LINKS_DIFFERENT_VALUES);
    });
  });

  describe('submap links with the same ID and overlayID and the same values or colors', () => {
    it('should create submap link with Infinity value, for displaying black border of submap link', () => {
      const { Wrapper } = getReduxStoreWithActionsListener({
        overlayBioEntity: {
          overlaysId: [12],
          data: {
            12: {
              52: MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_SAME_COLORS,
            },
          },
        },
        modelElements: {
          data: {
            52: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
        map: mapStateWithCurrentlySelectedMainMapFixture,

        models: {
          ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
        },
      });

      const {
        result: { current },
      } = renderHook(() => useBioEntitiesWithSubmapsLinks(), {
        wrapper: Wrapper,
      });

      expect(current[0]).toEqual({
        type: 'submap-link',
        amount: 0,
        id: 97,
        modelId: 52,
        width: 30,
        x1: 18412,
        x2: 18492,
        y1: 2100.5,
        y2: 2000.5,
        overlayId: 12,
        height: 100,
        value: Infinity,
        color: null,
      });
    });

    it('should modify height, coordinates and return in sorted order to create submap link from several submap link rectangles', () => {
      const { Wrapper } = getReduxStoreWithActionsListener({
        overlayBioEntity: {
          overlaysId: [12],
          data: {
            12: {
              52: MOCKED_OVERLAY_SUBMAPS_LINKS_WITH_SAME_COLORS,
            },
          },
        },
        modelElements: {
          data: {
            52: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
        map: mapStateWithCurrentlySelectedMainMapFixture,

        models: {
          ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
        },
      });

      const {
        result: { current },
      } = renderHook(() => useBioEntitiesWithSubmapsLinks(), {
        wrapper: Wrapper,
      });

      expect(current).toStrictEqual(RESULT_SUBMAP_LINKS_SAME_COLORS);
    });
  });
});

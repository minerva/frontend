import { LINE_WIDTH } from '@/constants/canvas';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { LinePoint } from '@/types/reactions';
import { usePointToProjection } from '@/utils/map/usePointToProjection';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { Feature } from 'ol';
import { Geometry } from 'ol/geom';
import { createOverlayLineFeature } from './createOverlayLineFeature';

/* eslint-disable no-magic-numbers */
const CASES: [LinePoint, number[]][] = [
  [
    [
      { x: 0, y: 0 },
      { x: 0, y: 0 },
    ],
    [0, 0, 0, 0],
  ],
  [
    [
      { x: 0, y: 0 },
      { x: 100, y: 100 },
    ],
    [0, -238107693, Infinity, 0],
  ],
  [
    [
      { x: 100, y: 100 },
      { x: 0, y: 0 },
    ],
    [0, -238107693, Infinity, 0],
  ],
  [
    [
      { x: 100, y: 0 },
      { x: 0, y: 100 },
    ],
    [0, 0, 0, 0],
  ],
  [
    [
      { x: -50, y: 0 },
      { x: 0, y: -50 },
    ],
    [0, 0, 0, 0],
  ],
];

const COLOR = '#FFB3B3cc';

const getFeature = (linePoint: LinePoint): Feature<Geometry> => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });
  const {
    result: { current: pointToProjection },
  } = renderHook(() => usePointToProjection(), {
    wrapper: Wrapper,
  });

  return createOverlayLineFeature(linePoint, { pointToProjection, color: COLOR });
};

describe('createOverlayLineFeature - util', () => {
  it.each(CASES)('should return Feature instance', linePoint => {
    const feature = getFeature(linePoint);

    expect(feature).toBeInstanceOf(Feature);
  });

  it.each(CASES)('should return Feature instance with valid style and stroke', linePoint => {
    const feature = getFeature(linePoint);
    const style = feature.getStyle();

    expect(style).toMatchObject({
      fill_: { color_: COLOR },
      stroke_: {
        color_: COLOR,
        width_: LINE_WIDTH,
      },
    });
  });

  it.each(CASES)('should return Feature instance with valid geometry', (linePoint, extent) => {
    const feature = getFeature(linePoint);
    const geometry = feature.getGeometry();

    expect(geometry?.getExtent()).toEqual(extent);
  });
});

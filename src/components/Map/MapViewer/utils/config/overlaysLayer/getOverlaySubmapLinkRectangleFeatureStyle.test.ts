/* eslint-disable no-magic-numbers */
import { Fill, Stroke, Style } from 'ol/style';
import { getOverlaySubmapLinkRectangleFeatureStyle } from './getOverlaySubmapLinkRectangleFeatureStyle';

const COLORS = ['#000000', '#FFFFFF', '#F5F5F5', '#C0C0C0', '#C0C0C0aa', '#C0C0C0bb'];

describe('getOverlaySubmapLinkRectangleFeatureStyle - util', () => {
  it.each(COLORS)('should return Style object', color => {
    const result = getOverlaySubmapLinkRectangleFeatureStyle(color);
    expect(result).toBeInstanceOf(Style);
  });

  it.each(COLORS)('should set valid color values for fill', color => {
    const result = getOverlaySubmapLinkRectangleFeatureStyle(color);
    const fill = result.getFill();
    expect(fill).toBeInstanceOf(Fill);
    expect(fill?.getColor()).toBe(color);
  });

  it.each(COLORS)('should set valid color values for fill', color => {
    const result = getOverlaySubmapLinkRectangleFeatureStyle(color);
    const stroke = result.getStroke();
    expect(stroke).toBeInstanceOf(Stroke);
    expect(stroke?.getColor()).toBe(color);
    expect(stroke?.getWidth()).toBe(1);
  });
  it('should set transparent fill and black stroke if color is null', () => {
    const result = getOverlaySubmapLinkRectangleFeatureStyle(null);
    const stroke = result.getStroke();
    expect(stroke).toBeInstanceOf(Stroke);
    expect(stroke?.getColor()).toBe('black');
    expect(stroke?.getWidth()).toBe(1);
    const fill = result.getFill();
    expect(fill).toBeInstanceOf(Fill);
    expect(fill?.getColor()).toBe('transparent');
  });
});

import { CONFIGURATION_INITIAL_STORE_MOCKS } from '@/redux/configuration/configuration.mock';
import { OverlayBioEntityRender } from '@/types/OLrendering';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useGetOverlayColor } from './useGetOverlayColor';

describe('useOverlayFeatures - hook', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    configuration: {
      ...CONFIGURATION_INITIAL_STORE_MOCKS,
    },
  });
  const {
    result: {
      current: { getOverlayBioEntityColorByAvailableProperties },
    },
  } = renderHook(() => useGetOverlayColor(), {
    wrapper: Wrapper,
  });

  describe('getOverlayBioEntityColorByAvailableProperties - function', () => {
    const ENTITY: OverlayBioEntityRender = {
      type: 'rectangle',
      id: 0,
      modelId: 0,
      x1: 0,
      y1: 0,
      x2: 0,
      y2: 0,
      width: 0,
      height: 0,
      value: null,
      overlayId: 0,
      color: null,
    };

    it('should return color based on value', () => {});

    it('should return the correct result if entity has a value equal to 0', () => {
      const entity = { ...ENTITY, value: 0 };

      expect(getOverlayBioEntityColorByAvailableProperties(entity)).toEqual('#FFFFFFcc');
    });

    it('should return the result if entity has a value', () => {
      const entity = { ...ENTITY, value: -0.2137 };

      expect(getOverlayBioEntityColorByAvailableProperties(entity)).toEqual('#FFC9C9cc');
    });

    it('should return the correct result if entity has a color but no value', () => {
      const entity = { ...ENTITY, color: { rgb: -65536, alpha: 0 } }; // red  color

      expect(getOverlayBioEntityColorByAvailableProperties(entity)).toEqual('#ff0000cc');
    });

    it('should return the default color if entity has neither a value nor a color', () => {
      const entity = { ...ENTITY };

      expect(getOverlayBioEntityColorByAvailableProperties(entity)).toEqual('#00FF00cc');
    });
  });
});

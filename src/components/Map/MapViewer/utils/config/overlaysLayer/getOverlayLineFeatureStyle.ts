import { LINE_WIDTH } from '@/constants/canvas';
import { Fill, Stroke, Style } from 'ol/style';

export const getOverlayLineFeatureStyle = (color: string): Style =>
  new Style({ fill: new Fill({ color }), stroke: new Stroke({ color, width: LINE_WIDTH }) });

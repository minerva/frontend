/* eslint-disable no-magic-numbers */
import { Fill, Stroke, Style } from 'ol/style';

export const getOverlaySubmapLinkRectangleFeatureStyle = (color: string | null): Style =>
  new Style({
    fill: new Fill({ color: color || 'transparent' }),
    stroke: new Stroke({ color: color || 'black', width: 1 }),
    zIndex: color ? 0 : 1,
  });

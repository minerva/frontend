import { LINE_WIDTH } from '@/constants/canvas';
import { Fill, Stroke, Style } from 'ol/style';
import { getOverlayLineFeatureStyle } from './getOverlayLineFeatureStyle';

const COLORS = ['#000000', '#FFFFFF', '#F5F5F5', '#C0C0C0', '#C0C0C0aa', '#C0C0C0bb'];

describe('getOverlayLineFeatureStyle - util', () => {
  it.each(COLORS)('should return Style object', color => {
    const result = getOverlayLineFeatureStyle(color);
    expect(result).toBeInstanceOf(Style);
  });

  it.each(COLORS)('should set valid color values for Fill', color => {
    const result = getOverlayLineFeatureStyle(color);
    const fill = result.getFill();
    expect(fill).toBeInstanceOf(Fill);
    expect(fill?.getColor()).toBe(color);
  });

  it.each(COLORS)('should set valid color values for Fill', color => {
    const result = getOverlayLineFeatureStyle(color);
    const stroke = result.getStroke();
    expect(stroke).toBeInstanceOf(Stroke);
    expect(stroke?.getColor()).toBe(color);
    expect(stroke?.getWidth()).toBe(LINE_WIDTH);
  });
});

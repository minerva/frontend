/* eslint-disable no-magic-numbers */
import { CONFIGURATION_INITIAL_STORE_MOCKS } from '@/redux/configuration/configuration.mock';
import { mapStateWithCurrentlySelectedMainMapFixture } from '@/redux/map/map.fixtures';
import { SURFACE_MARKER } from '@/redux/models/marker.mock';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { MOCKED_OVERLAY_BIO_ENTITY_RENDER } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import {
  OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
  USER_OVERLAYS_MOCK,
} from '@/redux/overlays/overlays.mock';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useOverlayFeatures } from './useOverlayFeatures';
import * as getPolygonLatitudeCoordinates from './getPolygonLatitudeCoordinates';

/**
 * mocks for useOverlayFeatures
 * are taken from helpers functions that are used inside useOverlayFeatures
 * point of the test is to test if all helper functions work correctly when combined together
 */

jest.mock('./getPolygonLatitudeCoordinates', () => ({
  __esModule: true,
  ...jest.requireActual('./getPolygonLatitudeCoordinates'),
}));

const getPolygonLatitudeCoordinatesSpy = jest.spyOn(
  getPolygonLatitudeCoordinates,
  'getPolygonLatitudeCoordinates',
);

describe('useOverlayFeatures', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  const { Wrapper } = getReduxWrapperWithStore({
    configuration: {
      ...CONFIGURATION_INITIAL_STORE_MOCKS,
    },
    overlayBioEntity: {
      overlaysId: [11, 12],
      data: {
        // overlayId
        11: {
          // modelId
          52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
        },
        12: {
          52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
        },
      },
    },
    overlays: {
      ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
    },
    map: {
      ...mapStateWithCurrentlySelectedMainMapFixture,
    },
    models: {
      ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
    },
    markers: {
      data: [SURFACE_MARKER],
    },
  });

  it('should return an array of features', () => {
    const {
      result: { current: features },
    } = renderHook(() => useOverlayFeatures(), {
      wrapper: Wrapper,
    });

    expect(features).toHaveLength(11);

    // type: rectangle
    expect(features[0].getGeometry()?.getCoordinates()).toEqual([
      [
        [-13149141, 18867005],
        [-13149141, 18881970],
        [-13143529, 18881970],
        [-13143529, 18867005],
        [-13149141, 18867005],
      ],
    ]);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(features[0].getStyle().getFill().getColor()).toBe('#FFFFFFcc');

    // type: line
    expect(features[7].getGeometry()?.getCoordinates()).toEqual([
      [
        [-13149141, 18867005],
        [-13149141, 18881970],
        [-13141659, 18881970],
        [-13141659, 18867005],
        [-13149141, 18867005],
      ],
    ]);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(features[7].getStyle().getFill().getColor()).toBe('#ff0000cc');

    // type: rectangle (marker)
    expect(features[10].getGeometry()?.getCoordinates()).toEqual([
      [
        [-19588560, 19831740],
        [-19588560, 19850446],
        [-19551147, 19850446],
        [-19551147, 19831740],
        [-19588560, 19831740],
      ],
    ]);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    expect(features[10].getStyle().getFill().getColor()).toBe('#0000001a');
  });

  it('should get coordinates only for active shared overlays and user overlays if exist', () => {
    const { Wrapper: OverlayFeaturesWrapper } = getReduxWrapperWithStore({
      configuration: {
        ...CONFIGURATION_INITIAL_STORE_MOCKS,
      },
      overlayBioEntity: {
        overlaysId: [11, 12, 99, 123],
        data: {
          // overlayId
          11: {
            // modelId
            52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
            53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          },
          12: {
            52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
            53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          },
          99: {
            52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
            53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          },
          123: {
            52: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
            53: MOCKED_OVERLAY_BIO_ENTITY_RENDER,
          },
        },
      },
      overlays: {
        ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
        userOverlays: {
          data: USER_OVERLAYS_MOCK,
          error: { message: '', name: '' },
          loading: 'succeeded',
        },
      },
      map: {
        ...mapStateWithCurrentlySelectedMainMapFixture,
      },
      models: {
        ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
      },
    });

    renderHook(() => useOverlayFeatures(), {
      wrapper: OverlayFeaturesWrapper,
    });

    expect(getPolygonLatitudeCoordinatesSpy).toHaveBeenLastCalledWith({
      nOverlays: 4,
      overlayIndexBasedOnOrder: 0,
      width: 8.923194537814197,
      xMin: 4454.850442288663,
    });
  });
  it('should not get coordinates for active overlays if no overlay is active', () => {
    const { Wrapper: OverlayFeaturesWrapper } = getReduxWrapperWithStore({
      configuration: {
        ...CONFIGURATION_INITIAL_STORE_MOCKS,
      },
      overlayBioEntity: {
        overlaysId: [],
        data: {},
      },
      overlays: {
        ...OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
        userOverlays: {
          data: USER_OVERLAYS_MOCK,
          error: { message: '', name: '' },
          loading: 'succeeded',
        },
      },
      map: {
        ...mapStateWithCurrentlySelectedMainMapFixture,
      },
      models: {
        ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
      },
    });

    renderHook(() => useOverlayFeatures(), {
      wrapper: OverlayFeaturesWrapper,
    });

    expect(getPolygonLatitudeCoordinatesSpy).not.toHaveBeenCalled();
  });
});

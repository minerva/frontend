import { FEATURE_TYPE } from '@/constants/features';
import { createOverlayGeometryFeature } from './createOverlayGeometryFeature';

describe('createOverlayGeometryFeature', () => {
  it('should create a feature with the correct geometry and style', () => {
    const xMin = 0;
    const yMin = 0;
    const xMax = 10;
    const yMax = 10;
    const colorHexString = '#FF0000';
    const entityId = 2007;

    const feature = createOverlayGeometryFeature(
      [xMin, yMin, xMax, yMax],
      colorHexString,
      entityId,
    );

    expect(feature.getGeometry()!.getCoordinates()).toEqual([
      [
        [xMin, yMin],
        [xMin, yMax],
        [xMax, yMax],
        [xMax, yMin],
        [xMin, yMin],
      ],
    ]);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore - getStyle() is not typed
    expect(feature.getStyle().getFill().getColor()).toEqual(colorHexString);

    expect(feature.get('id')).toBe(entityId);
    expect(feature.get('type')).toBe(FEATURE_TYPE.SURFACE_OVERLAY);
  });

  it('should create a feature with the correct geometry and style when using a different color', () => {
    const xMin = -5;
    const yMin = -5;
    const xMax = 5;
    const yMax = 5;
    const colorHexString = '#00FF00';
    const entityId = 'a6e21d64-fd3c-4f7c-8acc-5fc305f4395a';

    const feature = createOverlayGeometryFeature(
      [xMin, yMin, xMax, yMax],
      colorHexString,
      entityId,
    );

    expect(feature.getGeometry()!.getCoordinates()).toEqual([
      [
        [xMin, yMin],
        [xMin, yMax],
        [xMax, yMax],
        [xMax, yMin],
        [xMin, yMin],
      ],
    ]);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore - getStyle() is not typed
    expect(feature.getStyle().getFill().getColor()).toEqual(colorHexString);

    expect(feature.get('id')).toBe(entityId);
    expect(feature.get('type')).toBe(FEATURE_TYPE.SURFACE_MARKER);
  });
});

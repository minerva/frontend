import { getPolygonLatitudeCoordinates } from './getPolygonLatitudeCoordinates';

describe('getPolygonLatitudeCoordinates', () => {
  const cases = [
    {
      width: 80,
      nOverlays: 3,
      xMin: 2137.5,
      overlayIndexBasedOnOrder: 2,
      expected: { xMin: 2190.83, xMax: 2217.5 },
    },
    {
      width: 120,
      nOverlays: 6,
      xMin: 2137.5,
      overlayIndexBasedOnOrder: 5,
      expected: { xMin: 2237.5, xMax: 2257.5 },
    },
    {
      width: 40,
      nOverlays: 1,
      xMin: 2137.5,
      overlayIndexBasedOnOrder: 0,
      expected: { xMin: 2137.5, xMax: 2177.5 },
    },
  ];

  it.each(cases)(
    'should return the correct latitude coordinates for width=$width, nOverlays=$nOverlays, xMin=$xMin, and overlayIndexBasedOnOrder=$overlayIndexBasedOnOrder',
    ({ width, nOverlays, xMin, overlayIndexBasedOnOrder, expected }) => {
      const result = getPolygonLatitudeCoordinates({
        width,
        nOverlays,
        xMin,
        overlayIndexBasedOnOrder,
      });
      expect(result).toEqual(expected);
    },
  );
});

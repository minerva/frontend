/* eslint-disable no-magic-numbers */
import Feature from 'ol/Feature';
import { createOverlaySubmapLinkRectangleFeature } from './createOverlaySubmapLinkRectangleFeature';

const COLOR = '#FFFFFFcc';

const CASES = [
  [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
  ],
  [
    [0, 0, 100, 100],
    [0, 0, 100, 100],
  ],
  [
    [100, 0, 230, 100],
    [100, 0, 230, 100],
  ],
  [
    [-50, 0, 0, 50],
    [-50, 0, 0, 50],
  ],
];

describe('createOverlaySubmapLinkRectangleFeature - util', () => {
  it.each(CASES)('should return Feature instance', points => {
    const feature = createOverlaySubmapLinkRectangleFeature(points, COLOR, 1234);

    expect(feature).toBeInstanceOf(Feature);
  });

  it.each(CASES)('should return Feature instance with valid style and stroke', points => {
    const feature = createOverlaySubmapLinkRectangleFeature(points, COLOR, 1234);
    const style = feature.getStyle();

    expect(style).toMatchObject({
      fill_: { color_: COLOR },
      stroke_: {
        color_: COLOR,
        width_: 1,
      },
    });
  });
  it('should return object with transparent fill and black stroke color when color is null', () => {
    const feature = createOverlaySubmapLinkRectangleFeature([0, 0, 0, 0], null, 1234);
    const style = feature.getStyle();

    expect(style).toMatchObject({
      fill_: { color_: 'transparent' },
      stroke_: {
        color_: 'black',
        width_: 1,
      },
    });
  });
  it.each(CASES)('should return Feature instance with valid geometry', (points, extent) => {
    const feature = createOverlaySubmapLinkRectangleFeature(points, COLOR, 1234);
    const geometry = feature.getGeometry();

    expect(geometry?.getExtent()).toEqual(extent);
  });

  it('should throw error if extent is not valid', () => {
    expect(() => createOverlaySubmapLinkRectangleFeature([100, 100, 0, 0], COLOR, 1234)).toThrow();
  });
});

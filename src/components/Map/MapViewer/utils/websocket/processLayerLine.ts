import { WebSocketEntityUpdateInterface } from '@/utils/websocket-entity-updates/webSocketEntityUpdates.types';
import { store } from '@/redux/store';
import { ENTITY_OPERATION_TYPES } from '@/utils/websocket-entity-updates/webSocketEntityUpdates.constants';
import { MapInstance } from '@/types/map';
import drawElementOnLayer from '@/components/Map/MapViewer/utils/shapes/layer/utils/drawElementOnLayer';
import { getLayerLine } from '@/redux/layers/layers.thunks';
import updateElement from '@/components/Map/MapViewer/utils/shapes/layer/utils/updateElement';
import { layerDeleteLine } from '@/redux/layers/layers.slice';
import removeElementFromLayer from '@/components/Map/MapViewer/utils/shapes/layer/utils/removeElementFromLayer';

export default async function processLayerLine({
  data,
  mapInstance,
}: {
  data: WebSocketEntityUpdateInterface;
  mapInstance: MapInstance;
}): Promise<void> {
  const { dispatch } = store;
  if (
    data.type === ENTITY_OPERATION_TYPES.ENTITY_CREATED ||
    data.type === ENTITY_OPERATION_TYPES.ENTITY_UPDATED
  ) {
    const resultLine = await dispatch(
      getLayerLine({
        modelId: data.mapId,
        layerId: data.layerId,
        lineId: data.entityId,
      }),
    ).unwrap();
    if (!resultLine) {
      return;
    }
    if (data.type === ENTITY_OPERATION_TYPES.ENTITY_CREATED) {
      drawElementOnLayer({
        mapInstance,
        activeLayer: data.layerId,
        object: resultLine,
        drawFunctionKey: 'drawLine',
      });
    } else {
      updateElement(mapInstance, data.layerId, resultLine);
    }
  } else if (data.type === ENTITY_OPERATION_TYPES.ENTITY_DELETED) {
    dispatch(
      layerDeleteLine({
        modelId: data.mapId,
        layerId: data.layerId,
        lineId: data.entityId,
      }),
    );
    removeElementFromLayer({ mapInstance, layerId: data.layerId, featureId: data.entityId });
  }
}

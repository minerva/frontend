/* eslint-disable no-magic-numbers */
import { Stroke } from 'ol/style';
import getScaledStrokeStyle from './getScaledStrokeStyle';

describe('getScaledStrokeStyle', () => {
  it('should correctly scale the width and lineDash array of a Stroke style', () => {
    const strokeStyle = new Stroke({
      color: 'red',
      width: 2,
      lineDash: [4, 8],
    });
    const scale = 0.2;

    const scaledStroke = getScaledStrokeStyle(strokeStyle, scale);

    expect(scaledStroke.getColor()).toBe('red');
    expect(scaledStroke.getWidth()).toBe(0.4);
    expect(scaledStroke.getLineDash()).toEqual([0.8, 1.6]);
  });

  it('should use a default width of 1 and scale it correctly when getWidth() returns undefined', () => {
    const strokeStyle = new Stroke({
      color: 'green',
      lineDash: [2, 3],
    });
    jest.spyOn(strokeStyle, 'getWidth').mockReturnValue(undefined);
    const scale = 3;

    const scaledStroke = getScaledStrokeStyle(strokeStyle, scale);

    expect(scaledStroke.getColor()).toBe('green');
    expect(scaledStroke.getWidth()).toBe(3);
    expect(scaledStroke.getLineDash()).toEqual([6, 9]);
  });
});

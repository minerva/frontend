/* eslint-disable no-magic-numbers */
import Style from 'ol/style/Style';
import { Extent } from 'ol/extent';
import getWrappedTextWithFontSize from '@/components/Map/MapViewer/utils/shapes/text/getWrappedTextWithFontSize';
import getTextStyle from '@/components/Map/MapViewer/utils/shapes/text/getTextStyle';
import { latLngToPoint } from '@/utils/map/latLngToPoint';
import getCoverStyles from '@/components/Map/MapViewer/utils/shapes/style/getCoverStyles';
import { DEFAULT_TILE_SIZE } from '@/constants/map';

jest.mock('../text/getWrappedTextWithFontSize');
jest.mock('../text/getTextStyle');
jest.mock('../../../../../../utils/map/latLngToPoint');

describe('getCoverStyles', () => {
  it('should return cover and text styles based on the provided parameters', () => {
    const coverStyle = new Style();
    const largestExtent: Extent = [10, 10, 50, 50];
    const text = 'Sample Text';
    const scale = 1;
    const zIndex = 5;
    const mapSize = {
      width: 1000,
      height: 800,
      minZoom: 1,
      maxZoom: 9,
      tileSize: DEFAULT_TILE_SIZE,
    };

    (latLngToPoint as jest.Mock).mockImplementation(([lat, lng], size) => ({
      x: (lng * size.width) / 100,
      y: (lat * size.height) / 100,
    }));

    (getWrappedTextWithFontSize as jest.Mock).mockReturnValue({
      text: 'Sample\nText',
      fontSize: 12,
    });

    const mockTextStyle = new Style();
    (getTextStyle as jest.Mock).mockReturnValue(mockTextStyle);

    const result = getCoverStyles({ coverStyle, largestExtent, text, scale, zIndex, mapSize });

    expect(result).toHaveLength(2);
    expect(result[0]).toBe(coverStyle);
    expect(coverStyle.getZIndex()).toBe(zIndex);

    expect(result[1]).toBe(mockTextStyle);
    expect(getWrappedTextWithFontSize).toHaveBeenCalledWith({
      text,
      maxWidth: expect.any(Number),
      maxHeight: expect.any(Number),
    });

    expect(getTextStyle).toHaveBeenCalledWith({
      text: 'Sample\nText',
      fontSize: 12,
      color: '#000',
      zIndex,
      horizontalAlign: 'CENTER',
    });
  });

  it('should handle empty text gracefully', () => {
    const coverStyle = new Style();
    const largestExtent: Extent = [10, 10, 50, 50];
    const text = '';
    const scale = 1;
    const zIndex = 5;
    const mapSize = {
      width: 1000,
      height: 800,
      minZoom: 1,
      maxZoom: 9,
      tileSize: DEFAULT_TILE_SIZE,
    };

    (getWrappedTextWithFontSize as jest.Mock).mockReturnValue({
      text: '',
      fontSize: 0,
    });

    const result = getCoverStyles({ coverStyle, largestExtent, text, scale, zIndex, mapSize });

    expect(result).toHaveLength(1);
    expect(result[0]).toBe(coverStyle);
  });
});

/* eslint-disable no-magic-numbers */
import Style from 'ol/style/Style';
import { Geometry } from 'ol/geom';
import { BLACK_COLOR, WHITE_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import { Color } from '@/types/models';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import getFill from '@/components/Map/MapViewer/utils/shapes/style/getFill';

export default function getStyle({
  geometry,
  borderColor = BLACK_COLOR,
  fillColor = WHITE_COLOR,
  lineWidth = 1,
  lineDash = [],
  zIndex = 1,
}: {
  geometry?: Geometry;
  borderColor?: Color | string;
  fillColor?: Color | string;
  lineWidth?: number;
  lineDash?: Array<number>;
  zIndex?: number;
}): Style {
  return new Style({
    geometry,
    stroke: getStroke({
      color: typeof borderColor === 'string' ? borderColor : rgbToHex(borderColor),
      width: lineWidth,
      lineDash,
    }),
    fill: getFill({ color: typeof fillColor === 'string' ? fillColor : rgbToHex(fillColor) }),
    zIndex,
  });
}

/* eslint-disable no-magic-numbers */
import getStyle from '@/components/Map/MapViewer/utils/shapes/style/getStyle';
import Style from 'ol/style/Style';
import Polygon from 'ol/geom/Polygon';

describe('getStyle', () => {
  it('should return a Style object with the default values', () => {
    const result = getStyle({});
    expect(result).toBeInstanceOf(Style);
    expect(result.getGeometry()).toEqual(null);
    expect(result.getStroke()?.getWidth()).toEqual(1);
    expect(result.getStroke()?.getColor()).toEqual('#000000FF');
    expect(result.getStroke()?.getLineDash()).toEqual([]);
    expect(result.getFill()?.getColor()).toEqual('#FFFFFFFF');
    expect(result.getZIndex()).toEqual(1);
  });

  it('should return a Style object with the provided color and width', () => {
    const geometry = new Polygon([
      [
        [10, 10],
        [10, 10],
      ],
    ]);
    const borderColor = {
      alpha: 255,
      rgb: -16777216,
    };
    const fillColor = {
      alpha: 255,
      rgb: -5646081,
    };
    const lineWidth = 3;
    const lineDash = [10, 5];
    const zIndex = 2;
    const result = getStyle({
      geometry,
      borderColor,
      fillColor,
      lineWidth,
      lineDash,
      zIndex,
    });
    expect(result).toBeInstanceOf(Style);
    expect(result.getGeometry()).toEqual(geometry);
    expect(result.getStroke()?.getWidth()).toEqual(lineWidth);
    expect(result.getStroke()?.getColor()).toEqual('#000000FF');
    expect(result.getStroke()?.getLineDash()).toEqual(lineDash);
    expect(result.getFill()?.getColor()).toEqual('#A9D8FFFF');
    expect(result.getZIndex()).toEqual(zIndex);
  });
});

import { Color } from '@/types/models';
import { rgbToHex } from './rgbToHex';

describe('rgbToHex', () => {
  it('should correctly convert RGB and alpha values to hex', () => {
    const color: Color = {
      rgb: -16222216,
      alpha: 255,
    };

    const result = rgbToHex(color);

    expect(result).toBe('#0877F8FF');
  });

  it('should correctly handle alpha values less than 255', () => {
    const color: Color = {
      rgb: -16777216,
      alpha: 128,
    };

    const result = rgbToHex(color);

    expect(result).toBe('#00000080');
  });
});

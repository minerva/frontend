/* eslint-disable no-magic-numbers */
import { Stroke } from 'ol/style';
import getStroke from './getStroke';

describe('getStroke', () => {
  it('should return a Stroke object with the default color and width', () => {
    const result = getStroke({});
    expect(result).toBeInstanceOf(Stroke);
    expect(result.getColor()).toEqual('#000');
    expect(result.getWidth()).toEqual(1);
    expect(result.getLineDash()).toEqual([]);
    expect(result.getLineCap()).toEqual('butt');
  });

  it('should return a Stroke object with the provided values', () => {
    const color = '#ff0000';
    const width = 2;
    const lineDash = [10, 5];
    const lineCap = 'round';
    const result = getStroke({ color, width, lineDash, lineCap });
    expect(result).toBeInstanceOf(Stroke);
    expect(result.getColor()).toEqual(color);
    expect(result.getWidth()).toEqual(width);
    expect(result.getLineDash()).toEqual(lineDash);
    expect(result.getLineCap()).toEqual(lineCap);
  });
});

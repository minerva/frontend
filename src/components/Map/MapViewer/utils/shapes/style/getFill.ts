/* eslint-disable no-magic-numbers */
import { Fill } from 'ol/style';

export default function getFill({ color = '#fff' }: { color?: string }): Fill {
  return new Fill({
    color,
  });
}

import { Fill } from 'ol/style';
import getFill from './getFill';

describe('getFill', () => {
  it('should return a Fill object with the default color when no color is provided', () => {
    const result = getFill({});
    expect(result).toBeInstanceOf(Fill);
    expect(result.getColor()).toBe('#fff');
  });

  it('should return a Fill object with the provided color', () => {
    const color = '#ff0000';
    const result = getFill({ color });
    expect(result).toBeInstanceOf(Fill);
    expect(result.getColor()).toBe(color);
  });
});

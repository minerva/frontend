/* eslint-disable no-magic-numbers */
import Style from 'ol/style/Style';
import { Stroke, Text } from 'ol/style';
import getScaledStrokeStyle from '@/components/Map/MapViewer/utils/shapes/style/getScaledStrokeStyle';
import getScaledElementStyle from './getScaledElementStyle';

jest.mock('./getScaledStrokeStyle');

describe('getScaledElementStyle', () => {
  it('should scale the stroke style and text scale when strokeStyle is provided', () => {
    const mockScaledStroke = new Stroke({ color: 'blue', width: 4 });
    (getScaledStrokeStyle as jest.Mock).mockReturnValue(mockScaledStroke);

    const strokeStyle = new Stroke({ color: 'red', width: 2 });
    const textStyle = new Text({ text: 'Test', scale: 1 });
    const style = new Style({ stroke: strokeStyle, text: textStyle });
    const scale = 2;

    const scaledStyle = getScaledElementStyle(style, strokeStyle, scale);

    expect(getScaledStrokeStyle).toHaveBeenCalledWith(strokeStyle, scale);
    expect(scaledStyle.getStroke()).toBe(mockScaledStroke);
    expect(scaledStyle.getText()?.getScale()).toBe(2);
  });

  it('should scale only text when strokeStyle is not provided', () => {
    const textStyle = new Text({ text: 'Test', scale: 1 });
    const style = new Style({ text: textStyle });
    const scale = 3;

    const scaledStyle = getScaledElementStyle(style, undefined, scale);

    expect(scaledStyle.getStroke()).toBeNull();
    expect(scaledStyle.getText()?.getScale()).toBe(3);
  });
});

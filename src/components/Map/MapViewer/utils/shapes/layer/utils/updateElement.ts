import VectorSource from 'ol/source/Vector';
import { LayerImage, LayerOval, LayerRect, LayerText, LayerLine } from '@/types/models';
import { MapInstance } from '@/types/map';

export default function updateElement(
  mapInstance: MapInstance,
  layerId: number,
  layerObject: LayerImage | LayerText | LayerRect | LayerOval | LayerLine,
): void {
  mapInstance?.getAllLayers().forEach(layer => {
    if (layer.get('id') === layerId) {
      const source = layer.getSource();
      if (source instanceof VectorSource) {
        const feature = source.getFeatureById(layerObject.id);
        const update = feature?.get('updateElement');
        if (update && update instanceof Function) {
          update(layerObject);
          feature.changed();
        }
      }
    }
  });
}

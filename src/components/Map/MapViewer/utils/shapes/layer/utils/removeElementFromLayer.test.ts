/* eslint-disable no-magic-numbers */
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Feature } from 'ol';
import Map from 'ol/Map';
import removeElementFromLayer from './removeElementFromLayer';

jest.mock('ol/source/Vector');

describe('removeElementFromLayer', () => {
  let mockMapInstance: Map;
  let mockLayer: VectorLayer;
  let mockSource: VectorSource;
  let mockFeature: Feature;

  beforeEach(() => {
    mockFeature = new Feature();
    mockFeature.setId(123);

    mockSource = new VectorSource();
    jest.spyOn(mockSource, 'getFeatureById').mockImplementation(id => {
      if (id === 123) {
        return mockFeature;
      }
      return null;
    });
    jest.spyOn(mockSource, 'removeFeature').mockImplementation(() => {});

    mockLayer = new VectorLayer();
    jest.spyOn(mockLayer, 'get').mockImplementation(key => {
      if (key === 'id') {
        return 1;
      }
      return undefined;
    });
    jest.spyOn(mockLayer, 'getSource').mockImplementation(() => mockSource);

    const dummyElement = document.createElement('div');
    mockMapInstance = new Map({ target: dummyElement });
    jest.spyOn(mockMapInstance, 'getAllLayers').mockImplementation(() => [mockLayer]);
  });

  it('should remove the feature from the layer when feature exists', () => {
    removeElementFromLayer({
      mapInstance: mockMapInstance,
      layerId: 1,
      featureId: 123,
    });

    expect(mockSource.getFeatureById).toHaveBeenCalledWith(123);

    expect(mockSource.removeFeature).toHaveBeenCalledWith(mockFeature);
  });

  it('should not call removeFeature when feature does not exist', () => {
    removeElementFromLayer({
      mapInstance: mockMapInstance,
      layerId: 1,
      featureId: 999,
    });

    expect(mockSource.getFeatureById).toHaveBeenCalledWith(999);

    expect(mockSource.removeFeature).not.toHaveBeenCalled();
  });

  it('should not call removeFeature when layerId does not match', () => {
    removeElementFromLayer({
      mapInstance: mockMapInstance,
      layerId: 2,
      featureId: 123,
    });

    expect(mockSource.getFeatureById).not.toHaveBeenCalled();

    expect(mockSource.removeFeature).not.toHaveBeenCalled();
  });
});

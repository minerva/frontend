/* eslint-disable no-magic-numbers */
import VectorLayer from 'ol/layer/Vector';
import { Feature } from 'ol';
import Map from 'ol/Map';
import { layerImageFixture } from '@/models/fixtures/layerImageFixture';
import { layerTextFixture } from '@/models/fixtures/layerTextFixture';
import drawElementOnLayer from './drawElementOnLayer';

describe('drawElementOnLayer', () => {
  let mockMapInstance: Map;
  let mockLayer: VectorLayer;
  let mockFeature: Feature;
  let drawImageMock: () => void;
  let drawTextMock: () => void;

  beforeEach(() => {
    mockFeature = new Feature();
    mockFeature.setId(123);
    drawImageMock = jest.fn(() => {});
    drawTextMock = jest.fn(() => {});

    mockLayer = new VectorLayer();
    jest.spyOn(mockLayer, 'get').mockImplementation(key => {
      if (key === 'id') {
        return 1;
      }
      if (key === 'drawImage') {
        return drawImageMock;
      }
      if (key === 'drawText') {
        return drawTextMock;
      }
      return undefined;
    });

    const dummyElement = document.createElement('div');
    mockMapInstance = new Map({ target: dummyElement });
    jest.spyOn(mockMapInstance, 'getAllLayers').mockImplementation(() => [mockLayer]);
  });

  it('should call the drawImage function to draw a new image on the specified layer', () => {
    drawElementOnLayer({
      mapInstance: mockMapInstance,
      activeLayer: 1,
      object: layerImageFixture,
      drawFunctionKey: 'drawImage',
    });

    expect(mockLayer.get).toHaveBeenCalledTimes(2);
    expect(drawImageMock).toHaveBeenCalledWith(layerImageFixture);
  });

  it('should call the drawText function to draw a new text object on the specified layer', () => {
    drawElementOnLayer({
      mapInstance: mockMapInstance,
      activeLayer: 1,
      object: layerTextFixture,
      drawFunctionKey: 'drawText',
    });

    expect(mockLayer.get).toHaveBeenCalledTimes(2);
    expect(drawTextMock).toHaveBeenCalledWith(layerTextFixture);
  });

  it('should not invoke drawing functions when the activeLayer does not match the specified layer', () => {
    drawElementOnLayer({
      mapInstance: mockMapInstance,
      activeLayer: 2,
      object: layerTextFixture,
      drawFunctionKey: 'drawText',
    });

    expect(mockLayer.get).toHaveBeenCalledTimes(1);
    expect(drawTextMock).not.toHaveBeenCalled();
    expect(drawImageMock).not.toHaveBeenCalled();
  });
});

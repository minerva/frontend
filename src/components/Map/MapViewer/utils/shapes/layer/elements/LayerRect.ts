/* eslint-disable no-magic-numbers */
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import Style from 'ol/style/Style';
import { Feature } from 'ol';
import { FeatureLike } from 'ol/Feature';
import { MapInstance } from '@/types/map';
import { Color, LayerRect as LayerRectModel } from '@/types/models';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import Polygon from 'ol/geom/Polygon';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';
import getStyle from '@/components/Map/MapViewer/utils/shapes/style/getStyle';
import getScaledElementStyle from '@/components/Map/MapViewer/utils/shapes/style/getScaledElementStyle';
import { Stroke } from 'ol/style';
import { MapSize } from '@/redux/map/map.types';
import { Coordinate } from 'ol/coordinate';
import { BoundingBox } from '@/components/Map/MapViewer/MapViewer.types';
import { store } from '@/redux/store';
import { updateLayerRect } from '@/redux/layers/layers.thunks';
import { layerUpdateRect } from '@/redux/layers/layers.slice';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';
import getBoundingBoxPolygon from '@/components/Map/MapViewer/utils/shapes/elements/getBoundingBoxPolygon';
import { LAYER_ELEMENT_TYPES } from '@/components/Map/MapViewer/MapViewer.constants';

export interface LayerRectProps {
  elementId: number;
  x: number;
  y: number;
  width: number;
  height: number;
  layer: number;
  zIndex: number;
  fillColor: Color;
  borderColor: Color;
  lineWidth: number;
  pointToProjection: UsePointToProjectionResult;
  mapInstance: MapInstance;
  mapSize: MapSize;
}

export default class LayerRect {
  elementId: number;

  x: number;

  y: number;

  width: number;

  height: number;

  layer: number;

  zIndex: number;

  fillColor: Color;

  borderColor: Color;

  lineWidth: number;

  style: Style = new Style();

  polygon: Polygon = new Polygon([]);

  strokeStyle: Stroke = new Stroke();

  feature: Feature<Polygon>;

  mapSize: MapSize;

  pointToProjection: UsePointToProjectionResult;

  minResolution: number;

  constructor({
    elementId,
    x,
    y,
    width,
    height,
    layer,
    zIndex,
    fillColor,
    borderColor,
    lineWidth,
    pointToProjection,
    mapInstance,
    mapSize,
  }: LayerRectProps) {
    this.elementId = elementId;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.layer = layer;
    this.zIndex = zIndex;
    this.fillColor = fillColor;
    this.borderColor = borderColor;
    this.lineWidth = lineWidth;
    this.pointToProjection = pointToProjection;
    this.mapSize = mapSize;

    const maxZoom = mapInstance?.getView().get('originalMaxZoom');
    this.minResolution = mapInstance?.getView().getResolutionForZoom(maxZoom) || 1;

    this.polygon = getBoundingBoxPolygon({
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height,
      pointToProjection: this.pointToProjection,
    });

    this.setStyles();

    this.feature = new Feature({
      geometry: this.polygon,
      layer,
      elementType: LAYER_ELEMENT_TYPES.RECT,
    });
    this.feature.setId(this.elementId);
    this.feature.set('getObjectData', this.getData.bind(this));
    this.feature.set('setCoordinates', this.setCoordinates.bind(this));
    this.feature.set('refreshPolygon', this.refreshPolygon.bind(this));
    this.feature.set('updateElement', this.updateElement.bind(this));
    this.feature.set('save', this.save.bind(this));
    this.feature.setStyle(this.getStyle.bind(this));
  }

  private getData(): LayerRectModel {
    return {
      id: this.elementId,
      x: this.x,
      y: this.y,
      z: this.zIndex,
      width: this.width,
      height: this.height,
      layer: this.layer,
      fillColor: this.fillColor,
      borderColor: this.borderColor,
      lineWidth: this.lineWidth,
    };
  }

  private async save({
    modelId,
    boundingBox,
  }: {
    modelId: number;
    boundingBox: BoundingBox;
  }): Promise<void> {
    const { dispatch } = store;
    const layerRect = await dispatch(
      updateLayerRect({
        modelId,
        layerId: this.layer,
        ...this.getData(),
        ...boundingBox,
      }),
    ).unwrap();
    if (layerRect) {
      dispatch(layerUpdateRect({ modelId, layerId: layerRect.layer, layerRect }));
      dispatch(mapEditToolsSetLayerObject(layerRect));
      this.updateElement(layerRect);
    }
  }

  private refreshPolygon(): void {
    this.polygon = getBoundingBoxPolygon({
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height,
      pointToProjection: this.pointToProjection,
    });
    this.feature.setGeometry(this.polygon);
    this.feature.changed();
  }

  private setStyles(): void {
    this.strokeStyle = getStroke({
      color: rgbToHex(this.borderColor),
      width: this.lineWidth,
    });
    this.style = getStyle({
      geometry: this.polygon,
      borderColor: this.borderColor,
      fillColor: this.fillColor,
      lineWidth: this.lineWidth,
      zIndex: this.zIndex,
    });
    this.style.setGeometry(this.polygon);
  }

  private updateElement(layerRect: LayerRectModel): void {
    this.elementId = layerRect.id;
    this.x = layerRect.x;
    this.y = layerRect.y;
    this.width = layerRect.width;
    this.height = layerRect.height;
    this.zIndex = layerRect.z;
    this.fillColor = layerRect.fillColor;
    this.borderColor = layerRect.borderColor;

    this.refreshPolygon();
    this.setStyles();
    this.feature.changed();
  }

  private setCoordinates(coords: Coordinate[][]): void {
    const geometry = this.style.getGeometry();
    if (geometry && geometry instanceof Polygon) {
      geometry.setCoordinates(coords);
    }
  }

  protected getStyle(_: FeatureLike, resolution: number): Style | Array<Style> | void {
    const scale = this.minResolution / resolution;
    return getScaledElementStyle(this.style, this.strokeStyle, scale);
  }
}

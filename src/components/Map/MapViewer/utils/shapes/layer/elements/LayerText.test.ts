/* eslint-disable no-magic-numbers */
import { Map } from 'ol';
import { Style } from 'ol/style';
import View from 'ol/View';
import { BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import LayerText, {
  LayerTextProps,
} from '@/components/Map/MapViewer/utils/shapes/layer/elements/LayerText';
import getTextStyle from '@/components/Map/MapViewer/utils/shapes/text/getTextStyle';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import getTextCoords from '@/components/Map/MapViewer/utils/shapes/text/getTextCoords';
import { DEFAULT_TILE_SIZE } from '@/constants/map';

jest.mock('../../text/getTextCoords');
jest.mock('../../text/getTextStyle');
jest.mock('../../style/rgbToHex');

describe('Text', () => {
  let props: LayerTextProps;

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });
    props = {
      elementId: 1,
      x: 0,
      y: 0,
      width: 100,
      height: 100,
      zIndex: 1,
      layer: 1,
      text: 'Test',
      fontSize: 12,
      color: BLACK_COLOR,
      borderColor: BLACK_COLOR,
      backgroundColor: BLACK_COLOR,
      verticalAlign: 'MIDDLE',
      horizontalAlign: 'CENTER',
      pointToProjection: jest.fn(({ x, y }) => [x, y]),
      mapInstance,
      mapSize: {
        minZoom: 1,
        maxZoom: 9,
        width: 0,
        height: 0,
        tileSize: DEFAULT_TILE_SIZE,
      },
    };

    (getTextStyle as jest.Mock).mockReturnValue(new Style());
    (getTextCoords as jest.Mock).mockReturnValue([10, 10]);
    (rgbToHex as jest.Mock).mockReturnValue('#FFFFFF');
  });

  it('should apply correct styles to the feature', () => {
    const text = new LayerText(props);
    const { feature } = text;

    const style = feature.getStyleFunction()?.call(text, feature, 1);

    if (Array.isArray(style)) {
      expect(style.length).toBeGreaterThan(0);
    } else {
      expect(style).toBeInstanceOf(Style);
    }
  });

  it('should hide text when the scaled font size is too small', () => {
    const text = new LayerText(props);
    const { feature } = text;

    const style = feature.getStyleFunction()?.call(text, feature, 20);

    if (Array.isArray(style)) {
      expect(style[0].getText()?.getText()).toBeUndefined();
    } else {
      expect(style?.getText()?.getText()).toBeUndefined();
    }
  });
});

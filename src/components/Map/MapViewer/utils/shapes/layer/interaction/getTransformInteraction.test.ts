/* eslint-disable no-magic-numbers */
import modalReducer from '@/redux/modal/modal.slice';
import { MapSize } from '@/redux/map/map.types';
import {
  createStoreInstanceUsingSliceReducer,
  ToolkitStoreWithSingleSlice,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { ModalState } from '@/redux/modal/modal.types';
import { DEFAULT_TILE_SIZE } from '@/constants/map';
import { Collection, Feature } from 'ol';
import Transform from 'ol-ext/interaction/Transform';
import { Geometry } from 'ol/geom';
import getTransformInteraction from '@/components/Map/MapViewer/utils/shapes/layer/interaction/getTransformInteraction';

jest.mock('../../../../../../../utils/map/latLngToPoint', () => ({
  latLngToPoint: jest.fn(latLng => ({ x: latLng[0], y: latLng[1] })),
}));

describe('getTransformInteraction', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModalState>;
  let modelIdMock: number;
  let featuresCollectionMock: Collection<Feature<Geometry>>;
  const mockDispatch = jest.fn(() => {});

  let mapSize: MapSize;

  beforeEach(() => {
    mapSize = {
      width: 800,
      height: 600,
      minZoom: 1,
      maxZoom: 9,
      tileSize: DEFAULT_TILE_SIZE,
    };
    store = createStoreInstanceUsingSliceReducer('modal', modalReducer);
    store.dispatch = mockDispatch;
    modelIdMock = 1;
    featuresCollectionMock = new Collection<Feature<Geometry>>();
  });

  it('returns a Transform interaction', () => {
    const transformInteraction = getTransformInteraction(
      store.dispatch,
      mapSize,
      modelIdMock,
      featuresCollectionMock,
      [1000, 1000, 1000, 1000],
    );
    expect(transformInteraction).toBeInstanceOf(Transform);
  });
});

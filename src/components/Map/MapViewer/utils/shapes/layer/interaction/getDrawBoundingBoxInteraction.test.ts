/* eslint-disable no-magic-numbers */
import Draw from 'ol/interaction/Draw';
import { latLngToPoint } from '@/utils/map/latLngToPoint';
import modalReducer, { openLayerImageObjectFactoryModal } from '@/redux/modal/modal.slice';
import { MapSize } from '@/redux/map/map.types';
import {
  createStoreInstanceUsingSliceReducer,
  ToolkitStoreWithSingleSlice,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { ModalState } from '@/redux/modal/modal.types';
import { DEFAULT_TILE_SIZE } from '@/constants/map';
import { Map } from 'ol';
import getDrawBoundingBoxInteraction from './getDrawBoundingBoxInteraction';

jest.mock('../../../../../../../utils/map/latLngToPoint', () => ({
  latLngToPoint: jest.fn(latLng => ({ x: latLng[0], y: latLng[1] })),
}));

describe('getDrawBoundingBoxInteraction', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModalState>;
  const mockDispatch = jest.fn(() => {});

  let mapSize: MapSize;

  beforeEach(() => {
    mapSize = {
      width: 800,
      height: 600,
      minZoom: 1,
      maxZoom: 9,
      tileSize: DEFAULT_TILE_SIZE,
    };
    store = createStoreInstanceUsingSliceReducer('modal', modalReducer);
    store.dispatch = mockDispatch;
  });

  it('returns a Draw interaction', () => {
    const drawInteraction = getDrawBoundingBoxInteraction(
      mapSize,
      store.dispatch,
      [1000, 1000, 1000, 1000],
      openLayerImageObjectFactoryModal,
    );
    expect(drawInteraction).toBeInstanceOf(Draw);
  });

  it('dispatches openLayerImageObjectFactoryModal on drawend', () => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({ target: dummyElement });
    const drawInteraction = getDrawBoundingBoxInteraction(
      mapSize,
      store.dispatch,
      [0, 0, 1000, 1000],
      openLayerImageObjectFactoryModal,
    );
    mapInstance.addInteraction(drawInteraction);
    drawInteraction.appendCoordinates([
      [0, 0],
      [10, 10],
    ]);

    drawInteraction.finishDrawing();

    expect(latLngToPoint).toHaveBeenCalledTimes(4);
    expect(store.dispatch).toHaveBeenCalledWith(
      openLayerImageObjectFactoryModal(
        expect.objectContaining({
          x: expect.any(Number),
          y: expect.any(Number),
          width: expect.any(Number),
          height: expect.any(Number),
        }),
      ),
    );
  });
});

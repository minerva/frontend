/* eslint-disable no-magic-numbers */
import { Map } from 'ol';
import { Stroke, Style, Text } from 'ol/style';
import View from 'ol/View';
import { WHITE_COLOR, BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import getTextStyle from '@/components/Map/MapViewer/utils/shapes/text/getTextStyle';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import getTextCoords from '@/components/Map/MapViewer/utils/shapes/text/getTextCoords';
import Layer, { LayerProps } from '@/components/Map/MapViewer/utils/shapes/layer/Layer';

jest.mock('../text/getTextCoords');
jest.mock('../text/getTextStyle');
jest.mock('../style/getStroke');
jest.mock('../style/rgbToHex');

describe('Layer', () => {
  let props: LayerProps;
  const mapSize = {
    width: 90,
    height: 90,
    tileSize: 256,
    minZoom: 2,
    maxZoom: 9,
  };

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });
    props = {
      texts: {
        1: {
          id: 1,
          x: 10,
          y: 10,
          z: 3,
          width: 100,
          height: 100,
          layer: 1,
          fontSize: 12,
          notes: 'XYZ',
          verticalAlign: 'MIDDLE',
          horizontalAlign: 'CENTER',
          backgroundColor: WHITE_COLOR,
          borderColor: BLACK_COLOR,
          color: BLACK_COLOR,
        },
      },
      rects: {
        1: {
          id: 1,
          x: 10,
          y: 10,
          z: 3,
          width: 100,
          height: 100,
          layer: 1,
          lineWidth: 2,
          borderColor: BLACK_COLOR,
          fillColor: WHITE_COLOR,
        },
      },
      ovals: {
        1: {
          id: 1,
          layer: 1,
          x: 10,
          y: 10,
          z: 3,
          width: 100,
          height: 100,
          lineWidth: 2,
          color: BLACK_COLOR,
          borderColor: BLACK_COLOR,
        },
      },
      lines: {
        1: {
          id: 1,
          width: 5.0,
          color: {
            alpha: 255,
            rgb: -16777216,
          },
          z: 0,
          segments: [
            {
              x1: 36.0,
              y1: 39.0,
              x2: 213.0,
              y2: 41.0,
            },
          ],
          startArrow: {
            arrowType: 'NONE',
            angle: 2.748893571891069,
            lineType: 'SOLID',
            length: 15.0,
          },
          endArrow: {
            arrowType: 'NONE',
            angle: 2.748893571891069,
            lineType: 'SOLID',
            length: 15.0,
          },
          lineType: 'SOLID',
          layer: 1,
        },
      },
      images: {
        1: {
          id: 1,
          glyph: 1,
          x: 1,
          y: 1,
          width: 1,
          layer: 1,
          height: 1,
          z: 1,
        },
      },
      visible: true,
      layerId: 23,
      pointToProjection: jest.fn(point => [point.x, point.y]),
      mapInstance,
      mapSize,
      zIndex: 1,
      lineTypes: {},
      arrowTypes: {},
    };
    (getTextStyle as jest.Mock).mockReturnValue(
      new Style({
        text: new Text({}),
      }),
    );
    (getTextCoords as jest.Mock).mockReturnValue([10, 10]);
    (getStroke as jest.Mock).mockReturnValue(new Stroke());
    (rgbToHex as jest.Mock).mockReturnValue('#FFFFFF');
  });

  it('should initialize a Layer class', () => {
    const layer = new Layer(props);

    expect(layer.vectorSource).toBeInstanceOf(VectorSource);
    expect(layer.vectorLayer).toBeInstanceOf(VectorLayer);
  });
});

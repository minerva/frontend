/* eslint-disable no-magic-numbers */
import { Feature, Map, View } from 'ol';
import { Style } from 'ol/style';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import Glyph, { GlyphProps } from '@/components/Map/MapViewer/utils/shapes/elements/Glyph/Glyph';
import { MapInstance } from '@/types/map';
import Polygon from 'ol/geom/Polygon';

describe('Glyph', () => {
  let props: GlyphProps;
  let glyph: Glyph;
  let mapInstance: MapInstance;
  let pointToProjectionMock: jest.MockedFunction<UsePointToProjectionResult>;
  const mapSize = {
    width: 90,
    height: 90,
    tileSize: 256,
    minZoom: 2,
    maxZoom: 9,
  };

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });

    pointToProjectionMock = jest.fn().mockReturnValue([10, 20]);

    props = {
      elementId: 1,
      glyphId: 1,
      x: 10,
      y: 20,
      width: 32,
      height: 32,
      zIndex: 1,
      pointToProjection: pointToProjectionMock,
      mapInstance,
      mapSize,
    };
    glyph = new Glyph(props);
  });

  it('should initialize with correct feature and style properties', () => {
    expect(glyph.feature).toBeInstanceOf(Feature);
    const geometry = glyph.feature.getGeometry();
    expect(geometry).toBeInstanceOf(Polygon);
    expect(geometry?.getCoordinates()).toEqual([
      [
        [10, 20],
        [10, 20],
        [10, 20],
        [10, 20],
        [10, 20],
      ],
    ]);

    expect(glyph.style).toBeInstanceOf(Style);
  });

  it('should scale image based on map resolution', () => {
    const getAnchorAndCoords = glyph.feature.get('getAnchorAndCoords');
    if (mapInstance) {
      expect(getAnchorAndCoords()).toEqual({ anchor: [0, 0], coords: [0, 0] });
    }
  });
});

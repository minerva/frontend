/* eslint-disable no-magic-numbers */
import { Feature, Map } from 'ol';
import { Fill, Stroke, Style, Text } from 'ol/style';
import { MultiPolygon } from 'ol/geom';
import View from 'ol/View';
import { WHITE_COLOR, BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import VectorSource from 'ol/source/Vector';
import MapBackgroundsEnum from '@/redux/map/map.enums';
import { DEFAULT_TILE_SIZE } from '@/constants/map';
import getTextStyle from '@/components/Map/MapViewer/utils/shapes/text/getTextStyle';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';
import getFill from '@/components/Map/MapViewer/utils/shapes/style/getFill';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import CompartmentSquare, {
  CompartmentSquareProps,
} from '@/components/Map/MapViewer/utils/shapes/elements/CompartmentSquare';
import getPolygonCoords from '@/components/Map/MapViewer/utils/shapes/coords/getPolygonCoords';
import getTextCoords from '@/components/Map/MapViewer/utils/shapes/text/getTextCoords';

jest.mock('../text/getTextStyle');
jest.mock('../text/getTextCoords');
jest.mock('../style/getStroke');
jest.mock('../coords/getPolygonCoords');
jest.mock('../style/getFill');
jest.mock('../style/rgbToHex');

describe('CompartmentSquare', () => {
  let props: CompartmentSquareProps;

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });
    props = {
      id: 1,
      complexId: null,
      compartmentId: null,
      pathwayId: null,
      sboTerm: 'SBO:0000253',
      x: 0,
      y: 0,
      width: 100,
      height: 100,
      zIndex: 1,
      fillColor: WHITE_COLOR,
      borderColor: BLACK_COLOR,
      fontColor: BLACK_COLOR,
      innerWidth: 1,
      outerWidth: 2,
      thickness: 12,
      text: 'Test Text',
      fontSize: 12,
      nameX: 10,
      nameY: 20,
      nameHeight: 30,
      nameWidth: 40,
      nameVerticalAlign: 'MIDDLE',
      nameHorizontalAlign: 'CENTER',
      overlaysVisible: false,
      pointToProjection: jest.fn(),
      mapInstance,
      vectorSource: new VectorSource(),
      backgroundId: MapBackgroundsEnum.SEMANTIC,
      mapSize: {
        minZoom: 1,
        maxZoom: 9,
        width: 0,
        height: 0,
        tileSize: DEFAULT_TILE_SIZE,
      },
    };

    (getTextStyle as jest.Mock).mockReturnValue(
      new Style({
        text: new Text({
          text: props.text,
          font: `bold ${props.fontSize}px Arial`,
          fill: new Fill({
            color: '#000',
          }),
          placement: 'point',
          textAlign: 'center',
          textBaseline: 'middle',
        }),
      }),
    );
    (getTextCoords as jest.Mock).mockReturnValue([10, 10]);
    (getStroke as jest.Mock).mockReturnValue(new Stroke());
    (getFill as jest.Mock).mockReturnValue(new Style());
    (rgbToHex as jest.Mock).mockReturnValue('#FFFFFF');
    (getPolygonCoords as jest.Mock).mockReturnValue([
      [10, 10],
      [20, 20],
      [30, 30],
    ]);
  });

  it('should initialize with correct default properties', () => {
    const multiPolygon = new CompartmentSquare(props);

    expect(multiPolygon.polygons.length).toBe(4);
    expect(multiPolygon.feature).toBeInstanceOf(Feature);
    expect(multiPolygon.feature.getGeometry()).toBeInstanceOf(MultiPolygon);
  });

  it('should apply correct styles to the feature', () => {
    const multiPolygon = new CompartmentSquare(props);
    const { feature } = multiPolygon;

    const style = feature.getStyleFunction()?.call(multiPolygon, feature, 1);

    if (Array.isArray(style)) {
      expect(style.length).toBeGreaterThan(0);
    } else {
      expect(style).toBeInstanceOf(Style);
    }
  });
});

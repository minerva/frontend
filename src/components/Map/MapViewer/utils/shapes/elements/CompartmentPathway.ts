/* eslint-disable no-magic-numbers */
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import { MapInstance, Point } from '@/types/map';
import { HorizontalAlign, VerticalAlign } from '@/components/Map/MapViewer/MapViewer.types';
import {
  BLACK_COLOR,
  MAP_ELEMENT_TYPES,
  WHITE_COLOR,
} from '@/components/Map/MapViewer/MapViewer.constants';
import Polygon from 'ol/geom/Polygon';
import { Color } from '@/types/models';
import VectorSource from 'ol/source/Vector';
import { Style } from 'ol/style';
import { MapSize } from '@/redux/map/map.types';
import BaseMultiPolygon from '@/components/Map/MapViewer/utils/shapes/elements/BaseMultiPolygon';
import getStyle from '@/components/Map/MapViewer/utils/shapes/style/getStyle';
import getFill from '@/components/Map/MapViewer/utils/shapes/style/getFill';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';

export type CompartmentPathwayProps = {
  id: number;
  complexId?: number | null;
  compartmentId: number | null;
  pathwayId: number | null;
  sboTerm: string;
  x: number;
  y: number;
  width: number;
  height: number;
  zIndex: number;
  fillColor?: Color;
  borderColor?: Color;
  fontColor?: Color;
  outerWidth?: number;
  text?: string;
  fontSize?: number;
  nameX: number;
  nameY: number;
  nameHeight: number;
  nameWidth: number;
  nameVerticalAlign?: VerticalAlign;
  nameHorizontalAlign?: HorizontalAlign;
  overlaysVisible: boolean;
  pointToProjection: UsePointToProjectionResult;
  mapInstance: MapInstance;
  vectorSource: VectorSource;
  backgroundId: number;
  mapSize: MapSize;
};

export default class CompartmentPathway extends BaseMultiPolygon {
  outerWidth: number;

  overlaysVisible: boolean;

  constructor({
    id,
    complexId,
    compartmentId,
    pathwayId,
    sboTerm,
    x,
    y,
    width,
    height,
    zIndex,
    fillColor = WHITE_COLOR,
    borderColor = BLACK_COLOR,
    fontColor = BLACK_COLOR,
    outerWidth = 1,
    text = '',
    fontSize = 12,
    nameX,
    nameY,
    nameHeight,
    nameWidth,
    nameVerticalAlign = 'MIDDLE',
    nameHorizontalAlign = 'CENTER',
    overlaysVisible,
    pointToProjection,
    mapInstance,
    vectorSource,
    backgroundId,
    mapSize,
  }: CompartmentPathwayProps) {
    super({
      type: MAP_ELEMENT_TYPES.COMPARTMENT,
      id,
      complexId,
      compartmentId,
      pathwayId,
      sboTerm,
      x,
      y,
      width,
      height,
      zIndex,
      text,
      fontSize,
      nameX,
      nameY,
      nameWidth,
      nameHeight,
      fontColor,
      nameVerticalAlign,
      nameHorizontalAlign,
      fillColor,
      borderColor,
      pointToProjection,
      overlaysVisible,
      vectorSource,
      backgroundId,
      mapSize,
      mapInstance,
    });
    this.outerWidth = outerWidth;
    this.overlaysVisible = overlaysVisible;
    this.createPolygons();
    this.drawText();
    this.drawMultiPolygonFeature(mapInstance);
    this.feature.set('isCompartmentClicked', this.isCompartmentClicked.bind(this));
  }

  private isCompartmentClicked({
    point,
    hitTolerance = 5,
  }: {
    point: Point;
    hitTolerance?: number;
  }): boolean {
    const minX = this.x;
    const maxX = this.x + this.width;
    const minY = this.y;
    const maxY = this.y + this.height;

    const innerMinX = minX + this.outerWidth + hitTolerance;
    const innerMaxX = maxX - this.outerWidth - hitTolerance;
    const innerMinY = minY + this.outerWidth + hitTolerance;
    const innerMaxY = maxY - this.outerWidth - hitTolerance;

    return !(
      point.x > innerMinX &&
      point.x < innerMaxX &&
      point.y > innerMinY &&
      point.y < innerMaxY
    );
  }

  protected createPolygons(): void {
    const compartmentPolygon = new Polygon([
      [
        this.pointToProjection({ x: this.x, y: this.y }),
        this.pointToProjection({ x: this.x + this.width, y: this.y }),
        this.pointToProjection({ x: this.x + this.width, y: this.y + this.height }),
        this.pointToProjection({ x: this.x, y: this.y + this.height }),
        this.pointToProjection({ x: this.x, y: this.y }),
      ],
    ]);
    compartmentPolygon.set('id', this.id);
    compartmentPolygon.set('type', MAP_ELEMENT_TYPES.COMPARTMENT);
    this.coverStyle = new Style({
      geometry: compartmentPolygon,
      fill: getFill({ color: rgbToHex({ ...this.fillColor, alpha: 255 }) }),
    });

    compartmentPolygon.set(
      'strokeStyle',
      getStroke({
        color: rgbToHex(this.borderColor),
        width: this.outerWidth,
      }),
    );
    this.styles.push(
      getStyle({
        geometry: compartmentPolygon,
        borderColor: this.borderColor,
        fillColor: this.overlaysVisible ? WHITE_COLOR : { ...this.fillColor, alpha: 9 },
        lineWidth: this.outerWidth,
        zIndex: this.zIndex,
      }),
    );
    compartmentPolygon.setProperties({ id: this.id });
    this.polygons.push(compartmentPolygon);
  }
}

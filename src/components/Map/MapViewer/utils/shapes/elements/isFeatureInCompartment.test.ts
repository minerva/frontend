/* eslint-disable no-magic-numbers */
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import isFeatureInCompartment from './isFeatureInCompartment';

describe('isFeatureInCompartment', () => {
  let mockVectorSource: jest.Mocked<VectorSource>;

  beforeEach(() => {
    mockVectorSource = {
      getFeatureById: jest.fn(),
    } as unknown as jest.Mocked<VectorSource>;
  });

  it('should return false if the feature has no compartmentId', () => {
    const feature = new Feature();
    feature.set('compartmentId', null);

    const result = isFeatureInCompartment(1, mockVectorSource, feature);

    expect(result).toBe(false);
  });

  it('should return true if the feature compartmentId matches parentCompartmentId', () => {
    const feature = new Feature();
    feature.set('compartmentId', 1);

    const result = isFeatureInCompartment(1, mockVectorSource, feature);

    expect(result).toBe(true);
  });

  it('should return false if the parent feature does not exist', () => {
    const feature = new Feature();
    feature.set('compartmentId', 2);

    mockVectorSource.getFeatureById.mockReturnValueOnce(null);

    const result = isFeatureInCompartment(1, mockVectorSource, feature);

    expect(result).toBe(false);
    expect(mockVectorSource.getFeatureById).toHaveBeenCalledWith(2);
  });

  it('should return true if parent feature matches parentCompartmentId in recursive call', () => {
    const parentFeature = new Feature();
    parentFeature.set('compartmentId', 1);

    const childFeature = new Feature();
    childFeature.set('compartmentId', 2);

    mockVectorSource.getFeatureById.mockReturnValueOnce(parentFeature);

    const result = isFeatureInCompartment(1, mockVectorSource, childFeature);

    expect(result).toBe(true);
    expect(mockVectorSource.getFeatureById).toHaveBeenCalledWith(2);
  });

  it('should return false if recursive call finds no matching parent', () => {
    const grandParentFeature = new Feature();
    grandParentFeature.set('compartmentId', 3);

    const parentFeature = new Feature();
    parentFeature.set('compartmentId', 2);

    const childFeature = new Feature();
    childFeature.set('compartmentId', 4);

    mockVectorSource.getFeatureById
      .mockReturnValueOnce(parentFeature)
      .mockReturnValueOnce(grandParentFeature);

    const result = isFeatureInCompartment(1, mockVectorSource, childFeature);

    expect(result).toBe(false);
    expect(mockVectorSource.getFeatureById).toHaveBeenCalledWith(4);
    expect(mockVectorSource.getFeatureById).toHaveBeenCalledWith(2);
  });
});

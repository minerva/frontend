/* eslint-disable no-magic-numbers */
import Feature from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import getDividedExtents from '@/components/Map/MapViewer/utils/shapes/coords/getDividedExtents';
import findLargestExtent from '@/components/Map/MapViewer/utils/shapes/coords/findLargestExtent';
import handleSemanticView from '@/components/Map/MapViewer/utils/shapes/elements/handleSemanticView';
import Geometry from 'ol/geom/Geometry';
import { fromExtent } from 'ol/geom/Polygon';

jest.mock('../coords/getDividedExtents');
jest.mock('../coords/findLargestExtent');

describe('handleSemanticView', () => {
  let vectorSource: VectorSource;
  let feature: Feature;

  beforeEach(() => {
    vectorSource = new VectorSource();
    feature = new Feature({
      geometry: fromExtent([0, 0, 100, 100]),
      type: 'COMPARTMENT',
      zIndex: 1,
      getMapExtent: (): Array<number> => [0, 0, 100, 100],
      filled: false,
      hidden: false,
    });
    feature.setId(1);

    const mockGeometry = {
      getExtent: jest.fn(() => [2, 0, 10, 10]),
    } as unknown as Geometry;

    feature.getGeometry = jest.fn(() => mockGeometry);
  });

  it('should return cover = true, hide = false, and calculate largestExtent when feature meets cover conditions', () => {
    jest
      .spyOn(vectorSource, 'forEachFeatureIntersectingExtent')
      .mockImplementation((_, callback) => {
        callback(
          new Feature({
            id: 1,
            geometry: fromExtent([1, 0, 5, 5]),
            hidden: false,
            type: 'COMPARTMENT',
            zIndex: 123,
            filled: true,
            getMapExtent: (): Array<number> => [1, 0, 5, 5],
          }),
        );
      });
    (getDividedExtents as jest.Mock).mockReturnValue([[0, 0, 10, 5]]);
    (findLargestExtent as jest.Mock).mockReturnValue([0, 0, 10, 5]);

    const result = handleSemanticView({
      vectorSource,
      feature,
      resolution: 1,
      sboTerm: 'SBO:123456',
      compartmentId: 2,
      pathwayId: null,
    });

    expect(result).toEqual({
      cover: true,
      hide: false,
      largestExtent: [0, 0, 10, 5],
    });

    expect(feature.get('filled')).toBe(true);
    expect(getDividedExtents).toHaveBeenCalled();
    expect(findLargestExtent).toHaveBeenCalled();
  });

  it('should return hide = true when complexId points to a filled feature', () => {
    const complexFeature = new Feature({ filled: true });
    jest
      .spyOn(vectorSource, 'getFeatureById')
      .mockImplementation(id => (id === 1 ? complexFeature : null));

    const result = handleSemanticView({
      vectorSource,
      feature,
      resolution: 1,
      sboTerm: 'SBO:123456',
      compartmentId: null,
      pathwayId: null,
      complexId: 1,
    });

    expect(result).toEqual({
      cover: true,
      hide: true,
      largestExtent: [0, 0, 10, 5],
    });
  });

  it('should return hide = true when compartmentId points to a filled feature', () => {
    const compartmentFeature = new Feature({ filled: true });
    jest
      .spyOn(vectorSource, 'getFeatureById')
      .mockImplementation(id => (id === 2 ? compartmentFeature : null));

    const result = handleSemanticView({
      vectorSource,
      feature,
      resolution: 1,
      sboTerm: 'SBO:123456',
      compartmentId: 2,
      pathwayId: null,
    });

    expect(result).toEqual({
      cover: true,
      hide: true,
      largestExtent: [0, 0, 10, 5],
    });
  });

  it('should return hide = true when pathwayId points to a filled feature', () => {
    const compartmentFeature = new Feature({ filled: true });
    jest
      .spyOn(vectorSource, 'getFeatureById')
      .mockImplementation(id => (id === 2 ? compartmentFeature : null));

    const result = handleSemanticView({
      vectorSource,
      feature,
      resolution: 1,
      sboTerm: 'SBO:123456',
      compartmentId: null,
      pathwayId: 2,
    });

    expect(result).toEqual({
      cover: true,
      hide: true,
      largestExtent: [0, 0, 10, 5],
    });
  });
});

import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';

export default function isFeatureInCompartment(
  parentCompartmentId: number,
  vectorSource: VectorSource,
  feature: Feature,
): boolean {
  const compartmentId: undefined | null | number = feature.get('compartmentId');

  if (!compartmentId) {
    return false;
  }
  if (compartmentId === parentCompartmentId || compartmentId === feature.get('id')) {
    return true;
  }
  const compartmentFeature = vectorSource.getFeatureById(compartmentId);
  if (!compartmentFeature) {
    return false;
  }
  return isFeatureInCompartment(parentCompartmentId, vectorSource, compartmentFeature);
}

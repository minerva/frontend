/* eslint-disable no-magic-numbers */
import getPolygonCoords from '@/components/Map/MapViewer/utils/shapes/coords/getPolygonCoords';
import getEllipseCoords from '@/components/Map/MapViewer/utils/shapes/coords/getEllipseCoords';
import Polygon from 'ol/geom/Polygon';
import { Shape } from '@/types/models';
import getShapePolygon from '@/components/Map/MapViewer/utils/shapes/elements/getShapePolygon';

jest.mock('../coords/getPolygonCoords');
jest.mock('../coords/getEllipseCoords');

describe('getShapePolygon', () => {
  const mockPointToProjection = jest.fn(point => [point.x, point.y]);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return an array of Polygons for POLYGON shapes', () => {
    const x = 10;
    const y = 20;
    const width = 100;
    const height = 200;

    const shapes: Shape[] = [
      {
        type: 'POLYGON',
        points: [
          {
            type: 'REL_ABS_POINT',
            absoluteX: 0.0,
            absoluteY: 0.0,
            relativeX: 0.0,
            relativeY: 0.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
          {
            type: 'REL_ABS_POINT',
            absoluteX: 0.0,
            absoluteY: 0.0,
            relativeX: 75.0,
            relativeY: 0.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
          {
            type: 'REL_ABS_POINT',
            absoluteX: 0.0,
            absoluteY: 0.0,
            relativeX: 100.0,
            relativeY: 100.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
          {
            type: 'REL_ABS_POINT',
            absoluteX: 0.0,
            absoluteY: 0.0,
            relativeX: 25.0,
            relativeY: 100.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
        ],
      },
    ];

    const mockPolygonCoords = [
      [
        [0, 0],
        [2, 0],
        [2, 2],
        [0, 2],
      ],
    ];
    (getPolygonCoords as jest.Mock).mockReturnValue(mockPolygonCoords);

    const result = shapes.map(shape => {
      return getShapePolygon({
        shape,
        x,
        y,
        width,
        height,
        pointToProjection: mockPointToProjection,
      });
    });
    expect(result).toHaveLength(1);
    expect(result[0]).toBeInstanceOf(Polygon);
    expect(result[0].getCoordinates()).toEqual([mockPolygonCoords]);

    expect(getPolygonCoords).toHaveBeenCalledTimes(1);
  });

  it('should return an array of Polygons for ELLIPSE shapes', () => {
    const x = 30;
    const y = 40;
    const width = 100;
    const height = 200;

    const shapes: Shape[] = [
      {
        type: 'ELLIPSE',
        center: {
          type: 'REL_ABS_POINT',
          absoluteX: 0.0,
          absoluteY: 0.0,
          relativeX: 50.0,
          relativeY: 50.0,
          relativeHeightForX: null,
          relativeWidthForY: null,
        },
        radius: {
          type: 'REL_ABS_RADIUS',
          absoluteX: -7.0,
          absoluteY: -7.0,
          relativeX: 50.0,
          relativeY: 50.0,
          relativeHeightForX: null,
          relativeWidthForY: null,
        },
      },
    ];

    const mockEllipseCoords = [
      [
        [0, 0],
        [30, 0],
        [45, 60],
        [30, 120],
        [0, 120],
        [-15, 60],
      ],
    ];
    (getEllipseCoords as jest.Mock).mockReturnValue(mockEllipseCoords);

    const result = shapes.map(shape => {
      return getShapePolygon({
        shape,
        x,
        y,
        width,
        height,
        pointToProjection: mockPointToProjection,
      });
    });

    expect(result).toHaveLength(1);
    expect(result[0]).toBeInstanceOf(Polygon);
    expect(result[0].getCoordinates()).toEqual([mockEllipseCoords]);

    expect(getEllipseCoords).toHaveBeenCalledTimes(1);
  });

  it('should handle multiple shapes (POLYGON and ELLIPSE)', () => {
    const x = 10;
    const y = 20;
    const width = 100;
    const height = 200;

    const shapes: Shape[] = [
      {
        type: 'ELLIPSE',
        center: {
          type: 'REL_ABS_POINT',
          absoluteX: 0.0,
          absoluteY: 0.0,
          relativeX: 50.0,
          relativeY: 50.0,
          relativeHeightForX: null,
          relativeWidthForY: null,
        },
        radius: {
          type: 'REL_ABS_RADIUS',
          absoluteX: -7.0,
          absoluteY: -7.0,
          relativeX: 50.0,
          relativeY: 50.0,
          relativeHeightForX: null,
          relativeWidthForY: null,
        },
      },
      {
        type: 'POLYGON',
        points: [
          {
            type: 'REL_ABS_POINT',
            absoluteX: 7.0,
            absoluteY: 0.0,
            relativeX: 50.0,
            relativeY: 0.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
          {
            type: 'REL_ABS_POINT',
            absoluteX: -7.0,
            absoluteY: 0.0,
            relativeX: 50.0,
            relativeY: 100.0,
            relativeHeightForX: null,
            relativeWidthForY: null,
          },
        ],
      },
    ];

    const mockPolygonCoords = [
      [
        [0, 0],
        [2, 0],
        [2, 2],
        [0, 2],
      ],
    ];
    const mockEllipseCoords = [
      [
        [0, 0],
        [30, 0],
        [45, 60],
        [30, 120],
        [0, 120],
        [-15, 60],
      ],
    ];
    (getPolygonCoords as jest.Mock).mockReturnValue(mockPolygonCoords);
    (getEllipseCoords as jest.Mock).mockReturnValue(mockEllipseCoords);

    const result = shapes.map(shape => {
      return getShapePolygon({
        shape,
        x,
        y,
        width,
        height,
        pointToProjection: mockPointToProjection,
      });
    });

    expect(result).toHaveLength(2);
    expect(getPolygonCoords).toHaveBeenCalledTimes(1);
    expect(getEllipseCoords).toHaveBeenCalledTimes(1);
  });
});

/* eslint-disable no-magic-numbers */
import { Feature } from 'ol';
import { Fill, Stroke, Style } from 'ol/style';
import { Polygon, MultiPolygon } from 'ol/geom';
import { BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import { ArrowTypeDict } from '@/redux/shapes/shapes.types';
import getShapePolygon from '@/components/Map/MapViewer/utils/shapes/elements/getShapePolygon';
import getStyle from '@/components/Map/MapViewer/utils/shapes/style/getStyle';
import getReactionArrowFeature from '@/components/Map/MapViewer/utils/shapes/reaction/getReactionArrowFeature';

jest.mock('../style/getStyle');
jest.mock('../elements/getShapePolygon');

describe('getReactionArrowFeature', () => {
  const props = {
    arrowTypes: {
      FULL: [
        {
          type: 'POLYGON',
          fill: false,
          points: [
            {
              type: 'REL_ABS_POINT',
              absoluteX: 0.0,
              absoluteY: 0.0,
              relativeX: 0.0,
              relativeY: 50.0,
              relativeHeightForX: null,
              relativeWidthForY: null,
            },
            {
              type: 'REL_ABS_POINT',
              absoluteX: 0.0,
              absoluteY: 0.0,
              relativeX: 100.0,
              relativeY: 50.0,
              relativeHeightForX: null,
              relativeWidthForY: null,
            },
            {
              type: 'REL_ABS_POINT',
              absoluteX: 0.0,
              absoluteY: 0.0,
              relativeX: 7.612046748871326,
              relativeY: 50.0,
              relativeHeightForX: null,
              relativeWidthForY: 19.134171618254495,
            },
            {
              type: 'REL_ABS_POINT',
              absoluteX: 0.0,
              absoluteY: 0.0,
              relativeX: 7.612046748871326,
              relativeY: 50.0,
              relativeHeightForX: null,
              relativeWidthForY: -19.134171618254495,
            },
            {
              type: 'REL_ABS_POINT',
              absoluteX: 0.0,
              absoluteY: 0.0,
              relativeX: 100.0,
              relativeY: 50.0,
              relativeHeightForX: null,
              relativeWidthForY: null,
            },
          ],
        },
      ],
    } as ArrowTypeDict,
    arrow: { length: 15, arrowType: 'FULL', angle: 2.74, lineType: 'SOLID' },
    x: 0,
    y: 0,
    zIndex: 1,
    rotation: 1,
    lineWidth: 1,
    color: BLACK_COLOR,
    pointToProjection: jest.fn(() => [10, 10]),
  };
  const polygon = new Polygon([
    [
      [0, 0],
      [1, 1],
      [2, 2],
    ],
  ]);

  beforeEach(() => {
    (getStyle as jest.Mock).mockReturnValue(
      new Style({
        geometry: polygon,
        stroke: new Stroke({}),
        fill: new Fill({}),
      }),
    );
    (getShapePolygon as jest.Mock).mockReturnValue(
      new Polygon([
        [
          [0, 0],
          [1, 1],
          [2, 2],
        ],
      ]),
    );
  });

  it('should return arrow feature', () => {
    const arrowFeature = getReactionArrowFeature(props);

    expect(arrowFeature).toBeInstanceOf(Feature<MultiPolygon>);
  });

  it('should handle missing arrowType in props', () => {
    const invalidProps = { ...props, arrowTypes: {} };
    expect(() => getReactionArrowFeature(invalidProps)).not.toThrow();
  });

  it('should call getStyle', () => {
    getReactionArrowFeature(props);
    expect(getStyle).toBeCalled();
  });
});

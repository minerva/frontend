/* eslint-disable no-magic-numbers */
import { Feature, Map } from 'ol';
import Reaction, { ReactionProps } from '@/components/Map/MapViewer/utils/shapes/reaction/Reaction';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { lineTypesFixture } from '@/models/fixtures/lineTypesFixture';
import { arrowTypesFixture } from '@/models/fixtures/arrowTypesFixture';
import { shapesFixture } from '@/models/fixtures/shapesFixture';
import View from 'ol/View';
import { ArrowTypeDict, LineTypeDict } from '@/redux/shapes/shapes.types';
import { ArrowType, LineType } from '@/types/models';
import VectorSource from 'ol/source/Vector';

describe('Layer', () => {
  let props: ReactionProps;

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });
    props = {
      id: 1,
      line: newReactionFixture.line,
      products: newReactionFixture.products,
      reactants: newReactionFixture.reactants,
      modifiers: newReactionFixture.modifiers,
      operators: newReactionFixture.operators,
      shapes: shapesFixture,
      zIndex: newReactionFixture.z,
      pointToProjection: jest.fn(() => [10, 10]),
      lineTypes: lineTypesFixture.reduce((acc: LineTypeDict, line: LineType) => {
        acc[line.name] = line.pattern;
        return acc;
      }, {}),
      arrowTypes: arrowTypesFixture.reduce((acc: ArrowTypeDict, arrow: ArrowType) => {
        acc[arrow.arrowType] = arrow.shapes;
        return acc;
      }, {}),
      vectorSource: new VectorSource(),
      sbgnFormat: false,
      mapInstance,
    };
  });

  it('should initialize a Reaction class', () => {
    const reaction = new Reaction(props);

    expect(reaction.reactionFeatures.length).toBe(3);
    expect(reaction.reactionFeatures).toBeInstanceOf(Array<Feature>);
    expect(reaction.lineFeature).toBeInstanceOf(Feature);
  });
});

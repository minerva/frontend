/* eslint-disable no-magic-numbers */
import { OverlayBioEntityRender } from '@/types/OLrendering';
import calculateOverlayDimensions from './calculateOverlayDimensions';

describe('calculateOverlayDimensions', () => {
  it('should calculate overlay dimensions and update y1, y2, and height', () => {
    const overlay: OverlayBioEntityRender & { amount: number } = {
      amount: 3,
      color: null,
      height: 100,
      hexColor: '#0000001a',
      id: '3',
      modelId: 0,
      overlayId: 1,
      type: 'submap-link',
      value: -0.43,
      width: 100,
      x1: 200,
      x2: 300,
      y1: 750,
      y2: 700,
    };
    const entityOverlays: Array<OverlayBioEntityRender> = [
      {
        color: null,
        height: 100,
        hexColor: '#0000001a',
        id: '3',
        modelId: 0,
        overlayId: 1,
        type: 'submap-link',
        value: -0.43,
        width: 100,
        x1: 200,
        x2: 300,
        y1: 750,
        y2: 700,
      },
    ];

    const index = 1;
    const result = calculateOverlayDimensions(overlay, index, 4, 100, entityOverlays);

    expect(result.height).toEqual(75);
    expect(result.y1).toEqual(entityOverlays[index - 1].y1 + 75);
  });
});

/* eslint-disable no-magic-numbers */
import { OverlayBioEntityRender } from '@/types/OLrendering';
import groupOverlayEntities from './groupOverlayEntities';

describe('groupOverlays', () => {
  it('should group overlay entities correctly by overlayId, value, and color', () => {
    const overlayBioEntities: Array<OverlayBioEntityRender> = [
      {
        color: null,
        height: 50,
        hexColor: '#0000001a',
        id: '1',
        modelId: 0,
        overlayId: 1,
        type: 'submap-link',
        value: 0.1,
        width: 100,
        x1: 1200,
        x2: 1300,
        y1: 550,
        y2: 500,
      },
      {
        color: null,
        height: 50,
        hexColor: '#0000001a',
        id: '1',
        modelId: 0,
        overlayId: 1,
        type: 'submap-link',
        value: 0.1,
        width: 100,
        x1: 1200,
        x2: 1300,
        y1: 550,
        y2: 500,
      },
      {
        color: null,
        height: 50,
        hexColor: '#0000001a',
        id: '3',
        modelId: 0,
        overlayId: 2,
        type: 'submap-link',
        value: -0.43,
        width: 100,
        x1: 200,
        x2: 300,
        y1: 750,
        y2: 700,
      },
    ];

    const result = groupOverlayEntities(overlayBioEntities);
    expect(result['1'][0].amount).toBe(2);
    expect(result['2'][0].amount).toBe(1);
    expect(result['3']).toBeUndefined();
  });
});

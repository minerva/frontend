/* eslint-disable no-magic-numbers */
import { OverlayBioEntityRender } from '@/types/OLrendering';
import { GetOverlayBioEntityColorByAvailableProperties } from '@/components/Map/MapViewer/utils/config/overlaysLayer/useGetOverlayColor';
import sortElementOverlayByColor from './sortElementOverlayByColor';

describe('sortElementOverlayByColor', () => {
  it('should sort elements by color', () => {
    const elementOverlay: Array<OverlayBioEntityRender & { amount: number }> = [
      {
        amount: 1,
        color: null,
        height: 50,
        hexColor: '#0000001a',
        id: '5',
        modelId: 0,
        overlayId: 0,
        type: 'submap-link',
        value: 0.1,
        width: 100,
        x1: 1200,
        x2: 1300,
        y1: 550,
        y2: 500,
      },
      {
        amount: 1,
        color: null,
        height: 50,
        hexColor: '#0000001a',
        id: '2',
        modelId: 0,
        overlayId: 1,
        type: 'submap-link',
        value: -0.43,
        width: 100,
        x1: 200,
        x2: 300,
        y1: 750,
        y2: 700,
      },
    ];

    const getColor: GetOverlayBioEntityColorByAvailableProperties = jest.fn(
      entity => `#A633C${entity.id}`,
    );
    sortElementOverlayByColor(elementOverlay, getColor);

    expect(elementOverlay[0].id).toEqual('2');
    expect(elementOverlay[1].id).toEqual('5');
  });
});

/* eslint-disable no-magic-numbers */
import { OverlayBioEntityRender } from '@/types/OLrendering';
import findMatchingSubmapLinkRectangle from './findMatchingSubmapLinkRectangle';

describe('findMatchingSubmapLinkRectangle', () => {
  const elements: Array<OverlayBioEntityRender & { amount: number }> = [
    {
      amount: 1,
      color: null,
      height: 50,
      hexColor: '#0000001a',
      id: '3',
      modelId: 0,
      overlayId: 1,
      type: 'submap-link',
      value: -0.43,
      width: 100,
      x1: 200,
      x2: 300,
      y1: 750,
      y2: 700,
    },
  ];

  it('should find a matching submap link rectangle by value or color', () => {
    const overlayBioEntity: OverlayBioEntityRender = {
      color: null,
      height: 50,
      hexColor: '#0000001a',
      id: '3',
      modelId: 0,
      overlayId: 1,
      type: 'submap-link',
      value: -0.43,
      width: 100,
      x1: 200,
      x2: 300,
      y1: 750,
      y2: 700,
    };

    const result = findMatchingSubmapLinkRectangle(elements, overlayBioEntity);
    expect(result).toEqual(elements[0]);
  });

  it('should return undefined if no matching element is found', () => {
    const overlayBioEntity: OverlayBioEntityRender = {
      color: null,
      height: 50,
      hexColor: '#0000001a',
      id: '3',
      modelId: 0,
      overlayId: 1,
      type: 'submap-link',
      value: 0.21,
      width: 100,
      x1: 200,
      x2: 300,
      y1: 750,
      y2: 700,
    };

    const result = findMatchingSubmapLinkRectangle(elements, overlayBioEntity);
    expect(result).toBeUndefined();
  });
});

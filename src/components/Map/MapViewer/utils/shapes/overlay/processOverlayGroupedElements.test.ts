/* eslint-disable no-magic-numbers */
import { OverlayBioEntityRender } from '@/types/OLrendering';
import { GetOverlayBioEntityColorByAvailableProperties } from '@/components/Map/MapViewer/utils/config/overlaysLayer/useGetOverlayColor';
import { OverlayBioEntityGroupedElementsType } from '@/components/Map/MapViewer/MapViewer.types';
import processOverlayGroupedElements from '@/components/Map/MapViewer/utils/shapes/overlay/processOverlayGroupedElements';

describe('processOverlayGroupedElements', () => {
  it('should correctly process overlay grouped elements and add to entityOverlays', () => {
    const groupedElements = {
      '1': [
        {
          amount: 1,
          color: null,
          height: 50,
          hexColor: '#0000001a',
          id: '5',
          modelId: 0,
          overlayId: 1,
          type: 'submap-link',
          value: 0.1,
          width: 100,
          x1: 1200,
          x2: 1300,
          y1: 550,
          y2: 500,
        },
        {
          amount: 1,
          color: null,
          height: 50,
          hexColor: '#0000001a',
          id: '5',
          modelId: 0,
          overlayId: 1,
          type: 'submap-link',
          value: -0.43,
          width: 100,
          x1: 200,
          x2: 300,
          y1: 750,
          y2: 700,
        },
      ],
    } as OverlayBioEntityGroupedElementsType;

    const entityOverlays: Array<OverlayBioEntityRender> = [];
    const getColor: GetOverlayBioEntityColorByAvailableProperties = jest.fn(() => 'color');

    processOverlayGroupedElements(groupedElements, entityOverlays, getColor);

    expect(entityOverlays.length).toBe(2);
    expect(entityOverlays[0].height).toEqual(25);
    expect(entityOverlays[1].height).toEqual(25);
  });
});

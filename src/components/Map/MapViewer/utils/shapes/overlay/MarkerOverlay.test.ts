/* eslint-disable no-magic-numbers */
import { Feature, Map } from 'ol';
import { Stroke, Style } from 'ol/style';
import getStroke from '@/components/Map/MapViewer/utils/shapes/style/getStroke';
import getFill from '@/components/Map/MapViewer/utils/shapes/style/getFill';
import { rgbToHex } from '@/components/Map/MapViewer/utils/shapes/style/rgbToHex';
import View from 'ol/View';
import { Coordinate } from 'ol/coordinate';
import MarkerOverlay, {
  MarkerOverlayProps,
} from '@/components/Map/MapViewer/utils/shapes/overlay/MarkerOverlay';

jest.mock('../style/getStroke');
jest.mock('../style/getFill');
jest.mock('../style/rgbToHex');

describe('MarkerOverlay', () => {
  let props: MarkerOverlayProps;

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    const mapInstance = new Map({
      target: dummyElement,
      view: new View({
        zoom: 5,
        minZoom: 3,
        maxZoom: 7,
      }),
    });
    props = {
      markerOverlay: {
        color: null,
        height: 2,
        hexColor: '#A756BA90',
        id: '3',
        modelId: 1,
        overlayId: 0,
        type: 'rectangle',
        value: 0.43,
        width: 2,
        x1: 1,
        x2: 3,
        y1: 3,
        y2: 1,
      },
      getOverlayColor: (): string => '#AABB11',
      pointToProjection: ({ x, y }: { x: number; y: number }): Coordinate => [x, y],
      mapInstance,
    };

    (getStroke as jest.Mock).mockReturnValue(new Stroke());
    (getFill as jest.Mock).mockReturnValue(new Style());
    (rgbToHex as jest.Mock).mockReturnValue('#FFFFFF');
  });

  it('should initialize line overlay feature', () => {
    const markerOverlay = new MarkerOverlay(props);

    expect(markerOverlay.markerFeature).toBeInstanceOf(Feature);
  });
});

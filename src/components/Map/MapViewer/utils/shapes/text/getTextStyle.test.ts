/* eslint-disable no-magic-numbers */
import getTextStyle from '@/components/Map/MapViewer/utils/shapes/text/getTextStyle';
import Style from 'ol/style/Style';

describe('getTextStyle', () => {
  it('should return a text style object', () => {
    const text = 'Text styl test';
    const fontSize = 12;
    const color = '#CCFFCC';
    const zIndex = 122;
    const horizontalAlign = 'CENTER';
    const textStyle = getTextStyle({
      text,
      fontSize,
      color,
      zIndex,
      horizontalAlign,
    });
    expect(textStyle).toBeInstanceOf(Style);
    expect(textStyle.getText()?.getText()).toBe(text);
    expect(textStyle.getText()?.getFont()).toBe('12pt Arial');
    expect(textStyle.getText()?.getFill()?.getColor()).toBe(color);
    expect(textStyle.getZIndex()).toBe(zIndex);
    expect(textStyle.getText()?.getTextAlign()).toBe(horizontalAlign.toLowerCase());
  });
});

/* eslint-disable no-magic-numbers */
import getWrappedTextWithFontSize from '@/components/Map/MapViewer/utils/shapes/text/getWrappedTextWithFontSize';

describe('getWrappedTextWithFontSize', () => {
  it('should return a wrapped text and font size for this text when maxWidth is limited and maxHeight is unlimited', () => {
    const text = 'Wrapped text with font size test';
    const maxWidth = 15;
    const maxHeight = 9999;

    const { text: wrappedText, fontSize } = getWrappedTextWithFontSize({
      text,
      maxWidth,
      maxHeight,
    });

    expect(wrappedText.trim()).toEqual('Wrapped text\nwith font size\ntest');
    expect(fontSize).toEqual(12);
  });

  it('should return a wrapped text and font size for this text when maxWidth is unlimited and maxHeight is limited', () => {
    const text = 'Wrapped text with font size test';
    const maxWidth = 9999;
    const maxHeight = 9;

    const { text: wrappedText, fontSize } = getWrappedTextWithFontSize({
      text,
      maxWidth,
      maxHeight,
    });

    expect(wrappedText.trim()).toEqual('Wrapped text with font size test');
    expect(fontSize).toEqual(4);
  });

  it('should return a wrapped text and font size for this text when maxWidth is limited and maxHeight is limited', () => {
    const text = 'Wrapped text with font size test';
    const maxWidth = 20;
    const maxHeight = 9;

    const { text: wrappedText, fontSize } = getWrappedTextWithFontSize({
      text,
      maxWidth,
      maxHeight,
    });

    expect(wrappedText.trim()).toEqual('Wrapped text with\nfont size test');
    expect(fontSize).toEqual(3);
  });
});

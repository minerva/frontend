/* eslint-disable no-magic-numbers */
import getTextCoords from '@/components/Map/MapViewer/utils/shapes/text/getTextCoords';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';

describe('getTextCoords', () => {
  it('should return a text coords', () => {
    const mockPointToProjection: UsePointToProjectionResult = jest.fn(point => [point.x, point.y]);
    const textCoords = getTextCoords({
      x: 20,
      y: 20,
      height: 100,
      width: 100,
      fontSize: 12,
      verticalAlign: 'MIDDLE',
      horizontalAlign: 'CENTER',
      pointToProjection: mockPointToProjection,
    });
    expect(textCoords).toEqual([70, 70]);
  });
});

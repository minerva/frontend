/* eslint-disable no-magic-numbers */
import { Extent } from 'ol/extent';
import getDividedExtents from '@/components/Map/MapViewer/utils/shapes/coords/getDividedExtents';

describe('getDividedExtents', () => {
  it('should return original extents if there is no intersection with dividingExtent', () => {
    const extentsArray: Array<Extent> = [
      [0, 0, 10, 10],
      [20, 20, 30, 30],
    ];
    const dividingExtent: Extent = [15, 15, 18, 18];

    const result = getDividedExtents(extentsArray, dividingExtent);

    expect(result).toEqual(extentsArray);
  });

  it('should divide extent when it intersects with dividingExtent', () => {
    const extentsArray: Array<Extent> = [[0, 0, 20, 20]];
    const dividingExtent: Extent = [10, 10, 15, 15];

    const result = getDividedExtents(extentsArray, dividingExtent);

    const expected = [
      [0, 0, 10, 20],
      [15, 0, 20, 20],
      [0, 15, 20, 20],
      [0, 0, 20, 10],
    ];

    expect(result).toEqual(expected);
  });

  it('should return a mix of original and divided extents when some extents intersect and others do not', () => {
    const extentsArray: Array<Extent> = [
      [0, 0, 20, 20],
      [25, 25, 30, 30],
    ];
    const dividingExtent: Extent = [10, 10, 15, 15];

    const result = getDividedExtents(extentsArray, dividingExtent);

    const expected = [
      [0, 0, 10, 20],
      [15, 0, 20, 20],
      [0, 15, 20, 20],
      [0, 0, 20, 10],
      [25, 25, 30, 30],
    ];

    expect(result).toEqual(expected);
  });

  it('should handle case where dividingExtent completely overlaps an extent', () => {
    const extentsArray: Array<Extent> = [[10, 10, 20, 20]];
    const dividingExtent: Extent = [10, 10, 20, 20];

    const result = getDividedExtents(extentsArray, dividingExtent);

    expect(result).toEqual([]);
  });

  it('should handle case where extents are completely outside dividingExtent', () => {
    const extentsArray: Array<Extent> = [
      [0, 0, 5, 5],
      [25, 25, 30, 30],
    ];
    const dividingExtent: Extent = [10, 10, 20, 20];

    const result = getDividedExtents(extentsArray, dividingExtent);

    expect(result).toEqual(extentsArray);
  });
});

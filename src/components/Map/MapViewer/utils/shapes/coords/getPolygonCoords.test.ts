/* eslint-disable no-magic-numbers */
import getCoordsX from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsX';
import getCoordsY from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsY';
import getCurveCoords from '@/components/Map/MapViewer/utils/shapes/coords/getCurveCoords';
import getBezierCurve from '@/components/Map/MapViewer/utils/shapes/coords/getBezierCurve';
import { ShapeRelAbs, ShapeRelAbsBezierPoint } from '@/types/models';
import getPolygonCoords from './getPolygonCoords';

jest.mock('./getCoordsX');
jest.mock('./getCoordsY');
jest.mock('./getCurveCoords');
jest.mock('./getBezierCurve');

describe('getPolygonCoords', () => {
  const mockPointToProjection = jest.fn(point => [point.x, point.y]);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return coordinates for ShapePoint (simple point)', () => {
    const x = 10;
    const y = 20;
    const height = 100;
    const width = 200;

    const points: Array<ShapeRelAbs | ShapeRelAbsBezierPoint> = [
      {
        type: 'REL_ABS_POINT',
        absoluteX: 0,
        absoluteY: 0,
        relativeX: 0,
        relativeY: 0,
        relativeHeightForX: null,
        relativeWidthForY: null,
      },
      {
        type: 'REL_ABS_POINT',
        absoluteX: 50,
        absoluteY: 50,
        relativeX: 10,
        relativeY: 10,
        relativeHeightForX: null,
        relativeWidthForY: null,
      },
    ];

    (getCoordsX as jest.Mock).mockReturnValueOnce(10).mockReturnValueOnce(80);
    (getCoordsY as jest.Mock).mockReturnValueOnce(20).mockReturnValueOnce(80);

    const result = getPolygonCoords({
      points,
      x,
      y,
      height,
      width,
      pointToProjection: mockPointToProjection,
    });

    expect(result).toEqual([
      [10, 20],
      [80, 80],
    ]);

    expect(getCoordsX).toHaveBeenCalledTimes(2);
    expect(getCoordsY).toHaveBeenCalledTimes(2);
  });

  it('should handle a mix of ShapePoint and ShapeCurvePoint', () => {
    const x = 10;
    const y = 20;
    const height = 100;
    const width = 200;

    const points: Array<ShapeRelAbs | ShapeRelAbsBezierPoint> = [
      {
        type: 'REL_ABS_POINT',
        absoluteX: 0,
        absoluteY: 0,
        relativeX: 0,
        relativeY: 0,
        relativeHeightForX: null,
        relativeWidthForY: null,
      },
      {
        type: 'REL_ABS_BEZIER_POINT',
        absoluteX1: 50,
        absoluteY1: 50,
        relativeX1: 10,
        relativeY1: 10,
        relativeHeightForX1: null,
        relativeWidthForY1: null,
        absoluteX2: 60,
        absoluteY2: 60,
        relativeX2: 20,
        relativeY2: 20,
        relativeHeightForX2: null,
        relativeWidthForY2: null,
        absoluteX3: 70,
        absoluteY3: 70,
        relativeX3: 30,
        relativeY3: 30,
        relativeHeightForX3: null,
        relativeWidthForY3: null,
      },
    ];

    const mockBezierCurve = [
      [15, 25],
      [35, 45],
      [55, 65],
    ];

    (getCoordsX as jest.Mock).mockReturnValueOnce(10);
    (getCoordsY as jest.Mock).mockReturnValueOnce(20);
    (getCurveCoords as jest.Mock).mockReturnValue({
      p1: [30, 40],
      p2: [50, 60],
      p3: [70, 80],
    });
    (getBezierCurve as jest.Mock).mockReturnValue(mockBezierCurve);

    const result = getPolygonCoords({
      points,
      x,
      y,
      height,
      width,
      pointToProjection: mockPointToProjection,
    });

    expect(result).toEqual([[10, 20], ...mockBezierCurve]);

    expect(getCoordsX).toHaveBeenCalledTimes(1);
    expect(getCoordsY).toHaveBeenCalledTimes(1);
    expect(getCurveCoords).toHaveBeenCalledTimes(1);
    expect(getBezierCurve).toHaveBeenCalledTimes(1);
  });
});

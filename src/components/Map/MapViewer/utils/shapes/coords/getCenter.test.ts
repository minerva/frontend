/* eslint-disable no-magic-numbers */
import { Coordinate } from 'ol/coordinate';
import getCenter from '@/components/Map/MapViewer/utils/shapes/coords/getCenter';

describe('getCenter', () => {
  it('should return a center for coordinates', () => {
    const coords: Array<Coordinate> = [
      [0, 0],
      [20, 0],
      [35, 10],
      [20, 20],
      [10, 30],
      [0, 20],
    ];

    const result = getCenter(coords);

    expect(result[0]).toBeCloseTo(17.5);
    expect(result[1]).toBeCloseTo(15);
  });
});

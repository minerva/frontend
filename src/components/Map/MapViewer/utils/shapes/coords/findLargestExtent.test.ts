/* eslint-disable no-magic-numbers */

import findLargestExtent from '@/components/Map/MapViewer/utils/shapes/coords/findLargestExtent';
import { Extent } from 'ol/extent';

describe('findLargestExtent', () => {
  it('should find largest extent from a given array', () => {
    const extents: Array<Extent> = [
      [100, 100, 200, 200],
      [150, 200, 400, 500],
    ];

    const result = findLargestExtent(extents);
    expect(result).toEqual(extents[1]);
  });
});

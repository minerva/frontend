/* eslint-disable no-magic-numbers */

import getCoordsX from './getCoordsX';

describe('getCoordsX', () => {
  it('should calculate the correct coordinate when all parameters are provided', () => {
    const x = 10;
    const absoluteX = 20;
    const relativeX = 10;
    const relativeHeightForX = 5;
    const height = 100;
    const width = 200;

    const result = getCoordsX(x, absoluteX, relativeX, relativeHeightForX, height, width);
    expect(result).toBe(55);
  });

  it('should handle null for relativeHeightForX correctly', () => {
    const x = 10;
    const absoluteX = 20;
    const relativeX = 10;
    const relativeHeightForX = null;
    const height = 100;
    const width = 200;

    const result = getCoordsX(x, absoluteX, relativeX, relativeHeightForX, height, width);
    expect(result).toBe(50);
  });

  it('should calculate the correct coordinate when relativeX and relativeHeightForX are zero', () => {
    const x = 10;
    const absoluteX = 20;
    const relativeX = 0;
    const relativeHeightForX = 0;
    const height = 100;
    const width = 200;

    const result = getCoordsX(x, absoluteX, relativeX, relativeHeightForX, height, width);
    expect(result).toBe(30);
  });

  it('should handle negative values for parameters', () => {
    const x = -10;
    const absoluteX = -20;
    const relativeX = -10;
    const relativeHeightForX = -5;
    const height = 100;
    const width = 200;

    const result = getCoordsX(x, absoluteX, relativeX, relativeHeightForX, height, width);
    expect(result).toBe(-55);
  });
});

/* eslint-disable no-magic-numbers */
import { Extent } from 'ol/extent';
import getBoundingBoxFromExtent from '@/components/Map/MapViewer/utils/shapes/coords/getBoundingBoxFromExtent';

describe('getBoundingBoxFromExtent', () => {
  it('should return a bounding box for extent', () => {
    const extent: Extent = [0, 195700, 195700, 0];
    const mapSize = {
      width: 512,
      height: 512,
      tileSize: 256,
      minZoom: 2,
      maxZoom: 4,
    };

    const result = getBoundingBoxFromExtent(extent, mapSize);

    expect(result).toHaveProperty('x', 1024);
    expect(result).toHaveProperty('y', 1024);
    expect(result).toHaveProperty('width');
    expect(result).toHaveProperty('height');

    expect(result.width).toBeCloseTo(10);
    expect(result.height).toBeCloseTo(10);
  });
});

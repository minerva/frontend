/* eslint-disable no-magic-numbers */
import getRotation from '@/components/Map/MapViewer/utils/shapes/coords/getRotation';
import { Coordinate } from 'ol/coordinate';

const testCases: Array<[Coordinate, Coordinate, number]> = [
  [[0, 0], [1, 1], -0.785],
  [[1, 1], [1, 2], -1.57],
  [[2, 2], [3, 0], 1.107],
];

describe('getRotation', () => {
  it.each(testCases)(
    'should return the correct rotation for start: %s and end: %s',
    (start: Coordinate, end: Coordinate, expected: number) => {
      const result = getRotation(start, end);
      expect(result).toBeCloseTo(expected);
    },
  );
});

/* eslint-disable no-magic-numbers */
import getCoordsX from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsX';
import getCoordsY from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsY';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import { ShapeRelAbs } from '@/types/models';
import getEllipseCoords from './getEllipseCoords';

jest.mock('./getCoordsX');
jest.mock('./getCoordsY');

describe('getEllipseCoords', () => {
  const mockPointToProjection: UsePointToProjectionResult = jest.fn(point => [point.x, point.y]);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should calculate correct coordinates for an ellipse', () => {
    const x = 10;
    const y = 20;
    const height = 100;
    const width = 200;
    const center = {
      type: 'REL_ABS_POINT',
      absoluteX: 15,
      absoluteY: 25,
      relativeX: 5,
      relativeY: 10,
      relativeHeightForX: null,
      relativeWidthForY: null,
    } as ShapeRelAbs;
    const radius = {
      type: 'REL_ABS_POINT',
      absoluteX: 30,
      absoluteY: 40,
      relativeX: 10,
      relativeY: 15,
      relativeHeightForX: null,
      relativeWidthForY: null,
    } as ShapeRelAbs;
    const points = 4;

    (getCoordsX as jest.Mock).mockReturnValue(35);
    (getCoordsY as jest.Mock).mockReturnValue(55);

    const result = getEllipseCoords({
      x,
      y,
      center,
      radius,
      height,
      width,
      pointToProjection: mockPointToProjection,
      points,
    });

    expect(result).toHaveLength(points + 1);

    expect(result[0]).toEqual(result[result.length - 1]);

    expect(getCoordsX).toHaveBeenCalledTimes(1);
    expect(getCoordsY).toHaveBeenCalledTimes(1);
    expect(mockPointToProjection).toHaveBeenCalledTimes(points);
  });
});

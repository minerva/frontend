/* eslint-disable no-magic-numbers */
import getCoordsX from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsX';
import getCoordsY from '@/components/Map/MapViewer/utils/shapes/coords/getCoordsY';
import { UsePointToProjectionResult } from '@/utils/map/usePointToProjection';
import { ShapeRelAbsBezierPoint } from '@/types/models';
import getCurveCoords from './getCurveCoords';

jest.mock('./getCoordsX');
jest.mock('./getCoordsY');

describe('getCurveCoords', () => {
  const mockPointToProjection: UsePointToProjectionResult = jest.fn(point => [point.x, point.y]);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should calculate correct coordinates for p1, p2, and p3', () => {
    const x = 10;
    const y = 20;
    const height = 100;
    const width = 200;

    const point: ShapeRelAbsBezierPoint = {
      type: 'REL_ABS_BEZIER_POINT',
      absoluteX1: 10,
      relativeX1: 5,
      relativeHeightForX1: 2,
      absoluteY1: 15,
      relativeY1: 3,
      relativeWidthForY1: 1,
      absoluteX2: 12,
      relativeX2: 7,
      relativeHeightForX2: 4,
      absoluteY2: 16,
      relativeY2: 4,
      relativeWidthForY2: 2,
      absoluteX3: 14,
      relativeX3: 9,
      relativeHeightForX3: 6,
      absoluteY3: 18,
      relativeY3: 5,
      relativeWidthForY3: 3,
    };

    (getCoordsX as jest.Mock)
      .mockReturnValueOnce(25)
      .mockReturnValueOnce(35)
      .mockReturnValueOnce(45);
    (getCoordsY as jest.Mock)
      .mockReturnValueOnce(30)
      .mockReturnValueOnce(40)
      .mockReturnValueOnce(50);

    const result = getCurveCoords({
      x,
      y,
      point,
      height,
      width,
      pointToProjection: mockPointToProjection,
    });

    expect(result.p1).toEqual([35, 40]);
    expect(result.p2).toEqual([45, 50]);
    expect(result.p3).toEqual([25, 30]);

    expect(getCoordsX).toHaveBeenCalledTimes(3);
    expect(getCoordsY).toHaveBeenCalledTimes(3);
    expect(mockPointToProjection).toHaveBeenCalledTimes(3);
  });
});

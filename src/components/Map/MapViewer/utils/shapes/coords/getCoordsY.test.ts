/* eslint-disable no-magic-numbers */
import getCoordsY from './getCoordsY';

describe('getCoordsY', () => {
  it('should calculate the correct coordinate when all parameters are provided', () => {
    const x = 10;
    const absoluteY = 20;
    const relativeY = 10;
    const relativeWidthForY = 5;
    const height = 80;
    const width = 150;

    const result = getCoordsY(x, absoluteY, relativeY, relativeWidthForY, height, width);
    expect(result).toBe(45.5);
  });

  it('should handle null for relativeWidthForY correctly', () => {
    const x = 10;
    const absoluteY = 20;
    const relativeY = 10;
    const relativeWidthForY = null;
    const height = 100;
    const width = 140;

    const result = getCoordsY(x, absoluteY, relativeY, relativeWidthForY, height, width);
    expect(result).toBe(40);
  });

  it('should calculate the correct coordinate when relativeY and relativeWidthForY are zero', () => {
    const x = 10;
    const absoluteY = 20;
    const relativeY = 0;
    const relativeWidthForY = 0;
    const height = 100;
    const width = 200;

    const result = getCoordsY(x, absoluteY, relativeY, relativeWidthForY, height, width);
    expect(result).toBe(30);
  });

  it('should handle negative values for parameters', () => {
    const x = -10;
    const absoluteY = -20;
    const relativeY = -10;
    const relativeWidthForY = -5;
    const height = 100;
    const width = 200;

    const result = getCoordsY(x, absoluteY, relativeY, relativeWidthForY, height, width);
    expect(result).toBe(-50);
  });
});

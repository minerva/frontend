/* eslint-disable no-magic-numbers */
import { Line } from '@/types/models';
import { BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import getLineSegments from './getLineSegments';

describe('getLineSegments', () => {
  it('should return flattened array of projected coordinates for all line segments', () => {
    const pointToProjection = jest.fn(({ x, y }) => [x * 10, y * 10]);

    const line = {
      id: 1,
      width: 1,
      color: BLACK_COLOR,
      z: 1,
      startArrow: {
        arrowType: 'NONE',
        angle: 2.748893571891069,
        lineType: 'SOLID',
        length: 15.0,
      },
      endArrow: {
        arrowType: 'NONE',
        angle: 2.748893571891069,
        lineType: 'SOLID',
        length: 15.0,
      },
      segments: [
        { x1: 1, y1: 2, x2: 3, y2: 4 },
        { x1: 3, y1: 4, x2: 5, y2: 6 },
      ],
    } as Line;

    const result = getLineSegments(line, pointToProjection);

    expect(pointToProjection).toHaveBeenCalledTimes(3);
    expect(pointToProjection).toHaveBeenNthCalledWith(1, { x: 1, y: 2 });
    expect(pointToProjection).toHaveBeenNthCalledWith(2, { x: 3, y: 4 });
    expect(pointToProjection).toHaveBeenNthCalledWith(3, { x: 5, y: 6 });

    expect(result).toEqual([
      [10, 20],
      [30, 40],
      [50, 60],
    ]);
  });
});

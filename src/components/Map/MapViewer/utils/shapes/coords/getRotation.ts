/* eslint-disable no-magic-numbers */
import { Coordinate } from 'ol/coordinate';

export default function getRotation(start: Coordinate, end: Coordinate): number {
  return Math.atan2(-end[1] + start[1], end[0] - start[0]);
}

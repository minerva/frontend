/* eslint-disable no-magic-numbers */
import getBezierCurve from './getBezierCurve';

describe('getBezierCurve', () => {
  it('should return an array of coordinates representing a Bezier curve', () => {
    const p0 = [0, 0];
    const p1 = [10, 20];
    const p2 = [20, 20];
    const p3 = [30, 0];

    const result = getBezierCurve({
      p0,
      p1,
      p2,
      p3,
      numPoints: 50,
    });

    expect(result).toHaveLength(51);

    expect(result[0]).toEqual(p0);
    expect(result[result.length - 1]).toEqual(p3);

    result.forEach(coord => {
      expect(coord).toHaveLength(2);
      expect(typeof coord[0]).toBe('number');
      expect(typeof coord[1]).toBe('number');
    });
  });

  it('should generate a different number of points based on numPoints', () => {
    const p0 = [0, 0];
    const p1 = [10, 20];
    const p2 = [20, 20];
    const p3 = [30, 0];

    const result100 = getBezierCurve({
      p0,
      p1,
      p2,
      p3,
      numPoints: 100,
    });

    const result10 = getBezierCurve({
      p0,
      p1,
      p2,
      p3,
      numPoints: 10,
    });

    expect(result100).toHaveLength(101);
    expect(result10).toHaveLength(11);
  });

  it('should return correct coordinates for a simple straight line', () => {
    const p0 = [0, 0];
    const p1 = [0, 0];
    const p2 = [10, 10];
    const p3 = [10, 10];

    const result = getBezierCurve({
      p0,
      p1,
      p2,
      p3,
      numPoints: 10,
    });

    expect(result[0]).toEqual([0, 0]);
    expect(result[result.length - 1]).toEqual([10, 10]);

    result.forEach(coord => {
      expect(coord[0]).toBe(coord[1]);
    });
  });
});

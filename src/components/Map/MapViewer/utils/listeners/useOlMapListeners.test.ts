/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { renderHook } from '@testing-library/react';
import { View } from 'ol';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import * as positionListener from '@/components/Map/MapViewer/utils/listeners/onMapPositionChange';
import { useOlMapListeners } from '@/components/Map/MapViewer/utils/listeners/useOlMapListeners';
import { onMapLeftClick } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/onMapLeftClick';

jest.mock('./onMapPositionChange', () => ({
  __esModule: true,
  onMapPositionChange: jest.fn(),
}));

jest.mock('./mouseClick/mouseLeftClick/onMapLeftClick', () => ({
  __esModule: true,
  onMapLeftClick: jest.fn(),
}));

jest.mock('use-debounce', () => {
  return {
    useDebounce: () => {},
    useDebouncedCallback: () => {},
  };
});

describe('useOlMapListeners - util', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: {
      data: { ...initialMapDataFixture },
      loading: 'succeeded',
      error: { message: '', name: '' },
      openedMaps: openedMapsThreeSubmapsFixture,
    },
  });

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('on change:center view event', () => {
    it('should run onMapPositionChange event', () => {
      const CALLED_ONCE = 1;
      const view = new View();

      renderHook(() => useOlMapListeners({ view, mapInstance: undefined }), { wrapper: Wrapper });
      view.dispatchEvent('change:center');

      expect(positionListener.onMapPositionChange).toBeCalledTimes(CALLED_ONCE);
    });
  });

  describe('on left click event', () => {
    it('should run onMapLeftClick event', () => {
      const CALLED_ONCE = 1;
      const view = new View();

      renderHook(() => useOlMapListeners({ view, mapInstance: undefined }), {
        wrapper: Wrapper,
      });
      view.dispatchEvent('singleclick');

      expect(onMapLeftClick).toBeCalledTimes(CALLED_ONCE);
    });
  });
});

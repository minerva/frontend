import { ZERO } from '@/constants/common';
import { initialStateFixture } from '@/redux/drawer/drawerFixture';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { useHandlePinIconClick } from './useHandlePinIconClick';

describe('useHandlePinIconClick - util', () => {
  describe('when pin clicked', () => {
    describe('when tab is invalid', () => {
      const pinId = 123;
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...INITIAL_STORE_STATE_MOCK,
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: '',
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: null,
                      id: pinId,
                      model: ZERO,
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
      });

      const {
        result: { current: onPinIconClick },
      } = renderHook(() => useHandlePinIconClick(), {
        wrapper: Wrapper,
      });

      onPinIconClick({ id: pinId });

      it('should do not dispatch any action', () => {
        const actions = store.getActions();

        expect(actions).toStrictEqual([]);
      });
    });

    describe('when tab is already selected', () => {
      const pinId = 123;
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...INITIAL_STORE_STATE_MOCK,
        drawer: {
          ...initialStateFixture,
          drawerName: 'search',
          isOpen: true,
          searchDrawerState: {
            ...initialStateFixture.searchDrawerState,
            selectedSearchElement: 'search-tab',
          },
        },
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: 'search-tab',
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: null,
                      id: pinId,
                      model: ZERO,
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
      });

      const {
        result: { current: onPinIconClick },
      } = renderHook(() => useHandlePinIconClick(), {
        wrapper: Wrapper,
      });

      onPinIconClick({ id: pinId });

      it('should do not dispatch any action', () => {
        const actions = store.getActions();

        expect(actions).toStrictEqual([]);
      });
    });

    describe('when pin is marker', () => {
      const pinId = 1;
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...INITIAL_STORE_STATE_MOCK,
        drawer: {
          ...initialStateFixture,
          drawerName: 'search',
          isOpen: true,
          searchDrawerState: {
            ...initialStateFixture.searchDrawerState,
            selectedSearchElement: 'search-tab',
          },
        },
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: 'search-tab',
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: null,
                      id: pinId,
                      model: ZERO,
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
      });

      const {
        result: { current: onPinIconClick },
      } = renderHook(() => useHandlePinIconClick(), {
        wrapper: Wrapper,
      });

      onPinIconClick({ id: pinId });

      it('should do not dispatch any action', () => {
        const actions = store.getActions();

        expect(actions).toStrictEqual([]);
      });
    });

    describe('when all valid', () => {
      const pinId = 123;
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...INITIAL_STORE_STATE_MOCK,
        drawer: {
          ...initialStateFixture,
          drawerName: 'search',
          isOpen: true,
          searchDrawerState: {
            ...initialStateFixture.searchDrawerState,
            selectedSearchElement: 'search-tab-2',
          },
        },
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: 'search-tab',
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: null,
                      id: pinId,
                      model: ZERO,
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
      });

      const {
        result: { current: onPinIconClick },
      } = renderHook(() => useHandlePinIconClick(), {
        wrapper: Wrapper,
      });

      onPinIconClick({ id: pinId });

      it('should dispatch action', () => {
        const actions = store.getActions();
        expect(actions).toStrictEqual([{ payload: 'search-tab', type: 'drawer/selectTab' }]);
      });
    });
  });
});

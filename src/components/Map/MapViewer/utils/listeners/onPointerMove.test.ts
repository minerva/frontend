/* eslint-disable @typescript-eslint/no-explicit-any */
import { Feature, Map, MapBrowserEvent } from 'ol';
import { onPointerMove } from './onPointerMove';

const TARGET_STRING = 'abcd';

const EVENT_DRAGGING_MOCK = {
  dragging: true,
} as unknown as MapBrowserEvent<PointerEvent>;

const EVENT_MOCK = {
  dragging: false,
  originalEvent: undefined,
} as unknown as MapBrowserEvent<PointerEvent>;

const MAP_INSTANCE_BASE_MOCK = {
  getEventPixel: (): void => {},
  forEachFeatureAtPixel: (): void => {},
};

const showPointerMoveOnLine = (): boolean => {
  return false;
};

describe('onPointerMove - util', () => {
  describe('when event dragging', () => {
    const target = document.createElement('div');
    const mapInstance = {
      ...MAP_INSTANCE_BASE_MOCK,
      getTarget: () => target,
    } as unknown as Map;

    it('should return nothing and not modify target', () => {
      expect(onPointerMove(mapInstance, EVENT_DRAGGING_MOCK, showPointerMoveOnLine)).toBe(
        undefined,
      );
      expect((mapInstance as any).getTarget().style.cursor).toBe('');
    });
  });

  describe('when pin feature present and target is html', () => {
    const target = document.createElement('div');
    const mapInstance = {
      ...MAP_INSTANCE_BASE_MOCK,
      forEachFeatureAtPixel: () => new Feature(),
      getTarget: () => target,
    } as unknown as Map;

    it('should return nothing and modify target', () => {
      expect(onPointerMove(mapInstance, EVENT_MOCK, showPointerMoveOnLine)).toBe(undefined);
      expect((mapInstance as any).getTarget().style.cursor).toBe('pointer');
    });
  });

  describe('when pin feature present and target is string', () => {
    const mapInstance = {
      ...MAP_INSTANCE_BASE_MOCK,
      forEachFeatureAtPixel: () => new Feature(),
      getTarget: () => TARGET_STRING,
    } as unknown as Map;

    it('should return nothing and not modify target', () => {
      expect(onPointerMove(mapInstance, EVENT_MOCK, showPointerMoveOnLine)).toBe(undefined);
      expect((mapInstance as any).getTarget()).toBe(TARGET_STRING);
    });
  });

  describe('when pin feature is not present and target is html', () => {
    const target = document.createElement('div');
    const mapInstance = {
      ...MAP_INSTANCE_BASE_MOCK,
      forEachFeatureAtPixel: () => undefined,
      getTarget: () => target,
    } as unknown as Map;

    it('should return nothing and not modify target', () => {
      expect(onPointerMove(mapInstance, EVENT_MOCK, showPointerMoveOnLine)).toBe(undefined);
      expect((mapInstance as any).getTarget().style.cursor).toBe('');
    });
  });
});

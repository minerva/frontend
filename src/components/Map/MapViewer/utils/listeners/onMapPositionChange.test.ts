import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { mapDataSelector } from '@/redux/map/map.selectors';
import { MapSize } from '@/redux/map/map.types';
import { Point } from '@/types/map';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { ObjectEvent } from 'openlayers';
import { onMapPositionChange } from './onMapPositionChange';

const getEvent = (targetValues: ObjectEvent['target']['values_']): ObjectEvent =>
  ({
    target: {
      values_: targetValues,
    },
  }) as unknown as ObjectEvent;

/* eslint-disable no-magic-numbers */
describe('onMapPositionChange - util', () => {
  const cases: [MapSize, ObjectEvent['target']['values_'], Point][] = [
    [
      {
        width: 26779.25,
        height: 13503,
        tileSize: 256,
        minZoom: 2,
        maxZoom: 9,
      },
      {
        center: [-18320768.57141088, 18421138.0064355],
        zoom: 6,
      },
      {
        x: 4589,
        y: 4320,
        z: 6,
      },
    ],
    [
      {
        width: 5170,
        height: 1535.1097689075634,
        tileSize: 256,
        minZoom: 2,
        maxZoom: 7,
      },
      {
        center: [-17172011.827663105, 18910737.010646995],
        zoom: 6.68620779943448,
      },
      {
        x: 1479,
        y: 581,
        z: 6.68620779943448,
      },
    ],
  ];

  it.each(cases)(
    'should set map data position to valid one',
    (mapSize, targetValues, lastPosition) => {
      const { Wrapper, store } = getReduxWrapperWithStore();
      const { result } = renderHook(() => useAppDispatch(), { wrapper: Wrapper });
      const dispatch = result.current;
      const event = getEvent(targetValues);

      onMapPositionChange(mapSize, dispatch)(event);

      const { position } = mapDataSelector(store.getState());
      expect(position.last).toMatchObject(lastPosition);
    },
  );
});

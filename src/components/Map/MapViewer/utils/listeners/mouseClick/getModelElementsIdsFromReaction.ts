import { NewReaction } from '@/types/models';

export default function getModelElementsIdsFromReaction(reaction: NewReaction): Array<number> {
  return [...reaction.products, ...reaction.reactants, ...reaction.modifiers].map(
    bioEntity => bioEntity.element,
  );
}

/* eslint-disable no-magic-numbers */
import { updateLastClick } from '@/redux/map/map.slice';
import { closeDrawer } from '@/redux/drawer/drawer.slice';
import { resetReactionsData } from '@/redux/reactions/reactions.slice';
import { handleFeaturesClick } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/handleFeaturesClick';
import Map from 'ol/Map';
import { onMapLeftClick } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/onMapLeftClick';
import { Comment } from '@/types/models';
import SimpleGeometry from 'ol/geom/SimpleGeometry';
import { Feature } from 'ol';
import { FEATURE_TYPE } from '@/constants/features';
import VectorLayer from 'ol/layer/Vector';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import { clearSearchModelElements } from '@/redux/modelElements/modelElements.slice';
import * as leftClickHandleAlias from './leftClickHandleAlias';
import * as clickHandleReaction from '../clickHandleReaction';

jest.mock('./handleFeaturesClick', () => ({
  handleFeaturesClick: jest.fn(),
}));
jest.mock('./leftClickHandleAlias', () => ({
  __esModule: true,
  ...jest.requireActual('./leftClickHandleAlias'),
}));
const leftClickHandleAliasSpy = jest.spyOn(leftClickHandleAlias, 'leftClickHandleAlias');
jest.mock('../clickHandleReaction', () => ({
  __esModule: true,
  ...jest.requireActual('../clickHandleReaction'),
}));
const clickHandleReactionSpy = jest.spyOn(clickHandleReaction, 'clickHandleReaction');

describe('onMapLeftClick', () => {
  const modelId = 1;
  const isResultDrawerOpen = true;
  const comments: Array<Comment> = [];
  let mapInstance: Map;
  const vectorLayer = new VectorLayer({});
  vectorLayer.set('type', LAYER_TYPE.PROCESS_LAYER);
  const event = { coordinate: [100, 50], pixel: [200, 100] };
  const mapSize = {
    width: 90,
    height: 90,
    tileSize: 256,
    minZoom: 2,
    maxZoom: 9,
  };

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    mapInstance = new Map({ target: dummyElement });
    jest.clearAllMocks();
  });

  it('dispatches updateLastClick and resets data if no feature at pixel', async () => {
    const dispatch = jest.fn();
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(new Feature({ zIndex: 1 }), vectorLayer, null as unknown as SimpleGeometry);
    });
    await onMapLeftClick(
      mapSize,
      modelId,
      dispatch,
      isResultDrawerOpen,
      comments,
      [],
      [],
    )(event, mapInstance);

    expect(dispatch).toHaveBeenCalledWith(updateLastClick(expect.any(Object)));
    expect(dispatch).toHaveBeenCalledWith(closeDrawer());
    expect(dispatch).toHaveBeenCalledWith(resetReactionsData());
    expect(dispatch).toHaveBeenCalledWith(clearSearchModelElements());
  });

  it('calls leftClickHandleAlias if feature type is ALIAS', async () => {
    const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }];
    const dispatch = jest.fn(() => ({
      unwrap: jest.fn().mockResolvedValue(mockBioEntities),
    }));
    const feature = new Feature({ id: 1, type: FEATURE_TYPE.ALIAS, zIndex: 1 });
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(feature, vectorLayer, null as unknown as SimpleGeometry);
    });
    (handleFeaturesClick as jest.Mock).mockReturnValue({ shouldBlockCoordSearch: false });

    await onMapLeftClick(
      mapSize,
      modelId,
      dispatch,
      isResultDrawerOpen,
      comments,
      [],
      [],
    )(event, mapInstance);

    expect(leftClickHandleAliasSpy).toHaveBeenCalledWith(dispatch);
  });

  it('calls clickHandleReaction if feature type is REACTION', async () => {
    const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }];
    const dispatch = jest.fn(() => ({
      unwrap: jest.fn().mockResolvedValue(mockBioEntities),
    }));
    const feature = new Feature({ id: 1, type: FEATURE_TYPE.REACTION, zIndex: 1 });
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(feature, vectorLayer, null as unknown as SimpleGeometry);
    });
    (handleFeaturesClick as jest.Mock).mockReturnValue({ shouldBlockCoordSearch: false });

    await onMapLeftClick(
      mapSize,
      modelId,
      dispatch,
      isResultDrawerOpen,
      comments,
      [],
      [],
    )(event, mapInstance);

    expect(clickHandleReactionSpy).toHaveBeenCalledWith(dispatch);
  });
});

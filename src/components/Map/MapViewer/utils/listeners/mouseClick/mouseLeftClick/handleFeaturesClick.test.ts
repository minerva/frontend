import { FEATURE_TYPE } from '@/constants/features';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { Feature } from 'ol';
import { handleFeaturesClick } from './handleFeaturesClick';

describe('handleFeaturesClick - util', () => {
  beforeEach(() => {
    PluginsEventBus.events = [];
  });

  describe('when feature contains pin icon marker', () => {
    const { store } = getReduxStoreWithActionsListener();
    const { dispatch } = store;
    const featureId = 1234;
    const features = [
      new Feature({
        id: featureId,
        type: FEATURE_TYPE.PIN_ICON_MARKER,
      }),
    ];

    it('should dispatch event onPinIconClick', () => {
      const dispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      handleFeaturesClick(features, dispatch, []);
      expect(dispatchEventSpy).toHaveBeenCalledWith('onPinIconClick', { id: featureId });
    });

    it('should return shouldBlockCoordSearch=true', () => {
      expect(handleFeaturesClick(features, dispatch, [])).toStrictEqual({
        shouldBlockCoordSearch: true,
      });
    });
  });

  describe('when feature contains pin icon bioentity', () => {
    const { store } = getReduxStoreWithActionsListener();
    const { dispatch } = store;
    const featureId = 1234;
    const features = [
      new Feature({
        id: featureId,
        type: FEATURE_TYPE.PIN_ICON_BIOENTITY,
      }),
    ];

    it('should dispatch event onPinIconClick', () => {
      const dispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      handleFeaturesClick(features, dispatch, []);
      expect(dispatchEventSpy).toHaveBeenCalledWith('onPinIconClick', { id: featureId });
    });

    it('should dispatch actions regarding opening entity drawer', () => {
      const { store: localStore } = getReduxStoreWithActionsListener();
      const { dispatch: localDispatch } = localStore;
      handleFeaturesClick(features, localDispatch, []);
      expect(store.getActions()).toStrictEqual([
        { payload: 1234, type: 'drawer/openBioEntityDrawerById' },
      ]);
    });

    it('should return shouldBlockCoordSearch=true', () => {
      expect(handleFeaturesClick(features, dispatch, [])).toStrictEqual({
        shouldBlockCoordSearch: true,
      });
    });
  });

  describe('when feature contains surface overlay', () => {
    const { store } = getReduxStoreWithActionsListener();
    const { dispatch } = store;
    const featureId = 1234;
    const features = [
      new Feature({
        id: featureId,
        type: FEATURE_TYPE.SURFACE_OVERLAY,
      }),
    ];

    it('should dispatch event onSurfaceClick', () => {
      const dispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      handleFeaturesClick(features, dispatch, []);
      expect(dispatchEventSpy).toHaveBeenCalledWith('onSurfaceClick', { id: featureId });
    });

    it('should return shouldBlockCoordSearch=false', () => {
      expect(handleFeaturesClick(features, dispatch, [])).toStrictEqual({
        shouldBlockCoordSearch: false,
      });
    });
  });

  describe('when feature contains surface marker', () => {
    const { store } = getReduxStoreWithActionsListener();
    const { dispatch } = store;
    const featureId = 1234;
    const features = [
      new Feature({
        id: featureId,
        type: FEATURE_TYPE.SURFACE_MARKER,
      }),
    ];

    it('should dispatch event onSurfaceClick', () => {
      const dispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      handleFeaturesClick(features, dispatch, []);
      expect(dispatchEventSpy).toHaveBeenCalledWith('onSurfaceClick', { id: featureId });
    });

    it('should return shouldBlockCoordSearch=false', () => {
      expect(handleFeaturesClick(features, dispatch, [])).toStrictEqual({
        shouldBlockCoordSearch: false,
      });
    });
  });
});

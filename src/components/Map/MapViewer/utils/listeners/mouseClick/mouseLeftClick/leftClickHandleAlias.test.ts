/* eslint-disable no-magic-numbers */
import { leftClickHandleAlias } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseLeftClick/leftClickHandleAlias';
import { openBioEntityDrawerById, selectTab } from '@/redux/drawer/drawer.slice';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import { searchFitBounds } from '@/services/pluginsManager/map/triggerSearch/searchFitBounds';
import { Feature } from 'ol';
import { FEATURE_TYPE } from '@/constants/features';
import { ModelElement } from '@/types/models';

jest.mock('../../../../../../../services/pluginsManager/map/triggerSearch/searchFitBounds');
const eventBusDispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');

describe('leftClickHandleAlias', () => {
  let dispatch: jest.Mock;
  const modelId = 1;
  const hasFitBounds = true;
  const feature = new Feature({ type: FEATURE_TYPE.ALIAS, id: 1 });

  beforeEach(() => {
    jest.clearAllMocks();
    dispatch = jest.fn();
  });

  it('dispatches getMultiBioEntityByIds, selectTab, and openBioEntityDrawerById, then triggers PluginsEventBus and searchFitBounds when hasFitBounds is true', async () => {
    const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }] as Array<ModelElement>;
    dispatch = jest.fn(() => ({
      unwrap: jest.fn().mockResolvedValue(mockBioEntities),
    }));

    await leftClickHandleAlias(dispatch, hasFitBounds)(mockBioEntities, feature, modelId);
    expect(dispatch).toHaveBeenCalledTimes(4);

    expect(dispatch).toHaveBeenCalledWith(selectTab('1'));
    expect(dispatch).toHaveBeenCalledWith(openBioEntityDrawerById(1));

    expect(eventBusDispatchEventSpy).toHaveBeenCalledWith('onSearch', {
      type: 'bioEntity',
      searchValues: [{ id: 1, modelId, type: FEATURE_TYPE.ALIAS }],
      results: [
        mockBioEntities.map(modelElement => ({
          perfect: true,
          bioEntity: modelElement,
        })),
      ],
    });

    expect(searchFitBounds).toHaveBeenCalled();
  });

  it('does not call searchFitBounds when hasFitBounds is false', async () => {
    const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }] as Array<ModelElement>;
    dispatch.mockImplementation(() => ({
      unwrap: jest.fn().mockResolvedValue(mockBioEntities),
    }));

    await leftClickHandleAlias(dispatch, false)(mockBioEntities, feature, modelId);

    expect(searchFitBounds).not.toHaveBeenCalled();
  });
});

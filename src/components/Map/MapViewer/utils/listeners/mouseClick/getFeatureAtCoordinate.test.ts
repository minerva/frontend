/* eslint-disable no-magic-numbers */
import { Feature } from 'ol';
import VectorLayer from 'ol/layer/Vector';
import Map from 'ol/Map';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import { FEATURE_TYPE } from '@/constants/features';
import getFeatureAtCoordinate from '@/components/Map/MapViewer/utils/listeners/mouseClick/getFeatureAtCoordinate';
import SimpleGeometry from 'ol/geom/SimpleGeometry';

describe('getFeatureAtCoordinate', () => {
  let mapInstance: Map;
  const coordinate = [100, 50];
  const point = { x: 100, y: 50 };
  const vectorLayer = new VectorLayer({});
  vectorLayer.set('type', LAYER_TYPE.PROCESS_LAYER);

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    mapInstance = new Map({ target: dummyElement });
    jest.clearAllMocks();
  });

  it('returns undefined if mapInstance is not provided', () => {
    const result = getFeatureAtCoordinate({
      mapInstance: undefined,
      coordinate,
      point,
    });
    expect(result).toBeUndefined();
  });

  it('returns undefined if no feature is found at the pixel', () => {
    jest.spyOn(mapInstance, 'getPixelFromCoordinate').mockReturnValue([200, 100]);
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation(() => {});

    const result = getFeatureAtCoordinate({
      mapInstance,
      coordinate,
      point,
    });
    expect(result).toBeUndefined();
  });

  it('returns the feature if a filled compartment feature is found', () => {
    const feature = new Feature({ type: FEATURE_TYPE.COMPARTMENT, filled: true, zIndex: 1 });

    jest.spyOn(mapInstance, 'getPixelFromCoordinate').mockReturnValue([200, 100]);
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(feature, vectorLayer, new SimpleGeometry());
    });

    const result = getFeatureAtCoordinate({
      mapInstance,
      coordinate,
      point,
    });
    expect(result).toBe(feature);
  });

  it('returns the feature if a non-compartment feature is found', () => {
    const feature = new Feature({ type: FEATURE_TYPE.ALIAS, zIndex: 1 });

    jest.spyOn(mapInstance, 'getPixelFromCoordinate').mockReturnValue([200, 100]);
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(feature, vectorLayer, new SimpleGeometry());
    });

    const result = getFeatureAtCoordinate({
      mapInstance,
      coordinate,
      point,
    });
    expect(result).toBe(feature);
  });

  it('ignores features with invalid zIndex', () => {
    const feature = new Feature({ type: FEATURE_TYPE.ALIAS, zIndex: -1 });

    jest.spyOn(mapInstance, 'getPixelFromCoordinate').mockReturnValue([200, 100]);
    jest.spyOn(mapInstance, 'forEachFeatureAtPixel').mockImplementation((_, callback) => {
      callback(feature, vectorLayer, new SimpleGeometry());
    });

    const result = getFeatureAtCoordinate({
      mapInstance,
      coordinate,
      point,
    });
    expect(result).toBeUndefined();
  });
});

import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { handleDataReset } from './handleDataReset';

describe('handleDataReset', () => {
  const { store } = getReduxStoreWithActionsListener();
  const { dispatch } = store;

  it('should dispatch reset actions', () => {
    dispatch(handleDataReset);

    const actions = store.getActions();
    const actionsTypes = [
      'reactions/resetReactionsData',
      'search/clearSearchData',
      'drugs/clearDrugsData',
      'chemicals/clearChemicalsData',
      'contextMenu/closeContextMenu',
      'entityNumber/clearEntityNumberData',
    ];

    expect(actions.map(a => a.type)).toStrictEqual(actionsTypes);
  });
});

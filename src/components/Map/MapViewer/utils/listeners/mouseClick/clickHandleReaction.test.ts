/* eslint-disable no-magic-numbers */
import { openReactionDrawerById, selectTab } from '@/redux/drawer/drawer.slice';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import { searchFitBounds } from '@/services/pluginsManager/map/triggerSearch/searchFitBounds';
import { clickHandleReaction } from '@/components/Map/MapViewer/utils/listeners/mouseClick/clickHandleReaction';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';

jest.mock('../../../../../../services/pluginsManager/map/triggerSearch/searchFitBounds');
const eventBusDispatchEventSpy = jest.spyOn(PluginsEventBus, 'dispatchEvent');

describe('clickHandleReaction', () => {
  let dispatch: jest.Mock;
  let modelId = 1;
  let reactionId = 1;
  const hasFitBounds = true;

  beforeEach(() => {
    jest.clearAllMocks();
    dispatch = jest.fn();
  });

  it('dispatches selectTab and openBioEntityDrawerById, then triggers PluginsEventBus and searchFitBounds when hasFitBounds is true', async () => {
    dispatch = jest.fn(() => {
      return {
        unwrap: jest.fn().mockResolvedValue([]),
        payload: {
          data: [newReactionFixture],
        },
      };
    });
    reactionId = newReactionFixture.id;
    modelId = newReactionFixture.model;

    clickHandleReaction(dispatch, hasFitBounds)(
      [],
      [{ ...newReactionFixture, id: reactionId }],
      reactionId,
      modelId,
    );
    expect(dispatch).toHaveBeenCalledTimes(5);
    expect(dispatch).toHaveBeenCalledWith(openReactionDrawerById(reactionId));
    expect(dispatch).toHaveBeenCalledWith(selectTab(''));
    expect(eventBusDispatchEventSpy).toHaveBeenCalled();
    expect(searchFitBounds).toHaveBeenCalled();
  });

  it('does not call searchFitBounds when hasFitBounds is false', async () => {
    const mockBioEntities = [{ id: 1, name: 'BioEntity 1' }];
    dispatch.mockImplementation(() => ({
      unwrap: jest.fn().mockResolvedValue(mockBioEntities),
    }));

    clickHandleReaction(dispatch, false)([], [{ ...newReactionFixture, id: 1 }], 1, modelId);

    expect(searchFitBounds).not.toHaveBeenCalled();
  });
});

/* eslint-disable no-magic-numbers */
import { rightClickHandleAlias } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseRightClick/rightClickHandleAlias';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { addNumbersToEntityNumberData } from '@/redux/entityNumber/entityNumber.slice';
import { setCurrentSelectedBioEntityId } from '@/redux/contextMenu/contextMenu.slice';
import { setModelElementSearch } from '@/redux/modelElements/modelElements.slice';

jest.mock('../../../../../../../services/pluginsManager/map/triggerSearch/searchFitBounds');

describe('rightClickHandleAlias', () => {
  let dispatch: jest.Mock;

  beforeEach(() => {
    jest.clearAllMocks();
    dispatch = jest.fn();
  });

  it('dispatches setBioEntityContents, addNumbersToEntityNumberData and setCurrentSelectedBioEntityId', async () => {
    await rightClickHandleAlias(dispatch)(modelElementFixture.id, modelElementFixture);
    expect(dispatch).toHaveBeenCalledTimes(3);

    expect(dispatch).toHaveBeenCalledWith(setModelElementSearch(expect.any(Object)));
    expect(dispatch).toHaveBeenCalledWith(
      addNumbersToEntityNumberData([modelElementFixture.elementId]),
    );
    expect(dispatch).toHaveBeenCalledWith(setCurrentSelectedBioEntityId(modelElementFixture.id));
  });
});

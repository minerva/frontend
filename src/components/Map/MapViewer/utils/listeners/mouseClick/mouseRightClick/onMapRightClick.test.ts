/* eslint-disable no-magic-numbers */
import { updateLastRightClick } from '@/redux/map/map.slice';
import Map from 'ol/Map';
import { onMapRightClick } from '@/components/Map/MapViewer/utils/listeners/mouseClick/mouseRightClick/onMapRightClick';
import { Layer } from 'ol/layer';
import { openContextMenu } from '@/redux/contextMenu/contextMenu.slice';
import { Source } from 'ol/source';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import { Feature } from 'ol';
import { FEATURE_TYPE } from '@/constants/features';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { LAYER_TYPE } from '@/components/Map/MapViewer/MapViewer.constants';
import * as rightClickHandleAlias from './rightClickHandleAlias';
import * as clickHandleReaction from '../clickHandleReaction';

jest.mock('./rightClickHandleAlias', () => ({
  __esModule: true,
  ...jest.requireActual('./rightClickHandleAlias'),
}));
const rightClickHandleAliasSpy = jest.spyOn(rightClickHandleAlias, 'rightClickHandleAlias');
jest.mock('../clickHandleReaction', () => ({
  __esModule: true,
  ...jest.requireActual('../clickHandleReaction'),
}));
const clickHandleReactionSpy = jest.spyOn(clickHandleReaction, 'clickHandleReaction');

describe('onMapRightClick', () => {
  const modelId = 1;
  let mapInstance: Map;
  const event = { coordinate: [100, 50], pixel: [200, 100] };
  const mapSize = {
    width: 90,
    height: 90,
    tileSize: 256,
    minZoom: 2,
    maxZoom: 9,
  };
  let vectorLayer: VectorLayer;
  let vectorSource: VectorSource;

  beforeEach(() => {
    const dummyElement = document.createElement('div');
    mapInstance = new Map({ target: dummyElement });
    vectorSource = new VectorSource({});
    vectorLayer = new VectorLayer({
      source: vectorSource,
    });
    vectorLayer.set('type', LAYER_TYPE.PROCESS_LAYER);
    jest.clearAllMocks();
  });

  it('calls rightClickHandleAlias if feature type is ALIAS', async () => {
    const dispatch = jest.fn();
    jest.spyOn(mapInstance, 'getAllLayers').mockImplementation((): Layer<Source>[] => {
      return [vectorLayer];
    });
    jest.spyOn(vectorLayer, 'isVisible').mockImplementation((): boolean => {
      return true;
    });
    jest.spyOn(vectorLayer, 'getSource').mockImplementation((): VectorSource => {
      return vectorSource;
    });
    jest.spyOn(vectorSource, 'getClosestFeatureToCoordinate').mockImplementation((): Feature => {
      return new Feature({ id: modelElementFixture.id, type: FEATURE_TYPE.ALIAS });
    });
    await onMapRightClick(
      mapSize,
      modelId,
      dispatch,
      [modelElementFixture],
      [],
    )(event, mapInstance);

    expect(dispatch).toHaveBeenCalledWith(updateLastRightClick(expect.any(Object)));
    expect(dispatch).toHaveBeenCalledWith(openContextMenu(event.pixel));
    expect(rightClickHandleAliasSpy).toHaveBeenCalledWith(dispatch);
  });

  it('calls rightClickHandleAlias if feature type is REACTION', async () => {
    const dispatch = jest.fn();
    jest.spyOn(mapInstance, 'getAllLayers').mockImplementation((): Layer<Source>[] => {
      return [vectorLayer];
    });
    jest.spyOn(vectorLayer, 'isVisible').mockImplementation((): boolean => {
      return true;
    });
    jest.spyOn(vectorLayer, 'getSource').mockImplementation((): VectorSource => {
      return vectorSource;
    });
    jest.spyOn(vectorSource, 'getClosestFeatureToCoordinate').mockImplementation((): Feature => {
      return new Feature({ id: 1, type: FEATURE_TYPE.REACTION });
    });
    await onMapRightClick(
      mapSize,
      modelId,
      dispatch,
      [],
      [{ ...newReactionFixture, id: 1 }],
    )(event, mapInstance);

    expect(dispatch).toHaveBeenCalledWith(updateLastRightClick(expect.any(Object)));
    expect(dispatch).toHaveBeenCalledWith(openContextMenu(event.pixel));
    expect(clickHandleReactionSpy).toHaveBeenCalledWith(dispatch);
  });
});

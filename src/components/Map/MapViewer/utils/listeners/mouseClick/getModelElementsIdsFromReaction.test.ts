/* eslint-disable no-magic-numbers */
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import getModelElementsIdsFromReaction from './getModelElementsIdsFromReaction';

describe('getModelElementsIdsFromReaction', () => {
  it('should return correct model elements ids from given reaction', () => {
    const result = getModelElementsIdsFromReaction(newReactionFixture);
    expect(result).toEqual([
      ...newReactionFixture.products.map(product => product.element),
      ...newReactionFixture.reactants.map(reactant => reactant.element),
      ...newReactionFixture.modifiers.map(modifier => modifier.element),
    ]);
  });

  it('should return empty array', () => {
    const result = getModelElementsIdsFromReaction({
      ...newReactionFixture,
      products: [],
      reactants: [],
      modifiers: [],
    });
    expect(result).toEqual([]);
  });
});

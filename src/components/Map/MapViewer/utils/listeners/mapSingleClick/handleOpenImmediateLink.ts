import { showToast } from '@/utils/showToast';
import { ModelElement } from '@/types/models';

export const handleOpenImmediateLink = (bioEntity: ModelElement): void => {
  const link = bioEntity.immediateLink;
  if (link !== null) {
    const tab = window.open(link, '_blank');
    if (tab) {
      tab.focus();
    } else {
      showToast({
        type: 'error',
        message: `Browser prevented minerva from opening link: <a href="${link}" target="_blank">${link}</a>`,
      });
    }
  }
};

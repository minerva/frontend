import { renderHook, waitFor } from '@testing-library/react';
import { Map } from 'ol';
import React from 'react';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { useOlMap } from './useOlMap';

const useRefValue = {
  current: null,
};

Object.defineProperty(useRefValue, 'current', {
  get: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
  set: jest.fn(() => ({
    innerHTML: '',
    appendChild: jest.fn(),
    addEventListener: jest.fn(),
    getRootNode: jest.fn(),
  })),
});

jest.spyOn(React, 'useRef').mockReturnValue(useRefValue);

jest.mock('./config/additionalLayers/useOlMapAdditionalLayers', () => ({
  useOlMapAdditionalLayers: jest.fn(() => []),
}));

describe('useOlMap - util', () => {
  const { Wrapper } = getReduxWrapperWithStore({
    map: initialMapStateFixture,
  });

  describe('when initializing', () => {
    it('should set map instance', async () => {
      const dummyElement = document.createElement('div');
      const { result } = renderHook(() => useOlMap({ target: dummyElement }), { wrapper: Wrapper });
      await waitFor(() => expect(result.current.mapInstance).toBeInstanceOf(Map));
    });

    it('should render content inside the target element', async () => {
      const FIRST_NODE = 0;
      const dummyElement = document.createElement('div');
      renderHook(() => useOlMap({ target: dummyElement }), { wrapper: Wrapper });

      expect(dummyElement.childNodes[FIRST_NODE]).toHaveClass('ol-viewport');
    });
  });
});

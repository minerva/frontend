import View from 'ol/View';
import BaseLayer from 'ol/layer/Base';
import { OverlayBioEntityRender } from '@/types/OLrendering';

export type MapConfig = {
  view: View;
  layers: BaseLayer[];
};

export type VerticalAlign = 'TOP' | 'MIDDLE' | 'BOTTOM';
export type HorizontalAlign = 'LEFT' | 'RIGHT' | 'CENTER';

export type OverlayBioEntityGroupedElementsType = {
  [id: string]: Array<OverlayBioEntityRender & { amount: number }>;
};

export type BoundingBox = {
  x: number;
  y: number;
  width: number;
  height: number;
};

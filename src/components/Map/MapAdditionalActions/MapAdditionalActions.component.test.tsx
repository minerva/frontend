/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { MAP_DATA_INITIAL_STATE } from '@/redux/map/map.constants';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState, StoreType } from '@/redux/store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import Map from 'ol/Map';
import { MockStoreEnhanced } from 'redux-mock-store';
import { MapAdditionalActions } from './MapAdditionalActions.component';
import { useVisibleBioEntitiesPolygonCoordinates } from './utils/useVisibleBioEntitiesPolygonCoordinates';

const setBounds = jest.fn();

jest.mock('../../../utils/map/useSetBounds', () => ({
  _esModule: true,
  useSetBounds: (): jest.Mock => setBounds,
}));

jest.mock('./utils/useVisibleBioEntitiesPolygonCoordinates', () => ({
  _esModule: true,
  useVisibleBioEntitiesPolygonCoordinates: jest.fn(),
}));

const useVisibleBioEntitiesPolygonCoordinatesMock =
  useVisibleBioEntitiesPolygonCoordinates as jest.Mock;

setBounds.mockImplementation(() => {});

const renderComponent = (
  initialStore?: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <MapAdditionalActions />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithMapInstance = (initialStore?: InitialStoreState): { store: StoreType } => {
  const dummyElement = document.createElement('div');
  const mapInstance = new Map({ target: dummyElement });

  const { Wrapper, store } = getReduxWrapperWithStore(initialStore, {
    mapInstanceContextValue: {
      mapInstance,
      handleSetMapInstance: () => {},
    },
  });

  return (
    render(
      <Wrapper>
        <MapAdditionalActions />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('MapAdditionalActions - component', () => {
  describe('when always', () => {
    beforeEach(() => {
      renderComponent(INITIAL_STORE_STATE_MOCK);
    });

    it('should render zoom in button', () => {
      const button = screen.getByTestId('zoom-in-button');
      expect(button).toBeInTheDocument();
    });

    it('should render zoom out button', () => {
      const button = screen.getByTestId('zoom-out-button');
      expect(button).toBeInTheDocument();
    });

    it('should render location button', () => {
      const button = screen.getByTestId('location-button');
      expect(button).toBeInTheDocument();
    });
  });

  describe('when clicked on zoom in button', () => {
    it('should dispatch varyPositionZoom action with valid delta', () => {
      const { store } = renderComponent(INITIAL_STORE_STATE_MOCK);
      const button = screen.getByTestId('zoom-in-button');
      button!.click();

      const actions = store.getActions();
      expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
        payload: { delta: 1 },
        type: 'map/varyPositionZoom',
      });
    });
  });

  describe('when clicked on zoom in button', () => {
    it('should dispatch varyPositionZoom action with valid delta', () => {
      const { store } = renderComponent(INITIAL_STORE_STATE_MOCK);
      const button = screen.getByTestId('zoom-out-button');
      button!.click();

      const actions = store.getActions();
      expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
        payload: { delta: -1 },
        type: 'map/varyPositionZoom',
      });
    });
  });

  describe('when clicked on location button', () => {
    it('setBounds should be called', () => {
      useVisibleBioEntitiesPolygonCoordinatesMock.mockImplementation(() => [
        [128, 128],
        [192, 192],
      ]);

      renderComponentWithMapInstance({
        map: {
          data: {
            ...MAP_DATA_INITIAL_STATE,
            size: {
              width: 256,
              height: 256,
              tileSize: 256,
              minZoom: 1,
              maxZoom: 1,
            },
          },
          loading: 'idle',
          error: {
            name: '',
            message: '',
          },
          openedMaps: [],
        },
      });

      const button = screen.getByTestId('location-button');
      act(() => {
        button!.click();
      });

      expect(setBounds).toHaveBeenCalled();
    });
  });
});

/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { MAP_DATA_INITIAL_STATE } from '@/redux/map/map.constants';
import { initialMapDataFixture } from '@/redux/map/map.fixtures';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import Map from 'ol/Map';
import { useAddtionalActions } from './useAdditionalActions';
import { useVisibleBioEntitiesPolygonCoordinates } from './useVisibleBioEntitiesPolygonCoordinates';

jest.mock('./useVisibleBioEntitiesPolygonCoordinates', () => ({
  _esModule: true,
  useVisibleBioEntitiesPolygonCoordinates: jest.fn(),
}));

const useVisibleBioEntitiesPolygonCoordinatesMock =
  useVisibleBioEntitiesPolygonCoordinates as jest.Mock;

describe('useAddtionalActions - hook', () => {
  describe('on zoomIn', () => {
    it('should dispatch varyPositionZoom action with valid delta', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener(INITIAL_STORE_STATE_MOCK);
      const {
        result: {
          current: { zoomIn },
        },
      } = renderHook(() => useAddtionalActions(), {
        wrapper: Wrapper,
      });

      zoomIn();

      const actions = store.getActions();
      expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
        payload: { delta: 1 },
        type: 'map/varyPositionZoom',
      });
    });
  });

  describe('on zoomOut', () => {
    it('should dispatch varyPositionZoom action with valid delta', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener(INITIAL_STORE_STATE_MOCK);
      const {
        result: {
          current: { zoomOut },
        },
      } = renderHook(() => useAddtionalActions(), {
        wrapper: Wrapper,
      });

      zoomOut();

      const actions = store.getActions();
      expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
        payload: { delta: -1 },
        type: 'map/varyPositionZoom',
      });
    });
  });

  describe('on zoomInToBioEntities', () => {
    describe('when there are valid polygon coordinates', () => {
      beforeEach(() => {
        useVisibleBioEntitiesPolygonCoordinatesMock.mockImplementation(() => [
          [128, 128],
          [192, 192],
        ]);
      });

      it('should return valid results', () => {
        const dummyElement = document.createElement('div');
        const mapInstance = new Map({ target: dummyElement });

        const { Wrapper } = getReduxWrapperWithStore(
          {
            map: {
              data: {
                ...MAP_DATA_INITIAL_STATE,
                size: {
                  width: 256,
                  height: 256,
                  tileSize: 256,
                  minZoom: 1,
                  maxZoom: 1,
                },
              },
              loading: 'idle',
              error: {
                name: '',
                message: '',
              },
              openedMaps: [],
            },
          },
          {
            mapInstanceContextValue: {
              mapInstance,
              handleSetMapInstance: () => {},
            },
          },
        );
        const {
          result: {
            current: { zoomInToBioEntities },
          },
        } = renderHook(() => useAddtionalActions(), {
          wrapper: Wrapper,
        });

        expect(zoomInToBioEntities()).toStrictEqual({
          extent: [128, 128, 192, 192],
          options: { maxZoom: 1, padding: [128, 128, 128, 128], size: undefined },
          // size is real size on the screen, so it'll be undefined in the jest
        });
      });
    });

    describe('when there are no polygon coordinates', () => {
      beforeEach(() => {
        useVisibleBioEntitiesPolygonCoordinatesMock.mockImplementation(() => undefined);
      });

      it('should return undefined and center map by calculating coordinates when default center coordinates do not exist', () => {
        const MAP_CONFIG = {
          size: {
            width: 3500,
            height: 2000,
          },
          zoom: {
            minZoom: 2,
            maxZoom: 9,
          },
          position: {
            x: 1300,
            y: 1900,
            z: 7,
          },
        };
        const { Wrapper, store } = getReduxStoreWithActionsListener({
          ...INITIAL_STORE_STATE_MOCK,
          models: {
            ...INITIAL_STORE_STATE_MOCK.models,
            data: [
              {
                ...modelsFixture[0],
                ...MAP_CONFIG.size,
                ...MAP_CONFIG.zoom,
                defaultCenterX: null,
                defaultCenterY: null,
                defaultZoomLevel: null,
              },
            ],
          },
          map: {
            ...INITIAL_STORE_STATE_MOCK.map,
            data: {
              ...initialMapDataFixture,
              position: {
                last: MAP_CONFIG.position,
                initial: MAP_CONFIG.position,
              },
              size: {
                ...MAP_CONFIG.size,
                ...MAP_CONFIG.zoom,
                tileSize: 256,
              },
              modelId: modelsFixture[0].id,
            },
          },
        });
        const {
          result: {
            current: { zoomInToBioEntities },
          },
        } = renderHook(() => useAddtionalActions(), {
          wrapper: Wrapper,
        });
        const result = zoomInToBioEntities();
        expect(result).toBeUndefined();
        const actions = store.getActions();
        const position = store.getState().map?.data.position;
        expect(position?.last).toEqual(MAP_CONFIG.position);
        expect(actions[0]).toEqual({
          payload: { x: 1750, y: 1000, z: 4 },
          type: 'map/setMapPosition',
        });
      });

      it('should return undefined and center map using default center coordinates if exist', () => {
        const MAP_CONFIG = {
          size: {
            width: 5000,
            height: 10000,
          },
          zoom: {
            minZoom: 2,
            maxZoom: 9,
          },
          position: {
            x: 1300,
            y: 1900,
            z: 7,
          },
        };
        const { Wrapper, store } = getReduxStoreWithActionsListener({
          ...INITIAL_STORE_STATE_MOCK,
          models: {
            ...INITIAL_STORE_STATE_MOCK.models,
            data: [
              {
                ...modelsFixture[0],
                ...MAP_CONFIG.size,
                ...MAP_CONFIG.zoom,
                defaultCenterX: 2500,
                defaultCenterY: 5000,
                defaultZoomLevel: 3,
              },
            ],
          },
          map: {
            ...INITIAL_STORE_STATE_MOCK.map,
            data: {
              ...initialMapDataFixture,
              position: {
                last: MAP_CONFIG.position,
                initial: MAP_CONFIG.position,
              },
              size: {
                ...MAP_CONFIG.size,
                ...MAP_CONFIG.zoom,
                tileSize: 256,
              },
              modelId: modelsFixture[0].id,
            },
          },
        });
        const {
          result: {
            current: { zoomInToBioEntities },
          },
        } = renderHook(() => useAddtionalActions(), {
          wrapper: Wrapper,
        });
        const result = zoomInToBioEntities();
        expect(result).toBeUndefined();
        const actions = store.getActions();
        const position = store.getState().map?.data.position;
        expect(position?.last).toEqual(MAP_CONFIG.position);
        expect(actions[0]).toEqual({
          payload: { x: 2500, y: 5000, z: 3 },
          type: 'map/setMapPosition',
        });
      });
    });
  });
});

import { drugsFixture } from '@/models/fixtures/drugFixtures';
/* eslint-disable no-magic-numbers */
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { BIOENTITY_INITIAL_STATE_MOCK } from '@/redux/bioEntity/bioEntity.mock';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState } from '@/redux/store';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import { CHEMICALS_INITIAL_STATE_MOCK } from '../../../../redux/chemicals/chemicals.mock';
import { DRUGS_INITIAL_STATE_MOCK } from '../../../../redux/drugs/drugs.mock';
import { DEFAULT_POSITION, MAIN_MAP, MAP_INITIAL_STATE } from '../../../../redux/map/map.constants';
import { MODELS_INITIAL_STATE_MOCK } from '../../../../redux/models/models.mock';
import { useVisibleBioEntitiesPolygonCoordinates } from './useVisibleBioEntitiesPolygonCoordinates';

/* key elements of the state:
  - this state simulates situation where there is:
  -- one searched element
  -- of currently selected map
  -- for each content/chemicals/drugs data set

  - the key differences in this states are x/y/z coordinates of element's bioEntities
*/

const getInitalState = (
  { hideElements }: { hideElements: boolean } = { hideElements: false },
): RootState => {
  const elementsLimit = hideElements ? 0 : 1;

  return {
    ...INITIAL_STORE_STATE_MOCK,
    drawer: {
      ...DRAWER_INITIAL_STATE,
      searchDrawerState: {
        ...DRAWER_INITIAL_STATE.searchDrawerState,
        selectedSearchElement: 'search',
      },
    },
    models: {
      ...MODELS_INITIAL_STATE_MOCK,
      data: [
        {
          ...modelsFixture[0],
          id: HISTAMINE_MAP_ID,
        },
      ],
    },
    map: {
      ...MAP_INITIAL_STATE,
      data: {
        ...MAP_INITIAL_STATE.data,
        modelId: HISTAMINE_MAP_ID,
        size: {
          width: 256,
          height: 256,
          tileSize: 256,
          minZoom: 1,
          maxZoom: 1,
        },
      },
      openedMaps: [
        { modelId: HISTAMINE_MAP_ID, modelName: MAIN_MAP, lastPosition: DEFAULT_POSITION },
      ],
    },
    modelElements: {
      data: {
        0: {
          data: [modelElementFixture],
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      },
      search: {
        data: [
          {
            searchQueryElement: 'search',
            loading: 'pending',
            error: DEFAULT_ERROR,
            data: [
              {
                modelElement: {
                  ...modelElementFixture,
                  model: HISTAMINE_MAP_ID,
                  x: 16,
                  y: 16,
                  z: 1,
                },
                perfect: true,
              },
            ],
          },
        ].slice(0, elementsLimit),
        loading: 'pending',
        error: DEFAULT_ERROR,
      },
    } as ModelElementsState,
    bioEntity: BIOENTITY_INITIAL_STATE_MOCK,
    chemicals: {
      ...CHEMICALS_INITIAL_STATE_MOCK,
      data: [
        {
          searchQueryElement: 'search',
          data: [
            {
              ...chemicalsFixture[0],
              targets: [
                {
                  ...chemicalsFixture[0].targets[0],
                  targetElements: [
                    {
                      ...chemicalsFixture[0].targets[0].targetElements[0],
                      model: HISTAMINE_MAP_ID,
                      x: 32,
                      y: 32,
                      z: 1,
                    },
                  ],
                },
              ],
            },
          ].slice(0, elementsLimit),
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
        {
          searchQueryElement: 'not-search',
          data: [
            {
              ...chemicalsFixture[0],
              targets: [
                {
                  ...chemicalsFixture[0].targets[0],
                  targetElements: [
                    {
                      ...chemicalsFixture[0].targets[0].targetElements[0],
                      model: HISTAMINE_MAP_ID,
                      x: 8,
                      y: 2,
                      z: 9,
                    },
                  ],
                },
              ],
            },
          ].slice(0, elementsLimit),
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      ],
    },
    drugs: {
      ...DRUGS_INITIAL_STATE_MOCK,
      data: [
        {
          searchQueryElement: 'search',
          data: [
            {
              ...drugsFixture[0],
              targets: [
                {
                  ...drugsFixture[0].targets[0],
                  targetElements: [
                    {
                      ...drugsFixture[0].targets[0].targetElements[0],
                      model: HISTAMINE_MAP_ID,
                      x: 128,
                      y: 128,
                      z: 1,
                    },
                  ],
                },
              ],
            },
          ].slice(0, elementsLimit),
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
        {
          searchQueryElement: 'not-search',
          data: [
            {
              ...drugsFixture[0],
              targets: [
                {
                  ...drugsFixture[0].targets[0],
                  targetElements: [
                    {
                      ...drugsFixture[0].targets[0].targetElements[0],
                      model: HISTAMINE_MAP_ID,
                      x: 100,
                      y: 50,
                      z: 4,
                    },
                  ],
                },
              ],
            },
          ].slice(0, elementsLimit),
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      ],
    },
  };
};

describe('useVisibleBioEntitiesPolygonCoordinates - hook', () => {
  describe('when allVisibleBioEntities is empty', () => {
    const { Wrapper } = getReduxWrapperWithStore(getInitalState({ hideElements: true }));
    const { result } = renderHook(() => useVisibleBioEntitiesPolygonCoordinates(), {
      wrapper: Wrapper,
    });

    it('should return undefined', () => {
      expect(result.current).toBe(undefined);
    });
  });

  describe('when allVisibleBioEntities has data', () => {
    const { Wrapper } = getReduxWrapperWithStore(getInitalState());
    const { result } = renderHook(() => useVisibleBioEntitiesPolygonCoordinates(), {
      wrapper: Wrapper,
    });

    it('should return undefined', () => {
      expect(result.current).toStrictEqual([
        [-17532820, -0],
        [0, 17532820],
      ]);
    });
  });
});

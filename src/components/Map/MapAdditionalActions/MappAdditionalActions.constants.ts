export const MAP_ZOOM_IN_DELTA = 1;

export const MAP_ZOOM_OUT_DELTA = -1;

export const LOCATION_BTN_ID = 'location-button';

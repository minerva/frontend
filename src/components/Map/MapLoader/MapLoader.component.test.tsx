import React from 'react';
import { render, screen } from '@testing-library/react';
import {
  getReduxWrapperWithStore,
  InitialStoreState,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { newReactionsLoadingSelector } from '@/redux/newReactions/newReactions.selectors';
import { modelElementsCurrentModelLoadingSelector } from '@/redux/modelElements/modelElements.selector';
import { isDrawerOpenSelector } from '@/redux/drawer/drawer.selectors';
import {
  arrowTypesLoadingSelector,
  bioShapesLoadingSelector,
  lineTypesLoadingSelector,
} from '@/redux/shapes/shapes.selectors';
import { layersLoadingSelector } from '@/redux/layers/layers.selectors';
import { modelLoadingSelector } from '@/redux/selectors';
import { MapLoader } from './MapLoader.component';

jest.mock('../../../redux/hooks/useAppSelector', () => ({
  useAppSelector: jest.fn(),
}));
type SelectorFunction = (state: never) => string | boolean;

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);

  return (
    render(
      <Wrapper>
        <MapLoader />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('MapLoader', () => {
  const mockUseAppSelector = useAppSelector as jest.Mock;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should not render the LoadingIndicator when no data is loading', () => {
    mockUseAppSelector.mockImplementation(selector => {
      const selectorMap = new Map<SelectorFunction, string | boolean>([
        [newReactionsLoadingSelector, 'succeeded'],
        [modelElementsCurrentModelLoadingSelector, 'succeeded'],
        [bioShapesLoadingSelector, 'succeeded'],
        [lineTypesLoadingSelector, 'succeeded'],
        [arrowTypesLoadingSelector, 'succeeded'],
        [layersLoadingSelector, 'succeeded'],
        [isDrawerOpenSelector, false],
      ]);

      return selectorMap.get(selector) ?? false;
    });
    renderComponent();

    expect(screen.queryByTestId('loading-indicator')).not.toBeInTheDocument();
  });

  it('should render the LoadingIndicator when data is loading', () => {
    mockUseAppSelector.mockImplementation(selector => {
      const selectorMap = new Map<SelectorFunction, boolean>([
        [modelLoadingSelector, true],
        [isDrawerOpenSelector, false],
      ]);

      return selectorMap.get(selector) ?? false;
    });
    renderComponent();

    expect(screen.queryByTestId('loading-indicator')).toBeInTheDocument();
  });
});

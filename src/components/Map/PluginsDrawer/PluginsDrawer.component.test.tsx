import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';

import { PLUGINS_INITIAL_STATE_MOCK } from '@/redux/plugins/plugins.mock';
import { render, screen } from '@testing-library/react';
import { PluginsDrawer } from './PluginsDrawer.component';
import { PLUGINS_DRAWER_ROLE } from './PluginsDrawer.constants';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <PluginsDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PluginsDrawer - component', () => {
  describe('when drawer is open', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            isOpen: false,
          },
        },
      });
    });

    it('should render component without show class', () => {
      const drawer = screen.getByRole(PLUGINS_DRAWER_ROLE);

      expect(drawer).not.toHaveClass('translate-x-0');
    });
  });

  describe('when drawer is NOT open', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            isOpen: true,
          },
        },
      });
    });

    it('should render component without show class', () => {
      const drawer = screen.getByRole(PLUGINS_DRAWER_ROLE);
      expect(drawer).toHaveClass('translate-x-0');
    });
  });

  describe('when always', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
        },
      });
    });

    it('should render plugins tab', () => {
      const element = screen.getByTestId('drawer-plugins-tab');
      expect(element).toBeInTheDocument();
    });

    it('should render plugins tab', () => {
      const element = screen.getByTestId('drawer-plugins-header');
      expect(element).toBeInTheDocument();
    });

    it('should render plugins content', () => {
      const element = screen.getByTestId('drawer-plugins-content');
      expect(element).toBeInTheDocument();
    });
  });
});

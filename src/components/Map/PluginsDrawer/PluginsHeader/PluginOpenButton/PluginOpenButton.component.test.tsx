import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import { PLUGINS_INITIAL_STATE_MOCK } from '@/redux/plugins/plugins.mock';
import { AppDispatch, RootState } from '@/redux/store';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { PluginOpenButton } from './PluginOpenButton.component';

const renderComponent = (
  initialStore: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <PluginOpenButton />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const PLUGIN = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

const STATE = {
  plugins: {
    ...PLUGINS_INITIAL_STATE_MOCK,
    drawer: {
      ...PLUGINS_INITIAL_STATE_MOCK.drawer,
      currentPluginHash: PLUGIN.hash,
    },
    activePlugins: {
      data: {
        [PLUGIN.hash]: PLUGIN,
      },
      pluginsId: [PLUGIN.hash],
    },
  },
};

describe('PluginOpenButton - component', () => {
  describe('when public plugins list is empty', () => {});

  describe('when public plugins list is present', () => {
    const publicPlugins = PLUGINS_MOCK.filter(p => p.isPublic);

    beforeEach(() => {
      renderComponent({
        ...STATE,
        plugins: {
          ...STATE.plugins,
          list: {
            ...PLUGINS_INITIAL_STATE_MOCK.list,
            data: PLUGINS_MOCK,
          },
        },
        legend: LEGEND_INITIAL_STATE_MOCK,
      });
    });

    it.each(publicPlugins)('should render load plugin button for public plugins', plugin => {
      const element = screen.getByText(plugin.name, { exact: false });
      expect(element).toBeInTheDocument();
    });

    it('should render open new plugin button', () => {
      const element = screen.getByText('Open new plugin', { exact: false });
      expect(element).toBeInTheDocument();
    });
  });
});

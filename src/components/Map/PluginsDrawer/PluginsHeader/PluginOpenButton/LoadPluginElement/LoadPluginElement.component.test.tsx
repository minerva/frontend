import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import {
  PLUGINS_INITIAL_STATE_LIST_MOCK,
  PLUGINS_INITIAL_STATE_MOCK,
} from '@/redux/plugins/plugins.mock';
import { AppDispatch, RootState } from '@/redux/store';
import { PluginsManager } from '@/services/pluginsManager';
import { MinervaPlugin } from '@/types/models';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen, waitFor } from '@testing-library/react';
import axios, { HttpStatusCode } from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { MockStoreEnhanced } from 'redux-mock-store';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { LoadPluginElement } from './LoadPluginElement.component';

const mockedAxiosClient = new MockAdapter(axios);

const renderComponent = (
  initialStore: InitialStoreState,
  plugin: MinervaPlugin,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <LoadPluginElement plugin={plugin} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const setHashedPluginSpy = jest.spyOn(PluginsManager, 'setHashedPlugin');

const PLUGIN = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

const STATE = {
  plugins: {
    ...PLUGINS_INITIAL_STATE_MOCK,
    drawer: {
      ...PLUGINS_INITIAL_STATE_MOCK.drawer,
      currentPluginHash: PLUGIN.hash,
    },
    activePlugins: {
      data: {
        [PLUGIN.hash]: PLUGIN,
      },
      pluginsId: [PLUGIN.hash],
    },
    list: PLUGINS_INITIAL_STATE_LIST_MOCK,
  },
  legend: LEGEND_INITIAL_STATE_MOCK,
};

describe('LoadPluginButton - component', () => {
  beforeEach(() => {
    renderComponent(STATE, PLUGIN);
  });

  it('renders plugin button element', () => {
    const element = screen.getByText(PLUGIN.name);
    expect(element).toBeInTheDocument();
  });

  it('loads plugin on button click', async () => {
    mockedAxiosClient.onGet(PLUGIN.urls[FIRST_ARRAY_ELEMENT]).reply(HttpStatusCode.Ok, '');
    const button = screen.getByText(PLUGIN.name);
    button.click();

    await waitFor(() => {
      expect(setHashedPluginSpy).toHaveBeenCalledWith({
        pluginScript: '',
        pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/disease-associations/plugin.js',
      });
    });
  });
});

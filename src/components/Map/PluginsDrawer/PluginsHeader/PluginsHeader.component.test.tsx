import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import {
  PLUGINS_INITIAL_STATE_LIST_MOCK,
  PLUGINS_INITIAL_STATE_MOCK,
} from '@/redux/plugins/plugins.mock';
import { AppDispatch, RootState } from '@/redux/store';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { CLOSE_PLUGINS_DRAWER_BUTTON_ROLE } from '../PluginsDrawer.constants';
import { PluginsHeader } from './PluginsHeader.component';

const renderComponent = (
  initialStore?: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <PluginsHeader />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PluginsHeader - component', () => {
  describe('when currentPlugin is NOT present', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            currentPluginHash: undefined,
          },
        },
        legend: LEGEND_INITIAL_STATE_MOCK,
      });
    });

    it('should render no plugin selected info', () => {
      const element = screen.getByText('No plugin selected');
      expect(element).toBeInTheDocument();
    });
  });

  describe('when currentPlugin is present', () => {
    const plugin = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            currentPluginHash: plugin.hash,
          },
          activePlugins: {
            data: {
              [plugin.hash]: plugin,
            },
            pluginsId: [plugin.hash],
          },
          list: PLUGINS_INITIAL_STATE_LIST_MOCK,
        },
        legend: LEGEND_INITIAL_STATE_MOCK,
      });
    });

    it('should render header info', () => {
      const title = screen.getByText(`Plugin:`, { exact: false });
      const pluginName = screen.getByText(plugin.name, { exact: false });

      expect(title).toBeInTheDocument();
      expect(pluginName).toBeInTheDocument();
    });
  });

  describe('when always', () => {
    it('should render close plugins drawer button', () => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
        },
      });

      const element = screen.getByRole(CLOSE_PLUGINS_DRAWER_BUTTON_ROLE);
      expect(element).toBeInTheDocument();
    });

    it('should render plugin open button', () => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
        },
      });

      const element = screen.getByTestId('open-new-plugin-list');
      expect(element).toBeInTheDocument();
    });

    it('should dispatch close plugins drawer on button click', () => {
      const { store } = renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
        },
      });

      const element = screen.getByRole(CLOSE_PLUGINS_DRAWER_BUTTON_ROLE);
      element.click();

      const actions = store.getActions();
      expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
        payload: undefined,
        type: 'plugins/closePluginsDrawer',
      });
    });
  });
});

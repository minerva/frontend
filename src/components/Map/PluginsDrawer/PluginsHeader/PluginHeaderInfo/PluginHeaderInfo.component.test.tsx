import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import {
  PLUGINS_INITIAL_STATE_LIST_MOCK,
  PLUGINS_INITIAL_STATE_MOCK,
} from '@/redux/plugins/plugins.mock';
import { AppDispatch, RootState } from '@/redux/store';
import { PluginsManager } from '@/services/pluginsManager';
import { MinervaPlugin } from '@/types/models';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen, waitFor } from '@testing-library/react';
import axios, { HttpStatusCode } from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { MockStoreEnhanced } from 'redux-mock-store';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { PluginHeaderInfo } from './PluginHeaderInfo.component';
import { RELOAD_PLUGIN_DRAWER_BUTTON_ROLE } from './PluginHeaderInfo.constants';

const mockedAxiosClient = new MockAdapter(axios);

const renderComponent = (
  initialStore: InitialStoreState,
  plugin: MinervaPlugin,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <PluginHeaderInfo plugin={plugin} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const removePluginContentSpy = jest.spyOn(PluginsManager, 'removePluginContent');
const setHashedPluginSpy = jest.spyOn(PluginsManager, 'setHashedPlugin');

const PLUGIN = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

const STATE = {
  plugins: {
    ...PLUGINS_INITIAL_STATE_MOCK,
    drawer: {
      ...PLUGINS_INITIAL_STATE_MOCK.drawer,
      currentPluginHash: PLUGIN.hash,
    },
    activePlugins: {
      data: {
        [PLUGIN.hash]: PLUGIN,
      },
      pluginsId: [PLUGIN.hash],
    },
    list: PLUGINS_INITIAL_STATE_LIST_MOCK,
  },
  legend: LEGEND_INITIAL_STATE_MOCK,
};

describe('PluginHeaderInfo - component', () => {
  it('renders plugin title and name', () => {
    renderComponent(STATE, PLUGIN);

    const title = screen.getByText(`Plugin:`, { exact: false });
    const pluginName = screen.getByText(PLUGIN.name, { exact: false });

    expect(title).toBeInTheDocument();
    expect(pluginName).toBeInTheDocument();
  });

  it('renders plugin reload button', () => {
    renderComponent(STATE, PLUGIN);

    const button = screen.getByRole(RELOAD_PLUGIN_DRAWER_BUTTON_ROLE);
    expect(button).toBeInTheDocument();
  });

  it('reload plugin on reload button', async () => {
    renderComponent(STATE, PLUGIN);
    mockedAxiosClient.onGet(PLUGIN.urls[FIRST_ARRAY_ELEMENT]).reply(HttpStatusCode.Ok, '');

    const button = screen.getByRole(RELOAD_PLUGIN_DRAWER_BUTTON_ROLE);
    button.click();

    expect(removePluginContentSpy).toHaveBeenCalledWith({
      hash: PLUGIN.hash,
    });

    await waitFor(() => {
      expect(setHashedPluginSpy).toHaveBeenCalledWith({
        pluginScript: '',
        pluginUrl: 'https://minerva-service.lcsb.uni.lu/plugins/disease-associations/plugin.js',
      });
    });
  });
});

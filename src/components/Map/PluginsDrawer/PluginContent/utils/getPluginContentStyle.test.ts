import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import {
  HIDE_ALL_ELEMENTS_STYLE,
  SHOW_SELECTED_PLUGIN_ELEMENT_STYLE,
} from '../PluginContent.constants';
import { getPluginContentStyle } from './getPluginContentStyle';

describe('getPluginContentStyle - util', () => {
  describe('when selectedPlugin is NOT present', () => {
    it('should return valid value', () => {
      expect(getPluginContentStyle(undefined)).toBe(HIDE_ALL_ELEMENTS_STYLE);
    });
  });

  describe('when selectedPlugin is present', () => {
    const selectedPlugin = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];

    it('should return valid value', () => {
      const result = getPluginContentStyle(selectedPlugin);

      expect(result.includes(HIDE_ALL_ELEMENTS_STYLE)).toBeTruthy();
      expect(result.includes(SHOW_SELECTED_PLUGIN_ELEMENT_STYLE(selectedPlugin.hash))).toBeTruthy();
    });
  });
});

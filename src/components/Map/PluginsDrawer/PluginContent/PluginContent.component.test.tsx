import { PLUGINS_CONTENT_ELEMENT_ID } from '@/constants/plugins';
import { PLUGINS_INITIAL_STATE_MOCK } from '@/redux/plugins/plugins.mock';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { PluginContent } from './PluginContent.component';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <PluginContent />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PluginContent - component', () => {
  describe('when always', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
        },
      });
    });

    it('should render plugins content', () => {
      const element = screen.getByTestId('drawer-plugins-content');
      expect(element).toBeInTheDocument();
      expect(element.id).toBe(PLUGINS_CONTENT_ELEMENT_ID);
    });
  });

  /*
    NOTE:
    Testing of <style jsx global> is not possible in Jest
  */
});

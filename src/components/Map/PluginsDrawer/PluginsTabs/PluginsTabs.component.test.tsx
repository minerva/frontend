import { FIRST_ARRAY_ELEMENT, SECOND_ARRAY_ELEMENT } from '@/constants/common';
import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import {
  PLUGINS_INITIAL_STATE_LIST_MOCK,
  PLUGINS_INITIAL_STATE_MOCK,
} from '@/redux/plugins/plugins.mock';
import { AppDispatch, RootState } from '@/redux/store';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { PluginsTabs } from './PluginsTabs.component';

const renderComponent = (
  initialStore?: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <PluginsTabs />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const PLUGIN = PLUGINS_MOCK[FIRST_ARRAY_ELEMENT];
const PLUGIN_2 = PLUGINS_MOCK[SECOND_ARRAY_ELEMENT];

describe('PluginsTabs - component', () => {
  describe('when active plugins list is empty', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            currentPluginHash: PLUGIN.hash,
          },
          activePlugins: {
            data: {},
            pluginsId: [],
          },
          list: PLUGINS_INITIAL_STATE_LIST_MOCK,
        },
      });
    });

    it('should render empty plugin info', () => {
      const element = screen.getByText(`You don't have any opened plugin yet`);
      expect(element).toBeInTheDocument();
    });
  });

  describe('when active plugins list is present', () => {
    beforeEach(() => {
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          drawer: {
            ...PLUGINS_INITIAL_STATE_MOCK.drawer,
            currentPluginHash: PLUGIN.hash,
          },
          activePlugins: {
            data: {
              [PLUGIN.hash]: PLUGIN,
              [PLUGIN_2.hash]: PLUGIN_2,
            },
            pluginsId: [PLUGIN.hash, PLUGIN_2.hash],
          },
          list: PLUGINS_INITIAL_STATE_LIST_MOCK,
        },
        legend: LEGEND_INITIAL_STATE_MOCK,
      });
    });

    it.each([PLUGIN, PLUGIN_2])('should render plugins tabs', plugin => {
      const element = screen.getByText(plugin.name);
      expect(element).toBeInTheDocument();
    });
  });
});

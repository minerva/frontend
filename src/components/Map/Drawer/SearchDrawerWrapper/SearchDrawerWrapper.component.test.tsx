import { SearchDrawerWrapper } from '@/components/Map/Drawer/SearchDrawerWrapper';
import { drawerSearchStepOneFixture } from '@/redux/drawer/drawerFixture';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <SearchDrawerWrapper />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('SearchDrawerWrapper - component', () => {
  it('should display the first step for search', () => {
    renderComponent({
      drawer: drawerSearchStepOneFixture,
    });

    expect(screen.getByTestId('grouped-search-results')).toBeInTheDocument();
  });

  it('should display the second step for value type bioEntity', () => {
    renderComponent({
      drawer: {
        isOpen: true,
        drawerName: 'search',
        searchDrawerState: {
          currentStep: 2,
          stepType: 'modelElement',
          selectedValue: undefined,
          listOfBioEnitites: [],
          selectedSearchElement: '',
        },
        reactionDrawerState: {},
        bioEntityDrawerState: {
          drugs: {},
          chemicals: {},
        },
        overlayDrawerState: {
          currentStep: 0,
        },
        commentDrawerState: {
          commentId: undefined,
        },
      },
    });

    expect(screen.getByTestId('search-second-step')).toBeInTheDocument();
  });

  it('should display the second step for value type drugs', () => {
    renderComponent({
      drawer: {
        isOpen: true,
        drawerName: 'search',
        searchDrawerState: {
          currentStep: 2,
          stepType: 'drugs',
          selectedValue: undefined,
          listOfBioEnitites: [],
          selectedSearchElement: '',
        },
        reactionDrawerState: {},
        bioEntityDrawerState: {
          drugs: {},
          chemicals: {},
        },
        overlayDrawerState: {
          currentStep: 0,
        },
        commentDrawerState: {
          commentId: undefined,
        },
      },
    });

    expect(screen.getByTestId('search-second-step')).toBeInTheDocument();
  });
});

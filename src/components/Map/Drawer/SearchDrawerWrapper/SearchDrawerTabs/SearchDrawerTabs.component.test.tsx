import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { SearchDrawerTabs } from './SearchDrawerTabs.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <SearchDrawerTabs />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('SearchDrawerTabs - component', () => {
  it('should display tabs with search values', () => {
    renderComponent({
      search: {
        searchValue: ['aspirin', 'nadh'],
        loading: 'idle',
        perfectMatch: false,
      },
    });

    expect(screen.getByText('aspirin')).toBeInTheDocument();
    expect(screen.getByText('nadh')).toBeInTheDocument();
  });
  it('second test', async () => {
    const { store } = renderComponent({
      search: {
        searchValue: ['aspirin', 'nadh'],
        loading: 'idle',
        perfectMatch: false,
      },
    });

    const tabButton = screen.getByText('nadh');
    await act(() => {
      tabButton.click();
    });

    const {
      drawer: {
        searchDrawerState: { selectedSearchElement },
      },
    } = store.getState();

    expect(selectedSearchElement).toBe('nadh');
  });

  it('should display no tabs when no search values', () => {
    renderComponent({
      search: {
        searchValue: [],
        loading: 'idle',
        perfectMatch: false,
      },
    });

    expect(screen.queryByText('aspirin')).toBeNull();
    expect(screen.queryByText('nadh')).toBeNull();
  });

  it('should display no tabs when empty search value', () => {
    renderComponent({
      search: {
        searchValue: [''],
        loading: 'idle',
        perfectMatch: false,
      },
    });

    expect(screen.queryByText('aspirin')).toBeNull();
    expect(screen.queryByText('nadh')).toBeNull();
  });
});

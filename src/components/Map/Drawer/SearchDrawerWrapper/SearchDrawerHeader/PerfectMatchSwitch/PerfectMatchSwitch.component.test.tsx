/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT, SECOND_ARRAY_ELEMENT, THIRD_ARRAY_ELEMENT } from '@/constants/common';
import { SEARCH_STATE_INITIAL_MOCK } from '@/redux/search/search.mock';
import { AppDispatch, RootState, StoreType } from '@/redux/store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { MockStoreEnhanced } from 'redux-mock-store';
import { PerfectMatchSwitch } from './PerfectMatchSwitch.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <PerfectMatchSwitch />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithActionListener = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <PerfectMatchSwitch />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PerfectMatchSwitch - component', () => {
  it('should initialy be set to false when perfectMatch is not in query or set to false', () => {
    renderComponent({ search: SEARCH_STATE_INITIAL_MOCK });

    const checkbox = screen.getByRole<HTMLInputElement>('checkbox');

    expect(checkbox.checked).toBe(false);
  });
  it('should initialy be set to true when perfectMatch query is set to true', () => {
    renderComponent({ search: { ...SEARCH_STATE_INITIAL_MOCK, perfectMatch: true } });

    const checkbox = screen.getByRole<HTMLInputElement>('checkbox');

    expect(checkbox.checked).toBe(true);
  });
  it('should set checkbox to true and update store', async () => {
    const { store } = renderComponent({ search: SEARCH_STATE_INITIAL_MOCK });
    expect(store.getState().search.perfectMatch).toBe(false);

    const checkbox = screen.getByRole<HTMLInputElement>('checkbox');
    act(() => {
      checkbox.click();
    });

    expect(store.getState().search.perfectMatch).toBe(true);
  });
  it('should set checkbox to false and update store', async () => {
    const { store } = renderComponent({
      search: { ...SEARCH_STATE_INITIAL_MOCK, perfectMatch: true },
    });
    expect(store.getState().search.perfectMatch).toBe(true);

    const checkbox = screen.getByRole<HTMLInputElement>('checkbox');
    act(() => {
      checkbox.click();
    });

    expect(store.getState().search.perfectMatch).toBe(false);
  });
  it('should trigger get search data and reset reactions on checked value change', async () => {
    const { store } = renderComponentWithActionListener({
      search: { ...SEARCH_STATE_INITIAL_MOCK, perfectMatch: false, searchValue: ['nadh', 'scna'] },
    });

    const checkbox = screen.getByRole<HTMLInputElement>('checkbox');
    act(() => {
      checkbox.click();
    });

    const actions = store.getActions();

    expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
      payload: true,
      type: 'search/setPerfectMatch',
    });

    expect(actions[SECOND_ARRAY_ELEMENT]).toStrictEqual({
      payload: undefined,
      type: 'reactions/resetReactionsData',
    });

    expect(actions[THIRD_ARRAY_ELEMENT].meta.arg).toStrictEqual({
      isPerfectMatch: true,
      searchQueries: ['nadh', 'scna'],
    });
    expect(actions[THIRD_ARRAY_ELEMENT].type).toEqual('project/getSearchData/pending');
  });
});

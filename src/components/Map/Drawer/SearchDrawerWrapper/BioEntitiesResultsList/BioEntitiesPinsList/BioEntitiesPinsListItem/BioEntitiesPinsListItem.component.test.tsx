/* eslint-disable no-magic-numbers */
import { DEFAULT_MAX_ZOOM } from '@/constants/map';
import { bioEntitiesContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { MAP_INITIAL_STATE } from '@/redux/map/map.constants';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState, StoreType } from '@/redux/store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { MockStoreEnhanced } from 'redux-mock-store';
import { getTypeBySBOTerm } from '@/utils/bioEntity/getTypeBySBOTerm';
import { newReactionsFixture } from '@/models/fixtures/newReactionsFixture';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { BioEntitiesPinsListItem } from './BioEntitiesPinsListItem.component';
import { PinListModelElement } from './BioEntitiesPinsListItem.types';

const BIO_ENTITY = {
  ...bioEntitiesContentFixture[0].bioEntity,
  fullName: 'fullName_',
  name: 'name_',
  symbol: 'symbol_',
};

const INITIAL_STORE_WITH_ENTITY_NUMBER: InitialStoreState = {
  entityNumber: {
    data: {
      [BIO_ENTITY.elementId]: 123,
    },
  },
};

const renderComponent = (
  name: string,
  pin: PinListModelElement,
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <BioEntitiesPinsListItem name={name} pin={pin} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithActionListener = (
  name: string,
  pin: PinListModelElement,
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <BioEntitiesPinsListItem name={name} pin={pin} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('BioEntitiesPinsListItem - component ', () => {
  it('should display name of bio entity element', () => {
    renderComponent(BIO_ENTITY.name, BIO_ENTITY, INITIAL_STORE_WITH_ENTITY_NUMBER);

    const bioEntityName = BIO_ENTITY.fullName || '';

    expect(screen.getByText(bioEntityName, { exact: false })).toBeInTheDocument();
  });
  it('should display symbol of bio entity element', () => {
    renderComponent(BIO_ENTITY.name, BIO_ENTITY, INITIAL_STORE_WITH_ENTITY_NUMBER);

    const bioEntitySymbol = BIO_ENTITY.symbol || '';

    expect(screen.getByText(bioEntitySymbol, { exact: false })).toBeInTheDocument();
  });
  it('should display empty string when symbol does not exist', () => {
    const bioEntity = {
      ...bioEntitiesContentFixture[0].bioEntity,
      symbol: null,
    };

    renderComponent(bioEntity.name, bioEntity);

    expect(screen.queryAllByTestId('bio-entity-symbol')).toHaveLength(0);
  });
  it('should display string type of bio entity element', () => {
    renderComponent(BIO_ENTITY.name, BIO_ENTITY, INITIAL_STORE_WITH_ENTITY_NUMBER);

    const bioEntityType = getTypeBySBOTerm(BIO_ENTITY.sboTerm);

    expect(screen.getByText(bioEntityType, { exact: false })).toBeInTheDocument();
  });
  it('should display synonyms of bio entity element', () => {
    renderComponent(BIO_ENTITY.name, BIO_ENTITY, INITIAL_STORE_WITH_ENTITY_NUMBER);

    const firstBioEntitySynonym = BIO_ENTITY.synonyms[0];
    const secondBioEntitySynonym = BIO_ENTITY.synonyms[1];

    expect(screen.getByText(firstBioEntitySynonym, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondBioEntitySynonym, { exact: false })).toBeInTheDocument();
  });
  it('should display list of references for pin', () => {
    renderComponent(BIO_ENTITY.name, BIO_ENTITY, INITIAL_STORE_WITH_ENTITY_NUMBER);

    const firstPinReferenceType = BIO_ENTITY.references[0].type;
    const firstPinReferenceResource = BIO_ENTITY.references[0].resource;
    const secondPinReferenceType = BIO_ENTITY.references[1].type;
    const secondPinReferenceResource = BIO_ENTITY.references[1].resource;

    expect(screen.getByText(firstPinReferenceType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(firstPinReferenceResource, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinReferenceType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinReferenceResource, { exact: false })).toBeInTheDocument();
  });
  it('should center map to pin coordinates after click on pin icon', async () => {
    const { store } = renderComponent(BIO_ENTITY.name, BIO_ENTITY, {
      ...INITIAL_STORE_WITH_ENTITY_NUMBER,
      map: {
        ...MAP_INITIAL_STATE,
        data: {
          ...MAP_INITIAL_STATE.data,
          modelId: HISTAMINE_MAP_ID,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
          position: {
            initial: {
              x: 0,
              y: 0,
              z: 2,
            },
            last: {
              x: 1,
              y: 1,
              z: 3,
            },
          },
        },
      },
    });
    const button = screen.getByTestId('center-to-pin-button');
    expect(button).toBeInTheDocument();

    act(() => {
      button.click();
    });

    expect(store.getState().map.data.position.last).toEqual({
      x: BIO_ENTITY.x,
      y: BIO_ENTITY.y,
      z: DEFAULT_MAX_ZOOM,
    });
  });

  it('should not center map to pin coordinates after click on pin icon if pin has no coords', async () => {
    const { store } = renderComponent(
      BIO_ENTITY.name,
      {
        ...BIO_ENTITY,
        x: undefined,
        y: undefined,
      },
      {
        ...INITIAL_STORE_WITH_ENTITY_NUMBER,
        map: {
          ...MAP_INITIAL_STATE,
          data: {
            ...MAP_INITIAL_STATE.data,
            modelId: HISTAMINE_MAP_ID,
            size: {
              width: 256,
              height: 256,
              tileSize: 256,
              minZoom: 1,
              maxZoom: 1,
            },
            position: {
              initial: {
                x: 0,
                y: 0,
                z: 2,
              },
              last: {
                x: 1,
                y: 1,
                z: 3,
              },
            },
          },
        },
      },
    );
    const button = screen.getByTestId('center-to-pin-button');
    expect(button).toBeInTheDocument();

    act(() => {
      button.click();
    });

    expect(store.getState().map.data.position.last).toEqual({
      x: 1,
      y: 1,
      z: DEFAULT_MAX_ZOOM,
    });
  });

  it('should dispatch get search data and open drawer on fullName click', async () => {
    const { store } = renderComponentWithActionListener(
      BIO_ENTITY.name,
      {
        ...BIO_ENTITY,
        x: undefined,
        y: undefined,
      },
      {
        ...INITIAL_STORE_WITH_ENTITY_NUMBER,
        map: {
          ...MAP_INITIAL_STATE,
          data: {
            ...MAP_INITIAL_STATE.data,
            modelId: HISTAMINE_MAP_ID,
            size: {
              width: 256,
              height: 256,
              tileSize: 256,
              minZoom: 1,
              maxZoom: 1,
            },
            position: {
              initial: {
                x: 0,
                y: 0,
                z: 2,
              },
              last: {
                x: 1,
                y: 1,
                z: 3,
              },
            },
          },
        },
      },
    );
    const button = screen.getByText(BIO_ENTITY.name);
    expect(button).toBeInTheDocument();

    act(() => {
      button.click();
    });

    const actions = store.getActions();

    expect(actions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          payload: undefined,
          type: 'project/getSearchData/pending',
        }),
      ]),
    );

    expect(actions).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          payload: BIO_ENTITY.name,
          type: 'drawer/openSearchDrawerWithSelectedTab',
        }),
      ]),
    );

    // expect(actions).toEqual(
    //   expect.arrayContaining([
    //     expect.objectContaining({
    //       payload: undefined,
    //       type: 'project/getSubmapConnectionsBioEntity/pending',
    //     }),
    //   ]),
    // );
  });

  it('should reset reactions on fullName click', async () => {
    const { store } = renderComponent(
      BIO_ENTITY.name,
      {
        ...BIO_ENTITY,
        x: undefined,
        y: undefined,
      },
      {
        map: {
          ...MAP_INITIAL_STATE,
          data: {
            ...MAP_INITIAL_STATE.data,
            modelId: HISTAMINE_MAP_ID,
            size: {
              width: 256,
              height: 256,
              tileSize: 256,
              minZoom: 1,
              maxZoom: 1,
            },
            position: {
              initial: {
                x: 0,
                y: 0,
                z: 2,
              },
              last: {
                x: 1,
                y: 1,
                z: 3,
              },
            },
          },
        },
        reactions: {
          ...INITIAL_STORE_STATE_MOCK.reactions,
          data: newReactionsFixture.content,
        },
      },
    );
    const button = screen.getByText(BIO_ENTITY.name);
    expect(button).toBeInTheDocument();

    act(() => {
      button.click();
    });

    const state = store.getState();

    expect(state.reactions.data).toStrictEqual([]);
  });
});

import { ModelElement } from '@/types/models';

export type PinListModelElement = Pick<ModelElement, 'synonyms' | 'references'> & {
  symbol?: ModelElement['symbol'];
  fullName?: ModelElement['fullName'];
  x?: ModelElement['x'];
  y?: ModelElement['y'];
  elementId?: ModelElement['elementId'];
  sboTerm?: ModelElement['sboTerm'];
};

export type PinListModelElementWithCoords = PinListModelElement & {
  x: ModelElement['x'];
  y: ModelElement['y'];
};

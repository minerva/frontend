/* eslint-disable no-magic-numbers */
import { bioEntitiesContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { StoreType } from '@/redux/store';
import { BioEntityContent } from '@/types/models';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { BioEntitiesPinsList } from './BioEntitiesPinsList.component';

const renderComponent = (
  bioEnititesPins: BioEntityContent[],
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <BioEntitiesPinsList bioEnititesPins={bioEnititesPins} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('BioEntitiesPinsList - component ', () => {
  it('should display list of bio entites elements', () => {
    renderComponent(bioEntitiesContentFixture);
    const bioEntitiesWithFullName = bioEntitiesContentFixture.filter(({ bioEntity }) =>
      Boolean(bioEntity.fullName),
    );

    if (bioEntitiesWithFullName.length > 0) {
      expect(screen.getAllByTestId('bio-entity-name')).toHaveLength(bioEntitiesWithFullName.length);
    }
  });
});

/* eslint-disable no-magic-numbers */
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { PinItem } from '../PinsList/PinsList.types';
import { AccordionsDetails } from './AccordionsDetails.component';

const DRUGS_PINS_LIST = drugsFixture.map(drug => ({
  id: drug.id,
  name: drug.name,
  data: drug,
}));

const renderComponent = (
  pinsList: PinItem[],
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <AccordionsDetails pinsList={pinsList} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('AccordionsDetails - component', () => {
  it('should display name of drug', () => {
    renderComponent(DRUGS_PINS_LIST);

    const drugName = drugsFixture[0].name;

    expect(screen.getByText(drugName, { exact: false })).toBeInTheDocument();
  });
  it('should display description of drug', () => {
    renderComponent(DRUGS_PINS_LIST);

    const drugDescription = drugsFixture[0].description ? drugsFixture[0].description : '';

    expect(screen.getByTestId('details-description').textContent).toContain(drugDescription);
  });
  it('should display synonyms of drug', () => {
    renderComponent(DRUGS_PINS_LIST);

    const firstDrugSynonym = drugsFixture[0].synonyms[0];
    const secondDrugSynonym = drugsFixture[0].synonyms[1];

    expect(screen.getByText(firstDrugSynonym, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondDrugSynonym, { exact: false })).toBeInTheDocument();
  });
});

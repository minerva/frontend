/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { drugFixture } from '@/models/fixtures/drugFixtures';
import { EntityNumber } from '@/redux/entityNumber/entityNumber.types';
import { PinItem } from '../PinsList.types';
import { getTargetElementsUniqueSorted } from './getTargetElementsUniqueSorted';

const BASE_TARGET_ELEMENT =
  drugFixture.targets[FIRST_ARRAY_ELEMENT].targetElements[FIRST_ARRAY_ELEMENT];
const TARGET_ELEMENT_ID_DUPLICATED = 'el_duplicated';
const TARGET_ELEMENT_ID_UNIQUE = 'el_unique';
const TARGET_ELEMENT_ID_UNIQUE_2 = 'el_unique_2';
const NUMBER_OF_UNIQUE_ELEMENTS = 3; // 4 elements, but 1 is an duplicate

const PIN_ITEM: PinItem = {
  id: 1001,
  name: 'pin name',
  data: {
    ...drugFixture,
    targets: [
      {
        ...drugFixture.targets[FIRST_ARRAY_ELEMENT],
        targetElements: [
          {
            ...BASE_TARGET_ELEMENT,
            elementId: TARGET_ELEMENT_ID_DUPLICATED,
          },
          {
            ...BASE_TARGET_ELEMENT,
            elementId: TARGET_ELEMENT_ID_UNIQUE,
          },
          {
            ...BASE_TARGET_ELEMENT,
            elementId: TARGET_ELEMENT_ID_UNIQUE_2,
          },
        ],
      },
      {
        ...drugFixture.targets[FIRST_ARRAY_ELEMENT],
        targetElements: [
          {
            ...BASE_TARGET_ELEMENT,
            elementId: TARGET_ELEMENT_ID_DUPLICATED,
          },
        ],
      },
    ],
  },
};

const ENTITY_NUMBER: EntityNumber = {
  [TARGET_ELEMENT_ID_UNIQUE]: 1,
  [TARGET_ELEMENT_ID_DUPLICATED]: 2,
  [TARGET_ELEMENT_ID_UNIQUE_2]: 3,
};

describe('getTargetElementsUniqueSorted - util', () => {
  it('should return sorted by entityNumber unique target elements', () => {
    const result = getTargetElementsUniqueSorted([PIN_ITEM], { entityNumber: ENTITY_NUMBER });

    expect(result.length).toEqual(NUMBER_OF_UNIQUE_ELEMENTS);
    expect(result[0].element.elementId).toEqual(TARGET_ELEMENT_ID_UNIQUE);
    expect(result[1].element.elementId).toEqual(TARGET_ELEMENT_ID_DUPLICATED);
    expect(result[2].element.elementId).toEqual(TARGET_ELEMENT_ID_UNIQUE_2);
  });
});

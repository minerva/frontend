import { Chemical, Drug, ModelElement, PinDetailsItem } from '@/types/models';
import { PinType } from '@/types/pin';

export type PinItem = {
  id: string | number;
  name: string;
  data: Drug | Chemical;
};

export type PinTypeWithNone = PinType | 'none';

export type AvailableSubmaps = {
  id: number | string;
  modelId: number;
  name: string;
};

export type TargetElement = {
  target: PinDetailsItem;
  element: ModelElement;
};

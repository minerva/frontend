/* eslint-disable no-magic-numbers */
import { bioEntitiesContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { AppDispatch, RootState } from '@/redux/store';
import { ModelElement, PinDetailsItem } from '@/types/models';
import { InitialStoreState } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { MockStoreEnhanced } from 'redux-mock-store';
import { MAIN_MAP_ID } from '@/constants/mocks';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { PinTypeWithNone } from '../PinsList.types';
import { PinsListItem } from './PinsListItem.component';

const DRUGS_PIN = {
  name: drugsFixture[0].targets[0].name,
  pin: drugsFixture[0].targets[0],
};

const CHEMICALS_PIN = {
  name: chemicalsFixture[0].targets[0].name,
  pin: chemicalsFixture[0].targets[0],
};

const PIN_NUMBER = 10;
const MODEL_ELEMENT = modelElementFixture;

const INITIAL_STORE_STATE: InitialStoreState = {
  models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
  map: {
    data: {
      ...initialMapDataFixture,
      modelId: MAIN_MAP_ID,
    },
    loading: 'succeeded',
    error: { message: '', name: '' },
    openedMaps: openedMapsThreeSubmapsFixture,
  },
};

const renderComponent = (
  name: string,
  pin: PinDetailsItem,
  type: PinTypeWithNone,
  element: ModelElement,
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <PinsListItem name={name} type={type} pin={pin} element={element} number={PIN_NUMBER} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PinsListItem - component ', () => {
  drugsFixture[0].targets[0].targetParticipants[0].link = 'https://example.com/plugin.js';
  drugsFixture[0].targets[0].targetParticipants[1].link = 'https://example.com/plugin.js';
  chemicalsFixture[0].targets[0].targetParticipants[0].link = 'https://example.com/plugin.js';
  chemicalsFixture[0].targets[0].targetParticipants[1].link = 'https://example.com/plugin.js';

  it('should display full name of pin', () => {
    renderComponent(DRUGS_PIN.name, DRUGS_PIN.pin, 'drugs', MODEL_ELEMENT, INITIAL_STORE_STATE);

    const drugName = drugsFixture[0].targets[0].name;

    expect(screen.getByText(drugName)).toBeInTheDocument();
  });
  it('should display list of elements for pin for drugs', () => {
    renderComponent(DRUGS_PIN.name, DRUGS_PIN.pin, 'drugs', MODEL_ELEMENT, INITIAL_STORE_STATE);

    const firstPinElementType = drugsFixture[0].targets[0].targetParticipants[0].type;
    const firstPinElementResource = drugsFixture[0].targets[0].targetParticipants[0].resource;
    const secondPinElementType = drugsFixture[0].targets[0].targetParticipants[1].type;
    const secondPinElementResource = drugsFixture[0].targets[0].targetParticipants[1].resource;

    expect(screen.getByText(firstPinElementType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(firstPinElementResource, { exact: false })).toBeInTheDocument();

    if (!secondPinElementType) {
      expect(screen.queryByText(secondPinElementType, { exact: false })).toBeNull();
      expect(screen.queryByText(secondPinElementResource, { exact: false })).toBeNull();
    }
  });
  it('should display list of references for pin', () => {
    renderComponent(DRUGS_PIN.name, DRUGS_PIN.pin, 'drugs', MODEL_ELEMENT, INITIAL_STORE_STATE);

    const firstPinReferenceType = drugsFixture[0].targets[0].references[0].type;
    const firstPinReferenceResource = drugsFixture[0].targets[0].references[0].resource;
    const secondPinReferenceType = drugsFixture[0].targets[0].references[1].type;
    const secondPinReferenceResource = drugsFixture[0].targets[0].references[1].resource;

    expect(screen.getByText(firstPinReferenceType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(firstPinReferenceResource, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinReferenceType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinReferenceResource, { exact: false })).toBeInTheDocument();
  });
  it('should display list of elements for pin for chemicals', () => {
    renderComponent(
      CHEMICALS_PIN.name,
      CHEMICALS_PIN.pin,
      'chemicals',
      MODEL_ELEMENT,
      INITIAL_STORE_STATE,
    );

    const firstPinElementType = chemicalsFixture[0].targets[0].targetParticipants[0].type;
    const firstPinElementResource = chemicalsFixture[0].targets[0].targetParticipants[0].resource;
    const secondPinElementType = chemicalsFixture[0].targets[0].targetParticipants[1].type;
    const secondPinElementResource = chemicalsFixture[0].targets[0].targetParticipants[1].resource;

    expect(screen.getByText(firstPinElementType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(firstPinElementResource, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinElementType, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(secondPinElementResource, { exact: false })).toBeInTheDocument();
  });

  // TODO - it's probably flacky test
  it.skip('should not display list of elements for pin for bioentities', () => {
    renderComponent(CHEMICALS_PIN.name, CHEMICALS_PIN.pin, 'drugs', MODEL_ELEMENT);

    const bioEntityName = bioEntitiesContentFixture[2].bioEntity.fullName
      ? bioEntitiesContentFixture[2].bioEntity.fullName
      : '';

    expect(screen.queryByText(bioEntityName, { exact: false })).not.toBeInTheDocument();
  });
  it("should not display list of available submaps for pin when there aren't any submaps", () => {
    const chemicalWithoutSubmaps = {
      ...CHEMICALS_PIN.pin,
      targetElements: [],
    };

    renderComponent(
      CHEMICALS_PIN.name,
      chemicalWithoutSubmaps,
      'chemicals',
      MODEL_ELEMENT,
      INITIAL_STORE_STATE,
    );

    expect(screen.queryByText('Available in submaps:')).toBeNull();
  });
  it('should call setMapPosition if coordinates exist in bioEntity element', () => {
    const { store } = renderComponent(
      DRUGS_PIN.name,
      DRUGS_PIN.pin,
      'drugs',
      {
        ...MODEL_ELEMENT,
        x: 1000,
        y: 500,
      },
      {
        models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MODEL_ELEMENT.model,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
      },
    );

    const centerToPinButton = screen.getByTestId('center-to-pin');
    centerToPinButton.click();

    expect(store.getActions()).toStrictEqual([
      { payload: { x: 1000, y: 500, z: 8 }, type: 'map/setMapPosition' },
    ]);
  });

  it('should call setMapPosition and onSubmapOpen if bioentity element model is different from current', () => {
    const { store } = renderComponent(
      DRUGS_PIN.name,
      DRUGS_PIN.pin,
      'drugs',
      {
        ...MODEL_ELEMENT,
        x: 1000,
        y: 500,
        model: 52,
      },
      {
        models: MODELS_DATA_MOCK_WITH_MAIN_MAP,
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MODEL_ELEMENT.model,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
      },
    );

    const centerToPinButton = screen.getByTestId('center-to-pin');
    centerToPinButton.click();

    expect(store.getActions()).toEqual(
      expect.arrayContaining([
        {
          payload: { modelId: 52, modelName: 'Core PD map' },
          type: 'map/openMapAndSetActive',
        },
      ]),
    );

    expect(store.getActions()).toEqual(
      expect.arrayContaining([{ payload: { x: 1000, y: 500, z: 8 }, type: 'map/setMapPosition' }]),
    );
  });
});

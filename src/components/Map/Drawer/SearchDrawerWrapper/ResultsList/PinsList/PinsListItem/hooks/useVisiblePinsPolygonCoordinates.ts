import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { mapModelIdSelector } from '@/redux/map/map.selectors';
import { Point } from '@/types/map';
import { PinDetailsItem } from '@/types/models';
import { usePointToProjection } from '@/utils/map/usePointToProjection';
import { isPointValid } from '@/utils/point/isPointValid';
import { Coordinate } from 'ol/coordinate';
import { useMemo } from 'react';

const VALID_POLYGON_COORDINATES_LENGTH = 2;

export const useVisiblePinsPolygonCoordinates = (
  pinTargetElements: PinDetailsItem['targetElements'],
): Coordinate[] | undefined => {
  const pointToProjection = usePointToProjection();
  const currentModelId = useAppSelector(mapModelIdSelector);
  const currentMapPinElements = useMemo(
    () => pinTargetElements.filter(el => el.model === currentModelId),
    [currentModelId, pinTargetElements],
  );

  const polygonPoints = useMemo((): Point[] => {
    const allX = currentMapPinElements.map(({ x }) => x);
    const allY = currentMapPinElements.map(({ y }) => y);
    const minX = Math.min(...allX);
    const maxX = Math.max(...allX);

    const minY = Math.min(...allY);
    const maxY = Math.max(...allY);

    const points = [
      {
        x: minX,
        y: maxY,
      },
      {
        x: maxX,
        y: minY,
      },
    ];
    return points.filter(isPointValid);
  }, [currentMapPinElements]);

  const polygonCoordinates = useMemo(
    () => polygonPoints.map(point => pointToProjection(point)),
    [polygonPoints, pointToProjection],
  );

  if (polygonCoordinates.length !== VALID_POLYGON_COORDINATES_LENGTH) {
    return undefined;
  }

  return polygonCoordinates;
};

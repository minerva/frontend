/* eslint-disable no-magic-numbers */
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { MAP_INITIAL_STATE } from '@/redux/map/map.constants';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { useVisiblePinsPolygonCoordinates } from './useVisiblePinsPolygonCoordinates';

describe('useVisiblePinsPolygonCoordinates - hook', () => {
  it('should return undefined if receives empty array', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        ...MAP_INITIAL_STATE,
        data: {
          ...MAP_INITIAL_STATE.data,
          modelId: HISTAMINE_MAP_ID,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
        },
      },
    });

    const { result } = renderHook(() => useVisiblePinsPolygonCoordinates([]), {
      wrapper: Wrapper,
    });

    expect(result.current).toBe(undefined);
  });
  it('should return undefined if received array does not contain bioEntities with current map id', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        ...MAP_INITIAL_STATE,
        data: {
          ...MAP_INITIAL_STATE.data,
          modelId: HISTAMINE_MAP_ID,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
        },
      },
    });

    const { result } = renderHook(
      () =>
        useVisiblePinsPolygonCoordinates([
          {
            ...bioEntityContentFixture.bioEntity,
            model: 52,
          },
          {
            ...bioEntityContentFixture.bioEntity,
            model: 51,
          },
        ]),
      {
        wrapper: Wrapper,
      },
    );

    expect(result.current).toBe(undefined);
  });
  it('should return coordinates if received array contain bioEntities with current map id', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      map: {
        ...MAP_INITIAL_STATE,
        data: {
          ...MAP_INITIAL_STATE.data,
          modelId: HISTAMINE_MAP_ID,
          size: {
            width: 256,
            height: 256,
            tileSize: 256,
            minZoom: 1,
            maxZoom: 1,
          },
        },
      },
    });

    const { result } = renderHook(
      () =>
        useVisiblePinsPolygonCoordinates([
          {
            ...bioEntityContentFixture.bioEntity,
            model: 5051,
            x: 97,
            y: 53,
            z: 1,
          },
          {
            ...bioEntityContentFixture.bioEntity,
            model: HISTAMINE_MAP_ID,
            x: 12,
            y: 25,
            z: 1,
          },
          {
            ...bioEntityContentFixture.bioEntity,
            model: HISTAMINE_MAP_ID,
            x: 16,
            y: 16,
            z: 1,
          },
        ]),
      {
        wrapper: Wrapper,
      },
    );

    expect(result.current).toEqual([
      [-18158992, 16123932],
      [-17532820, 17532820],
    ]);
  });
});

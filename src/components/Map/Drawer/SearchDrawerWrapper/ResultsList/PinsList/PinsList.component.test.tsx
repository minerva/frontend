import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { PinsList } from './PinsList.component';
import { PinItem, PinTypeWithNone } from './PinsList.types';

const DRUGS_PINS_LIST = drugsFixture.map(drug => ({
  id: drug.id,
  name: drug.name,
  data: drug,
}));

const CHEMICALS_PINS_LIST = chemicalsFixture.map(chemical => ({
  id: chemical.id.id,
  name: chemical.name,
  data: chemical,
}));

const renderComponent = (
  pinsList: PinItem[],
  type: PinTypeWithNone,
  initialStoreState: InitialStoreState = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <PinsList pinsList={pinsList} type={type} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PinsList - component ', () => {
  it('should display list of drug targets', () => {
    renderComponent(DRUGS_PINS_LIST, 'drugs');

    expect(screen.getByTestId('pins-list')).toBeInTheDocument();
  });
  it('should display drug details when drug is searched', () => {
    renderComponent(DRUGS_PINS_LIST, 'drugs');

    expect(screen.getByTestId('accordions-details')).toBeInTheDocument();
  });
  it('should display list of chemicals targets', () => {
    renderComponent(CHEMICALS_PINS_LIST, 'chemicals');

    expect(screen.getByTestId('pins-list')).toBeInTheDocument();
  });
  it('should display chemicals details when chemical is searched', () => {
    renderComponent(CHEMICALS_PINS_LIST, 'chemicals');

    expect(screen.getByTestId('accordions-details')).toBeInTheDocument();
  });
  it('should not display list of bio enities when modelElement is searched', () => {
    renderComponent([], 'modelElement');

    expect(screen.queryByTestId('pins-list')).toBeNull();
  });
});

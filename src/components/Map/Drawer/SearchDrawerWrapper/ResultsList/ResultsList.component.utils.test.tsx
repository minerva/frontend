import { mapStepTypeToHeading } from './ResultsList.component.utils';

describe('mapStepTypeToHeading - util', () => {
  it('should return "Drug targets" for stepType "drugs"', () => {
    const result = mapStepTypeToHeading('drugs');
    expect(result).toBe('Drug targets');
  });

  it('should return "Small molecule targets" for stepType "chemicals"', () => {
    const result = mapStepTypeToHeading('chemicals');
    expect(result).toBe('Small molecule targets');
  });

  it('should return the input stepType for other values', () => {
    const result = mapStepTypeToHeading('someOtherType');
    expect(result).toBe('someOtherType');
  });

  it('should return the same string case-sensitive for stepType "drugs"', () => {
    const result = mapStepTypeToHeading('Drugs');
    expect(result).toBe('Drugs');
  });

  it('should return the same string case-sensitive for stepType "chemicals"', () => {
    const result = mapStepTypeToHeading('Chemicals');
    expect(result).toBe('Chemicals');
  });

  it('should return empty string for empty input', () => {
    const result = mapStepTypeToHeading('');
    expect(result).toBe('');
  });
});

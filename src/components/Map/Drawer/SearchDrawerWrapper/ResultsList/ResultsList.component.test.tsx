/* eslint-disable no-magic-numbers */
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { ResultsList } from './ResultsList.component';

const INITIAL_STATE: InitialStoreState = {
  search: {
    searchValue: ['aspirin'],
    loading: 'idle',
    perfectMatch: false,
  },
  drawer: {
    isOpen: true,
    drawerName: 'search',
    searchDrawerState: {
      currentStep: 2,
      stepType: 'drugs',
      selectedValue: undefined,
      listOfBioEnitites: [],
      selectedSearchElement: 'aspirin',
    },
    reactionDrawerState: {},
    bioEntityDrawerState: {
      drugs: {},
      chemicals: {},
    },
    overlayDrawerState: {
      currentStep: 0,
    },
    commentDrawerState: {
      commentId: undefined,
    },
  },
  drugs: {
    data: [
      {
        searchQueryElement: 'aspirin',
        loading: 'succeeded',
        error: { name: '', message: '' },
        data: drugsFixture,
      },
    ],
    loading: 'succeeded',
    error: { name: '', message: '' },
  },
};

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ResultsList />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ResultsList - component ', () => {
  it('should render results and navigation panel', () => {
    renderComponent(INITIAL_STATE);

    const headingText = screen.getByTestId('drawer-heading-text');
    expect(headingText.textContent).toBe('Drug targets');

    const fristDrugName = drugsFixture[0].targets[0].name;
    const secondDrugName = drugsFixture[0].targets[1].name;

    const fristDrugTargetElementsNumber = drugsFixture[0].targets[0].targetElements.length;
    const secondDrugTargetElementsNumber = drugsFixture[0].targets[1].targetElements.length;

    expect(screen.queryAllByText(fristDrugName).length).toEqual(fristDrugTargetElementsNumber);
    expect(screen.queryAllByText(secondDrugName).length).toEqual(secondDrugTargetElementsNumber);
  });
  it('should navigate to grouped search results after backward button click', async () => {
    const { store } = renderComponent(INITIAL_STATE);

    const {
      drawer: {
        searchDrawerState: { currentStep, stepType },
      },
    } = store.getState();

    expect(currentStep).toBe(2);
    expect(stepType).toBe('drugs');

    const backwardButton = screen.getByRole('close-drawer-button');
    await act(() => {
      backwardButton.click();
    });

    const {
      drawer: {
        searchDrawerState: { currentStep: updatedStep, stepType: updatedStepType },
      },
    } = store.getState();

    expect(updatedStep).toBe(1);
    expect(updatedStepType).toBe('none');
  });
});

/* eslint-disable no-magic-numbers */
import { act, render, screen } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { drawerSearchStepOneFixture } from '@/redux/drawer/drawerFixture';
import {
  initialMapDataFixture,
  openedMapsInitialValueFixture,
  openedMapsThreeSubmapsFixture,
} from '@/redux/map/map.fixtures';
import { MAIN_MAP_ID } from '@/constants/mocks';
import { DEFAULT_ERROR } from '@/constants/errors';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { BioEntitiesSubmapItem } from './BioEntitiesSubmapItem.component';

const SECOND_STEP = 2;

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <BioEntitiesSubmapItem
          mapName={MODELS_MOCK_SHORT[0].name}
          mapId={MODELS_MOCK_SHORT[0].id}
          numberOfModelElements={21}
          modelElements={[{ modelElement: modelElementFixture, perfect: true }]}
        />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('BioEntitiesSubmapItem - component', () => {
  it('should display map name, number of elements, icon', () => {
    renderComponent();

    expect(screen.getByText('Core PD map (21)')).toBeInTheDocument();
    expect(screen.getByTestId('arrow-icon')).toBeInTheDocument();
  });
  it('should navigate user to bio enitites results list after clicking button', async () => {
    const { store } = renderComponent({
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [
            {
              searchQueryElement: '',
              loading: 'idle',
              error: DEFAULT_ERROR,
              data: [{ modelElement: modelElementFixture, perfect: true }],
            },
          ],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
      drawer: drawerSearchStepOneFixture,
    });

    const navigationButton = screen.getByTestId('bio-entites-submap-button');
    await act(() => {
      navigationButton.click();
    });

    const {
      drawer: {
        searchDrawerState: { stepType, selectedValue, currentStep, listOfBioEnitites },
      },
    } = store.getState();

    expect(stepType).toBe('modelElement');
    expect(selectedValue).toBe(undefined);
    expect(currentStep).toBe(SECOND_STEP);
    expect(listOfBioEnitites).toStrictEqual([{ modelElement: modelElementFixture, perfect: true }]);
  });
  it("should open submap and set it to active if it's not already opened", async () => {
    const { store } = renderComponent({
      drawer: drawerSearchStepOneFixture,
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
      map: {
        data: initialMapDataFixture,
        loading: 'succeeded',
        error: { name: '', message: '' },
        openedMaps: openedMapsInitialValueFixture,
      },
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [
            {
              searchQueryElement: '',
              loading: 'idle',
              error: DEFAULT_ERROR,
              data: [{ modelElement: modelElementFixture, perfect: true }],
            },
          ],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const {
      data: { modelId },
      openedMaps,
    } = store.getState().map;

    expect(modelId).toBe(0);
    expect(openedMaps).not.toContainEqual({
      modelId: MAIN_MAP_ID,
      modelName: 'Core PD map',
      lastPosition: { x: 0, y: 0, z: 0 },
    });

    const navigationButton = screen.getByTestId('bio-entites-submap-button');
    await act(() => {
      navigationButton.click();
    });

    const {
      data: { modelId: newModelId },
      openedMaps: newOpenedMaps,
    } = store.getState().map;

    expect(newOpenedMaps).toContainEqual({
      modelId: MAIN_MAP_ID,
      modelName: 'Core PD map',
      lastPosition: { x: 0, y: 0, z: 0 },
    });

    expect(newModelId).toBe(MAIN_MAP_ID);
  });
  it("should set map active if it's already opened", async () => {
    const { store } = renderComponent({
      drawer: drawerSearchStepOneFixture,
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
      map: {
        data: {
          ...initialMapDataFixture,
          modelId: MAIN_MAP_ID,
        },
        loading: 'succeeded',
        error: { name: '', message: '' },
        openedMaps: openedMapsThreeSubmapsFixture,
      },
    });

    const navigationButton = screen.getByTestId('bio-entites-submap-button');
    await act(() => {
      navigationButton.click();
    });

    const {
      map: {
        data: { modelId },
        openedMaps,
      },
    } = store.getState();

    const histamineMap = openedMaps.filter(map => map.modelName === 'Histamine signaling');

    // eslint-disable-next-line no-magic-numbers
    expect(histamineMap.length).toBe(1);
    expect(modelId).toBe(MAIN_MAP_ID);
  });
});

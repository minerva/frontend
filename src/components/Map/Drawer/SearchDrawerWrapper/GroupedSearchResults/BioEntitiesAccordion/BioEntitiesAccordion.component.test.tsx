import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { MODELS_MOCK } from '@/models/mocks/modelsMock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState, StoreType } from '@/redux/store';
import { Accordion } from '@/shared/Accordion';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID, PRKN_SUBSTRATES_MAP_ID } from '@/constants/mocks';
import { DEFAULT_ERROR } from '@/constants/errors';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { BioEntitiesAccordion } from './BioEntitiesAccordion.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Accordion>
          <BioEntitiesAccordion />
        </Accordion>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithActionListener = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <Accordion>
          <BioEntitiesAccordion />
        </Accordion>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('BioEntitiesAccordion - component', () => {
  it('should display loading indicator when bioEntity search is pending', () => {
    renderComponent({
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [
            {
              searchQueryElement: '',
              loading: 'pending',
              error: DEFAULT_ERROR,
              data: [{ modelElement: modelElementFixture, perfect: true }],
            },
          ],
          loading: 'pending',
          error: DEFAULT_ERROR,
        },
      },
      models: {
        data: [],
        loading: 'pending',
        error: { name: '', message: '' },
      },
    });

    expect(screen.getByText('Content (Loading...)')).toBeInTheDocument();
  });

  it('should render list of maps with number of entities after succeeded bio entity search', () => {
    renderComponent({
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [
            {
              searchQueryElement: '',
              loading: 'succeeded',
              error: DEFAULT_ERROR,
              data: [
                {
                  modelElement: { ...modelElementFixture, model: HISTAMINE_MAP_ID },
                  perfect: true,
                },
                {
                  modelElement: { ...modelElementFixture, model: MAIN_MAP_ID },
                  perfect: true,
                },
                {
                  modelElement: { ...modelElementFixture, model: PRKN_SUBSTRATES_MAP_ID },
                  perfect: true,
                },
              ],
            },
          ],
          loading: 'succeeded',
          error: DEFAULT_ERROR,
        },
      },
      models: {
        data: MODELS_MOCK,
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
    });

    expect(screen.getByText(`Content (3)`)).toBeInTheDocument();
    expect(screen.getByText(`Core PD map (1)`)).toBeInTheDocument();
    expect(screen.getByText(`Histamine signaling (1)`)).toBeInTheDocument();
    expect(screen.getByText(`PRKN substrates (1)`)).toBeInTheDocument();
  });

  it('should fire toggleIsContentTabOpened on accordion item button click', () => {
    const { store } = renderComponentWithActionListener({
      ...INITIAL_STORE_STATE_MOCK,
      bioEntity: {
        isContentTabOpened: false,
      },
      models: {
        data: MODELS_MOCK,
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
    });

    const button = screen.getByTestId('accordion-item-button');

    button.click();
    expect(store.getActions()[FIRST_ARRAY_ELEMENT]).toStrictEqual({
      payload: true,
      type: 'bioEntityContents/toggleIsContentTabOpened',
    });
  });
});

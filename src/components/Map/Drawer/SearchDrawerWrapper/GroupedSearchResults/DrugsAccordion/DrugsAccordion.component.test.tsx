import { StoreType } from '@/redux/store';
import { Accordion } from '@/shared/Accordion';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import {
  drawerSearchStepOneFixture,
  drawerSearchDrugsStepTwoFixture,
} from '@/redux/drawer/drawerFixture';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { DrugsAccordion } from './DrugsAccordion.component';

const SECOND_STEP = 2;

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Accordion>
          <DrugsAccordion />
        </Accordion>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DrugsAccordion - component', () => {
  it('should display drugs number after successfull drug search', () => {
    renderComponent({
      drugs: {
        data: [
          {
            searchQueryElement: '',
            loading: 'succeeded',
            error: { name: '', message: '' },
            data: drugsFixture,
          },
        ],
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
    });
    expect(screen.getByText('Drug targets (4 of 4 found in the map)')).toBeInTheDocument();
  });
  it('should display loading indicator while waiting for drug search response', () => {
    renderComponent({
      drugs: {
        data: [
          {
            searchQueryElement: '',
            loading: 'pending',
            error: { name: '', message: '' },
            data: drugsFixture,
          },
        ],
        loading: 'pending',
        error: { name: '', message: '' },
      },
    });
    expect(screen.getByText('Drug targets (Loading...)')).toBeInTheDocument();
  });
  it('should navigate user to drugs results list after clicking button', async () => {
    const { store } = renderComponent({
      drawer: drawerSearchDrugsStepTwoFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    await act(() => {
      navigationButton.click();
    });

    const {
      drawer: {
        searchDrawerState: { stepType, selectedValue, currentStep },
      },
    } = store.getState();

    expect(stepType).toBe('drugs');
    expect(selectedValue).toBe(undefined);
    expect(currentStep).toBe(SECOND_STEP);
  });
  it('should disable navigation button when there is no drugs', async () => {
    renderComponent({
      drugs: { data: [], loading: 'succeeded', error: { name: '', message: '' } },
      drawer: drawerSearchStepOneFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    expect(navigationButton).toBeDisabled();
  });
  it('should disable navigation button when waiting for api response', async () => {
    renderComponent({
      drugs: { data: [], loading: 'pending', error: { name: '', message: '' } },
      drawer: drawerSearchStepOneFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    expect(navigationButton).toBeDisabled();
  });
});

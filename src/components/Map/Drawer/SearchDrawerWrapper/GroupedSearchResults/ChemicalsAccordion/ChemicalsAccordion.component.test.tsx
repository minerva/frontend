import { act, render, screen } from '@testing-library/react';
import { MODELS_MOCK } from '@/models/mocks/modelsMock';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { Accordion } from '@/shared/Accordion';
import {
  drawerSearchStepOneFixture,
  drawerSearchChemicalsStepTwoFixture,
} from '@/redux/drawer/drawerFixture';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { ChemicalsAccordion } from './ChemicalsAccordion.component';

const SECOND_STEP = 2;

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Accordion>
          <ChemicalsAccordion />
        </Accordion>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DrugsAccordion - component', () => {
  it('should display drugs number after succesfull chemicals search', () => {
    renderComponent({
      chemicals: {
        data: [
          {
            searchQueryElement: '',
            loading: 'succeeded',
            error: { name: '', message: '' },
            data: chemicalsFixture,
          },
        ],
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
      models: {
        data: MODELS_MOCK,
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
    });

    expect(
      screen.getByText('Small molecule targets (4 of 4 found in the map)'),
    ).toBeInTheDocument();
  });
  it('should display loading indicator while waiting for chemicals search response', () => {
    renderComponent({
      chemicals: {
        data: [
          {
            searchQueryElement: '',
            loading: 'pending',
            error: { name: '', message: '' },
            data: chemicalsFixture,
          },
        ],
        loading: 'pending',
        error: { name: '', message: '' },
      },
      models: {
        data: [],
        loading: 'pending',
        error: { name: '', message: '' },
      },
    });

    expect(screen.getByText('Small molecule targets (Loading...)')).toBeInTheDocument();
  });
  it('should navigate user to chemical results list after clicking button', async () => {
    const { store } = renderComponent({
      drawer: drawerSearchChemicalsStepTwoFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    await act(() => {
      navigationButton.click();
    });

    const {
      drawer: {
        searchDrawerState: { stepType, selectedValue, currentStep },
      },
    } = store.getState();

    expect(stepType).toBe('chemicals');
    expect(selectedValue).toBe(undefined);
    expect(currentStep).toBe(SECOND_STEP);
  });
  it('should disable navigation button when there is no chemicals', async () => {
    renderComponent({
      chemicals: { data: [], loading: 'succeeded', error: { name: '', message: '' } },
      drawer: drawerSearchStepOneFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    expect(navigationButton).toBeDisabled();
  });
  it('should disable navigation button when waiting for api response', async () => {
    renderComponent({
      chemicals: { data: [], loading: 'pending', error: { name: '', message: '' } },
      drawer: drawerSearchStepOneFixture,
    });

    const navigationButton = screen.getByTestId('accordion-item-button');
    expect(navigationButton).toBeDisabled();
  });
});

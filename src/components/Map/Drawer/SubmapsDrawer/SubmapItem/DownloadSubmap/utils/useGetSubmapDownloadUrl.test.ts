import { BASE_API_URL, PROJECT_ID } from '@/constants';
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState } from '@/redux/store';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { useGetSubmapDownloadUrl } from './useGetSubmapDownloadUrl';

const VALID_HANDLER = 'lcsb.mapviewer.wikipathway.GpmlParser';
const VALID_MODEL_ID = HISTAMINE_MAP_ID;
const VALID_BACKGROUND_ID = 53;
const VALID_MAX_ZOOM = 9;

const getState = ({
  modelId,
  backgroundId,
  mapSizeMaxZoom,
}: {
  modelId: number;
  backgroundId: number;
  mapSizeMaxZoom: number;
}): RootState => ({
  ...INITIAL_STORE_STATE_MOCK,
  map: {
    ...INITIAL_STORE_STATE_MOCK.map,
    data: {
      ...INITIAL_STORE_STATE_MOCK.map.data,
      modelId,
      backgroundId,
      size: {
        ...INITIAL_STORE_STATE_MOCK.map.data.size,
        maxZoom: mapSizeMaxZoom,
      },
    },
  },
  models: {
    ...INITIAL_STORE_STATE_MOCK.models,
    data: [
      {
        ...modelsFixture[FIRST_ARRAY_ELEMENT],
        id: VALID_MODEL_ID,
      },
    ],
  },
});

describe('useGetSubmapDownloadUrl - hook', () => {
  describe('when not all params valid', () => {
    const cases = [
      {
        modelId: 0,
        backgroundId: VALID_BACKGROUND_ID,
        mapSizeMaxZoom: VALID_MAX_ZOOM,
        handler: VALID_HANDLER,
      },
      {
        modelId: VALID_MODEL_ID,
        backgroundId: 0,
        mapSizeMaxZoom: VALID_MAX_ZOOM,
        handler: VALID_HANDLER,
      },
      {
        modelId: VALID_MODEL_ID,
        backgroundId: VALID_BACKGROUND_ID,
        mapSizeMaxZoom: 0,
        handler: VALID_HANDLER,
      },
      {
        modelId: VALID_MODEL_ID,
        backgroundId: VALID_BACKGROUND_ID,
        mapSizeMaxZoom: VALID_MAX_ZOOM,
        handler: '',
      },
    ];

    it.each(cases)('should return empty string', ({ handler, ...stateParams }) => {
      const { Wrapper } = getReduxWrapperWithStore(getState(stateParams));

      const {
        result: { current: getSubmapDownloadUrl },
      } = renderHook(() => useGetSubmapDownloadUrl(), { wrapper: Wrapper });

      expect(getSubmapDownloadUrl({ handler, modelId: 0 })).toBe('');
    });
  });

  describe('when all params valid', () => {
    it('should return valid string', () => {
      const { Wrapper } = getReduxWrapperWithStore(
        getState({
          modelId: VALID_MODEL_ID,
          backgroundId: VALID_BACKGROUND_ID,
          mapSizeMaxZoom: VALID_MAX_ZOOM,
        }),
      );

      const {
        result: { current: getSubmapDownloadUrl },
      } = renderHook(() => useGetSubmapDownloadUrl(), { wrapper: Wrapper });

      expect(getSubmapDownloadUrl({ handler: VALID_HANDLER, modelId: HISTAMINE_MAP_ID })).toBe(
        `${BASE_API_URL}/projects/${PROJECT_ID}/models/${HISTAMINE_MAP_ID}:downloadModel?backgroundOverlayId=53&handlerClass=lcsb.mapviewer.wikipathway.GpmlParser&zoomLevel=9`,
      );
    });
  });
});

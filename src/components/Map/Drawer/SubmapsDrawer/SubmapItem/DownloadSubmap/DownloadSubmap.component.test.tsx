import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { configurationFixture } from '@/models/fixtures/configurationFixture';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import {
  CONFIGURATION_FORMATS_MOCK,
  CONFIGURATION_FORMATS_TYPES_MOCK,
} from '@/models/mocks/configurationFormatsMock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { RootState, StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, renderHook, screen } from '@testing-library/react';
import { HISTAMINE_MAP_ID } from '@/constants/mocks';
import { DownloadSubmap } from './DownloadSubmap.component';
import { GetSubmapDownloadUrl, useGetSubmapDownloadUrl } from './utils/useGetSubmapDownloadUrl';

const VALID_MODEL_ID = HISTAMINE_MAP_ID;
const VALID_BACKGROUND_ID = 53;
const VALID_MAX_ZOOM = 9;

const getState = (): RootState => ({
  ...INITIAL_STORE_STATE_MOCK,
  map: {
    ...INITIAL_STORE_STATE_MOCK.map,
    data: {
      ...INITIAL_STORE_STATE_MOCK.map.data,
      modelId: VALID_MODEL_ID,
      backgroundId: VALID_BACKGROUND_ID,
      size: {
        ...INITIAL_STORE_STATE_MOCK.map.data.size,
        maxZoom: VALID_MAX_ZOOM,
      },
    },
  },
  models: {
    ...INITIAL_STORE_STATE_MOCK.models,
    data: [
      {
        ...modelsFixture[FIRST_ARRAY_ELEMENT],
        id: VALID_MODEL_ID,
      },
    ],
  },
  configuration: {
    ...INITIAL_STORE_STATE_MOCK.configuration,
    main: {
      ...INITIAL_STORE_STATE_MOCK.configuration.main,
      data: {
        ...configurationFixture,
        modelFormats: CONFIGURATION_FORMATS_MOCK,
      },
    },
  },
});

const toggleListByButtonClick = (): void => {
  const button = screen.getByTestId('download-submap-button');
  act(() => {
    button.click();
  });
};

const getUtilGetSubmapDownloadUrl = (): GetSubmapDownloadUrl => {
  const { Wrapper } = getReduxWrapperWithStore(getState());

  const {
    result: { current: getSubmapDownloadUrl },
  } = renderHook(() => useGetSubmapDownloadUrl(), { wrapper: Wrapper });

  return getSubmapDownloadUrl;
};

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <DownloadSubmap modelId={VALID_MODEL_ID} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DownloadSubmap - component', () => {
  it('should render download button', () => {
    renderComponent(getState());
    const button = screen.getByTestId('download-submap-button');
    expect(button).toBeInTheDocument();
  });

  it('should open list on button click', () => {
    renderComponent(getState());
    toggleListByButtonClick();

    const list = screen.getByTestId('download-submap-list');
    expect(list).not.toHaveClass('hidden');
  });

  it('should close list on button click twice', () => {
    renderComponent(getState());
    const list = screen.getByTestId('download-submap-list');

    // list should be opened
    toggleListByButtonClick();
    expect(list).not.toHaveClass('hidden');

    // list should be closed
    toggleListByButtonClick();
    expect(list).toHaveClass('hidden');
  });

  it('should not show list when closed (default state)', () => {
    renderComponent(getState());
    const list = screen.getByTestId('download-submap-list');
    expect(list).toHaveClass('hidden');
  });

  it('should render list elements with href and names when opened', () => {
    const getSubmapDownloadUrl = getUtilGetSubmapDownloadUrl();
    renderComponent(getState());
    const list = screen.getByTestId('download-submap-list');

    const validHrefs = CONFIGURATION_FORMATS_MOCK.map(({ handler }) =>
      getSubmapDownloadUrl({ handler, modelId: VALID_MODEL_ID }),
    );
    const validNames = CONFIGURATION_FORMATS_TYPES_MOCK;
    const allAnchors = [...list.getElementsByTagName('a')];

    allAnchors.forEach(anchor => {
      expect(validHrefs.includes(anchor.href)).toBeTruthy();
      expect(validNames.includes(anchor.innerText)).toBeTruthy();
    });
  });
});

import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { openedDrawerSubmapsFixture } from '@/redux/drawer/drawerFixture';
import {
  initialMapDataFixture,
  openedMapsInitialValueFixture,
  openedMapsThreeSubmapsFixture,
} from '@/redux/map/map.fixtures';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID } from '@/constants/mocks';
import { SubmapsDrawer } from './SubmapsDrawer';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <SubmapsDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('SubmapsDrawer - component', () => {
  it('should display drawer heading and list of submaps', () => {
    renderComponent({
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
    });

    expect(screen.getByText('Core PD map')).toBeInTheDocument();
    expect(screen.getByText('Histamine signaling')).toBeInTheDocument();
    expect(screen.getByText('PRKN substrates')).toBeInTheDocument();
  });
  it('should close drawer after clicking close button', () => {
    const { store } = renderComponent({
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
      drawer: openedDrawerSubmapsFixture,
    });
    const closeButton = screen.getByRole('close-drawer-button');

    closeButton.click();

    const {
      drawer: { isOpen },
    } = store.getState();

    expect(isOpen).toBe(false);
  });
  it("should open submap and set it to active if it's not already opened", async () => {
    const { store } = renderComponent({
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
      map: {
        data: initialMapDataFixture,
        loading: 'succeeded',
        error: { name: '', message: '' },
        openedMaps: openedMapsInitialValueFixture,
      },
    });

    const {
      data: { modelId },
      openedMaps,
    } = store.getState().map;

    // eslint-disable-next-line no-magic-numbers
    expect(modelId).toBe(0);
    expect(openedMaps).not.toContainEqual({
      modelId: HISTAMINE_MAP_ID,
      modelName: 'Histamine signaling',
      lastPosition: { x: 0, y: 0, z: 0 },
    });

    const openHistamineMapButton = screen.getByTestId('Histamine signaling-open');
    await act(() => {
      openHistamineMapButton.click();
    });

    const {
      data: { modelId: newModelId },
      openedMaps: newOpenedMaps,
    } = store.getState().map;

    expect(newOpenedMaps).toContainEqual({
      modelId: HISTAMINE_MAP_ID,
      modelName: 'Histamine signaling',
      lastPosition: { x: 0, y: 0, z: 0 },
    });

    expect(newModelId).toBe(HISTAMINE_MAP_ID);
  });
  it("should set map active if it's already opened", async () => {
    const { store } = renderComponent({
      models: { data: MODELS_MOCK_SHORT, loading: 'succeeded', error: { name: '', message: '' } },
      map: {
        data: {
          ...initialMapDataFixture,
          modelId: MAIN_MAP_ID,
        },
        openedMaps: openedMapsThreeSubmapsFixture,
        loading: 'succeeded',
        error: { name: '', message: '' },
      },
    });

    const openHistamineMapButton = screen.getByTestId('Histamine signaling-open');
    await act(() => {
      openHistamineMapButton.click();
    });

    const {
      map: {
        data: { modelId },
        openedMaps,
      },
    } = store.getState();

    const histamineMap = openedMaps.filter(map => map.modelName === 'Histamine signaling');

    // eslint-disable-next-line no-magic-numbers
    expect(histamineMap.length).toBe(1);
    expect(modelId).toBe(HISTAMINE_MAP_ID);
  });
});

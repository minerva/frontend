/* eslint-disable no-magic-numbers */
import { pluginFixture } from '@/models/fixtures/pluginFixture';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { PluginsManager } from '@/services/pluginsManager';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook, waitFor } from '@testing-library/react';
import axios, { HttpStatusCode } from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import { showToast } from '@/utils/showToast';
import { LEGEND_INITIAL_STATE_MOCK } from '@/redux/legend/legend.mock';
import { useLoadPlugin } from './useLoadPlugin';

const mockedAxiosClient = new MockAdapter(axios);
jest.mock('../../../../../../services/pluginsManager/pluginsManager');
jest.mock('../../../../../../utils/showToast');

const STATE_MOCK = {
  ...INITIAL_STORE_STATE_MOCK,
  plugins: {
    ...INITIAL_STORE_STATE_MOCK.plugins,
    activePlugins: {
      pluginsId: [pluginFixture.hash],
      data: {
        [pluginFixture.hash]: pluginFixture,
      },
    },
    list: INITIAL_STORE_STATE_MOCK.plugins.list,
  },
  legend: {
    ...LEGEND_INITIAL_STATE_MOCK,
    pluginLegend: {
      [pluginFixture.hash]: ['url1', 'url2'],
    },
  },
};

describe('useLoadPlugin', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('should load plugin successfully', async () => {
    const hash = 'pluginHash';
    const pluginUrl = 'http://example.com/plugin.js';
    Math.max = jest.fn();
    const { Wrapper } = getReduxStoreWithActionsListener(INITIAL_STORE_STATE_MOCK);
    const pluginScript = `function init() {${Math.max(1, 2)}} init()`;

    mockedAxiosClient.onGet(pluginUrl).reply(HttpStatusCode.Ok, pluginScript);

    const {
      result: {
        current: { isPluginActive, isPluginLoading, togglePlugin },
      },
    } = renderHook(() => useLoadPlugin({ hash, pluginUrl }), {
      wrapper: Wrapper,
    });

    expect(isPluginActive).toBe(false);
    expect(isPluginLoading).toBe(false);

    togglePlugin();

    expect(Math.max).toHaveBeenCalledWith(1, 2);

    await waitFor(() => {
      expect(PluginsManager.setHashedPlugin).toHaveBeenCalledWith({
        pluginScript,
        pluginUrl,
      });
    });
  });
  it('should show toast if plugin failed to load', async () => {
    const hash = 'pluginHash';
    const pluginUrl = 'http://example.com/plugin.js';

    const { Wrapper } = getReduxStoreWithActionsListener(INITIAL_STORE_STATE_MOCK);

    mockedAxiosClient.onGet(pluginUrl).reply(HttpStatusCode.Forbidden, null);

    const {
      result: {
        current: { togglePlugin },
      },
    } = renderHook(() => useLoadPlugin({ hash, pluginUrl }), {
      wrapper: Wrapper,
    });

    togglePlugin();

    await waitFor(() => {
      expect(showToast).toHaveBeenCalled();
      expect(showToast).toHaveBeenCalledWith({
        message:
          "Failed to load plugin: Access Forbidden! You don't have permission to access this resource.",
        type: 'error',
      });
    });
  });
  describe('when unload plugin', () => {
    it('should unload plugin successfully', async () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener(STATE_MOCK);

      const {
        result: {
          current: { isPluginActive, isPluginLoading, togglePlugin },
        },
      } = renderHook(
        () => useLoadPlugin({ hash: pluginFixture.hash, pluginUrl: pluginFixture.urls[0] }),
        {
          wrapper: Wrapper,
        },
      );

      expect(isPluginActive).toBe(true);
      expect(isPluginLoading).toBe(false);

      act(() => {
        togglePlugin();
      });

      const actions = store.getActions();
      expect(actions[0]).toEqual({
        payload: { pluginId: pluginFixture.hash },
        type: 'plugins/removePlugin',
      });
    });
    it('should remove plugin legend', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener(STATE_MOCK);

      const {
        result: {
          current: { togglePlugin },
        },
      } = renderHook(
        () => useLoadPlugin({ hash: pluginFixture.hash, pluginUrl: pluginFixture.urls[0] }),
        {
          wrapper: Wrapper,
        },
      );

      act(() => {
        togglePlugin();
      });

      const actions = store.getActions();

      expect(actions).toEqual([
        {
          payload: { pluginId: pluginFixture.hash },
          type: 'plugins/removePlugin',
        },
        {
          payload: pluginFixture.hash,
          type: 'legend/removePluginLegend',
        },
      ]);
    });

    it('should set active legend to main legend if plugin legend was active one', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...STATE_MOCK,
        legend: {
          ...STATE_MOCK.legend,
          activeLegendId: pluginFixture.hash,
        },
      });

      const {
        result: {
          current: { togglePlugin },
        },
      } = renderHook(
        () => useLoadPlugin({ hash: pluginFixture.hash, pluginUrl: pluginFixture.urls[0] }),
        {
          wrapper: Wrapper,
        },
      );

      act(() => {
        togglePlugin();
      });

      const actions = store.getActions();

      expect(actions).toEqual([
        {
          payload: { pluginId: pluginFixture.hash },
          type: 'plugins/removePlugin',
        },
        {
          payload: undefined,
          type: 'legend/setDefaultLegendId',
        },
        {
          payload: pluginFixture.hash,
          type: 'legend/removePluginLegend',
        },
      ]);
    });
    it('should unload plugin from plugins manager', () => {
      const unloadActivePluginSpy = jest.spyOn(PluginsManager, 'unloadActivePlugin');
      const { Wrapper } = getReduxStoreWithActionsListener(STATE_MOCK);
      PluginsManager.activePlugins = {
        [pluginFixture.hash]: [pluginFixture.hash],
      };

      const {
        result: {
          current: { togglePlugin },
        },
      } = renderHook(
        () => useLoadPlugin({ hash: pluginFixture.hash, pluginUrl: pluginFixture.urls[0] }),
        {
          wrapper: Wrapper,
        },
      );

      act(() => {
        togglePlugin();
      });

      expect(unloadActivePluginSpy).toHaveBeenCalledWith(pluginFixture.hash);
    });
    it('should set last active plugin as current plugin tab in drawer', () => {
      const firstHash = 'hash1';
      const secondHash = 'hash2';

      const { Wrapper, store } = getReduxStoreWithActionsListener({
        ...STATE_MOCK,
        plugins: {
          ...STATE_MOCK.plugins,
          activePlugins: {
            data: {
              [firstHash]: {
                ...pluginFixture,
                hash: firstHash,
              },
              [secondHash]: {
                ...pluginFixture,
                hash: secondHash,
              },
            },
            pluginsId: [firstHash, secondHash],
          },
        },
      });

      const {
        result: {
          current: { togglePlugin },
        },
      } = renderHook(() => useLoadPlugin({ hash: secondHash, pluginUrl: pluginFixture.urls[0] }), {
        wrapper: Wrapper,
      });

      act(() => {
        togglePlugin();
      });

      const actions = store.getActions();

      expect(actions).toEqual([
        {
          payload: { pluginId: secondHash },
          type: 'plugins/removePlugin',
        },
        {
          payload: firstHash,
          type: 'plugins/setCurrentDrawerPluginHash',
        },
        {
          payload: secondHash,
          type: 'legend/removePluginLegend',
        },
      ]);
    });
  });
});

import { publicPluginsListSelector } from '@/redux/plugins/plugins.selectors';
import React from 'react';
import { useSelector } from 'react-redux';
import { LoadPlugin } from '../LoadPlugin';

export const PublicPlugins = (): React.ReactNode => {
  const publicPlugins = useSelector(publicPluginsListSelector);
  return publicPlugins.map(plugin => <LoadPlugin key={plugin.hash} plugin={plugin} />);
};

import { privateActivePluginsSelector } from '@/redux/plugins/plugins.selectors';
import { useSelector } from 'react-redux';
import { LoadPlugin } from '../LoadPlugin';

export const PrivateActivePlugins = (): React.ReactNode => {
  const privateActivePlugins = useSelector(privateActivePluginsSelector);
  return privateActivePlugins.map(plugin => <LoadPlugin key={plugin.hash} plugin={plugin} />);
};

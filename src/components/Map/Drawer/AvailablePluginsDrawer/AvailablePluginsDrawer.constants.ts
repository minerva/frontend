export const PLUGIN_LOADING_ERROR_PREFIX = 'Failed to load plugin';
export const PLUGIN_LOADING_TWICE_ERROR = 'Plugin cannot be loaded more than once';

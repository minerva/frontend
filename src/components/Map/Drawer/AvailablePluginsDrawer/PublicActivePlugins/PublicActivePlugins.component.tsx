import { publicActivePluginsSelector } from '@/redux/plugins/plugins.selectors';
import { useSelector } from 'react-redux';
import { LoadPlugin } from '../LoadPlugin';

export const PublicActivePlugins = (): React.ReactNode => {
  const publicActivePlugins = useSelector(publicActivePluginsSelector);
  return publicActivePlugins.map(plugin => <LoadPlugin key={plugin.hash} plugin={plugin} />);
};

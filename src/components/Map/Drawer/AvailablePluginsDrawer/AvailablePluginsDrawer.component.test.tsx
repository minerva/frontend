import { PLUGINS_MOCK } from '@/models/mocks/pluginsMock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { AvailablePluginsDrawer } from './AvailablePluginsDrawer.component';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <AvailablePluginsDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('AvailablePluginsDrawer - component', () => {
  describe('when always', () => {
    it('should render drawer heading', () => {
      renderComponent(INITIAL_STORE_STATE_MOCK);
      const drawerTitle = screen.getByText('Available plugins');
      expect(drawerTitle).toBeInTheDocument();
    });

    it('should render load plugin from url', () => {
      renderComponent(INITIAL_STORE_STATE_MOCK);
      const loadPluginFromUrlInput = screen.getByTestId('load-plugin-input-url');
      expect(loadPluginFromUrlInput).toBeInTheDocument();
    });

    it.each(PLUGINS_MOCK.filter(p => p.isPublic))(
      'should render render all public plugins',
      currentPlugin => {
        renderComponent({
          ...INITIAL_STORE_STATE_MOCK,
          plugins: {
            ...INITIAL_STORE_STATE_MOCK.plugins,
            list: {
              ...INITIAL_STORE_STATE_MOCK.plugins.list,
              data: PLUGINS_MOCK,
            },
          },
        });

        const pluginLabel = screen.getByText(`${currentPlugin.name} (${currentPlugin.version})`);
        expect(pluginLabel).toBeInTheDocument();
      },
    );
  });
});

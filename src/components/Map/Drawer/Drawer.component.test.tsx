import {
  openBioEntityDrawerById,
  openReactionDrawerById,
  openSearchDrawerWithSelectedTab,
  openSubmapsDrawer,
} from '@/redux/drawer/drawer.slice';
import { getReactionsByIds } from '@/redux/reactions/reactions.thunks';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';
import { Drawer } from './Drawer.component';
import type {} from 'redux-thunk/extend-redux';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <Drawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('Drawer - component', () => {
  it('should render Drawer', () => {
    renderComponent();

    expect(screen.getByRole('drawer')).toBeInTheDocument();
  });

  it('should not display drawer when its not open', () => {
    renderComponent();

    expect(screen.getByRole('drawer')).not.toHaveClass('translate-x-0');
  });

  describe('search drawer ', () => {
    it('should open drawer and display search drawer content', async () => {
      const { store } = renderComponent();

      expect(screen.queryByTestId('search-drawer-content')).not.toBeInTheDocument();

      await act(() => {
        store.dispatch(openSearchDrawerWithSelectedTab(''));
      });

      expect(screen.getByTestId('search-drawer-content')).toBeInTheDocument();
    });

    it('should close drawer after pressing close button', async () => {
      const { store } = renderComponent();

      await act(() => {
        store.dispatch(openSearchDrawerWithSelectedTab(''));
      });

      expect(screen.getByTestId('search-drawer-content')).toBeInTheDocument();

      const button = screen.getByRole('close-drawer-button');

      await act(() => {
        fireEvent.click(button);
      });

      expect(screen.getByRole('drawer')).not.toHaveClass('translate-x-0');
      expect(screen.queryByTestId('search-drawer-content')).not.toBeInTheDocument();
    });
  });

  describe('submap drawer', () => {
    it('should open drawer and display submaps', async () => {
      const { store } = renderComponent();

      expect(screen.queryByTestId('submap-drawer')).not.toBeInTheDocument();

      await act(() => {
        store.dispatch(openSubmapsDrawer());
      });

      expect(screen.getByTestId('submap-drawer')).toBeInTheDocument();
    });
  });

  describe('reaction drawer', () => {
    it('should open drawer and display reaction', async () => {
      const { id, model } = newReactionFixture;

      const { store } = renderComponent({
        reactions: {
          data: [newReactionFixture],
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
        newReactions: {
          0: {
            data: [newReactionFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
      });

      expect(screen.queryByTestId('reaction-drawer')).not.toBeInTheDocument();
      await act(() => {
        store.dispatch(getReactionsByIds({ ids: [{ id, modelId: model }] }));
        store.dispatch(openReactionDrawerById(id));
      });
      await waitFor(() => expect(screen.getByTestId('reaction-drawer')).toBeInTheDocument());
    });
  });

  describe('bioEntity drawer', () => {
    it.skip('should open drawer and display bioEntity', async () => {
      const { id } = modelElementFixture;

      const { store } = renderComponent({
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
      });

      expect(screen.queryByTestId('bioentity-drawer')).not.toBeInTheDocument();

      await act(() => {
        store.dispatch(openBioEntityDrawerById(id));
      });

      await waitFor(() => expect(screen.getByTestId('bioentity-drawer')).toBeInTheDocument());
    });
  });
});

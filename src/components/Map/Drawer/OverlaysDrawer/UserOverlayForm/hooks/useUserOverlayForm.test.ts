import { renderHook } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { ChangeEvent } from 'react';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { useUserOverlayForm } from './useUserOverlayForm';

describe('useUserOverlayForm', () => {
  it('should update state when form fields are changed', () => {
    const { Wrapper } = getReduxWrapperWithStore();
    const { result } = renderHook(() => useUserOverlayForm(), { wrapper: Wrapper });

    act(() => {
      result.current.handleChangeType({ id: '1', label: 'Test Type' });
      result.current.handleChangeGroup(overlayGroupFixture);
    });

    expect(result.current.type).toEqual({ id: '1', label: 'Test Type' });
    expect(result.current.group).toEqual(overlayGroupFixture);
  });

  it('should update overlayContent when handleChangeOverlayContent is called', () => {
    const { Wrapper } = getReduxWrapperWithStore();
    const { result } = renderHook(() => useUserOverlayForm(), { wrapper: Wrapper });

    act(() => {
      result.current.handleChangeOverlayContent('Test Overlay Content');
    });

    expect(result.current.overlayContent).toBe('Test Overlay Content');
  });
  it('should update elementsList and overlayContent when handleChangeElementsList is called', () => {
    const { Wrapper } = getReduxWrapperWithStore();
    const { result } = renderHook(() => useUserOverlayForm(), { wrapper: Wrapper });

    act(() => {
      result.current.handleChangeElementsList({
        target: { value: 'Test Elements List' },
      } as ChangeEvent<HTMLTextAreaElement>);
    });

    expect(result.current.elementsList).toBe('Test Elements List');
    expect(result.current.overlayContent).toBe('Test Elements List');
  });
  it('should update state variables based on updateUserOverlayForm', () => {
    const { Wrapper } = getReduxWrapperWithStore();
    const { result } = renderHook(() => useUserOverlayForm(), { wrapper: Wrapper });

    act(() => {
      result.current.updateUserOverlayForm('NAME', 'Test Name');
    });

    expect(result.current.name).toBe('Test Name');

    act(() => {
      result.current.updateUserOverlayForm('DESCRIPTION', 'Test Description');
    });
    expect(result.current.description).toBe('Test Description');
  });
});

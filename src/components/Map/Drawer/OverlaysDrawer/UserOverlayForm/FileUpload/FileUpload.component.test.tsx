import React from 'react';
import { render, screen } from '@testing-library/react';
import { FileUpload } from './FileUpload.component';

describe('FileUpload component', () => {
  const handleChangeFile = jest.fn();
  const handleChangeOverlayContent = jest.fn();
  const handleOverlayChange = jest.fn();
  const uploadedFile = new File(['file content'], 'test.txt', {
    type: 'text/plain',
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('renders correctly with default state', () => {
    render(
      <FileUpload
        handleChangeFile={handleChangeFile}
        handleChangeOverlayContent={handleChangeOverlayContent}
        updateUserOverlayForm={handleOverlayChange}
        uploadedFile={null}
      />,
    );

    expect(screen.getByText(/drag and drop here or/i)).toBeInTheDocument();
  });

  it('renders filename when file type is correct', () => {
    render(
      <FileUpload
        handleChangeFile={handleChangeFile}
        handleChangeOverlayContent={handleChangeOverlayContent}
        updateUserOverlayForm={handleOverlayChange}
        uploadedFile={uploadedFile}
      />,
    );

    expect(screen.getByText(/test.txt/i)).toBeInTheDocument();
  });
});

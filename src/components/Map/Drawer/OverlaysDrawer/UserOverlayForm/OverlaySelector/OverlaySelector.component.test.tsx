/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { OverlaySelector } from './OverlaySelector.component';
import { SelectorItem } from '../UserOverlayForm.types';

const items: SelectorItem[] = [
  { id: '1', label: 'Item 1' },
  { id: '2', label: 'Item 2' },
  { id: '3', label: 'Item 3' },
];

const onChangeMock = jest.fn();

describe('OverlaySelector component', () => {
  it('renders the component with initial values', () => {
    const label = 'Select an item';
    const value = items[0];

    render(<OverlaySelector items={items} value={value} onChange={onChangeMock} label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();

    expect(screen.getByTestId('selector-dropdown-button-name')).toHaveTextContent(value.label);
  });

  it('opens the dropdown and selects an item', () => {
    const label = 'Select an item';
    const value = items[0];

    render(<OverlaySelector items={items} value={value} onChange={onChangeMock} label={label} />);

    fireEvent.click(screen.getByTestId('selector-dropdown-button-name'));

    expect(screen.getByRole('listbox')).toBeInTheDocument();

    const selectedItem = items[1];
    const firstItem = screen.getByText(selectedItem.label);

    fireEvent.click(firstItem);

    waitFor(() => {
      expect(screen.queryByRole('listbox')).not.toBeInTheDocument();
    });

    expect(onChangeMock).toHaveBeenCalledWith(selectedItem);
  });
});

/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { OverlayGroup } from '@/types/models';
import { OverlayGroupSelector } from './OverlayGroupSelector.component';

const items: OverlayGroup[] = [
  { id: 1, name: 'Item 1', order: 1 },
  { id: 2, name: 'Item 2', order: 2 },
  { id: 3, name: 'Item 3', order: 3 },
];

const onChangeMock = jest.fn();

describe('OverlayGroupSelector component', () => {
  it('renders the component with initial values', () => {
    const label = 'Select an item';
    const value = items[0];

    render(
      <OverlayGroupSelector items={items} value={value} onChange={onChangeMock} label={label} />,
    );

    expect(screen.getByText(label)).toBeInTheDocument();

    expect(screen.getByTestId('selector-dropdown-button-name')).toHaveTextContent(value.name);
  });

  it('opens the dropdown and selects an item', () => {
    const label = 'Select an item';
    const value = items[0];

    render(
      <OverlayGroupSelector items={items} value={value} onChange={onChangeMock} label={label} />,
    );

    fireEvent.click(screen.getByTestId('selector-dropdown-button-name'));

    expect(screen.getByRole('listbox')).toBeInTheDocument();

    const selectedItem = items[1];
    const firstItem = screen.getByText(selectedItem.name);

    fireEvent.click(firstItem);

    waitFor(() => {
      expect(screen.queryByRole('listbox')).not.toBeInTheDocument();
    });

    expect(onChangeMock).toHaveBeenCalledWith(selectedItem);
  });
});

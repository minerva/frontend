/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { projectFixture } from '@/models/fixtures/projectFixture';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { AppDispatch, RootState, StoreType } from '@/redux/store';
import { drawerOverlaysStepOneFixture } from '@/redux/drawer/drawerFixture';
import { MockStoreEnhanced } from 'redux-mock-store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { mockNetworkNewAPIResponse, mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { apiPath } from '@/redux/apiPath';
import {
  createdOverlayFileFixture,
  overlayFixture,
  overlaysPageFixture,
  uploadedOverlayFileContentFixture,
} from '@/models/fixtures/overlaysFixture';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { DEFAULT_ERROR } from '@/constants/errors';
import { showToast } from '@/utils/showToast';
import { UserOverlayForm } from './UserOverlayForm.component';

jest.mock('../../../../../utils/showToast');

const mockedAxiosClient = mockNetworkResponse();
const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <UserOverlayForm />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithActionListener = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <UserOverlayForm />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const testOverlaysPageFixture = {
  ...overlaysPageFixture,
  content: [
    {
      ...overlaysPageFixture.content[0],
      publicOverlay: false,
    },
  ],
};

describe('UserOverlayForm - Component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('renders the UserOverlayForm component', () => {
    renderComponent();

    expect(screen.getByTestId('overlay-name')).toBeInTheDocument();
    expect(screen.getByLabelText('upload overlay')).toBeInTheDocument();
  });

  it('should submit the form with elements list when upload button is clicked', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(createdOverlayFileFixture.id))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosClient
      .onPost(apiPath.createOverlay(projectFixture.projectId))
      .reply(HttpStatusCode.Ok, overlayFixture);

    renderComponent({
      project: {
        data: projectFixture,
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    fireEvent.change(screen.getByTestId('overlay-name'), { target: { value: 'Test Overlay' } });
    fireEvent.change(screen.getByTestId('overlay-description'), {
      target: { value: 'Description Overlay' },
    });
    fireEvent.change(screen.getByTestId('overlay-elements-list'), {
      target: { value: 'Elements List Overlay' },
    });

    fireEvent.click(screen.getByLabelText('upload overlay'));

    expect(screen.getByLabelText('upload overlay')).toBeDisabled();
  });

  it('should create correct name for file which contains elements list as content', async () => {
    const { store } = renderComponentWithActionListener({
      project: {
        data: projectFixture,
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    fireEvent.change(screen.getByTestId('overlay-name'), { target: { value: 'Test Overlay' } });
    fireEvent.change(screen.getByTestId('overlay-description'), {
      target: { value: 'Description Overlay' },
    });
    fireEvent.change(screen.getByTestId('overlay-elements-list'), {
      target: { value: 'Elements List Overlay' },
    });

    const actions = store.getActions();
    fireEvent.click(screen.getByLabelText('upload overlay'));

    expect(actions[0].meta.arg.filename).toBe('unknown.txt');
  });

  it('should update the form inputs based on overlay content provided by elements list', async () => {
    renderComponent({
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    fireEvent.change(screen.getByTestId('overlay-name'), { target: { value: 'Test Overlay' } });
    fireEvent.change(screen.getByTestId('overlay-description'), {
      target: { value: 'Description Overlay' },
    });

    fireEvent.change(screen.getByTestId('overlay-elements-list'), {
      target: {
        value: '#NAME = John\n# DESCRIPTION = Some description\n# TYPE = GENETIC_VARIANT\n',
      },
    });

    expect(screen.getByTestId('overlay-name')).toHaveValue('John');
    expect(screen.getByTestId('overlay-description')).toHaveValue('Some description');
    expect(screen.getByText('GENETIC_VARIANT')).toBeVisible();
  });

  it('should display correct filename', async () => {
    const uploadedFile = new File(['file content'], 'test.txt', {
      type: 'text/plain',
    });
    renderComponent({
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    fireEvent.change(screen.getByTestId('dropzone-input'), {
      target: { files: [uploadedFile] },
    });

    const dropzone: HTMLInputElement = screen.getByTestId('dropzone-input');
    expect(dropzone?.files?.[0].name).toBe('test.txt');
  });

  it('should not submit when form is not filled', async () => {
    renderComponent({
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    expect(screen.getByTestId('overlay-description')).toHaveValue('');
    fireEvent.click(screen.getByLabelText('upload overlay'));
    expect(screen.getByLabelText('upload overlay')).not.toBeDisabled();
  });
  it('should navigate to overlays after clicking backward button', async () => {
    const { store } = renderComponent({
      drawer: drawerOverlaysStepOneFixture,
      project: {
        data: projectFixture,
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    const backButton = screen.getByRole('back-button');

    backButton.click();

    const {
      drawer: {
        overlayDrawerState: { currentStep },
      },
    } = store.getState();

    expect(currentStep).toBe(1);
  });
  it('should refetch user overlays after submit the form', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(createdOverlayFileFixture.id))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosNewClient
      .onPost(apiPath.createOverlay(projectFixture.projectId))
      .reply(HttpStatusCode.Ok, overlayFixture);

    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent({
      user: {
        authenticated: true,
        error: DEFAULT_ERROR,
        login: 'test',
        loading: 'succeeded',
        role: 'user',
        userData: null,
        token: null,
      },
      project: {
        data: projectFixture,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        projectId: '',
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    const userOverlays = store.getState().overlays.userOverlays.data;

    expect(userOverlays).toEqual([]);

    fireEvent.change(screen.getByTestId('overlay-name'), { target: { value: 'Test Overlay' } });
    fireEvent.change(screen.getByTestId('overlay-description'), {
      target: { value: 'Description Overlay' },
    });
    fireEvent.change(screen.getByTestId('overlay-elements-list'), {
      target: { value: 'Elements List Overlay' },
    });

    fireEvent.click(screen.getByLabelText('upload overlay'));

    expect(screen.getByLabelText('upload overlay')).toBeDisabled();

    await waitFor(() => {
      expect(store.getState().overlays.userOverlays.loading).toBe('succeeded');
    });

    const refetchedUserOverlays = store.getState().overlays.userOverlays.data;

    await waitFor(() => {
      // eslint-disable-next-line no-console
      console.log(refetchedUserOverlays);
      // eslint-disable-next-line no-console
      console.log(testOverlaysPageFixture.content);

      expect(refetchedUserOverlays).toEqual(testOverlaysPageFixture.content);
    });
  });
  it('should show toast after successful creating user overlays', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(createdOverlayFileFixture.id))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosNewClient
      .onPost(apiPath.createOverlay(projectFixture.projectId))
      .reply(HttpStatusCode.Ok, overlayFixture);

    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent({
      user: {
        authenticated: true,
        error: DEFAULT_ERROR,
        login: 'test',
        loading: 'succeeded',
        role: 'user',
        userData: null,
        token: null,
      },
      project: {
        data: projectFixture,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        projectId: '',
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    const userOverlays = store.getState().overlays.userOverlays.data;

    expect(userOverlays).toEqual([]);

    fireEvent.change(screen.getByTestId('overlay-name'), { target: { value: 'Test Overlay' } });
    fireEvent.change(screen.getByTestId('overlay-description'), {
      target: { value: 'Description Overlay' },
    });
    fireEvent.change(screen.getByTestId('overlay-elements-list'), {
      target: { value: 'Elements List Overlay' },
    });

    fireEvent.click(screen.getByLabelText('upload overlay'));

    await waitFor(() => {
      expect(showToast).toHaveBeenCalled();
      expect(showToast).toHaveBeenCalledWith({
        message: 'User overlay added successfully',
        type: 'success',
      });
    });
  });
});

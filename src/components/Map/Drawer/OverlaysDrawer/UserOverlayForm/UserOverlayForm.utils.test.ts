/* eslint-disable no-magic-numbers */
import { processOverlayContentChange } from './UserOverlayForm.utils';

const handleOverlayChange = jest.fn();

describe('processOverlayContentChange', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should parse overlay file content and invoke the handleOverlayChange callback for valid lines', () => {
    const fileContent = `#NAME = John\n# DESCRIPTION = Some description\n# TYPE = Type1\n`;

    processOverlayContentChange(fileContent, handleOverlayChange);

    expect(handleOverlayChange).toHaveBeenCalledTimes(3);
    expect(handleOverlayChange).toHaveBeenCalledWith('NAME', 'John');
    expect(handleOverlayChange).toHaveBeenCalledWith('DESCRIPTION', 'Some description');
    expect(handleOverlayChange).toHaveBeenCalledWith('TYPE', 'Type1');
  });

  it('should handle lines with invalid format without calling handleOverlayChange', () => {
    const fileContent = `InvalidLine1\n#InvalidLine2\n=InvalidLine3\n`;

    processOverlayContentChange(fileContent, handleOverlayChange);

    expect(handleOverlayChange).not.toHaveBeenCalled();
  });
});

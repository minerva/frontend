import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
  PUBLIC_OVERLAYS_MOCK,
} from '@/redux/overlays/overlays.mock';
import { GeneralOverlays } from './GeneralOverlays.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <GeneralOverlays />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('GeneralOverlays - component', () => {
  describe('render', () => {
    const publicOverlaysNames = PUBLIC_OVERLAYS_MOCK.map(({ name }) => name);

    it.each(publicOverlaysNames)('should display %s overlay item', source => {
      renderComponent({ overlays: OVERLAYS_PUBLIC_FETCHED_STATE_MOCK });

      expect(screen.getByText(source)).toBeInTheDocument();
    });
  });

  describe('view overlays', () => {
    // TODO implement when connecting logic to component
    it.skip('should allow to turn on more then one overlay', () => {});
  });
});

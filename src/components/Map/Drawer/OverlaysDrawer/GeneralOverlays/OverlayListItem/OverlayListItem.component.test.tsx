import { StoreType } from '@/redux/store';
import { act, render, screen } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import { HttpStatusCode } from 'axios';
import { overlayBioEntityFixture } from '@/models/fixtures/overlayBioEntityFixture';
import { apiPath } from '@/redux/apiPath';
import { CORE_PD_MODEL_MOCK } from '@/models/mocks/modelsMock';
import { MODELS_INITIAL_STATE_MOCK } from '@/redux/models/models.mock';
import { parseOverlayBioEntityToOlRenderingFormat } from '@/redux/overlayBioEntity/overlayBioEntity.utils';
import { BASE_API_URL } from '@/constants';
import { MAIN_MAP_ID } from '@/constants/mocks';
import MapBackgroundsEnum from '@/redux/map/map.enums';
import { OverlayListItem } from './OverlayListItem.component';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();
const DEFAULT_BACKGROUND_ID = 2;

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <OverlayListItem name="Ageing brain" overlayId={21} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('OverlayListItem - component', () => {
  it('should render component with correct properties', () => {
    renderComponent();

    expect(screen.getByText('Ageing brain')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'View' })).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Download' })).toBeInTheDocument();
  });

  describe('view overlays', () => {
    it('should trigger view overlays on view button click and switch background to Network', async () => {
      const OVERLAY_ID = 21;
      const { store } = renderComponent({
        map: initialMapStateFixture,
        overlayBioEntity: OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
        models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
      });
      mockedAxiosNewClient
        .onGet(apiPath.getOverlayBioEntity({ overlayId: OVERLAY_ID, modelId: MAIN_MAP_ID }))
        .reply(HttpStatusCode.Ok, overlayBioEntityFixture);

      expect(store.getState().map.data.backgroundId).toBe(DEFAULT_BACKGROUND_ID);

      const ViewButton = screen.getByRole('button', { name: 'View' });
      await act(() => {
        ViewButton.click();
      });

      expect(store.getState().map.data.backgroundId).toBe(MapBackgroundsEnum.NETWORK);
      expect(store.getState().overlayBioEntity.data).toEqual({
        [OVERLAY_ID]: {
          [MAIN_MAP_ID]: parseOverlayBioEntityToOlRenderingFormat(
            overlayBioEntityFixture,
            OVERLAY_ID,
          ),
        },
      });
    });
    it('should hide overlay on view button click if overlay is active', async () => {
      const OVERLAY_ID = 21;
      const { store } = renderComponent({
        map: {
          ...initialMapStateFixture,
          data: { ...initialMapStateFixture.data, backgroundId: MapBackgroundsEnum.NETWORK },
        },
        overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
        models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
      });

      const ViewButton = screen.getByRole('button', { name: 'Hide' });
      await act(() => {
        ViewButton.click();
      });

      expect(store.getState().overlayBioEntity.data).toEqual([]);
      expect(store.getState().overlayBioEntity.overlaysId).toEqual([]);
    });
    it('should display spinner icon if overlay is loading', () => {
      const OVERLAY_ID = 21;
      renderComponent({
        overlayBioEntity: {
          data: {
            [OVERLAY_ID]: {},
          },
          overlaysId: [OVERLAY_ID],
        },
      });

      expect(screen.getByAltText('spinner icon')).toBeVisible();
      expect(screen.getByText('Hide')).toBeVisible();
    });
  });

  it('should trigger download overlay to PC on download button click', () => {
    const OVERLAY_ID = 21;
    renderComponent({
      map: {
        ...initialMapStateFixture,
        data: { ...initialMapStateFixture.data, backgroundId: MapBackgroundsEnum.SEMANTIC },
      },
      overlayBioEntity: { ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK, overlaysId: [OVERLAY_ID] },
      models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
    });

    const downloadButton = screen.getByText('Download');

    expect(downloadButton).toBeVisible();

    const windowOpenMock = jest.spyOn(window, 'open').mockImplementation();

    downloadButton.click();

    expect(windowOpenMock).toHaveBeenCalledWith(
      `${BASE_API_URL}/${apiPath.downloadOverlay(OVERLAY_ID)}`,
      '_blank',
    );
  });
});

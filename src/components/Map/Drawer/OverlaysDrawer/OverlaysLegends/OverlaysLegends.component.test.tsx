import { BASE_API_URL, PROJECT_ID } from '@/constants';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import {
  OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
  PUBLIC_OVERLAYS_MOCK,
} from '@/redux/overlays/overlays.mock';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { OverlaysLegends } from './OverlaysLegends.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <OverlaysLegends />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('OverlaysLegends - component', () => {
  describe('when active overlays are empty', () => {
    beforeEach(() => {
      renderComponent({
        overlays: OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
        overlayBioEntity: {
          ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
        },
      });
    });

    it('should not render list of overlays legends', () => {
      expect(screen.getByTestId('overlays-legends')).toBeEmptyDOMElement();
    });
  });

  describe('when active overlays are present', () => {
    beforeEach(() => {
      renderComponent({
        overlays: OVERLAYS_PUBLIC_FETCHED_STATE_MOCK,
        overlayBioEntity: {
          ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
          overlaysId: PUBLIC_OVERLAYS_MOCK.map(o => o.id),
        },
      });
    });

    it.each(PUBLIC_OVERLAYS_MOCK)('should render overlay legend', overlay => {
      const image = screen.getByAltText(`${overlay.name} legend`);

      expect(screen.getByText(overlay.name)).toBeInTheDocument();
      expect(image).toBeInTheDocument();
      expect(image.getAttribute('src')).toBe(
        `${BASE_API_URL}/projects/${PROJECT_ID}/overlays/${overlay.id}:downloadLegend`,
      );
    });
  });
});

import { BASE_API_URL, PROJECT_ID } from '@/constants';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { MapOverlay } from '@/types/models';
import { render, screen } from '@testing-library/react';
import { OverlaySingleLegend } from './OverlaySingleLegend.component';

const renderComponent = ({ overlay }: { overlay: MapOverlay }): void => {
  render(<OverlaySingleLegend overlay={overlay} />);
};

describe('OverlaySingleLegend - component', () => {
  beforeEach(() => {
    renderComponent({
      overlay: {
        ...overlayFixture,
        name: 'overlay name',
        id: 1234,
      },
    });
  });

  it('should render title with overlay name', () => {
    expect(screen.getByText('overlay name')).toBeInTheDocument();
  });

  it('should render image with valid src and alt', () => {
    const image = screen.getByAltText('overlay name legend');

    expect(image).toBeInTheDocument();
    expect(image.getAttribute('src')).toBe(
      `${BASE_API_URL}/projects/${PROJECT_ID}/overlays/1234:downloadLegend`,
    );
  });
});

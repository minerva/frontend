/* eslint-disable no-magic-numbers */
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { DEFAULT_ERROR } from '@/constants/errors';
import { overlayFixture, overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { act } from 'react-dom/test-utils';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { useUserOverlays } from './useUserOverlays';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

describe('useUserOverlays', () => {
  it('should not fetch user overlays on mount if login does not exist', async () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: false,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });

    const {
      result: {
        current: { userOverlaysList },
      },
    } = renderHook(
      () => useUserOverlays(OVERLAYS_INITIAL_STATE_MOCK.userOverlays.data || [], null),
      {
        wrapper: Wrapper,
      },
    );

    const actions = store.getActions();
    const firstAction = actions[0];

    expect(firstAction).toBeUndefined();
    expect(userOverlaysList).toEqual([]);
  });
  it('should store fetched user overlays to userOverlaysList state', () => {
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          publicOverlay: false,
          creator: 'test',
        }),
      )
      .reply(HttpStatusCode.Ok, overlaysPageFixture);

    const { Wrapper } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: overlaysPageFixture.content,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const {
      result: {
        current: { userOverlaysList },
      },
    } = renderHook(() => useUserOverlays(overlaysPageFixture.content, null), {
      wrapper: Wrapper,
    });

    expect(userOverlaysList).toEqual(overlaysPageFixture.content);
  });
  it('should move user overlay list item on order change', async () => {
    const FIRST_USER_OVERLAY = overlayFixture;
    const SECOND_USER_OVERLAY = overlayFixture;

    const overlays = [FIRST_USER_OVERLAY, SECOND_USER_OVERLAY];
    const reversedOverlays = [SECOND_USER_OVERLAY, FIRST_USER_OVERLAY];

    const page = {
      ...overlaysPageFixture,
      data: overlays,
    };
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          publicOverlay: false,
          creator: 'test',
        }),
      )
      .reply(HttpStatusCode.Ok, page);

    const { Wrapper } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: overlays,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const {
      result: {
        current: { moveUserOverlayListItem, userOverlaysList },
      },
    } = renderHook(() => useUserOverlays(overlays, null), {
      wrapper: Wrapper,
    });

    await act(() => {
      moveUserOverlayListItem(0, 1);
    });

    expect(userOverlaysList).toEqual(reversedOverlays);
  });
  it('calls updateOverlays on calling updateUserOverlaysOrder', async () => {
    const FIRST_USER_OVERLAY = { ...overlayFixture, order: 1, id: 12 };
    const SECOND_USER_OVERLAY = { ...overlayFixture, order: 2, id: 92 };

    const page = {
      ...overlaysPageFixture,
      data: [FIRST_USER_OVERLAY, SECOND_USER_OVERLAY],
    };

    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          publicOverlay: false,
          creator: 'test',
        }),
      )
      .reply(HttpStatusCode.Ok, page);

    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: [FIRST_USER_OVERLAY, SECOND_USER_OVERLAY],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const originalOverlays = [FIRST_USER_OVERLAY, SECOND_USER_OVERLAY];

    const {
      result: {
        current: { moveUserOverlayListItem, updateUserOverlaysOrder },
      },
    } = renderHook(() => useUserOverlays(originalOverlays, null), {
      wrapper: Wrapper,
    });

    await act(() => {
      moveUserOverlayListItem(0, 1);
    });

    updateUserOverlaysOrder(FIRST_USER_OVERLAY, null);

    const actions = store.getActions();

    const firstAction = actions[0];
    expect(firstAction.type).toBe('overlays/updateOverlays/pending');
  });
});

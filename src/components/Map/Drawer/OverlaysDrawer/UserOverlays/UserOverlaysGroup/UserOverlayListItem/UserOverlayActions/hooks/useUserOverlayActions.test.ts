/* eslint-disable no-magic-numbers */
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { BASE_API_URL } from '@/constants';
import { apiPath } from '@/redux/apiPath';
import { useUserOverlayActions } from './useUserOverlayActions';

describe('useUserOverlayActions', () => {
  it('should handle handleActionClick based on edit action', async () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({});

    const {
      result: {
        current: { handleActionClick },
      },
    } = renderHook(() => useUserOverlayActions(overlayFixture), {
      wrapper: Wrapper,
    });

    await act(() => {
      handleActionClick('Edit');
    });

    const actions = store.getActions();

    const FIRST_ACTION = actions[0];

    expect(FIRST_ACTION.payload).toBe(overlayFixture);
    expect(FIRST_ACTION.type).toBe('modal/openEditOverlayModal');
  });
  it('should handle handleActionClick based on download action', async () => {
    const { Wrapper } = getReduxStoreWithActionsListener({});

    const {
      result: {
        current: { handleActionClick },
      },
    } = renderHook(() => useUserOverlayActions(overlayFixture), {
      wrapper: Wrapper,
    });

    const windowOpenMock = jest.spyOn(window, 'open').mockImplementation();

    await act(() => {
      handleActionClick('Download');
    });

    expect(windowOpenMock).toHaveBeenCalledWith(
      `${BASE_API_URL}/${apiPath.downloadOverlay(overlayFixture.id)}`,
      '_blank',
    );
  });
  it('should throw Error if handleActionClick action is not valid', async () => {
    const { Wrapper } = getReduxStoreWithActionsListener({});

    const {
      result: {
        current: { handleActionClick },
      },
    } = renderHook(() => useUserOverlayActions(overlayFixture), {
      wrapper: Wrapper,
    });

    expect(() => handleActionClick('Wrong Action')).toThrow('Wrong Action is not valid');
  });
});

/* eslint-disable no-magic-numbers */
import { render, screen } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { overlayFixture, overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { apiPath } from '@/redux/apiPath';
import { DEFAULT_ERROR } from '@/constants/errors';
import { act } from 'react-dom/test-utils';
import { BASE_API_URL } from '@/constants';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { UserOverlaysGroup } from './UserOverlaysGroup.component';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  const overlays = initialStoreState.overlays?.userOverlays.data || [];
  return (
    render(
      <Wrapper>
        <UserOverlaysGroup overlays={overlays} groupId={null} groupName="Without group" />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('UserOverlaysGroup - component', () => {
  it('should render list of overlays', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, overlaysPageFixture);
    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: overlaysPageFixture.content,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    for (let index = 0; index < overlaysPageFixture.content.length; index += 1) {
      const overlay = overlaysPageFixture.content[index];
      expect(screen.getByText(overlay.name)).toBeVisible();
    }
  });

  it('should display loading message if fetching user overlays is pending', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, overlaysPageFixture);

    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          ...OVERLAYS_INITIAL_STATE_MOCK.userOverlays,
          loading: 'pending',
        },
      },
    });

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });

  it('should display functioning action types list after click', async () => {
    renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: overlaysPageFixture.content,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    const actionsButton = screen.getAllByTestId('actions-button');

    const firstActionsButton = actionsButton[0];

    expect(firstActionsButton).toBeVisible();

    await act(() => {
      firstActionsButton.click();
    });

    expect(screen.getByText('Download')).toBeVisible();
    expect(screen.getByText('Edit')).toBeVisible();
  });
  it('should display overlay description on info icon hover/click', async () => {
    const page = {
      ...overlaysPageFixture,
      data: [overlayFixture],
    };

    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, page);

    renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: [overlayFixture],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    const info = screen.getByTestId('info');

    expect(info).toBeVisible();

    await act(() => {
      info.click();
    });

    expect(screen.getByText(overlayFixture.description)).toBeVisible();
  });
  it('should change state to display edit overlay modal after edit action click', async () => {
    const { store } = renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: overlaysPageFixture.content,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    const actionsButton = screen.getAllByTestId('actions-button');

    const firstActionsButton = actionsButton[0];

    expect(firstActionsButton).toBeVisible();

    await act(() => {
      firstActionsButton.click();
    });

    const editAction = screen.getByText('Edit');

    expect(editAction).toBeVisible();

    await act(() => {
      editAction.click();
    });

    const { modalName, isOpen } = store.getState().modal;

    expect(modalName).toBe('edit-overlay');
    expect(isOpen).toBe(true);
  });
  it('should display propert text for toggle overlay button', async () => {
    renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: [overlayFixture],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
      overlayBioEntity: {
        data: [],
        overlaysId: [],
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    const toggleOverlayButton = screen.getByTestId('toggle-overlay-button');

    expect(toggleOverlayButton).toBeVisible();

    await act(() => {
      toggleOverlayButton.click();
    });

    expect(screen.getByTestId('toggle-overlay-button')).toHaveTextContent('Hide');
  });
  it('should call window.open with download link after download action click', async () => {
    renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: [overlayFixture],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    const actionButton = screen.getByTestId('actions-button');

    await act(() => {
      actionButton.click();
    });

    const windowOpenMock = jest.spyOn(window, 'open').mockImplementation();

    const downloadButton = screen.getByText('Download');
    expect(downloadButton).toBeVisible();

    await act(() => {
      downloadButton.click();
    });

    expect(windowOpenMock).toHaveBeenCalledWith(
      `${BASE_API_URL}/${apiPath.downloadOverlay(overlayFixture.id)}`,
      '_blank',
    );
  });
  it('should display spinner icon if user overlay is loading', async () => {
    const OVERLAY_ID = overlayFixture.id;
    renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      overlays: {
        ...OVERLAYS_INITIAL_STATE_MOCK,
        userOverlays: {
          data: [overlayFixture],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
      overlayBioEntity: {
        data: {
          [OVERLAY_ID]: {},
        },
        overlaysId: [OVERLAY_ID],
      },
    });

    const withoutGroupTitle = screen.getByText('Without group');

    expect(withoutGroupTitle).toBeVisible();

    await act(() => {
      withoutGroupTitle.click();
    });

    expect(screen.getByAltText('spinner icon')).toBeVisible();
    expect(screen.getByText('Hide')).toBeVisible();
  });
});

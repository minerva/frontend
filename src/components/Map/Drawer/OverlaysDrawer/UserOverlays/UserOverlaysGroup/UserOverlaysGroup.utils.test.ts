/* eslint-disable no-magic-numbers */
import { MapOverlay } from '@/types/models';
import { moveArrayElement } from './UserOverlaysGroup.utils';

const INPUT_ARRAY: MapOverlay[] = [
  {
    id: 1,
    group: null,
    name: 'Overlay1',
    description: 'Description1',
    type: 'Type1',
    creator: 'Creator1',
    genomeType: 'GenomeType1',
    genomeVersion: 'GenomeVersion1',
    publicOverlay: false,
    order: 1,
  },
  {
    id: 2,
    group: null,
    name: 'Overlay2',
    description: 'Description2',
    type: 'Type2',
    creator: 'Creator2',
    genomeType: 'GenomeType2',
    genomeVersion: 'GenomeVersion2',
    publicOverlay: true,
    order: 2,
  },
  {
    id: 3,
    group: null,
    name: 'Overlay3',
    description: 'Description3',
    type: 'Type3',
    creator: 'Creator3',
    genomeType: 'GenomeType3',
    genomeVersion: 'GenomeVersion3',
    publicOverlay: false,
    order: 3,
  },
];

describe('moveArrayElement', () => {
  it('should move an element down in the array', () => {
    const expectedResult: MapOverlay[] = [
      {
        id: 1,
        group: null,
        name: 'Overlay1',
        description: 'Description1',
        type: 'Type1',
        creator: 'Creator1',
        genomeType: 'GenomeType1',
        genomeVersion: 'GenomeVersion1',
        publicOverlay: false,
        order: 1,
      },
      {
        id: 3,
        group: null,
        name: 'Overlay3',
        description: 'Description3',
        type: 'Type3',
        creator: 'Creator3',
        genomeType: 'GenomeType3',
        genomeVersion: 'GenomeVersion3',
        publicOverlay: false,
        order: 3,
      },
      {
        id: 2,
        group: null,
        name: 'Overlay2',
        description: 'Description2',
        type: 'Type2',
        creator: 'Creator2',
        genomeType: 'GenomeType2',
        genomeVersion: 'GenomeVersion2',
        publicOverlay: true,
        order: 2,
      },
    ];

    const result = moveArrayElement(INPUT_ARRAY, 1, 2);

    expect(result).toEqual(expectedResult);
  });

  it('should move an element up in the array', () => {
    const expectedResult: MapOverlay[] = [
      {
        id: 1,
        group: null,
        name: 'Overlay1',
        description: 'Description1',
        type: 'Type1',
        creator: 'Creator1',
        genomeType: 'GenomeType1',
        genomeVersion: 'GenomeVersion1',
        publicOverlay: false,
        order: 1,
      },
      {
        id: 3,
        group: null,
        name: 'Overlay3',
        description: 'Description3',
        type: 'Type3',
        creator: 'Creator3',
        genomeType: 'GenomeType3',
        genomeVersion: 'GenomeVersion3',
        publicOverlay: false,
        order: 3,
      },
      {
        id: 2,
        group: null,
        name: 'Overlay2',
        description: 'Description2',
        type: 'Type2',
        creator: 'Creator2',
        genomeType: 'GenomeType2',
        genomeVersion: 'GenomeVersion2',
        publicOverlay: true,
        order: 2,
      },
    ];

    const result = moveArrayElement(INPUT_ARRAY, 2, 1);

    expect(result).toEqual(expectedResult);
  });

  it('should handle moving an element to the beginning of the array', () => {
    const expectedResult: MapOverlay[] = [
      {
        id: 3,
        group: null,
        name: 'Overlay3',
        description: 'Description3',
        type: 'Type3',
        creator: 'Creator3',
        genomeType: 'GenomeType3',
        genomeVersion: 'GenomeVersion3',
        publicOverlay: false,
        order: 3,
      },
      {
        id: 1,
        group: null,
        name: 'Overlay1',
        description: 'Description1',
        type: 'Type1',
        creator: 'Creator1',
        genomeType: 'GenomeType1',
        genomeVersion: 'GenomeVersion1',
        publicOverlay: false,
        order: 1,
      },
      {
        id: 2,
        group: null,
        name: 'Overlay2',
        description: 'Description2',
        type: 'Type2',
        creator: 'Creator2',
        genomeType: 'GenomeType2',
        genomeVersion: 'GenomeVersion2',
        publicOverlay: true,
        order: 2,
      },
    ];

    const result = moveArrayElement(INPUT_ARRAY, 2, 0);

    expect(result).toEqual(expectedResult);
  });

  it('should handle moving an element to the end of the array', () => {
    const expectedResult: MapOverlay[] = [
      {
        id: 2,
        group: null,
        name: 'Overlay2',
        description: 'Description2',
        type: 'Type2',
        creator: 'Creator2',
        genomeType: 'GenomeType2',
        genomeVersion: 'GenomeVersion2',
        publicOverlay: true,
        order: 2,
      },
      {
        id: 3,
        group: null,
        name: 'Overlay3',
        description: 'Description3',
        type: 'Type3',
        creator: 'Creator3',
        genomeType: 'GenomeType3',
        genomeVersion: 'GenomeVersion3',
        publicOverlay: false,
        order: 3,
      },
      {
        id: 1,
        group: null,
        name: 'Overlay1',
        description: 'Description1',
        type: 'Type1',
        creator: 'Creator1',
        genomeType: 'GenomeType1',
        genomeVersion: 'GenomeVersion1',
        publicOverlay: false,
        order: 1,
      },
    ];

    const result = moveArrayElement(INPUT_ARRAY, 0, 2);

    expect(result).toEqual(expectedResult);
  });

  it('should handle out-of-bounds indices gracefully', () => {
    const result = moveArrayElement(INPUT_ARRAY, 5, 1);

    expect(result).toEqual(INPUT_ARRAY);
  });
});

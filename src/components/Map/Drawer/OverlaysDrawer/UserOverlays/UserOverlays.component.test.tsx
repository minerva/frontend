import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { UserOverlays } from './UserOverlays.component';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <UserOverlays />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('UserOverlays component', () => {
  it('renders loading message when user is loading', () => {
    renderComponent({
      user: {
        loading: 'pending',
        authenticated: false,
        error: { name: '', message: '' },
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
    });

    expect(screen.getByText('Loading')).toBeInTheDocument();
  });

  it('renders add overlay button when user is authenticated', () => {
    renderComponent({
      user: {
        loading: 'succeeded',
        authenticated: true,
        error: { name: '', message: '' },
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
    });

    expect(screen.getByLabelText('add overlay button')).toBeInTheDocument();
  });
  it('renders user overlays section when user is authenticated', () => {
    renderComponent({
      user: {
        loading: 'succeeded',
        authenticated: true,
        error: { name: '', message: '' },
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
    });

    expect(screen.getByText('Without group')).toBeInTheDocument();
  });
});

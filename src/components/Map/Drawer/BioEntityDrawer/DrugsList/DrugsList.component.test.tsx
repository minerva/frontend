import { DEFAULT_FETCH_DATA } from '@/constants/fetchData';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { DrugsList } from './DrugsList.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <DrugsList />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('DrugsList - component', () => {
  describe('when drugs data is loading', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'pending',
                data: [],
              },
            },
            chemicals: {},
          },
        },
      });
    });

    it('should show loading indicator', () => {
      expect(screen.getByTestId('loading-indicator')).toBeInTheDocument();
    });
  });

  describe('when drugs data is empty', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: [],
              },
            },
            chemicals: {},
          },
        },
      });
    });

    it('should show text with info', () => {
      expect(screen.getByText('List is empty')).toBeInTheDocument();
    });
  });

  describe('when drugs data is present and valid', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: drugsFixture,
              },
            },
            chemicals: {},
          },
        },
      });
    });

    it.each(drugsFixture)('should show bio entitity card', drug => {
      expect(screen.getByText(drug.name)).toBeInTheDocument();
    });
  });

  describe('when drugs data is present but for different bio entity id', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {
              '2137': {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: drugsFixture,
              },
            },
            chemicals: {},
          },
        },
      });
    });

    it.each(drugsFixture)('should not show bio entitity card', drug => {
      expect(screen.queryByText(drug.name)).not.toBeInTheDocument();
    });

    it('should show text with info', () => {
      expect(screen.getByText('List is empty')).toBeInTheDocument();
    });
  });
});

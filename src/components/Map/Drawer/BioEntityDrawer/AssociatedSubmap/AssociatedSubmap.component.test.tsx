import { SIZE_OF_ARRAY_WITH_ONE_ELEMENT, ZERO } from '@/constants/common';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { BIOENTITY_INITIAL_STATE_MOCK } from '@/redux/bioEntity/bioEntity.mock';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import {
  initialMapDataFixture,
  openedMapsInitialValueFixture,
  openedMapsThreeSubmapsFixture,
} from '@/redux/map/map.fixtures';
import { MODELS_INITIAL_STATE_MOCK } from '@/redux/models/models.mock';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID } from '@/constants/mocks';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import {
  MODEL_ELEMENT_LINKING_TO_SUBMAP,
  MODEL_ELEMENTS_SEARCH_LINKING_TO_SUBMAP_DATA_MOCK,
} from '@/redux/modelElements/modelElements.mock';
import { DEFAULT_ERROR } from '@/constants/errors';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { AssociatedSubmap } from './AssociatedSubmap.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <AssociatedSubmap />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('AssociatedSubmap - component', () => {
  it('should not display component when can not find asociated map model', () => {
    renderComponent({
      bioEntity: {
        ...BIOENTITY_INITIAL_STATE_MOCK,
      },
      drawer: {
        ...DRAWER_INITIAL_STATE,
        bioEntityDrawerState: {
          bioentityId: modelElementFixture.id,
          drugs: {},
          chemicals: {},
        },
      },
      modelElements: {
        data: {
          [MAIN_MAP_ID]: {
            data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: MODEL_ELEMENTS_SEARCH_LINKING_TO_SUBMAP_DATA_MOCK,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      } as ModelElementsState,
      models: {
        ...MODELS_INITIAL_STATE_MOCK,
      },
    });

    expect(screen.queryByTestId('associated-submap')).not.toBeInTheDocument();
  });

  it('should render component when associated map model is found', () => {
    renderComponent({
      map: {
        data: {
          ...initialMapDataFixture,
          modelId: MAIN_MAP_ID,
        },
        loading: 'succeeded',
        error: { name: '', message: '' },
        openedMaps: openedMapsThreeSubmapsFixture,
      },
      bioEntity: {
        ...BIOENTITY_INITIAL_STATE_MOCK,
      },
      drawer: {
        ...DRAWER_INITIAL_STATE,
        bioEntityDrawerState: {
          bioentityId: modelElementFixture.id,
          drugs: {},
          chemicals: {},
        },
      },
      modelElements: {
        data: {
          [MAIN_MAP_ID]: {
            data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: MODEL_ELEMENTS_SEARCH_LINKING_TO_SUBMAP_DATA_MOCK,
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      } as ModelElementsState,
      models: {
        ...MODELS_INITIAL_STATE_MOCK,
        data: MODELS_MOCK_SHORT,
      },
    });

    expect(screen.getByTestId('associated-submap')).toBeInTheDocument();
  });

  describe('when map is already opened', () => {
    it('should open submap and set it to active on open submap button click', async () => {
      const { store } = renderComponent({
        map: {
          data: initialMapDataFixture,
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsInitialValueFixture,
        },
        bioEntity: {
          ...BIOENTITY_INITIAL_STATE_MOCK,
        },
        modelElements: {
          data: {
            0: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: MODEL_ELEMENTS_SEARCH_LINKING_TO_SUBMAP_DATA_MOCK,
            loading: 'idle',
            error: DEFAULT_ERROR,
          },
        } as ModelElementsState,
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
        models: {
          ...MODELS_INITIAL_STATE_MOCK,
          data: MODELS_MOCK_SHORT,
        },
      });

      const {
        data: { modelId },
        openedMaps,
      } = store.getState().map;
      expect(modelId).toBe(ZERO);
      expect(openedMaps).not.toContainEqual({
        modelId: HISTAMINE_MAP_ID,
        modelName: 'Histamine signaling',
        lastPosition: { x: 0, y: 0, z: 0 },
      });
      const openSubmapButton = screen.getByRole('button', { name: 'Open submap' });
      await act(() => {
        openSubmapButton.click();
      });

      const {
        data: { modelId: newModelId },
        openedMaps: newOpenedMaps,
      } = store.getState().map;

      expect(newOpenedMaps).toContainEqual({
        modelId: HISTAMINE_MAP_ID,
        modelName: 'Histamine signaling',
        lastPosition: { x: 0, y: 0, z: 0 },
      });

      expect(newModelId).toBe(HISTAMINE_MAP_ID);
    });

    it('should set map active on open submap button click', async () => {
      const { store } = renderComponent({
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
        bioEntity: {
          ...BIOENTITY_INITIAL_STATE_MOCK,
        },
        modelElements: {
          data: {
            [MAIN_MAP_ID]: {
              data: [MODEL_ELEMENT_LINKING_TO_SUBMAP],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: MODEL_ELEMENTS_SEARCH_LINKING_TO_SUBMAP_DATA_MOCK,
            loading: 'idle',
            error: DEFAULT_ERROR,
          },
        } as ModelElementsState,
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
        models: {
          ...MODELS_INITIAL_STATE_MOCK,
          data: MODELS_MOCK_SHORT,
        },
      });

      const openSubmapButton = screen.getByRole('button', { name: 'Open submap' });
      await act(() => {
        openSubmapButton.click();
      });

      const {
        map: {
          data: { modelId },
          openedMaps,
        },
      } = store.getState();

      const histamineMap = openedMaps.filter(map => map.modelName === 'Histamine signaling');

      expect(histamineMap.length).toBe(SIZE_OF_ARRAY_WITH_ONE_ELEMENT);
      expect(modelId).toBe(HISTAMINE_MAP_ID);
    });
  });
});

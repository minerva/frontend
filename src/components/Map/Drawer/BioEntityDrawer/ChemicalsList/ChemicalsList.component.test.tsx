import { DEFAULT_FETCH_DATA } from '@/constants/fetchData';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { ChemicalsList } from './ChemicalsList.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ChemicalsList />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ChemicalsList - component', () => {
  describe('when chemicals data is loading', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {},
            chemicals: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'pending',
                data: [],
              },
            },
          },
        },
      });
    });

    it('should show loading indicator', () => {
      expect(screen.getByTestId('loading-indicator')).toBeInTheDocument();
    });
  });

  describe('when chemicals data is empty', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {},
            chemicals: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: [],
              },
            },
          },
        },
      });
    });

    it('should show text with info', () => {
      expect(screen.getByText('List is empty')).toBeInTheDocument();
    });
  });

  describe('when chemicals data is present and valid', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {},
            chemicals: {
              [`${bioEntityId}`]: {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: chemicalsFixture,
              },
            },
          },
        },
      });
    });

    it.each(chemicalsFixture)('should show bio entitity card', chemical => {
      expect(screen.getByText(chemical.name)).toBeInTheDocument();
    });
  });

  describe('when chemicals data is present but for different bio entity id', () => {
    beforeEach(() => {
      const bioEntityId = 5000;

      renderComponent({
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioentityId: bioEntityId,
            drugs: {},
            chemicals: {
              '2137': {
                ...DEFAULT_FETCH_DATA,
                loading: 'succeeded',
                data: chemicalsFixture,
              },
            },
          },
        },
      });
    });

    it.each(chemicalsFixture)('should not show bio entitity card', chemical => {
      expect(screen.queryByText(chemical.name)).not.toBeInTheDocument();
    });

    it('should show text with info', () => {
      expect(screen.getByText('List is empty')).toBeInTheDocument();
    });
  });
});

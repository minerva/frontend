/* eslint-disable no-magic-numbers */
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import { MODELS_INITIAL_STATE_MOCK } from '@/redux/models/models.mock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState } from '@/redux/store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { InitialStoreState } from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { getTypeBySBOTerm } from '@/utils/bioEntity/getTypeBySBOTerm';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import {
  MODEL_ELEMENT_LINKING_TO_SUBMAP,
  MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
} from '@/redux/modelElements/modelElements.mock';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { PROJECT_STATE_INITIAL_MOCK } from '@/redux/project/project.mock';
import { BioEntityDrawer } from './BioEntityDrawer.component';

const renderComponent = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener({
    ...INITIAL_STORE_STATE_MOCK,
    ...initialStoreState,
  });

  return (
    render(
      <Wrapper>
        <BioEntityDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('BioEntityDrawer - component', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  describe("when there's NO matching bioEntity", () => {
    beforeEach(() =>
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: DRAWER_INITIAL_STATE,
      }),
    );

    it('should not show drawer content', () => {
      expect(screen.queryByText('Compartment:')).toBeNull();
      expect(screen.queryByText('Full name:')).toBeNull();
      expect(screen.queryByText('Annotations:')).toBeNull();
      expect(screen.queryByText('Source:')).toBeNull();
    });
  });

  describe('when there IS a matching bioEntity', () => {
    const bioEntityName = 'test';
    const bioEntityFullName = 'BioEntity Full Name';

    it('should show drawer header', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      const bioEntityType = getTypeBySBOTerm(modelElementFixture.sboTerm);
      expect(screen.getByText(bioEntityType, { exact: false })).toBeInTheDocument();
      expect(screen.getByText(bioEntityName, { exact: false })).toBeInTheDocument();
    });

    it('should show drawer bioEntity full name', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: bioEntityFullName }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      expect(screen.getByText('Full name:', { exact: false })).toBeInTheDocument();
      expect(screen.getByText(bioEntityFullName, { exact: false })).toBeInTheDocument();
    });

    it("should not show drawer bioEntity full name if it doesn't exists", () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      expect(screen.queryByText('Full name:')).toBeNull();
    });

    it('should show list of annotations ', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      expect(screen.getByText('Annotations:')).toBeInTheDocument();
      expect(
        screen.getByText(modelElementFixture.references[0].type, { exact: false }),
      ).toBeInTheDocument();
      expect(
        screen.getByText(modelElementFixture.references[0].resource, { exact: false }),
      ).toBeInTheDocument();
    });

    it('should display associated submaps if bio entity links to submap', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...MODEL_ELEMENT_LINKING_TO_SUBMAP, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: MODEL_ELEMENT_LINKING_TO_SUBMAP.id,
            drugs: {},
            chemicals: {},
          },
        },
        models: {
          ...MODELS_INITIAL_STATE_MOCK,
          data: MODELS_MOCK_SHORT,
        },
      });

      expect(screen.getByTestId('associated-submap')).toBeInTheDocument();
    });

    it('should display chemicals list header', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      expect(screen.getByText('Drugs for target')).toBeInTheDocument();
    });

    it('should display drugs list header', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      expect(screen.getByText('Chemicals for target', { exact: false })).toBeInTheDocument();
    });

    it('should fetch chemicals on chemicals for target click', () => {
      const { store } = renderComponent({
        ...INITIAL_STORE_STATE_MOCK,
        project: {
          data: projectFixture,
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: projectFixture.projectId,
        },
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
      });

      const button = screen.getByText('Chemicals for target', { exact: false });
      act(() => {
        button.click();
      });

      expect(store.getActions()[0].type).toBe(
        'drawer/getChemicalsForBioEntityDrawerTarget/pending',
      );
    });

    it('should fetch drugs on drugs for target click', () => {
      const { store } = renderComponent({
        modelElements: {
          data: {
            0: {
              data: [{ ...modelElementFixture, name: bioEntityName, fullName: null }],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          bioEntityDrawerState: {
            bioentityId: modelElementFixture.id,
            drugs: {},
            chemicals: {},
          },
        },
        project: {
          ...PROJECT_STATE_INITIAL_MOCK,
          data: projectFixture,
        },
      });

      const button = screen.getByText('Drugs for target', { exact: false });
      act(() => {
        button.click();
      });

      expect(store.getActions()[0].type).toBe('drawer/getDrugsForBioEntityDrawerTarget/pending');
    });
  });
});

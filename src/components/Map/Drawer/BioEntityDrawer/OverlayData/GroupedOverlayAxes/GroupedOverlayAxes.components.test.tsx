import { ZERO } from '@/constants/common';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { MapOverlay } from '@/types/models';
import { render, screen } from '@testing-library/react';
import { OverlayDataAxis } from '../OverlayData.types';
import { GroupedOverlayAxes } from './GroupedOverlayAxes.components';

const BASE_AXIS: OverlayDataAxis = {
  id: 123,
  title: 'axis title',
  value: 2137,
  color: '#FFFFFF',
  geneVariants: undefined,
  overlayId: overlayFixture.id,
};

const renderComponent = (axes: OverlayDataAxis[], overlay: MapOverlay): void => {
  render(<GroupedOverlayAxes axes={axes} overlay={overlay} />);
};

describe('GroupedOverlayAxes', () => {
  describe('when axes array is empty', () => {
    beforeEach(() => {
      renderComponent([], overlayFixture);
    });

    it('should not render title', () => {
      expect(screen.queryAllByText(overlayFixture.name).length).toEqual(ZERO);
    });
  });

  describe('when axes array is present', () => {
    const AXES = [
      {
        ...BASE_AXIS,
        title: 'axis 1',
      },
      {
        ...BASE_AXIS,
        title: 'axis 2',
      },
    ];

    beforeEach(() => {
      renderComponent(AXES, overlayFixture);
    });
    it('should render title', () => {
      expect(screen.getByText(overlayFixture.name)).toBeInTheDocument();
    });

    it.each(AXES)('should render overlay axis', ({ title }) => {
      expect(screen.getByText(title)).toBeInTheDocument();
    });
  });
});

import { GeneVariant } from '@/types/models';

export interface OverlayDataAxis {
  id: number | string;
  title: string;
  value?: number;
  color: string;
  geneVariants?: GeneVariant[] | null;
  overlayId: number;
}

/* eslint-disable no-magic-numbers */
import { ZERO } from '@/constants/common';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { GENE_VARIANTS_MOCK } from '@/models/mocks/geneVariantsMock';
import { CORE_PD_MODEL_MOCK } from '@/models/mocks/modelsMock';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { MODELS_INITIAL_STATE_MOCK } from '@/redux/models/models.mock';
import {
  MOCKED_OVERLAY_BIO_ENTITY_RENDER,
  OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
} from '@/redux/overlayBioEntity/overlayBioEntity.mock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { OverlayData, Props } from './OverlayData.component';

const renderComponent = (
  initialStoreState: InitialStoreState = {},
  props: Props = {},
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <OverlayData {...props} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('OverlayData - component', () => {
  describe('when axes list is empty', () => {
    beforeEach(() => {
      renderComponent({});
    });

    it('should not render component', () => {
      expect(screen.queryAllByText('Overlay data:').length).toEqual(ZERO);
    });
  });

  describe('when axes list is present', () => {
    beforeEach(() => {
      const OVERLAY_ID = overlayFixture.id;
      const BIO_ENTITY = MOCKED_OVERLAY_BIO_ENTITY_RENDER[0];

      renderComponent({
        ...INITIAL_STORE_STATE_MOCK,
        map: {
          ...initialMapStateFixture,
          data: { ...initialMapStateFixture.data, modelId: CORE_PD_MODEL_MOCK.id },
        },
        overlays: {
          ...INITIAL_STORE_STATE_MOCK.overlays,
          data: [{ ...overlayFixture, name: 'axis name' }],
        },
        overlayBioEntity: {
          ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
          overlaysId: [OVERLAY_ID],
          data: {
            [OVERLAY_ID]: {
              [CORE_PD_MODEL_MOCK.id]: [
                {
                  ...BIO_ENTITY,
                  geneVariants: GENE_VARIANTS_MOCK,
                  modelId: CORE_PD_MODEL_MOCK.id,
                  overlayId: OVERLAY_ID,
                },
              ],
            },
          },
        },
        models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
        drawer: {
          ...INITIAL_STORE_STATE_MOCK.drawer,
          bioEntityDrawerState: {
            ...INITIAL_STORE_STATE_MOCK.drawer.bioEntityDrawerState,
            bioentityId: BIO_ENTITY.id,
          },
        },
      });
    });

    it('should render title', () => {
      expect(screen.getByText('Overlay data:')).toBeInTheDocument();
    });

    it('should render axis title', () => {
      expect(screen.getByText('axis name')).toBeInTheDocument();
    });
  });

  describe('when axes list is present and isShowGroupedOverlays=true', () => {
    beforeEach(() => {
      const OVERLAY_ID = overlayFixture.id;
      const BIO_ENTITY = MOCKED_OVERLAY_BIO_ENTITY_RENDER[0];

      renderComponent(
        {
          ...INITIAL_STORE_STATE_MOCK,
          map: {
            ...initialMapStateFixture,
            data: { ...initialMapStateFixture.data, modelId: CORE_PD_MODEL_MOCK.id },
          },
          overlays: {
            ...INITIAL_STORE_STATE_MOCK.overlays,
            data: [{ ...overlayFixture, name: 'overlay name' }],
          },
          overlayBioEntity: {
            ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
            overlaysId: [OVERLAY_ID],
            data: {
              [OVERLAY_ID]: {
                [CORE_PD_MODEL_MOCK.id]: [
                  {
                    ...BIO_ENTITY,
                    geneVariants: GENE_VARIANTS_MOCK,
                    modelId: CORE_PD_MODEL_MOCK.id,
                    overlayId: OVERLAY_ID,
                  },
                ],
              },
            },
          },
          models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
          drawer: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioEntityDrawerState: {
              ...INITIAL_STORE_STATE_MOCK.drawer.bioEntityDrawerState,
              bioentityId: BIO_ENTITY.id,
            },
          },
        },
        {
          isShowGroupedOverlays: true,
        },
      );
    });

    it('should render title', () => {
      expect(screen.getByText('Overlay data:')).toBeInTheDocument();
    });

    it('should render names of two overlays', () => {
      expect(screen.queryAllByText('overlay name').length).toEqual(2);
    });
  });

  describe('when axes list is present and isShowOverlayBioEntityName=true', () => {
    beforeEach(() => {
      const OVERLAY_ID = overlayFixture.id;
      const BIO_ENTITY = MOCKED_OVERLAY_BIO_ENTITY_RENDER[0];

      renderComponent(
        {
          ...INITIAL_STORE_STATE_MOCK,
          map: {
            ...initialMapStateFixture,
            data: { ...initialMapStateFixture.data, modelId: CORE_PD_MODEL_MOCK.id },
          },
          overlays: {
            ...INITIAL_STORE_STATE_MOCK.overlays,
            data: [{ ...overlayFixture, name: 'overlay name' }],
          },
          overlayBioEntity: {
            ...OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
            overlaysId: [OVERLAY_ID],
            data: {
              [OVERLAY_ID]: {
                [CORE_PD_MODEL_MOCK.id]: [
                  {
                    ...BIO_ENTITY,
                    geneVariants: GENE_VARIANTS_MOCK,
                    modelId: CORE_PD_MODEL_MOCK.id,
                    overlayId: OVERLAY_ID,
                    name: 'element name',
                  },
                ],
              },
            },
          },
          models: { ...MODELS_INITIAL_STATE_MOCK, data: [CORE_PD_MODEL_MOCK] },
          drawer: {
            ...INITIAL_STORE_STATE_MOCK.drawer,
            bioEntityDrawerState: {
              ...INITIAL_STORE_STATE_MOCK.drawer.bioEntityDrawerState,
              bioentityId: BIO_ENTITY.id,
            },
          },
        },
        {
          isShowOverlayBioEntityName: true,
        },
      );
    });

    it('should render title', () => {
      expect(screen.getByText('Overlay data:')).toBeInTheDocument();
    });

    it('should render element name', () => {
      expect(screen.getByText('element name')).toBeInTheDocument();
    });
  });
});

import { ZERO } from '@/constants/common';
import { OverlayDataAxis } from '../OverlayData.types';

export const getAxesSortedByValue = (axes: OverlayDataAxis[]): OverlayDataAxis[] =>
  axes.sort((a, b) => (b?.value || ZERO) - (a?.value || ZERO));

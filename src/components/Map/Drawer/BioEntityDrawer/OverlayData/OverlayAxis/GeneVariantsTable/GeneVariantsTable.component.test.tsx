import { GENE_VARIANTS_MOCK } from '@/models/mocks/geneVariantsMock';
import { GeneVariant } from '@/types/models';
import { render, screen } from '@testing-library/react';
import { GeneVariantsTable } from './GeneVariantsTable.component';

const renderComponent = (data: GeneVariant[]): void => {
  render(<GeneVariantsTable data={data} />);
};

describe('GeneVariantsTable - component', () => {
  beforeEach(() => {
    renderComponent(GENE_VARIANTS_MOCK);
  });

  it('should render header', () => {
    expect(screen.getByText('Contig')).toBeInTheDocument();
    expect(screen.getByText('Position')).toBeInTheDocument();
    expect(screen.getByText('From')).toBeInTheDocument();
    expect(screen.getByText('To')).toBeInTheDocument();
    expect(screen.getByText('rsID')).toBeInTheDocument();
  });

  it.each(GENE_VARIANTS_MOCK)(
    'should render row',
    ({ contig, position, originalDna, modifiedDna, variantIdentifier }) => {
      const elements = [
        screen.getAllByText(contig),
        screen.getAllByText(position),
        screen.getAllByText(originalDna),
        screen.getAllByText(modifiedDna),
        screen.getAllByText(variantIdentifier),
      ].flat();

      elements.forEach(element => expect(element).toBeInTheDocument());
    },
  );
});

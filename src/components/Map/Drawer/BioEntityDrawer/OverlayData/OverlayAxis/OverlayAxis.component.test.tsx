/* eslint-disable no-magic-numbers */
import { ZERO } from '@/constants/common';
import { GENE_VARIANTS_MOCK } from '@/models/mocks/geneVariantsMock';
import { act, render, screen } from '@testing-library/react';
import { OverlayDataAxis } from '../OverlayData.types';
import { OverlayAxis } from './OverlayAxis.component';

const renderComponent = (axis: OverlayDataAxis): void => {
  render(<OverlayAxis axis={axis} />);
};

const BASE_AXIS: OverlayDataAxis = {
  id: 123,
  title: 'axis title',
  value: 2137,
  color: '#FFFFFF',
  geneVariants: undefined,
  overlayId: 1,
};

describe('OverlayAxis - component', () => {
  describe('when always', () => {
    beforeEach(() => {
      renderComponent(BASE_AXIS);
    });

    it('renders title', () => {
      expect(screen.getByText('axis title')).toBeInTheDocument();
    });

    it('renders background with valid color', () => {
      expect(screen.getByTestId('overlay-axis-bg')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-bg').getAttribute('style')).toContain(
        'background: rgba(255, 255, 255, 0.102);',
      );
    });

    it('renders valid value title (when no gene variants)', () => {
      expect(screen.getByTestId('overlay-axis-value')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-value').innerHTML).toContain('2137');
    });
  });

  describe('when value is positive', () => {
    beforeEach(() => {
      renderComponent({
        ...BASE_AXIS,
        value: 0.564234344,
        color: '#FF0000',
      });
    });

    it('renders bar with valid color and width', () => {
      const axis = screen.getByTestId('overlay-axis-bg');
      const bar = axis.childNodes[0] as HTMLElement;

      expect(bar).toBeInTheDocument();
      expect(bar?.getAttribute('class')).toContain('rounded-r');
      expect(bar?.getAttribute('class')).toContain('left-1/2');
      expect(bar?.getAttribute('style')).toContain('width: 28.211717200000002%;');
      expect(bar?.getAttribute('style')).toContain('background: rgb(255, 0, 0);');
    });

    it('renders valid value title (when no gene variants)', () => {
      expect(screen.getByTestId('overlay-axis-value')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-value').innerHTML).toContain('0.56');
    });
  });

  describe('when value is negative', () => {
    beforeEach(() => {
      renderComponent({
        ...BASE_AXIS,
        value: -0.3255323223,
        color: '#00FF00',
      });
    });

    it('renders bar with valid color and width', () => {
      const axis = screen.getByTestId('overlay-axis-bg');
      const bar = axis.childNodes[0] as HTMLElement;

      expect(bar).toBeInTheDocument();
      expect(bar?.getAttribute('class')).toContain('rounded-l');
      expect(bar?.getAttribute('class')).toContain('right-1/2');
      expect(bar?.getAttribute('style')).toContain('width: 16.276616115%;');
      expect(bar?.getAttribute('style')).toContain('background: rgb(0, 255, 0);');
    });

    it('renders valid value title (when no gene variants)', () => {
      expect(screen.getByTestId('overlay-axis-value')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-value').innerHTML).toContain('-0.33');
    });
  });

  describe('when value is zero', () => {
    beforeEach(() => {
      renderComponent({
        ...BASE_AXIS,
        value: 0,
        color: '#0000FF',
      });
    });

    it('renders bar with valid color and width', () => {
      const axis = screen.getByTestId('overlay-axis-bg');
      const bar = axis.childNodes[0] as HTMLElement;

      expect(bar).toBeInTheDocument();
      expect(bar?.getAttribute('class')).not.toContain('right-1/2');
      expect(bar?.getAttribute('class')).not.toContain('rounded-l');
      expect(bar?.getAttribute('class')).not.toContain('right-1/2');
      expect(bar?.getAttribute('style')).toContain('width: 0%;');
    });

    it('renders valid value title (when no gene variants)', () => {
      expect(screen.getByTestId('overlay-axis-value')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-value').innerHTML).toContain('-');
    });
  });

  describe('when value is undefined', () => {
    beforeEach(() => {
      renderComponent({
        ...BASE_AXIS,
        value: undefined,
      });
    });

    it('renders bar with valid color and width', () => {
      const axis = screen.getByTestId('overlay-axis-bg');
      const bar = axis.childNodes[0] as HTMLElement;

      expect(bar).toBeInTheDocument();
      expect(bar?.getAttribute('class')).toContain('w-full');
      expect(bar?.getAttribute('style')).toContain('width: 100%;');
      expect(bar?.getAttribute('style')).toContain('background: rgb(255, 255, 255);');
    });

    it('renders valid value title (when no gene variants)', () => {
      expect(screen.getByTestId('overlay-axis-value')).toBeInTheDocument();
      expect(screen.getByTestId('overlay-axis-value').innerHTML).toContain('-');
    });
  });

  describe('when there is gene variants', () => {
    beforeEach(() => {
      renderComponent({
        ...BASE_AXIS,
        geneVariants: GENE_VARIANTS_MOCK,
      });
    });

    it('renders gene variants icon button', () => {
      expect(screen.getByTestId('overlay-axis-icon')).toBeInTheDocument();
    });

    it('renders gene variants info icon', () => {
      const infoIconWrapper = screen.getByTitle(
        'Number of variants mapped to this gene. See their details in the tabular view below.',
      );
      const icon = infoIconWrapper.childNodes[1] as HTMLElement;

      expect(infoIconWrapper).toBeInTheDocument();
      expect(icon).toBeInTheDocument();
    });

    it('shows gene variants table on gene variants icon button click', () => {
      const geneVariantsTable = screen.queryAllByText('Contig');
      expect(geneVariantsTable.length).toEqual(ZERO);

      const iconButton = screen.getByTestId('overlay-axis-icon');
      act(() => {
        iconButton.click();
      });

      const geneVariantsTableVisible = screen.getByText('Contig');
      expect(geneVariantsTableVisible).toBeInTheDocument();
    });
  });
});

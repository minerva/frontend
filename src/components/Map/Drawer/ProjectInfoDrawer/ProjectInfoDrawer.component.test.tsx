import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { StoreType } from '@/redux/store';
import { MODEL_WITH_DESCRIPTION } from '@/models/mocks/modelsMock';
import { ProjectInfoDrawer } from './ProjectInfoDrawer.component';

const PROJECT = {
  ...projectFixture,
  topMap: {
    id: MODEL_WITH_DESCRIPTION.id,
  },
};

const MOCKED_STORE: InitialStoreState = {
  project: {
    data: PROJECT,
    loading: 'idle',
    error: new Error(),
    projectId: '',
  },
  models: {
    data: [MODEL_WITH_DESCRIPTION],
    loading: 'idle',
    error: new Error(),
  },
};

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <ProjectInfoDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ProjectInfoDrawer', () => {
  it('should render the project name', () => {
    renderComponent(MOCKED_STORE);

    expect(screen.getByText(projectFixture.name)).toBeInTheDocument();
  });

  it('should render the version', () => {
    renderComponent(MOCKED_STORE);

    expect(screen.getByText(projectFixture.version)).toBeInTheDocument();
  });

  it.skip('should render number of publications', () => {});
  it.skip('should open publications modal when publications link is clicked', () => {});

  it('should render the manual link', () => {
    renderComponent(MOCKED_STORE);

    const manualLink = screen.getByText(/Manual/i);
    expect(manualLink).toBeInTheDocument();
    expect(manualLink).toHaveAttribute('href', 'https://minerva.pages.uni.lu/doc/');
  });

  it('should render the disease link with name and href', async () => {
    await act(() => {
      renderComponent(MOCKED_STORE);
    });

    const diseaseLink = screen.getByText(/Disease:/i);
    expect(diseaseLink).toBeInTheDocument();

    const linkelement = screen.getByRole('link', {
      name: projectFixture.diseaseName ? projectFixture.diseaseName : 'xyz',
    });
    expect(linkelement).toBeInTheDocument();
    expect(linkelement).toHaveAttribute('href', projectFixture.disease?.link);
  });

  it('should fetch disease name when diseaseId is provided', async () => {
    await act(() => {
      renderComponent(MOCKED_STORE);
    });

    const organismLink = screen.getByText(/Organism:/i);
    expect(organismLink).toBeInTheDocument();

    const linkelement = screen.getByRole('link', {
      name: projectFixture.organismName ? projectFixture.organismName : 'xyz',
    });
    expect(linkelement).toBeInTheDocument();
    expect(linkelement).toHaveAttribute('href', projectFixture.organism?.link);
  });

  it('should render the source file download button', () => {
    renderComponent(MOCKED_STORE);

    const downloadButton = screen.getByRole('link', { name: /Download source file/i });
    expect(downloadButton).toBeInTheDocument();
    expect(downloadButton).toHaveAttribute(
      'href',
      'https://lux1.atcomp.pl/minerva/api/projects/pdmap_appu_test:downloadSource',
    );
    expect(downloadButton).toHaveAttribute('download', 'sourceFile.txt');
  });

  it('should render the description when it exists', () => {
    renderComponent(MOCKED_STORE);

    const desc = screen.getByTestId('project-description');

    expect(desc.innerHTML).toContain(
      'For information on content, functionalities and referencing the Parkinson\'s disease map, click <a href="http://pdmap.uni.lu" target="_blank">here</a>',
    );
  });

  it.skip('should not render the description when it does not exist', () => {
    renderComponent();

    const descriptionElement = screen.queryByText('This is the project description.');
    expect(descriptionElement).not.toBeInTheDocument();
  });
});

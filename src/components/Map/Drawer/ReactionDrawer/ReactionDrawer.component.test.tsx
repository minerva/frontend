import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { referenceFixture } from '@/models/fixtures/referenceFixture';
import { ReactionDrawer } from './ReactionDrawer.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ReactionDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const reference = { ...referenceFixture, link: 'https://uni.lu' };

describe('ReactionDrawer - component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe("when there's NO matching reaction", () => {
    beforeEach(() =>
      renderComponent({
        reactions: {
          data: [],
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
        drawer: DRAWER_INITIAL_STATE,
      }),
    );

    it('should not show drawer content', () => {
      expect(screen.queryByText('Reaction:')).toBeNull();
      expect(screen.queryByText('Type:')).toBeNull();
      expect(screen.queryByText('Annotations:')).toBeNull();
      expect(screen.queryByText('Source:')).toBeNull();
    });
  });

  describe('when there IS a matching reaction', () => {
    const reaction = { ...newReactionFixture, references: [reference] };

    const filteredReferences = reaction.references.filter(
      ref => ref.link !== null && ref.link !== undefined,
    );

    const referencesTextHref: [string, string][] = filteredReferences.map(ref => [
      `${ref.type} (${ref.resource})`,
      ref.link as string,
    ]);

    beforeEach(() =>
      renderComponent({
        reactions: {
          data: [reaction],
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
        newReactions: {
          0: {
            data: [reaction],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        drawer: {
          ...DRAWER_INITIAL_STATE,
          reactionDrawerState: {
            reactionId: reaction.id,
          },
        },
      }),
    );

    it('should show drawer header', () => {
      expect(screen.getByText('Reaction:')).toBeInTheDocument();
      expect(screen.getByText(reaction.idReaction)).toBeInTheDocument();
    });

    it('should show drawer reaction type', () => {
      expect(screen.getByText('Type:')).toBeInTheDocument();
      // expect(screen.getByText(reaction.type)).toBeInTheDocument();
    });

    it('should show drawer reaction annotations title', () => {
      expect(screen.getByText('Annotations:')).toBeInTheDocument();
    });

    it.each(referencesTextHref)(
      'should show drawer reaction reference with text=%s, href=%s',
      (refText, href) => {
        const linkReferenceSpan = screen.getByText(refText, { exact: false });
        const linkReferenceAnchor = linkReferenceSpan.closest('a');

        expect(linkReferenceSpan).toBeInTheDocument();
        expect(linkReferenceAnchor).toBeInTheDocument();
        expect(linkReferenceAnchor?.href).toBe(`${href}/`); // component render adds trailing slash
      },
    );
  });
});

/* eslint-disable no-magic-numbers */
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import { DEFAULT_ERROR } from '@/constants/errors';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { ConnectedBioEntitiesList } from './ConnectedBioEntitiesList.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ConnectedBioEntitiesList />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ConnectedBioEntitiesList', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders loading indicator when searchModelElementLoading is pending', () => {
    renderComponent({
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'pending',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [],
          loading: 'pending',
          error: DEFAULT_ERROR,
        },
      },
    });

    const loadingIndicator = screen.getByTestId('loading-indicator');

    expect(loadingIndicator).toBeVisible();
  });

  it('renders list of bio entities when bioEntityData is available', () => {
    renderComponent({
      modelElements: {
        data: {
          0: {
            data: [modelElementFixture],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: {
          data: [
            {
              searchQueryElement: '',
              loading: 'idle',
              error: DEFAULT_ERROR,
              data: [{ modelElement: modelElementFixture, perfect: true }],
            },
          ],
          loading: 'idle',
          error: DEFAULT_ERROR,
        },
      },
    });

    expect(screen.queryByTestId('loading-indicator')).not.toBeInTheDocument();
    expect(screen.queryByText(modelElementFixture.name)).toBeVisible();
  });
});

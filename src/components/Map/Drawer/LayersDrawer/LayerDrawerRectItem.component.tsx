import React, { JSX, useMemo } from 'react';
import { LayerRect } from '@/types/models';
import { Icon } from '@/shared/Icon';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { mapEditToolsLayerObjectSelector } from '@/redux/mapEditTools/mapEditTools.selectors';
import { hasPrivilegeToWriteProjectSelector } from '@/redux/user/user.selectors';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';
import { LayersDrawerObjectActions } from '@/components/Map/Drawer/LayersDrawer/LayersDrawerObjectActions.component';

interface LayersDrawerRectItemProps {
  layerRect: LayerRect;
  moveToFront: () => void;
  moveToBack: () => void;
  removeObject: () => void;
  centerObject: () => void;
  editObject: () => void;
  isLayerVisible: boolean;
  isLayerActive: boolean;
}

export const LayersDrawerRectItem = ({
  layerRect,
  moveToFront,
  moveToBack,
  removeObject,
  centerObject,
  editObject,
  isLayerVisible,
  isLayerActive,
}: LayersDrawerRectItemProps): JSX.Element | null => {
  const dispatch = useAppDispatch();
  const activeLayerObject = useAppSelector(mapEditToolsLayerObjectSelector);
  const hasPrivilegeToWriteProject = useAppSelector(hasPrivilegeToWriteProjectSelector);

  const showActions = useMemo(() => {
    return activeLayerObject?.id === layerRect.id;
  }, [activeLayerObject?.id, layerRect.id]);

  const canSelectItem = useMemo(() => {
    return isLayerVisible && isLayerActive && hasPrivilegeToWriteProject;
  }, [isLayerVisible, isLayerActive, hasPrivilegeToWriteProject]);

  const selectItem = useMemo(() => {
    return (): void => {
      if (canSelectItem) {
        dispatch(mapEditToolsSetLayerObject(layerRect));
      }
    };
  }, [canSelectItem, dispatch, layerRect]);

  const handleKeyPress = (): void => {};

  return (
    <div
      className="flex min-h-[24px] items-center justify-between gap-2"
      id={`layer-rect-item-${layerRect.id}`}
    >
      <div
        className={`flex gap-2 ${canSelectItem ? 'cursor-pointer' : 'cursor-default'}`}
        onClick={selectItem}
        tabIndex={0}
        onKeyDown={handleKeyPress}
        role="button"
      >
        <Icon name="rectangle" className="shrink-0" />
        <span className="truncate">rectangle - {layerRect.id}</span>
      </div>
      {showActions && (
        <LayersDrawerObjectActions
          moveToFront={moveToFront}
          moveToBack={moveToBack}
          removeObject={removeObject}
          centerObject={centerObject}
          editObject={editObject}
        />
      )}
    </div>
  );
};

import React, { JSX, useMemo } from 'react';
import { LayerImage } from '@/types/models';
import { Icon } from '@/shared/Icon';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { glyphFileNameByIdSelector } from '@/redux/glyphs/glyphs.selectors';
import { LayersDrawerObjectActions } from '@/components/Map/Drawer/LayersDrawer/LayersDrawerObjectActions.component';
import { mapEditToolsLayerObjectSelector } from '@/redux/mapEditTools/mapEditTools.selectors';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { hasPrivilegeToWriteProjectSelector } from '@/redux/user/user.selectors';

interface LayersDrawerImageItemProps {
  layerImage: LayerImage;
  moveToFront: () => void;
  moveToBack: () => void;
  removeObject: () => void;
  centerObject: () => void;
  editObject: () => void;
  isLayerVisible: boolean;
  isLayerActive: boolean;
}

export const LayersDrawerImageItem = ({
  layerImage,
  moveToFront,
  moveToBack,
  removeObject,
  centerObject,
  editObject,
  isLayerVisible,
  isLayerActive,
}: LayersDrawerImageItemProps): JSX.Element | null => {
  const dispatch = useAppDispatch();
  const activeLayerImage = useAppSelector(mapEditToolsLayerObjectSelector);
  const fileName = useAppSelector(state => glyphFileNameByIdSelector(state, layerImage.glyph));
  const hasPrivilegeToWriteProject = useAppSelector(hasPrivilegeToWriteProjectSelector);

  const showActions = useMemo(() => {
    return activeLayerImage?.id === layerImage.id;
  }, [activeLayerImage?.id, layerImage.id]);

  const canSelectItem = useMemo(() => {
    return isLayerVisible && isLayerActive && hasPrivilegeToWriteProject;
  }, [isLayerVisible, isLayerActive, hasPrivilegeToWriteProject]);

  const selectItem = useMemo(() => {
    return (): void => {
      if (canSelectItem) {
        dispatch(mapEditToolsSetLayerObject(layerImage));
      }
    };
  }, [canSelectItem, dispatch, layerImage]);

  const handleKeyPress = (): void => {};

  return (
    <div
      className="flex min-h-[24px] items-center justify-between gap-2"
      id={`layer-image-item-${layerImage.id}`}
    >
      <div
        className={`flex gap-2 ${canSelectItem ? 'cursor-pointer' : 'cursor-default'}`}
        onClick={selectItem}
        tabIndex={0}
        onKeyDown={handleKeyPress}
        role="button"
      >
        <Icon className="shrink-0" name="image" />
        <span className={`min-w-0 flex-1 truncate ${showActions ? 'max-w-[205px]' : ''}`}>
          {fileName}
        </span>
      </div>
      {showActions && (
        <LayersDrawerObjectActions
          moveToFront={moveToFront}
          moveToBack={moveToBack}
          removeObject={removeObject}
          centerObject={centerObject}
          editObject={editObject}
        />
      )}
    </div>
  );
};

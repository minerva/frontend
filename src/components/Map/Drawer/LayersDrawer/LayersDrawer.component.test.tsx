import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import { openedExportDrawerFixture } from '@/redux/drawer/drawerFixture';
import { LayersDrawer } from '@/components/Map/Drawer/LayersDrawer/LayersDrawer.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <LayersDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ExportDrawer - component', () => {
  it('should display drawer heading', () => {
    renderComponent();

    expect(screen.getByText('Layers')).toBeInTheDocument();
  });

  it('should close drawer after clicking close button', () => {
    const { store } = renderComponent({
      drawer: openedExportDrawerFixture,
    });
    const closeButton = screen.getByRole('close-drawer-button');

    closeButton.click();

    const {
      drawer: { isOpen },
    } = store.getState();

    expect(isOpen).toBe(false);
  });
});

import React, { JSX, useMemo } from 'react';
import { LayerText } from '@/types/models';
import { Icon } from '@/shared/Icon';
import { LayersDrawerObjectActions } from '@/components/Map/Drawer/LayersDrawer/LayersDrawerObjectActions.component';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { mapEditToolsLayerObjectSelector } from '@/redux/mapEditTools/mapEditTools.selectors';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';
import { hasPrivilegeToWriteProjectSelector } from '@/redux/user/user.selectors';

interface LayersDrawerTextItemProps {
  layerText: LayerText;
  moveToFront: () => void;
  moveToBack: () => void;
  removeObject: () => void;
  centerObject: () => void;
  editObject: () => void;
  isLayerVisible: boolean;
  isLayerActive: boolean;
}

export const LayersDrawerTextItem = ({
  layerText,
  moveToFront,
  moveToBack,
  removeObject,
  centerObject,
  editObject,
  isLayerVisible,
  isLayerActive,
}: LayersDrawerTextItemProps): JSX.Element | null => {
  const dispatch = useAppDispatch();
  const activeLayerObject = useAppSelector(mapEditToolsLayerObjectSelector);
  const hasPrivilegeToWriteProject = useAppSelector(hasPrivilegeToWriteProjectSelector);

  const showActions = useMemo(() => {
    return activeLayerObject?.id === layerText.id;
  }, [activeLayerObject?.id, layerText.id]);

  const canSelectItem = useMemo(() => {
    return isLayerVisible && isLayerActive && hasPrivilegeToWriteProject;
  }, [isLayerVisible, isLayerActive, hasPrivilegeToWriteProject]);

  const selectItem = useMemo(() => {
    return (): void => {
      if (canSelectItem) {
        dispatch(mapEditToolsSetLayerObject(layerText));
      }
    };
  }, [canSelectItem, dispatch, layerText]);

  const handleKeyPress = (): void => {};

  return (
    <div
      className="flex min-h-[24px] items-center justify-between gap-2"
      id={`layer-text-item-${layerText.id}`}
    >
      <div
        className={`flex gap-2 ${canSelectItem ? 'cursor-pointer' : 'cursor-default'}`}
        onClick={selectItem}
        tabIndex={0}
        onKeyDown={handleKeyPress}
        role="button"
      >
        <Icon name="text" className="shrink-0" />
        <span className="truncate">{layerText.notes}</span>
      </div>
      {showActions && (
        <LayersDrawerObjectActions
          moveToFront={moveToFront}
          moveToBack={moveToBack}
          removeObject={removeObject}
          centerObject={centerObject}
          editObject={editObject}
        />
      )}
    </div>
  );
};

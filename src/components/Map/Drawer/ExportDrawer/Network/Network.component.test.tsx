/* eslint-disable no-magic-numbers */
import { configurationFixture } from '@/models/fixtures/configurationFixture';
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { statisticsFixture } from '@/models/fixtures/statisticsFixture';
import { apiPath } from '@/redux/apiPath';
import { CONFIGURATION_INITIAL_STORE_MOCK } from '@/redux/configuration/configuration.mock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState } from '@/redux/store';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { InitialStoreState } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { HttpStatusCode } from 'axios';
import { act } from 'react-dom/test-utils';
import { MockStoreEnhanced } from 'redux-mock-store';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { COMPARTMENT_SBO_TERM } from '@/components/Map/MapViewer/MapViewer.constants';
import { MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';
import { NETWORK_COLUMNS } from '../ExportCompound/ExportCompound.constant';
import { Network } from './Network.component';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const renderComponent = (
  initialStore?: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <Network />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('Network - component', () => {
  it('should render all network sections', () => {
    renderComponent({
      ...INITIAL_STORE_STATE_MOCK,
      configuration: {
        ...CONFIGURATION_INITIAL_STORE_MOCK,
        main: {
          ...CONFIGURATION_INITIAL_STORE_MOCK.main,
          data: {
            ...configurationFixture,
            miriamTypes: {
              compartment_label: {
                commonName: 'Compartment',
                homepage: '',
                registryIdentifier: '',
                uris: [''],
              },
            },
          },
        },
      },
      statistics: {
        data: {
          ...statisticsFixture,
          reactionAnnotations: {
            compartment_label: 1,
            pathway: 0,
          },
        },
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
    });
    const annotations = screen.getByText('Select annotations');
    const includedCompartmentPathways = screen.getByText('Select included compartment / pathways');
    const excludedCompartmentPathways = screen.getByText('Select excluded compartment / pathways');
    const downloadButton = screen.getByText('Download');

    expect(annotations).toBeVisible();
    expect(includedCompartmentPathways).toBeVisible();
    expect(excludedCompartmentPathways).toBeVisible();
    expect(downloadButton).toBeVisible();
  });

  it('should handle download button click and dispatch proper data', async () => {
    const FIRST_COMPARMENT_PATHWAY_NAME = 'test1';
    const FIRST_COMPARMENT_PATHWAY_ID = 1;
    const SECOND_COMPARMENT_PATHWAY_NAME = 'test2';
    const SECOND_COMPARMENT_PATHWAY_ID = 2;
    mockedAxiosClient.onPost(apiPath.downloadNetworkCsv()).reply(HttpStatusCode.Ok, 'test');
    const { store } = renderComponent({
      ...INITIAL_STORE_STATE_MOCK,
      configuration: {
        ...CONFIGURATION_INITIAL_STORE_MOCK,
        main: {
          ...CONFIGURATION_INITIAL_STORE_MOCK.main,
          data: {
            ...configurationFixture,
            miriamTypes: {
              reaction: {
                commonName: 'Reaction Label',
                homepage: '',
                registryIdentifier: '',
                uris: [''],
              },
            },
          },
        },
      },
      statistics: {
        data: {
          ...statisticsFixture,
          elementAnnotations: {
            compartment: 1,
            pathway: 0,
          },
          reactionAnnotations: {
            reaction: 2,
            path: 0,
          },
        },
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
      models: {
        data: modelsFixture,
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
      modelElements: {
        data: {
          0: {
            data: [
              {
                ...modelElementFixture,
                id: FIRST_COMPARMENT_PATHWAY_ID,
                name: FIRST_COMPARMENT_PATHWAY_NAME,
                sboTerm: COMPARTMENT_SBO_TERM,
              },
              {
                ...modelElementFixture,
                id: SECOND_COMPARMENT_PATHWAY_ID,
                name: SECOND_COMPARMENT_PATHWAY_NAME,
                sboTerm: COMPARTMENT_SBO_TERM,
              },
            ],
            loading: 'succeeded',
            error: { message: '', name: '' },
          },
        },
        search: MODEL_ELEMENTS_SEARCH_INITIAL_STATE_MOCK,
      } as ModelElementsState,
    });

    const annotations = screen.getByText('Select annotations');

    await act(() => {
      annotations.click();
    });
    const annotationInput = screen.getByLabelText('Reaction Label');

    await act(() => {
      annotationInput.click();
    });

    expect(annotationInput).toBeChecked();

    const includedCompartmentPathways = screen.getByText('Select included compartment / pathways');

    await act(() => {
      includedCompartmentPathways.click();
    });
    const includedCompartmentPathwaysInput = screen.getAllByLabelText(
      FIRST_COMPARMENT_PATHWAY_NAME,
    )[0];

    await act(() => {
      includedCompartmentPathwaysInput.click();
    });

    expect(includedCompartmentPathwaysInput).toBeChecked();

    const excludedCompartmentPathways = screen.getByText('Select excluded compartment / pathways');

    await act(() => {
      excludedCompartmentPathways.click();
    });
    const excludedCompartmentPathwaysInput = screen.getAllByLabelText(
      SECOND_COMPARMENT_PATHWAY_NAME,
    )[1];

    await act(() => {
      excludedCompartmentPathwaysInput.click();
    });

    expect(excludedCompartmentPathwaysInput).toBeChecked();

    const downloadButton = screen.getByText('Download');

    await act(() => {
      downloadButton.click();
    });

    const actions = store.getActions();

    const firstAction = actions[0];
    expect(firstAction.meta.arg).toEqual({
      columns: NETWORK_COLUMNS,
      submaps: modelsFixture.map(item => item.id),
      annotations: ['reaction'],
      includedCompartmentIds: [FIRST_COMPARMENT_PATHWAY_ID],
      excludedCompartmentIds: [SECOND_COMPARMENT_PATHWAY_ID],
    });
  });
});

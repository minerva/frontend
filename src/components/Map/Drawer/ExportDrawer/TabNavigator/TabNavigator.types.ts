import { TAB_NAMES } from './TabNavigator.constants';

export type TabNames = (typeof TAB_NAMES)[keyof typeof TAB_NAMES];

import { fireEvent, render, screen } from '@testing-library/react';
import { TAB_NAMES } from './TabNavigator.constants';
import { TabNavigator } from './TabNavigator.component';

const mockOnTabChange = jest.fn();

describe('TabNavigator - component', () => {
  beforeEach(() => {
    mockOnTabChange.mockReset();
  });
  it('should render TabNavigator with correct tabs', () => {
    render(<TabNavigator activeTab="ELEMENTS" onTabChange={mockOnTabChange} />);

    Object.values(TAB_NAMES).forEach(label => {
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });

  it('should change tabs correctly', () => {
    render(<TabNavigator activeTab="ELEMENTS" onTabChange={mockOnTabChange} />);

    fireEvent.click(screen.getByText(/NETWORK/i));
    expect(mockOnTabChange).toHaveBeenCalledWith('NETWORK');

    fireEvent.click(screen.getByText(/GRAPHICS/i));
    expect(mockOnTabChange).toHaveBeenCalledWith('GRAPHICS');
  });

  it('should set initial active tab', () => {
    render(<TabNavigator activeTab="NETWORK" onTabChange={mockOnTabChange} />);
    const currentTab = screen.getByRole('button', { current: true });
    const networkTab = screen.getByText(/NETWORK/i);

    expect(currentTab).toBe(networkTab);
  });
});

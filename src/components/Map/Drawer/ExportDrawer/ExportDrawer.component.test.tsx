import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import { openedExportDrawerFixture } from '@/redux/drawer/drawerFixture';
import { act } from 'react-dom/test-utils';
import { ExportDrawer } from './ExportDrawer.component';
import { TAB_NAMES } from './TabNavigator/TabNavigator.constants';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ExportDrawer />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ExportDrawer - component', () => {
  it('should display drawer heading and tab names', () => {
    renderComponent();

    expect(screen.getByText('Export')).toBeInTheDocument();

    Object.values(TAB_NAMES).forEach(label => {
      expect(screen.getByText(label)).toBeInTheDocument();
    });
  });
  it('should close drawer after clicking close button', () => {
    const { store } = renderComponent({
      drawer: openedExportDrawerFixture,
    });
    const closeButton = screen.getByRole('close-drawer-button');

    closeButton.click();

    const {
      drawer: { isOpen },
    } = store.getState();

    expect(isOpen).toBe(false);
  });
  it('should set elements as initial tab', () => {
    renderComponent();

    expect(screen.getByTestId('elements-tab')).toBeInTheDocument();
  });
  it('should set correct tab on tab change', () => {
    renderComponent();
    const currentTab = screen.getByRole('button', { current: true });
    const networkTab = screen.getByText(/network/i);
    const elementsTab = screen.getByTestId('elements-tab');
    expect(currentTab).not.toBe(networkTab);
    expect(screen.getByTestId('elements-tab')).toBeInTheDocument();

    act(() => {
      networkTab.click();
    });
    expect(screen.getByRole('button', { current: true })).toBe(networkTab);
    expect(elementsTab).not.toBeInTheDocument();
  });
});

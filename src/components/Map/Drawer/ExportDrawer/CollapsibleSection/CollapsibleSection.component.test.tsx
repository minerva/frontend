import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { CollapsibleSection } from './CollapsibleSection.component';

describe('CollapsibleSection - component', () => {
  it('should render with title and content', () => {
    render(
      <CollapsibleSection title="Section">
        <div>Content</div>
      </CollapsibleSection>,
    );

    expect(screen.getByText('Section')).toBeInTheDocument();
    expect(screen.getByText('Content')).toBeInTheDocument();
  });

  it('should collapse and expands on button click', () => {
    render(
      <CollapsibleSection title="Test Section">
        <div>Test Content</div>
      </CollapsibleSection>,
    );

    const button = screen.getByText('Test Section');
    const content = screen.getByText('Test Content');

    expect(content).not.toBeVisible();

    // Expand
    fireEvent.click(button);
    expect(content).toBeVisible();

    // Collapse
    fireEvent.click(button);
    expect(content).not.toBeVisible();
  });
});

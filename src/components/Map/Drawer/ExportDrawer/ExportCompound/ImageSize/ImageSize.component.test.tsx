/* eslint-disable no-magic-numbers */
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { fireEvent, render, screen } from '@testing-library/react';
import { Export } from '../ExportCompound.component';
import { ImageSize } from './ImageSize.component';
import { ImageSize as ImageSizeType } from './ImageSize.types';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);

  return (
    render(
      <Wrapper>
        <Export>
          <ImageSize />
        </Export>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ImageSize - component', () => {
  describe('width input', () => {
    it('renders input with valid value', () => {
      renderComponent();

      const widthInput: HTMLInputElement = screen.getByLabelText('export graphics width input');
      expect(widthInput).toBeInTheDocument();
      expect(widthInput.value).toBe('600');
    });

    // MAX_WIDTH 600
    // MAX_HEIGHT 200
    const widthCases: [number, ImageSizeType][] = [
      [
        // default
        600,
        {
          width: 600,
          height: 200,
        },
      ],
      [
        // aspect ratio test
        100,
        {
          width: 100,
          height: 33,
        },
      ],
      [
        // transform to integer
        120.2137,
        {
          width: 120,
          height: 40,
        },
      ],
      [
        // max width
        997,
        {
          width: 600,
          height: 200,
        },
      ],
    ];

    it.each(widthCases)(
      'handles input events by setting correct values',
      async (newWidth, newImageSize) => {
        renderComponent();

        const widthInput: HTMLInputElement = screen.getByLabelText('export graphics width input');
        const heightInput: HTMLInputElement = screen.getByLabelText('export graphics height input');

        fireEvent.change(widthInput, { target: { value: `${newWidth}` } });

        expect(widthInput).toHaveValue(newImageSize.width);
        expect(heightInput).toHaveValue(newImageSize.height);
      },
    );
  });

  describe('height input', () => {
    it('renders input', () => {
      renderComponent();

      const heightInput: HTMLInputElement = screen.getByLabelText('export graphics height input');
      expect(heightInput).toBeInTheDocument();
      expect(heightInput.value).toBe('200');
    });

    // MAX_WIDTH 600
    // MAX_HEIGHT 200
    const heightCases: [number, ImageSizeType][] = [
      [
        // default
        200,
        {
          width: 600,
          height: 200,
        },
      ],
      [
        // aspect ratio test
        100,
        {
          width: 300,
          height: 100,
        },
      ],
      [
        // transform to integer
        120.2137,
        {
          width: 361,
          height: 120,
        },
      ],
      [
        // max height
        997,
        {
          width: 600,
          height: 200,
        },
      ],
    ];

    it.each(heightCases)(
      'handles input events by setting correct values',
      async (newHeight, newImageSize) => {
        renderComponent();

        const widthInput: HTMLInputElement = screen.getByLabelText('export graphics width input');
        const heightInput: HTMLInputElement = screen.getByLabelText('export graphics height input');

        fireEvent.change(heightInput, { target: { value: `${newHeight}` } });

        expect(widthInput).toHaveValue(newImageSize.width);
        expect(heightInput).toHaveValue(newImageSize.height);
      },
    );
  });
});

export interface ImageSize {
  width: number;
  height: number;
}

export interface ModelAspectRatios {
  vertical: number;
  horizontal: number;
}

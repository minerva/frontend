import { MapModel } from '@/types/models';
import { DEFAULT_MODEL_ASPECT_RATIOS } from '../ImageSize.constants';
import { ModelAspectRatios } from '../ImageSize.types';
import { getModelAspectRatios } from './useModelAspectRatios';

describe('useModelAspectRatios - hook', () => {
  describe('when model is not present', () => {
    const model = undefined;

    it('should return default model aspect ratio', () => {
      const result = getModelAspectRatios(model);
      expect(result).toEqual(DEFAULT_MODEL_ASPECT_RATIOS);
    });
  });

  describe('when model is present', () => {
    const modelCases: [Pick<MapModel, 'width' | 'height'>, ModelAspectRatios][] = [
      [
        {
          width: 1000,
          height: 500,
        },
        {
          vertical: 0.5,
          horizontal: 2,
        },
      ],
      [
        {
          width: 4200,
          height: 420,
        },
        {
          vertical: 0.1,
          horizontal: 10,
        },
      ],
    ];

    it.each(modelCases)('should return valid model aspect ratio', (model, expectedResult) => {
      const result = getModelAspectRatios(model);
      expect(result).toEqual(expectedResult);
    });
  });
});

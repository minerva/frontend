/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { renderHook } from '@testing-library/react';
import { EXPORT_CONTEXT_DEFAULT_VALUE } from '../../ExportCompound.constant';
import { getExportContextWithReduxWrapper } from '../../utils/getExportContextWithReduxWrapper';
import { DEFAULT_IMAGE_SIZE } from '../ImageSize.constants';
import { ImageSize } from '../ImageSize.types';
import { useImageSize } from './useImageSize';

describe('useImageSize - hook', () => {
  describe('when there is no selected model', () => {
    const { Wrapper } = getExportContextWithReduxWrapper(
      {
        ...EXPORT_CONTEXT_DEFAULT_VALUE,
        data: {
          ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
          models: [],
        },
      },
      {
        models: {
          data: [],
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      },
    );

    const { result } = renderHook(() => useImageSize(), {
      wrapper: Wrapper,
    });

    it('should should return default image size', () => {
      const { width, height } = result.current || {};
      expect({ width, height }).toEqual(DEFAULT_IMAGE_SIZE);
    });
  });

  describe('when there is a selected model', () => {
    const selectedModel = MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT];
    const setImageSize = jest.fn();

    const { Wrapper } = getExportContextWithReduxWrapper(
      {
        ...EXPORT_CONTEXT_DEFAULT_VALUE,
        setImageSize,
        data: {
          ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
          models: [
            {
              id: selectedModel.id.toString(),
              label: selectedModel.name,
            },
          ],
          imageSize: DEFAULT_IMAGE_SIZE,
        },
      },
      {
        models: {
          data: MODELS_MOCK_SHORT,
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      },
    );

    renderHook(() => useImageSize(), {
      wrapper: Wrapper,
    });

    it('should should set size of selected model', () => {
      expect(setImageSize).toHaveBeenCalledWith({
        width: 26779,
        height: 13503,
      });
    });
  });

  describe('when always', () => {
    describe('handleChangeHeight', () => {
      const selectedModel = MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT];
      const setImageSize = jest.fn();

      const { Wrapper } = getExportContextWithReduxWrapper(
        {
          ...EXPORT_CONTEXT_DEFAULT_VALUE,
          setImageSize,
          data: {
            ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
            models: [
              {
                id: selectedModel.id.toString(),
                label: selectedModel.name,
              },
            ],
            imageSize: DEFAULT_IMAGE_SIZE,
          },
        },
        {
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        },
      );

      const {
        result: {
          current: { handleChangeHeight },
        },
      } = renderHook(() => useImageSize(), {
        wrapper: Wrapper,
      });

      // MAX_WIDTH 26779.25
      // MAX_HEIGHT 13503.0

      const heightCases: [number, ImageSize][] = [
        [
          // aspect ratio test
          1000,
          {
            width: 1983,
            height: 1000,
          },
        ],
        [
          // transform to integer
          997.2137,
          {
            width: 1978,
            height: 997,
          },
        ],
        [
          // max height
          26779000,
          {
            width: 26779,
            height: 13503,
          },
        ],
      ];

      it.each(heightCases)(
        'should set valid height and width values',
        (newHeight, newImageSize) => {
          handleChangeHeight(newHeight);

          expect(setImageSize).toHaveBeenLastCalledWith(newImageSize);
        },
      );
    });

    describe('handleChangeWidth', () => {
      const selectedModel = MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT];
      const setImageSize = jest.fn();

      const { Wrapper } = getExportContextWithReduxWrapper(
        {
          ...EXPORT_CONTEXT_DEFAULT_VALUE,
          setImageSize,
          data: {
            ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
            models: [
              {
                id: selectedModel.id.toString(),
                label: selectedModel.name,
              },
            ],
            imageSize: DEFAULT_IMAGE_SIZE,
          },
        },
        {
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        },
      );

      const {
        result: {
          current: { handleChangeWidth },
        },
      } = renderHook(() => useImageSize(), {
        wrapper: Wrapper,
      });

      // MAX_WIDTH 26779.25
      // MAX_HEIGHT 13503.0

      const widthCases: [number, ImageSize][] = [
        [
          // aspect ratio test
          1000,
          {
            width: 1000,
            height: 504,
          },
        ],
        [
          // transform to integer
          997.2137,
          {
            width: 997,
            height: 503,
          },
        ],
        [
          // max width
          26779000,
          {
            width: 26779,
            height: 13503,
          },
        ],
      ];

      it.each(widthCases)('should set valid height and width values', (newWidth, newImageSize) => {
        handleChangeWidth(newWidth);

        expect(setImageSize).toHaveBeenLastCalledWith(newImageSize);
      });
    });
  });
});

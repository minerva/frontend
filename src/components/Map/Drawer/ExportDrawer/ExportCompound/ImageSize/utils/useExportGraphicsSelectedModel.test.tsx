import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { renderHook } from '@testing-library/react';
import { EXPORT_CONTEXT_DEFAULT_VALUE } from '../../ExportCompound.constant';
import { getExportContextWithReduxWrapper } from '../../utils/getExportContextWithReduxWrapper';
import { useExportGraphicsSelectedModel } from './useExportGraphicsSelectedModel';

describe('useExportGraphicsSelectedModel - util', () => {
  describe('when current selected models is empty', () => {
    const { Wrapper } = getExportContextWithReduxWrapper(
      {
        ...EXPORT_CONTEXT_DEFAULT_VALUE,
        data: {
          ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
          models: [],
        },
      },
      {
        models: {
          data: [],
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      },
    );

    const { result } = renderHook(() => useExportGraphicsSelectedModel(), {
      wrapper: Wrapper,
    });

    it('should return undefined', () => {
      expect(result.current).toBeUndefined();
    });
  });

  describe('when current selected models has one element', () => {
    describe('when redux models has selected model', () => {
      const selectedModel = MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT];

      const { Wrapper } = getExportContextWithReduxWrapper(
        {
          ...EXPORT_CONTEXT_DEFAULT_VALUE,
          data: {
            ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
            models: [
              {
                id: selectedModel.id.toString(),
                label: selectedModel.name,
              },
            ],
          },
        },
        {
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        },
      );

      const { result } = renderHook(() => useExportGraphicsSelectedModel(), {
        wrapper: Wrapper,
      });

      it('should return valid model from redux', () => {
        expect(result.current).toEqual(selectedModel);
      });
    });

    describe('when redux models has not selected model', () => {
      const { Wrapper } = getExportContextWithReduxWrapper(
        {
          ...EXPORT_CONTEXT_DEFAULT_VALUE,
          data: {
            ...EXPORT_CONTEXT_DEFAULT_VALUE.data,
            models: [],
          },
        },
        {
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        },
      );

      const { result } = renderHook(() => useExportGraphicsSelectedModel(), {
        wrapper: Wrapper,
      });

      it('should return undefined', () => {
        expect(result.current).toBeUndefined();
      });
    });
  });
});

import { getAnnotationsCheckboxElements } from './Annotations.utils';

describe('getAnnotationsCheckboxElements', () => {
  const statisticsMock = {
    elementAnnotations: {
      chebi: 2,
      mesh: 0,
      reactome: 1,
    },
    publications: 1234,
    reactionAnnotations: {
      brenda: 0,
      reactome: 3,
      rhea: 1,
    },
  };

  const miramiTypeMock = {
    commonName: 'Name',
    homepage: '',
    registryIdentifier: '',
    uris: [''],
  };

  const miramiTypesMock = {
    chebi: {
      ...miramiTypeMock,
      commonName: 'Chebi',
    },
    mesh: {
      ...miramiTypeMock,
      commonName: 'MeSH',
    },
    reactome: {
      ...miramiTypeMock,
      commonName: 'Reactome',
    },
    rhea: {
      ...miramiTypeMock,
      commonName: 'Rhea',
    },
    brenda: {
      ...miramiTypeMock,
      commonName: 'BRENDA',
    },
    gene_ontology: {
      ...miramiTypeMock,
      commonName: 'Gene Ontology',
    },
  };

  it('returns an empty array when statistics or miramiTypes are undefined', () => {
    const result = getAnnotationsCheckboxElements({
      type: 'Elements',
      statistics: undefined,
      miramiTypes: undefined,
    });
    expect(result).toEqual([]);
  });

  it('returns checkbox elements for element annotations sorted by label', () => {
    const result = getAnnotationsCheckboxElements({
      type: 'Elements',
      statistics: statisticsMock,
      miramiTypes: miramiTypesMock,
    });
    expect(result).toEqual([
      { id: 'chebi', label: 'Chebi' },
      { id: 'reactome', label: 'Reactome' },
    ]);
  });

  it('returns checkbox elements for reaction annotations sorted by count', () => {
    const result = getAnnotationsCheckboxElements({
      type: 'Network',
      statistics: statisticsMock,
      miramiTypes: miramiTypesMock,
    });
    expect(result).toEqual([
      { id: 'reactome', label: 'Reactome' },
      { id: 'rhea', label: 'Rhea' },
    ]);
  });

  it('returns an empty array when no annotations have count greater than 0', () => {
    const statisticsMockEmpty = {
      elementAnnotations: { annotation1: 0, annotation2: 0 },
      publications: 0,
      reactionAnnotations: { annotation1: 0, annotation2: 0 },
    };
    const result = getAnnotationsCheckboxElements({
      type: 'Elements',
      statistics: statisticsMockEmpty,
      miramiTypes: miramiTypesMock,
    });
    expect(result).toEqual([]);
  });
});

import { ANNOTATIONS_TYPE } from '../ExportCompound.constant';

export type AnnotationsType = (typeof ANNOTATIONS_TYPE)[keyof typeof ANNOTATIONS_TYPE];

import { render, screen, waitFor } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { statisticsFixture } from '@/models/fixtures/statisticsFixture';
import { act } from 'react-dom/test-utils';
import { CONFIGURATION_INITIAL_STORE_MOCK } from '@/redux/configuration/configuration.mock';
import { configurationFixture } from '@/models/fixtures/configurationFixture';
import { Annotations } from './Annotations.component';
import { ANNOTATIONS_TYPE } from '../ExportCompound.constant';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Annotations type={ANNOTATIONS_TYPE.ELEMENTS} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('Annotations - component', () => {
  it('should display annotations checkboxes when fetching data is successful', async () => {
    renderComponent({
      configuration: {
        ...CONFIGURATION_INITIAL_STORE_MOCK,
        main: {
          ...CONFIGURATION_INITIAL_STORE_MOCK.main,
          data: {
            ...configurationFixture,
            miriamTypes: {
              compartment_label: {
                commonName: 'Compartment',
                homepage: '',
                registryIdentifier: '',
                uris: [''],
              },
            },
          },
        },
      },
      statistics: {
        data: {
          ...statisticsFixture,
          elementAnnotations: {
            compartment_label: 1,
            pathway: 0,
          },
        },
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeVisible();

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Select annotations')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByTestId('checkbox-filter')).toBeInTheDocument();
      expect(screen.getByLabelText('Compartment')).toBeInTheDocument();
      expect(screen.getByLabelText('search-input')).toBeInTheDocument();
    });
  });
  it('should not display annotations checkboxes when fetching data fails', async () => {
    renderComponent({
      statistics: {
        data: undefined,
        loading: 'failed',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Select annotations')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });
  it('should not display annotations checkboxes when fetched data is empty object', async () => {
    renderComponent({
      statistics: {
        data: {
          ...statisticsFixture,
          elementAnnotations: {},
        },
        loading: 'failed',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Select annotations')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });

  it('should display loading message when fetching data is pending', async () => {
    renderComponent({
      statistics: {
        data: undefined,
        loading: 'pending',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Select annotations')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });
});

import { ZERO } from '@/constants/common';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { useContext } from 'react';
import {
  compartmentPathwaysSelector,
  modelElementsAnyModelLoadingSelector,
} from '@/redux/modelElements/modelElements.selector';
import { CheckboxFilter } from '../../CheckboxFilter';
import { CollapsibleSection } from '../../CollapsibleSection';
import { ExportContext } from '../ExportCompound.context';
import { getCompartmentPathwaysCheckboxElements } from '../utils/getCompartmentPathwaysCheckboxElements';

export const IncludedCompartmentPathways = (): React.ReactNode => {
  const { setIncludedCompartmentPathways, data } = useContext(ExportContext);
  const currentIncludedCompartmentPathways = data.includedCompartmentPathways;
  const compartmentPathways = useAppSelector(compartmentPathwaysSelector);
  const isPending = useAppSelector(modelElementsAnyModelLoadingSelector);
  const checkboxElements = getCompartmentPathwaysCheckboxElements('included', compartmentPathways);

  return (
    <CollapsibleSection title="Select included compartment / pathways">
      {isPending && <p>Loading...</p>}
      {!isPending && checkboxElements && checkboxElements.length > ZERO && (
        <CheckboxFilter
          options={checkboxElements}
          currentOptions={currentIncludedCompartmentPathways}
          onCheckedChange={setIncludedCompartmentPathways}
        />
      )}
    </CollapsibleSection>
  );
};

/* eslint-disable no-magic-numbers */
import { getNetworkDownloadBodyRequest } from './getNetworkBodyRequest';

describe('getNetworkDownloadBodyRequest', () => {
  it('should return the correct DownloadBodyRequest object', () => {
    const columns = ['column1', 'column2'];
    const modelIds = [1, 2, 3];
    const annotations = [
      { id: 'Annotation 1', label: 'Annotation 1' },
      { id: 'Annotation 2', label: 'Annotation 2' },
    ];
    const includedCompartmentPathways = [
      { id: 'include-7', label: 'Compartment 1' },
      { id: 'include-8', label: 'Compartment 2' },
    ];
    const excludedCompartmentPathways = [
      { id: 'exclude-9', label: 'Compartment 3' },
      { id: 'exclude-10', label: 'Compartment 4' },
    ];

    const result = getNetworkDownloadBodyRequest({
      columns,
      modelIds,
      annotations,
      includedCompartmentPathways,
      excludedCompartmentPathways,
    });

    expect(result).toEqual({
      columns: ['column1', 'column2'],
      submaps: [1, 2, 3],
      annotations: ['Annotation 1', 'Annotation 2'],
      includedCompartmentIds: [7, 8],
      excludedCompartmentIds: [9, 10],
    });
  });
});

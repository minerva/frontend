/* eslint-disable no-magic-numbers */
import { getDownloadElementsBodyRequest } from './getDownloadElementsBodyRequest';

describe('getDownloadElementsBodyRequest', () => {
  it('should return the correct DownloadBodyRequest object', () => {
    // eslint-disable-next-line no-magic-numbers
    const modelIds = [1, 2, 3];
    const annotations = [
      { id: 'Annotation 1', label: 'Annotation 1' },
      { id: 'Annotation 2', label: 'Annotation 2' },
    ];
    const includedCompartmentPathways = [
      { id: 'include-7', label: 'Compartment 1' },
      { id: 'include-8', label: 'Compartment 2' },
    ];
    const excludedCompartmentPathways = [
      { id: 'exclude-9', label: 'Compartment 3' },
      { id: 'exclude-10', label: 'Compartment 4' },
    ];

    const result = getDownloadElementsBodyRequest({
      columns: ['Column 23', 'Column99'],
      modelIds,
      annotations,
      includedCompartmentPathways,
      excludedCompartmentPathways,
    });

    expect(result).toEqual({
      columns: ['Column 23', 'Column99'],
      // eslint-disable-next-line no-magic-numbers
      submaps: [1, 2, 3],
      annotations: ['Annotation 1', 'Annotation 2'],
      includedCompartmentIds: [7, 8],
      excludedCompartmentIds: [9, 10],
    });
  });
  it('should throw error if compartment id is not a number', () => {
    const modelIds = [1, 2, 3];
    const annotations = [
      { id: 'Annotation 1', label: 'Annotation 1' },
      { id: 'Annotation 2', label: 'Annotation 2' },
    ];
    const includedCompartmentPathways = [
      { id: '', label: 'Compartment 1' },
      { id: '', label: 'Compartment 2' },
    ];
    const excludedCompartmentPathways = [
      { id: '', label: 'Compartment 3' },
      { id: '', label: 'Compartment 4' },
    ];

    expect(() =>
      getDownloadElementsBodyRequest({
        columns: ['Column 23', 'Column99'],
        modelIds,
        annotations,
        includedCompartmentPathways,
        excludedCompartmentPathways,
      }),
    ).toThrow('compartment id is not a number');
  });
});

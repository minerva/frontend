/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT, ZERO } from '@/constants/common';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import { getModelExportZoom } from './getModelExportZoom';

describe('getModelExportZoom - util', () => {
  describe('when there is no model', () => {
    const model = undefined;
    const exportWidth = 100;

    it('should return return zero', () => {
      expect(getModelExportZoom(exportWidth, model)).toBe(ZERO);
    });
  });

  // Math.log2 of zero is -Infty
  describe('when model width is zero', () => {
    const model = {
      ...MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT],
      width: 0,
    };
    const exportWidth = 100;

    it('should return return zero', () => {
      expect(getModelExportZoom(exportWidth, model)).toBe(ZERO);
    });
  });

  describe('when model is present and model width > ZERO', () => {
    const model = MODELS_MOCK_SHORT[FIRST_ARRAY_ELEMENT];

    // MAX_WIDTH 26779.25
    // [zoom, width]
    const cases: [number, number][] = [
      [2, 100], // MIN ZOOM
      [2.7142, 420],
      [4.5391, 1488],
      [9, 80000000], // MAX ZOOM
    ];

    it.each(cases)('should return export zoom=%s for width=%s', (zoom, width) => {
      expect(getModelExportZoom(width, model)).toBeCloseTo(zoom);
    });
  });
});

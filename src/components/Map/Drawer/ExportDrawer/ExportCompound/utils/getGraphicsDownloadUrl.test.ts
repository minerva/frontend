import { BASE_API_URL, PROJECT_ID } from '@/constants';
import { GetGraphicsDownloadUrlProps, getGraphicsDownloadUrl } from './getGraphicsDownloadUrl';

describe('getGraphicsDownloadUrl - util', () => {
  const cases: [GetGraphicsDownloadUrlProps, string | undefined][] = [
    [{ overlayIds: [] }, undefined],
    [
      {
        overlayIds: [],
      },
      undefined,
    ],
    [
      {
        modelId: '30',
        overlayIds: [],
      },
      undefined,
    ],
    [
      {
        modelId: '30',
        handler: 'any.handler.image',
        overlayIds: [],
      },
      undefined,
    ],
    [
      {
        modelId: '30',
        handler: 'any.handler.image',
        zoom: 7,
        overlayIds: ['107'],
      },
      `${BASE_API_URL}/projects/${PROJECT_ID}/models/30:downloadImage?handlerClass=any.handler.image&zoomLevel=7&overlayIds=107`,
    ],
  ];

  it.each(cases)('should return valid result', (input, result) => {
    expect(getGraphicsDownloadUrl(input)).toBe(result);
  });
});

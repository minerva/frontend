/* eslint-disable no-magic-numbers */
import { ModelElement } from '@/types/models';
import { getCompartmentPathwaysCheckboxElements } from './getCompartmentPathwaysCheckboxElements';

describe('getCompartmentPathwaysCheckboxElements', () => {
  it('should return an empty array when given an empty items array', () => {
    const items: ModelElement[] = [];
    const result = getCompartmentPathwaysCheckboxElements('include', items);
    expect(result).toEqual([]);
  });

  it('should correctly extract unique names and corresponding ids from items', () => {
    const items = [
      { id: 1, name: 'Compartment A' },
      { id: 2, name: 'Compartment B' },
      { id: 3, name: 'Compartment A' },
      { id: 4, name: 'Compartment C' },
    ] as ModelElement[];

    const result = getCompartmentPathwaysCheckboxElements('test', items);

    expect(result).toEqual([
      { id: 'test-1', label: 'Compartment A' },
      { id: 'test-2', label: 'Compartment B' },
      { id: 'test-4', label: 'Compartment C' },
    ]);
  });
  it('should correctly extract unique names and corresponding ids from items and sorts them alphabetically', () => {
    const items = [
      { id: 1, name: 'Compartment C' },
      { id: 2, name: 'Compartment A' },
      { id: 3, name: 'Compartment B' },
      { id: 4, name: 'Compartment A' },
      { id: 5, name: 'Compartment D' },
    ] as ModelElement[];

    const result = getCompartmentPathwaysCheckboxElements('prefix', items);

    expect(result).toEqual([
      { id: 'prefix-2', label: 'Compartment A' },
      { id: 'prefix-3', label: 'Compartment B' },
      { id: 'prefix-1', label: 'Compartment C' },
      { id: 'prefix-5', label: 'Compartment D' },
    ]);
  });
});

/* eslint-disable no-magic-numbers */
import { extractAndParseNumberIdFromCompartment } from './extractAndParseNumberIdFromCompartment';

describe('extractAndParseNumberIdFromCompartment', () => {
  it('should extract and parse number id from compartment', () => {
    const compartment = { id: 'compartment-123', label: 'x' };
    const result = extractAndParseNumberIdFromCompartment(compartment);
    expect(result).toBe(123);
  });

  it('should handle id with non-numeric characters', () => {
    const compartment = { id: 'compartment-abc', label: 'x' };

    expect(() => extractAndParseNumberIdFromCompartment(compartment)).toThrowError(
      'compartment id is not a number',
    );
  });

  it('should handle missing id', () => {
    const compartment = { id: 'compartment-', label: 'x' };

    expect(() => extractAndParseNumberIdFromCompartment(compartment)).toThrowError(
      'compartment id is not a number',
    );
  });
});

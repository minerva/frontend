/* eslint-disable react/no-multi-comp */
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { EXPORT_CONTEXT_DEFAULT_VALUE } from '../ExportCompound.constant';
import { ExportContext } from '../ExportCompound.context';
import { ExportContextType } from '../ExportCompound.types';

interface WrapperProps {
  children: React.ReactNode;
}

export type ComponentWrapper = ({ children }: WrapperProps) => JSX.Element;

export type GetExportContextWithReduxWrapper = (
  contextValue?: ExportContextType,
  initialState?: InitialStoreState,
) => {
  Wrapper: ComponentWrapper;
  store: StoreType;
};

export const getExportContextWithReduxWrapper: GetExportContextWithReduxWrapper = (
  contextValue,
  initialState,
) => {
  const { Wrapper: ReduxWrapper, store } = getReduxWrapperWithStore(initialState);

  const ContextWrapper: ComponentWrapper = ({ children }) => {
    return (
      <ExportContext.Provider value={contextValue || EXPORT_CONTEXT_DEFAULT_VALUE}>
        {children}
      </ExportContext.Provider>
    );
  };

  const ContextWrapperWithRedux: ComponentWrapper = ({ children }) => {
    return (
      <ReduxWrapper>
        <ContextWrapper>{children}</ContextWrapper>
      </ReduxWrapper>
    );
  };

  return { Wrapper: ContextWrapperWithRedux, store };
};

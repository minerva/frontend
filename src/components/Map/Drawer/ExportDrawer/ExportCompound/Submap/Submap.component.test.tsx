/* eslint-disable no-magic-numbers */
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { Submap } from './Submap.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Submap />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const CHECKBOX_ELEMENT_NAME = modelsFixture[0].name;

describe('Submap - component', () => {
  it('should display submaps checkboxes when fetching data is successful', async () => {
    renderComponent({
      models: {
        data: modelsFixture,
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
    });

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Diagram')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByTestId('checkbox-filter')).toBeInTheDocument();
      expect(screen.getByLabelText('search-input')).toBeInTheDocument();
      expect(screen.getByLabelText(CHECKBOX_ELEMENT_NAME)).toBeInTheDocument();
    });
  });
  it('should not display submaps checkboxes when fetching data fails', async () => {
    renderComponent({
      models: {
        data: [],
        loading: 'failed',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Diagram')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });
  it('should not display submaps checkboxes when fetched data is empty', async () => {
    renderComponent({
      models: {
        data: [],
        loading: 'succeeded',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Diagram')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });

  it('should display loading message when fetching data is pending', async () => {
    renderComponent({
      models: {
        data: [],
        loading: 'pending',
        error: {
          message: '',
          name: '',
        },
      },
    });
    expect(screen.getByText('Diagram')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });
});

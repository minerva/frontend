import { createContext } from 'react';
import { EXPORT_CONTEXT_DEFAULT_VALUE } from './ExportCompound.constant';
import { ExportContextType } from './ExportCompound.types';

export const ExportContext = createContext<ExportContextType>(EXPORT_CONTEXT_DEFAULT_VALUE);

import { configurationFixture } from '@/models/fixtures/configurationFixture';
import {
  CONFIGURATION_IMAGE_FORMATS_MOCK,
  CONFIGURATION_IMAGE_FORMATS_TYPES_MOCK,
} from '@/models/mocks/configurationFormatsMock';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen, waitFor } from '@testing-library/react';
import { ImageFormat } from './ImageFormat.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ImageFormat />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ImageFormat - component', () => {
  it('should display formats checkboxes when fetching data is successful', async () => {
    renderComponent({
      configuration: {
        ...INITIAL_STORE_STATE_MOCK.configuration,
        main: {
          ...INITIAL_STORE_STATE_MOCK.configuration.main,
          data: {
            ...configurationFixture,
            imageFormats: CONFIGURATION_IMAGE_FORMATS_MOCK,
          },
        },
      },
    });

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Image format')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByTestId('checkbox-filter')).toBeInTheDocument();

      CONFIGURATION_IMAGE_FORMATS_TYPES_MOCK.map(formatName =>
        expect(screen.getByLabelText(formatName)).toBeInTheDocument(),
      );
    });
  });

  it('should not display formats checkboxes when fetching data fails', async () => {
    renderComponent({
      configuration: {
        ...INITIAL_STORE_STATE_MOCK.configuration,
        main: {
          ...INITIAL_STORE_STATE_MOCK.configuration.main,
          loading: 'failed',
        },
      },
    });

    expect(screen.getByText('Image format')).toBeInTheDocument();

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });

  it('should not display formats checkboxes when fetched data is empty', async () => {
    renderComponent({
      configuration: {
        ...INITIAL_STORE_STATE_MOCK.configuration,
        main: {
          ...INITIAL_STORE_STATE_MOCK.configuration.main,
          data: {
            ...configurationFixture,
            modelFormats: [],
            imageFormats: [],
          },
        },
      },
    });

    expect(screen.getByText('Image format')).toBeInTheDocument();

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });

  it('should display loading message when fetching data is pending', async () => {
    renderComponent({
      configuration: {
        ...INITIAL_STORE_STATE_MOCK.configuration,
        main: {
          ...INITIAL_STORE_STATE_MOCK.configuration.main,
          loading: 'pending',
        },
      },
    });

    expect(screen.getByText('Image format')).toBeInTheDocument();

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });
});

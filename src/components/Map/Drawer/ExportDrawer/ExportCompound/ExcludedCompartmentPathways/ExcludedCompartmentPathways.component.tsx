import { ZERO } from '@/constants/common';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { useContext } from 'react';
import {
  compartmentPathwaysSelector,
  modelElementsAnyModelLoadingSelector,
} from '@/redux/modelElements/modelElements.selector';
import { CheckboxFilter } from '../../CheckboxFilter';
import { CollapsibleSection } from '../../CollapsibleSection';
import { ExportContext } from '../ExportCompound.context';
import { getCompartmentPathwaysCheckboxElements } from '../utils/getCompartmentPathwaysCheckboxElements';

export const ExcludedCompartmentPathways = (): React.ReactNode => {
  const { setExcludedCompartmentPathways, data } = useContext(ExportContext);
  const currentExcludedCompartmentPathways = data.excludedCompartmentPathways;
  const compartmentPathways = useAppSelector(compartmentPathwaysSelector);
  const isPending = useAppSelector(modelElementsAnyModelLoadingSelector);
  const checkboxElements = getCompartmentPathwaysCheckboxElements('excluded', compartmentPathways);
  const isCheckboxFilterVisible = !isPending && checkboxElements && checkboxElements.length > ZERO;

  return (
    <CollapsibleSection title="Select excluded compartment / pathways">
      {isPending && <p>Loading...</p>}
      {isCheckboxFilterVisible && (
        <CheckboxFilter
          options={checkboxElements}
          currentOptions={currentExcludedCompartmentPathways}
          onCheckedChange={setExcludedCompartmentPathways}
        />
      )}
    </CollapsibleSection>
  );
};

/* eslint-disable no-magic-numbers */
import { render, screen, waitFor } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { act } from 'react-dom/test-utils';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { ModelElement } from '@/types/models';
import {
  compartmentPathwaysSelector,
  modelElementsAnyModelLoadingSelector,
} from '@/redux/modelElements/modelElements.selector';
import { ExcludedCompartmentPathways } from './ExcludedCompartmentPathways.component';

jest.mock('../../../../../../redux/hooks/useAppSelector', () => ({
  useAppSelector: jest.fn(),
}));
type SelectorFunction = (state: never) => ModelElement[] | boolean;

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <ExcludedCompartmentPathways />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const CHECKBOX_ELEMENT_NAME = modelElementFixture.name;

describe('ExcludedCompartmentPathways - component', () => {
  const mockUseAppSelector = useAppSelector as jest.Mock;

  it('should display compartment / pathways checkboxes when fetching data is successful', async () => {
    mockUseAppSelector.mockImplementation(selector => {
      const selectorMap = new Map<SelectorFunction, ModelElement[] | boolean>([
        [
          compartmentPathwaysSelector,
          [
            {
              ...modelElementFixture,
            },
          ],
        ],
        [modelElementsAnyModelLoadingSelector, false],
      ]);

      return selectorMap.get(selector) ?? false;
    });
    renderComponent({});

    expect(screen.queryByTestId('checkbox-filter')).not.toBeVisible();

    const navigationButton = screen.getByTestId('accordion-item-button');

    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Select excluded compartment / pathways')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByTestId('checkbox-filter')).toBeInTheDocument();
      expect(screen.getByLabelText('search-input')).toBeInTheDocument();
      expect(screen.getByLabelText(CHECKBOX_ELEMENT_NAME)).toBeInTheDocument();
    });
  });

  it('should not display compartment / pathways checkboxes when fetched data is empty', async () => {
    mockUseAppSelector.mockImplementation(selector => {
      const selectorMap = new Map<SelectorFunction, ModelElement[] | boolean>([
        [compartmentPathwaysSelector, []],
        [modelElementsAnyModelLoadingSelector, false],
      ]);

      return selectorMap.get(selector) ?? false;
    });

    renderComponent({});
    expect(screen.getByText('Select excluded compartment / pathways')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.queryByTestId('checkbox-filter')).not.toBeInTheDocument();
  });

  it('should display loading message when fetching data is pending', async () => {
    mockUseAppSelector.mockImplementation(selector => {
      const selectorMap = new Map<SelectorFunction, ModelElement[] | boolean>([
        [compartmentPathwaysSelector, []],
        [modelElementsAnyModelLoadingSelector, true],
      ]);

      return selectorMap.get(selector) ?? false;
    });

    renderComponent({});
    expect(screen.getByText('Select excluded compartment / pathways')).toBeInTheDocument();
    const navigationButton = screen.getByTestId('accordion-item-button');
    act(() => {
      navigationButton.click();
    });

    expect(screen.getByText('Loading...')).toBeInTheDocument();
  });
});

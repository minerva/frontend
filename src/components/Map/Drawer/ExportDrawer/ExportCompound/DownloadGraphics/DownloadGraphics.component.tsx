import { Button } from '@/shared/Button';
import { useContext } from 'react';
import { ExportContext } from '../ExportCompound.context';

export const DownloadGraphics = (): React.ReactNode => {
  const { handleDownloadGraphics } = useContext(ExportContext);

  return (
    <div className="mt-6">
      <Button onClick={handleDownloadGraphics}>Download</Button>
    </div>
  );
};

import { CheckboxItem } from '../CheckboxFilter/CheckboxFilter.types';
import { ImageSize } from './ImageSize/ImageSize.types';

export type ExportContextType = {
  setAnnotations: React.Dispatch<React.SetStateAction<CheckboxItem[]>>;
  setIncludedCompartmentPathways: React.Dispatch<React.SetStateAction<CheckboxItem[]>>;
  setExcludedCompartmentPathways: React.Dispatch<React.SetStateAction<CheckboxItem[]>>;
  setModels: React.Dispatch<React.SetStateAction<CheckboxItem[]>>;
  setImageSize: React.Dispatch<React.SetStateAction<ImageSize>>;
  setImageFormats: React.Dispatch<React.SetStateAction<CheckboxItem[]>>;
  handleDownloadElements: () => Promise<void>;
  handleDownloadNetwork: () => Promise<void>;
  handleDownloadGraphics: () => void;
  handleDownloadCurrentView: () => void;
  data: {
    annotations: CheckboxItem[];
    includedCompartmentPathways: CheckboxItem[];
    excludedCompartmentPathways: CheckboxItem[];
    models: CheckboxItem[];
    imageSize: ImageSize;
    imageFormats: CheckboxItem[];
  };
};

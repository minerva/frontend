import { MapModel } from '@/types/models';

export const getModelsIds = (models: MapModel[] | undefined): number[] => {
  if (!models) return [];

  return models.map(model => model.id);
};

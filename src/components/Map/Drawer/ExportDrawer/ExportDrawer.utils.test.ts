import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { getModelsIds } from './ExportDrawer.component.utils';

const MODELS_IDS = modelsFixture.map(item => item.id);

describe('getModelsIds', () => {
  it('should return an empty array if models are not provided', () => {
    const result = getModelsIds(undefined);
    expect(result).toEqual([]);
  });

  it('should return an array of model IDs', () => {
    const result = getModelsIds(modelsFixture);
    expect(result).toEqual(MODELS_IDS);
  });
});

import { fireEvent, render, screen } from '@testing-library/react';

import { CheckboxFilter } from './CheckboxFilter.component';

const options = [
  { id: '1', label: 'Option 1' },
  { id: '2', label: 'Option 2' },
  { id: '3', label: 'Option 3' },
];

const currentOptions = [{ id: '2', label: 'Option 2' }];

describe('CheckboxFilter - component', () => {
  it('should render CheckboxFilter properly', () => {
    render(<CheckboxFilter options={options} currentOptions={[]} />);
    expect(screen.getByTestId('search')).toBeInTheDocument();
  });

  it('should filter options based on search term', async () => {
    render(<CheckboxFilter options={options} currentOptions={[]} />);
    const searchInput = screen.getByLabelText('search-input');

    fireEvent.change(searchInput, { target: { value: `Option 1` } });

    expect(screen.getByLabelText('Option 1')).toBeInTheDocument();
    expect(screen.queryByText('Option 2')).not.toBeInTheDocument();
    expect(screen.queryByText('Option 3')).not.toBeInTheDocument();
  });

  it('should handle checkbox value change', async () => {
    const onCheckedChange = jest.fn();
    render(
      <CheckboxFilter currentOptions={[]} options={options} onCheckedChange={onCheckedChange} />,
    );
    const checkbox = screen.getByLabelText('Option 1');

    fireEvent.click(checkbox);

    expect(onCheckedChange).toHaveBeenCalledWith([{ id: '1', label: 'Option 1' }]);
  });

  it('should handle radio value change', async () => {
    const onCheckedChange = jest.fn();
    render(
      <CheckboxFilter
        currentOptions={[]}
        type="radio"
        options={options}
        onCheckedChange={onCheckedChange}
      />,
    );
    const checkbox = screen.getByLabelText('Option 1');

    fireEvent.click(checkbox);

    expect(onCheckedChange).toHaveBeenCalledWith([{ id: '1', label: 'Option 1' }]);
  });

  it('should call onFilterChange when searching new term', async () => {
    const onFilterChange = jest.fn();
    render(
      <CheckboxFilter currentOptions={[]} options={options} onFilterChange={onFilterChange} />,
    );
    const searchInput = screen.getByLabelText('search-input');

    fireEvent.change(searchInput, { target: { value: 'Option 1' } });

    expect(onFilterChange).toHaveBeenCalledWith([{ id: '1', label: 'Option 1' }]);
  });
  it('should display message when no elements are found', async () => {
    render(<CheckboxFilter currentOptions={[]} options={options} />);
    const searchInput = screen.getByLabelText('search-input');

    fireEvent.change(searchInput, { target: { value: 'Nonexistent Option' } });

    expect(screen.getByText('No matching elements found.')).toBeInTheDocument();
  });
  it('should display message when options are empty', () => {
    const onFilterChange = jest.fn();
    render(<CheckboxFilter currentOptions={[]} options={[]} onFilterChange={onFilterChange} />);

    expect(screen.getByText('No matching elements found.')).toBeInTheDocument();
  });
  it('should handle multiple checkbox selection', () => {
    const onCheckedChange = jest.fn();
    render(
      <CheckboxFilter currentOptions={[]} options={options} onCheckedChange={onCheckedChange} />,
    );

    const checkbox1 = screen.getByLabelText('Option 1');
    const checkbox2 = screen.getByLabelText('Option 2');

    fireEvent.click(checkbox1);
    fireEvent.click(checkbox2);

    expect(onCheckedChange).toHaveBeenCalledWith([
      { id: '1', label: 'Option 1' },
      { id: '2', label: 'Option 2' },
    ]);
  });

  it('should handle multiple change of radio selection', () => {
    const onCheckedChange = jest.fn();
    render(
      <CheckboxFilter
        currentOptions={[]}
        options={options}
        onCheckedChange={onCheckedChange}
        type="radio"
      />,
    );

    const checkbox1 = screen.getByLabelText('Option 1');
    const checkbox2 = screen.getByLabelText('Option 2');

    fireEvent.click(checkbox1);
    expect(onCheckedChange).toHaveBeenCalledWith([{ id: '1', label: 'Option 1' }]);

    fireEvent.click(checkbox2);
    expect(onCheckedChange).toHaveBeenCalledWith([{ id: '2', label: 'Option 2' }]);
  });

  it('should handle unchecking a checkbox', () => {
    const onCheckedChange = jest.fn();
    render(
      <CheckboxFilter currentOptions={[]} options={options} onCheckedChange={onCheckedChange} />,
    );

    const checkbox = screen.getByLabelText('Option 1');

    fireEvent.click(checkbox); // Check
    fireEvent.click(checkbox); // Uncheck

    expect(onCheckedChange).toHaveBeenCalledWith([]);
  });
  it('should render search input when isSearchEnabled is true', () => {
    render(<CheckboxFilter currentOptions={[]} options={options} />);
    const searchInput = screen.getByLabelText('search-input');
    expect(searchInput).toBeInTheDocument();
  });

  it('should not render search input when isSearchEnabled is false', () => {
    render(<CheckboxFilter currentOptions={[]} options={options} isSearchEnabled={false} />);
    const searchInput = screen.queryByLabelText('search-input');
    expect(searchInput).not.toBeInTheDocument();
  });

  it('should not filter options based on search input when isSearchEnabled is false', () => {
    render(<CheckboxFilter currentOptions={[]} options={options} isSearchEnabled={false} />);
    const searchInput = screen.queryByLabelText('search-input');
    expect(searchInput).not.toBeInTheDocument();
    options.forEach(option => {
      const checkboxLabel = screen.getByText(option.label);
      expect(checkboxLabel).toBeInTheDocument();
    });
  });

  it('should set checked param based on currentOptions prop', async () => {
    render(<CheckboxFilter options={options} currentOptions={currentOptions} />);
    const option1: HTMLInputElement = screen.getByLabelText('Option 1');
    const option2: HTMLInputElement = screen.getByLabelText('Option 2');
    const option3: HTMLInputElement = screen.getByLabelText('Option 3');

    expect(option1.checked).toBe(false);
    expect(option2.checked).toBe(true);
    expect(option3.checked).toBe(false);
  });
});

import { RenderResult, fireEvent, render, screen } from '@testing-library/react';
import { TabButton } from './TabButton.component';

const mockHandleChangeTab = jest.fn();

const renderTabButton = (label: string, active = false): RenderResult =>
  render(<TabButton label={label} handleChangeTab={mockHandleChangeTab} active={active} />);

describe('TabButton - component', () => {
  it('should render TabButton with custom label', () => {
    renderTabButton('Map');

    expect(screen.getByText('Map')).toBeInTheDocument();
  });

  it('should handle click event', () => {
    renderTabButton('Network');

    fireEvent.click(screen.getByText('Network'));
    expect(mockHandleChangeTab).toHaveBeenCalled();
  });

  it('should indicate active tab correctly', () => {
    renderTabButton('Graphics', true);

    const currentTab = screen.getByRole('button', { current: true });
    expect(currentTab).toHaveTextContent('Graphics');
  });
  it('should indicate not active tab correctly', () => {
    renderTabButton('Graphics');

    const activeTab = screen.queryByRole('button', { current: true });
    const graphicsTab = screen.getByRole('button', { current: false });
    expect(activeTab).not.toBeInTheDocument();
    expect(graphicsTab).toHaveTextContent('Graphics');
  });
});

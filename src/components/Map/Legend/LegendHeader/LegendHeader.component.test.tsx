import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { AppDispatch, RootState } from '@/redux/store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { InitialStoreState } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { CLOSE_BUTTON_ROLE, LegendHeader } from './LegendHeader.component';

const renderComponent = (
  initialStore?: InitialStoreState,
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);
  return (
    render(
      <Wrapper>
        <LegendHeader />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('LegendHeader - component', () => {
  it('should render legend title', () => {
    renderComponent();

    const legendTitle = screen.getByText('Legend');
    expect(legendTitle).toBeInTheDocument();
  });

  it('should render legend close button', () => {
    renderComponent();

    const closeButton = screen.getByRole(CLOSE_BUTTON_ROLE);
    expect(closeButton).toBeInTheDocument();
  });

  it('should close legend on close button click', async () => {
    const { store } = renderComponent();

    const closeButton = screen.getByRole(CLOSE_BUTTON_ROLE);
    closeButton.click();

    const actions = store.getActions();
    expect(actions[FIRST_ARRAY_ELEMENT].type).toBe('legend/closeLegend');
  });
});

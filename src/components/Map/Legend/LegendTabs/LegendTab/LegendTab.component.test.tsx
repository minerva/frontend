import { StoreType } from '@/redux/store';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { LegendTab } from './LegendTab.component';

const LEGEND_ID = '2';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <LegendTab id={LEGEND_ID} name="Legend Tab Name" />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('LegendTab - component', () => {
  it('should display tab with provided name', () => {
    renderComponent();

    expect(screen.queryByText('Legend Tab Name')).toBeVisible();
  });
  it('should set tab as active on tab click', () => {
    const { store } = renderComponent();
    const tab = screen.queryByText('Legend Tab Name');
    expect(tab).toHaveClass('font-normal');

    act(() => {
      tab?.click();
    });

    const { activeLegendId } = store.getState().legend;
    expect(activeLegendId).toBe(LEGEND_ID);

    expect(tab).not.toHaveClass('font-normal');
    expect(tab).toHaveClass('bg-[#EBF4FF]');
  });
});

import { currentLegendImagesSelector, legendSelector } from '@/redux/legend/legend.selectors';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { LegendImages } from './LegendImages.component';

jest.mock('../../../../redux/legend/legend.selectors', () => ({
  legendSelector: jest.fn(),
  currentLegendImagesSelector: jest.fn(),
}));

const legendSelectorMock = legendSelector as unknown as jest.Mock;
const currentLegendImagesSelectorMock = currentLegendImagesSelector as unknown as jest.Mock;

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <LegendImages />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('LegendImages - component', () => {
  beforeAll(() => {
    legendSelectorMock.mockImplementation(() => ({
      isOpen: true,
    }));
  });

  describe('when current images are empty', () => {
    beforeEach(() => {
      currentLegendImagesSelectorMock.mockImplementation(() => []);
      renderComponent();
    });

    it('should render empty container', () => {
      expect(screen.getByTestId('legend-images')).toBeEmptyDOMElement();
    });
  });

  describe('when current images are present', () => {
    const imagesPartialUrls = ['url1/image.png', 'url2/image.png', 'url3/image.png'];

    beforeEach(() => {
      currentLegendImagesSelectorMock.mockImplementation(() => imagesPartialUrls);
      renderComponent();
    });

    it.each(imagesPartialUrls)('should render img element, partialUrl=%s', partialUrl => {
      const imgElement = screen.getByAltText(partialUrl);

      expect(imgElement).toBeInTheDocument();
      expect(imgElement.getAttribute('src')).toBe(partialUrl);
    });
  });
});

import {
  allLegendsNamesAndIdsSelector,
  currentLegendImagesSelector,
  legendSelector,
  pluginLegendsSelector,
} from '@/redux/legend/legend.selectors';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen, within } from '@testing-library/react';
import { PLUGINS_INITIAL_STATE_MOCK } from '@/redux/plugins/plugins.mock';
import { pluginFixture } from '@/models/fixtures/pluginFixture';
import { DEFAULT_LEGEND_TAB } from '@/redux/legend/legend.constants';
import { LEGEND_ROLE } from './Legend.constants';
import { Legend } from './Legend.component';

const PLUGIN_ID = '1';
const PLUGIN_NAME = 'Plugin Custom Name';
const LEGEND_IMAGES = [
  'https://pdmap.uni.lu/minerva/resources/images/legend_d.png',
  'https://pdmap.uni.lu/minerva/resources/images/legend_a.png',
  'https://pdmap.uni.lu/minerva/resources/images/legend_b.png',
];

jest.mock('../../../redux/legend/legend.selectors', () => ({
  legendSelector: jest.fn(),
  currentLegendImagesSelector: jest.fn(),
  allLegendsNamesAndIdsSelector: jest.fn(),
  pluginLegendsSelector: jest.fn(),
  isActiveLegendSelector: jest.fn(),
}));

const legendSelectorMock = legendSelector as unknown as jest.Mock;
const currentLegendImagesSelectorMock = currentLegendImagesSelector as unknown as jest.Mock;
const pluginLegendsSelectorMock = pluginLegendsSelector as unknown as jest.Mock;
const allLegendsNamesAndIdsSelectorMock = allLegendsNamesAndIdsSelector as unknown as jest.Mock;

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <Legend />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('Legend - component', () => {
  beforeAll(() => {
    allLegendsNamesAndIdsSelectorMock.mockImplementation(() => [
      DEFAULT_LEGEND_TAB,
      {
        id: PLUGIN_ID,
        name: PLUGIN_NAME,
      },
    ]);

    pluginLegendsSelectorMock.mockImplementation(() => ({
      PLUGIN_ID: LEGEND_IMAGES,
    }));

    currentLegendImagesSelectorMock.mockImplementation(() => LEGEND_IMAGES);
  });

  describe('when is closed', () => {
    beforeEach(() => {
      legendSelectorMock.mockImplementation(() => ({
        isOpen: false,
      }));
      renderComponent();
    });

    it('should render the component without translation', () => {
      expect(screen.getByRole(LEGEND_ROLE)).not.toHaveClass('translate-y-0');
    });
  });

  describe('when is open', () => {
    beforeEach(() => {
      legendSelectorMock.mockImplementation(() => ({
        isOpen: true,
      }));
      renderComponent();
    });

    it('should render the component with translation', () => {
      expect(screen.getByRole(LEGEND_ROLE)).toHaveClass('translate-y-0');
    });

    it('should render legend header', async () => {
      const legendContainer = screen.getByRole(LEGEND_ROLE);
      const legendHeader = await within(legendContainer).getByTestId('legend-header');
      expect(legendHeader).toBeInTheDocument();
    });

    it('should render legend images', async () => {
      const legendContainer = screen.getByRole(LEGEND_ROLE);
      const legendImages = await within(legendContainer).getByTestId('legend-images');
      expect(legendImages).toBeInTheDocument();
    });
  });
  describe('when loaded plugin has own legend', () => {
    it('should display legend tabs', () => {
      legendSelectorMock.mockImplementation(() => ({
        isOpen: false,
        pluginLegend: {
          PLUGIN_ID: LEGEND_IMAGES,
        },
        activeLegendId: 'MAIN',
      }));
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          activePlugins: {
            data: {
              PLUGIN_ID: {
                ...pluginFixture,
                hash: PLUGIN_ID,
                name: PLUGIN_NAME,
              },
            },
            pluginsId: [PLUGIN_ID],
          },
        },
      });

      expect(screen.getByText('Plugin Custom Name')).toBeVisible();
      expect(screen.getByText('Main Legend')).toBeVisible();
    });
  });
  describe('when loaded plugin does not have own legend', () => {
    it('should not display legend tabs and display only main legend', () => {
      allLegendsNamesAndIdsSelectorMock.mockImplementation(() => [DEFAULT_LEGEND_TAB]);

      pluginLegendsSelectorMock.mockImplementation(() => ({}));

      currentLegendImagesSelectorMock.mockImplementation(() => LEGEND_IMAGES);
      legendSelectorMock.mockImplementation(() => ({
        isOpen: false,
        pluginLegend: {},
        activeLegendId: 'MAIN',
      }));
      renderComponent({
        plugins: {
          ...PLUGINS_INITIAL_STATE_MOCK,
          activePlugins: {
            data: {
              PLUGIN_ID: {
                ...pluginFixture,
                hash: PLUGIN_ID,
                name: PLUGIN_NAME,
              },
            },
            pluginsId: [PLUGIN_ID],
          },
        },
      });

      expect(screen.queryByText('Plugin Custom Name')).not.toBeInTheDocument();
      expect(screen.queryByText('Main Legend')).not.toBeInTheDocument();
    });
  });
});

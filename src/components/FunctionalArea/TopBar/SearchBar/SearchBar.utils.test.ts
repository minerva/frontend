import { getDefaultSearchTab, getSearchValuesArrayAndTrimToSeven } from './SearchBar.utils';

describe('getSearchValuesArray - util', () => {
  it('should return array of values when string has ; separator', () => {
    expect(getSearchValuesArrayAndTrimToSeven('value1;value2;value3')).toEqual([
      'value1',
      'value2',
      'value3',
    ]);
  });
  it('should trim values to seven if more values are provided', () => {
    expect(
      getSearchValuesArrayAndTrimToSeven('value1;value2;value3;value4;value5;value6;value7;value8'),
    ).toEqual(['value1', 'value2', 'value3', 'value4', 'value5', 'value6']);
  });
  it('should return single value in array if no ; was passed in string', () => {
    expect(getSearchValuesArrayAndTrimToSeven('value1,value2 value3')).toEqual([
      'value1,value2 value3',
    ]);
  });
});

describe('getDefaultSearchTab', () => {
  const cases: [string[], string][] = [
    [['nadh', 'o2', 'mp3'], 'nadh'],
    [['o2'], 'o2'],
    [[''], ''],
  ];
  it.each(cases)('for %s should return %s', (input, output) => {
    expect(getDefaultSearchTab(input)).toBe(output);
  });
});

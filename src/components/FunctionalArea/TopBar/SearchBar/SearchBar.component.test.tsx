import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { fireEvent, render, screen } from '@testing-library/react';
import { useRouter } from 'next/router';
import { initialStateFixture } from '@/redux/drawer/drawerFixture';
import { newReactionsFixture } from '@/models/fixtures/newReactionsFixture';
import { SearchBar } from './SearchBar.component';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);

  return (
    render(
      <Wrapper>
        <SearchBar />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

jest.mock('next/router', () => ({
  useRouter: jest.fn(),
}));

(useRouter as jest.Mock).mockReturnValue({
  query: {},
});

describe('SearchBar - component', () => {
  it('should let user type text', () => {
    renderComponent();
    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'test value' } });

    expect(input.value).toBe('test value');
  });

  it('should disable button when the user clicks the lens button', () => {
    renderComponent();
    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'park7' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    expect(button).toBeDisabled();
  });

  it('should disable input when the user clicks the Enter', () => {
    renderComponent();
    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'park7' } });
    fireEvent.keyDown(input, { key: 'Enter', code: 'Enter', charCode: 13 });

    expect(input).toBeDisabled();
  });

  it('should change selected search element when user search another', () => {
    const { store } = renderComponent();
    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'nadh' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    const {
      drawer: {
        searchDrawerState: { selectedSearchElement },
      },
    } = store.getState();

    expect(selectedSearchElement).toBe('nadh');
  });

  it('should reset reactions data on search click', () => {
    const { store } = renderComponent({
      reactions: {
        ...INITIAL_STORE_STATE_MOCK.reactions,
        data: newReactionsFixture.content,
      },
    });
    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'nadh' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    const { reactions } = store.getState();

    expect(reactions.data).toStrictEqual([]);
  });
  it('should open search drawer if it is not open', () => {
    const { store } = renderComponent({
      drawer: initialStateFixture,
    });

    const state = store.getState();
    expect(state.drawer.isOpen).toBeFalsy();

    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'nadh' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    const {
      drawer: { isOpen, drawerName },
    } = store.getState();

    expect(isOpen).toBeTruthy();
    expect(drawerName).toBe('search');
  });
  it('should open the search drawer if any other drawer is open', () => {
    const { store } = renderComponent({
      drawer: {
        ...initialStateFixture,
        drawerName: 'export',
        isOpen: true,
      },
    });

    const state = store.getState();
    expect(state.drawer.isOpen).toBeTruthy();

    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'nadh' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    const {
      drawer: { isOpen, drawerName },
    } = store.getState();

    expect(isOpen).toBeTruthy();
    expect(drawerName).toBe('search');
  });
  it('should only select tab if search drawer is already open', () => {
    const { store } = renderComponent({
      drawer: {
        ...initialStateFixture,
        drawerName: 'search',
        isOpen: true,
        searchDrawerState: {
          ...initialStateFixture.searchDrawerState,
          selectedSearchElement: '',
        },
      },
    });

    const state = store.getState();
    const { drawer } = state;
    expect(drawer.isOpen).toBeTruthy();
    expect(drawer.searchDrawerState.selectedSearchElement).toBe('');

    const input = screen.getByTestId<HTMLInputElement>('search-input');

    fireEvent.change(input, { target: { value: 'nadh' } });

    const button = screen.getByRole('button');

    fireEvent.click(button);

    const {
      drawer: {
        isOpen,
        drawerName,
        searchDrawerState: { selectedSearchElement },
      },
    } = store.getState();

    expect(isOpen).toBeTruthy();
    expect(drawerName).toBe('search');
    expect(selectedSearchElement).toBe('nadh');
  });
});

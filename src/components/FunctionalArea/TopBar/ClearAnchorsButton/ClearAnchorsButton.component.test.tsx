import { AppDispatch, RootState } from '@/redux/store';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { fireEvent, render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { MockStoreEnhanced } from 'redux-mock-store';
import { DRAWER_INITIAL_STATE } from '@/redux/drawer/drawer.constants';
import { ClearAnchorsButton } from './ClearAnchorsButton.component';

const renderComponent = (
  initialStore: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStore);

  return (
    render(
      <Wrapper>
        <ClearAnchorsButton />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ClearAnchorsButton - component', () => {
  it('should trigger clear actions on clear button click and close result drawer if it is open', () => {
    const { store } = renderComponent({
      drawer: {
        ...DRAWER_INITIAL_STATE,
        drawerName: 'bio-entity',
        isOpen: true,
      },
    });

    const button = screen.getByTitle('Clear');

    act(() => {
      fireEvent.click(button);
    });

    const actions = store.getActions();

    expect(actions).toEqual([
      { payload: undefined, type: 'drawer/closeDrawer' },
      { payload: undefined, type: 'contextMenu/closeContextMenu' },
      { payload: undefined, type: 'reactions/resetReactionsData' },
      { payload: undefined, type: 'search/clearSearchData' },
      { payload: undefined, type: 'modelElements/clearSearchModelElements' },
      { payload: undefined, type: 'drugs/clearDrugsData' },
      { payload: undefined, type: 'chemicals/clearChemicalsData' },
    ]);
  });
  it('should trigger clear actions on clear button click and not close result drawer if it is not open', () => {
    const { store } = renderComponent({
      drawer: {
        ...DRAWER_INITIAL_STATE,
        drawerName: 'bio-entity',
        isOpen: false,
      },
    });

    const button = screen.getByTitle('Clear');

    act(() => {
      fireEvent.click(button);
    });

    const actions = store.getActions();

    expect(actions).toEqual([
      { payload: undefined, type: 'contextMenu/closeContextMenu' },
      { payload: undefined, type: 'reactions/resetReactionsData' },
      { payload: undefined, type: 'search/clearSearchData' },
      { payload: undefined, type: 'modelElements/clearSearchModelElements' },
      { payload: undefined, type: 'drugs/clearDrugsData' },
      { payload: undefined, type: 'chemicals/clearChemicalsData' },
    ]);
  });
});

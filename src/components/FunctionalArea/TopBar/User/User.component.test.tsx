import { render, screen, waitFor } from '@testing-library/react';
import { InitialStoreState } from '@/utils/testing/getReduxStoreActionsListener';
import { StoreType } from '@/redux/store';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { USER_INITIAL_STATE_MOCK } from '@/redux/user/user.mock';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { oauthFixture } from '@/models/fixtures/oauthFixture';
import { User } from './User.component';

const mockedAxiosClient = mockNetworkResponse();

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <User />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('User component', () => {
  it('should render authenticated user component when user is authenticated', () => {
    renderComponent({
      user: {
        ...USER_INITIAL_STATE_MOCK,
        authenticated: true,
        login: 'name',
        role: 'admin',
      },
    });

    expect(screen.getByTestId('authenticated-button')).toBeVisible();
  });

  it('should render unauthenticated user component when user is not authenticated', () => {
    renderComponent({
      user: USER_INITIAL_STATE_MOCK,
    });

    expect(screen.getByTestId('unauthenticated-button')).toBeVisible();
  });
});

describe('AuthenticatedUser component', () => {
  it('should render action buttons', async () => {
    renderComponent({
      user: {
        ...USER_INITIAL_STATE_MOCK,
        authenticated: true,
        login: 'name',
        role: 'admin',
      },
    });

    const button = screen.getByTestId('authenticated-button');

    await waitFor(() => {
      button.click();
    });

    expect(screen.getByText('Switch account')).toBeVisible();
    expect(screen.getByText('Log out')).toBeVisible();
    expect(screen.getByText('Go to the admin panel')).toBeVisible();
  });
  it('should not render all action buttons if user is not admin or curator', async () => {
    renderComponent({
      user: {
        ...USER_INITIAL_STATE_MOCK,
        authenticated: true,
        login: 'name',
        role: 'user',
      },
    });

    const button = screen.getByTestId('authenticated-button');

    await waitFor(() => {
      button.click();
    });

    expect(screen.getByText('Switch account')).toBeVisible();
    expect(screen.getByText('Log out')).toBeVisible();
    expect(screen.queryByText('Go to the admin panel')).not.toBeInTheDocument();
  });

  it('should logout user if logout is pressed', async () => {
    mockedAxiosClient.onPost(apiPath.logout()).reply(HttpStatusCode.Ok, {});

    const { store } = renderComponent({
      user: {
        ...USER_INITIAL_STATE_MOCK,
        authenticated: true,
        login: 'name',
        role: 'user',
      },
    });

    const button = screen.getByTestId('authenticated-button');

    await waitFor(() => {
      button.click();
    });

    const logoutButton = screen.getByText('Log out');

    await waitFor(() => {
      logoutButton.click();
    });
    const userState = store.getState().user;

    expect(userState).toEqual({
      authenticated: false,
      error: {
        message: '',
        name: '',
      },
      loading: 'succeeded',
      login: null,
      role: null,
      userData: null,
      token: null,
    });
  });

  it('should display login modal if switch account is pressed', async () => {
    mockedAxiosClient.onGet(apiPath.getOauthProviders()).reply(HttpStatusCode.Ok, oauthFixture);

    const { store } = renderComponent({
      user: {
        ...USER_INITIAL_STATE_MOCK,
        authenticated: true,
        login: 'name',
        role: 'user',
      },
    });

    const button = screen.getByTestId('authenticated-button');

    await waitFor(() => {
      button.click();
    });

    const switchAccountButton = screen.getByText('Switch account');

    await waitFor(() => {
      switchAccountButton.click();
    });

    const modalState = store.getState().modal;

    expect(modalState.isOpen).toBeTruthy();
    expect(modalState.modalName).toBe('login');
  });
});

describe('UnauthenticatedUser component', () => {
  it('should display login modal if login button is pressed', async () => {
    const { store } = renderComponent({
      user: USER_INITIAL_STATE_MOCK,
    });

    const button = screen.getByTestId('unauthenticated-button');

    await waitFor(() => {
      button.click();
    });

    const modalState = store.getState().modal;

    expect(modalState.isOpen).toBeTruthy();
    expect(modalState.modalName).toBe('login');
  });
});

export const BASE_ACTIONS = {
  SWITCH_ACCOUNT: 'Switch account',
  LOG_OUT: 'Log out',
};

export const ADMIN_CURATOR_ACTIONS = {
  ADMIN_PANEL: 'Go to the admin panel',
  ...BASE_ACTIONS,
};

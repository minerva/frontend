import { AppDispatch, RootState, StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { initialStateFixture } from '@/redux/drawer/drawerFixture';
import { MockStoreEnhanced } from 'redux-mock-store';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { PROJECT_STATE_INITIAL_MOCK } from '@/redux/project/project.mock';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { initialMapStateFixture } from '@/redux/map/map.fixtures';
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { USER_INITIAL_STATE_MOCK } from '@/redux/user/user.mock';
import { SEARCH_STATE_INITIAL_MOCK } from '@/redux/search/search.mock';
import { ZOD_SEED } from '@/constants';
import { createFixture } from 'zod-fixture';
import { overviewImageView } from '@/models/overviewImageView';
import { AUTOCOMPLETE_INITIAL_STATE } from '@/redux/autocomplete/autocomplete.constants';
import { TopBar } from './TopBar.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <TopBar />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const renderComponentWithActionListener = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <TopBar />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('TopBar - component', () => {
  it('Should contain search bar', () => {
    renderComponent();

    expect(screen.getByTestId('search-bar')).toBeInTheDocument();
  });

  it('should open submaps drawer on submaps button click', () => {
    const { store } = renderComponent({ drawer: initialStateFixture });

    const button = screen.getByRole('button', { name: 'Submaps' });
    button.click();

    const { isOpen, drawerName } = store.getState().drawer;

    expect(isOpen).toBe(true);
    expect(drawerName).toBe('submaps');
  });

  it('should open overlays drawer on overlays button click', () => {
    const { store } = renderComponent({ drawer: initialStateFixture });

    const button = screen.getByRole('button', { name: 'Overlays' });
    button.click();

    const { isOpen, drawerName } = store.getState().drawer;

    expect(isOpen).toBe(true);
    expect(drawerName).toBe('overlays');
  });
  it('should render browse overview images button', () => {
    renderComponent({
      user: USER_INITIAL_STATE_MOCK,
      search: SEARCH_STATE_INITIAL_MOCK,
      drawer: initialStateFixture,
      project: {
        ...PROJECT_STATE_INITIAL_MOCK,
        data: {
          ...projectFixture,
          overviewImageViews: [
            createFixture(overviewImageView, {
              seed: ZOD_SEED,
              array: { min: 1, max: 1 },
            }),
          ],
        },
      },
      map: initialMapStateFixture,
    });
    expect(screen.getByText('Browse overview images')).toBeInTheDocument();
  });

  it('should open overview image modal on button click', () => {
    const { store } = renderComponentWithActionListener({
      project: {
        ...PROJECT_STATE_INITIAL_MOCK,
        data: projectFixture,
      },
      drawer: initialStateFixture,
      user: USER_INITIAL_STATE_MOCK,
      map: initialMapStateFixture,
      search: SEARCH_STATE_INITIAL_MOCK,
      autocompleteSearch: AUTOCOMPLETE_INITIAL_STATE,
      autocompleteDrug: AUTOCOMPLETE_INITIAL_STATE,
      autocompleteChemical: AUTOCOMPLETE_INITIAL_STATE,
    });

    const overviewImageButton = screen.getByText('Browse overview images');
    overviewImageButton.click();

    const actions = store.getActions();
    expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
      payload: projectFixture.topOverviewImage?.id,
      type: 'modal/openOverviewImagesModalById',
    });
  });
  it('should hide button browse overview images if there are no overview images', async () => {
    renderComponentWithActionListener({
      user: USER_INITIAL_STATE_MOCK,
      search: SEARCH_STATE_INITIAL_MOCK,
      autocompleteSearch: AUTOCOMPLETE_INITIAL_STATE,
      autocompleteDrug: AUTOCOMPLETE_INITIAL_STATE,
      autocompleteChemical: AUTOCOMPLETE_INITIAL_STATE,
      drawer: initialStateFixture,
      project: {
        ...PROJECT_STATE_INITIAL_MOCK,
        data: {
          ...projectFixture,
          overviewImageViews: [],
        },
      },
      map: initialMapStateFixture,
    });

    const overviewImageButton = screen.queryByText('Browse overview images');
    expect(overviewImageButton).toBeNull();
  });
});

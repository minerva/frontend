/* eslint-disable no-magic-numbers */
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen } from '@testing-library/react';
import { CONTEXT_MENU_INITIAL_STATE } from '@/redux/contextMenu/contextMenu.constants';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { PluginsContextMenu } from '@/services/pluginsManager/pluginContextMenu/pluginsContextMenu';
import { modelElementFixture } from '@/models/fixtures/modelElementFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { ContextMenu } from './ContextMenu.component';

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);
  return (
    render(
      <Wrapper>
        <ContextMenu />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('ContextMenu - Component', () => {
  beforeEach(() => {
    PluginsContextMenu.menuItems = [];
  });
  afterEach(() => {
    PluginsContextMenu.menuItems = [];
  });

  describe('when context menu is hidden', () => {
    beforeEach(() => {
      renderComponent({
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: false,
          coordinates: [],
          uniprot: '',
        },
      });
    });

    it('should context menu has hidden class', () => {
      expect(screen.getByTestId('context-modal')).toBeInTheDocument();
      expect(screen.getByTestId('context-modal')).toHaveClass('hidden');
    });
  });

  describe('when context menu is shown', () => {
    it('should display context menu', () => {
      renderComponent({
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: true,
          coordinates: [0, 0],
          uniprot: '',
        },
      });

      expect(screen.getByTestId('context-modal')).toBeInTheDocument();
      expect(screen.getByTestId('context-modal')).not.toHaveClass('hidden');
    });

    it('should display proper text when uniprot is not provided', () => {
      renderComponent({
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: true,
          coordinates: [0, 0],
          uniprot: '',
        },
      });
      expect(screen.getByTestId('open-molart')).toBeInTheDocument();
      expect(screen.getByTestId('open-molart')).toHaveClass('cursor-not-allowed');
    });

    it('should display proper text when uniprot is not provided', () => {
      renderComponent({
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: true,
          coordinates: [0, 0],
          uniprot: '',
        },
      });

      const openMolartElement = screen.getByTestId('open-molart');

      expect(openMolartElement).toBeInTheDocument();
      expect(openMolartElement).toHaveClass('cursor-not-allowed');
      expect(screen.getByText('no UnitProt ID available', { exact: false })).toBeInTheDocument();
    });

    it('should display uniprot id as option if it is provided', () => {
      renderComponent({
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: modelElementFixture.id.toString(),
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: 'BioEntity Full Name',
                      references: [
                        {
                          ...bioEntityContentFixture.bioEntity.references[0],
                          type: 'UNIPROT',
                        },
                      ],
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: true,
          coordinates: [0, 0],
          currentSelectedBioEntityId: bioEntityContentFixture.bioEntity.id,
        },
      });

      const openMolartElement = screen.getByTestId('open-molart');

      expect(openMolartElement).toBeInTheDocument();
      expect(openMolartElement).not.toHaveClass('cursor-not-allowed');
      expect(
        screen.getByText(bioEntityContentFixture.bioEntity.references[0].resource, {
          exact: false,
        }),
      ).toBeInTheDocument();
    });

    it('should open molart modal when clicking on uniprot', async () => {
      const { store } = renderComponent({
        modelElements: {
          data: {
            0: {
              data: [modelElementFixture],
              loading: 'succeeded',
              error: { message: '', name: '' },
            },
          },
          search: {
            data: [
              {
                searchQueryElement: modelElementFixture.id.toString(),
                loading: 'succeeded',
                error: DEFAULT_ERROR,
                data: [
                  {
                    modelElement: {
                      ...modelElementFixture,
                      fullName: 'BioEntity Full Name',
                      references: [
                        {
                          ...bioEntityContentFixture.bioEntity.references[0],
                          type: 'UNIPROT',
                        },
                      ],
                    },
                    perfect: true,
                  },
                ],
              },
            ],
            loading: 'pending',
            error: DEFAULT_ERROR,
          },
        },
        contextMenu: {
          ...CONTEXT_MENU_INITIAL_STATE,
          isOpen: true,
          coordinates: [0, 0],
          currentSelectedBioEntityId: bioEntityContentFixture.bioEntity.id,
        },
      });

      const openMolartElement = screen.getByTestId('open-molart');

      await act(() => {
        openMolartElement.click();
      });

      const { contextMenu, modal } = store.getState();

      expect(contextMenu.isOpen).toBe(false);
      expect(modal.isOpen).toBe(true);
      expect(modal.modalName).toBe('mol-art');
    });
  });

  it('should render context menu', () => {
    const callback = jest.fn();
    PluginsContextMenu.addMenu('1324235432', 'Click me', '', true, callback);

    renderComponent({
      contextMenu: {
        ...CONTEXT_MENU_INITIAL_STATE,
        isOpen: true,
        coordinates: [0, 0],
        uniprot: '',
      },
    });

    expect(screen.getByTestId('context-modal')).toBeInTheDocument();
    expect(screen.getByTestId('context-modal')).not.toHaveClass('hidden');

    expect(screen.getByText('Click me')).toBeInTheDocument();
  });
});

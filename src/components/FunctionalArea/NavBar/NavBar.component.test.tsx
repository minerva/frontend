import { render, screen } from '@testing-library/react';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { NavBar } from './NavBar.component';

const renderComponent = (): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore();
  return (
    render(
      <Wrapper>
        <NavBar />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('NavBar - component', () => {
  it('Should contain navigation buttons and logos with powered by info', () => {
    renderComponent();

    expect(screen.getByTestId('nav-buttons')).toBeInTheDocument();
    expect(screen.getByTestId('nav-logos-and-powered-by')).toBeInTheDocument();
    expect(screen.getByAltText('University of Luxembourg logo')).toBeInTheDocument();
    expect(screen.getByAltText('Minerva logo')).toBeInTheDocument();
  });
});

/* eslint-disable no-magic-numbers */
import { initialMapDataFixture, openedMapsThreeSubmapsFixture } from '@/redux/map/map.fixtures';
import { StoreType } from '@/redux/store';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act, render, screen, within } from '@testing-library/react';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID } from '@/constants/mocks';
import { MapModel, Project } from '@/types/models';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { ProjectState } from '@/redux/project/project.types';
import { PROJECT_STATE_INITIAL_MOCK } from '@/redux/project/project.mock';
import { ModelsState } from '@/redux/models/models.types';
import { DEFAULT_ERROR } from '@/constants/errors';
import { MapNavigation } from './MapNavigation.component';

const MODELS_MOCK: MapModel[] = [
  {
    id: MAIN_MAP_ID,
    width: 26779.25,
    height: 13503.0,
    defaultCenterX: null,
    defaultCenterY: null,
    description: '',
    name: 'Core PD map',
    defaultZoomLevel: null,
    tileSize: 256,
    references: [],
    authors: [],
    creationDate: null,
    modificationDates: [],
    minZoom: 2,
    maxZoom: 9,
  },
  {
    id: 5054,
    width: 26779.25,
    height: 13503.0,
    defaultCenterX: null,
    defaultCenterY: null,
    description: '',
    name: 'Core PD map',
    defaultZoomLevel: null,
    tileSize: 256,
    references: [],
    authors: [],
    creationDate: null,
    modificationDates: [],
    minZoom: 2,
    maxZoom: 9,
  },
];

export const MODELS_DATA: ModelsState = {
  data: [
    {
      id: MAIN_MAP_ID,
      width: 26779.25,
      height: 13503,
      defaultCenterX: null,
      defaultCenterY: null,
      description: '',
      name: 'Core PD map',
      defaultZoomLevel: null,
      tileSize: 256,
      references: [],
      authors: [],
      creationDate: null,
      modificationDates: [],
      minZoom: 2,
      maxZoom: 9,
    },
  ],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

const PROJECT: Project = {
  ...projectFixture,
  topMap: { id: MAIN_MAP_ID },
};

const PROJECT_STATE: ProjectState = {
  ...PROJECT_STATE_INITIAL_MOCK,
  data: PROJECT,
};

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <MapNavigation />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('MapNavigation - component', () => {
  it('should render list of currently opened maps, main map should not have close button', async () => {
    renderComponent({
      map: {
        data: { ...initialMapDataFixture },
        loading: 'succeeded',
        error: { message: '', name: '' },
        openedMaps: openedMapsThreeSubmapsFixture,
      },
    });

    const mainMapButton = screen.getByRole('button', { name: 'Main map' });
    expect(mainMapButton).toBeInTheDocument();

    const histamineMapButton = screen.getByRole('button', { name: 'Histamine signaling' });
    expect(histamineMapButton).toBeInTheDocument();

    const prknMapButton = screen.getByRole('button', { name: 'PRKN substrates' });
    expect(prknMapButton).toBeInTheDocument();
  });

  it('all maps should have close button expect main map', async () => {
    renderComponent({
      map: {
        data: { ...initialMapDataFixture },
        loading: 'succeeded',
        error: { message: '', name: '' },
        openedMaps: openedMapsThreeSubmapsFixture,
      },
    });

    const mainMapButton = screen.getByRole('button', { name: 'Main map' });
    const mainMapCloseButton = await within(mainMapButton).queryByTestId('close-icon');
    expect(mainMapCloseButton).not.toBeInTheDocument();

    const histamineMapButton = screen.getByRole('button', { name: 'Histamine signaling' });
    const histamineMapCloseButton = await within(histamineMapButton).getByTestId('close-icon');
    expect(histamineMapCloseButton).toBeInTheDocument();

    const prknMapButton = screen.getByRole('button', { name: 'PRKN substrates' });
    const prknMapCloseButton = await within(prknMapButton).getByTestId('close-icon');
    expect(prknMapCloseButton).toBeInTheDocument();
  });

  it('should close map tab when clicking on close button while', async () => {
    const { store } = renderComponent({
      map: {
        data: {
          ...initialMapDataFixture,
          modelId: MAIN_MAP_ID,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        openedMaps: openedMapsThreeSubmapsFixture,
      },
    });

    const histamineMapButton = screen.getByRole('button', { name: 'Histamine signaling' });
    const histamineMapCloseButton = await within(histamineMapButton).getByTestId('close-icon');
    await act(() => {
      histamineMapCloseButton.click();
    });

    const {
      map: {
        data: { modelId },
        openedMaps,
      },
    } = store.getState();

    const isHistamineMapOpened = openedMaps.some(map => map.modelName === 'Histamine signaling');

    expect(isHistamineMapOpened).toBe(false);
    expect(modelId).toBe(MAIN_MAP_ID);
  });

  it('should close currently selected map map and open main map', async () => {
    const { store } = renderComponent({
      project: PROJECT_STATE,
      map: {
        data: {
          ...initialMapDataFixture,
          modelId: HISTAMINE_MAP_ID,
        },
        openedMaps: openedMapsThreeSubmapsFixture,
        loading: 'succeeded',
        error: { message: '', name: '' },
      },
      models: {
        loading: 'succeeded',
        error: { message: '', name: '' },
        data: MODELS_MOCK,
      },
    });

    const histamineMapButton = screen.getByRole('button', { name: 'Histamine signaling' });
    const histamineMapCloseButton = await within(histamineMapButton).getByTestId('close-icon');
    await act(() => {
      histamineMapCloseButton.click();
    });

    const {
      map: {
        data: { modelId },
        openedMaps,
      },
    } = store.getState();

    const isHistamineMapOpened = openedMaps.some(map => map.modelName === 'Histamine signaling');

    expect(isHistamineMapOpened).toBe(false);
    expect(modelId).toBe(MAIN_MAP_ID);
  });
  describe('plugin event bus', () => {
    beforeEach(() => {
      PluginsEventBus.events = [];
    });
    afterEach(() => {
      jest.clearAllMocks();
    });
    it('should dispatch event if it closes active map and set main map as active', async () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');

      renderComponent({
        project: { ...PROJECT_STATE },
        models: MODELS_DATA,
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: HISTAMINE_MAP_ID,
          },
          openedMaps: openedMapsThreeSubmapsFixture,
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      });

      const histamineMapButton = screen.getByRole('button', { name: 'Histamine signaling' });
      const histamineMapCloseButton = await within(histamineMapButton).getByTestId('close-icon');
      await act(() => {
        histamineMapCloseButton.click();
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(2);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapClose', HISTAMINE_MAP_ID);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapOpen', MAIN_MAP_ID);
    });
    it('should not dispatch event if it closes not active map', async () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');

      renderComponent({
        project: PROJECT_STATE,
        models: MODELS_DATA,
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: HISTAMINE_MAP_ID,
          },
          openedMaps: openedMapsThreeSubmapsFixture,
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      });

      const prknMapButton = screen.getByRole('button', { name: 'PRKN substrates' });
      const prknMapCloseButton = await within(prknMapButton).getByTestId('close-icon');
      await act(() => {
        prknMapCloseButton.click();
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(0);
    });
    it('should dispatch event if it switches to new tab and set is as active map', async () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');

      renderComponent({
        project: PROJECT_STATE,
        models: MODELS_DATA,
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: HISTAMINE_MAP_ID,
          },
          openedMaps: openedMapsThreeSubmapsFixture,
          loading: 'succeeded',
          error: { message: '', name: '' },
        },
      });

      const prknMapButton = screen.getByRole('button', { name: 'PRKN substrates' });

      await act(() => {
        prknMapButton.click();
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(2);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapClose', HISTAMINE_MAP_ID);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapOpen', 5054);
    });
  });
});

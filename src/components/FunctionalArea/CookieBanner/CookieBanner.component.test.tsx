import React from 'react';
import { render, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { StoreType } from '@/redux/store';
import { CONFIGURATION_INITIAL_STORE_MOCKS } from '@/redux/configuration/configuration.mock';
import {
  USER_ACCEPTED_COOKIES_COOKIE_NAME,
  USER_ACCEPTED_COOKIES_COOKIE_VALUE,
} from './CookieBanner.constants';
import { CookieBanner } from './CookieBanner.component';

const renderComponent = (): { store: StoreType } => {
  // const { Wrapper, store } = getReduxWrapperUsingSliceReducer('cookieBanner', cookieBannerReducer);
  //

  const { Wrapper, store } = getReduxWrapperWithStore({
    ...INITIAL_STORE_STATE_MOCK,
    configuration: CONFIGURATION_INITIAL_STORE_MOCKS,
  });

  return (
    render(
      <Wrapper>
        <CookieBanner />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('CookieBanner component', () => {
  beforeEach(() => {
    localStorage.clear();
  });
  it('renders cookie banner correctly first time', () => {
    renderComponent();
    expect(localStorage.getItem).toHaveBeenCalledWith(USER_ACCEPTED_COOKIES_COOKIE_NAME);

    const button = screen.getByLabelText(/accept cookies/i);
    expect(button).toBeInTheDocument();
  });

  it('hides the banner after accepting cookies', () => {
    renderComponent();
    const button = screen.getByLabelText(/accept cookies/i);
    act(() => {
      button.click();
    });

    expect(button).not.toBeInTheDocument();
    expect(localStorage.setItem).toHaveBeenCalledWith(
      USER_ACCEPTED_COOKIES_COOKIE_NAME,
      USER_ACCEPTED_COOKIES_COOKIE_VALUE.ACCEPTED,
    );
  });

  it('does not render the cookies banner when cookies are accepted', () => {
    renderComponent();

    const button = screen.getByLabelText(/accept cookies/i);
    expect(button).toBeInTheDocument();

    act(() => {
      button.click();
    });

    renderComponent();

    expect(localStorage.getItem).toHaveBeenCalledWith(USER_ACCEPTED_COOKIES_COOKIE_NAME);
    expect(button).not.toBeInTheDocument();
  });
});

export const USER_ACCEPTED_COOKIES_COOKIE_NAME = 'cookiesAccepted';
export const USER_ACCEPTED_COOKIES_DATE = 'cookiesAcceptedDate';
export const USER_ACCEPTED_MATOMO_COOKIES_COOKIE_NAME = 'matomoAccepted';

export const USER_ACCEPTED_COOKIES_COOKIE_VALUE = {
  ACCEPTED: 'true',
  DECLINED: 'false',
};

import { Color } from '@/types/models';

export type LayerRectFactoryForm = {
  fillColor: Color;
  borderColor: Color;
};

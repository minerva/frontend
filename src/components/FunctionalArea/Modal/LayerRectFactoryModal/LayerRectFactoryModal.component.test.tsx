/* eslint-disable no-magic-numbers */
import { render, screen, fireEvent } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { StoreType } from '@/redux/store';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { GLYPHS_STATE_INITIAL_MOCK } from '@/redux/glyphs/glyphs.mock';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import {
  LAYER_STATE_DEFAULT_DATA,
  LAYERS_STATE_INITIAL_LAYER_MOCK,
} from '@/redux/layers/layers.mock';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { showToast } from '@/utils/showToast';
import { layerRectFixture } from '@/models/fixtures/layerRectFixture';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { LayerRectFactoryModal } from './LayerRectFactoryModal.component';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const glyph = { id: 1, file: 23, filename: 'Glyph1.png' };

jest.mock('../../../../utils/showToast');

const renderComponent = (): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore({
    ...INITIAL_STORE_STATE_MOCK,
    glyphs: {
      ...GLYPHS_STATE_INITIAL_MOCK,
      data: [glyph],
    },
    layers: {
      0: {
        ...LAYERS_STATE_INITIAL_LAYER_MOCK,
        data: {
          ...LAYER_STATE_DEFAULT_DATA,
          activeLayers: [1],
          drawLayer: 1,
        },
      },
    },
    modal: {
      isOpen: true,
      modalTitle: overlayFixture.name,
      modalName: 'layer-rect-factory',
      editOverlayState: overlayFixture,
      editOverlayGroupState: overlayGroupFixture,
      molArtState: {},
      overviewImagesState: {},
      errorReportState: {},
      layerFactoryState: { id: undefined },
      layerObjectFactoryState: {
        x: 1,
        y: 1,
        width: 1,
        height: 1,
      },
      layerLineFactoryState: undefined,
    },
    models: {
      ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
    },
  });

  return {
    store,
    ...render(
      <Wrapper>
        <LayerRectFactoryModal />
      </Wrapper>,
    ),
  };
};

describe('LayerRectFactoryModal - component', () => {
  it('should render LayerRectFactoryModal component with initial state', () => {
    renderComponent();

    expect(screen.getByText('Fill color:')).toBeInTheDocument();
    expect(screen.getByText('Border color:')).toBeInTheDocument();
  });

  it('should handle form submission correctly', async () => {
    mockedAxiosNewClient
      .onPost(apiPath.addLayerRect(0, 1))
      .reply(HttpStatusCode.Ok, layerRectFixture);
    renderComponent();

    const submitButton = screen.getByText(/Submit/i);

    await act(async () => {
      fireEvent.click(submitButton);
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'A new rectangle has been successfully added.',
      type: 'success',
    });
  });
});

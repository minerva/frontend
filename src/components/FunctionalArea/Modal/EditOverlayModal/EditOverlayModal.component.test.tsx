import { render, screen, fireEvent } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { overlayFixture, overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { mockNetworkNewAPIResponse, mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { DEFAULT_ERROR } from '@/constants/errors';
import { act } from 'react-dom/test-utils';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { showToast } from '@/utils/showToast';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { Modal } from '../Modal.component';

const mockedAxiosClient = mockNetworkResponse();

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

jest.mock('../../../../utils/showToast');

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Modal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const overlay = {
  ...overlayFixture,
  group: null,
};
describe('EditOverlayModal - component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should render modal with correct data', () => {
    renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    expect(screen.getByLabelText('Name')).toBeVisible();
    expect(screen.getByLabelText('Description')).toBeVisible();
    expect(screen.getByTestId('overlay-name')).toHaveValue(overlayFixture.name);
    expect(screen.getByTestId('overlay-description')).toHaveValue(overlayFixture.description);
  });
  it('should handle input change correctly', () => {
    renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const overlayNameInput: HTMLInputElement = screen.getByTestId('overlay-name');
    const overlayDescriptionInput: HTMLTextAreaElement = screen.getByTestId('overlay-description');

    fireEvent.change(overlayNameInput, { target: { value: 'Test name' } });
    fireEvent.change(overlayDescriptionInput, { target: { value: 'Descripiton' } });

    expect(overlayNameInput.value).toBe('Test name');
    expect(overlayDescriptionInput.value).toBe('Descripiton');
  });
  it('should handle remove user overlay', async () => {
    const { store } = renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosClient
      .onDelete(apiPath.removeOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, {});

    const removeButton = screen.getByTestId('remove-button');
    expect(removeButton).toBeVisible();
    await act(() => {
      removeButton.click();
    });

    const { loading } = store.getState().overlays.removeOverlay;
    expect(loading).toBe('succeeded');
    expect(removeButton).not.toBeVisible();
  });
  it('should show toast after successful removing user overlay', async () => {
    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosClient
      .onDelete(apiPath.removeOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, {});

    const removeButton = screen.getByTestId('remove-button');
    expect(removeButton).toBeVisible();
    await act(() => {
      removeButton.click();
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'User overlay removed successfully',
      type: 'success',
    });
  });
  it('should handle save edited user overlay', async () => {
    const { store } = renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlay.name,
        modalName: 'edit-overlay',
        editOverlayState: overlay,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient.onPut(apiPath.updateOverlay(overlay.id)).reply(HttpStatusCode.Ok, overlay);

    const page = {
      ...overlaysPageFixture,
      data: [overlay],
    };

    mockedAxiosNewClient
      .onGet(apiPath.getAllUserOverlaysByCreatorQuery({ creator: 'test', publicOverlay: false }))
      .reply(HttpStatusCode.Ok, page);

    const saveButton = screen.getByTestId('save-button');
    expect(saveButton).toBeVisible();
    await act(() => {
      saveButton.click();
    });

    const { loading } = store.getState().overlays.updateOverlays;
    expect(loading).toBe('succeeded');
    expect(saveButton).not.toBeVisible();
  });
  it('should show toast after successful editing user overlay', async () => {
    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlay.name,
        modalName: 'edit-overlay',
        editOverlayState: overlay,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient.onPut(apiPath.updateOverlay(overlay.id)).reply(HttpStatusCode.Ok, overlay);

    const saveButton = screen.getByTestId('save-button');
    expect(saveButton).toBeVisible();
    await act(() => {
      saveButton.click();
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'User overlay updated successfully',
      type: 'success',
    });
  });

  it('should handle cancel edit user overlay', async () => {
    const { store } = renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const cancelButton = screen.getByTestId('cancel-button');
    expect(cancelButton).toBeVisible();
    await act(() => {
      cancelButton.click();
    });

    const { isOpen } = store.getState().modal;
    expect(isOpen).toBe(false);
    expect(cancelButton).not.toBeVisible();
  });
});

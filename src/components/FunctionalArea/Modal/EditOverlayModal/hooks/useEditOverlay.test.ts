/* eslint-disable no-magic-numbers */
import { DEFAULT_ERROR } from '@/constants/errors';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { useEditOverlay } from './useEditOverlay';

describe('useEditOverlay', () => {
  it('should handle cancel edit overlay', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleCancelEdit },
      },
    } = renderHook(() => useEditOverlay(), {
      wrapper: Wrapper,
    });

    handleCancelEdit();

    const actions = store.getActions();

    expect(actions[0].type).toBe('modal/closeModal');
  });

  it('should handle handleRemoveOverlay if proper data is provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleRemoveOverlay },
      },
    } = renderHook(() => useEditOverlay(), {
      wrapper: Wrapper,
    });

    handleRemoveOverlay();

    const actions = store.getActions();

    expect(actions[0].type).toBe('overlays/removeOverlay/pending');

    const { overlayId } = actions[0].meta.arg;

    expect(overlayId).toBe(overlayFixture.id);
  });
  it('should not handle handleRemoveOverlay if proper data is not provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'failed',
        error: DEFAULT_ERROR,
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleRemoveOverlay },
      },
    } = renderHook(() => useEditOverlay(), {
      wrapper: Wrapper,
    });

    handleRemoveOverlay();

    const actions = store.getActions();

    expect(actions.length).toBe(0);
  });
  it('should handle handleSaveEditedOverlay if proper data is provided', () => {
    const overlay = { ...overlayFixture, group: null };
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlay.name,
        modalName: 'edit-overlay',
        editOverlayState: overlay,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleSaveEditedOverlay },
      },
    } = renderHook(() => useEditOverlay(), {
      wrapper: Wrapper,
    });

    handleSaveEditedOverlay();

    const actions = store.getActions();

    expect(actions[0].type).toBe('overlays/updateOverlays/pending');
    expect(actions[0].meta.arg).toEqual([overlay]);
  });
  it('should not handle handleSaveEditedOverlay if proper data is not provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleSaveEditedOverlay },
      },
    } = renderHook(() => useEditOverlay(), {
      wrapper: Wrapper,
    });

    handleSaveEditedOverlay();

    const actions = store.getActions();

    expect(actions.length).toBe(0);
  });
});

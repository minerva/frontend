/* eslint-disable no-magic-numbers */
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { act } from 'react-dom/test-utils';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import { PUBLICATIONS_INITIAL_STATE_MOCK } from '@/redux/publications/publications.mock';
import { DEFAULT_ERROR } from '@/constants/errors';
import { MODELS_MOCK } from '@/models/mocks/modelsMock';
import { FIRST_ARRAY_ELEMENT, ZERO } from '@/constants/common';
import { FilterBySubmapHeader } from './FilterBySubmapHeader.component';

mockNetworkNewAPIResponse();

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <FilterBySubmapHeader />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('FilterBySubmapHeader - component', () => {
  describe('render', () => {
    it('should render closed dropdown', async () => {
      renderComponent({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });

      const ulTag = await screen.findByTestId('background-dropdown-list');
      const listItems = screen.queryAllByRole('listitem');

      expect(listItems).toHaveLength(ZERO);
      expect(ulTag).toBeInTheDocument();
      expect(ulTag).toHaveClass('hidden');
    });

    it('should render available submaps on dropdown open', async () => {
      renderComponent({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });

      const button = await screen.findByTestId('background-dropdown-button');
      await act(() => {
        button.click();
      });

      const ulTag = await screen.findByTestId('background-dropdown-list');
      expect(ulTag).not.toHaveClass('hidden');

      const listItems = await screen.findAllByRole('option');
      expect(listItems).toHaveLength(MODELS_MOCK.length);
      expect(listItems[FIRST_ARRAY_ELEMENT]).toHaveTextContent(
        MODELS_MOCK[FIRST_ARRAY_ELEMENT].name,
      );
    });

    it('should display no value selected initially', () => {
      renderComponent({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });

      const button = screen.getByTestId('background-dropdown-button-name');
      expect(button).toHaveTextContent('Submaps');
    });

    it('should display selected submap name in toggle button', async () => {
      renderComponent({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });

      const button = await screen.findByTestId('background-dropdown-button');
      await act(() => {
        button.click();
      });

      const listItems = screen.getAllByRole('option');
      const selectedItem = listItems[FIRST_ARRAY_ELEMENT];
      await act(() => {
        selectedItem.click();
      });

      const buttonName = screen.getByTestId('background-dropdown-button-name');
      expect(buttonName).toHaveTextContent(MODELS_MOCK[FIRST_ARRAY_ELEMENT].name);
    });
  });

  describe('on submap selection', () => {
    it('should dispatch setSelectedModelId action', async () => {
      const mockStore = configureMockStore([thunk]);
      const store = mockStore({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });
      render(
        <Provider store={store}>
          <FilterBySubmapHeader />
        </Provider>,
      );

      const button = await screen.findByTestId('background-dropdown-button');
      await act(() => {
        button.click();
      });

      const listItems = screen.getAllByRole('option');
      const selectedItem = listItems[FIRST_ARRAY_ELEMENT];
      await act(() => {
        selectedItem.click();
      });

      const actions = store.getActions();
      expect(actions).toHaveLength(3); // 2 - getPublications (pending, fulfilled), 1 - setSelectedModelId
      expect(actions[FIRST_ARRAY_ELEMENT].type).toBe('publications/setSelectedModelId');

      const selectedModelId = actions[FIRST_ARRAY_ELEMENT].payload;
      expect(selectedModelId).toBe(String(MODELS_MOCK[FIRST_ARRAY_ELEMENT].id));
    });
    it('should dispatch getPublications action', async () => {
      const mockStore = configureMockStore([thunk]);
      const store = mockStore({
        publications: PUBLICATIONS_INITIAL_STATE_MOCK,
        models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
      });
      render(
        <Provider store={store}>
          <FilterBySubmapHeader />
        </Provider>,
      );

      const button = await screen.findByTestId('background-dropdown-button');
      await act(() => {
        button.click();
      });

      const listItems = screen.getAllByRole('option');
      const selectedItem = listItems[FIRST_ARRAY_ELEMENT];
      await act(() => {
        selectedItem.click();
      });

      const actions = store.getActions();
      expect(actions).toHaveLength(3); // 2 - getPublications (pending, fulfilled), 1 - setSelectedModelId
      expect(actions[1].type).toBe('publications/getPublications/pending');
    });
  });
});

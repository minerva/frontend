import { ONE } from '@/constants/common';
import { PublicationElement } from '@/types/models';
import { ElementLink } from './ElementLink';

interface Props {
  targets: PublicationElement[];
}

export const ElementsOnMapCell = ({ targets }: Props): JSX.Element => {
  return (
    <div className="inline">
      {targets.map((target, index) => {
        const isLastElement = index + ONE === targets.length;

        return (
          <>
            <ElementLink key={target.id} target={target} />
            {isLastElement ? '' : ', '}
          </>
        );
      })}
    </div>
  );
};

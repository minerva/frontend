import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { AppDispatch, RootState } from '@/redux/store';
import { PublicationElement } from '@/types/models';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { render, screen, waitFor } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { isReactionElement } from '@/redux/reactions/isReactionElement';
import { publicationElementFixture } from '@/models/fixtures/publicationElementFixture';
import { ElementsOnMapCell } from './ElementsOnMapCell.component';

interface Props {
  targets: PublicationElement[];
}

const renderComponent = (
  props: Props,
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <ElementsOnMapCell targets={props.targets} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const elementFixture = { ...publicationElementFixture, idReaction: undefined };
const reactionFixture = { ...publicationElementFixture, idReaction: '123' };

const mockTargets = [{ ...elementFixture }, { ...reactionFixture }];

describe('ElementsOnMapCell - component', () => {
  test.each(mockTargets)('should render correctly', async bioEntity => {
    renderComponent(
      {
        targets: mockTargets,
      },
      INITIAL_STORE_STATE_MOCK,
    );

    await waitFor(() => {
      // type as elementId
      const isReaction = isReactionElement(bioEntity);
      const prefix = isReaction ? 'Reaction: ' : 'Element: ';
      expect(screen.getByText(prefix + bioEntity.elementId)).toBeInTheDocument();
    });
  });
});

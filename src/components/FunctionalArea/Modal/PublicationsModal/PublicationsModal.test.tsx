import { mockNetworkResponse } from '@/utils/mockNetworkResponse';

mockNetworkResponse();

/**
 * Since data to table is passed down from Publications Modal component
 * this test will be covered in the integration test for the Publications Table component
 * and interactions beetween filtering, sorting, searching and pagination
 */

describe('Publications Modal - component', () => {
  it('should render number of publications', () => {});
  it('should render download csv button', () => {});
  it('should trigger download on csv button click', () => {});
  it('should render search input', () => {});
  it('should be able to search publications by using search input', () => {});
  it('should be able to sort publications by clicking on Pubmed ID column header', () => {});
  it('should be able to sort publications by clicking on Title column header', () => {});
  it('should be able to sort publications by clicking on Authors column header', () => {});
  it('should be able to sort publications by clicking on Journal column header', () => {});
  it('should be able to sort publications by clicking on Year column header', () => {});
  it('should be able to sort publications by clicking on Elements on map column header', () => {});
  it('should be able to sort publications by clicking on SUBMAPS on map column header', () => {});

  it('should render publications list', () => {});

  it('should render pagination', () => {});
  it('should be able to navigate to next page', () => {});
  it('should be able to navigate to previous page', () => {});

  describe('submaps filter', () => {
    it('should render submaps filter', () => {});
    it('should have no default submap selected on init on submaps filter', () => {});
    it('should display publications for selected submap', () => {});
    it('should display publications related to selected submap when submap is selected', () => {});
    it('should display publications related to selected submap when user searches for publications using search input', () => {});
    it('should display publications related to selected submap when user sorts publications by clicking on column header', () => {});
  });
});

/* eslint-disable no-magic-numbers */
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK } from '@/models/mocks/publicationsResponseMock';
import { BioEntityContent, Publication } from '@/types/models';
import { StandarizedPublication } from '@/types/publications';
import { fetchElementData } from '../../utils/fetchElementData';
import { mapBasePublicationToStandarized } from './mapBasePublicationToStandarized';

const FIRST_MODEL_ID = 53;
const SECOND_MODEL_ID = 63;
const THIRD_MODEL_ID = 99;

const FIRST_ELEMENT_ID = 100;
const SECOND_ELEMENT_ID = 200;
const THIRD_ELEMENT_ID = 300;
const FOURTH_ELEMENT_ID = 400;

const MODEL_NAME_ID_MAP: Record<number, string> = {
  [FIRST_MODEL_ID]: 'first model',
  [SECOND_MODEL_ID]: 'second model',
  [THIRD_MODEL_ID]: 'third model',
};

const BASE_PUBLICATION: Publication =
  PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK.content[FIRST_ARRAY_ELEMENT];
const BASE_ELEMENT = BASE_PUBLICATION.elements[FIRST_ARRAY_ELEMENT];
const PUBLICATION: Publication = {
  ...BASE_PUBLICATION,
  elements: [
    {
      ...BASE_ELEMENT,
      id: FIRST_ELEMENT_ID,
      model: FIRST_MODEL_ID,
    },
    {
      ...BASE_ELEMENT,
      id: SECOND_ELEMENT_ID,
      model: SECOND_MODEL_ID,
    },
    {
      ...BASE_ELEMENT,
      id: THIRD_ELEMENT_ID,
      model: THIRD_MODEL_ID, // model id duplicate
    },
    {
      ...BASE_ELEMENT,
      id: FOURTH_ELEMENT_ID,
      model: THIRD_MODEL_ID, // model id duplicate
    },
  ],
};

const BIO_ENTITY_CONTENT = (id: number, elementId: string): BioEntityContent => ({
  ...bioEntityContentFixture,
  bioEntity: {
    ...bioEntityContentFixture.bioEntity,
    id,
    elementId,
  },
});

jest.mock('../../utils/fetchElementData');
(fetchElementData as jest.Mock).mockImplementation(
  (id: string): BioEntityContent =>
    ({
      [`${FIRST_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(FIRST_ELEMENT_ID, 'mi100'),
      [`${SECOND_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(SECOND_ELEMENT_ID, 'ne200'),
      [`${THIRD_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(THIRD_ELEMENT_ID, 'r300'),
      [`${FOURTH_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(FOURTH_ELEMENT_ID, 'va400'),
    })[id] as BioEntityContent,
);

const getFuncResult = async (
  publication: Publication,
  modelNameIdMap: Record<number, string> = MODEL_NAME_ID_MAP,
): Promise<StandarizedPublication> =>
  mapBasePublicationToStandarized(publication, { modelNameIdMap });

describe('mapBasePublicationToStandarized - util', () => {
  it('should return valid pubmedId, journal, year and title', async () => {
    const results = await getFuncResult(PUBLICATION);
    const { pubmedId, journal, year, title } = PUBLICATION.article;

    expect(results.pubmedId).toBe(pubmedId);
    expect(results.journal).toBe(journal);
    expect(results.year).toBe(year.toString());
    expect(results.title).toBe(title);
  });

  it('should return joined authors', async () => {
    const results = await getFuncResult(PUBLICATION);
    const { authors } = PUBLICATION.article;

    expect(results.authors).toBe(authors.join(','));
  });

  it('should return joined and unique model names if present', async () => {
    const results = await getFuncResult(PUBLICATION);

    expect(results.modelNames).toBe(['first model', 'second model', 'third model'].join(','));
  });

  it('should return empty model names string if model not existing', async () => {
    const EMPTY_MODEL_ID_MAP = {};
    const results = await getFuncResult(PUBLICATION, EMPTY_MODEL_ID_MAP);

    expect(results.modelNames).toBe('');
  });

  it('should return joined elementsIds', async () => {
    const results = await getFuncResult(PUBLICATION);

    expect(results.elementsIds).toBe(['mi100', 'ne200', 'r300', 'va400'].join(','));
  });
});

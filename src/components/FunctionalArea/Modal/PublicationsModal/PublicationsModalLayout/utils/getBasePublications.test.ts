import { apiPath } from '@/redux/apiPath';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { showToast } from '@/utils/showToast';
import { PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK } from '../../../../../../models/mocks/publicationsResponseMock';
import { getBasePublications } from './getBasePublications';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

jest.mock('./../../../../../../utils/showToast');

describe('getBasePublications - util', () => {
  const length = 10;

  it('should return valid data if provided', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getPublications({ params: { length } }))
      .reply(HttpStatusCode.Ok, JSON.stringify(PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK));

    const result = await getBasePublications({ length });

    expect(result).toStrictEqual(
      JSON.parse(JSON.stringify(PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK.content)),
    );
  });

  it('should return empty array if data structure is invalid', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getPublications({ params: { length } }))
      .reply(HttpStatusCode.Ok, { randomObject: true });

    const result = await getBasePublications({ length });

    expect(result).toStrictEqual([]);
  });

  it('should return empty array and show toast error if http error', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getPublications({ params: { length } }))
      .reply(HttpStatusCode.BadRequest);

    const result = await getBasePublications({ length });

    expect(result).toStrictEqual([]);
    expect(showToast).toHaveBeenCalledWith({
      message:
        "Problem with fetching publications: The server couldn't understand your request. Please check your input and try again.",
      type: 'error',
    });
  });
});

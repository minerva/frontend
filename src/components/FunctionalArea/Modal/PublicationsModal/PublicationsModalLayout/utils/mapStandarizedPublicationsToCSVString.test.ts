import { StandarizedPublication } from '@/types/publications';
import { mapStandarizedPublicationsToCSVString } from './mapStandarizedPublicationsToCSVString';

const CASES: [StandarizedPublication[], string][] = [
  [[], ''],
  [
    [
      {
        pubmedId: '',
        year: '',
        journal: '',
        authors: '',
        title: '',
        modelNames: '',
        elementsIds: '',
      },
    ],
    '"","","","","","",""',
  ],
  [
    [
      {
        authors: 'authors',
        title: 'title',
        journal: 'journal',
        pubmedId: 'pubmedId',
        modelNames: 'modelNames',
        elementsIds: 'elementsIds',
        year: 'year',
      },
    ],
    '"pubmedId","title","authors","journal","year","elementsIds","modelNames"',
  ],
];

describe('mapStandarizedPublicationsToCSVString - util', () => {
  it.each(CASES)('should return valid string', (input, result) => {
    expect(mapStandarizedPublicationsToCSVString(input)).toBe(result);
  });
});

/* eslint-disable react/no-children-prop */
import { FIRST_ARRAY_ELEMENT, SECOND_ARRAY_ELEMENT, THIRD_ARRAY_ELEMENT } from '@/constants/common';
import { DEFAULT_ERROR } from '@/constants/errors';
import { bioEntityContentFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { MODELS_MOCK } from '@/models/mocks/modelsMock';
import { PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK } from '@/models/mocks/publicationsResponseMock';
import { apiPath } from '@/redux/apiPath';
import { downloadFileFromBlob } from '@/redux/export/export.utils';
import { StoreType } from '@/redux/store';
import { BioEntityContent, Publication } from '@/types/models';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen, waitFor } from '@testing-library/react';
import { HttpStatusCode } from 'axios';
import { fetchElementData } from '../utils/fetchElementData';
import { PublicationsModalLayout } from './PublicationsModalLayout.component';

const FIRST_MODEL_ID = MODELS_MOCK[FIRST_ARRAY_ELEMENT].id;
const SECOND_MODEL_ID = MODELS_MOCK[SECOND_ARRAY_ELEMENT].id;
const THIRD_MODEL_ID = MODELS_MOCK[THIRD_ARRAY_ELEMENT].id;

const FIRST_ELEMENT_ID = 100;
const SECOND_ELEMENT_ID = 200;
const THIRD_ELEMENT_ID = 300;
const FOURTH_ELEMENT_ID = 400;

const BASE_PUBLICATION: Publication =
  PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK.content[FIRST_ARRAY_ELEMENT];
const BASE_ELEMENT = BASE_PUBLICATION.elements[FIRST_ARRAY_ELEMENT];
const PUBLICATION: Publication = {
  ...BASE_PUBLICATION,
  elements: [
    {
      ...BASE_ELEMENT,
      id: FIRST_ELEMENT_ID,
      model: FIRST_MODEL_ID,
    },
    {
      ...BASE_ELEMENT,
      id: SECOND_ELEMENT_ID,
      model: SECOND_MODEL_ID,
    },
    {
      ...BASE_ELEMENT,
      id: THIRD_ELEMENT_ID,
      model: THIRD_MODEL_ID, // model id duplicate
    },
    {
      ...BASE_ELEMENT,
      id: FOURTH_ELEMENT_ID,
      model: THIRD_MODEL_ID, // model id duplicate
    },
  ],
};

const BIO_ENTITY_CONTENT = (id: number, elementId: string): BioEntityContent => ({
  ...bioEntityContentFixture,
  bioEntity: {
    ...bioEntityContentFixture.bioEntity,
    id,
    elementId,
  },
});

jest.mock('../../../../../redux/export/export.utils');
jest.mock('../utils/fetchElementData');
(fetchElementData as jest.Mock).mockImplementation(
  (id: string): BioEntityContent =>
    ({
      [`${FIRST_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(FIRST_ELEMENT_ID, 'mi100'),
      [`${SECOND_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(SECOND_ELEMENT_ID, 'ne200'),
      [`${THIRD_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(THIRD_ELEMENT_ID, 'r300'),
      [`${FOURTH_ELEMENT_ID}`]: BIO_ENTITY_CONTENT(FOURTH_ELEMENT_ID, 'va400'),
    })[id] as BioEntityContent,
);

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const renderComponent = (initialStore?: InitialStoreState): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStore);

  return (
    render(
      <Wrapper>
        <PublicationsModalLayout children={null} />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('PublicationsModalLayout - component', () => {
  const length = 1;
  mockedAxiosNewClient
    .onGet(apiPath.getPublications({ params: { length } }))
    .reply(HttpStatusCode.Ok, {
      ...PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK,
      content: [PUBLICATION],
    });

  it('should render download csv button', () => {
    renderComponent();

    expect(screen.getByTestId('download-csv-button')).toBeInTheDocument();
  });

  it('should run download file with valid content when download csv clicked', async () => {
    renderComponent({
      publications: {
        data: {
          ...PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK,
          content: [PUBLICATION],
          filteredSize: length,
        },
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        sortColumn: '',
        sortOrder: 'asc',
        searchValue: '',
        selectedModelId: undefined,
      },
      models: { data: MODELS_MOCK, loading: 'idle', error: DEFAULT_ERROR },
    });

    const downloadButton = screen.getByTestId('download-csv-button');
    downloadButton.click();

    await waitFor(() => {
      expect(downloadFileFromBlob).toHaveBeenCalledWith(
        '"10049997","The glutamate receptor ion channels.","Dingledine R, Borges K, Bowie D, Traynelis SF.","Pharmacological reviews","1999","mi100,ne200,r300,va400","Core PD map,Histamine signaling,PRKN substrates"',
        'publications.csv',
      );
    });
  });
});

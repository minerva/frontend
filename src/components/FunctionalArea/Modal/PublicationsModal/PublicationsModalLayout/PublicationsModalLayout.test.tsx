import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { StoreType } from '@/redux/store';
import { render, screen } from '@testing-library/react';
import { PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_STATE_MOCK } from '@/redux/publications/publications.mock';
import { PublicationsModalLayout } from './PublicationsModalLayout.component';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <PublicationsModalLayout>
          <div>children</div>
        </PublicationsModalLayout>
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('Publications Modal Layout - component', () => {
  it('should render number of publications', () => {
    renderComponent({ publications: PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_STATE_MOCK });

    expect(screen.getByText('Publications (1586 results)')).toBeInTheDocument();
  });
  it.skip('should render download csv button', () => {});
  it.skip('should trigger download on csv button click', () => {});
  it('should render search input', () => {
    renderComponent({ publications: PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_STATE_MOCK });

    expect(screen.getByTestId('search-input')).toBeInTheDocument();
  });
  it('should render children', () => {
    renderComponent({ publications: PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_STATE_MOCK });

    expect(screen.getByText('children')).toBeInTheDocument();
  });
});

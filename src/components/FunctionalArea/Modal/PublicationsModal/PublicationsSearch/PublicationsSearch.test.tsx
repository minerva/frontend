// remember to mock api call

describe('PublicationsSearch', () => {
  it('should render the input field', () => {});

  it('should update the value when the input field changes', () => {});

  it('should dispatch getPublications action when the input field changes', () => {});

  it('should disable the input and button when isLoading is true', () => {});
});

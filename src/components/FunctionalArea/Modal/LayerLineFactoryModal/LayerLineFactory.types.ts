import { Arrow, Color, Segment } from '@/types/models';

export type LayerLineFactoryForm = {
  color: Color;
  lineType: string;
  width: number;
  startArrow: string;
  endArrow: string;
};

export type LayerLineFactoryPayload = {
  color: Color;
  lineType: string;
  width: number;
  startArrow: Arrow;
  endArrow: Arrow;
  segments: Array<Segment>;
  z: number;
};

export type LayerLineEditFactoryPayload = {
  color: Color;
  lineType: string;
  width: number;
  startArrow: Arrow;
  endArrow: Arrow;
  segments: Array<Segment>;
  z: number;
};

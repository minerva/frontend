/* eslint-disable no-magic-numbers */
import { ColorTilePicker } from '@/shared/ColorPicker/ColorTilePicker.component';
import hexToRgbIntAlpha from '@/utils/convert/hexToRgbIntAlpha';
import { Color } from '@/types/models';
import { LayerLineFactoryForm } from '@/components/FunctionalArea/Modal/LayerLineFactoryModal/LayerLineFactory.types';
import { Select } from '@/shared/Select';
import React from 'react';
import { Input } from '@/shared/Input';
import {
  MAX_LINE_WIDTH,
  MIN_LINE_WIDTH,
} from '@/components/FunctionalArea/Modal/LayerLineFactoryModal/LayerLineFactory.constants';

type LayerLineFormProps = {
  data: LayerLineFactoryForm;
  lineTypes: Array<{ id: string; name: string }>;
  arrowTypes: Array<{ id: string; name: string }>;
  onChange: (value: number | string | Color, key: string) => void;
};

export const LayerLineForm = ({
  data,
  onChange,
  lineTypes,
  arrowTypes,
}: LayerLineFormProps): React.JSX.Element => {
  const onLineWidthChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const value = Number(event.target.value);
    let resultValue = value;
    if (value < MIN_LINE_WIDTH) {
      resultValue = MIN_LINE_WIDTH;
    }
    if (value > MAX_LINE_WIDTH) {
      resultValue = MAX_LINE_WIDTH;
    }
    onChange(resultValue, 'width');
  };

  return (
    <div className="grid grid-cols-3 gap-2">
      <div>
        <span>Color:</span>
        <ColorTilePicker
          initialColor={data.color}
          colorChange={color => onChange(hexToRgbIntAlpha(color), 'color')}
        />
      </div>
      <div>
        <span>Line type:</span>
        <Select
          options={lineTypes}
          selectedId={data.lineType}
          listClassName="max-h-48"
          testId="font-size-select"
          onChange={value => onChange(value, 'lineType')}
        />
      </div>
      <div>
        <span>Line width:</span>
        <Input
          type="number"
          name="line-width"
          min={MIN_LINE_WIDTH}
          max={MAX_LINE_WIDTH}
          id="line-width"
          value={data.width}
          onChange={onLineWidthChange}
          className="text-sm font-medium text-font-400"
        />
      </div>

      <div>
        <span>Start arrow:</span>
        <Select
          options={arrowTypes}
          selectedId={data.startArrow}
          listClassName="max-h-48"
          testId="font-size-select"
          onChange={value => onChange(value, 'startArrow')}
        />
      </div>

      <div>
        <span>End arrow:</span>
        <Select
          options={arrowTypes}
          selectedId={data.endArrow}
          listClassName="max-h-48"
          testId="font-size-select"
          onChange={value => onChange(value, 'endArrow')}
        />
      </div>
    </div>
  );
};

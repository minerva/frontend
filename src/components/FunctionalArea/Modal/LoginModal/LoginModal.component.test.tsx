import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act } from 'react-dom/test-utils';
import { mockNetworkNewAPIResponse, mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { loginFixture } from '@/models/fixtures/loginFixture';
import { overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { userFixture } from '@/models/fixtures/userFixture';
import { ZERO } from '@/constants/common';
import { LoginModal } from './LoginModal.component';

const mockedAxiosClient = mockNetworkResponse();
const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <LoginModal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const testOverlaysPageFixture = {
  ...overlaysPageFixture,
  content: [
    {
      ...overlaysPageFixture.content[ZERO],
      publicOverlay: false,
    },
  ],
};

describe('LoginModal - component', () => {
  test('renders LoginModal component', () => {
    renderComponent();

    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    expect(loginInput).toBeInTheDocument();
    expect(passwordInput).toBeInTheDocument();
  });

  test('handles input change correctly', () => {
    renderComponent();

    const loginInput: HTMLInputElement = screen.getByLabelText(/login/i);
    const passwordInput: HTMLInputElement = screen.getByLabelText(/password/i);

    fireEvent.change(loginInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });

    expect(loginInput.value).toBe('testuser');
    expect(passwordInput.value).toBe('testpassword');
  });

  test('submits form', () => {
    renderComponent();

    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(loginInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });
    act(() => {
      submitButton.click();
    });

    expect(submitButton).toBeDisabled();
  });
  it('should fetch user overlays when login is successful', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          creator: loginFixture.login,
          publicOverlay: false,
        }),
      )
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent();
    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(loginInput, { target: { value: loginFixture.login } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });
    act(() => {
      submitButton.click();
    });

    await waitFor(() => {
      expect(store.getState().user.loading).toBe('succeeded');
    });

    expect(store.getState().overlays.userOverlays.loading).toBe('succeeded');
    expect(store.getState().overlays.userOverlays.data).toEqual(testOverlaysPageFixture.content);
  });
  it('should display loggedInMenuModal after successful login as admin', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, {
      ...userFixture,
      privileges: [
        {
          privilegeType: 'IS_ADMIN',
          objectId: null,
        },
      ],
    });
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          creator: loginFixture.login,
          publicOverlay: false,
        }),
      )
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
    });
    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(loginInput, { target: { value: loginFixture.login } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });
    act(() => {
      submitButton.click();
    });

    await waitFor(() => {
      const modalState = store.getState().modal;
      expect(modalState.isOpen).toBeTruthy();
      expect(modalState.modalName).toBe('logged-in-menu');
    });
  });
  it('should display loggedInMenuModal after successful login as curator', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, {
      ...userFixture,
      privileges: [
        {
          privilegeType: 'IS_CURATOR',
          objectId: null,
        },
      ],
    });
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          creator: loginFixture.login,
          publicOverlay: false,
        }),
      )
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
    });
    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(loginInput, { target: { value: loginFixture.login } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });
    act(() => {
      submitButton.click();
    });

    await waitFor(() => {
      const modalState = store.getState().modal;
      expect(modalState.isOpen).toBeTruthy();
      expect(modalState.modalName).toBe('logged-in-menu');
    });
  });
  it('should close modal after successful login as user', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    mockedAxiosNewClient
      .onGet(
        apiPath.getAllUserOverlaysByCreatorQuery({
          creator: loginFixture.login,
          publicOverlay: false,
        }),
      )
      .reply(HttpStatusCode.Ok, testOverlaysPageFixture);

    const { store } = renderComponent({
      modal: MODAL_INITIAL_STATE_MOCK,
    });
    const loginInput = screen.getByLabelText(/login/i);
    const passwordInput = screen.getByLabelText(/password/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(loginInput, { target: { value: loginFixture.login } });
    fireEvent.change(passwordInput, { target: { value: 'testpassword' } });
    act(() => {
      submitButton.click();
    });

    await waitFor(() => {
      const modalState = store.getState().modal;
      expect(modalState.isOpen).toBeFalsy();
      expect(modalState.modalName).toBe('none');
    });
  });
});

import { StoreType } from '@/redux/store';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { act } from 'react-dom/test-utils';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { AddCommentModal } from '@/components/FunctionalArea/Modal/AddCommentModal/AddCommentModal.component';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { ZERO } from '@/constants/common';
import { commentFixture } from '@/models/fixtures/commentFixture';
import { commentsFixture } from '@/models/fixtures/commentsFixture';

const mockedAxiosClient = mockNetworkResponse();

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <AddCommentModal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('AddCommentModal - component', () => {
  test('renders AddCommentModal component', () => {
    renderComponent();

    const emailInput = screen.getByLabelText(/email/i);
    const contentInput = screen.getByLabelText(/content/i);
    expect(emailInput).toBeInTheDocument();
    expect(contentInput).toBeInTheDocument();
  });

  test('handles input change correctly', () => {
    renderComponent();

    const emailInput: HTMLInputElement = screen.getByLabelText(/email/i);
    const contentInput: HTMLInputElement = screen.getByLabelText(/content/i);

    fireEvent.change(emailInput, { target: { value: 'test@email.pl' } });
    fireEvent.change(contentInput, { target: { value: 'test content' } });

    expect(emailInput.value).toBe('test@email.pl');
    expect(contentInput.value).toBe('test content');
  });

  test('submits form', async () => {
    mockedAxiosClient
      .onPost(apiPath.addComment(ZERO, ZERO, ZERO))
      .reply(HttpStatusCode.Ok, commentFixture);
    mockedAxiosClient.onGet(apiPath.getComments()).reply(HttpStatusCode.Ok, commentsFixture);

    const { store } = renderComponent({
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        isOpen: true,
        modalName: 'add-comment',
      },
    });

    const emailInput: HTMLInputElement = screen.getByLabelText(/email/i);
    const contentInput: HTMLInputElement = screen.getByLabelText(/content/i);
    const submitButton = screen.getByText(/submit/i);

    fireEvent.change(emailInput, { target: { value: 'test@email.pl' } });
    fireEvent.change(contentInput, { target: { value: 'test content' } });
    act(() => {
      submitButton.click();
    });

    await waitFor(() => {
      const modalState = store.getState().modal;
      expect(modalState.modalName).toBe('none');
      expect(modalState.isOpen).toBeFalsy();
    });
  });
});

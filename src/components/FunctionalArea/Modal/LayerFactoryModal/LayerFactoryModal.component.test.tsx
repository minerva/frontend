/* eslint-disable no-magic-numbers */
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { layerFixture } from '@/models/fixtures/layerFixture';
import { layersFixture } from '@/models/fixtures/layersFixture';
import { layerTextsFixture } from '@/models/fixtures/layerTextsFixture';
import { layerRectsFixture } from '@/models/fixtures/layerRectsFixture';
import { layerOvalsFixture } from '@/models/fixtures/layerOvalsFixture';
import { layerLinesFixture } from '@/models/fixtures/layerLinesFixture';
import { act } from 'react-dom/test-utils';
import { layerImagesFixture } from '@/models/fixtures/layerImagesFixture';
import { LAYERS_STATE_INITIAL_LAYER_MOCK } from '@/redux/layers/layers.mock';
import * as appDispatch from '@/redux/hooks/useAppDispatch';
import { LayerFactoryModal } from './LayerFactoryModal.component';

jest.mock('../../../../utils/showToast');
jest.mock('../../../../redux/hooks/useAppDispatch');

const dispatchMock = jest.fn(() => {
  return {
    unwrap: jest.fn().mockResolvedValue(layerFixture),
  };
});
jest.spyOn(appDispatch, 'useAppDispatch').mockReturnValue(dispatchMock);

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const renderComponent = (
  initialStoreState: InitialStoreState = {
    layers: {
      0: {
        ...LAYERS_STATE_INITIAL_LAYER_MOCK,
      },
    },
  },
): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <LayerFactoryModal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('LayerFactoryModal - component', () => {
  it('should render LayerFactoryModal component', () => {
    renderComponent();

    const name = screen.getByTestId('layer-factory-name');
    const visible = screen.getByTestId('layer-factory-visible');
    const locked = screen.getByTestId('layer-factory-locked');
    expect(name).toBeInTheDocument();
    expect(visible).toBeInTheDocument();
    expect(locked).toBeInTheDocument();
  });

  it('should handles input change correctly', () => {
    renderComponent();

    const nameInput: HTMLInputElement = screen.getByTestId('layer-factory-name');

    fireEvent.change(nameInput, { target: { value: 'test layer' } });

    expect(nameInput.value).toBe('test layer');
  });

  it('should fetch layer when the form is  successfully submitted', async () => {
    mockedAxiosNewClient.onPost(apiPath.storeLayer(0)).reply(HttpStatusCode.Ok, layerFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayer(0, layerFixture.id))
      .reply(HttpStatusCode.Ok, layerFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayerTexts(0, layersFixture.content[0].id))
      .reply(HttpStatusCode.Ok, layerTextsFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayerRects(0, layersFixture.content[0].id))
      .reply(HttpStatusCode.Ok, layerRectsFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayerOvals(0, layersFixture.content[0].id))
      .reply(HttpStatusCode.Ok, layerOvalsFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayerLines(0, layersFixture.content[0].id))
      .reply(HttpStatusCode.Ok, layerLinesFixture);
    mockedAxiosNewClient
      .onGet(apiPath.getLayerImages(0, layersFixture.content[0].id))
      .reply(HttpStatusCode.Ok, layerImagesFixture);

    renderComponent();
    const nameInput: HTMLInputElement = screen.getByTestId('layer-factory-name');
    const submitButton = screen.getByTestId('submit');

    fireEvent.change(nameInput, { target: { value: 'test layer' } });
    act(() => {
      submitButton.click();
    });
    await waitFor(() => {
      expect(dispatchMock).toHaveBeenCalledTimes(3);
    });
  });
});

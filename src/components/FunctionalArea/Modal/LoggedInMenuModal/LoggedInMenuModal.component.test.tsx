import { AppDispatch, RootState } from '@/redux/store';
import {
  InitialStoreState,
  getReduxStoreWithActionsListener,
} from '@/utils/testing/getReduxStoreActionsListener';
import { fireEvent, render, screen } from '@testing-library/react';
import { MockStoreEnhanced } from 'redux-mock-store';
import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { LoggedInMenuModal } from './LoggedInMenuModal.component';

const renderComponent = (
  initialStoreState: InitialStoreState = {},
): { store: MockStoreEnhanced<Partial<RootState>, AppDispatch> } => {
  const { Wrapper, store } = getReduxStoreWithActionsListener(initialStoreState);

  return (
    render(
      <Wrapper>
        <LoggedInMenuModal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('LoggedInMenuModal component', () => {
  it('should render all actions correctly', () => {
    renderComponent();
    expect(screen.getByText('Back to the map')).toBeVisible();
    expect(screen.getByText('Go to the admin panel')).toBeVisible();
  });

  it('should close modal when "Back to the map" button is clicked', () => {
    const { store } = renderComponent();

    fireEvent.click(screen.getByText('Back to the map'));
    const actions = store.getActions();
    expect(actions[FIRST_ARRAY_ELEMENT].type).toBe('modal/closeModal');
  });
});

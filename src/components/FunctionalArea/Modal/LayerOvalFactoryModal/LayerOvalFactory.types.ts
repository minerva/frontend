import { Color } from '@/types/models';

export type LayerOvalFactoryForm = {
  color: Color;
};

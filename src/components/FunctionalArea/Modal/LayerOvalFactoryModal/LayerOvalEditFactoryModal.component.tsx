/* eslint-disable no-magic-numbers */
import React, { useState } from 'react';
import './LayerOvalFactoryModal.styles.css';
import { LoadingIndicator } from '@/shared/LoadingIndicator';
import { Button } from '@/shared/Button';
import { useAppSelector } from '@/redux/hooks/useAppSelector';
import { currentModelIdSelector } from '@/redux/models/models.selectors';
import { Color } from '@/types/models';
import { showToast } from '@/utils/showToast';
import { closeModal } from '@/redux/modal/modal.slice';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';
import { SerializedError } from '@reduxjs/toolkit';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { useMapInstance } from '@/utils/context/mapInstanceContext';
import { mapEditToolsLayerObjectSelector } from '@/redux/mapEditTools/mapEditTools.selectors';
import updateElement from '@/components/Map/MapViewer/utils/shapes/layer/utils/updateElement';
import { LayerOvalFactoryForm } from '@/components/FunctionalArea/Modal/LayerOvalFactoryModal/LayerOvalFactory.types';
import { LayerOvalForm } from '@/components/FunctionalArea/Modal/LayerOvalFactoryModal/LayerOvalForm.component';
import { updateLayerOval } from '@/redux/layers/layers.thunks';
import { layerUpdateOval } from '@/redux/layers/layers.slice';

export const LayerOvalEditFactoryModal: React.FC = () => {
  const layerObject = useAppSelector(mapEditToolsLayerObjectSelector);
  const currentModelId = useAppSelector(currentModelIdSelector);
  const dispatch = useAppDispatch();
  const { mapInstance } = useMapInstance();

  if (!layerObject || !('color' in layerObject && 'borderColor' in layerObject)) {
    throw new Error('Invalid layer oval object');
  }

  const [isSending, setIsSending] = useState<boolean>(false);
  const [data, setData] = useState<LayerOvalFactoryForm>({
    color: layerObject.color,
  });

  const handleSubmit = async (): Promise<void> => {
    if (!layerObject) {
      return;
    }
    try {
      const layerOval = await dispatch(
        updateLayerOval({
          modelId: currentModelId,
          layerId: layerObject.layer,
          id: layerObject.id,
          x: layerObject.x,
          y: layerObject.y,
          z: layerObject.z,
          width: layerObject.width,
          height: layerObject.height,
          color: data.color,
        }),
      ).unwrap();

      if (!layerOval) {
        showToast({
          type: 'error',
          message: 'An error occurred while editing the oval.',
        });
        return;
      }

      dispatch(layerUpdateOval({ modelId: currentModelId, layerId: layerOval.layer, layerOval }));
      dispatch(mapEditToolsSetLayerObject(layerOval));
      updateElement(mapInstance, layerOval.layer, layerOval);
      showToast({
        type: 'success',
        message: 'The oval has been successfully updated.',
      });
      dispatch(closeModal());
    } catch (error) {
      const typedError = error as SerializedError;
      showToast({
        type: 'error',
        message: typedError.message || 'An error occurred while editing the oval.',
      });
    } finally {
      setIsSending(false);
    }
  };

  const changeValues = (value: string | number | Color, key: string): void => {
    setData(prevData => ({ ...prevData, [key]: value }));
  };

  return (
    <div className="relative flex w-[300px] flex-col gap-4 rounded-b-lg border border-t-[#E1E0E6] bg-white p-[24px]">
      {isSending && (
        <div className="c-layer-oval-factory-modal-loader">
          <LoadingIndicator width={44} height={44} />
        </div>
      )}
      <LayerOvalForm onChange={changeValues} data={data} />
      <hr />
      <Button
        type="button"
        onClick={handleSubmit}
        className="justify-center self-end justify-self-end text-base font-medium"
      >
        Submit
      </Button>
    </div>
  );
};

import { Color } from '@/types/models';
import { HorizontalAlign, VerticalAlign } from '@/components/Map/MapViewer/MapViewer.types';

export type LayerTextFactoryForm = {
  notes: string;
  fontSize: number;
  horizontalAlign: HorizontalAlign;
  verticalAlign: VerticalAlign;
  color: Color;
  borderColor: Color;
};

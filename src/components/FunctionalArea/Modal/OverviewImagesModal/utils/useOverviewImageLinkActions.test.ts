/* eslint-disable no-magic-numbers */
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODELS_MOCK_SHORT } from '@/models/mocks/modelsMock';
import {
  OVERVIEW_LINK_IMAGE_MOCK,
  OVERVIEW_LINK_MODEL_MOCK,
} from '@/models/mocks/overviewImageMocks';
import {
  initialMapDataFixture,
  openedMapsInitialValueFixture,
  openedMapsThreeSubmapsFixture,
} from '@/redux/map/map.fixtures';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { PluginsEventBus } from '@/services/pluginsManager/pluginsEventBus';
import { OverviewImageLink } from '@/types/models';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { MAIN_MAP_ID } from '@/constants/mocks';
import {
  FIRST_ARRAY_ELEMENT,
  NOOP,
  SECOND_ARRAY_ELEMENT,
  SIZE_OF_EMPTY_ARRAY,
  THIRD_ARRAY_ELEMENT,
} from '../../../../../constants/common';
import { useOverviewImageLinkActions } from './useOverviewImageLinkActions';

jest.mock('../../../../../constants/common', () => ({
  ...jest.requireActual('../../../../../constants/common'),
  NOOP: jest.fn(),
}));

describe('useOverviewImageLinkActions - hook', () => {
  describe('when clicked on image link', () => {
    describe('when image id is NOT valid', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        modal: {
          ...MODAL_INITIAL_STATE_MOCK,
          overviewImagesState: {
            imageId: 0,
          },
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      it('should NOT fire action set overview image id', () => {
        handleLinkClick(OVERVIEW_LINK_IMAGE_MOCK);
        const actions = store.getActions();
        expect(actions.length).toEqual(SIZE_OF_EMPTY_ARRAY);
      });
    });

    describe('when image id is valid', () => {
      const { Wrapper, store } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [
              {
                ...PROJECT_OVERVIEW_IMAGE_MOCK,
                height: 500,
                width: 500,
              },
            ],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        modal: {
          ...MODAL_INITIAL_STATE_MOCK,
          overviewImagesState: {
            imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
          },
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsThreeSubmapsFixture,
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      it('should fire action set overview image id', () => {
        handleLinkClick(OVERVIEW_LINK_IMAGE_MOCK);
        const actions = store.getActions();
        expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
          payload: 440,
          type: 'modal/setOverviewImageId',
        });
      });
    });
  });
  describe('when clicked on model link', () => {
    describe('when model is not available', () => {});

    describe('when model is available', () => {
      describe('when map is already opened', () => {
        const { Wrapper, store } = getReduxStoreWithActionsListener({
          project: {
            data: {
              ...projectFixture,
              overviewImageViews: [
                {
                  ...PROJECT_OVERVIEW_IMAGE_MOCK,
                  height: 500,
                  width: 500,
                },
              ],
              topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
            },
            loading: 'succeeded',
            error: { message: '', name: '' },
            projectId: '',
          },
          modal: {
            ...MODAL_INITIAL_STATE_MOCK,
            overviewImagesState: {
              imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
            },
          },
          map: {
            data: {
              ...initialMapDataFixture,
              modelId: MAIN_MAP_ID,
            },
            loading: 'succeeded',
            error: { name: '', message: '' },
            openedMaps: openedMapsThreeSubmapsFixture,
          },
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        });

        const {
          result: {
            current: { handleLinkClick },
          },
        } = renderHook(() => useOverviewImageLinkActions(), {
          wrapper: Wrapper,
        });

        it('should set active map', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
            payload: {
              modelId: MAIN_MAP_ID,
            },
            type: 'map/setActiveMap',
          });
        });

        it('should set map position', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[SECOND_ARRAY_ELEMENT]).toStrictEqual({
            payload: { x: 15570, y: 3016, z: 7 },
            type: 'map/setMapPosition',
          });
        });

        it('should close modal', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[THIRD_ARRAY_ELEMENT]).toStrictEqual({
            payload: undefined,
            type: 'modal/closeModal',
          });
        });
      });

      describe('when map is not opened', () => {
        const { Wrapper, store } = getReduxStoreWithActionsListener({
          project: {
            data: {
              ...projectFixture,
              overviewImageViews: [
                {
                  ...PROJECT_OVERVIEW_IMAGE_MOCK,
                  height: 500,
                  width: 500,
                },
              ],
              topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
            },
            loading: 'succeeded',
            error: { message: '', name: '' },
            projectId: '',
          },
          modal: {
            ...MODAL_INITIAL_STATE_MOCK,
            overviewImagesState: {
              imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
            },
          },
          map: {
            data: {
              ...initialMapDataFixture,
              modelId: MAIN_MAP_ID,
            },
            loading: 'succeeded',
            error: { name: '', message: '' },
            openedMaps: openedMapsInitialValueFixture,
          },
          models: {
            data: MODELS_MOCK_SHORT,
            loading: 'succeeded',
            error: { name: '', message: '' },
          },
        });

        const {
          result: {
            current: { handleLinkClick },
          },
        } = renderHook(() => useOverviewImageLinkActions(), {
          wrapper: Wrapper,
        });

        it('should open map and set as active', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[FIRST_ARRAY_ELEMENT]).toStrictEqual({
            payload: {
              modelId: MAIN_MAP_ID,
              modelName: 'Core PD map',
            },
            type: 'map/openMapAndSetActive',
          });
        });

        it('should set map position', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[SECOND_ARRAY_ELEMENT]).toStrictEqual({
            payload: { x: 15570, y: 3016, z: 7 },
            type: 'map/setMapPosition',
          });
        });

        it('should close modal', () => {
          handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
          const actions = store.getActions();
          expect(actions[THIRD_ARRAY_ELEMENT]).toStrictEqual({
            payload: undefined,
            type: 'modal/closeModal',
          });
        });
      });
    });
  });
  describe('when clicked on unsupported link', () => {
    const { Wrapper } = getReduxWrapperWithStore();
    const {
      result: {
        current: { handleLinkClick },
      },
    } = renderHook(() => useOverviewImageLinkActions(), {
      wrapper: Wrapper,
    });

    it('should noop', () => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore to simulate invalid link object
      handleLinkClick({ link: {} as unknown as OverviewImageLink });
      expect(NOOP).toBeCalled();
    });
  });
  describe('plugin event bus', () => {
    beforeEach(() => {
      PluginsEventBus.events = [];
    });
    afterEach(() => {
      jest.clearAllMocks();
    });
    it('should dispatch event if coordinates changed', () => {
      const { Wrapper } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [
              {
                ...PROJECT_OVERVIEW_IMAGE_MOCK,
                height: 500,
                width: 500,
              },
            ],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        modal: {
          ...MODAL_INITIAL_STATE_MOCK,
          overviewImagesState: {
            imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
          },
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsInitialValueFixture,
        },
        models: {
          data: MODELS_MOCK_SHORT,
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      handleLinkClick(OVERVIEW_LINK_MODEL_MOCK);
    });
    it('should not dispatch event if coordinates do not changed', () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      const { Wrapper } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [
              {
                ...PROJECT_OVERVIEW_IMAGE_MOCK,
                height: 500,
                width: 500,
              },
            ],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
            position: {
              initial: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
              last: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
            },
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsInitialValueFixture,
        },
        models: {
          data: MODELS_MOCK_SHORT,
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      handleLinkClick({
        ...OVERVIEW_LINK_MODEL_MOCK,
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(0);
    });
    it('should dispatch event if new submap has different id than current opened map', () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      const { Wrapper } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [
              {
                ...PROJECT_OVERVIEW_IMAGE_MOCK,
                height: 500,
                width: 500,
              },
            ],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: 5054,
            position: {
              initial: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
              last: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
            },
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsInitialValueFixture,
        },
        models: {
          data: MODELS_MOCK_SHORT,
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      handleLinkClick({
        ...OVERVIEW_LINK_MODEL_MOCK,
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(2);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapClose', 5054);
      expect(dispatchEventMock).toHaveBeenCalledWith('onSubmapOpen', MAIN_MAP_ID);
    });
    it('should not dispatch event if provided submap to open is already opened', () => {
      const dispatchEventMock = jest.spyOn(PluginsEventBus, 'dispatchEvent');
      const { Wrapper } = getReduxStoreWithActionsListener({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [
              {
                ...PROJECT_OVERVIEW_IMAGE_MOCK,
                height: 500,
                width: 500,
              },
            ],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        map: {
          data: {
            ...initialMapDataFixture,
            modelId: MAIN_MAP_ID,
            position: {
              initial: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
              last: {
                x: 15570.0,
                y: 3016.0,
                z: 7,
              },
            },
          },
          loading: 'succeeded',
          error: { name: '', message: '' },
          openedMaps: openedMapsInitialValueFixture,
        },
        models: {
          data: MODELS_MOCK_SHORT,
          loading: 'succeeded',
          error: { name: '', message: '' },
        },
      });

      const {
        result: {
          current: { handleLinkClick },
        },
      } = renderHook(() => useOverviewImageLinkActions(), {
        wrapper: Wrapper,
      });

      handleLinkClick({
        ...OVERVIEW_LINK_MODEL_MOCK,
      });

      expect(dispatchEventMock).toHaveBeenCalledTimes(0);
    });
  });
});

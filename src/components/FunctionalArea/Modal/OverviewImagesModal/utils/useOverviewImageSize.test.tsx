/* eslint-disable no-magic-numbers */
import { DEFAULT_OVERVIEW_IMAGE_SIZE } from '@/constants/project';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useOverviewImageSize } from './useOverviewImageSize';

describe('useOverviewImageSize - hook', () => {
  describe('when currentImage is not valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: 0,
        },
      },
    });

    const { result } = renderHook(
      () => useOverviewImageSize({ containerRect: { width: 800, height: 600 } as DOMRect }),
      {
        wrapper: Wrapper,
      },
    );

    it('should return default value', () => {
      expect(result.current).toStrictEqual(DEFAULT_OVERVIEW_IMAGE_SIZE);
    });
  });

  describe('when containerRect is not valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const { result } = renderHook(() => useOverviewImageSize({ containerRect: undefined }), {
      wrapper: Wrapper,
    });

    it('should return default value', () => {
      expect(result.current).toStrictEqual(DEFAULT_OVERVIEW_IMAGE_SIZE);
    });
  });

  describe('when data is valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const { result } = renderHook(
      () => useOverviewImageSize({ containerRect: { width: 1600, height: 1000 } as DOMRect }),
      {
        wrapper: Wrapper,
      },
    );

    const { height, width, sizeFactor } = result.current;

    it('should return calculated height, width, sizeFactor', () => {
      expect(height).toBeCloseTo(1000);
      expect(width).toBeCloseTo(1429.7);
      expect(sizeFactor).toBeCloseTo(0.247);
    });
  });
});

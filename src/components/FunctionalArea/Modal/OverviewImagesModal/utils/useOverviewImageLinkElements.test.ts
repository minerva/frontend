import { ZERO } from '@/constants/common';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useOverviewImageLinkConfigs } from './useOverviewImageLinkElements';

describe('useOverviewImageLinkConfigs - hook', () => {
  describe('when currentImage is undefined', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: 0,
        },
      },
    });

    const {
      result: { current: returnValue },
    } = renderHook(
      () =>
        useOverviewImageLinkConfigs({
          sizeFactor: 1,
        }),
      {
        wrapper: Wrapper,
      },
    );

    it('should return empty array', () => {
      expect(returnValue).toStrictEqual([]);
    });
  });

  describe('when sizeFactor is zero', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const {
      result: { current: returnValue },
    } = renderHook(
      () =>
        useOverviewImageLinkConfigs({
          sizeFactor: ZERO,
        }),
      {
        wrapper: Wrapper,
      },
    );

    it('should return empty array', () => {
      expect(returnValue).toStrictEqual([]);
    });
  });

  describe('when all args are valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const {
      result: { current: returnValue },
    } = renderHook(
      () =>
        useOverviewImageLinkConfigs({
          sizeFactor: 1,
        }),
      {
        wrapper: Wrapper,
      },
    );

    it('should return correct value', () => {
      expect(returnValue).toStrictEqual([
        {
          idObject: 2062,
          size: { top: 2187, left: 515, width: 558, height: 333 },
          onClick: expect.any(Function),
        },
        {
          idObject: 2063,
          size: { top: 1360, left: 2410, width: 282, height: 210 },
          onClick: expect.any(Function),
        },
        {
          idObject: 2064,
          size: { top: 497, left: 2830, width: 426, height: 335 },
          onClick: expect.any(Function),
        },
        {
          idObject: 2065,
          size: { top: 2259, left: 3232, width: 288, height: 197 },
          onClick: expect.any(Function),
        },
        {
          idObject: 2066,
          size: { top: 761, left: 4205, width: 420, height: 341 },
          onClick: expect.any(Function),
        },
        {
          idObject: 2067,
          size: { top: 1971, left: 4960, width: 281, height: 192 },
          onClick: expect.any(Function),
        },
      ]);
    });
  });
});

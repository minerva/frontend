import { BASE_MAP_IMAGES_URL } from '@/constants';
import { DEFAULT_OVERVIEW_IMAGE_SIZE } from '@/constants/project';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useOverviewImage } from './useOverviewImage';

const PROJECT_DIRECTORY = 'directory';

describe('useOverviewImage - hook', () => {
  describe('when image data is invalid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          directory: PROJECT_DIRECTORY,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: 0,
        },
      },
    });

    const { result } = renderHook(() => useOverviewImage({ containerRect: undefined }), {
      wrapper: Wrapper,
    });

    it('should return default size of image and empty imageUrl', () => {
      expect(result.current).toStrictEqual({
        imageUrl: '',
        size: DEFAULT_OVERVIEW_IMAGE_SIZE,
        linkConfigs: [],
      });
    });
  });

  describe('when containerReact is undefined', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          directory: PROJECT_DIRECTORY,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const { result } = renderHook(() => useOverviewImage({ containerRect: undefined }), {
      wrapper: Wrapper,
    });

    it('should return default size of image and valid imageUrl', () => {
      const imageUrl = `${BASE_MAP_IMAGES_URL}/map_images/${PROJECT_DIRECTORY}/${PROJECT_OVERVIEW_IMAGE_MOCK.filename}`;

      expect(result.current).toMatchObject({
        imageUrl,
        size: DEFAULT_OVERVIEW_IMAGE_SIZE,
      });
    });
  });

  describe('when containerReact is valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [
            {
              ...PROJECT_OVERVIEW_IMAGE_MOCK,
              height: 500,
              width: 500,
            },
          ],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          directory: PROJECT_DIRECTORY,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    const { result } = renderHook(
      () => useOverviewImage({ containerRect: { width: 100, height: 200 } as DOMRect }),
      {
        wrapper: Wrapper,
      },
    );

    it('should return size of image and valid imageUrl', () => {
      const imageUrl = `${BASE_MAP_IMAGES_URL}/map_images/${PROJECT_DIRECTORY}/${PROJECT_OVERVIEW_IMAGE_MOCK.filename}`;

      expect(result.current).toMatchObject({
        imageUrl,
        size: { height: 100, width: 100, sizeFactor: 0.2 },
      });
    });
  });
});

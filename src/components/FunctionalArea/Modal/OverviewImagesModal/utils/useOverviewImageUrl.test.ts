import { BASE_MAP_IMAGES_URL } from '@/constants';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { renderHook } from '@testing-library/react';
import { useOverviewImageUrl } from './useOverviewImageUrl';

const PROJECT_DIRECTORY = 'directory';

describe('useOverviewImageUrl - hook', () => {
  describe('when currentImage data is valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          directory: PROJECT_DIRECTORY,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: 0,
        },
      },
    });

    it('should return valid url', () => {
      const { result } = renderHook(() => useOverviewImageUrl(), { wrapper: Wrapper });

      expect(result.current).toBe('');
    });
  });

  describe('when currentImage data is valid', () => {
    const { Wrapper } = getReduxWrapperWithStore({
      project: {
        data: {
          ...projectFixture,
          overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
          topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
          directory: PROJECT_DIRECTORY,
        },
        loading: 'succeeded',
        error: { message: '', name: '' },
        projectId: '',
      },
      modal: {
        ...MODAL_INITIAL_STATE_MOCK,
        overviewImagesState: {
          imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
        },
      },
    });

    it('should return valid url', () => {
      const { result } = renderHook(() => useOverviewImageUrl(), { wrapper: Wrapper });

      expect(result.current).toBe(
        `${BASE_MAP_IMAGES_URL}/map_images/${PROJECT_DIRECTORY}/${PROJECT_OVERVIEW_IMAGE_MOCK.filename}`,
      );
    });
  });
});

import { OverviewImageLink } from '@/types/models';
import { OverviewImageLinkConfigSize } from '../OverviewImageModal.types';
import { DEFAULT_OVERVIEW_IMAGE_LINK_CONFIG } from '../OverviewImagesModal.constants';
import { getOverviewImageLinkSize } from './getOverviewImageLinkSize';

describe('getOverviewImageLinkSize - util', () => {
  const cases: [
    Pick<OverviewImageLink, 'polygon'>,
    {
      sizeFactor: number;
    },
    OverviewImageLinkConfigSize,
  ][] = [
    // invalid polygon
    [
      {
        polygon: [],
      },
      {
        sizeFactor: 1,
      },
      DEFAULT_OVERVIEW_IMAGE_LINK_CONFIG,
    ],
    // invalid polygon
    [
      {
        polygon: [
          { x: 0, y: 0 },
          { x: 0, y: 0 },
          { x: 0, y: 0 },
        ],
      },
      {
        sizeFactor: 1,
      },
      DEFAULT_OVERVIEW_IMAGE_LINK_CONFIG,
    ],
    // valid polygon with size of 0x0
    [
      {
        polygon: [
          { x: 0, y: 0 },
          { x: 0, y: 0 },
          { x: 0, y: 0 },
          { x: 0, y: 0 },
        ],
      },
      {
        sizeFactor: 1,
      },
      {
        top: 0,
        left: 0,
        width: 0,
        height: 0,
      },
    ],
    // valid polygon with size of 20x50
    [
      {
        polygon: [
          { x: 10, y: 0 },
          { x: 30, y: 0 },
          { x: 30, y: 50 },
          { x: 10, y: 50 },
        ],
      },
      {
        sizeFactor: 1,
      },
      {
        top: 0,
        left: 10,
        width: 20,
        height: 50,
      },
    ],
    // valid polygon with size of 27x67.5 in scale of 1.35
    [
      {
        polygon: [
          { x: 10, y: 0 },
          { x: 30, y: 0 },
          { x: 30, y: 50 },
          { x: 10, y: 50 },
        ],
      },
      {
        sizeFactor: 1.35,
      },
      {
        height: 67.5,
        left: 13.5,
        top: 0,
        width: 27,
      },
    ],
  ];

  it.each(cases)(
    'should return valid link config size',
    (overviewImageWithPolygon, options, finalConfigSize) => {
      expect(getOverviewImageLinkSize(overviewImageWithPolygon, options)).toStrictEqual(
        finalConfigSize,
      );
    },
  );
});

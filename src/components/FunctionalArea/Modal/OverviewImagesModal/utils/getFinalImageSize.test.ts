import { getFinalImageSize } from './getFinalImageSize';

describe('getFinalImageSize - util', () => {
  const cases = [
    [
      { width: 0, height: 0 },
      { width: 0, height: 0 },
      { width: 0, height: 0, sizeFactor: 0 },
    ],
    [
      { width: 100, height: 100 },
      { width: 100, height: 100 },
      { width: 100, height: 100, sizeFactor: 1 },
    ],
    [
      { width: 100, height: 100 },
      { width: 200, height: 250 },
      { width: 80, height: 100, sizeFactor: 0.4 },
    ],
    [
      { width: 10, height: 40 },
      { width: 40, height: 60 },
      { width: 10, height: 15, sizeFactor: 0.25 },
    ],
  ];

  it.each(cases)(
    'should return valid size and size factor',
    (containerSize, maxImageSize, finalSize) => {
      expect(getFinalImageSize(containerSize, maxImageSize)).toStrictEqual(finalSize);
    },
  );
});

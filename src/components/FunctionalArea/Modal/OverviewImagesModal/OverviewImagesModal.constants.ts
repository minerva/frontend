import { OverviewImageLinkConfigSize } from './OverviewImageModal.types';

export const DEFAULT_OVERVIEW_IMAGE_LINK_CONFIG: OverviewImageLinkConfigSize = {
  top: 0,
  left: 0,
  width: 0,
  height: 0,
};

import { BASE_MAP_IMAGES_URL } from '@/constants';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { MODAL_INITIAL_STATE_MOCK } from '@/redux/modal/modal.mock';
import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { render, screen } from '@testing-library/react';
import { OverviewImagesModal } from './OverviewImagesModal.component';

jest.mock('./utils/useOverviewImageSize', () => ({
  __esModule: true,
  useOverviewImageSize: jest.fn().mockImplementation(() => ({
    width: 200,
    height: 300,
  })),
}));

const PROJECT_DIRECTORY = 'directory';

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <OverviewImagesModal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

describe('OverviewImagesModal - component', () => {
  describe('when currentImage is NOT valid', () => {
    beforeEach(() => {
      renderComponent({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
            directory: PROJECT_DIRECTORY,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        modal: {
          ...MODAL_INITIAL_STATE_MOCK,
          overviewImagesState: {
            imageId: 0,
          },
        },
      });
    });

    it('should not render component', () => {
      const element = screen.queryByTestId('overview-images-modal');
      expect(element).toBeNull();
    });
  });

  describe('when currentImage is valid', () => {
    beforeEach(() => {
      renderComponent({
        project: {
          data: {
            ...projectFixture,
            overviewImageViews: [PROJECT_OVERVIEW_IMAGE_MOCK],
            topOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK,
            directory: PROJECT_DIRECTORY,
          },
          loading: 'succeeded',
          error: { message: '', name: '' },
          projectId: '',
        },
        modal: {
          ...MODAL_INITIAL_STATE_MOCK,
          overviewImagesState: {
            imageId: PROJECT_OVERVIEW_IMAGE_MOCK.id,
          },
        },
      });
    });

    it('should render component', () => {
      const element = screen.queryByTestId('overview-images-modal');
      expect(element).not.toBeNull();
    });

    it('should render image with valid src', () => {
      const imageElement = screen.getByAltText('overview');
      const result = `${BASE_MAP_IMAGES_URL}/map_images/${PROJECT_DIRECTORY}/${PROJECT_OVERVIEW_IMAGE_MOCK.filename}`;
      expect(imageElement.getAttribute('src')).toBe(result);
    });

    it('should render image wrapper with valid size', () => {
      const imageElement = screen.getByAltText('overview');
      const wrapperElement = imageElement.closest('div');
      const wrapperStyle = wrapperElement?.getAttribute('style');

      expect(wrapperStyle).toBe('width: 200px; height: 300px;');
    });
  });
});

import {
  OverviewImageLinkImage,
  OverviewImageLinkModel,
  OverviewImageLinkSearch,
} from '@/types/models';

export interface OverviewImageSize {
  width: number;
  height: number;
}

export interface ImageContainerSize {
  width: number;
  height: number;
}

export interface OverviewImageLinkConfigSize {
  top: number;
  left: number;
  width: number;
  height: number;
}

export interface OverviewImageLinkConfig {
  idObject: number;
  size: OverviewImageLinkConfigSize;
  onClick(): void;
}

export type OverviewImageLinkImageHandler = (link: OverviewImageLinkImage) => void;

export type OverviewImageLinkModelHandler = (link: OverviewImageLinkModel) => void;

export type OverviewImageLinkSearchHandler = (link: OverviewImageLinkSearch) => void;

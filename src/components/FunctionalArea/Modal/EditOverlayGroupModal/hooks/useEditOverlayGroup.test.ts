/* eslint-disable no-magic-numbers */
import { DEFAULT_ERROR } from '@/constants/errors';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { getReduxStoreWithActionsListener } from '@/utils/testing/getReduxStoreActionsListener';
import { renderHook } from '@testing-library/react';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { useEditOverlayGroup } from './useEditOverlayGroup';

const overlayGroup = { ...overlayGroupFixture, id: 109 };
describe('useEditOverlayGroup', () => {
  it('should handle cancel edit overlay', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleCancelEdit },
      },
    } = renderHook(() => useEditOverlayGroup(), {
      wrapper: Wrapper,
    });

    handleCancelEdit();

    const actions = store.getActions();

    expect(actions[0].type).toBe('modal/closeModal');
  });

  it('should handle handleRemoveOverlay if proper data is provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: null,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleRemoveOverlayGroup },
      },
    } = renderHook(() => useEditOverlayGroup(), {
      wrapper: Wrapper,
    });

    handleRemoveOverlayGroup();

    const actions = store.getActions();

    expect(actions[0].type).toBe('overlayGroups/removeOverlayGroup/pending');

    const { overlayGroupId } = actions[0].meta.arg;

    expect(overlayGroupId).toBe(overlayGroup.id);
  });
  it('should not handle handleRemoveOverlay if proper data is not provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'failed',
        error: DEFAULT_ERROR,
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleRemoveOverlayGroup },
      },
    } = renderHook(() => useEditOverlayGroup(), {
      wrapper: Wrapper,
    });

    handleRemoveOverlayGroup();

    const actions = store.getActions();

    expect(actions.length).toBe(0);
  });
  it('should handle handleSaveEditedOverlay if proper data is provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayGroup.name,
        modalName: 'edit-overlay-group',
        editOverlayState: null,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleSaveEditedOverlayGroup },
      },
    } = renderHook(() => useEditOverlayGroup(), {
      wrapper: Wrapper,
    });

    handleSaveEditedOverlayGroup();

    const actions = store.getActions();

    expect(actions[0].type).toBe('overlayGroups/updateOverlayGroups/pending');
    expect(actions[0].meta.arg).toEqual([overlayGroup]);
  });
  it('should not handle handleSaveEditedOverlay if proper data is not provided', () => {
    const { Wrapper, store } = getReduxStoreWithActionsListener({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: null,
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const {
      result: {
        current: { handleSaveEditedOverlayGroup },
      },
    } = renderHook(() => useEditOverlayGroup(), {
      wrapper: Wrapper,
    });

    handleSaveEditedOverlayGroup();

    const actions = store.getActions();

    expect(actions.length).toBe(0);
  });
});

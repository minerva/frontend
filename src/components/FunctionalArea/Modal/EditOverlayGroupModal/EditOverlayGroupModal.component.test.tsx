import { render, screen, fireEvent } from '@testing-library/react';
import { StoreType } from '@/redux/store';
import {
  InitialStoreState,
  getReduxWrapperWithStore,
} from '@/utils/testing/getReduxWrapperWithStore';
import { overlayFixture, overlaysPageFixture } from '@/models/fixtures/overlaysFixture';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { DEFAULT_ERROR } from '@/constants/errors';
import { act } from 'react-dom/test-utils';
import { OVERLAYS_INITIAL_STATE_MOCK } from '@/redux/overlays/overlays.mock';
import { showToast } from '@/utils/showToast';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { Modal } from '../Modal.component';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

jest.mock('../../../../utils/showToast');

const renderComponent = (initialStoreState: InitialStoreState = {}): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore(initialStoreState);

  return (
    render(
      <Wrapper>
        <Modal />
      </Wrapper>,
    ),
    {
      store,
    }
  );
};

const overlayGroup = {
  ...overlayGroupFixture,
  id: 1,
};
describe('EditOverlayModal - component', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should render modal with correct data', () => {
    renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroupFixture,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    expect(screen.getByLabelText('Name')).toBeVisible();
    expect(screen.getByLabelText('Order')).toBeVisible();
    expect(screen.getByTestId('overlay-group-name')).toHaveValue(overlayGroupFixture.name);
    expect(screen.getByTestId('overlay-group-order')).toHaveValue(overlayGroupFixture.order);
  });
  it('should handle input change correctly', () => {
    renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const overlayNameInput: HTMLInputElement = screen.getByTestId('overlay-group-name');
    const overlayOrderInput: HTMLInputElement = screen.getByTestId('overlay-group-order');

    fireEvent.change(overlayNameInput, { target: { value: 'Test name' } });
    fireEvent.change(overlayOrderInput, { target: { value: `11` } });

    expect(overlayNameInput.value).toBe('Test name');
    expect(overlayOrderInput.value).toBe('11');
  });
  it('should handle remove user overlay', async () => {
    const { store } = renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient
      .onDelete(apiPath.removeOverlayGroup(overlayGroup.id))
      .reply(HttpStatusCode.Ok, {});

    const page = {
      ...overlaysPageFixture,
      data: [],
    };

    mockedAxiosNewClient.onGet(apiPath.getOverlayGroups()).reply(HttpStatusCode.Ok, page);

    const removeButton = screen.getByTestId('remove-button');
    expect(removeButton).toBeVisible();
    await act(() => {
      removeButton.click();
    });

    const { loading } = store.getState().overlayGroups;
    expect(loading).toBe('succeeded');
    expect(removeButton).not.toBeVisible();
  });
  it('should show toast after successful removing user overlay', async () => {
    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient
      .onDelete(apiPath.removeOverlayGroup(overlayGroup.id))
      .reply(HttpStatusCode.Ok, {});

    const removeButton = screen.getByTestId('remove-button');
    expect(removeButton).toBeVisible();
    await act(() => {
      removeButton.click();
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'User overlay group removed successfully',
      type: 'success',
    });
  });
  it('should handle save edited user overlay', async () => {
    const { store } = renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayGroup.name,
        modalName: 'edit-overlay-group',
        editOverlayState: null,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient
      .onPut(apiPath.updateOverlay(overlayGroup.id))
      .reply(HttpStatusCode.Ok, overlayGroup);

    const page = {
      ...overlaysPageFixture,
      data: [overlayGroup],
    };

    mockedAxiosNewClient.onGet(apiPath.getOverlayGroups()).reply(HttpStatusCode.Ok, page);

    const saveButton = screen.getByTestId('save-button');
    expect(saveButton).toBeVisible();
    await act(() => {
      saveButton.click();
    });

    const { loading } = store.getState().overlayGroups;
    expect(loading).toBe('succeeded');
    expect(saveButton).not.toBeVisible();
  });
  it('should show toast after successful editing user overlay', async () => {
    renderComponent({
      user: {
        authenticated: true,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
        login: 'test',
        role: 'user',
        userData: null,
        token: null,
      },
      modal: {
        isOpen: true,
        modalTitle: overlayGroup.name,
        modalName: 'edit-overlay-group',
        editOverlayState: null,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
      overlays: OVERLAYS_INITIAL_STATE_MOCK,
    });
    mockedAxiosNewClient
      .onPut(apiPath.updateOverlayGroup(overlayGroup.id))
      .reply(HttpStatusCode.Ok, overlayGroup);

    const saveButton = screen.getByTestId('save-button');
    expect(saveButton).toBeVisible();
    await act(() => {
      saveButton.click();
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'User overlay group updated successfully',
      type: 'success',
    });
  });

  it('should handle cancel edit user overlay', async () => {
    const { store } = renderComponent({
      modal: {
        isOpen: true,
        modalTitle: overlayFixture.name,
        modalName: 'edit-overlay-group',
        editOverlayState: overlayFixture,
        editOverlayGroupState: overlayGroup,
        molArtState: {},
        overviewImagesState: {},
        errorReportState: {},
        layerFactoryState: { id: undefined },
        layerObjectFactoryState: undefined,
        layerLineFactoryState: undefined,
      },
    });

    const cancelButton = screen.getByTestId('cancel-button');
    expect(cancelButton).toBeVisible();
    await act(() => {
      cancelButton.click();
    });

    const { isOpen } = store.getState().modal;
    expect(isOpen).toBe(false);
    expect(cancelButton).not.toBeVisible();
  });
});

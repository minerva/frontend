/* eslint-disable no-magic-numbers */
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import { StoreType } from '@/redux/store';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { INITIAL_STORE_STATE_MOCK } from '@/redux/root/root.fixtures';
import { GLYPHS_STATE_INITIAL_MOCK } from '@/redux/glyphs/glyphs.mock';
import { apiPath } from '@/redux/apiPath';
import { HttpStatusCode } from 'axios';
import { layerImageFixture } from '@/models/fixtures/layerImageFixture';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import {
  LAYER_STATE_DEFAULT_DATA,
  LAYERS_STATE_INITIAL_LAYER_MOCK,
} from '@/redux/layers/layers.mock';
import { MODELS_DATA_MOCK_WITH_MAIN_MAP } from '@/redux/models/models.mock';
import { overlayFixture } from '@/models/fixtures/overlaysFixture';
import { showToast } from '@/utils/showToast';
import { overlayGroupFixture } from '@/models/fixtures/overlayGroupsFixture';
import { LayerImageObjectFactoryModal } from './LayerImageObjectFactoryModal.component';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const glyph = { id: 1, file: 23, filename: 'Glyph1.png' };

jest.mock('../../../../utils/showToast');

const renderComponent = (): { store: StoreType } => {
  const { Wrapper, store } = getReduxWrapperWithStore({
    ...INITIAL_STORE_STATE_MOCK,
    glyphs: {
      ...GLYPHS_STATE_INITIAL_MOCK,
      data: [glyph],
    },
    layers: {
      0: {
        ...LAYERS_STATE_INITIAL_LAYER_MOCK,
        data: {
          ...LAYER_STATE_DEFAULT_DATA,
          activeLayers: [1],
          drawLayer: 1,
        },
      },
    },
    modal: {
      isOpen: true,
      modalTitle: overlayFixture.name,
      modalName: 'edit-overlay',
      editOverlayState: overlayFixture,
      editOverlayGroupState: overlayGroupFixture,
      molArtState: {},
      overviewImagesState: {},
      errorReportState: {},
      layerFactoryState: { id: undefined },
      layerObjectFactoryState: {
        x: 1,
        y: 1,
        width: 1,
        height: 1,
      },
      layerLineFactoryState: undefined,
    },
    models: {
      ...MODELS_DATA_MOCK_WITH_MAIN_MAP,
    },
  });

  return {
    store,
    ...render(
      <Wrapper>
        <LayerImageObjectFactoryModal />
      </Wrapper>,
    ),
  };
};

describe('LayerImageObjectFactoryModal - component', () => {
  it('should render LayerImageObjectFactoryModal component with initial state', () => {
    renderComponent();

    expect(screen.getByText(/Glyph:/i)).toBeInTheDocument();
    expect(screen.getByText(/File:/i)).toBeInTheDocument();
    expect(screen.getByText(/Submit/i)).toBeInTheDocument();
    expect(screen.getByText(/No Image/i)).toBeInTheDocument();
  });

  it('should display a list of glyphs in the dropdown', async () => {
    renderComponent();

    const dropdown = screen.getByTestId('autocomplete');
    if (!dropdown.firstChild) {
      throw new Error('Dropdown does not have a firstChild');
    }
    fireEvent.keyDown(dropdown.firstChild, { key: 'ArrowDown' });
    await waitFor(() => expect(screen.getByText(glyph.filename)).toBeInTheDocument());
    fireEvent.click(screen.getByText(glyph.filename));
  });

  it('should update the selected glyph on dropdown change', async () => {
    renderComponent();

    const dropdown = screen.getByTestId('autocomplete');
    if (!dropdown.firstChild) {
      throw new Error('Dropdown does not have a firstChild');
    }
    fireEvent.keyDown(dropdown.firstChild, { key: 'ArrowDown' });
    await waitFor(() => expect(screen.getByText(glyph.filename)).toBeInTheDocument());
    fireEvent.click(screen.getByText(glyph.filename));

    await waitFor(() => {
      const imgPreview: HTMLImageElement = screen.getByTestId('layer-image-preview');
      const decodedSrc = decodeURIComponent(imgPreview.src);
      expect(decodedSrc).toContain(`glyphs/${glyph.id}/fileContent`);
    });
  });

  it('should handle form submission correctly', async () => {
    mockedAxiosNewClient
      .onPost(apiPath.addLayerImageObject(0, 1))
      .reply(HttpStatusCode.Ok, layerImageFixture);
    renderComponent();

    const submitButton = screen.getByText(/Submit/i);

    await act(async () => {
      fireEvent.click(submitButton);
    });

    expect(showToast).toHaveBeenCalledWith({
      message: 'A new image has been successfully added.',
      type: 'success',
    });
  });

  it('should display "No Image" when there is no image file', () => {
    const { store } = renderComponent();

    store.dispatch({
      type: 'glyphs/clearGlyphData',
    });

    expect(screen.getByText(/No Image/i)).toBeInTheDocument();
  });
});

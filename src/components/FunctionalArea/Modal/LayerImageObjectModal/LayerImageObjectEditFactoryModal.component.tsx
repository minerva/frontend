/* eslint-disable no-magic-numbers */
import React, { useState } from 'react';
import { useAppSelector } from '@/redux/hooks/useAppSelector';

import { mapEditToolsLayerObjectSelector } from '@/redux/mapEditTools/mapEditTools.selectors';
import { LayerImageObjectForm } from '@/components/FunctionalArea/Modal/LayerImageObjectModal/LayerImageObjectForm.component';
import { currentModelIdSelector } from '@/redux/models/models.selectors';
import { addGlyph } from '@/redux/glyphs/glyphs.thunks';
import { useAppDispatch } from '@/redux/hooks/useAppDispatch';
import { updateLayerImageObject } from '@/redux/layers/layers.thunks';
import { layerUpdateImage } from '@/redux/layers/layers.slice';
import { showToast } from '@/utils/showToast';
import { closeModal } from '@/redux/modal/modal.slice';
import { SerializedError } from '@reduxjs/toolkit';
import { useMapInstance } from '@/utils/context/mapInstanceContext';
import updateElement from '@/components/Map/MapViewer/utils/shapes/layer/utils/updateElement';
import { mapEditToolsSetLayerObject } from '@/redux/mapEditTools/mapEditTools.slice';

export const LayerImageObjectEditFactoryModal: React.FC = () => {
  const layerObject = useAppSelector(mapEditToolsLayerObjectSelector);
  const { mapInstance } = useMapInstance();

  if (!layerObject || !('glyph' in layerObject)) {
    throw new Error('Invalid layer image object');
  }
  const currentModelId = useAppSelector(currentModelIdSelector);
  const dispatch = useAppDispatch();

  const [selectedGlyph, setSelectedGlyph] = useState<number | null>(layerObject?.glyph || null);
  const [file, setFile] = useState<File | null>(null);
  const [isSending, setIsSending] = useState<boolean>(false);

  const handleSubmit = async (): Promise<void> => {
    if (!layerObject) {
      return;
    }
    setIsSending(true);

    try {
      let glyphId = selectedGlyph;
      if (file) {
        const data = await dispatch(addGlyph(file)).unwrap();
        if (!data) {
          return;
        }
        glyphId = data.id;
      }
      const layerImage = await dispatch(
        updateLayerImageObject({
          modelId: currentModelId,
          layerId: layerObject.layer,
          ...layerObject,
          glyph: glyphId,
        }),
      ).unwrap();
      if (layerImage) {
        dispatch(
          layerUpdateImage({ modelId: currentModelId, layerId: layerImage.layer, layerImage }),
        );
        dispatch(mapEditToolsSetLayerObject(layerImage));
        updateElement(mapInstance, layerImage.layer, layerImage);
      }
      showToast({
        type: 'success',
        message: 'The image has been successfully updated.',
      });
      dispatch(closeModal());
    } catch (error) {
      const typedError = error as SerializedError;
      showToast({
        type: 'error',
        message: typedError.message || 'An error occurred while editing the layer image',
      });
    } finally {
      setIsSending(false);
    }
  };

  return (
    <LayerImageObjectForm
      file={file}
      selectedGlyph={selectedGlyph}
      isSending={isSending}
      onSubmit={handleSubmit}
      setFile={setFile}
      setSelectedGlyph={setSelectedGlyph}
    />
  );
};

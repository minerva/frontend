/* eslint-disable no-magic-numbers */
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import QuestionModal from './QustionModal.component';

beforeEach(() => {
  const modalRoot = document.createElement('div');
  modalRoot.setAttribute('id', 'modal-root');
  document.body.appendChild(modalRoot);
});

afterEach(() => {
  const modalRoot = document.getElementById('modal-root');
  if (modalRoot) {
    document.body.removeChild(modalRoot);
  }
});

describe('QuestionModal', () => {
  const defaultProps = {
    isOpen: true,
    onClose: jest.fn(),
    onConfirm: jest.fn(),
    question: 'Are you sure?',
  };

  it('should not render when isOpen is false', () => {
    render(<QuestionModal {...defaultProps} isOpen={false} />);
    const modalContent = screen.queryByText(defaultProps.question);
    expect(modalContent).not.toBeInTheDocument();
  });

  it('should render the question when isOpen is true', () => {
    render(<QuestionModal {...defaultProps} />);
    expect(screen.getByText('Are you sure?')).toBeInTheDocument();
  });

  it('should call onClose when "No" button is clicked', () => {
    render(<QuestionModal {...defaultProps} />);
    const noButton = screen.getByText('No');
    fireEvent.click(noButton);
    expect(defaultProps.onClose).toHaveBeenCalledTimes(1);
  });

  it('should call onConfirm when "Yes" button is clicked', () => {
    render(<QuestionModal {...defaultProps} />);
    const yesButton = screen.getByText('Yes');
    fireEvent.click(yesButton);
    expect(defaultProps.onConfirm).toHaveBeenCalledTimes(1);
  });

  it('should render inside the modal-root portal', () => {
    render(<QuestionModal {...defaultProps} />);
    const modalRoot = document.getElementById('modal-root');
    expect(modalRoot).toContainElement(screen.getByText('Are you sure?'));
  });
});

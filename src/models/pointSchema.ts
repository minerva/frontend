import { z } from 'zod';

export const zPointSchema = z.number().nonnegative('z should be non negative').optional();

export const pointSchema = z.object({
  x: z.number().nonnegative('x should be non negative'),
  y: z.number().nonnegative('y should be non negative'),
  z: zPointSchema,
});

import { z } from 'zod';
import { ZERO } from '@/constants/common';

export const overlayGroupSchema = z.object({
  id: z.number().int().positive().nullable(),
  name: z.string(),
  order: z.number().int().gte(ZERO),
});

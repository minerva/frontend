import { z } from 'zod';

export const targetParticipantSchema = z.object({
  link: z.string().nullable(),
  type: z.string(),
  resource: z.string(),
  id: z.number(),
  annotatorClassName: z.string(),
});

import { z } from 'zod';
import { bioEntitySchema } from './bioEntitySchema';

export const bioEntityContentSchema = z.object({
  bioEntity: bioEntitySchema,
  /** indicates if bioEntity matches perfect match even if not provided in query */
  perfect: z.boolean(),
});

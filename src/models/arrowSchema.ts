import { z } from 'zod';

export const arrowSchema = z.object({
  arrowType: z.string(),
  angle: z.number(),
  lineType: z.string(),
  length: z.number(),
});

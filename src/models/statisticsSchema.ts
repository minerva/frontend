import { z } from 'zod';

export const statisticsSchema = z.object({
  elementAnnotations: z.record(z.string(), z.number()),
  publications: z.number(),
  reactionAnnotations: z.record(z.string(), z.number()),
});

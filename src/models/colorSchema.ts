import { z } from 'zod';

export const colorSchema = z.object({
  alpha: z.number(),
  rgb: z.number(),
});

import { z } from 'zod';

export const glyphSchema = z.object({
  id: z.number(),
  file: z.number(),
  filename: z.string().optional().nullable(),
});

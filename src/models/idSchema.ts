import { z } from 'zod';

export const idSchema = z.object({
  annotatorClassName: z.string(),
  id: z.number(),
  link: z.string().nullable(),
  resource: z.string(),
  type: z.string(),
});

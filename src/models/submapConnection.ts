import { z } from 'zod';
import { targetElementSchema } from './targetElementSchema';

export const submapConnection = z.object({
  from: targetElementSchema,
  to: z.object({ modelId: z.number() }),
});

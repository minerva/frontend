import { ZodNullable, ZodString } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Generator } from 'zod-fixture';

export const diseaseNameGenerator = Generator({
  schema: ZodNullable<ZodString>,
  // eslint-disable-next-line no-magic-numbers
  filter: ({ context }) => context.path.at(-1) === 'diseaseName',
  output: ({ transform }) => transform.utils.random.string({}),
});

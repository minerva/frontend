// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture, Generator } from 'zod-fixture';
import { ZOD_SEED } from '@/constants';
import { ZodNullable } from 'zod';
import { overviewImageView } from '@/models/overviewImageView';

export const topOverviewImageGenerator = Generator({
  schema: ZodNullable,
  // eslint-disable-next-line no-magic-numbers
  filter: ({ context }) => context.path.at(-1) === 'topOverviewImage',
  output: () =>
    createFixture(overviewImageView, {
      seed: ZOD_SEED,
      array: { min: 2, max: 2 },
    }),
});

// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture, Generator } from 'zod-fixture';
import { ZOD_SEED } from '@/constants';
import { ZodNullable } from 'zod';
import { organism } from '@/models/organism';

export const organismGenerator = Generator({
  schema: ZodNullable,
  // eslint-disable-next-line no-magic-numbers
  filter: ({ context }) => context.path.at(-1) === 'organism',
  output: () =>
    createFixture(organism, {
      seed: ZOD_SEED,
      array: { min: 2, max: 2 },
    }),
});

import { MODEL_IDS_MOCK } from '@/constants/mocks';
import { ZodNumber } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Generator } from 'zod-fixture';

// model in bioEntity model is the same as idObject in model model
export const modelIdGenerator = Generator({
  schema: ZodNumber,
  // eslint-disable-next-line no-magic-numbers
  filter: ({ context }) => context.path.at(-1) === 'model',
  output: ({ transform }) => transform.utils.random.from(MODEL_IDS_MOCK),
});

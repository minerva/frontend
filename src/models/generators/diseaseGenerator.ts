// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture, Generator } from 'zod-fixture';
import { ZOD_SEED } from '@/constants';
import { ZodNullable } from 'zod';
import { referenceSchema } from '@/models/referenceSchema';

export const diseaseGenerator = Generator({
  schema: ZodNullable,
  filter: ({ context }) => {
    // eslint-disable-next-line no-magic-numbers
    return context.path.at(-1) === 'disease';
  },
  output: () =>
    createFixture(referenceSchema, {
      seed: ZOD_SEED,
      array: { min: 2, max: 2 },
    }),
});

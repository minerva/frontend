import { z } from 'zod';

export const oauthSchema = z.object({
  Orcid: z.string().optional(),
});

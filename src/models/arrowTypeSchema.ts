import { z } from 'zod';
import { shapeSchema } from '@/models/shapeSchema';

export const arrowTypeSchema = z.object({
  arrowType: z.string(),
  shapes: z.array(shapeSchema),
});

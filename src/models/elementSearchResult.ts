import { z } from 'zod';

export const elementSearchResultType = z.union([z.literal('ALIAS'), z.literal('REACTION')]);

export const elementSearchResult = z.object({
  id: z.number(),
  modelId: z.number(),
  type: elementSearchResultType,
});

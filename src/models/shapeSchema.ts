import { z } from 'zod';
import { shapeEllipseSchema } from '@/models/shapeEllipseSchema';
import { shapePolygonSchema } from '@/models/shapePolygonSchema';

export const shapeSchema = z.union([shapeEllipseSchema, shapePolygonSchema]);

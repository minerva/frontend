import { z } from 'zod';

export const organism = z.object({
  id: z.number().int().positive(),
  link: z.string().nullable(),
  type: z.string(),
  resource: z.string(),
  annotatorClassName: z.string(),
});

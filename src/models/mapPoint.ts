import { z } from 'zod';

/* This schema is used only for local Point objects, it's NOT returned from backend */

export const mapPointSchema = z.object({
  x: z.number().finite().nonnegative(),
  y: z.number().finite().nonnegative(),
  z: z.number().finite().nonnegative().optional(),
});

import { z } from 'zod';

export const layerSchema = z.object({
  id: z.number(),
  name: z.string(),
  visible: z.boolean(),
  locked: z.boolean(),
  z: z.number(),
});

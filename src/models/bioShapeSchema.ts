import { z } from 'zod';
import { shapeSchema } from '@/models/shapeSchema';

export const bioShapeSchema = z.object({
  sboTerm: z.string(),
  shapes: z.array(shapeSchema),
});

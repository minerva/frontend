import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { bioShapeSchema } from '@/models/bioShapeSchema';

export const bioShapesFixture = createFixture(z.array(bioShapeSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

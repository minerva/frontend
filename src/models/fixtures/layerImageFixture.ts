import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { layerImageSchema } from '@/models/layerImageSchema';

export const layerImageFixture = createFixture(layerImageSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

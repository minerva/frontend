import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { overlayGroupSchema } from '@/models/overlayGroupSchema';

export const overlayGroupFixture = createFixture(overlayGroupSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

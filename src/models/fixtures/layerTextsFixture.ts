import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { layerTextSchema } from '@/models/layerTextSchema';
import { pageableSchema } from '@/models/pageableSchema';

export const layerTextsFixture = createFixture(pageableSchema(layerTextSchema), {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

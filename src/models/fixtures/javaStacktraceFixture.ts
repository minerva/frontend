import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { javaStacktraceSchema } from '@/models/javaStacktraceSchema';

export const javaStacktraceFixture = createFixture(javaStacktraceSchema, {
  seed: ZOD_SEED,
});

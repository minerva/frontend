import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { modelElementSchema } from '@/models/modelElementSchema';

export const modelElementFixture = createFixture(modelElementSchema, {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

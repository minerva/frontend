import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { arrowTypeSchema } from '@/models/arrowTypeSchema';

export const arrowTypesFixture = createFixture(z.array(arrowTypeSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

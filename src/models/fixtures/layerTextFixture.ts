import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { layerTextSchema } from '@/models/layerTextSchema';

export const layerTextFixture = createFixture(layerTextSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

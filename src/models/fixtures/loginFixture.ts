import { ZOD_SEED } from '@/constants';
import { loginSchema } from '@/models/loginSchema';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';

export const loginFixture = createFixture(loginSchema, {
  seed: ZOD_SEED,
});

import { ZOD_SEED } from '@/constants';
import { chemicalSchema } from '@/models/chemicalSchema';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';

export const chemicalsFixture = createFixture(z.array(chemicalSchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

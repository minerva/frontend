import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { shapeSchema } from '@/models/shapeSchema';

export const shapesFixture = createFixture(z.array(shapeSchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

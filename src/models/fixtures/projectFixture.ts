import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Fixture } from 'zod-fixture';
import { diseaseNameGenerator } from '@/models/generators/diseaseNameGenerator';
import { diseaseGenerator } from '@/models/generators/diseaseGenerator';
import { organismGenerator } from '@/models/generators/organismGenerator';
import { organismNameGenerator } from '@/models/generators/organismNameGenerator';
import { topOverviewImageGenerator } from '@/models/generators/topOverviewImageGenerator';
import { projectSchema } from '../projectSchema';

const projectFixtureGenerator = new Fixture({ seed: ZOD_SEED }).extend([
  diseaseNameGenerator,
  diseaseGenerator,
  organismNameGenerator,
  organismGenerator,
  topOverviewImageGenerator,
]);

export const projectFixture = projectFixtureGenerator.fromSchema(projectSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

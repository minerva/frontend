import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { oauthSchema } from '@/models/oauthSchema';

export const oauthFixture = createFixture(oauthSchema, {
  seed: ZOD_SEED,
});

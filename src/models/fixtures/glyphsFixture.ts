import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pageableSchema } from '@/models/pageableSchema';
import { glyphSchema } from '@/models/glyphSchema';

export const glyphsFixture = createFixture(pageableSchema(glyphSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

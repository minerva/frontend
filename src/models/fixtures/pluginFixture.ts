import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pluginSchema } from '../pluginSchema';

export const pluginFixture = createFixture(pluginSchema, {
  seed: ZOD_SEED,
});

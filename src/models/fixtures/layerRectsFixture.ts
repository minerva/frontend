import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pageableSchema } from '@/models/pageableSchema';
import { layerRectSchema } from '@/models/layerRectSchema';

export const layerRectsFixture = createFixture(pageableSchema(layerRectSchema), {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

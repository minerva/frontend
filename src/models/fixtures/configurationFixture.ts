import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { configurationSchema } from '../configurationSchema';

export const configurationFixture = createFixture(configurationSchema, {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

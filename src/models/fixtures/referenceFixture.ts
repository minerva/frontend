import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { referenceSchema } from '@/models/referenceSchema';

export const referenceFixture = createFixture(referenceSchema, {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { commentSchema } from '@/models/commentSchema';

export const commentFixture = createFixture(commentSchema, {
  seed: ZOD_SEED,
});

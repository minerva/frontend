import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { layerSchema } from '@/models/layerSchema';

export const layerFixture = createFixture(layerSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

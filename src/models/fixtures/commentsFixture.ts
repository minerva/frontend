import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { commentSchema } from '@/models/commentSchema';

export const commentsFixture = createFixture(z.array(commentSchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 10 },
});

import { ZOD_SEED } from '@/constants';
import { drugSchema } from '@/models/drugSchema';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';

export const drugsFixture = createFixture(z.array(drugSchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

export const drugFixture = createFixture(drugSchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { publicationElementSchema } from '@/models/publicationElementSchema';

export const publicationElementFixture = createFixture(publicationElementSchema, {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

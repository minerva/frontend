import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { lineTypeSchema } from '@/models/lineTypeSchema';

export const lineTypesFixture = createFixture(z.array(lineTypeSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

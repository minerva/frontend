import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pageableSchema } from '@/models/pageableSchema';
import {
  createdOverlayFileSchema,
  mapOverlaySchema,
  uploadedOverlayFileContentSchema,
} from '../mapOverlaySchema';

export const overlaysPageFixture = createFixture(pageableSchema(mapOverlaySchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

export const overlayFixture = createFixture(mapOverlaySchema, {
  seed: ZOD_SEED,
  array: { min: 1, max: 1 },
});

export const createdOverlayFileFixture = createFixture(createdOverlayFileSchema, {
  seed: ZOD_SEED,
});

export const uploadedOverlayFileContentFixture = createFixture(uploadedOverlayFileContentSchema, {
  seed: ZOD_SEED,
});

export const emptyPageFixture = createFixture(pageableSchema(mapOverlaySchema), {
  seed: ZOD_SEED,
  array: { min: 0, max: 0 },
});

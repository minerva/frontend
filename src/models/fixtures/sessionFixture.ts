import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { sessionSchemaValid } from '../sessionValidSchema';

export const sessionFixture = createFixture(sessionSchemaValid, {
  seed: ZOD_SEED,
});

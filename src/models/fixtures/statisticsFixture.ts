import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { statisticsSchema } from '../statisticsSchema';

export const statisticsFixture = createFixture(statisticsSchema, {
  seed: ZOD_SEED,
});

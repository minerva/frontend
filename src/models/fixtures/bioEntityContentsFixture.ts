import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture, Fixture } from 'zod-fixture';
import { ZOD_SEED } from '@/constants';
import { bioEntityContentSchema } from '@/models/bioEntityContentSchema';
import { modelIdGenerator } from '@/models/generators/modelIdGenerator';
import { bioEntityResponseSchema } from '../bioEntityResponseSchema';

export const bioEntityResponseFixture = createFixture(bioEntityResponseSchema, {
  seed: ZOD_SEED,
});

const bioEntityFixtureGenerator = new Fixture({ seed: ZOD_SEED }).extend([modelIdGenerator]);

export const bioEntityContentFixture = bioEntityFixtureGenerator.fromSchema(
  bioEntityContentSchema,
  {
    seed: ZOD_SEED,
    array: { min: 1, max: 1 },
  },
);

export const bioEntitiesContentFixture = bioEntityFixtureGenerator.fromSchema(
  z.array(bioEntityContentSchema),
  {
    seed: ZOD_SEED,
    array: { min: 10, max: 10 },
  },
);

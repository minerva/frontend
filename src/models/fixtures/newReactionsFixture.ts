import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pageableSchema } from '@/models/pageableSchema';
import { newReactionSchema } from '@/models/newReactionSchema';

export const newReactionsFixture = createFixture(pageableSchema(newReactionSchema), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import {
  overlayBioEntitySchema,
  overlayElementWithBioEntitySchema,
  overlayElementWithReactionSchema,
} from '../overlayBioEntitySchema';

export const overlayBioEntityFixture = createFixture(z.array(overlayBioEntitySchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

export const overlayElementWithReactionFixture = createFixture(overlayElementWithReactionSchema, {
  seed: ZOD_SEED,
});

export const overlayElementWithBioEntityFixture = createFixture(overlayElementWithBioEntitySchema, {
  seed: ZOD_SEED,
});

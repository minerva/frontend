import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { modelElementSchema } from '@/models/modelElementSchema';
import { pageableSchema } from '@/models/pageableSchema';

export const modelElementsFixture = createFixture(pageableSchema(modelElementSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

import { ZOD_SEED } from '@/constants';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { newReactionSchema } from '@/models/newReactionSchema';

export const newReactionFixture = createFixture(newReactionSchema, {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

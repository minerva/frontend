import { ZOD_SEED } from '@/constants';
import { z } from 'zod';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { elementSearchResult } from '../elementSearchResult';

export const elementSearchResultFixture = createFixture(z.array(elementSearchResult), {
  seed: ZOD_SEED,
  array: { min: 2, max: 2 },
});

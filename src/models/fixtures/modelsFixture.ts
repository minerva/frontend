import { ZOD_SEED } from '@/constants';
import { mapModelSchema } from '@/models/modelSchema';
// eslint-disable-next-line import/no-extraneous-dependencies
import { createFixture } from 'zod-fixture';
import { pageableSchema } from '@/models/pageableSchema';
import { z } from 'zod';

export const singleModelFixture = createFixture(mapModelSchema, {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

export const modelsPageFixture = createFixture(pageableSchema(mapModelSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

export const modelsFixture = createFixture(z.array(mapModelSchema), {
  seed: ZOD_SEED,
  array: { min: 3, max: 3 },
});

modelsPageFixture.content = modelsFixture;

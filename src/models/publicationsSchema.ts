import { z } from 'zod';
import { publicationElementSchema } from '@/models/publicationElementSchema';
import { articleSchema } from './articleSchema';

export const publicationSchema = z.object({
  elements: z.array(publicationElementSchema),
  article: articleSchema,
});

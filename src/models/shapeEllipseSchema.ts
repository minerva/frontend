import { z } from 'zod';
import { shapeRelAbsSchema } from '@/models/shapeRelAbsSchema';

export const shapeEllipseSchema = z.object({
  type: z.literal('ELLIPSE'),
  fill: z.boolean().nullable().optional(),
  center: shapeRelAbsSchema,
  radius: shapeRelAbsSchema,
});

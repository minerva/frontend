import { z } from 'zod';

export const submodelSchema = z.object({
  mapId: z.number(),
  type: z.string(),
});

import { z } from 'zod';

export const targetSearchNameResult = z.object({
  name: z.string(),
});

import { z } from 'zod';

export const lineTypeSchema = z.object({
  name: z.string(),
  pattern: z.array(z.number()),
});

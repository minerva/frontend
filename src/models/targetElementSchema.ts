import { z } from 'zod';

export const targetElementSchema = z.object({
  id: z.number(),
  modelId: z.number(),
  type: z.string(),
});

import { z } from 'zod';
import { shapeRelAbsBezierPointSchema } from '@/models/shapeRelAbsBezierPointSchema';
import { shapeRelAbsSchema } from '@/models/shapeRelAbsSchema';

export const shapePolygonSchema = z.object({
  type: z.literal('POLYGON'),
  fill: z.boolean().nullable().optional(),
  points: z.array(z.union([shapeRelAbsSchema, shapeRelAbsBezierPointSchema])),
});

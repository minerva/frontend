import { z } from 'zod';
import { lineSchema } from './lineSchema';

export const reactionProduct = z.object({
  id: z.number(),
  line: lineSchema,
  stoichiometry: z.number().nullable(),
  element: z.number(),
});

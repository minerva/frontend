import { z } from 'zod';

export const licenseSchema = z.object({
  id: z.number().int().positive(),
  name: z.string(),
  url: z.string(),
  content: z.string(),
});

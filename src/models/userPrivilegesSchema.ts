import { z } from 'zod';

export const userPrivilegeSchema = z.object({
  privilegeType: z.string(),
  objectId: z.string().nullable(),
});

import { z } from 'zod';

export const sessionSchemaValid = z.object({
  login: z.string(),
  token: z.string(),
});

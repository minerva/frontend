import { z } from 'zod';

export const javaStacktraceSchema = z.object({
  id: z.string(),
  content: z.string(),
  createdAt: z.string(),
});

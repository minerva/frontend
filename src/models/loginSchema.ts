import { z } from 'zod';

export const loginSchema = z.object({
  info: z.string(),
  login: z.string(),
  token: z.string(),
});

import { FIRST_ARRAY_ELEMENT } from '@/constants/common';
import { Reference } from '@/types/models';

export const REFERENCES_MOCK_ALL_VALID: Reference[] = [
  {
    link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
    article: {
      title:
        'The nutrient-responsive transcription factor TFE3 promotes autophagy, lysosomal biogenesis, and clearance of cellular debris.',
      authors: [
        'Martina JA',
        ' Diab HI',
        ' Lishu L',
        ' Jeong-A L',
        ' Patange S',
        ' Raben N',
        ' Puertollano R.',
      ],
      journal: 'Science signaling',
      year: 2014,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
      pubmedId: '24448649',
      citationCount: 321,
    },
    type: 'PUBMED',
    resource: '24448649',
    id: 154973,
    annotatorClassName: '',
  },
  {
    link: 'https://www.ncbi.nlm.nih.gov/pubmed/27299292',
    article: {
      title:
        'Transcription factor EB: from master coordinator of lysosomal pathways to candidate therapeutic target in degenerative storage diseases.',
      authors: ['Sardiello M.'],
      journal: 'Annals of the New York Academy of Sciences',
      year: 2016,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/27299292',
      pubmedId: '27299292',
      citationCount: 66,
    },
    type: 'PUBMED',
    resource: '27299292',
    id: 154974,
    annotatorClassName: '',
  },
  {
    link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
    article: {
      title:
        'The nutrient-responsive transcription factor TFE3 promotes autophagy, lysosomal biogenesis, and clearance of cellular debris.',
      authors: [
        'Martina JA',
        ' Diab HI',
        ' Lishu L',
        ' Jeong-A L',
        ' Patange S',
        ' Raben N',
        ' Puertollano R.',
      ],
      journal: 'Science signaling',
      year: 2014,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
      pubmedId: '24448649',
      citationCount: 321,
    },
    type: 'PUBMED',
    resource: '24448649',
    id: 154973,
    annotatorClassName: 'source1',
  },
  {
    link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
    article: {
      title:
        'Transcription factor EB: from master coordinator of lysosomal pathways to candidate therapeutic target in degenerative storage diseases.',
      authors: ['Sardiello M.'],
      journal: 'Annals of the New York Academy of Sciences',
      year: 2016,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/27299292',
      pubmedId: '27299292',
      citationCount: 66,
    },
    type: 'PUBMED',
    resource: '27299292',
    id: 154974,
    annotatorClassName: 'source2',
  },
];

export const REFERENCES_MOCK_ALL_INVALID: Reference[] = [
  {
    link: null,
    article: {
      title:
        'The nutrient-responsive transcription factor TFE3 promotes autophagy, lysosomal biogenesis, and clearance of cellular debris.',
      authors: [
        'Martina JA',
        ' Diab HI',
        ' Lishu L',
        ' Jeong-A L',
        ' Patange S',
        ' Raben N',
        ' Puertollano R.',
      ],
      journal: 'Science signaling',
      year: 2014,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
      pubmedId: '24448649',
      citationCount: 321,
    },
    type: 'PUBMED',
    resource: '24448649',
    id: 154973,
    annotatorClassName: '',
  },
  {
    link: null,
    article: {
      title:
        'Transcription factor EB: from master coordinator of lysosomal pathways to candidate therapeutic target in degenerative storage diseases.',
      authors: ['Sardiello M.'],
      journal: 'Annals of the New York Academy of Sciences',
      year: 2016,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/27299292',
      pubmedId: '27299292',
      citationCount: 66,
    },
    type: 'PUBMED',
    resource: '27299292',
    id: 154974,
    annotatorClassName: '',
  },
  {
    link: null,
    article: {
      title:
        'The nutrient-responsive transcription factor TFE3 promotes autophagy, lysosomal biogenesis, and clearance of cellular debris.',
      authors: [
        'Martina JA',
        ' Diab HI',
        ' Lishu L',
        ' Jeong-A L',
        ' Patange S',
        ' Raben N',
        ' Puertollano R.',
      ],
      journal: 'Science signaling',
      year: 2014,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/24448649',
      pubmedId: '24448649',
      citationCount: 321,
    },
    type: 'PUBMED',
    resource: '24448649',
    id: 154973,
    annotatorClassName: 'source1',
  },
  {
    link: null,
    article: {
      title:
        'Transcription factor EB: from master coordinator of lysosomal pathways to candidate therapeutic target in degenerative storage diseases.',
      authors: ['Sardiello M.'],
      journal: 'Annals of the New York Academy of Sciences',
      year: 2016,
      link: 'https://www.ncbi.nlm.nih.gov/pubmed/27299292',
      pubmedId: '27299292',
      citationCount: 66,
    },
    type: 'PUBMED',
    resource: '27299292',
    id: 154974,
    annotatorClassName: 'source2',
  },
];

export const SINGLE_VALID_REFERENCE = REFERENCES_MOCK_ALL_VALID[FIRST_ARRAY_ELEMENT];

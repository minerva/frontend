import { MinervaPlugin } from '@/types/models';

export const PLUGINS_MOCK: MinervaPlugin[] = [
  {
    hash: '5e3fcb59588cc311ef9839feea6382eb',
    name: 'Disease-variant associations',
    version: '1.0.0',
    isPublic: false,
    isDefault: false,
    urls: ['https://minerva-service.lcsb.uni.lu/plugins/disease-associations/plugin.js'],
  },
  {
    hash: '20df86476c311824bbfe73d1034af89e',
    name: 'GSEA',
    version: '0.9.2',
    isPublic: false,
    isDefault: false,
    urls: ['https://minerva-service.lcsb.uni.lu/plugins/gsea/plugin.js'],
  },
  {
    hash: '5314b9f996e56e67f0dad65e7df8b73b',
    name: 'PD map guide',
    version: '1.0.2',
    isPublic: true,
    isDefault: false,
    urls: ['https://minerva-service.lcsb.uni.lu/plugins/guide/plugin.js'],
  },
  {
    hash: 'b85ae2f4cd67736489b5fd2b635b1013',
    name: 'Map exploation',
    version: '1.0.0',
    isPublic: true,
    isDefault: false,
    urls: ['https://minerva-service.lcsb.uni.lu/plugins/exploration/plugin.js'],
  },
  {
    hash: '77c32edf387652dfaad8a20f2a0ce76b',
    name: 'Drug reactions',
    version: '1.0.0',
    isPublic: true,
    isDefault: false,
    urls: ['https://minerva-service.lcsb.uni.lu/plugins/drug-reactions/plugin.js'],
  },
];

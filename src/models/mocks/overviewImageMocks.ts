import { PROJECT_OVERVIEW_IMAGE_MOCK } from '@/redux/project/project.mock';
import { OverviewImageLinkImage, OverviewImageLinkModel } from '@/types/models';
import { MAIN_MAP_ID } from '@/constants/mocks';

export const OVERVIEW_LINK_IMAGE_MOCK: OverviewImageLinkImage = {
  id: 1,
  polygon: [],
  linkedOverviewImage: PROJECT_OVERVIEW_IMAGE_MOCK.id,
  // type: 'OverviewImageLink',
};

export const OVERVIEW_LINK_MODEL_MOCK: OverviewImageLinkModel = {
  id: 1,
  polygon: [],
  zoomLevel: 5,
  xCoord: 15570.0,
  yCoord: 3016.0,
  linkedModel: MAIN_MAP_ID,
  // type: 'OverviewImageLink',
};

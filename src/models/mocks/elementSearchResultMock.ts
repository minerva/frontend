import { ElementSearchResult } from '@/types/models';

export const ELEMENT_SEARCH_RESULT_MOCK_ALIAS: ElementSearchResult = {
  id: 4,
  modelId: 1000,
  type: 'ALIAS',
};

export const ELEMENT_SEARCH_RESULT_MOCK_REACTION: ElementSearchResult = {
  id: 5,
  modelId: 1000,
  type: 'REACTION',
};

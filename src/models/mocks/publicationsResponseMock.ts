import { FilteredPageOf, Publication } from '@/types/models';
import { publicationElementFixture } from '@/models/fixtures/publicationElementFixture';

export const PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK: FilteredPageOf<Publication> = {
  content: [
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 19519,
          model: 52,
        },
      ],
      article: {
        title: 'The glutamate receptor ion channels.',
        authors: ['Dingledine R', ' Borges K', ' Bowie D', ' Traynelis SF.'],
        journal: 'Pharmacological reviews',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10049997',
        pubmedId: '10049997',
        citationCount: 2458,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 16167,
          model: 61,
        },
      ],
      article: {
        title: 'Regulation of JNK signaling by GSTp.',
        authors: [
          'Adler V',
          ' Yin Z',
          ' Fuchs SY',
          ' Benezra M',
          ' Rosario L',
          ' Tew KD',
          ' Pincus MR',
          ' Sardana M',
          ' Henderson CJ',
          ' Wolf CR',
          ' Davis RJ',
          ' Ronai Z.',
        ],
        journal: 'The EMBO journal',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10064598',
        pubmedId: '10064598',
        citationCount: 656,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 17823,
          model: 52,
        },
        {
          ...publicationElementFixture,
          id: 19461,
          model: 52,
        },
      ],
      article: {
        title:
          'Generic signals and specific outcomes: signaling through Ca2+, calcineurin, and NF-AT.',
        authors: ['Crabtree GR.'],
        journal: 'Cell',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10089876',
        pubmedId: '10089876',
        citationCount: 454,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 18189,
          model: 52,
        },
        {
          ...publicationElementFixture,
          id: 18729,
          model: 52,
        },
      ],
      article: {
        title: 'G protein regulation of adenylate cyclase.',
        authors: ['Simonds WF.'],
        journal: 'Trends in pharmacological sciences',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10101967',
        pubmedId: '10101967',
        citationCount: 139,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 16077,
          model: 58,
        },
        {
          ...publicationElementFixture,
          id: 16135,
          model: 58,
        },
      ],
      article: {
        title:
          'Akt promotes cell survival by phosphorylating and inhibiting a Forkhead transcription factor.',
        authors: [
          'Brunet A',
          ' Bonni A',
          ' Zigmond MJ',
          ' Lin MZ',
          ' Juo P',
          ' Hu LS',
          ' Anderson MJ',
          ' Arden KC',
          ' Blenis J',
          ' Greenberg ME.',
        ],
        journal: 'Cell',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10102273',
        pubmedId: '10102273',
        citationCount: 4019,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 15955,
          model: 55,
        },
      ],
      article: {
        title: 'Ca2+-induced apoptosis through calcineurin dephosphorylation of BAD.',
        authors: [
          'Wang HG',
          ' Pathan N',
          ' Ethell IM',
          ' Krajewski S',
          ' Yamaguchi Y',
          ' Shibasaki F',
          ' McKeon F',
          ' Bobo T',
          ' Franke TF',
          ' Reed JC.',
        ],
        journal: 'Science (New York, N.Y.)',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10195903',
        pubmedId: '10195903',
        citationCount: 708,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 15937,
          model: 55,
        },
        {
          ...publicationElementFixture,
          id: 15955,
          model: 55,
        },
      ],
      article: {
        title:
          'The proapoptotic activity of the Bcl-2 family member Bim is regulated by interaction with the dynein motor complex.',
        authors: ['Puthalakath H', ' Huang DC', " O'Reilly LA", ' King SM', ' Strasser A.'],
        journal: 'Molecular cell',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10198631',
        pubmedId: '10198631',
        citationCount: 662,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 15948,
          model: 55,
        },
      ],
      article: {
        title:
          'An APAF-1.cytochrome c multimeric complex is a functional apoptosome that activates procaspase-9.',
        authors: ['Zou H', ' Li Y', ' Liu X', ' Wang X.'],
        journal: 'The Journal of biological chemistry',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10206961',
        pubmedId: '10206961',
        citationCount: 1162,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 16286,
          model: 62,
        },
      ],
      article: {
        title:
          'Biochemical characterization and crystal structure determination of human heart short chain L-3-hydroxyacyl-CoA dehydrogenase provide insights into catalytic mechanism.',
        authors: [
          'Barycki JJ',
          " O'Brien LK",
          ' Bratt JM',
          ' Zhang R',
          ' Sanishvili R',
          ' Strauss AW',
          ' Banaszak LJ.',
        ],
        journal: 'Biochemistry',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10231530',
        pubmedId: '10231530',
        citationCount: 56,
      },
    },
    {
      elements: [
        {
          ...publicationElementFixture,
          id: 17780,
          model: 52,
        },
        {
          ...publicationElementFixture,
          id: 17937,
          model: 52,
        },
      ],
      article: {
        title: 'The Ca-calmodulin-dependent protein kinase cascade.',
        authors: ['Soderling TR.'],
        journal: 'Trends in biochemical sciences',
        year: 1999,
        link: 'https://www.ncbi.nlm.nih.gov/pubmed/10366852',
        pubmedId: '10366852',
        citationCount: 322,
      },
    },
  ],
  totalElements: 159,
  filteredSize: 1586,
  size: 10,
  number: 0,
  numberOfElements: 10,
  totalPages: 16,
};

import { ConfigurationFormatSchema } from '@/types/models';

export const CONFIGURATION_FORMATS_TYPES_MOCK: string[] = [
  'CellDesigner SBML',
  'SBGN-ML',
  'SBML',
  'GPML',
];

export const CONFIGURATION_FORMATS_MOCK: ConfigurationFormatSchema[] = [
  {
    name: 'CellDesigner SBML',
    handler: 'lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser',
    extension: 'xml',
  },
  {
    name: 'SBGN-ML',
    handler: 'lcsb.mapviewer.converter.model.sbgnml.SbgnmlXmlConverter',
    extension: 'sbgn',
  },
  {
    name: 'SBML',
    handler: 'lcsb.mapviewer.converter.model.sbml.SbmlParser',
    extension: 'xml',
  },
  {
    name: 'GPML',
    handler: 'lcsb.mapviewer.wikipathway.GpmlParser',
    extension: 'gpml',
  },
];

export const CONFIGURATION_IMAGE_FORMATS_TYPES_MOCK: string[] = ['PNG image', 'PDF', 'SVG image'];

export const CONFIGURATION_IMAGE_FORMATS_MOCK: ConfigurationFormatSchema[] = [
  {
    name: 'PNG image',
    handler: 'lcsb.mapviewer.converter.graphics.PngImageGenerator',
    extension: 'png',
  },
  {
    name: 'PDF',
    handler: 'lcsb.mapviewer.converter.graphics.PdfImageGenerator',
    extension: 'pdf',
  },
  {
    name: 'SVG image',
    handler: 'lcsb.mapviewer.converter.graphics.SvgImageGenerator',
    extension: 'svg',
  },
];

import { DEFAULT_ERROR } from '@/constants/errors';
import { OverviewImageView } from '@/types/models';
import { MAIN_MAP_ID } from '@/constants/mocks';
import { ProjectState } from './project.types';

export const PROJECT_STATE_INITIAL_MOCK: ProjectState = {
  data: undefined,
  loading: 'idle',
  error: DEFAULT_ERROR,
  projectId: '',
};

export const PROJECT_OVERVIEW_IMAGE_MOCK: NonNullable<OverviewImageView> = {
  id: 440,
  filename: '9d4911bdeeea752f076e57a91d9b1f45/biolayout_main_root.png',
  width: 5776,
  height: 4040,
  links: [
    {
      id: 2062,
      polygon: [
        {
          x: 515,
          y: 2187,
        },
        {
          x: 1073,
          y: 2187,
        },
        {
          x: 1073,
          y: 2520,
        },
        {
          x: 515,
          y: 2520,
        },
      ],
      zoomLevel: 4,
      xCoord: 3473,
      yCoord: 5871,
      linkedModel: MAIN_MAP_ID,
      // type: 'OverviewModelLink',
    },
    {
      id: 2063,
      polygon: [
        {
          x: 2410,
          y: 1360,
        },
        {
          x: 2692,
          y: 1360,
        },
        {
          x: 2692,
          y: 1570,
        },
        {
          x: 2410,
          y: 1570,
        },
      ],
      linkedOverviewImage: 435,
      // type: 'OverviewImageLink',
    },
    {
      id: 2064,
      polygon: [
        {
          x: 2830,
          y: 497,
        },
        {
          x: 3256,
          y: 497,
        },
        {
          x: 3256,
          y: 832,
        },
        {
          x: 2830,
          y: 832,
        },
      ],
      zoomLevel: 5,
      xCoord: 8081,
      yCoord: 1240,
      linkedModel: MAIN_MAP_ID,
      // type: 'OverviewModelLink',
    },
    {
      id: 2065,
      polygon: [
        {
          x: 3232,
          y: 2259,
        },
        {
          x: 3520,
          y: 2259,
        },
        {
          x: 3520,
          y: 2456,
        },
        {
          x: 3232,
          y: 2456,
        },
      ],
      linkedOverviewImage: 433,
      // type: 'OverviewImageLink',
    },
    {
      id: 2066,
      polygon: [
        {
          x: 4205,
          y: 761,
        },
        {
          x: 4625,
          y: 761,
        },
        {
          x: 4625,
          y: 1102,
        },
        {
          x: 4205,
          y: 1102,
        },
      ],
      zoomLevel: 5,
      xCoord: 7488,
      yCoord: 11986,
      linkedModel: MAIN_MAP_ID,
      // type: 'OverviewModelLink',
    },
    {
      id: 2067,
      polygon: [
        {
          x: 4960,
          y: 1971,
        },
        {
          x: 5241,
          y: 1971,
        },
        {
          x: 5241,
          y: 2163,
        },
        {
          x: 4960,
          y: 2163,
        },
      ],
      linkedOverviewImage: 434,
      // type: 'OverviewImageLink',
    },
  ],
};

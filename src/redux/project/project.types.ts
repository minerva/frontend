import { Loading } from '@/types/loadingState';
import { Project } from '@/types/models';
import { QueryData } from '@/types/query';

export type ProjectState = {
  projectId: string;
  data: Project | undefined;
  loading: Loading;
  error: Error;
};

export type SetProjectIdParams = { queryData: QueryData };

import { PROJECT_ID } from '@/constants';
import { projectFixture } from '@/models/fixtures/projectFixture';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { apiPath } from '../apiPath';
import projectReducer from './project.slice';
import { getProjectById } from './project.thunks';
import { ProjectState } from './project.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: ProjectState = {
  data: undefined,
  loading: 'idle',
  error: { name: '', message: '' },
  projectId: '',
};

describe('project reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ProjectState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('project', projectReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(projectReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after succesfull getProjectById query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getProjectById(PROJECT_ID))
      .reply(HttpStatusCode.Ok, projectFixture);

    const { type } = await store.dispatch(getProjectById(PROJECT_ID));
    const { data, loading, error } = store.getState().project;

    expect(type).toBe('project/getProjectById/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(projectFixture);
  });

  it('should update store after failed getProjectById query', async () => {
    mockedAxiosClient.onGet(apiPath.getProjectById(PROJECT_ID)).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getProjectById(PROJECT_ID));
    const { data, loading, error } = store.getState().project;

    expect(action.type).toBe('project/getProjectById/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch project by id: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(undefined);
  });

  it('should update store on loading getProjectById query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getProjectById(PROJECT_ID))
      .reply(HttpStatusCode.Ok, projectFixture);

    const actionPromise = store.dispatch(getProjectById(PROJECT_ID));

    const { data, loading } = store.getState().project;
    expect(data).toEqual(undefined);
    expect(loading).toEqual('pending');

    actionPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } = store.getState().project;

      expect(dataPromiseFulfilled).toEqual(projectFixture);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

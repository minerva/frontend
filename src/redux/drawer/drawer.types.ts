import type { DrawerName } from '@/types/drawerName';
import { KeyedFetchDataState } from '@/types/fetchDataState';
import { BioEntityContent, Chemical, Drug } from '@/types/models';
import { PayloadAction } from '@reduxjs/toolkit';
import { SearchModelElementDataState } from '@/redux/modelElements/modelElements.types';

export type SearchDrawerState = {
  currentStep: number;
  stepType: 'modelElement' | 'drugs' | 'chemicals' | 'none';
  selectedValue: BioEntityContent | Drug | Chemical | undefined;
  listOfBioEnitites: Array<SearchModelElementDataState>;
  selectedSearchElement: string;
};

export type OverlayDrawerState = {
  currentStep: number;
};

export type ReactionDrawerState = {
  reactionId?: number;
};

export type BioEntityDrawerState = {
  bioentityId?: number | string;
  drugs: KeyedFetchDataState<Drug[], []>;
  chemicals: KeyedFetchDataState<Chemical[], []>;
};

export type CommentDrawerState = {
  commentId?: number;
};

export type DrawerState = {
  isOpen: boolean;
  drawerName: DrawerName;
  searchDrawerState: SearchDrawerState;
  reactionDrawerState: ReactionDrawerState;
  bioEntityDrawerState: BioEntityDrawerState;
  overlayDrawerState: OverlayDrawerState;
  commentDrawerState: CommentDrawerState;
};

export type OpenSearchDrawerWithSelectedTabReducerPayload = string;
export type OpenSearchDrawerWithSelectedTabReducerAction =
  PayloadAction<OpenSearchDrawerWithSelectedTabReducerPayload>;

export type OpenReactionDrawerByIdPayload = number;
export type OpenReactionDrawerByIdAction = PayloadAction<OpenReactionDrawerByIdPayload>;

export type OpenBioEntityDrawerByIdPayload = number | string;
export type OpenBioEntityDrawerByIdAction = PayloadAction<OpenBioEntityDrawerByIdPayload>;

export type OpenCommentDrawerByIdPayload = number;
export type OpenCommentDrawerByIdAction = PayloadAction<OpenCommentDrawerByIdPayload>;

import { drugFixture } from '@/models/fixtures/drugFixtures';
import * as toolkitRaw from '@reduxjs/toolkit';
import { AnyAction } from '@reduxjs/toolkit';
import type { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { DRAWER_INITIAL_STATE } from './drawer.constants';
import drawerReducer, {
  closeDrawer,
  displayChemicalsList,
  displayDrugsList,
  displayEntityDetails,
  displayGroupedSearchResults,
  openDrawer,
  openSearchDrawerWithSelectedTab,
  openSubmapsDrawer,
  selectTab,
} from './drawer.slice';
import type { DrawerState } from './drawer.types';

const STEP = {
  FIRST: 1,
  SECOND: 2,
  THIRD: 3,
};

type SliceReducerType = ToolkitStore<
  {
    drawer: DrawerState;
  },
  AnyAction
>;

const createStoreInstanceUsingSliceReducer = (): SliceReducerType =>
  toolkitRaw.configureStore({
    reducer: {
      drawer: drawerReducer,
    },
  });

describe('drawer reducer', () => {
  let store = {} as SliceReducerType;

  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer();
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(drawerReducer(undefined, action)).toEqual(DRAWER_INITIAL_STATE);
  });

  it('should update the store after openDrawer action', async () => {
    const { type } = await store.dispatch(openDrawer('project-info'));
    const { isOpen, drawerName } = store.getState().drawer;

    expect(type).toBe('drawer/openDrawer');
    expect(isOpen).toBe(true);
    expect(drawerName).toEqual('project-info');
  });

  it('should update the store after openSearchDrawerWithSelectedTab action', async () => {
    const { type } = await store.dispatch(openSearchDrawerWithSelectedTab('nadh'));
    const {
      isOpen,
      drawerName,
      searchDrawerState: { currentStep, selectedSearchElement },
    } = store.getState().drawer;

    expect(type).toBe('drawer/openSearchDrawerWithSelectedTab');
    expect(isOpen).toBe(true);
    expect(drawerName).toEqual('search');
    expect(selectedSearchElement).toBe('nadh');
    expect(currentStep).toEqual(STEP.FIRST);
  });

  it('should update the store after openSubmapsDrawer action', async () => {
    const { type } = await store.dispatch(openSubmapsDrawer());
    const { isOpen, drawerName } = store.getState().drawer;

    expect(type).toBe('drawer/openSubmapsDrawer');
    expect(isOpen).toBe(true);
    expect(drawerName).toEqual('submaps');
  });

  it('should update the store after selectTab action', async () => {
    const { type } = await store.dispatch(selectTab('nadh'));
    const {
      searchDrawerState: { selectedSearchElement },
    } = store.getState().drawer;

    expect(type).toBe('drawer/selectTab');
    expect(selectedSearchElement).toBe('nadh');
  });

  it('should update the store after closeDrawerReducer action', async () => {
    const { type } = await store.dispatch(closeDrawer());
    const {
      isOpen,
      drawerName,
      searchDrawerState: { currentStep, selectedValue, stepType },
    } = store.getState().drawer;

    expect(isOpen).toBe(false);
    expect(drawerName).toBe('none');
    expect(currentStep).toBe(STEP.FIRST);
    expect(selectedValue).toEqual(undefined);
    expect(stepType).toEqual('none');
    expect(type).toBe('drawer/closeDrawer');
  });

  it('should update the store after displayDrugsList action', async () => {
    const { type } = await store.dispatch(displayDrugsList());
    const {
      drawerName,
      searchDrawerState: { currentStep, stepType },
    } = store.getState().drawer;

    expect(type).toBe('drawer/displayDrugsList');
    expect(drawerName).toBe('search');
    expect(currentStep).toBe(STEP.SECOND);
    expect(stepType).toEqual('drugs');
  });

  it('should update the store after displayChemicalsList action', async () => {
    const { type } = await store.dispatch(displayChemicalsList());
    const {
      drawerName,
      searchDrawerState: { currentStep, stepType },
    } = store.getState().drawer;

    expect(type).toBe('drawer/displayChemicalsList');
    expect(drawerName).toBe('search');
    expect(currentStep).toBe(STEP.SECOND);
    expect(stepType).toEqual('chemicals');
  });

  it('should update the store after displayGroupedSearchResults action', async () => {
    const { type } = await store.dispatch(displayGroupedSearchResults());
    const {
      searchDrawerState: { currentStep, stepType },
    } = store.getState().drawer;

    expect(type).toBe('drawer/displayGroupedSearchResults');
    expect(currentStep).toBe(STEP.FIRST);
    expect(stepType).toEqual('none');
  });

  it('should update the store after displayEntityDetails action', async () => {
    const { type } = await store.dispatch(displayEntityDetails(drugFixture));
    const {
      searchDrawerState: { currentStep, selectedValue },
    } = store.getState().drawer;

    expect(type).toBe('drawer/displayEntityDetails');
    expect(currentStep).toBe(STEP.THIRD);
    expect(selectedValue).toEqual(drugFixture);
  });
});

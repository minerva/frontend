import { DrawerState } from './drawer.types';

export const initialStateFixture: DrawerState = {
  isOpen: false,
  drawerName: 'none',
  searchDrawerState: {
    currentStep: 0,
    stepType: 'none',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const openedDrawerSubmapsFixture: DrawerState = {
  isOpen: true,
  drawerName: 'submaps',
  searchDrawerState: {
    currentStep: 0,
    stepType: 'none',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const drawerSearchStepOneFixture: DrawerState = {
  isOpen: true,
  drawerName: 'search',
  searchDrawerState: {
    currentStep: 1,
    stepType: 'none',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const drawerSearchDrugsStepTwoFixture: DrawerState = {
  isOpen: true,
  drawerName: 'search',
  searchDrawerState: {
    currentStep: 2,
    stepType: 'drugs',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const drawerSearchChemicalsStepTwoFixture: DrawerState = {
  isOpen: true,
  drawerName: 'search',
  searchDrawerState: {
    currentStep: 2,
    stepType: 'chemicals',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const drawerOverlaysStepOneFixture: DrawerState = {
  isOpen: true,
  drawerName: 'overlays',
  searchDrawerState: {
    currentStep: 0,
    stepType: 'none',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 2,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

export const openedExportDrawerFixture: DrawerState = {
  isOpen: true,
  drawerName: 'export',
  searchDrawerState: {
    currentStep: 0,
    stepType: 'none',
    selectedValue: undefined,
    listOfBioEnitites: [],
    selectedSearchElement: '',
  },
  reactionDrawerState: {},
  bioEntityDrawerState: {
    drugs: {},
    chemicals: {},
  },
  overlayDrawerState: {
    currentStep: 0,
  },
  commentDrawerState: {
    commentId: undefined,
  },
};

import { EntityNumberState } from './entityNumber.types';

export const ENTITY_NUMBER_INITIAL_STATE_MOCK: EntityNumberState = {
  data: {},
};

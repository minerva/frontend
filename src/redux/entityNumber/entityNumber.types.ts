import { PayloadAction } from '@reduxjs/toolkit';

export type EntityNumberId = string;
export type EntityNumberValue = number;
export type EntityNumber = Record<EntityNumberId, EntityNumberValue>;

export interface EntityNumberState {
  data: EntityNumber;
}

export type AddNumbersToEntityNumberDataPayload = EntityNumberId[];
export type AddNumbersToEntityNumberDataAction = PayloadAction<AddNumbersToEntityNumberDataPayload>;

import { ModalName } from '@/types/modal';
import { PayloadAction } from '@reduxjs/toolkit';
import { ErrorData } from '@/utils/error-report/ErrorData';
import { BoundingBox } from '@/components/Map/MapViewer/MapViewer.types';
import { Segment } from '@/types/models';
import {
  ModalState,
  OpenEditOverlayGroupModalAction,
  OpenEditOverlayModalAction,
} from './modal.types';

const getOpenedModel = (state: ModalState): ModalName | null => {
  if (state.isOpen) {
    return state.modalName;
  }
  return null;
};

export const openModalReducer = (state: ModalState, action: PayloadAction<ModalName>): void => {
  state.isOpen = true;
  state.modalName = action.payload;
};

export const closeModalReducer = (state: ModalState): void => {
  state.isOpen = false;
  state.modalName = 'none';
};

export const openOverviewImagesModalByIdReducer = (
  state: ModalState,
  action: PayloadAction<number>,
): void => {
  state.isOpen = true;
  state.modalName = 'overview-images';
  state.modalTitle = 'Overview images';
  state.overviewImagesState = {
    imageId: action.payload,
  };
};

export const openMolArtModalByIdReducer = (
  state: ModalState,
  action: PayloadAction<string | undefined>,
): void => {
  state.isOpen = true;
  state.modalName = 'mol-art';
  state.modalTitle = 'MolArt';
  state.molArtState = {
    uniprotId: action.payload,
  };
};

export const openLoginModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'login';
  state.modalTitle = 'You need to login';
};

export const openAddCommentModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'add-comment';
  state.modalTitle = 'Add comment';
};

export const openLoggedInMenuModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'logged-in-menu';
  state.modalTitle = 'Select';
};

export const openErrorReportModalReducer = (
  state: ModalState,
  action: PayloadAction<ErrorData | undefined>,
): void => {
  state.isOpen = true;
  state.modalName = 'error-report';
  state.modalTitle = 'An error occurred!';
  state.errorReportState = {
    errorData: action.payload,
  };
};

export const openAccessDeniedModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'access-denied';
  state.modalTitle = 'Access denied!';
};

export const openSelectProjectModalReducer = (state: ModalState): void => {
  if (getOpenedModel(state) !== 'terms-of-service') {
    state.isOpen = true;
    state.modalName = 'select-project';
    state.modalTitle = 'Select project!';
  }
};

export const setOverviewImageIdReducer = (
  state: ModalState,
  action: PayloadAction<number>,
): void => {
  state.overviewImagesState = {
    imageId: action.payload,
  };
};

export const openPublicationsModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'publications';
  state.modalTitle = 'Publications';
};

export const openEditOverlayModalReducer = (
  state: ModalState,
  action: OpenEditOverlayModalAction,
): void => {
  state.isOpen = true;
  state.modalName = 'edit-overlay';
  state.modalTitle = action.payload.name;
  state.editOverlayState = action.payload;
};

export const openEditOverlayGroupModalReducer = (
  state: ModalState,
  action: OpenEditOverlayGroupModalAction,
): void => {
  state.isOpen = true;
  state.modalName = 'edit-overlay-group';
  state.modalTitle = action.payload.name;
  state.editOverlayGroupState = action.payload;
};

export const openLicenseModalReducer = (state: ModalState, action: PayloadAction<string>): void => {
  state.isOpen = true;
  state.modalName = 'license';
  state.modalTitle = `License: ${action.payload}`;
};

export const openToSModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'terms-of-service';
  state.modalTitle = 'Terms of service!';
};

export const openLayerFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<number | undefined>,
): void => {
  state.layerFactoryState = { id: action.payload };
  state.isOpen = true;
  state.modalName = 'layer-factory';
  if (action.payload) {
    state.modalTitle = 'Edit layer';
  } else {
    state.modalTitle = 'Add new layer';
  }
};

export const openLayerImageObjectFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<BoundingBox>,
): void => {
  state.layerObjectFactoryState = action.payload;
  state.isOpen = true;
  state.modalName = 'layer-image-object-factory';
  state.modalTitle = 'Select glyph or upload file';
};

export const openLayerImageObjectEditFactoryModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'layer-image-object-edit-factory';
  state.modalTitle = 'Edit image';
};

export const openLayerTextFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<BoundingBox>,
): void => {
  state.layerObjectFactoryState = action.payload;
  state.modalName = 'layer-text-factory';
  state.modalTitle = 'Add text';
  state.isOpen = true;
};

export const openLayerTextEditFactoryModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'layer-text-edit-factory';
  state.modalTitle = 'Edit text';
};

export const openLayerRectFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<BoundingBox>,
): void => {
  state.layerObjectFactoryState = action.payload;
  state.modalName = 'layer-rect-factory';
  state.modalTitle = 'Add rectangle';
  state.isOpen = true;
};

export const openLayerRectEditFactoryModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'layer-rect-edit-factory';
  state.modalTitle = 'Edit rectangle';
};

export const openLayerOvalFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<BoundingBox>,
): void => {
  state.layerObjectFactoryState = action.payload;
  state.modalName = 'layer-oval-factory';
  state.modalTitle = 'Add oval';
  state.isOpen = true;
};

export const openLayerOvalEditFactoryModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'layer-oval-edit-factory';
  state.modalTitle = 'Edit oval';
};

export const openLayerLineFactoryModalReducer = (
  state: ModalState,
  action: PayloadAction<Array<Segment>>,
): void => {
  state.layerLineFactoryState = action.payload;
  state.modalName = 'layer-line-factory';
  state.modalTitle = 'Add line';
  state.isOpen = true;
};

export const openLayerLineEditFactoryModalReducer = (state: ModalState): void => {
  state.isOpen = true;
  state.modalName = 'layer-line-edit-factory';
  state.modalTitle = 'Edit line';
};

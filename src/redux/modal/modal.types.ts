import { ModalName } from '@/types/modal';
import { MapOverlay, OverlayGroup, Segment } from '@/types/models';
import { PayloadAction } from '@reduxjs/toolkit';
import { ErrorData } from '@/utils/error-report/ErrorData';
import { BoundingBox } from '@/components/Map/MapViewer/MapViewer.types';

export type OverviewImagesModalState = {
  imageId?: number;
};

export type MolArtModalState = {
  uniprotId?: string | undefined;
};

export type ErrorRepostState = {
  errorData?: ErrorData | undefined;
};

export type EditOverlayState = MapOverlay | null;
export type EditOverlayGroupState = OverlayGroup | null;

export type LayerFactoryState = {
  id: number | undefined;
};

export type LayerObjectFactoryBoundingBoxState = BoundingBox | undefined;

export type LayerLineFactoryState = Array<Segment> | undefined;

export interface ModalState {
  isOpen: boolean;
  modalName: ModalName;
  modalTitle: string;
  overviewImagesState: OverviewImagesModalState;
  molArtState: MolArtModalState;
  errorReportState: ErrorRepostState;
  editOverlayState: EditOverlayState;
  editOverlayGroupState: EditOverlayGroupState;
  layerFactoryState: LayerFactoryState;
  layerObjectFactoryState: LayerObjectFactoryBoundingBoxState;
  layerLineFactoryState: LayerLineFactoryState;
}

export type OpenEditOverlayModalPayload = MapOverlay;

export type OpenEditOverlayModalAction = PayloadAction<OpenEditOverlayModalPayload>;

export type OpenEditOverlayGroupModalPayload = OverlayGroup;

export type OpenEditOverlayGroupModalAction = PayloadAction<OpenEditOverlayGroupModalPayload>;

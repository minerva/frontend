import { getSearchData } from '@/redux/search/search.thunks';
import type { SearchState } from '@/redux/search/search.types';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import searchReducer, { setPerfectMatch } from './search.slice';

const SEARCH_QUERY = ['Corticosterone'];

const INITIAL_STATE: SearchState = {
  searchValue: [''],
  perfectMatch: false,
  loading: 'idle',
};

// TODO -> mock api request and unskip test
describe.skip('search reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<SearchState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('search', searchReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(searchReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after succesfull getSearchData query', async () => {
    await store.dispatch(getSearchData({ searchQueries: SEARCH_QUERY, isPerfectMatch: false }));

    const { searchValue, loading } = store.getState().search;
    expect(searchValue).toEqual(SEARCH_QUERY);
    expect(loading).toEqual('succeeded');
  });

  it('should update store on loading getSearchData query', async () => {
    const searchPromise = store.dispatch(
      getSearchData({ searchQueries: SEARCH_QUERY, isPerfectMatch: false }),
    );

    const { searchValue, loading } = store.getState().search;
    expect(searchValue).toEqual(SEARCH_QUERY);
    expect(loading).toEqual('pending');

    searchPromise.then(() => {
      const { searchValue: searchValueFulfilled, loading: promiseFulfilled } =
        store.getState().search;

      expect(searchValueFulfilled).toEqual(SEARCH_QUERY);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });

  it('should update store on setPerfectMatch', async () => {
    await store.dispatch(setPerfectMatch(true));

    const { perfectMatch } = store.getState().search;
    expect(perfectMatch).toBe(true);
  });
});

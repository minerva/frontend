import { SearchState } from './search.types';

export const SEARCH_INITIAL_STATE: SearchState = {
  searchValue: [''],
  perfectMatch: false,
  loading: 'idle',
};

export const DATA_SEARCHING_ERROR_PREFIX = 'Failed to search data';

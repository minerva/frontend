import { Loading } from '@/types/loadingState';
import { PayloadAction } from '@reduxjs/toolkit';

export interface SearchState {
  searchValue: string[];
  perfectMatch: boolean;
  loading: Loading;
}

export type SetPerfectMatchPayload = boolean;
export type SetPerfectMatchAction = PayloadAction<SetPerfectMatchPayload>;

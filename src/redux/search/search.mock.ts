import { SearchState } from './search.types';

export const SEARCH_STATE_INITIAL_MOCK: SearchState = {
  searchValue: [''],
  perfectMatch: false,
  loading: 'idle',
};

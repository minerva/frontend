import { DEFAULT_LEGEND_ID } from './legend.constants';
import { LegendState } from './legend.types';

export const LEGEND_INITIAL_STATE_MOCK: LegendState = {
  isOpen: false,
  pluginLegend: {},
  activeLegendId: DEFAULT_LEGEND_ID,
};

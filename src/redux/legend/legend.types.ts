export type PluginId = string;
export type ImageUrl = string;

export type LegendState = {
  isOpen: boolean;
  pluginLegend: Record<PluginId, ImageUrl[]>;
  activeLegendId: string;
};

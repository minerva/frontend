import { BioEntityContentsState } from './bioEntity.types';

export const BIOENTITY_INITIAL_STATE_MOCK: BioEntityContentsState = {
  isContentTabOpened: false,
};

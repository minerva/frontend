import { PayloadAction } from '@reduxjs/toolkit';
import { BioEntityContentsState } from './bioEntity.types';

export const toggleIsContentTabOpenedReducer = (
  state: BioEntityContentsState,
  action: PayloadAction<boolean>,
): void => {
  state.isContentTabOpened = action.payload;
};

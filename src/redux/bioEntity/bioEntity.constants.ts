import { BioEntityContentsState } from './bioEntity.types';

export const BIO_ENTITY_FETCHING_ERROR_PREFIX = 'Failed to fetch bio entity';

export const BIOENTITY_INITIAL_STATE: BioEntityContentsState = {
  isContentTabOpened: false,
};

import { PublicationElement } from '@/types/models';

export const isReactionElement = (element: PublicationElement): boolean => {
  return element.idReaction !== undefined && element.idReaction !== null;
};

import { FetchDataState } from '@/types/fetchDataState';
import { NewReaction } from '@/types/models';

export type ReactionsState = FetchDataState<NewReaction[]>;

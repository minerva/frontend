/* eslint-disable no-magic-numbers */
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { newReactionFixture } from '@/models/fixtures/newReactionFixture';
import { apiPath } from '../apiPath';
import reactionsReducer from './reactions.slice';
import { getReactionsByIds } from './reactions.thunks';
import { ReactionsState } from './reactions.types';

const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const reactionId = 1;
const modelId = 2;

describe('reactions thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ReactionsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('reactions', reactionsReducer);
  });

  describe('getReactionsByIds', () => {
    it('should return data when data response from API is valid', async () => {
      const ids = { ids: [{ id: reactionId, modelId }] };

      mockedAxiosNewClient
        .onGet(apiPath.getNewReaction(modelId, reactionId))
        .reply(HttpStatusCode.Ok, newReactionFixture);

      const { payload } = await store.dispatch(getReactionsByIds(ids));
      expect(payload).toEqual({ data: [newReactionFixture], shouldConcat: false });
    });

    it('should return empty array when data asking for empty reaction list', async () => {
      const { payload } = await store.dispatch(getReactionsByIds({ ids: [] }));
      expect(payload).toEqual({ data: [], shouldConcat: false });
    });
  });
});

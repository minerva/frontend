import { ReactionsState } from './reactions.types';

export const REACTIONS_INITIAL_STATE: ReactionsState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
};

export const REACTIONS_FETCHING_ERROR_PREFIX = 'Failed to fetch reactions';

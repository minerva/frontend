import { DEFAULT_ERROR } from '@/constants/errors';
import { ReactionsState } from './reactions.types';

export const REACTIONS_STATE_INITIAL_MOCK: ReactionsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

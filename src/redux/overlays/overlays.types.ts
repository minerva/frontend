import { FetchDataState } from '@/types/fetchDataState';
import { Loading } from '@/types/loadingState';
import { MapOverlay } from '@/types/models';

export type AddOverlayState = {
  addOverlay: {
    loading: Loading;
    error: Error;
  };
};

export type UpdateOverlaysState = {
  updateOverlays: {
    loading: Loading;
    error: Error;
  };
};

export type RemoveOverlayState = {
  removeOverlay: {
    loading: Loading;
    error: Error;
  };
};

export type UserOverlays = {
  userOverlays: FetchDataState<MapOverlay[] | []>;
};

export type OverlaysState = FetchDataState<MapOverlay[] | []> &
  AddOverlayState &
  UserOverlays &
  UpdateOverlaysState &
  RemoveOverlayState;

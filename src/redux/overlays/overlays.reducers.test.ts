/* eslint-disable no-magic-numbers */
import { PROJECT_ID } from '@/constants';
import {
  createdOverlayFileFixture,
  overlayFixture,
  overlaysPageFixture,
  uploadedOverlayFileContentFixture,
} from '@/models/fixtures/overlaysFixture';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse, mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { waitFor } from '@testing-library/react';
import { unwrapResult } from '@reduxjs/toolkit';
import { apiPath } from '../apiPath';
import overlaysReducer from './overlays.slice';
import {
  addOverlay,
  getAllPublicOverlaysByProjectId,
  removeOverlay,
  updateOverlays,
} from './overlays.thunks';
import { OverlaysState } from './overlays.types';
import { ADD_OVERLAY_MOCK } from './overlays.mock';

const mockedAxiosClient = mockNetworkResponse();
const mockedAxiosNewClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: OverlaysState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
  addOverlay: {
    loading: 'idle',
    error: { name: '', message: '' },
  },
  userOverlays: {
    data: [],
    loading: 'idle',
    error: { name: '', message: '' },
  },
  updateOverlays: {
    loading: 'idle',
    error: { name: '', message: '' },
  },
  removeOverlay: {
    loading: 'idle',
    error: { name: '', message: '' },
  },
};

describe('overlays reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<OverlaysState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('overlays', overlaysReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(overlaysReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after successful getAllPublicOverlaysByProjectId query', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getAllOverlaysByProjectIdQuery(PROJECT_ID, { publicOverlay: true }))
      .reply(HttpStatusCode.Ok, overlaysPageFixture);

    const { type } = await store.dispatch(getAllPublicOverlaysByProjectId(PROJECT_ID));
    const { data, loading, error } = store.getState().overlays;

    expect(type).toBe('overlays/getAllPublicOverlaysByProjectId/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(overlaysPageFixture.content);
  });

  it('should update store after failed getAllPublicOverlaysByProjectId query', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getAllOverlaysByProjectIdQuery(PROJECT_ID, { publicOverlay: true }))
      .reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getAllPublicOverlaysByProjectId(PROJECT_ID));
    const { data, loading, error } = store.getState().overlays;

    expect(action.type).toBe('overlays/getAllPublicOverlaysByProjectId/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch overlays: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getAllPublicOverlaysByProjectId query', async () => {
    mockedAxiosNewClient
      .onGet(apiPath.getAllOverlaysByProjectIdQuery(PROJECT_ID, { publicOverlay: true }))
      .reply(HttpStatusCode.Ok, overlaysPageFixture);

    const actionPromise = store.dispatch(getAllPublicOverlaysByProjectId(PROJECT_ID));

    const { data, loading } = store.getState().overlays;
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    actionPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } = store.getState().overlays;

      expect(dataPromiseFulfilled).toEqual(overlaysPageFixture.content);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
  it('should update store when addOverlay is pending', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(123))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosClient.onPost(apiPath.createOverlay('pd')).reply(HttpStatusCode.Ok, overlayFixture);

    await store.dispatch(addOverlay(ADD_OVERLAY_MOCK));
    const { loading } = store.getState().overlays.addOverlay;

    waitFor(() => {
      expect(loading).toEqual('pending');
    });
  });

  it('should update store after successful addOverlay', async () => {
    mockedAxiosClient
      .onPost(apiPath.createOverlayFile())
      .reply(HttpStatusCode.Ok, createdOverlayFileFixture);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(123))
      .reply(HttpStatusCode.Ok, uploadedOverlayFileContentFixture);

    mockedAxiosNewClient
      .onPost(apiPath.createOverlay('pd'))
      .reply(HttpStatusCode.Ok, overlayFixture);

    await store.dispatch(addOverlay(ADD_OVERLAY_MOCK));
    const { loading, error } = store.getState().overlays.addOverlay;

    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
  });
  it('should update store after failed addOverlay', async () => {
    mockedAxiosClient.onPost(apiPath.createOverlayFile()).reply(HttpStatusCode.NotFound, undefined);

    mockedAxiosClient
      .onPost(apiPath.uploadOverlayFileContent(123))
      .reply(HttpStatusCode.NotFound, undefined);

    mockedAxiosClient.onPost(apiPath.createOverlay('pd')).reply(HttpStatusCode.NotFound, undefined);

    await store.dispatch(addOverlay(ADD_OVERLAY_MOCK));
    const { loading } = store.getState().overlays.addOverlay;

    expect(loading).toEqual('failed');
  });

  it('should update store when updateOverlay is pending', async () => {
    mockedAxiosClient
      .onPatch(apiPath.updateOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, overlayFixture);

    store.dispatch(updateOverlays([overlayFixture]));
    const { loading } = store.getState().overlays.updateOverlays;
    expect(loading).toBe('pending');
  });

  it('should update store after successful updateOverlay', async () => {
    mockedAxiosNewClient
      .onPut(apiPath.updateOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, overlayFixture);

    const updateUserOverlaysPromise = store.dispatch(updateOverlays([overlayFixture]));
    const { loading } = store.getState().overlays.updateOverlays;
    expect(loading).toBe('pending');

    await updateUserOverlaysPromise;

    const { loading: loadingFulfilled, error } = store.getState().overlays.updateOverlays;
    expect(loadingFulfilled).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
  });
  it('should update store after failed updateOverlay', async () => {
    mockedAxiosNewClient
      .onPut(apiPath.updateOverlay(overlayFixture.id))
      .reply(HttpStatusCode.NotFound, {});

    await store.dispatch(updateOverlays([overlayFixture]));

    const { loading } = store.getState().overlays.updateOverlays;
    expect(loading).toEqual('failed');
  });

  it('should update store when removeOverlay is pending', async () => {
    mockedAxiosClient
      .onDelete(apiPath.removeOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, {});

    store.dispatch(
      removeOverlay({
        overlayId: overlayFixture.id,
      }),
    );
    const { loading } = store.getState().overlays.removeOverlay;
    expect(loading).toBe('pending');
  });

  it('should update store after successful removeOverlay', async () => {
    mockedAxiosClient
      .onDelete(apiPath.removeOverlay(overlayFixture.id))
      .reply(HttpStatusCode.Ok, {});

    const removeUserOverlaysPromise = store.dispatch(
      removeOverlay({
        overlayId: overlayFixture.id,
      }),
    );
    const { loading } = store.getState().overlays.removeOverlay;
    expect(loading).toBe('pending');

    await removeUserOverlaysPromise;

    const { loading: loadingFulfilled, error } = store.getState().overlays.removeOverlay;
    expect(loadingFulfilled).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
  });
  it('should update store after failed removeOverlay', async () => {
    mockedAxiosClient
      .onDelete(apiPath.removeOverlay(overlayFixture.id))
      .reply(HttpStatusCode.NotFound, {});

    const removeUserOverlaysPromise = store.dispatch(
      removeOverlay({
        overlayId: overlayFixture.id,
      }),
    );
    const { loading } = store.getState().overlays.removeOverlay;
    expect(loading).toBe('pending');

    await removeUserOverlaysPromise;

    const { loading: loadingRejected } = store.getState().overlays.removeOverlay;
    expect(loadingRejected).toEqual('failed');
  });
});

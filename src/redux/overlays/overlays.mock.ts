import { DEFAULT_ERROR } from '@/constants/errors';
import { MapOverlay } from '@/types/models';
import { DEFAULT_GROUP } from '@/components/Map/Drawer/OverlaysDrawer/UserOverlayForm/UserOverlayForm.constants';
import { OverlaysState } from './overlays.types';

export const OVERLAYS_INITIAL_STATE_MOCK: OverlaysState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
  addOverlay: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  userOverlays: {
    data: [],
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  updateOverlays: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  removeOverlay: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
};

export const PUBLIC_OVERLAYS_MOCK: MapOverlay[] = [
  {
    id: 11,
    group: null,
    name: 'PD substantia nigra',
    creator: 'appu-admin',
    description:
      'Differential transcriptome expression from post mortem tissue. Meta-analysis from 8 published datasets, FDR = 0.05, see PMIDs 23832570 and 25447234.',
    genomeType: null,
    genomeVersion: null,
    publicOverlay: true,
    type: 'GENERIC',
    order: 1,
  },
  {
    id: 12,
    group: null,
    name: 'Ageing brain',
    creator: 'appu-admin',
    description:
      'Differential transcriptome expression from post mortem tissue. Source: Allen Brain Atlas datasets, see PMID 25447234.',
    genomeType: null,
    genomeVersion: null,
    publicOverlay: true,
    type: 'GENERIC',
    order: 2,
  },
  {
    id: 17,
    group: null,
    name: 'PRKN variants example',
    creator: 'appu-admin',
    description: 'PRKN variants',
    genomeType: 'UCSC',
    genomeVersion: 'hg19',
    publicOverlay: true,
    type: 'GENETIC_VARIANT',
    order: 3,
  },
  {
    id: 18,
    group: null,
    name: 'PRKN variants doubled',
    creator: 'appu-admin',
    description: 'PRKN variants',
    genomeType: 'UCSC',
    genomeVersion: 'hg19',
    publicOverlay: true,
    type: 'GENETIC_VARIANT',
    order: 4,
  },
  {
    id: 20,
    group: null,
    name: 'Generic advanced format overlay',
    creator: 'appu-admin',
    description: 'Data set provided by a user',
    genomeType: null,
    genomeVersion: null,
    publicOverlay: true,
    type: 'GENERIC',
    order: 5,
  },
];

export const OVERLAYS_PUBLIC_FETCHED_STATE_MOCK: OverlaysState = {
  data: PUBLIC_OVERLAYS_MOCK,
  loading: 'succeeded',
  error: DEFAULT_ERROR,
  addOverlay: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  userOverlays: {
    data: [],
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  updateOverlays: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  removeOverlay: {
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
};

export const ADD_OVERLAY_MOCK = {
  content: 'test',
  description: 'test',
  filename: 'unknown.txt',
  name: 'test',
  projectId: 'pd',
  type: 'GENERIC',
  group: DEFAULT_GROUP,
};

export const USER_OVERLAYS_MOCK: MapOverlay[] = [
  {
    id: 99,
    group: null,
    name: 'PD substantia nigra',
    creator: 'appu-admin',
    description:
      'Differential transcriptome expression from post mortem tissue. Meta-analysis from 8 published datasets, FDR = 0.05, see PMIDs 23832570 and 25447234.',
    genomeType: null,
    genomeVersion: null,
    publicOverlay: true,
    type: 'GENERIC',
    order: 1,
  },
  {
    id: 123,
    group: null,
    name: 'Ageing brain',
    creator: 'appu-admin',
    description:
      'Differential transcriptome expression from post mortem tissue. Source: Allen Brain Atlas datasets, see PMID 25447234.',
    genomeType: null,
    genomeVersion: null,
    publicOverlay: true,
    type: 'GENERIC',
    order: 2,
  },
];

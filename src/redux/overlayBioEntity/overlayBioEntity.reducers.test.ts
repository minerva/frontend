import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { overlayBioEntityFixture } from '@/models/fixtures/overlayBioEntityFixture';
import overlayBioEntityReducer from './overlayBioEntity.slice';
import { getOverlayBioEntity } from './overlayBioEntity.thunk';
import { parseOverlayBioEntityToOlRenderingFormat } from './overlayBioEntity.utils';
import { OverlaysBioEntityState } from './overlayBioEntity.types';
import { apiPath } from '../apiPath';

const mockedNewAxiosClient = mockNetworkNewAPIResponse();

describe('Overlay Bio Entity Reducers', () => {
  const OVERLAY_ID = 21;
  const MODEL_ID = 27;

  let store = {} as ToolkitStoreWithSingleSlice<OverlaysBioEntityState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('overlayBioEntity', overlayBioEntityReducer);
  });

  describe('getOverlayBioEntityReducer', () => {
    it('should update the state correctly when getOverlayBioEntity action is dispatched', async () => {
      mockedNewAxiosClient
        .onGet(apiPath.getOverlayBioEntity({ overlayId: OVERLAY_ID, modelId: MODEL_ID }))
        .reply(HttpStatusCode.Ok, overlayBioEntityFixture);

      const { type } = await store.dispatch(
        getOverlayBioEntity({ overlayId: OVERLAY_ID, modelId: MODEL_ID }),
      );
      const { data } = store.getState().overlayBioEntity;

      expect(type).toBe('overlayBioEntity/getOverlayBioEntity/fulfilled');
      expect(data[OVERLAY_ID][MODEL_ID]).toEqual(
        parseOverlayBioEntityToOlRenderingFormat(overlayBioEntityFixture, OVERLAY_ID),
      );
    });
  });
});

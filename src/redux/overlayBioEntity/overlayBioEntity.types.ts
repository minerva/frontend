import { OverlayBioEntityRender } from '@/types/OLrendering';
import { PayloadAction } from '@reduxjs/toolkit';

export type OverlaysBioEntityState = {
  overlaysId: number[];
  data: {
    [overlayId: number]: {
      [modelId: number]: OverlayBioEntityRender[];
    };
  };
};

export type RemoveOverlayBioEntityForGivenOverlayPayload = { overlayId: number };
export type RemoveOverlayBioEntityForGivenOverlayAction =
  PayloadAction<RemoveOverlayBioEntityForGivenOverlayPayload>;

export type OverlaysIdsAndOrder = {
  id: number;
  order: number;
}[];

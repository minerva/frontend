/* eslint-disable no-magic-numbers */
import { overlayBioEntityFixture } from '@/models/fixtures/overlayBioEntityFixture';
import { OverlayBioEntity } from '@/types/models';
import {
  calculateOvarlaysOrder,
  getActiveOverlaysIdsAndOrder,
  getActiveUserOverlaysIdsAndOrder,
  getValidOverlayBioEntities,
} from './overlayBioEntity.utils';

describe('calculateOverlaysOrder', () => {
  const cases = [
    {
      data: [
        { id: 1, order: 11 },
        { id: 2, order: 12 },
        { id: 3, order: 13 },
      ],
      expected: [
        { id: 1, order: 11, calculatedOrder: 1, index: 0 },
        { id: 2, order: 12, calculatedOrder: 2, index: 1 },
        { id: 3, order: 13, calculatedOrder: 3, index: 2 },
      ],
    },
    // different order
    {
      data: [
        { id: 2, order: 12 },
        { id: 3, order: 13 },
        { id: 1, order: 11 },
      ],
      expected: [
        { id: 1, order: 11, calculatedOrder: 1, index: 0 },
        { id: 2, order: 12, calculatedOrder: 2, index: 1 },
        { id: 3, order: 13, calculatedOrder: 3, index: 2 },
      ],
    },
    {
      data: [
        { id: 1, order: 11 },
        { id: 2, order: 11 },
        { id: 3, order: 11 },
      ],
      expected: [
        { id: 1, order: 11, calculatedOrder: 1, index: 0 },
        { id: 2, order: 11, calculatedOrder: 2, index: 1 },
        { id: 3, order: 11, calculatedOrder: 3, index: 2 },
      ],
    },
    // different order
    {
      data: [
        { id: 2, order: 11 },
        { id: 3, order: 11 },
        { id: 1, order: 11 },
      ],
      expected: [
        { id: 1, order: 11, calculatedOrder: 1, index: 0 },
        { id: 2, order: 11, calculatedOrder: 2, index: 1 },
        { id: 3, order: 11, calculatedOrder: 3, index: 2 },
      ],
    },
    {
      data: [],
      expected: [],
    },
  ];

  it.each(cases)('should return valid overlays order', ({ data, expected }) => {
    expect(calculateOvarlaysOrder(data)).toStrictEqual(expected);
  });
});

describe('getValidOverlayBioEntities', () => {
  it('should return empty array if overlayBioEntities are empty array', () => {
    const result = getValidOverlayBioEntities([]);

    expect(result).toEqual([]);
  });
  it('should return the same overlayBioEntities if all overlayBioEntities are valid', () => {
    const result = getValidOverlayBioEntities(overlayBioEntityFixture);
    expect(result).toEqual(overlayBioEntityFixture);
  });
  it('should filter properly and return valid overlayBioEntities if overlayBioEntities are mixed array with valid and invalid entities', () => {
    const invalidOverlayBioEntities = overlayBioEntityFixture.map(overlayBioEntity => ({
      ...overlayBioEntity,
      left: {},
    })) as OverlayBioEntity[];

    const result = getValidOverlayBioEntities([
      ...overlayBioEntityFixture,
      ...invalidOverlayBioEntities,
    ]);
    expect(result).toEqual(overlayBioEntityFixture);
  });
  it('should return empty array if all overlayBioEntities are invalid', () => {
    const invalidOverlayBioEntities = overlayBioEntityFixture.map(overlayBioEntity => ({
      ...overlayBioEntity,
      right: {},
    })) as OverlayBioEntity[];

    const result = getValidOverlayBioEntities(invalidOverlayBioEntities);

    expect(result).toEqual([]);
  });
});

describe('getActiveUserOverlaysIdsAndOrder', () => {
  const userOverlaysIdsAndOrder = [
    { id: 1, order: 1 },
    { id: 2, order: 2 },
    { id: 3, order: 3 },
    { id: 4, order: 4 },
  ];

  const activeOverlaysIds = [2, 4];
  const maxOrderValue = 10;

  it('should return active user overlays with updated order values', () => {
    const expectedOutput = [
      { id: 2, order: 12 },
      { id: 4, order: 14 },
    ];
    expect(
      getActiveUserOverlaysIdsAndOrder(userOverlaysIdsAndOrder, activeOverlaysIds, maxOrderValue),
    ).toEqual(expectedOutput);
  });

  it('should return an empty array when there are no active overlays', () => {
    expect(getActiveUserOverlaysIdsAndOrder(userOverlaysIdsAndOrder, [], maxOrderValue)).toEqual(
      [],
    );
  });

  it('should return all user overlays with updated order values when all overlays are active', () => {
    const allOverlaysActive = [1, 2, 3, 4];
    const expectedOutput = [
      { id: 1, order: 11 },
      { id: 2, order: 12 },
      { id: 3, order: 13 },
      { id: 4, order: 14 },
    ];
    expect(
      getActiveUserOverlaysIdsAndOrder(userOverlaysIdsAndOrder, allOverlaysActive, maxOrderValue),
    ).toEqual(expectedOutput);
  });

  it('should return active user overlays with order values starting from 1 when maxOrderValue is 0', () => {
    const maxOrderZero = 0;
    const expectedOutput = [
      { id: 2, order: 2 },
      { id: 4, order: 4 },
    ];
    expect(
      getActiveUserOverlaysIdsAndOrder(userOverlaysIdsAndOrder, activeOverlaysIds, maxOrderZero),
    ).toEqual(expectedOutput);
  });
});

describe('getActiveOverlaysIdsAndOrder', () => {
  const overlaysIdsAndOrder = [
    { id: 1, order: 1 },
    { id: 2, order: 2 },
    { id: 3, order: 3 },
    { id: 4, order: 4 },
  ];

  const activeOverlaysIds = [2, 4];

  it('should return maxOrderValue and active overlays', () => {
    const expectedOutput = {
      maxOrderValue: 4,
      activeOverlaysIdsAndOrder: [
        { id: 2, order: 2 },
        { id: 4, order: 4 },
      ],
    };
    expect(getActiveOverlaysIdsAndOrder(overlaysIdsAndOrder, activeOverlaysIds)).toEqual(
      expectedOutput,
    );
  });

  it('should return maxOrderValue as 0 when there are no active overlays', () => {
    const expectedOutput = {
      maxOrderValue: 0,
      activeOverlaysIdsAndOrder: [],
    };
    expect(getActiveOverlaysIdsAndOrder(overlaysIdsAndOrder, [])).toEqual(expectedOutput);
  });

  it('should return maxOrderValue and all overlays when all overlays are active', () => {
    const allOverlaysActive = [1, 2, 3, 4];
    const expectedOutput = {
      maxOrderValue: 4,
      activeOverlaysIdsAndOrder: overlaysIdsAndOrder,
    };
    expect(getActiveOverlaysIdsAndOrder(overlaysIdsAndOrder, allOverlaysActive)).toEqual(
      expectedOutput,
    );
  });

  it('should return maxOrderValue as 0 and no active overlays when none of the overlays are active', () => {
    const expectedOutput = {
      maxOrderValue: 0,
      activeOverlaysIdsAndOrder: [],
    };
    expect(getActiveOverlaysIdsAndOrder(overlaysIdsAndOrder, [])).toEqual(expectedOutput);
  });
  it('should return maxOrderValue as 0 and no active overlays when there are no overlays', () => {
    const expectedOutput = {
      maxOrderValue: 0,
      activeOverlaysIdsAndOrder: [],
    };
    expect(getActiveOverlaysIdsAndOrder([], [])).toEqual(expectedOutput);
  });
});

export const OVERLAY_BIO_ENTITY_FETCHING_ERROR_PREFIX = 'Failed to fetch overlay bio entity';
export const OVERLAY_BIO_ENTITY_ALL_MODELS_FETCHING_ERROR_PREFIX =
  'Failed to fetch overlay bio entity for all models';
export const INIT_OVERLAYS_ERROR_PREFIX = 'Failed to initialize overlays';

import { AutocompleteState } from './autocomplete.types';

export const AUTOCOMPLETE_INITIAL_STATE: AutocompleteState = {
  searchValues: [''],
  loading: 'idle',
};

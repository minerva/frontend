import { Loading } from '@/types/loadingState';

export interface AutocompleteState {
  searchValues: string[];
  loading: Loading;
}

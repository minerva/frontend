import { DEFAULT_CENTER_POINT, DEFAULT_TILE_SIZE } from '@/constants/map';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { MAP_DATA_INITIAL_STATE, MAP_INITIAL_STATE } from './map.constants';
import { varyPositionZoom } from './map.slice';

describe('map reducers', () => {
  describe('varyPositionZoomReducer', () => {
    const baseMapSize = {
      width: 0,
      height: 0,
      tileSize: DEFAULT_TILE_SIZE,
    };

    const cases: [
      {
        minZoom: number;
        maxZoom: number;
        currentZ: number;
      },
      { delta: number },
      { finalZ: number },
    ][] = [
      [{ minZoom: 1, maxZoom: 1, currentZ: 1 }, { delta: 1 }, { finalZ: 1 }], // exeeds the interval
      [{ minZoom: 1, maxZoom: 1, currentZ: 1 }, { delta: -1 }, { finalZ: 1 }], // deceeds the interval
      [{ minZoom: 1, maxZoom: 2, currentZ: 1 }, { delta: 1 }, { finalZ: 2 }], // inside the interval (with positive delta)
      [{ minZoom: 0, maxZoom: 1, currentZ: 1 }, { delta: -1 }, { finalZ: 0 }], // inside the interval (with negative delta)
    ];

    it.each(cases)(
      'should set valid final z position',
      ({ minZoom, maxZoom, currentZ }, { delta }, { finalZ }) => {
        const { store } = getReduxWrapperWithStore({
          map: {
            ...MAP_INITIAL_STATE,
            data: {
              ...MAP_DATA_INITIAL_STATE,
              size: {
                ...baseMapSize,
                minZoom,
                maxZoom,
              },
              position: {
                initial: {
                  ...DEFAULT_CENTER_POINT,
                  z: currentZ,
                },
                last: {
                  ...DEFAULT_CENTER_POINT,
                  z: currentZ,
                },
              },
            },
          },
        });

        store.dispatch(varyPositionZoom({ delta }));
        const newState = store.getState();
        const newInitialZ = newState.map.data.position.initial.z;
        const newLastZ = newState.map.data.position.last.z;

        expect(newInitialZ).toEqual(finalZ);
        expect(newLastZ).toEqual(finalZ);
      },
    );
  });
});

import { DEFAULT_ERROR } from '@/constants/errors';
import { MODEL_ID_DEFAULT } from '@/redux/map/map.constants';
import MapBackgroundsEnum from '@/redux/map/map.enums';
import { HISTAMINE_MAP_ID, MAIN_MAP_ID } from '@/constants/mocks';
import { MapData, MapState, OppenedMap } from './map.types';

export const openedMapsInitialValueFixture: OppenedMap[] = [
  { modelId: 0, modelName: 'Main map', lastPosition: { x: 0, y: 0, z: 0 } },
];

export const openedMapsThreeSubmapsFixture: OppenedMap[] = [
  { modelId: MAIN_MAP_ID, modelName: 'Main map', lastPosition: { x: 0, y: 0, z: 0 } },
  {
    modelId: HISTAMINE_MAP_ID,
    modelName: 'Histamine signaling',
    lastPosition: { x: 0, y: 0, z: 0 },
  },
  { modelId: 5054, modelName: 'PRKN substrates', lastPosition: { x: 0, y: 0, z: 0 } },
];

export const initialMapDataFixture: MapData = {
  projectId: 'pdmap',
  meshId: '',
  modelId: 0,
  backgroundId: MapBackgroundsEnum.SEMANTIC,
  overlaysIds: [],
  position: {
    initial: { x: 0, y: 0, z: 5 },
    last: { x: 0, y: 0, z: 5 },
  },
  show: {
    legend: false,
    comments: false,
  },
  size: {
    width: 0,
    height: 0,
    tileSize: 256,
    minZoom: 2,
    maxZoom: 9,
  },
  lastClick: {
    modelId: MODEL_ID_DEFAULT,
    position: {
      x: 0,
      y: 0,
    },
  },
  lastRightClick: {
    modelId: MODEL_ID_DEFAULT,
    position: {
      x: 0,
      y: 0,
    },
  },
};

export const initialMapStateFixture: MapState = {
  data: initialMapDataFixture,
  loading: 'idle',
  error: DEFAULT_ERROR,
  openedMaps: openedMapsInitialValueFixture,
};

export const mapStateWithCurrentlySelectedMainMapFixture: MapState = {
  data: {
    ...initialMapDataFixture,
    modelId: 52,
    size: {
      width: 26779.25,
      height: 13503,
      tileSize: 256,
      minZoom: 2,
      maxZoom: 9,
    },
  },
  loading: 'idle',
  error: DEFAULT_ERROR,
  openedMaps: openedMapsInitialValueFixture,
};

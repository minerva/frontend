import { FetchDataState } from '@/types/fetchDataState';
import { Point } from '@/types/map';
import { QueryData } from '@/types/query';
import { PayloadAction } from '@reduxjs/toolkit';

export interface MapSize {
  width: number;
  height: number;
  tileSize: number;
  minZoom: number;
  maxZoom: number;
}

export type OppenedMap = {
  modelId: number;
  modelName: string;
  lastPosition: Point;
};

export type Position = {
  initial: Point;
  last: Point;
};

export type MapData = {
  projectId: string;
  meshId: string;
  modelId: number;
  backgroundId: number;
  overlaysIds: number[];
  size: MapSize;
  position: Position;
  lastClick: {
    position: Point;
    modelId: number;
  };
  lastRightClick: {
    position: Point;
    modelId: number;
  };
  show: {
    legend: boolean;
    comments: boolean;
  };
};

export type MapState = FetchDataState<MapData, MapData> & { openedMaps: OppenedMap[] };

export type SetMapDataActionPayload =
  | (Omit<Partial<MapData>, 'position' | 'projectId'> & {
      projectId?: string;
    })
  | undefined;

export type UpdateOpenedMainMapActionPayload = Pick<OppenedMap, 'modelId' | 'lastPosition'>;

export type UpdateOpenedMainMapAction = PayloadAction<UpdateOpenedMainMapActionPayload>;

export type SetMapDataAction = PayloadAction<SetMapDataActionPayload>;

export type SetActiveMapActionPayload = Pick<OppenedMap, 'modelId'>;

export type SetActiveMapAction = PayloadAction<SetActiveMapActionPayload>;

export type SetMainMapModelIdAction = PayloadAction<SetActiveMapActionPayload>;

export type OpenMapAndSetActivePayload = Pick<OppenedMap, 'modelId' | 'modelName'>;

export type OpenMapAndSetActiveAction = PayloadAction<OpenMapAndSetActivePayload>;

export type CloseMapActionPayload = Pick<OppenedMap, 'modelId'>;

export type CloseMapAction = PayloadAction<CloseMapActionPayload>;

export type CloseMapActionAndSetMainMapActive = PayloadAction<
  {
    currentModelId: number;
  } & Pick<OppenedMap, 'modelId'>
>;

export type InitMapDataActionParams = { queryData: QueryData };

export type InitMapDataAction = PayloadAction<SetMapDataAction>;

export type SetMapDataByQueryDataActionParams = { queryData: QueryData };

export type SetMapDataByQueryDataActionPayload = Pick<
  MapData,
  'modelId' | 'backgroundId' | 'position'
>;

export type GetUpdatedMapDataResult = Pick<
  MapData,
  'modelId' | 'backgroundId' | 'size' | 'position'
>;

export type SetMapPositionDataAction = PayloadAction<Point>;

export type SetLastPositionZoomWithDeltaActionPayload = {
  delta: number;
};

export type SetLastPositionZoomWithDeltaAction =
  PayloadAction<SetLastPositionZoomWithDeltaActionPayload>;

export type SetLastPositionZoomActionPayload = {
  zoom: number;
};

export type SetLastPositionZoomAction = PayloadAction<SetLastPositionZoomActionPayload>;

export type SetLastClickPositionActionPayload = {
  modelId: number;
  coordinates: Point;
};

export type SetLastClickPositionAction = PayloadAction<SetLastClickPositionActionPayload>;

export type InitMapDataActionPayload = {
  data: GetUpdatedMapDataResult | object;
  openedMaps: OppenedMap[];
};
export type MiddlewareAllowedAction = PayloadAction<
  SetMapDataActionPayload | InitMapDataActionPayload
>;

export type InitOpenedMapsActionPayload = OppenedMap[];

export type InitOpenedMapsProps = {
  queryData: QueryData;
};

export type MapSizeAndModelId = Pick<MapData, 'modelId' | 'size'>;
export type InitMapSizeAndModelIdActionPayload = MapSizeAndModelId;
export type InitMapSizeAndModelIdParams = {
  queryData: QueryData;
};

export type InitMapPositionActionPayload = Position;
export type InitMapPositionParams = {
  queryData: QueryData;
};

export type InitMapBackgroundActionPayload = number;
export type InitMapBackgroundParams = {
  queryData: QueryData;
};

export type SetBackgroundActionPayload = number;
export type SetBackgroundAction = PayloadAction<SetBackgroundActionPayload>;

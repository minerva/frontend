import { RootState } from '@/redux/store';
import { Loading } from '@/types/loadingState';
import {
  MAP_DATA_INITIAL_STATE,
  MIDDLEWARE_ALLOWED_ACTIONS,
  OPENED_MAPS_INITIAL_STATE,
} from '../map.constants';
import { SetMapDataAction } from '../map.types';
import { checkIfIsMapUpdateActionValid } from './checkIfIsMapUpdateActionValid';

const state: Pick<RootState, 'map'> = {
  map: {
    data: {
      ...MAP_DATA_INITIAL_STATE,
      modelId: 2137,
    },
    loading: 'idle' as Loading,
    error: { name: '', message: '' },
    openedMaps: OPENED_MAPS_INITIAL_STATE,
  },
};

describe('checkIfIsActionValid - util', () => {
  describe('when action payload has model id equal to current', () => {
    const modelId = 2137;

    it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should return false', actionType => {
      expect(
        checkIfIsMapUpdateActionValid(
          {
            type: actionType,
            payload: {
              modelId,
            },
          } as SetMapDataAction,
          state as RootState,
        ),
      ).toBe(false);
    });
  });

  describe('when action payload has model id different than current', () => {
    const modelId = 997;

    it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should return true', actionType => {
      expect(
        checkIfIsMapUpdateActionValid(
          {
            type: actionType,
            payload: {
              modelId,
            },
          } as SetMapDataAction,
          state as RootState,
        ),
      ).toBe(true);
    });
  });

  describe('when action payload has NOT model id', () => {
    it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should return true', actionType => {
      expect(
        checkIfIsMapUpdateActionValid(
          {
            type: actionType,
            payload: {},
          } as SetMapDataAction,
          state as RootState,
        ),
      ).toBe(true);
    });
  });
});

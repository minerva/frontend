/* eslint-disable no-magic-numbers */
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { RootState } from '@/redux/store';
import { MIDDLEWARE_ALLOWED_ACTIONS } from '../map.constants';
import { getUpdatedModel } from './getUpdatedModel';

const state: Pick<RootState, 'models'> = {
  models: {
    data: modelsFixture,
    loading: 'idle',
    error: { name: '', message: '' },
  },
};

describe('getUpdatedModel - util', () => {
  describe('when payload has valid modelId', () => {
    const model = modelsFixture[0];
    const action = {
      type: MIDDLEWARE_ALLOWED_ACTIONS[0],
      payload: {
        modelId: model.id,
      },
    };

    it('returns model object', () => {
      expect(getUpdatedModel(action, state as RootState)).toStrictEqual(model);
    });
  });

  describe('when payload has invalid modelId', () => {
    const action = {
      type: MIDDLEWARE_ALLOWED_ACTIONS[0],
      payload: {
        modelId: null,
      },
    };

    it('returns undefined', () => {
      expect(getUpdatedModel(action, state as RootState)).toStrictEqual(undefined);
    });
  });

  describe('when payload does not have modelId', () => {
    const action = {
      type: MIDDLEWARE_ALLOWED_ACTIONS[0],
      payload: {},
    };

    it('returns undefined', () => {
      expect(getUpdatedModel(action, state as RootState)).toStrictEqual(undefined);
    });
  });
});

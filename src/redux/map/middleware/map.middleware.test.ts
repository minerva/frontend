/* eslint-disable no-magic-numbers */
import { modelsFixture } from '@/models/fixtures/modelsFixture';
import { Loading } from '@/types/loadingState';
import { getReduxWrapperWithStore } from '@/utils/testing/getReduxWrapperWithStore';
import { Action } from '@reduxjs/toolkit';
import {
  MAP_DATA_INITIAL_STATE,
  MIDDLEWARE_ALLOWED_ACTIONS,
  OPENED_MAPS_INITIAL_STATE,
} from '../map.constants';
import * as mapSlice from '../map.slice';
import * as checkIfIsMapUpdateActionValid from './checkIfIsMapUpdateActionValid';
import * as getUpdatedModel from './getUpdatedModel';
import { mapDataMiddlewareListener } from './map.middleware';

jest.mock('./checkIfIsMapUpdateActionValid', () => {
  return {
    __esModule: true,
    ...jest.requireActual('./checkIfIsMapUpdateActionValid'),
  };
});

jest.mock('./getUpdatedModel', () => {
  return {
    __esModule: true,
    ...jest.requireActual('./getUpdatedModel'),
  };
});

jest.mock('./getUpdatedModel', () => {
  return {
    __esModule: true,
    ...jest.requireActual('./getUpdatedModel'),
  };
});

jest.mock('../map.slice', () => {
  return {
    __esModule: true,
    ...jest.requireActual('../map.slice'),
  };
});

const defaultSliceState = {
  data: [],
  loading: 'idle' as Loading,
  error: { name: '', message: '' },
};

const checkIfIsMapUpdateActionValidSpy = jest.spyOn(
  checkIfIsMapUpdateActionValid,
  'checkIfIsMapUpdateActionValid',
);
const getUpdatedModelSpy = jest.spyOn(getUpdatedModel, 'getUpdatedModel');
const setMapDataSpy = jest.spyOn(mapSlice, 'setMapData');
const setMapPositionSpy = jest.spyOn(mapSlice, 'setMapPosition');

const { store } = getReduxWrapperWithStore({
  map: {
    ...defaultSliceState,
    data: {
      ...MAP_DATA_INITIAL_STATE,
      modelId: modelsFixture[0].id,
    },
    openedMaps: OPENED_MAPS_INITIAL_STATE,
  },
  models: {
    ...defaultSliceState,
    data: modelsFixture,
  },
});

const doDispatch = jest.fn();
const doGetState = store.getState;

const handler = async (action: Action): Promise<void> =>
  mapDataMiddlewareListener(action, {
    dispatch: doDispatch,
    getOriginalState: doGetState,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } as any);

describe('map middleware', () => {
  describe('on listen', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    describe('when model is valid and different than current', () => {
      it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should dispatch setMapData, %s', async actionType => {
        const model = modelsFixture[1];

        const action = {
          payload: {
            modelId: model.id,
          },
          type: actionType,
        };

        await handler(action);
        expect(checkIfIsMapUpdateActionValidSpy).toHaveLastReturnedWith(true);
        expect(getUpdatedModelSpy).toHaveLastReturnedWith(model);
        expect(setMapDataSpy).toBeCalled();
        expect(setMapPositionSpy).toBeCalled();
      });
    });

    describe('when model is valid and identical with current', () => {
      it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should NOT dispatch setMapData', async actionType => {
        const model = modelsFixture[0];
        const action = {
          payload: {
            modelId: model.id,
          },
          type: actionType,
        };

        await handler(action);
        expect(checkIfIsMapUpdateActionValidSpy).toHaveLastReturnedWith(false);
        expect(getUpdatedModelSpy).toHaveLastReturnedWith(model);
        expect(setMapDataSpy).not.toBeCalled();
        expect(setMapPositionSpy).not.toBeCalled();
      });
    });

    describe('when model is NOT valid', () => {
      it.each(MIDDLEWARE_ALLOWED_ACTIONS)('should NOT dispatch setMapData', async actionType => {
        const action = {
          payload: {
            modelId: null,
          },
          type: actionType,
        };

        await handler(action);
        expect(checkIfIsMapUpdateActionValidSpy).toHaveLastReturnedWith(true);
        expect(getUpdatedModelSpy).toHaveLastReturnedWith(undefined);
        expect(setMapDataSpy).not.toBeCalled();
        expect(setMapPositionSpy).not.toBeCalled();
      });
    });
  });
});

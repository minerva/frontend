/* eslint-disable no-magic-numbers */
enum MapBackgroundsEnum {
  NETWORK = 1,
  SEMANTIC = 2,
}

export default MapBackgroundsEnum;

import { MODELS_MOCK } from '@/models/mocks/modelsMock';
/* eslint-disable no-magic-numbers */
import { QueryData } from '@/types/query';
import { projectFixture } from '@/models/fixtures/projectFixture';
import { Project } from '@/types/models';
import { PROJECT_STATE_INITIAL_MOCK } from '@/redux/project/project.mock';
import { ProjectState } from '@/redux/project/project.types';
import { MAIN_MAP_ID } from '@/constants/mocks';
import MapBackgroundsEnum from '@/redux/map/map.enums';
import { MODELS_INITIAL_STATE_MOCK } from '../models/models.mock';
import { INITIAL_STORE_STATE_MOCK } from '../root/root.fixtures';
import { RootState } from '../store';
import { initialMapDataFixture, initialMapStateFixture } from './map.fixtures';
import {
  getBackgroundId,
  getInitMapPosition,
  getInitMapSizeAndModelId,
  getOpenedMaps,
} from './map.thunks';

const EMPTY_QUERY_DATA: QueryData = {
  modelId: undefined,
  backgroundId: undefined,
  initialPosition: undefined,
  perfectMatch: false,
};

const QUERY_DATA_WITH_BG: QueryData = {
  modelId: undefined,
  backgroundId: 21,
  initialPosition: undefined,
  perfectMatch: false,
};

const QUERY_DATA_WITH_MODELID: QueryData = {
  modelId: 5054,
  backgroundId: undefined,
  initialPosition: undefined,
  perfectMatch: false,
};

const QUERY_DATA_WITH_POSITION: QueryData = {
  modelId: undefined,
  backgroundId: undefined,
  initialPosition: {
    x: 21,
    y: 3,
    z: 7,
  },
  perfectMatch: false,
};

const PROJECT: Project = {
  ...projectFixture,
  topMap: { id: MAIN_MAP_ID },
};

const PROJECT_STATE: ProjectState = {
  ...PROJECT_STATE_INITIAL_MOCK,
  data: PROJECT,
};

const STATE_WITH_MODELS: RootState = {
  ...INITIAL_STORE_STATE_MOCK,
  project: { ...PROJECT_STATE },
  models: { ...MODELS_INITIAL_STATE_MOCK, data: MODELS_MOCK },
};

describe('map thunks - utils', () => {
  describe('getBackgroundId', () => {
    it('should return main map background id (2 - semantic) if query param does not include background id', () => {
      const store: RootState = {
        ...INITIAL_STORE_STATE_MOCK,
      };
      const backgroundId = getBackgroundId(store, EMPTY_QUERY_DATA);

      expect(backgroundId).toBe(MapBackgroundsEnum.SEMANTIC);
    });
    it('should return default value (2 - semantic) if query data does not include backgroundId and could not find main background in the store', () => {
      const backgroundId = getBackgroundId(INITIAL_STORE_STATE_MOCK, EMPTY_QUERY_DATA);

      expect(backgroundId).toBe(MapBackgroundsEnum.SEMANTIC);
    });

    it('should return semantic map background id if query param background id is invalid', () => {
      const store: RootState = {
        ...INITIAL_STORE_STATE_MOCK,
      };
      const backgroundId = getBackgroundId(store, QUERY_DATA_WITH_BG);

      expect(backgroundId).toBe(MapBackgroundsEnum.SEMANTIC);
    });
  });

  describe('getInitMapPosition', () => {
    it('should return valid map position from query params ', () => {
      const position = getInitMapPosition(STATE_WITH_MODELS, QUERY_DATA_WITH_POSITION);
      expect(position).toEqual({
        initial: { x: 21, y: 3, z: 7 },
        last: { x: 21, y: 3, z: 7 },
      });
    });

    it('should return valid map position if query params do not include position', () => {
      const position = getInitMapPosition(STATE_WITH_MODELS, EMPTY_QUERY_DATA);
      expect(position).toEqual({
        initial: { x: 13389.625, y: 6751.5, z: 4 },
        last: { x: 13389.625, y: 6751.5, z: 4 },
      });
    });
    it('should return default map position', () => {
      const position = getInitMapPosition(INITIAL_STORE_STATE_MOCK, EMPTY_QUERY_DATA);

      expect(position).toEqual({ initial: { x: 0, y: 0, z: 0 }, last: { x: 0, y: 0, z: 0 } });
    });
  });

  describe('getInitMapSizeAndModelId', () => {
    it('should return correct map size and modelId when modelId is provided in queryData', () => {
      const payload = getInitMapSizeAndModelId(STATE_WITH_MODELS, QUERY_DATA_WITH_MODELID);

      expect(payload).toEqual({
        modelId: 5054,
        size: { height: 1171.9429798877356, maxZoom: 5, minZoom: 2, tileSize: 256, width: 1652.75 },
      });
    });
    it('should return correct map size and modelId if query params do not include modelId', () => {
      const payload = getInitMapSizeAndModelId(STATE_WITH_MODELS, EMPTY_QUERY_DATA);
      expect(payload).toEqual({
        modelId: MAIN_MAP_ID,
        size: {
          height: 13503,
          maxZoom: 9,
          minZoom: 2,
          tileSize: 256,
          width: 26779.25,
        },
      });
    });
    it('should return correct map size and modelId if query params include invalid modelId', () => {
      const payload = getInitMapSizeAndModelId(STATE_WITH_MODELS, {
        ...EMPTY_QUERY_DATA,
        modelId: 1234567,
      });
      expect(payload).toEqual({
        modelId: MAIN_MAP_ID,
        size: {
          height: 13503,
          maxZoom: 9,
          minZoom: 2,
          tileSize: 256,
          width: 26779.25,
        },
      });
    });
  });

  describe('getOpenedMaps ', () => {
    it('should return main map only', () => {
      const openedMaps = getOpenedMaps(
        {
          ...STATE_WITH_MODELS,
          map: {
            ...initialMapStateFixture,
            data: { ...initialMapDataFixture, modelId: MAIN_MAP_ID },
          },
        },
        EMPTY_QUERY_DATA,
      );

      expect(openedMaps).toEqual([
        { lastPosition: { x: 0, y: 0, z: 0 }, modelId: MAIN_MAP_ID, modelName: 'Main map' },
      ]);
    });
    it('should return main map and opened submap', () => {
      const openedMaps = getOpenedMaps(
        {
          ...STATE_WITH_MODELS,
          map: { ...initialMapStateFixture, data: { ...initialMapDataFixture, modelId: 5054 } },
        },
        EMPTY_QUERY_DATA,
      );

      expect(openedMaps).toEqual([
        { lastPosition: { x: 0, y: 0, z: 0 }, modelId: MAIN_MAP_ID, modelName: 'Main map' },
        {
          lastPosition: {
            x: 0,
            y: 0,
            z: 0,
          },
          modelId: 5054,
          modelName: 'PRKN substrates',
        },
      ]);
    });
  });
});

import { projectPageFixture } from '@/models/fixtures/projectsFixture';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { ProjectsState } from '@/redux/projects/projects.types';
import { getProjects } from '@/redux/projects/projects.thunks';
import { apiPath } from '../apiPath';
import projectsReducer from './projects.slice';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: ProjectsState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
};

describe('projects reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ProjectsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('projects', projectsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(projectsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after successfull getProjects query', async () => {
    mockedAxiosClient.onGet(apiPath.getProjects()).reply(HttpStatusCode.Ok, projectPageFixture);

    const { type } = await store.dispatch(getProjects());
    const { data, loading, error } = store.getState().projects;

    expect(type).toBe('project/getProjects/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(projectPageFixture.content);
  });

  it('should update store after failed getProjects query', async () => {
    mockedAxiosClient.onGet(apiPath.getProjects()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getProjects());
    const { data, loading, error } = store.getState().projects;

    expect(action.type).toBe('project/getProjects/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch projects: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getProjects query', async () => {
    mockedAxiosClient.onGet(apiPath.getProjects()).reply(HttpStatusCode.Ok, projectPageFixture);

    const actionPromise = store.dispatch(getProjects());

    const { data, loading } = store.getState().projects;
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    actionPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } = store.getState().projects;

      expect(dataPromiseFulfilled).toEqual(projectPageFixture.content);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

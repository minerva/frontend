import { Project } from '@/types/models';
import { FetchDataState } from '@/types/fetchDataState';

export type ProjectsState = FetchDataState<Project[], []>;

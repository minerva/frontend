import { rootSelector } from '@/redux/root/root.selectors';
import { createSelector } from '@reduxjs/toolkit';

export const projectsRootSelector = createSelector(rootSelector, state => state.projects);
export const projectsSelector = createSelector(projectsRootSelector, state => state.data);

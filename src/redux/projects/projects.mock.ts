import { DEFAULT_ERROR } from '@/constants/errors';
import { ProjectsState } from '@/redux/projects/projects.types';

export const PROJECTS_STATE_INITIAL_MOCK: ProjectsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

import { Pixel } from 'ol/pixel';

export interface ContextMenuState {
  isOpen: boolean;
  coordinates: Pixel;
  uniprot: string;
  currentSelectedBioEntityId: number | string;
}

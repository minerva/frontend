import { ContextMenuState } from './contextMenu.types';

export const CONTEXT_MENU_INITIAL_STATE: ContextMenuState = {
  isOpen: false,
  coordinates: [],
  uniprot: '',
  currentSelectedBioEntityId: 0,
};

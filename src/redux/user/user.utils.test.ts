import { hasPrivilege } from './user.utils';

describe('hasPrivilege util', () => {
  const mockPrivileges = [
    { privilegeType: 'read', objectId: null },
    { privilegeType: 'write', objectId: '123' },
    { privilegeType: 'delete', objectId: null },
  ];

  it('should return true if the user has the specified privilege', () => {
    expect(hasPrivilege(mockPrivileges, 'read')).toBe(true);
    expect(hasPrivilege(mockPrivileges, 'write')).toBe(true);
    expect(hasPrivilege(mockPrivileges, 'delete')).toBe(true);
  });

  it('should return false if the user does not have the specified privilege', () => {
    expect(hasPrivilege(mockPrivileges, 'update')).toBe(false);
    expect(hasPrivilege([], 'read')).toBe(false);
  });
});

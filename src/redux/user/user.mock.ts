import { UserState } from './user.types';

export const USER_INITIAL_STATE_MOCK: UserState = {
  loading: 'idle',
  authenticated: false,
  error: { name: '', message: '' },
  login: null,
  role: null,
  userData: null,
  token: null,
};

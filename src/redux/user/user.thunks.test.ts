import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { loginFixture } from '@/models/fixtures/loginFixture';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { showToast } from '@/utils/showToast';
import { apiPath } from '../apiPath';
import { closeModal } from '../modal/modal.slice';
import userReducer from './user.slice';
import { UserState } from './user.types';
import { login } from './user.thunks';

jest.mock('../../utils/showToast');

const mockedAxiosClient = mockNetworkResponse();
const CREDENTIALS = {
  login: 'test',
  password: 'password',
};

describe('login thunk', () => {
  let store = {} as ToolkitStoreWithSingleSlice<UserState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('user', userReducer);
  });

  it('dispatches closeModal action on successful login with valid data', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);

    const mockDispatch = jest.fn(action => {
      if (action.type === closeModal.type) {
        expect(action).toEqual(closeModal());
      }
    });

    store.dispatch = mockDispatch;

    await store.dispatch(login(CREDENTIALS));
  });

  it('does not dispatch closeModal action on failed login with invalid data', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.NotFound, loginFixture);

    const mockDispatch = jest.fn(action => {
      if (action.type === closeModal.type) {
        expect(action).not.toEqual(closeModal());
      }
    });

    store.dispatch = mockDispatch;

    await store.dispatch(login(CREDENTIALS));
  });

  it('dispatch showToast on failed login with invalid data', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Unauthorized, loginFixture);

    await store.dispatch(login(CREDENTIALS));

    expect(showToast).toHaveBeenCalledWith({
      message: 'Invalid credentials.',
      type: 'error',
    });
  });
});

import { Loading } from '@/types/loadingState';
import { User } from '@/types/models';

export type UserState = {
  loading: Loading;
  error: Error;
  authenticated: boolean;
  login: null | string;
  role: string | null;
  userData: User | null;
  token: string | null;
};

import { login, getSessionValid } from '@/redux/user/user.thunks';
import type { UserState } from '@/redux/user/user.types';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { loginFixture } from '@/models/fixtures/loginFixture';
import { sessionFixture } from '@/models/fixtures/sessionFixture';
import { userFixture } from '@/models/fixtures/userFixture';
import { apiPath } from '../apiPath';
import userReducer from './user.slice';

const mockedAxiosClient = mockNetworkResponse();

const CREDENTIALS = {
  login: 'test',
  password: 'password',
};

const INITIAL_STATE: UserState = {
  loading: 'idle',
  authenticated: false,
  error: { name: '', message: '' },
  login: null,
  role: null,
  userData: null,
  token: null,
};

describe('user reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<UserState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('user', userReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };
    expect(userReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful login query', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    await store.dispatch(login(CREDENTIALS));
    const { authenticated, loading } = store.getState().user;

    expect(authenticated).toBe(true);
    expect(loading).toEqual('succeeded');
  });

  it('should update store on loading login query', async () => {
    mockedAxiosClient.onPost(apiPath.postLogin()).reply(HttpStatusCode.Ok, loginFixture);
    mockedAxiosClient.onGet(apiPath.user(loginFixture.login)).reply(HttpStatusCode.Ok, userFixture);
    const loginPromise = store.dispatch(login(CREDENTIALS));

    const { authenticated, loading } = store.getState().user;
    expect(authenticated).toBe(false);
    expect(loading).toEqual('pending');

    await loginPromise;

    const { authenticated: authenticatedFulfilled, loading: promiseFulfilled } =
      store.getState().user;

    expect(promiseFulfilled).toEqual('succeeded');
    expect(authenticatedFulfilled).toBe(true);
  });

  it('should update store after successful getSessionValid query', async () => {
    mockedAxiosClient.onGet(apiPath.getSessionValid()).reply(HttpStatusCode.Ok, sessionFixture);
    mockedAxiosClient
      .onGet(apiPath.user(sessionFixture.login))
      .reply(HttpStatusCode.Ok, userFixture);

    mockedAxiosClient
      .onGet(apiPath.user(sessionFixture.login))
      .reply(HttpStatusCode.Ok, userFixture);

    await store.dispatch(getSessionValid());
    const { authenticated, loading, login: sessionLogin, userData } = store.getState().user;

    expect(loading).toEqual('succeeded');
    expect(authenticated).toBe(true);
    expect(sessionLogin).toBeDefined();
    expect(userData).toBeDefined();
  });
});

import { createSelector } from '@reduxjs/toolkit';
import { rootSelector } from '../root/root.selectors';

export const oauthSelector = createSelector(rootSelector, state => state.oauth);

export const orcidEndpointSelector = createSelector(oauthSelector, oauth => oauth?.orcidEndpoint);

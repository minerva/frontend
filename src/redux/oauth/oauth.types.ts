import { OAuth } from '@/types/models';
import { Loading } from '@/types/loadingState';

export type OauthState = {
  orcidEndpoint: string | undefined;
  data: OAuth | undefined;
  loading: Loading;
  error: Error;
};

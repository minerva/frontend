import { DEFAULT_ERROR } from '@/constants/errors';
import { OauthState } from '@/redux/oauth/oauth.types';
import { initialOAuthFixture } from '@/redux/oauth/oauth.fixtures';

export const OAUTH_INITIAL_STATE_MOCK: OauthState = {
  data: initialOAuthFixture,
  loading: 'idle',
  error: DEFAULT_ERROR,
  orcidEndpoint: undefined,
};

/* eslint-disable no-magic-numbers */
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { LayersState } from '@/redux/layers/layers.types';
import {
  addLayerForModel,
  addLayerImageObject,
  addLayerText,
  getLayer,
  getLayersForModel,
  removeLayer,
  removeLayerImage,
  updateLayer,
  updateLayerImageObject,
} from '@/redux/layers/layers.thunks';
import { layersFixture } from '@/models/fixtures/layersFixture';
import { layerTextsFixture } from '@/models/fixtures/layerTextsFixture';
import { layerRectsFixture } from '@/models/fixtures/layerRectsFixture';
import { layerOvalsFixture } from '@/models/fixtures/layerOvalsFixture';
import { layerLinesFixture } from '@/models/fixtures/layerLinesFixture';
import { layerFixture } from '@/models/fixtures/layerFixture';
import { layerImagesFixture } from '@/models/fixtures/layerImagesFixture';
import { layerImageFixture } from '@/models/fixtures/layerImageFixture';
import { layerTextFixture } from '@/models/fixtures/layerTextFixture';
import { BLACK_COLOR } from '@/components/Map/MapViewer/MapViewer.constants';
import layersReducer from './layers.slice';

const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('layers thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<LayersState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('layers', layersReducer);
  });

  describe('getLayersForModel', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getLayers(1)).reply(HttpStatusCode.Ok, layersFixture);
      mockedAxiosClient
        .onGet(apiPath.getLayerTexts(1, layersFixture.content[0].id))
        .reply(HttpStatusCode.Ok, layerTextsFixture);
      mockedAxiosClient
        .onGet(apiPath.getLayerRects(1, layersFixture.content[0].id))
        .reply(HttpStatusCode.Ok, layerRectsFixture);
      mockedAxiosClient
        .onGet(apiPath.getLayerOvals(1, layersFixture.content[0].id))
        .reply(HttpStatusCode.Ok, layerOvalsFixture);
      mockedAxiosClient
        .onGet(apiPath.getLayerLines(1, layersFixture.content[0].id))
        .reply(HttpStatusCode.Ok, layerLinesFixture);
      mockedAxiosClient
        .onGet(apiPath.getLayerImages(1, layersFixture.content[0].id))
        .reply(HttpStatusCode.Ok, layerImagesFixture);

      const { payload } = await store.dispatch(getLayersForModel(1));
      const activeLayers = layersFixture.content
        .filter(layer => !layer.locked)
        .map(layer => layer.id);
      expect(payload).toEqual({
        activeLayers,
        drawLayer: null,
        layers: {
          [layersFixture.content[0].id]: {
            details: layersFixture.content[0],
            texts: {
              [layerTextsFixture.content[0].id]: layerTextsFixture.content[0],
            },
            rects: {
              [layerRectsFixture.content[0].id]: layerRectsFixture.content[0],
            },
            ovals: {
              [layerOvalsFixture.content[0].id]: layerOvalsFixture.content[0],
            },
            lines: {
              [layerLinesFixture.content[0].id]: layerLinesFixture.content[0],
            },
            images: {
              [layerImagesFixture.content[0].id]: layerImagesFixture.content[0],
            },
          },
        },
        layersVisibility: {
          [layersFixture.content[0].id]: layersFixture.content[0].visible,
        },
      });
    });

    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getLayers(1))
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getLayersForModel(1));
      expect(payload).toEqual(undefined);
    });
  });

  describe('getLayer', () => {
    it('should return a layer when data is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getLayer(1, 2)).reply(HttpStatusCode.Ok, layerFixture);

      const { payload } = await store.dispatch(getLayer({ modelId: 1, layerId: 2 }));
      expect(payload).toEqual(layerFixture);
    });

    it('should return null when data is invalid', async () => {
      mockedAxiosClient.onGet(apiPath.getLayer(1, 2)).reply(HttpStatusCode.Ok, { invalid: 'data' });

      const { payload } = await store.dispatch(getLayer({ modelId: 1, layerId: 2 }));
      expect(payload).toBeNull();
    });
  });

  describe('addLayerForModel', () => {
    it('should add a layer when data is valid', async () => {
      mockedAxiosClient.onPost(apiPath.storeLayer(1)).reply(HttpStatusCode.Created, layerFixture);

      const { payload } = await store.dispatch(
        addLayerForModel({
          name: 'New Layer',
          visible: true,
          locked: false,
          modelId: 1,
          zIndex: 1,
        }),
      );
      expect(payload).toEqual(layerFixture);
    });

    it('should return null when response data is invalid', async () => {
      mockedAxiosClient
        .onPost(apiPath.storeLayer(1))
        .reply(HttpStatusCode.Created, { invalid: 'data' });

      const { payload } = await store.dispatch(
        addLayerForModel({
          name: 'New Layer',
          visible: true,
          locked: false,
          modelId: 1,
          zIndex: 1,
        }),
      );
      expect(payload).toBeNull();
    });
  });

  describe('updateLayer', () => {
    it('should update a layer successfully', async () => {
      mockedAxiosClient.onPut(apiPath.updateLayer(1, 2)).reply(HttpStatusCode.Ok, layerFixture);

      const { payload } = await store.dispatch(
        updateLayer({
          name: 'Updated Layer',
          visible: false,
          locked: true,
          modelId: 1,
          layerId: 2,
          zIndex: 1,
        }),
      );
      expect(payload).toEqual(layerFixture);
    });

    it('should return null for invalid data', async () => {
      mockedAxiosClient
        .onPut(apiPath.updateLayer(1, 2))
        .reply(HttpStatusCode.Ok, { invalid: 'data' });

      const { payload } = await store.dispatch(
        updateLayer({
          name: 'Updated Layer',
          visible: false,
          locked: true,
          modelId: 1,
          layerId: 2,
          zIndex: 1,
        }),
      );
      expect(payload).toBeNull();
    });
  });

  describe('removeLayer', () => {
    it('should successfully remove a layer', async () => {
      mockedAxiosClient.onDelete(apiPath.removeLayer(1, 2)).reply(HttpStatusCode.NoContent);

      const result = await store.dispatch(removeLayer({ modelId: 1, layerId: 2 }));
      expect(result.meta.requestStatus).toBe('fulfilled');
    });
  });

  describe('addLayerImageObject', () => {
    it('should successfully add a new layer image object', async () => {
      mockedAxiosClient
        .onPost(apiPath.addLayerImageObject(1, 2))
        .reply(HttpStatusCode.Ok, layerImageFixture);

      const result = await store.dispatch(
        addLayerImageObject({
          modelId: 1,
          layerId: 2,
          x: 1,
          y: 1,
          z: 1,
          width: 1,
          height: 1,
          glyph: 1,
        }),
      );
      expect(result.meta.requestStatus).toBe('fulfilled');
      expect(result.payload).toEqual(layerImageFixture);
    });

    it('should return null when response data is invalid', async () => {
      mockedAxiosClient
        .onPost(apiPath.addLayerImageObject(1, 2))
        .reply(HttpStatusCode.Ok, { invalid: 'data' });

      const result = await store.dispatch(
        addLayerImageObject({
          modelId: 1,
          layerId: 2,
          x: 1,
          y: 1,
          z: 1,
          width: 1,
          height: 1,
          glyph: 1,
        }),
      );

      expect(result.payload).toBeNull();
    });
  });

  describe('updateLayerImageObject', () => {
    it('should successfully update the layer image object', async () => {
      mockedAxiosClient
        .onPut(apiPath.updateLayerImageObject(1, 2, 1))
        .reply(HttpStatusCode.Ok, layerImageFixture);

      const result = await store.dispatch(
        updateLayerImageObject({
          modelId: 1,
          layerId: 2,
          id: 1,
          x: 1,
          y: 1,
          z: 1,
          width: 1,
          height: 1,
          glyph: 1,
        }),
      );
      expect(result.meta.requestStatus).toBe('fulfilled');
      expect(result.payload).toEqual(layerImageFixture);
    });

    it('should return null when response data is invalid', async () => {
      mockedAxiosClient
        .onPut(apiPath.updateLayerImageObject(1, 2, 1))
        .reply(HttpStatusCode.Ok, { invalid: 'data' });

      const result = await store.dispatch(
        updateLayerImageObject({
          modelId: 1,
          layerId: 2,
          id: 1,
          x: 1,
          y: 1,
          z: 1,
          width: 1,
          height: 1,
          glyph: 1,
        }),
      );

      expect(result.payload).toBeNull();
    });
  });

  describe('removeLayerImage', () => {
    it('should successfully remove the layer image', async () => {
      mockedAxiosClient
        .onDelete(apiPath.removeLayerImageObject(1, 2, 1))
        .reply(HttpStatusCode.NoContent);

      const result = await store.dispatch(removeLayerImage({ modelId: 1, layerId: 2, imageId: 1 }));
      expect(result.meta.requestStatus).toBe('fulfilled');
    });
  });

  describe('addLayerText', () => {
    it('should successfully add a new layer text', async () => {
      mockedAxiosClient
        .onPost(apiPath.addLayerText(1, 2))
        .reply(HttpStatusCode.Ok, layerTextFixture);

      const result = await store.dispatch(
        addLayerText({
          modelId: 1,
          layerId: 2,
          z: 1,
          boundingBox: {
            width: 1,
            height: 1,
            x: 1,
            y: 1,
          },
          textData: {
            notes: '123',
            fontSize: 12,
            horizontalAlign: 'LEFT',
            verticalAlign: 'TOP',
            color: BLACK_COLOR,
            borderColor: BLACK_COLOR,
          },
        }),
      );
      expect(result.meta.requestStatus).toBe('fulfilled');
      expect(result.payload).toEqual(layerTextFixture);
    });

    it('should return null when response data is invalid', async () => {
      mockedAxiosClient
        .onPost(apiPath.addLayerText(1, 2))
        .reply(HttpStatusCode.Ok, { invalid: 'data' });

      const result = await store.dispatch(
        addLayerText({
          modelId: 1,
          layerId: 2,
          z: 1,
          boundingBox: {
            width: 1,
            height: 1,
            x: 1,
            y: 1,
          },
          textData: {
            notes: '123',
            fontSize: 12,
            horizontalAlign: 'LEFT',
            verticalAlign: 'TOP',
            color: BLACK_COLOR,
            borderColor: BLACK_COLOR,
          },
        }),
      );

      expect(result.payload).toBeNull();
    });
  });
});

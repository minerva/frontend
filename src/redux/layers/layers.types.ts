import { KeyedFetchDataState } from '@/types/fetchDataState';
import { Layer, LayerImage, LayerLine, LayerOval, LayerRect, LayerText } from '@/types/models';

export interface LayerStoreInterface {
  name: string;
  visible: boolean;
  locked: boolean;
  modelId: number;
  zIndex: number;
}

export interface LayerUpdateInterface {
  layerId: number;
  name: string;
  visible: boolean;
  locked: boolean;
  modelId: number;
  zIndex: number;
}

export type LayerState = {
  details: Layer;
  texts: { [key: string]: LayerText };
  rects: { [key: string]: LayerRect };
  ovals: { [key: string]: LayerOval };
  lines: { [key: string]: LayerLine };
  images: { [key: string]: LayerImage };
};

export type LayersDictState = {
  [key: number]: LayerState;
};

export type LayerVisibilityState = {
  [key: number]: boolean;
};

export type LayersVisibilitiesState = {
  layersVisibility: LayerVisibilityState;
  layers: LayersDictState;
  activeLayers: Array<number>;
  drawLayer: number | null;
};

export type LayersState = KeyedFetchDataState<LayersVisibilitiesState>;

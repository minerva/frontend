/* eslint-disable no-magic-numbers */
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import modelElementsReducer from '@/redux/modelElements/modelElements.slice';
import {
  getModelElementsForModel,
  searchModelElement,
} from '@/redux/modelElements/modelElements.thunks';
import { modelElementsFixture } from '@/models/fixtures/modelElementsFixture';
import { bioEntityResponseFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { DEFAULT_ERROR } from '@/constants/errors';
import { MODEL_ELEMENTS_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'park7';

describe('model elements reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModelElementsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('modelElements', modelElementsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(modelElementsReducer(undefined, action)).toEqual(MODEL_ELEMENTS_INITIAL_STATE_MOCK);
  });

  it('should update store after successful getModelElementsForModel query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getModelElements(0))
      .reply(HttpStatusCode.Ok, modelElementsFixture);

    const { type } = await store.dispatch(getModelElementsForModel(0));
    const { data, loading, error } = store.getState().modelElements.data[0];

    expect(type).toBe('modelElements/getModelElementsForModel/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(modelElementsFixture.content);
  });

  it('should update store after failed getModelElementsForModel query', async () => {
    mockedAxiosClient.onGet(apiPath.getModelElements(0)).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getModelElementsForModel(0));
    const { data, loading, error } = store.getState().modelElements.data[0];

    expect(action.type).toBe('modelElements/getModelElementsForModel/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch model elements: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getModelElementsForModel query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getModelElements(0))
      .reply(HttpStatusCode.Ok, modelElementsFixture);

    const modelElementsPromise = store.dispatch(getModelElementsForModel(0));

    const { data, loading } = store.getState().modelElements.data[0];
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    modelElementsPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } =
        store.getState().modelElements.data[0];

      expect(dataPromiseFulfilled).toEqual(modelElementsFixture.content);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });

  it('should update store after succesfull searchModelElement query', async () => {
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

    const { type } = await store.dispatch(
      searchModelElement({
        searchQuery: SEARCH_QUERY,
        isPerfectMatch: false,
      }),
    );
    const { search } = store.getState().modelElements;
    const modelElementWithSearchQuery = search.data.find(
      modelElement => modelElement.searchQueryElement === SEARCH_QUERY,
    );

    expect(type).toBe('modelElements/searchModelElement/fulfilled');
    expect(modelElementWithSearchQuery).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: bioEntityResponseFixture.content.map(data => ({
        modelElement: data.bioEntity,
        perfect: data.perfect,
      })),
      loading: 'succeeded',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store after failed getBioEntity query', async () => {
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.NotFound, bioEntityResponseFixture);

    const action = await store.dispatch(
      searchModelElement({
        searchQuery: SEARCH_QUERY,
        isPerfectMatch: false,
      }),
    );
    const { search } = store.getState().modelElements;

    const modelElementWithSearchQuery = search.data.find(
      modelElement => modelElement.searchQueryElement === SEARCH_QUERY,
    );
    expect(action.type).toBe('modelElements/searchModelElement/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to search model element: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(modelElementWithSearchQuery).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'failed',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store on loading getBioEntity query', async () => {
    mockedAxiosClient
      .onGet(
        apiPath.getBioEntityContentsStringWithQuery({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      )
      .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

    const searchModelElementPromise = store.dispatch(
      searchModelElement({
        searchQuery: SEARCH_QUERY,
        isPerfectMatch: false,
      }),
    );

    const { search } = store.getState().modelElements;
    const modelElementWithSearchQuery = search.data.find(
      modelElement => modelElement.searchQueryElement === SEARCH_QUERY,
    );

    expect(modelElementWithSearchQuery).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'pending',
      error: DEFAULT_ERROR,
    });

    searchModelElementPromise.then(() => {
      const { search: searchPromiseFulfilled } = store.getState().modelElements;
      const modelElementWithSearchQueryFulfilled = searchPromiseFulfilled.data.find(
        modelElement => modelElement.searchQueryElement === SEARCH_QUERY,
      );

      expect(modelElementWithSearchQueryFulfilled).toEqual({
        searchQueryElement: SEARCH_QUERY,
        data: bioEntityResponseFixture.content.map(data => ({
          modelElement: data.bioEntity,
          perfect: data.perfect,
        })),
        loading: 'succeeded',
        error: DEFAULT_ERROR,
      });
    });
  });
});

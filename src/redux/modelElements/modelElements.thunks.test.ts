/* eslint-disable no-magic-numbers */
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { ModelElementsState } from '@/redux/modelElements/modelElements.types';
import modelElementsReducer from '@/redux/modelElements/modelElements.slice';
import {
  getModelElementsForModel,
  searchModelElement,
  searchMultiModelElements,
} from '@/redux/modelElements/modelElements.thunks';
import { modelElementsFixture } from '@/models/fixtures/modelElementsFixture';
import { bioEntityResponseFixture } from '@/models/fixtures/bioEntityContentsFixture';
import { unwrapResult } from '@reduxjs/toolkit';

const SEARCH_QUERY = 'park7';
const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('model elements thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModelElementsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('modelElements', modelElementsReducer);
  });

  describe('getModelElementsForModel', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient
        .onGet(apiPath.getModelElements(0))
        .reply(HttpStatusCode.Ok, modelElementsFixture);

      const { payload } = await store.dispatch(getModelElementsForModel(0));
      expect(payload).toEqual(modelElementsFixture.content);
    });

    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getModelElements(0))
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getModelElementsForModel(0));
      expect(payload).toEqual(undefined);
    });
  });

  describe('searchMultiModelElements', () => {
    it('should return transformed bioEntityContent array', async () => {
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

      const data = await store
        .dispatch(
          searchMultiModelElements({
            searchQueries: [SEARCH_QUERY],
            isPerfectMatch: false,
          }),
        )
        .unwrap();

      expect(data).toEqual(bioEntityResponseFixture.content);
    });

    it('should combine all returned bioEntityContent arrays and return array with all provided bioEntityContent elements', async () => {
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

      const data = await store
        .dispatch(
          searchMultiModelElements({
            searchQueries: [SEARCH_QUERY, SEARCH_QUERY],
            isPerfectMatch: false,
          }),
        )
        .unwrap();

      expect(data).toEqual([
        ...bioEntityResponseFixture.content,
        ...bioEntityResponseFixture.content,
      ]);
    });
  });

  describe('searchModelElement', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, bioEntityResponseFixture);

      const { payload } = await store.dispatch(
        searchModelElement({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      );
      expect(payload).toEqual(bioEntityResponseFixture.content);
    });
    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(
        searchModelElement({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      );
      expect(payload).toEqual(undefined);
    });
    it('should handle error message when getBioEntityContents failed', async () => {
      mockedAxiosClient
        .onGet(
          apiPath.getBioEntityContentsStringWithQuery({
            searchQuery: SEARCH_QUERY,
            isPerfectMatch: false,
          }),
        )
        .reply(HttpStatusCode.NotFound, null);

      const action = await store.dispatch(
        searchModelElement({
          searchQuery: SEARCH_QUERY,
          isPerfectMatch: false,
        }),
      );
      expect(() => unwrapResult(action)).toThrow(
        "Failed to search model element: The page you're looking for doesn't exist. Please verify the URL and try again.",
      );
    });
  });
});

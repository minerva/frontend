import { KeyedFetchDataState, MultiFetchDataState } from '@/types/fetchDataState';
import { BioEntityContent, ModelElement } from '@/types/models';
import { PerfectMultiSearchParams, PerfectSearchParams } from '@/types/search';
import { PayloadAction } from '@reduxjs/toolkit';

export type SearchModelElementDataState = {
  perfect: boolean;
  modelElement: ModelElement;
};

export type ModelElementsState = {
  data: KeyedFetchDataState<ModelElement[]>;
  search: MultiFetchDataState<SearchModelElementDataState[]>;
};

export type SearchMultiModelElementsProps = PerfectMultiSearchParams;

export type SearchMultiModelElementsActions = PayloadAction<
  BioEntityContent[] | undefined | string
>[];

export type SearchModelElementProps = PerfectSearchParams;

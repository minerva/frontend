export const MODEL_ELEMENTS_FETCHING_ERROR_PREFIX = 'Failed to fetch model elements';

export const MODEL_ELEMENTS_DEFAULT_COMPARTMENT_NAME = 'default';

export const MULTI_MODEL_ELEMENTS_SEARCH_ERROR_PREFIX = 'Failed to search multi model elements';

export const MODEL_ELEMENT_SEARCH_ERROR_PREFIX = 'Failed to search model element';

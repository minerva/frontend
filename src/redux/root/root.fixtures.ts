import { CONSTANT_INITIAL_STATE } from '@/redux/constant/constant.adapter';
import { PROJECTS_STATE_INITIAL_MOCK } from '@/redux/projects/projects.mock';
import { OAUTH_INITIAL_STATE_MOCK } from '@/redux/oauth/oauth.mock';
import { COMMENT_INITIAL_STATE_MOCK } from '@/redux/comment/comment.mock';
import { AUTOCOMPLETE_INITIAL_STATE } from '@/redux/autocomplete/autocomplete.constants';
import { SHAPES_STATE_INITIAL_MOCK } from '@/redux/shapes/shapes.mock';
import { MODEL_ELEMENTS_INITIAL_STATE_MOCK } from '@/redux/modelElements/modelElements.mock';
import { LAYERS_STATE_INITIAL_MOCK } from '@/redux/layers/layers.mock';
import { NEW_REACTIONS_INITIAL_STATE_MOCK } from '@/redux/newReactions/newReactions.mock';
import { GLYPHS_STATE_INITIAL_MOCK } from '@/redux/glyphs/glyphs.mock';
import { MAP_EDIT_TOOLS_STATE_INITIAL_MOCK } from '@/redux/mapEditTools/mapEditTools.mock';
import { OVERLAY_GROUPS_STATE_INITIAL_MOCK } from '@/redux/overlayGroup/ovelayGroup.mock';
import { BIOENTITY_INITIAL_STATE_MOCK } from '../bioEntity/bioEntity.mock';
import { CHEMICALS_INITIAL_STATE_MOCK } from '../chemicals/chemicals.mock';
import { CONFIGURATION_INITIAL_STATE } from '../configuration/configuration.adapter';
import { CONTEXT_MENU_INITIAL_STATE } from '../contextMenu/contextMenu.constants';
import { COOKIE_BANNER_INITIAL_STATE_MOCK } from '../cookieBanner/cookieBanner.mock';
import { initialStateFixture as drawerInitialStateMock } from '../drawer/drawerFixture';
import { DRUGS_INITIAL_STATE_MOCK } from '../drugs/drugs.mock';
import { ENTITY_NUMBER_INITIAL_STATE_MOCK } from '../entityNumber/entityNumber.mock';
import { EXPORT_INITIAL_STATE_MOCK } from '../export/export.mock';
import { LEGEND_INITIAL_STATE_MOCK } from '../legend/legend.mock';
import { initialMapStateFixture } from '../map/map.fixtures';
import { MARKERS_INITIAL_STATE_MOCK } from '../markers/markers.mock';
import { MODAL_INITIAL_STATE_MOCK } from '../modal/modal.mock';
import { MODELS_INITIAL_STATE_MOCK } from '../models/models.mock';
import { OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK } from '../overlayBioEntity/overlayBioEntity.mock';
import { OVERLAYS_INITIAL_STATE_MOCK } from '../overlays/overlays.mock';
import { PLUGINS_INITIAL_STATE_MOCK } from '../plugins/plugins.mock';
import { PROJECT_STATE_INITIAL_MOCK } from '../project/project.mock';
import { PUBLICATIONS_INITIAL_STATE_MOCK } from '../publications/publications.mock';
import { REACTIONS_STATE_INITIAL_MOCK } from '../reactions/reactions.mock';
import { SEARCH_STATE_INITIAL_MOCK } from '../search/search.mock';
import { STATISTICS_STATE_INITIAL_MOCK } from '../statistics/statistics.mock';
import { RootState } from '../store';
import { USER_INITIAL_STATE_MOCK } from '../user/user.mock';

export const INITIAL_STORE_STATE_MOCK: RootState = {
  autocompleteSearch: AUTOCOMPLETE_INITIAL_STATE,
  autocompleteDrug: AUTOCOMPLETE_INITIAL_STATE,
  autocompleteChemical: AUTOCOMPLETE_INITIAL_STATE,
  search: SEARCH_STATE_INITIAL_MOCK,
  project: PROJECT_STATE_INITIAL_MOCK,
  shapes: SHAPES_STATE_INITIAL_MOCK,
  glyphs: GLYPHS_STATE_INITIAL_MOCK,
  projects: PROJECTS_STATE_INITIAL_MOCK,
  overlayGroups: OVERLAY_GROUPS_STATE_INITIAL_MOCK,
  drugs: DRUGS_INITIAL_STATE_MOCK,
  chemicals: CHEMICALS_INITIAL_STATE_MOCK,
  models: MODELS_INITIAL_STATE_MOCK,
  modelElements: MODEL_ELEMENTS_INITIAL_STATE_MOCK,
  layers: LAYERS_STATE_INITIAL_MOCK,
  bioEntity: BIOENTITY_INITIAL_STATE_MOCK,
  drawer: drawerInitialStateMock,
  map: initialMapStateFixture,
  oauth: OAUTH_INITIAL_STATE_MOCK,
  overlays: OVERLAYS_INITIAL_STATE_MOCK,
  reactions: REACTIONS_STATE_INITIAL_MOCK,
  newReactions: NEW_REACTIONS_INITIAL_STATE_MOCK,
  configuration: CONFIGURATION_INITIAL_STATE,
  constant: CONSTANT_INITIAL_STATE,
  overlayBioEntity: OVERLAY_BIO_ENTITY_INITIAL_STATE_MOCK,
  modal: MODAL_INITIAL_STATE_MOCK,
  contextMenu: CONTEXT_MENU_INITIAL_STATE,
  cookieBanner: COOKIE_BANNER_INITIAL_STATE_MOCK,
  user: USER_INITIAL_STATE_MOCK,
  legend: LEGEND_INITIAL_STATE_MOCK,
  statistics: STATISTICS_STATE_INITIAL_MOCK,
  publications: PUBLICATIONS_INITIAL_STATE_MOCK,
  export: EXPORT_INITIAL_STATE_MOCK,
  plugins: PLUGINS_INITIAL_STATE_MOCK,
  markers: MARKERS_INITIAL_STATE_MOCK,
  entityNumber: ENTITY_NUMBER_INITIAL_STATE_MOCK,
  comment: COMMENT_INITIAL_STATE_MOCK,
  mapEditTools: MAP_EDIT_TOOLS_STATE_INITIAL_MOCK,
};

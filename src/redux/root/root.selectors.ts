import type { RootState } from '@/redux/store';

export const rootSelector = (state: RootState): RootState => state;

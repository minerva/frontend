import { FetchDataState } from '@/types/fetchDataState';
import { Configuration } from '@/types/models';
import {
  CELL_DESIGNER_SBML_HANDLER_NAME_ID,
  GPML_HANDLER_NAME_ID,
  PDF_HANDLER_NAME_ID,
  PNG_IMAGE_HANDLER_NAME_ID,
  SBGN_ML_HANDLER_NAME_ID,
  SBML_HANDLER_NAME_ID,
  SVG_IMAGE_HANDLER_NAME_ID,
} from './configuration.constants';

export type ConfigurationMainState = FetchDataState<Configuration>;

export interface ConfigurationHandlersIds {
  [GPML_HANDLER_NAME_ID]?: string;
  [SBML_HANDLER_NAME_ID]?: string;
  [CELL_DESIGNER_SBML_HANDLER_NAME_ID]?: string;
  [SBGN_ML_HANDLER_NAME_ID]?: string;
}

export interface ConfigurationImageHandlersIds {
  [PNG_IMAGE_HANDLER_NAME_ID]?: string;
  [PDF_HANDLER_NAME_ID]?: string;
  [SVG_IMAGE_HANDLER_NAME_ID]?: string;
}

/* eslint-disable no-magic-numbers */
import { DEFAULT_ERROR } from '@/constants/errors';
import {
  CONFIGURATION_OPTIONS_COLOURS_MOCK,
  CONFIGURATION_OPTIONS_TYPES_MOCK,
} from '@/models/mocks/configurationOptionMock';
import { ConfigurationState } from './configuration.adapter';

export const CONFIGURATION_INITIAL_STORE_MOCK: ConfigurationState = {
  options: {
    ids: [],
    entities: {},
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  main: {
    data: undefined,
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
};

/** IMPORTANT MOCK IDS MUST MATCH KEYS IN ENTITIES  */
export const CONFIGURATION_INITIAL_STORE_MOCKS: ConfigurationState = {
  options: {
    ids: CONFIGURATION_OPTIONS_TYPES_MOCK,
    entities: {
      [CONFIGURATION_OPTIONS_TYPES_MOCK[0]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[0],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[1]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[1],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[2]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[2],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[3]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[3],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[4]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[4],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[5]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[5],
      [CONFIGURATION_OPTIONS_TYPES_MOCK[6]]: CONFIGURATION_OPTIONS_COLOURS_MOCK[6],
    },
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
  main: {
    data: undefined,
    loading: 'idle',
    error: DEFAULT_ERROR,
  },
};

import { DEFAULT_ERROR } from '@/constants/errors';
import { OverlayGroupsState } from '@/redux/overlayGroup/overlayGroup.types';

export const OVERLAY_GROUPS_STATE_INITIAL_MOCK: OverlayGroupsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

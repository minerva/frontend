import { OverlayGroup } from '@/types/models';
import { FetchDataState } from '@/types/fetchDataState';

export type OverlayGroupsState = FetchDataState<OverlayGroup[], []>;

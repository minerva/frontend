import { PROJECT_ID } from '@/constants';
import { apiPath } from '@/redux/apiPath';

describe('api path', () => {
  it('should return url string for drugs', () => {
    expect(apiPath.getDrugsStringWithQuery('aspirin')).toBe(
      `projects/${PROJECT_ID}/drugs:search?query=aspirin`,
    );
  });

  it('should return url string for bio entity content', () => {
    expect(
      apiPath.getBioEntityContentsStringWithQuery({ searchQuery: 'park7', isPerfectMatch: false }),
    ).toBe(
      `projects/${PROJECT_ID}/models/*/bioEntities/:search?query=park7&size=1000&perfectMatch=false`,
    );

    expect(
      apiPath.getBioEntityContentsStringWithQuery({ searchQuery: 'park7', isPerfectMatch: true }),
    ).toBe(
      `projects/${PROJECT_ID}/models/*/bioEntities/:search?query=park7&size=1000&perfectMatch=true`,
    );
  });

  it('should return url string for bio entity content', () => {
    expect(apiPath.getChemicalsStringWithQuery('Corticosterone')).toBe(
      `projects/${PROJECT_ID}/chemicals:search?query=Corticosterone`,
    );
  });
});

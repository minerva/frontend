import { PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK } from '@/models/mocks/publicationsResponseMock';
import { PublicationsState } from './publications.types';

export const PUBLICATIONS_INITIAL_STATE_MOCK: PublicationsState = {
  loading: 'idle',
  data: undefined,
  error: { name: '', message: '' },
  sortColumn: '',
  sortOrder: 'asc',
  searchValue: '',
  selectedModelId: undefined,
};

export const PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_STATE_MOCK: PublicationsState = {
  data: PUBLICATIONS_DEFAULT_SEARCH_FIRST_10_MOCK,
  loading: 'idle',
  error: { name: '', message: '' },
  sortColumn: '',
  sortOrder: 'asc',
  searchValue: '',
  selectedModelId: undefined,
};

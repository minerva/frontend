import { FetchDataState } from '@/types/fetchDataState';
import { FilteredPageOf, Publication } from '@/types/models';

export type SortColumn = '' | 'pubmedId' | 'title' | 'authors' | 'journal' | 'year' | 'level';
export type SortOrder = 'asc' | 'desc';

export type PublicationsState = FetchDataState<FilteredPageOf<Publication>> & {
  sortColumn: SortColumn;
  sortOrder: SortOrder;
  selectedModelId?: string;
  searchValue: string;
};

export type PublicationsQueryParams = {
  page?: number;
  sortColumn?: string;
  sortOrder?: string;
  level?: number;
  length?: number;
  search?: string;
};

export type GetPublicationsParams = {
  params: PublicationsQueryParams;
  modelId?: string;
};

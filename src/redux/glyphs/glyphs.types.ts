import { FetchDataState } from '@/types/fetchDataState';
import { Glyph } from '@/types/models';

export type GlyphsState = FetchDataState<Glyph[], []>;

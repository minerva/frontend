import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { GlyphsState } from '@/redux/glyphs/glyphs.types';
import { getGlyphs } from '@/redux/glyphs/glyphs.thunks';
import { glyphsFixture } from '@/models/fixtures/glyphsFixture';
import glyphsReducer from './glyphs.slice';

const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('glyphs thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<GlyphsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('glyphs', glyphsReducer);
  });

  describe('getGlyphs', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getGlyphs()).reply(HttpStatusCode.Ok, glyphsFixture);

      const { payload } = await store.dispatch(getGlyphs());
      expect(payload).toEqual(glyphsFixture.content);
    });

    it('should return empty object when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getGlyphs())
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getGlyphs());
      expect(payload).toEqual([]);
    });
  });
});

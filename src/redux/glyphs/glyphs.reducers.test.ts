import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { GLYPHS_STATE_INITIAL_MOCK } from '@/redux/glyphs/glyphs.mock';
import { GlyphsState } from '@/redux/glyphs/glyphs.types';
import { getGlyphs } from '@/redux/glyphs/glyphs.thunks';
import { glyphsFixture } from '@/models/fixtures/glyphsFixture';
import glyphsReducer from './glyphs.slice';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: GlyphsState = GLYPHS_STATE_INITIAL_MOCK;

describe('glyphs reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<GlyphsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('glyphs', glyphsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(glyphsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful getGlyphs query', async () => {
    mockedAxiosClient.onGet(apiPath.getGlyphs()).reply(HttpStatusCode.Ok, glyphsFixture);

    const { type } = await store.dispatch(getGlyphs());
    const { data, loading, error } = store.getState().glyphs;
    expect(type).toBe('getGlyphs/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(glyphsFixture.content);
  });

  it('should update store after failed getGlyphs query', async () => {
    mockedAxiosClient.onGet(apiPath.getGlyphs()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getGlyphs());
    const { data, loading, error } = store.getState().glyphs;

    expect(action.type).toBe('getGlyphs/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch glyphs: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getGlyphs query', async () => {
    mockedAxiosClient.onGet(apiPath.getGlyphs()).reply(HttpStatusCode.Ok, glyphsFixture);

    const glyphsPromise = store.dispatch(getGlyphs());

    const { data, loading } = store.getState().glyphs;
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    glyphsPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } = store.getState().glyphs;
      expect(dataPromiseFulfilled).toEqual(glyphsFixture.content);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

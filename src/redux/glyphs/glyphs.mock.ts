import { DEFAULT_ERROR } from '@/constants/errors';
import { GlyphsState } from '@/redux/glyphs/glyphs.types';

export const GLYPHS_STATE_INITIAL_MOCK: GlyphsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

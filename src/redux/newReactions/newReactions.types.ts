import { KeyedFetchDataState } from '@/types/fetchDataState';
import { NewReaction } from '@/types/models';

export type NewReactionsState = KeyedFetchDataState<Array<NewReaction>>;

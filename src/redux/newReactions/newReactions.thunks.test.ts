/* eslint-disable no-magic-numbers */
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import newReactionsReducer from '@/redux/newReactions/newReactions.slice';
import { NewReactionsState } from '@/redux/newReactions/newReactions.types';
import { newReactionsFixture } from '@/models/fixtures/newReactionsFixture';
import { getNewReactionsForModel } from '@/redux/newReactions/newReactions.thunks';

const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('newReactions thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<NewReactionsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('newReactions', newReactionsReducer);
  });

  describe('getReactions', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient
        .onGet(apiPath.getNewReactionsForModel(1))
        .reply(HttpStatusCode.Ok, newReactionsFixture);

      const { payload } = await store.dispatch(getNewReactionsForModel(1));
      expect(payload).toEqual(newReactionsFixture.content);
    });

    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getNewReactionsForModel(1))
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getNewReactionsForModel(1));
      expect(payload).toEqual(undefined);
    });
  });
});

/* eslint-disable no-magic-numbers */
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import newReactionsReducer from '@/redux/newReactions/newReactions.slice';
import { NewReactionsState } from '@/redux/newReactions/newReactions.types';
import { NEW_REACTIONS_INITIAL_STATE_MOCK } from '@/redux/newReactions/newReactions.mock';
import { getNewReactionsForModel } from '@/redux/newReactions/newReactions.thunks';
import { newReactionsFixture } from '@/models/fixtures/newReactionsFixture';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: NewReactionsState = NEW_REACTIONS_INITIAL_STATE_MOCK;

describe('newReactions reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<NewReactionsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('newReactions', newReactionsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(newReactionsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful getNewReactionsForModel query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getNewReactionsForModel(1))
      .reply(HttpStatusCode.Ok, newReactionsFixture);

    const { type } = await store.dispatch(getNewReactionsForModel(1));
    const { data, loading, error } = store.getState().newReactions[1];
    expect(type).toBe('newReactions/getNewReactionsForModel/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(newReactionsFixture.content);
  });

  it('should update store after failed getNewReactionsForModel query', async () => {
    mockedAxiosClient.onGet(apiPath.getNewReactionsForModel(1)).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getNewReactionsForModel(1));
    const { data, loading, error } = store.getState().newReactions[1];

    expect(action.type).toBe('newReactions/getNewReactionsForModel/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch new reactions: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getNewReactionsForModel query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getNewReactionsForModel(1))
      .reply(HttpStatusCode.Ok, newReactionsFixture);

    const newReactionsPromise = store.dispatch(getNewReactionsForModel(1));

    const { data, loading } = store.getState().newReactions[1];
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    newReactionsPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } =
        store.getState().newReactions[1];

      expect(dataPromiseFulfilled).toEqual(newReactionsFixture.content);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

import { FetchDataState } from '@/types/fetchDataState';
import { ModelElement, Comment, NewReaction } from '@/types/models';
import { PayloadAction } from '@reduxjs/toolkit';
import { Point } from '@/types/map';

export interface CommentsState extends FetchDataState<Comment[], []> {
  isOpen: boolean;
  commentElement: ModelElement | null;
  commentReaction: NewReaction | null;
}

export type OpenCommentByIdPayload = number | string;
export type OpenCommentByIdAction = PayloadAction<OpenCommentByIdPayload>;

export type GetElementProps = {
  elementId: number;
  modelId: number;
};

export type AddCommentProps = {
  email: string;
  content: string;
  modelId: number;
  position: Point;
};

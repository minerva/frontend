import { DEFAULT_ERROR } from '@/constants/errors';
import { CommentsState } from '@/redux/comment/comment.types';

export const COMMENT_INITIAL_STATE_MOCK: CommentsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
  isOpen: false,
  commentElement: null,
  commentReaction: null,
};

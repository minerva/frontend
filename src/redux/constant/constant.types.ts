import { FetchDataState } from '@/types/fetchDataState';

export type ConstantType = {
  apiBaseUrl: string;
  apiDocsUrl: string;
};

export type ConstantMainState = FetchDataState<ConstantType>;

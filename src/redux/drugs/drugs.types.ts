import { MultiFetchDataState } from '@/types/fetchDataState';
import { Drug } from '@/types/models';

export type DrugsState = MultiFetchDataState<Drug[]>;

export const DRUGS_FETCHING_ERROR_PREFIX = 'Failed to fetch drugs';
export const MULTI_DRUGS_FETCHING_ERROR_PREFIX = 'Failed to fetch multi drugs';

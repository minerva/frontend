import { DEFAULT_ERROR } from '@/constants/errors';
import { DrugsState } from './drugs.types';

export const DRUGS_INITIAL_STATE_MOCK: DrugsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

import { DEFAULT_ERROR } from '@/constants/errors';
import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import drugsReducer from './drugs.slice';
import { getDrugs } from './drugs.thunks';
import { DrugsState } from './drugs.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'aspirin';

const INITIAL_STATE: DrugsState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
};

describe('drugs reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<DrugsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('drugs', drugsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(drugsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after succesfull getDrugs query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, drugsFixture);

    const { type } = await store.dispatch(getDrugs(SEARCH_QUERY));
    const { data } = store.getState().drugs;
    const drugsWithSearchElement = data.find(
      searchModelElement => searchModelElement.searchQueryElement === SEARCH_QUERY,
    );

    expect(type).toBe('project/getDrugs/fulfilled');
    expect(drugsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: drugsFixture,
      loading: 'succeeded',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store after failed getDrugs query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getDrugs(SEARCH_QUERY));
    const { data } = store.getState().drugs;
    const drugsWithSearchElement = data.find(
      searchModelElement => searchModelElement.searchQueryElement === SEARCH_QUERY,
    );

    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch drugs: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(action.type).toBe('project/getDrugs/rejected');
    expect(drugsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'failed',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store on loading getDrugs query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, drugsFixture);

    const drugsPromise = store.dispatch(getDrugs(SEARCH_QUERY));

    const { data } = store.getState().drugs;
    const drugsWithSearchElement = data.find(
      searchModelElement => searchModelElement.searchQueryElement === SEARCH_QUERY,
    );

    expect(drugsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'pending',
      error: DEFAULT_ERROR,
    });

    drugsPromise.then(() => {
      const { data: dataPromiseFulfilled } = store.getState().drugs;

      const drugsWithSearchElementFulfilled = dataPromiseFulfilled.find(
        searchModelElement => searchModelElement.searchQueryElement === SEARCH_QUERY,
      );
      expect(drugsWithSearchElementFulfilled).toEqual({
        searchQueryElement: SEARCH_QUERY,
        data: drugsFixture,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
      });
    });
  });
});

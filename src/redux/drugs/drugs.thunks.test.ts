import { drugsFixture } from '@/models/fixtures/drugFixtures';
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import drugsReducer from './drugs.slice';
import { getDrugs } from './drugs.thunks';
import { DrugsState } from './drugs.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'aspirin';

describe('drugs thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<DrugsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('drugs', drugsReducer);
  });
  describe('getDrugs', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient
        .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, drugsFixture);

      const { payload } = await store.dispatch(getDrugs(SEARCH_QUERY));
      expect(payload).toEqual(drugsFixture);
    });
    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getDrugsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getDrugs(SEARCH_QUERY));
      expect(payload).toEqual(undefined);
    });
  });
});

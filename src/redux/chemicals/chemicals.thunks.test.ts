import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import chemicalsReducer from './chemicals.slice';
import { getChemicals } from './chemicals.thunks';
import { ChemicalsState } from './chemicals.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'Corticosterone';

jest.mock('../../utils/error-report/errorReporting');

describe('chemicals thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ChemicalsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('chemicals', chemicalsReducer);
  });
  describe('getChemicals', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient
        .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, chemicalsFixture);

      const { payload } = await store.dispatch(getChemicals(SEARCH_QUERY));
      expect(payload).toEqual(chemicalsFixture);
    });
    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getChemicals(SEARCH_QUERY));
      expect(payload).toEqual(undefined);
    });
    it('should handle error message when getChemicals failed ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
        .reply(HttpStatusCode.Forbidden, { randomProperty: 'randomValue' });

      const action = await store.dispatch(getChemicals(SEARCH_QUERY));
      expect(() => unwrapResult(action)).toThrow(
        "Failed to fetch chemicals: Access Forbidden! You don't have permission to access this resource.",
      );
    });
  });
});

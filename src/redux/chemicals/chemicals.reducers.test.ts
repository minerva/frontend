import { DEFAULT_ERROR } from '@/constants/errors';
import { chemicalsFixture } from '@/models/fixtures/chemicalsFixture';
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import chemicalsReducer from './chemicals.slice';
import { getChemicals } from './chemicals.thunks';
import { ChemicalsState } from './chemicals.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();
const SEARCH_QUERY = 'Corticosterone';

const INITIAL_STATE: ChemicalsState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
};

describe('chemicals reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ChemicalsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('chemicals', chemicalsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(chemicalsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after succesfull getChemicals query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, chemicalsFixture);

    const { type } = await store.dispatch(getChemicals(SEARCH_QUERY));
    const { data } = store.getState().chemicals;

    const chemicalsWithSearchElement = data.find(
      bioEntity => bioEntity.searchQueryElement === SEARCH_QUERY,
    );

    expect(type).toBe('project/getChemicals/fulfilled');
    expect(chemicalsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: chemicalsFixture,
      loading: 'succeeded',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store after failed getChemicals query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.NotFound, chemicalsFixture);

    const action = await store.dispatch(getChemicals(SEARCH_QUERY));
    const { data } = store.getState().chemicals;

    const chemicalsWithSearchElement = data.find(
      bioEntity => bioEntity.searchQueryElement === SEARCH_QUERY,
    );

    expect(action.type).toBe('project/getChemicals/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch chemicals: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(chemicalsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'failed',
      error: DEFAULT_ERROR,
    });
  });

  it('should update store on loading getChemicals query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getChemicalsStringWithQuery(SEARCH_QUERY))
      .reply(HttpStatusCode.Ok, chemicalsFixture);

    const chemicalsPromise = store.dispatch(getChemicals(SEARCH_QUERY));

    const { data } = store.getState().chemicals;
    const chemicalsWithSearchElement = data.find(
      bioEntity => bioEntity.searchQueryElement === SEARCH_QUERY,
    );

    expect(chemicalsWithSearchElement).toEqual({
      searchQueryElement: SEARCH_QUERY,
      data: undefined,
      loading: 'pending',
      error: DEFAULT_ERROR,
    });

    chemicalsPromise.then(() => {
      const { data: dataPromiseFulfilled } = store.getState().chemicals;

      const chemicalsWithSearchElementFulfilled = dataPromiseFulfilled.find(
        bioEntity => bioEntity.searchQueryElement === SEARCH_QUERY,
      );

      expect(chemicalsWithSearchElementFulfilled).toEqual({
        searchQueryElement: SEARCH_QUERY,
        data: chemicalsFixture,
        loading: 'succeeded',
        error: DEFAULT_ERROR,
      });
    });
  });
});

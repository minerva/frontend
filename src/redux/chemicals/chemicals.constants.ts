export const CHEMICALS_FETCHING_ERROR_PREFIX = 'Failed to fetch chemicals';
export const MULTI_CHEMICALS_FETCHING_ERROR_PREFIX = 'Failed to fetch multi chemicals';

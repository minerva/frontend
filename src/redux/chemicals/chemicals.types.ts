import { MultiFetchDataState } from '@/types/fetchDataState';
import { Chemical } from '@/types/models';

export type ChemicalsState = MultiFetchDataState<Chemical[]>;

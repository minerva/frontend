import { DEFAULT_ERROR } from '@/constants/errors';
import { ChemicalsState } from './chemicals.types';

export const CHEMICALS_INITIAL_STATE_MOCK: ChemicalsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

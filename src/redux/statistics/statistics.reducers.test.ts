import { PROJECT_ID } from '@/constants';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { waitFor } from '@testing-library/react';
import { statisticsFixture } from '@/models/fixtures/statisticsFixture';
import { unwrapResult } from '@reduxjs/toolkit';
import { StatisticsState } from './statistics.types';
import statisticsReducer from './statistics.slice';
import { apiPath } from '../apiPath';
import { getStatisticsById } from './statistics.thunks';

const mockedAxiosClient = mockNetworkResponse();

const INITIAL_STATE: StatisticsState = {
  data: undefined,
  loading: 'idle',
  error: { name: '', message: '' },
};

describe('statistics reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<StatisticsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('statistics', statisticsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(statisticsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful getStatisticById query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getStatisticsById(PROJECT_ID))
      .reply(HttpStatusCode.Ok, statisticsFixture);

    const { type } = await store.dispatch(getStatisticsById(PROJECT_ID));
    const { data, loading, error } = store.getState().statistics;

    expect(type).toBe('statistics/getStatisticsById/fulfilled');

    waitFor(() => {
      expect(loading).toEqual('pending');
    });

    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(statisticsFixture);
  });

  it('should update store after failed getStatisticById query', async () => {
    mockedAxiosClient
      .onGet(apiPath.getStatisticsById(PROJECT_ID))
      .reply(HttpStatusCode.NotFound, undefined);

    const action = await store.dispatch(getStatisticsById(PROJECT_ID));

    const { loading } = store.getState().statistics;

    expect(action.type).toBe('statistics/getStatisticsById/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch statistics: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );

    waitFor(() => {
      expect(loading).toEqual('pending');
    });

    expect(loading).toEqual('failed');
  });
});

import { DEFAULT_ERROR } from '@/constants/errors';
import { StatisticsState } from './statistics.types';

export const STATISTICS_STATE_INITIAL_MOCK: StatisticsState = {
  data: undefined,
  loading: 'idle',
  error: DEFAULT_ERROR,
};

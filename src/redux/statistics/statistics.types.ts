import { FetchDataState } from '@/types/fetchDataState';
import { Statistics } from '@/types/models';

export type StatisticsState = FetchDataState<Statistics>;

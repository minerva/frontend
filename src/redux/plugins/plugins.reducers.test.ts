/* eslint-disable no-magic-numbers */
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { pluginFixture } from '@/models/fixtures/pluginFixture';
import { unwrapResult } from '@reduxjs/toolkit';
import { apiPath } from '../apiPath';
import { PluginsState } from './plugins.types';
import pluginsReducer, { removePlugin } from './plugins.slice';
import { registerPlugin } from './plugins.thunks';
import { PLUGINS_INITIAL_STATE_MOCK } from './plugins.mock';

const mockedAxiosClient = mockNetworkResponse();

describe('plugins reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<PluginsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('plugins', pluginsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(pluginsReducer(undefined, action)).toEqual(PLUGINS_INITIAL_STATE_MOCK);
  });
  it('should remove overlay from store properly', () => {
    const { type, payload } = store.dispatch(
      removePlugin({
        pluginId: 'hash1',
      }),
    );

    expect(type).toBe('plugins/removePlugin');
    expect(payload).toEqual({ pluginId: 'hash1' });
  });
  it('should update store after successful registerPlugin query', async () => {
    mockedAxiosClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.Ok, pluginFixture);

    const { type } = await store.dispatch(
      registerPlugin({
        hash: pluginFixture.hash,
        isPublic: pluginFixture.isPublic,
        pluginName: pluginFixture.name,
        pluginUrl: pluginFixture.urls[0],
        pluginVersion: pluginFixture.version,
        extendedPluginName: pluginFixture.name,
        withoutPanel: false,
      }),
    );

    expect(type).toBe('plugins/registerPlugin/fulfilled');
    const { data, pluginsId } = store.getState().plugins.activePlugins;

    expect(data[pluginFixture.hash]).toEqual({ ...pluginFixture, withoutPanel: false });
    expect(pluginsId).toContain(pluginFixture.hash);
  });

  it('should update store after failed registerPlugin query', async () => {
    mockedAxiosClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.NotFound, undefined);

    const action = await store.dispatch(
      registerPlugin({
        hash: pluginFixture.hash,
        isPublic: pluginFixture.isPublic,
        pluginName: pluginFixture.name,
        pluginUrl: pluginFixture.urls[0],
        pluginVersion: pluginFixture.version,
        extendedPluginName: pluginFixture.name,
        withoutPanel: false,
      }),
    );

    expect(action.type).toBe('plugins/registerPlugin/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to register plugin: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    const { data, pluginsId } = store.getState().plugins.activePlugins;

    expect(data).toEqual({});

    expect(pluginsId).not.toContain(pluginFixture.hash);
  });

  it('should update store on loading registerPlugin query', async () => {
    mockedAxiosClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.NotFound, undefined);

    store.dispatch(
      registerPlugin({
        hash: pluginFixture.hash,
        isPublic: pluginFixture.isPublic,
        pluginName: pluginFixture.name,
        pluginUrl: pluginFixture.urls[0],
        pluginVersion: pluginFixture.version,
        extendedPluginName: pluginFixture.name,
        withoutPanel: false,
      }),
    );

    const { data, pluginsId } = store.getState().plugins.activePlugins;

    expect(data).toEqual({});
    expect(pluginsId).toContain(pluginFixture.hash);
  });
  it('should store loaded plugin with extendedPluginName as a pluginName', async () => {
    const extendedPluginName = `${pluginFixture.name} (2)`;
    mockedAxiosClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.Ok, pluginFixture);

    await store.dispatch(
      registerPlugin({
        hash: pluginFixture.hash,
        isPublic: pluginFixture.isPublic,
        pluginName: pluginFixture.name,
        pluginUrl: pluginFixture.urls[0],
        pluginVersion: pluginFixture.version,
        extendedPluginName,
        withoutPanel: false,
      }),
    );

    const { data } = store.getState().plugins.activePlugins;

    expect(data).toEqual({
      [pluginFixture.hash]: {
        ...pluginFixture,
        name: extendedPluginName,
        withoutPanel: false,
      },
    });
  });
  it('should set loaded plugin as current active plugin in plugins drawer', async () => {
    mockedAxiosClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.Ok, pluginFixture);

    const { type } = await store.dispatch(
      registerPlugin({
        hash: pluginFixture.hash,
        isPublic: pluginFixture.isPublic,
        pluginName: pluginFixture.name,
        pluginUrl: pluginFixture.urls[0],
        pluginVersion: pluginFixture.version,
        extendedPluginName: pluginFixture.name,
        withoutPanel: false,
      }),
    );

    expect(type).toBe('plugins/registerPlugin/fulfilled');
    const { currentPluginHash } = store.getState().plugins.drawer;

    expect(currentPluginHash).toBe(pluginFixture.hash);
  });
});

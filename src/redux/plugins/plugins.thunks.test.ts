/* eslint-disable no-magic-numbers */
import axios, { HttpStatusCode } from 'axios';
import { mockNetworkResponse } from '@/utils/mockNetworkResponse';
import MockAdapter from 'axios-mock-adapter';
import { pluginFixture } from '@/models/fixtures/pluginFixture';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { ONE, TWO } from '@/constants/common';
import { apiPath } from '../apiPath';
import { PluginsState } from './plugins.types';
import pluginsReducer from './plugins.slice';
import { getInitPlugins } from './plugins.thunks';

const mockedAxiosApiClient = mockNetworkResponse();
const mockedAxiosClient = new MockAdapter(axios);

describe('plugins - thunks', () => {
  describe('getInitPlugins', () => {
    let store = {} as ToolkitStoreWithSingleSlice<PluginsState>;
    beforeEach(() => {
      store = createStoreInstanceUsingSliceReducer('plugins', pluginsReducer);
    });
    const setHashedPluginMock = jest.fn();

    beforeEach(() => {
      setHashedPluginMock.mockClear();
    });

    it('should fetch and load initial plugins', async () => {
      const hash = 'hash';
      mockedAxiosApiClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.Ok, {
        ...pluginFixture,
        hash,
      });
      mockedAxiosApiClient.onGet(apiPath.getPlugin('hash')).reply(HttpStatusCode.Ok, {
        ...pluginFixture,
        hash,
      });
      mockedAxiosClient.onGet(pluginFixture.urls[0]).reply(HttpStatusCode.Ok, '');

      await store.dispatch(
        getInitPlugins({
          pluginsId: [hash],
          setHashedPlugin: setHashedPluginMock,
        }),
      );

      expect(setHashedPluginMock).toHaveBeenCalledTimes(1);
    });
    it('should not load plugin if fetched plugin is not valid', async () => {
      mockedAxiosApiClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.NotFound, {});
      mockedAxiosApiClient
        .onGet(apiPath.getPlugin(pluginFixture.hash))
        .reply(HttpStatusCode.NotFound, {});
      mockedAxiosClient.onGet(pluginFixture.urls[0]).reply(HttpStatusCode.NotFound, '');

      await store.dispatch(
        getInitPlugins({
          pluginsId: [pluginFixture.hash],
          setHashedPlugin: setHashedPluginMock,
        }),
      );

      expect(setHashedPluginMock).not.toHaveBeenCalled();
    });
    it('should allow to load one plugin multiple times with the same data', async () => {
      const hash = 'hash';
      const hashWithPrefix = `prefix-${hash}`;
      const script = 'function init() {} init()';

      mockedAxiosApiClient.onPost(apiPath.registerPluign()).reply(HttpStatusCode.Ok, {
        ...pluginFixture,
        hash,
      });
      mockedAxiosApiClient.onGet(apiPath.getPlugin(hash)).reply(HttpStatusCode.Ok, {
        ...pluginFixture,
        hash,
      });
      mockedAxiosClient.onGet(pluginFixture.urls[0]).reply(HttpStatusCode.Ok, script);

      await store.dispatch(
        getInitPlugins({
          pluginsId: [hash, hashWithPrefix],
          setHashedPlugin: setHashedPluginMock,
        }),
      );

      expect(setHashedPluginMock).toHaveBeenCalledTimes(2);
      expect(setHashedPluginMock).toHaveBeenNthCalledWith(ONE, {
        pluginScript: script,
        pluginUrl: pluginFixture.urls[0],
      });

      expect(setHashedPluginMock).toHaveBeenNthCalledWith(TWO, {
        pluginScript: script,
        pluginUrl: pluginFixture.urls[0],
      });
    });
  });
});

import { PayloadAction } from '@reduxjs/toolkit';

import { FetchDataState } from '@/types/fetchDataState';
import { MinervaPlugin } from '@/types/models';

export type RemovePluginPayload = { pluginId: string };
export type RemovePluginAction = PayloadAction<RemovePluginPayload>;

export type PluginsList = FetchDataState<MinervaPlugin[]>;
export type ActivePlugins = {
  pluginsId: string[];
  data: {
    [pluginId: string]: MinervaPlugin;
  };
};

export type PluginsDrawer = {
  isOpen: boolean;
  currentPluginHash?: string;
};

export type PluginsState = {
  list: PluginsList;
  activePlugins: ActivePlugins;
  drawer: PluginsDrawer;
};

export type SetCurrentDrawerPluginHashPayload = string;
export type SetCurrentDrawerPluginHashAction = PayloadAction<SetCurrentDrawerPluginHashPayload>;

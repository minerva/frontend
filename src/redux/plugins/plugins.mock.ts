import { DEFAULT_ERROR } from '@/constants/errors';
import { ActivePlugins, PluginsDrawer, PluginsList, PluginsState } from './plugins.types';

export const PLUGINS_INITIAL_STATE_ACTIVE_PLUGINS_MOCK: ActivePlugins = {
  data: {},
  pluginsId: [],
};

export const PLUGINS_INITIAL_STATE_LIST_MOCK: PluginsList = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

export const PLUGINS_INITIAL_STATE_DRAWER_MOCK: PluginsDrawer = {
  isOpen: false,
  currentPluginHash: undefined,
};

export const PLUGINS_INITIAL_STATE_MOCK: PluginsState = {
  list: PLUGINS_INITIAL_STATE_LIST_MOCK,
  activePlugins: PLUGINS_INITIAL_STATE_ACTIVE_PLUGINS_MOCK,
  drawer: PLUGINS_INITIAL_STATE_DRAWER_MOCK,
};

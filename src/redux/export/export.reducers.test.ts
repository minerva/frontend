import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { ExportState } from './export.types';
import exportReducer from './export.slice';
import { apiPath } from '../apiPath';
import { downloadNetwork, downloadElements } from './export.thunks';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: ExportState = {
  downloadNetwork: {
    loading: 'idle',
    error: { name: '', message: '' },
  },
  downloadElements: {
    loading: 'idle',
    error: { name: '', message: '' },
  },
};

describe('export reducer', () => {
  global.URL.createObjectURL = jest.fn();
  let store = {} as ToolkitStoreWithSingleSlice<ExportState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('export', exportReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };
    expect(exportReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful downloadNetwork query', async () => {
    mockedAxiosClient.onPost(apiPath.downloadNetworkCsv()).reply(HttpStatusCode.Ok, 'test');
    await store.dispatch(
      downloadNetwork({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );
    const { loading } = store.getState().export.downloadNetwork;

    expect(loading).toEqual('succeeded');
  });

  it('should update store on loading downloadNetwork query', async () => {
    mockedAxiosClient.onPost(apiPath.downloadNetworkCsv()).reply(HttpStatusCode.Ok, 'test');
    const downloadNetworkPromise = store.dispatch(
      downloadNetwork({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );

    const { loading } = store.getState().export.downloadNetwork;
    expect(loading).toEqual('pending');

    await downloadNetworkPromise;

    const { loading: promiseFulfilled } = store.getState().export.downloadNetwork;

    expect(promiseFulfilled).toEqual('succeeded');
  });

  it('should update store after failed downloadNetwork query', async () => {
    mockedAxiosClient
      .onPost(apiPath.downloadNetworkCsv())
      .reply(HttpStatusCode.NotFound, undefined);
    const action = await store.dispatch(
      downloadNetwork({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );
    expect(action.type).toBe('export/downloadNetwork/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to download network: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    const { loading } = store.getState().export.downloadNetwork;
    expect(loading).toEqual('failed');
  });

  it('should update store after successful downloadElements query', async () => {
    mockedAxiosClient.onPost(apiPath.downloadElementsCsv()).reply(HttpStatusCode.Ok, 'test');
    await store.dispatch(
      downloadElements({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );
    const { loading } = store.getState().export.downloadElements;

    expect(loading).toEqual('succeeded');
  });

  it('should update store on loading downloadElements query', async () => {
    mockedAxiosClient.onPost(apiPath.downloadElementsCsv()).reply(HttpStatusCode.Ok, 'test');
    const downloadElementsPromise = store.dispatch(
      downloadElements({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );

    const { loading } = store.getState().export.downloadElements;
    expect(loading).toEqual('pending');

    await downloadElementsPromise;

    const { loading: promiseFulfilled } = store.getState().export.downloadElements;

    expect(promiseFulfilled).toEqual('succeeded');
  });

  it('should update store after failed downloadElements query', async () => {
    mockedAxiosClient
      .onPost(apiPath.downloadElementsCsv())
      .reply(HttpStatusCode.NotFound, undefined);
    const action = await store.dispatch(
      downloadElements({
        annotations: [],
        columns: [],
        excludedCompartmentIds: [],
        includedCompartmentIds: [],
        submaps: [],
      }),
    );
    const { loading } = store.getState().export.downloadElements;

    expect(loading).toEqual('failed');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to download elements: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
  });
});

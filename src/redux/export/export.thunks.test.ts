import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { apiPath } from '../apiPath';
import { ExportState } from './export.types';
import exportReducer from './export.slice';
import { downloadNetwork, downloadElements } from './export.thunks';

const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('export thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ExportState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('export', exportReducer);

    global.URL.createObjectURL = jest.fn();
    global.document.body.appendChild = jest.fn();
  });
  describe('downloadNetwork', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('should download file when data response from API is valid', async () => {
      mockedAxiosClient.onPost(apiPath.downloadNetworkCsv()).reply(HttpStatusCode.Ok, 'test');

      await store.dispatch(
        downloadNetwork({
          annotations: [],
          columns: [],
          excludedCompartmentIds: [],
          includedCompartmentIds: [],
          submaps: [],
        }),
      );
      expect(global.URL.createObjectURL).toHaveBeenCalledWith(new Blob(['test']));

      expect(global.document.body.appendChild).toHaveBeenCalled();
    });
    it('should not download file when data response from API is not valid', async () => {
      mockedAxiosClient
        .onPost(apiPath.downloadNetworkCsv())
        .reply(HttpStatusCode.NotFound, undefined);

      await store.dispatch(
        downloadNetwork({
          annotations: [],
          columns: [],
          excludedCompartmentIds: [],
          includedCompartmentIds: [],
          submaps: [],
        }),
      );

      expect(global.document.body.appendChild).not.toHaveBeenCalled();
    });
  });

  describe('downloadElements', () => {
    beforeEach(() => {
      jest.clearAllMocks();
    });
    it('should download file when data response from API is valid', async () => {
      mockedAxiosClient.onPost(apiPath.downloadElementsCsv()).reply(HttpStatusCode.Ok, 'test');

      await store.dispatch(
        downloadElements({
          annotations: [],
          columns: [],
          excludedCompartmentIds: [],
          includedCompartmentIds: [],
          submaps: [],
        }),
      );
      expect(global.URL.createObjectURL).toHaveBeenCalledWith(new Blob(['test']));

      expect(global.document.body.appendChild).toHaveBeenCalled();
    });
    it('should not download file when data response from API is not valid', async () => {
      mockedAxiosClient
        .onPost(apiPath.downloadElementsCsv())
        .reply(HttpStatusCode.NotFound, undefined);

      await store.dispatch(
        downloadElements({
          annotations: [],
          columns: [],
          excludedCompartmentIds: [],
          includedCompartmentIds: [],
          submaps: [],
        }),
      );

      expect(global.document.body.appendChild).not.toHaveBeenCalled();
    });
  });
});

import { ExportState } from './export.types';

export const EXPORT_INITIAL_STATE_MOCK: ExportState = {
  downloadNetwork: {
    error: {
      message: '',
      name: '',
    },
    loading: 'idle',
  },
  downloadElements: {
    error: {
      message: '',
      name: '',
    },
    loading: 'idle',
  },
};

import { Loading } from '@/types/loadingState';

export type ExportState = {
  downloadNetwork: {
    loading: Loading;
    error: Error;
  };
  downloadElements: {
    loading: Loading;
    error: Error;
  };
};

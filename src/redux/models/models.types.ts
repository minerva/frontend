import { FetchDataState } from '@/types/fetchDataState';
import { MapModel } from '@/types/models';

export type ModelsState = FetchDataState<MapModel[], []>;

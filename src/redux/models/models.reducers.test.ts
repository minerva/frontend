import { modelsFixture, modelsPageFixture } from '@/models/fixtures/modelsFixture';
import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import modelsReducer from './models.slice';
import { getModels } from './models.thunks';
import { ModelsState } from './models.types';

const mockedAxiosNEWApiClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: ModelsState = {
  data: [],
  loading: 'idle',
  error: { name: '', message: '' },
};

describe('models reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModelsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('models', modelsReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(modelsReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should update store after successfull getModels query', async () => {
    mockedAxiosNEWApiClient
      .onGet(apiPath.getModelsString())
      .reply(HttpStatusCode.Ok, modelsPageFixture);

    const { type } = await store.dispatch(getModels());
    const { data, loading, error } = store.getState().models;

    expect(type).toBe('project/getModels/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual(modelsFixture);
  });

  it('should update store after failed getModels query', async () => {
    mockedAxiosNEWApiClient.onGet(apiPath.getModelsString()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getModels());
    const { data, loading, error } = store.getState().models;

    expect(action.type).toBe('project/getModels/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch models: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual([]);
  });

  it('should update store on loading getModels query', async () => {
    mockedAxiosNEWApiClient
      .onGet(apiPath.getModelsString())
      .reply(HttpStatusCode.Ok, modelsPageFixture);

    const modelsPromise = store.dispatch(getModels());

    const { data, loading } = store.getState().models;
    expect(data).toEqual([]);
    expect(loading).toEqual('pending');

    modelsPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } = store.getState().models;

      expect(dataPromiseFulfilled).toEqual(modelsFixture);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

/* eslint-disable no-magic-numbers */
import { ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { getModels } from './models.thunks';
import { ModelsState } from './models.types';

export const getModelsReducer = (builder: ActionReducerMapBuilder<ModelsState>): void => {
  builder.addCase(getModels.pending, state => {
    state.loading = 'pending';
  });
  builder.addCase(getModels.fulfilled, (state, action) => {
    state.data = action.payload || [];
    state.loading = 'succeeded';
  });
  builder.addCase(getModels.rejected, state => {
    state.loading = 'failed';
    // TODO to discuss manage state of failure
  });
};

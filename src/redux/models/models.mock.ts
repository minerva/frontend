import { DEFAULT_ERROR } from '@/constants/errors';
import { ModelsState } from './models.types';

export const MODELS_INITIAL_STATE_MOCK: ModelsState = {
  data: [],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

export const MODELS_DATA_MOCK_WITH_MAIN_MAP: ModelsState = {
  data: [
    {
      id: 52,
      width: 26779.25,
      height: 13503,
      defaultCenterX: null,
      defaultCenterY: null,
      description: '',
      name: 'Core PD map',
      defaultZoomLevel: null,
      tileSize: 256,
      references: [],
      authors: [],
      creationDate: null,
      modificationDates: [],
      minZoom: 2,
      maxZoom: 9,
    },
  ],
  loading: 'idle',
  error: DEFAULT_ERROR,
};

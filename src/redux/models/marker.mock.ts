import { MarkerPin, MarkerSurface } from '@/types/models';

export const SURFACE_MARKER: MarkerSurface = {
  type: 'surface',
  id: '1',
  color: '#000000',
  opacity: 0.1,
  x: 1200,
  y: 500,
  width: 100,
  height: 50,
  number: 0,
  modelId: 0,
};

export const PIN_MARKER: MarkerPin = {
  type: 'pin',
  id: '1',
  color: '#000000',
  opacity: 0.1,
  x: 1200,
  y: 500,
  number: 0,
  modelId: 0,
};

import { modelsFixture, modelsPageFixture } from '@/models/fixtures/modelsFixture';
import { apiPath } from '@/redux/apiPath';
import { ModelsState } from '@/redux/models/models.types';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import modelsReducer from './models.slice';
import { getModels } from './models.thunks';

const mockedAxiosNEWApiClient = mockNetworkNewAPIResponse();

describe('models thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ModelsState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('models', modelsReducer);
  });
  describe('getModels', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosNEWApiClient
        .onGet(apiPath.getModelsString())
        .reply(HttpStatusCode.Ok, modelsPageFixture);

      const { payload } = await store.dispatch(getModels());
      expect(payload).toEqual(modelsFixture);
    });
    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosNEWApiClient
        .onGet(apiPath.getModelsString())
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getModels());
      expect(payload).toEqual(undefined);
    });
  });
});

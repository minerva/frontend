import { CookieBannerState } from './cookieBanner.types';

export const COOKIE_BANNER_INITIAL_STATE_MOCK: CookieBannerState = {
  visible: false,
  accepted: false,
};

export type CookieBannerState = {
  accepted: boolean;
  visible: boolean;
};

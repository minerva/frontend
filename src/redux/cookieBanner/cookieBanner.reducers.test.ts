import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { CookieBannerState } from './cookieBanner.types';
import cookieBannerReducer, { acceptCookies, hideBanner, showBanner } from './cookieBanner.slice';

const INITIAL_STATE: CookieBannerState = {
  accepted: false,
  visible: false,
};

describe('cookieBanner reducers', () => {
  let store = {} as ToolkitStoreWithSingleSlice<CookieBannerState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('cookieBanner', cookieBannerReducer);
  });
  it('should match initial state', () => {
    const action = { type: 'unknown' };
    expect(cookieBannerReducer(undefined, action)).toEqual(INITIAL_STATE);
  });
  it('should handle showBanner action', () => {
    store.dispatch(showBanner());

    const { visible } = store.getState().cookieBanner;
    expect(visible).toEqual(true);
  });

  it('should handle hideBanner action', () => {
    store.dispatch(hideBanner());
    const { visible } = store.getState().cookieBanner;

    expect(visible).toEqual(false);
  });

  it('should handle acceptCookies action', () => {
    store.dispatch(acceptCookies());

    const expectedState: CookieBannerState = {
      visible: false,
      accepted: true,
    };

    const currentState = store.getState().cookieBanner;
    expect(currentState).toEqual(expectedState);
  });
});

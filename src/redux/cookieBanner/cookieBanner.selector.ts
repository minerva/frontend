import { createSelector } from '@reduxjs/toolkit';
import { rootSelector } from '../root/root.selectors';

export const selectCookieBanner = createSelector(rootSelector, state => state.cookieBanner);

import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { unwrapResult } from '@reduxjs/toolkit';
import { bioShapesFixture } from '@/models/fixtures/bioShapesFixture';
import { SHAPES_STATE_INITIAL_MOCK } from '@/redux/shapes/shapes.mock';
import { lineTypesFixture } from '@/models/fixtures/lineTypesFixture';
import { arrowTypesFixture } from '@/models/fixtures/arrowTypesFixture';
import arrayToDict from '@/utils/array/arrayToDict';
import shapesReducer from './shapes.slice';
import { getArrowTypes, getLineTypes, getShapes } from './shapes.thunks';
import { ArrowTypeDict, BioShapesDict, LineTypeDict, ShapesState } from './shapes.types';

const mockedAxiosClient = mockNetworkNewAPIResponse();

const INITIAL_STATE: ShapesState = SHAPES_STATE_INITIAL_MOCK;

describe('shapes reducer', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ShapesState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('shapes', shapesReducer);
  });

  it('should match initial state', () => {
    const action = { type: 'unknown' };

    expect(shapesReducer(undefined, action)).toEqual(INITIAL_STATE);
  });

  it('should update store after successful getShapes query', async () => {
    mockedAxiosClient.onGet(apiPath.getShapes()).reply(HttpStatusCode.Ok, bioShapesFixture);

    const { type } = await store.dispatch(getShapes());
    const { data, loading, error } = store.getState().shapes.bioShapesState;
    expect(type).toBe('vectorMap/getShapes/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    const expectedResult = arrayToDict(bioShapesFixture, 'sboTerm', 'shapes') as BioShapesDict;
    expect(data).toEqual(expectedResult);
  });

  it('should update store after successful getLineTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getLineTypes()).reply(HttpStatusCode.Ok, lineTypesFixture);

    const { type } = await store.dispatch(getLineTypes());
    const { data, loading, error } = store.getState().shapes.lineTypesState;
    expect(type).toBe('vectorMap/getLineTypes/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });

    const expectedResult = arrayToDict(lineTypesFixture, 'name', 'pattern') as LineTypeDict;
    expect(data).toEqual(expectedResult);
  });

  it('should update store after successful getArrowTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getArrowTypes()).reply(HttpStatusCode.Ok, arrowTypesFixture);

    const { type } = await store.dispatch(getArrowTypes());
    const { data, loading, error } = store.getState().shapes.arrowTypesState;
    expect(type).toBe('vectorMap/getArrowTypes/fulfilled');
    expect(loading).toEqual('succeeded');
    expect(error).toEqual({ message: '', name: '' });
    const expectedResult = arrayToDict(arrowTypesFixture, 'arrowType', 'shapes') as ArrowTypeDict;
    expect(data).toEqual(expectedResult);
  });

  it('should update store after failed getShapes query', async () => {
    mockedAxiosClient.onGet(apiPath.getShapes()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getShapes());
    const { data, loading, error } = store.getState().shapes.bioShapesState;

    expect(action.type).toBe('vectorMap/getShapes/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch shapes: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual({});
  });

  it('should update store after failed getLineTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getLineTypes()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getLineTypes());
    const { data, loading, error } = store.getState().shapes.lineTypesState;

    expect(action.type).toBe('vectorMap/getLineTypes/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch line types: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual({});
  });

  it('should update store after failed getArrowTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getArrowTypes()).reply(HttpStatusCode.NotFound, []);

    const action = await store.dispatch(getArrowTypes());
    const { data, loading, error } = store.getState().shapes.arrowTypesState;

    expect(action.type).toBe('vectorMap/getArrowTypes/rejected');
    expect(() => unwrapResult(action)).toThrow(
      "Failed to fetch arrow types: The page you're looking for doesn't exist. Please verify the URL and try again.",
    );
    expect(loading).toEqual('failed');
    expect(error).toEqual({ message: '', name: '' });
    expect(data).toEqual({});
  });

  it('should update store on loading getShapes query', async () => {
    mockedAxiosClient.onGet(apiPath.getShapes()).reply(HttpStatusCode.Ok, bioShapesFixture);

    const shapesPromise = store.dispatch(getShapes());

    const { data, loading } = store.getState().shapes.bioShapesState;
    expect(data).toEqual({});
    expect(loading).toEqual('pending');

    shapesPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } =
        store.getState().shapes.bioShapesState;
      const expectedResult = arrayToDict(bioShapesFixture, 'sboTerm', 'shapes') as BioShapesDict;
      expect(dataPromiseFulfilled).toEqual(expectedResult);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });

  it('should update store on loading getLineTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getLineTypes()).reply(HttpStatusCode.Ok, lineTypesFixture);

    const lineTypesPromise = store.dispatch(getLineTypes());

    const { data, loading } = store.getState().shapes.lineTypesState;
    expect(data).toEqual({});
    expect(loading).toEqual('pending');

    lineTypesPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } =
        store.getState().shapes.lineTypesState;
      const expectedResult = arrayToDict(lineTypesFixture, 'name', 'pattern') as LineTypeDict;
      expect(dataPromiseFulfilled).toEqual(expectedResult);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });

  it('should update store on loading getArrowTypes query', async () => {
    mockedAxiosClient.onGet(apiPath.getArrowTypes()).reply(HttpStatusCode.Ok, arrowTypesFixture);

    const arrowTypesPromise = store.dispatch(getArrowTypes());

    const { data, loading } = store.getState().shapes.arrowTypesState;
    expect(data).toEqual({});
    expect(loading).toEqual('pending');

    arrowTypesPromise.then(() => {
      const { data: dataPromiseFulfilled, loading: promiseFulfilled } =
        store.getState().shapes.arrowTypesState;
      const expectedResult = arrayToDict(arrowTypesFixture, 'arrowType', 'shapes') as ArrowTypeDict;
      expect(dataPromiseFulfilled).toEqual(expectedResult);
      expect(promiseFulfilled).toEqual('succeeded');
    });
  });
});

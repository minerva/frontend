export const SHAPES_FETCHING_ERROR_PREFIX = 'Failed to fetch shapes';

export const LINE_TYPES_FETCHING_ERROR_PREFIX = 'Failed to fetch line types';

export const ARROW_TYPES_FETCHING_ERROR_PREFIX = 'Failed to fetch arrow types';

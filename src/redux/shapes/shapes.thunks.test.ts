import { apiPath } from '@/redux/apiPath';
import {
  ToolkitStoreWithSingleSlice,
  createStoreInstanceUsingSliceReducer,
} from '@/utils/createStoreInstanceUsingSliceReducer';
import { mockNetworkNewAPIResponse } from '@/utils/mockNetworkResponse';
import { HttpStatusCode } from 'axios';
import { bioShapesFixture } from '@/models/fixtures/bioShapesFixture';
import {
  ArrowTypeDict,
  BioShapesDict,
  LineTypeDict,
  ShapesState,
} from '@/redux/shapes/shapes.types';
import { lineTypesFixture } from '@/models/fixtures/lineTypesFixture';
import { arrowTypesFixture } from '@/models/fixtures/arrowTypesFixture';
import arrayToDict from '@/utils/array/arrayToDict';
import shapesReducer from './shapes.slice';
import { getArrowTypes, getLineTypes, getShapes } from './shapes.thunks';

const mockedAxiosClient = mockNetworkNewAPIResponse();

describe('shapes thunks', () => {
  let store = {} as ToolkitStoreWithSingleSlice<ShapesState>;
  beforeEach(() => {
    store = createStoreInstanceUsingSliceReducer('shapes', shapesReducer);
  });

  describe('getShapes', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getShapes()).reply(HttpStatusCode.Ok, bioShapesFixture);

      const { payload } = await store.dispatch(getShapes());
      const expectedResult = arrayToDict(bioShapesFixture, 'sboTerm', 'shapes') as BioShapesDict;
      expect(payload).toEqual(expectedResult);
    });

    it('should return empty object when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getShapes())
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getShapes());
      expect(payload).toEqual({});
    });
  });

  describe('getLineTypes', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getLineTypes()).reply(HttpStatusCode.Ok, lineTypesFixture);

      const { payload } = await store.dispatch(getLineTypes());
      const expectedResult = arrayToDict(lineTypesFixture, 'name', 'pattern') as LineTypeDict;
      expect(payload).toEqual(expectedResult);
    });

    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getLineTypes())
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getLineTypes());
      expect(payload).toEqual({});
    });
  });

  describe('getArrowTypes', () => {
    it('should return data when data response from API is valid', async () => {
      mockedAxiosClient.onGet(apiPath.getArrowTypes()).reply(HttpStatusCode.Ok, arrowTypesFixture);

      const { payload } = await store.dispatch(getArrowTypes());
      const expectedResult = arrayToDict(arrowTypesFixture, 'arrowType', 'shapes') as ArrowTypeDict;
      expect(payload).toEqual(expectedResult);
    });

    it('should return undefined when data response from API is not valid ', async () => {
      mockedAxiosClient
        .onGet(apiPath.getArrowTypes())
        .reply(HttpStatusCode.Ok, { randomProperty: 'randomValue' });

      const { payload } = await store.dispatch(getArrowTypes());
      expect(payload).toEqual({});
    });
  });
});

import { FetchDataState } from '@/types/fetchDataState';
import { Shape } from '@/types/models';

export type LineTypeDict = {
  [key: string]: Array<number>;
};

export type ArrowTypeDict = {
  [key: string]: Array<Shape>;
};

export type BioShapesDict = {
  [key: string]: Array<Shape>;
};

export type LineTypesState = FetchDataState<LineTypeDict>;

export type ArrowTypesState = FetchDataState<ArrowTypeDict>;

export type BioShapesState = FetchDataState<BioShapesDict>;

export type ShapesState = {
  lineTypesState: LineTypesState;
  arrowTypesState: ArrowTypesState;
  bioShapesState: BioShapesState;
};

import { Marker } from '@/types/models';

export interface MarkerWithOptionalId extends Omit<Marker, 'id'> {
  id?: string;
}

export interface MarkersState {
  data: Marker[];
}

import { MarkersState } from './markers.types';

export const MARKERS_INITIAL_STATE: MarkersState = {
  data: [],
};

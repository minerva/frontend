import { Marker, MarkerLine, MarkerSurface } from '@/types/models';

export const isMarkerSurface = (marker: Marker): marker is MarkerSurface =>
  Boolean('width' in marker && marker?.width && marker?.height && marker.type === 'surface');

export const isMarkerLine = (marker: Marker): marker is MarkerLine =>
  Boolean(
    'start' in marker && 'end' in marker && marker?.start && marker?.end && marker.type === 'line',
  );

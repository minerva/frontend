import { MarkersState } from './markers.types';

export const MARKERS_INITIAL_STATE_MOCK: MarkersState = {
  data: [],
};

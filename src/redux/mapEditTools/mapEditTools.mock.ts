import { MapEditToolsState } from '@/redux/mapEditTools/mapEditTools.types';

export const MAP_EDIT_TOOLS_STATE_INITIAL_MOCK: MapEditToolsState = {
  activeAction: null,
  layerObject: null,
  layerLine: null,
};

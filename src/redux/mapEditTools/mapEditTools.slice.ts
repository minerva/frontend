import { createSlice } from '@reduxjs/toolkit';
import { MAP_EDIT_TOOLS_STATE_INITIAL_MOCK } from '@/redux/mapEditTools/mapEditTools.mock';
import {
  mapEditToolsSetActiveActionReducer,
  mapEditToolsSetLayerLineReducer,
  mapEditToolsSetLayerObjectReducer,
} from '@/redux/mapEditTools/mapEditTools.reducers';

export const layersSlice = createSlice({
  name: 'layers',
  initialState: MAP_EDIT_TOOLS_STATE_INITIAL_MOCK,
  reducers: {
    mapEditToolsSetActiveAction: mapEditToolsSetActiveActionReducer,
    mapEditToolsSetLayerObject: mapEditToolsSetLayerObjectReducer,
    mapEditToolsSetLayerLine: mapEditToolsSetLayerLineReducer,
  },
});

export const { mapEditToolsSetActiveAction, mapEditToolsSetLayerObject, mapEditToolsSetLayerLine } =
  layersSlice.actions;

export default layersSlice.reducer;

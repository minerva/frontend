/* eslint-disable no-magic-numbers */
import { createSelector } from '@reduxjs/toolkit';
import { rootSelector } from '@/redux/root/root.selectors';

export const mapEditToolsSelector = createSelector(rootSelector, state => state.mapEditTools);

export const mapEditToolsActiveActionSelector = createSelector(
  mapEditToolsSelector,
  state => state.activeAction,
);

export const mapEditToolsLayerObjectSelector = createSelector(
  mapEditToolsSelector,
  state => state.layerObject || state.layerLine,
);

export const mapEditToolsLayerLineSelector = createSelector(
  mapEditToolsSelector,
  state => state.layerLine,
);

export const mapEditToolsLayerNonLineObjectSelector = createSelector(
  mapEditToolsSelector,
  state => state.layerObject,
);

export const isMapEditToolsActiveSelector = createSelector(mapEditToolsSelector, state =>
  Boolean(state.activeAction),
);

import { MAP_EDIT_ACTIONS } from '@/redux/mapEditTools/mapEditTools.constants';
import { LayerImage, LayerLine, LayerOval, LayerRect, LayerText } from '@/types/models';

export type MapEditToolsState = {
  activeAction: keyof typeof MAP_EDIT_ACTIONS | null;
  layerObject: LayerImage | LayerText | LayerRect | LayerOval | null;
  layerLine: LayerLine | null;
};

import { getBounds } from '@/services/pluginsManager/map/data/getBounds';
import { fitBounds } from '@/services/pluginsManager/map/fitBounds';
import { getOpenMapId } from '@/services/pluginsManager/map/getOpenMapId';
import { triggerSearch } from '@/services/pluginsManager/map/triggerSearch';
import { getModels } from '@/services/pluginsManager/map/models/getModels';
import { openMap } from '@/services/pluginsManager/map/openMap';
import { getCenter } from '@/services/pluginsManager/map/position/getCenter';
import { setCenter } from '@/services/pluginsManager/map/position/setCenter';
import { getZoom } from '@/services/pluginsManager/map/zoom/getZoom';
import { setZoom } from '@/services/pluginsManager/map/zoom/setZoom';
import { getDisease } from '@/services/pluginsManager/project/data/getDisease';
import { getName } from '@/services/pluginsManager/project/data/getName';
import { getOrganism } from '@/services/pluginsManager/project/data/getOrganism';
import { getProjectId } from '@/services/pluginsManager/project/data/getProjectId';
import { getVersion } from '@/services/pluginsManager/project/data/getVersion';
import { getDataOverlays } from '@/services/pluginsManager/map/overlays/getDataOverlays';
import { getVisibleDataOverlays } from '@/services/pluginsManager/map/overlays/getVisibleDataOverlays';
import { showDataOverlay } from '@/services/pluginsManager/map/overlays/showDataOverlay';
import { hideDataOverlay } from '@/services/pluginsManager/map/overlays/hideDataOverlay';
import { removeDataOverlay } from '@/services/pluginsManager/map/overlays/removeDataOverlay';
import { addDataOverlay } from '@/services/pluginsManager/map/overlays/addDataOverlay';
import { getApiUrls } from '@/services/pluginsManager/project/data/getApiUrls';
import { getOpenedPanel } from '@/services/pluginsManager/interface/getOpenedPanel';
import { hidePanel } from '@/services/pluginsManager/interface/hidePanel';

type Plugin = {
  pluginName: string;
  pluginVersion: string;
  pluginUrl: string;
  withoutPanel: boolean | undefined;
};

type RegisterPlugin = ({ pluginName, pluginVersion, pluginUrl }: Plugin) => {
  element: HTMLDivElement;
};

type HashPlugin = {
  pluginUrl: string;
  pluginScript: string;
};

declare global {
  interface Window {
    config: {
      BASE_API_URL: string;
      BASE_NEW_API_URL: string;
      BASE_MAP_IMAGES_URL: string;
      DEFAULT_PROJECT_ID: string;
      ADMIN_PANEL_URL: string;
    };

    // plugins try to access those in the previous version of minerva
    minervaDefine: typeof minervaDefine;

    minerva: {
      configuration?: MinervaConfiguration;
      plugins: {
        registerPlugin: RegisterPlugin;
      };
      data: {
        bioEntities: BioEntitiesMethods;
      };
      map: {
        data: {
          getBounds: typeof getBounds;
          getOpenMapId: typeof getOpenMapId;
          getModels: typeof getModels;
        };
        fitBounds: typeof fitBounds;
        openMap: typeof openMap;
        triggerSearch: typeof triggerSearch;
        getZoom: typeof getZoom;
        setZoom: typeof setZoom;
        getCenter: typeof getCenter;
        setCenter: typeof setCenter;
      };
      interface: {
        getOpenedPanel: typeof getOpenedPanel;
        hidePanel: typeof hidePanel;
        openPanel: typeof openPanel;
      };
      overviewImage: {
        getCurrentOverviewImage: typeof getCurrentOverviewImage;
        getOverviewImages: typeof getOverviewImages;
        hideOverviewImageModal: typeof hideOverviewImageModal;
        selectOverviewImage: typeof selectOverviewImage;
        showOverviewImageModal: typeof showOverviewImageModal;
      };
      overlays: {
        data: {
          getDataOverlays: typeof getDataOverlays;
          getVisibleDataOverlays: typeof getVisibleDataOverlays;
        };
        showDataOverlay: typeof showDataOverlay;
        hideDataOverlay: typeof hideDataOverlay;
        removeDataOverlay: typeof removeDataOverlay;
        addDataOverlay: typeof addDataOverlay;
      };
      project: {
        data: {
          getProjectId: typeof getProjectId;
          getName: typeof getName;
          getVersion: typeof getVersion;
          getDisease: typeof getDisease;
          getOrganism: typeof getOrganism;
          getApiUrls: typeof getApiUrls;
        };
      };
    };
  }
}
